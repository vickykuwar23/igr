    <!-- about redresel -->
    <section class="complain-wrapper" id="complain-wrapper">
	  <div class="container-fluid">
            <div class="row">
			 <div class="col-md-2">
			 </div>
        <div class="col-md-8 mt-3">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="title-heading">
                                <h1 class="heading">Hyperlink Policy</h1>
                            </div>
                        </div>
                    </div>
                    <div class="inner-content">
						<h4>Links to external websites/portals</h4>
                        <p>At many places in this website, you shall find links to other websites/portals. This links have been placed for your convenience. Department of Registration & Stamps is not responsible for the contents and reliability of the linked websites and does not necessarily endorse the views expressed in them. Mere presence of the link or its listing on this website should not be assumed as endorsement of any kind. We cannot guarantee that these links will work all the time and we have no control over availability of linked pages.</p>
						<h4>Links to Department of Registration & Stamps, Govt. of Maharashtra:</h4>
						<p>Prior permission is required before hyperlinks are directed from any website/portal to this site. Permission for the same, stating the nature of the content on the pages from where the link has to be given and the exact language of the Hyperlink should be obtained by sending a request to stake holder.</p>
						
                    </div>
                </div>
				<div class="col-md-2">
			 </div>
			 </div>
         </div>    
    </section>
    <!-- //about redresel -->

