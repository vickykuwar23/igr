 <!-- asidebar -->
    <section class="sitemap-wrapper">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-12">
                    <div class="title-heading">
                        <h1 class="heading">Contact Us</h1>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-sm-4">
                    <div class="get-in-touch">
                        <p> If you are not happy with the service provided by the Dept. and
                            want to lodge a complaint, please send us mail at </p>
                        <a href="mailto:complaint@igrmaharashtra.gov.in"
                            class="mail-lbl">complaint@igrmaharashtra.gov.in </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="get-in-touch">
                        <p> If you want to give feedback about the Citizen's Charter or any other activity of the Dept.,
                            please send us mail at</p>
                        <a href="mailto:feedback@igrmaharashtra.gov.in"
                            class="mail-lbl">feedback@igrmaharashtra.gov.in</a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="get-in-touch">
                        <p> If you think that the functioning of the Dept. can be improved and if you have an idea for
                            the same, please send us mail at </p>
                        <a href="mailto:idea@igrmaharashtra.gov.in" class="mail-lbl">idea@igrmaharashtra.gov.in</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 mx-auto">
                    <div class="address_block">
                        <h5>Address</h5>
                        <p>Office of the Inspector General of Registration and Controller of Stamps, Ground Floor,
                            Opposite Vidhan Bhavan (Council Hall), New Administrative Building, Pune
                            411001, Maharashtra , India.</p>
                        <p>Phone: 8888007777</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //asidebar -->