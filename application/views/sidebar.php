<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- ==== Document Title ==== -->
    <title>Grievance Redressal System IGR.</title>
    <!-- ==== Document Meta ==== -->
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- ==== Bootstrap Framework ==== -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
    <!-- font awesome icons -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css">
    <!-- owl carousel  -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/owl.carousel.min.css">
    <!-- marquee -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/marquee.css">
    <!-- ==== Main Stylesheet ==== -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/styles.css">
</head>

<body>
    <div class="top-strip">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <a class="data-Now" href="#">17 February 2020</a>
                </div>
                <div class="col-sm-9">
                    <ul class="quick-nav">
                        <li> <a href="#">Skip to main content |</a> </li>
                        <li> <a href="#">Login |</a></li>
                        <li> <a href="#">Register |</a></li>
                        <li> <a href="#">Sitemap |</a></li>
                        <li> <a href="#" class="eng-lang">English </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- center logo -->
    <div class="centerLogo text-center d-flex justify-content-center">
        <div class="logo-place align-self-center">
            <span class="logo-left">
                <img src="<?php echo base_url();?>images/emblemofindia.png" class="img-fluid">
            </span>
        </div>
        <div class="logo-label align-self-center">
            <h1>Department of Registration &amp; Stamps</h1>
            <h3>Government of Maharashtra</h3>
        </div>
        <div class="logo-place align-self-center">
            <span class="logo-right">
                <img src="<?php echo base_url();?>images/IGRlogo.png" class="img-fluid">
            </span>
        </div>
    </div>
    <!-- //center logo -->

    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <img src="<?php echo base_url();?>images/logo.jpg" alt="sarthi">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown ">
                        <a class="nav-link" href="/">
                            home<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#">Complaint System</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#">IGR Regionwise Login</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#">Help</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- //navigation -->

    <!-- asidebar -->
    <section class="asidebar-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="aside">
                        <ul class="aside-list">
                            <li><a href="#" class="active-list"> Lorem ipsum dolor sit amet</a></li>
                            <li><a href="#"> Lorem ipsum dolor sit amet</a></li>
                            <li><a href="#"> Lorem ipsum dolor sit amet</a></li>
                            <li><a href="#"> Lorem ipsum dolor sit amet</a></li>
                            <li><a href="#"> Lorem ipsum dolor sit amet</a></li>
                            <li><a href="#"> Lorem ipsum dolor sit amet</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 mt-3">
                    <ul class="instruction-points">
                        <li>Please write your complaint in brief and bullet points.</li>
                        <li>Please mention your mobile number which will help in communication through sms.</li>
                        <li>Specify the details of complaint,if already lodged.</li>
                        <li>Please avoid lodging complaints about pending/awaiting court matters.</li>
                        <li>Do specify your category of complaint .</li>
                        <li>Remember your complaint token number for further communication.</li>
                        <li>Use UNICODE font to lodge complaint in Marathi language</li>
                        <li>Complainer will be responsible for wrong complaints.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- //asidebar -->

    <!-- footer -->
    <footer>
        <!-- Footer Area Start -->
        <section class="footer-Content">

        </section>
        <!-- Footer area End -->

        <!-- Copyright Start  -->
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="site-info float-left">
                            <p>Designed & Developed By lorem iposum</p>
                        </div>
                        <div class="float-right">
                            <ul class="nav nav-inline">
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fa fa-envelope-o"></i>
                                        igrmaharastra.gov.in</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fa fa-phone"></i> 8888007777</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyright End -->

    </footer>
    <!-- //footer end here -->

    <!-- Script Files -->
    <!-- ==== jQuery Library ==== -->
    <script src="<?php echo base_url();?>js/jquery-3.2.1.min.js"></script>
    <!-- ==== Bootstrap Framework ==== -->
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <!-- === owl carousel === -->
    <script src="<?php echo base_url();?>js/owl.carousel.min.js"></script>
    <!-- marquee -->
    <script src="<?php echo base_url();?>js/jquery.modern-ticker.min.js"></script>
    <!-- <script src="<?php echo base_url();?>js/marquee.js"></script> -->
    <!-- === custom script === -->
    <script src="<?php echo base_url();?>js/custom.js"></script>

    <script>
        $(document).ready(function () {
            $(".ticker1").modernTicker(
                {
                    effect: "scroll",
                    scrollType: "continuous",
                    scrollStart: "inside",
                    scrollInterval: 20,
                    transitionTime: 500,
                    autoplay: true
                }
            );
        })
    </script>
</body>

</html>