<!-- footer -->
<div class="clr"></div>
<footer id="main-navigation" class="clearfix" style="min-height:30px;">
    <div class="container">
    </div>
</footer>
<!-- / footer --> 
 
<!-- Bootstrap --> <!-- app --> 

<script>
$( "#selectcat" ).change(function() {
 if($('#selectcat').val()=='Business Client')
  {
	$('#fdc').hide();
    $('#company').show();
  }
   if($('#selectcat').val()=='Share Holder')
  {
	$('#fdc').hide();
	$('#company').hide();
  }
   if($('#selectcat').val()=='Investor')
  {
	$('#fdc').show();
	$('#company').hide();
  }
   if($('#selectcat').val()=='Bond Holder')
  {
	$('#fdc').show();
	$('#company').hide();
  }
   if($('#selectcat').val()=='Ex Employee')
  {
	$('#fdc').hide();
	$('#company').hide();
  }
   if($('#selectcat').val()=='Other')
  {
	$('#fdc').hide();
	$('#company').hide();
  }
});

$('#btn_submit').bind('click',function(){
 var flag=1;
 var msg=$('#msgcomplain').val(); 
 $('#error_account').html('');
 $('#msg_error').html('');
 
 if($('#selectcat').val()=='')
  {
		$('#fdc_id').val('');
		$('#error_account').html('Please select category');
		flag=0;
  }
 if($('#selectcat').val()=='Business Client')
  {
	var company=$('#company_val_id').val();
	$('#comp_id').html('');
	if(company=='')
	{
		$('#fdc_id').val('');
		$('#comp_id').html('Please enter company name');
		
		flag=0;
	}
  }
   if($('#selectcat').val()=='Investor')
  {
	  $('#company_val_id').val('');
	var company=$('#fdc_id').val();
	$('#error_fdc_id').html('');
	if(company=='')
	{
		$('#error_fdc_id').html('Please enter Folio No. / DP ID / Client ID');
		flag=0;
	}
  }
   if($('#selectcat').val()=='Bond Holder')
  {
	var company=$('#fdc_id').val();
	$('#error_fdc_id').html('');
	if(company=='')
	{
		$('#error_fdc_id').html('Please enter Folio No. / DP ID / Client ID');
		flag=0;
	}
	
  }
   if($('#selectcat').val()=='Share Holder')
  {
	$('#company_val_id').val('');
	$('#fdc_id').val('');
  }
  
   if($('#selectcat').val()=='Ex Employee')
  {
	$('#company_val_id').val('');
	$('#fdc_id').val('');
  }
   if($('#selectcat').val()=='Other')
  {
	$('#company_val_id').val('');
	$('#fdc_id').val('');
  }
  
	if(msg=='')
	{
		$('#msg_error').html('Please enter your Message');
		flag=0;
	}
	else if(msg.length > 1000)
	{
			$('#msg_error').html('The Message field can not exceed 1000 characters in length');
		flag=0;
	}
	

	if(flag==1)
	{
		return true;
	}else
	{
		return false;
	}
	});
</script>
</body>
<!-- Mirrored from flatfull.com/themes/first/signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Jun 2015 05:30:10 GMT -->
</html>