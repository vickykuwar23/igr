<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <script src="<?php echo base_url();?>js/jquery.min.js"></script>
      <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="<?php echo base_url();?>js/datatables/bootstrap.min.css">
      <script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
      <script src="<?php echo base_url();?>js/dataTables.bootstrap.min.js"></script>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>js/dataTables.bootstrap.min.css" >
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Grievance Admin Panel</title>
   </head>
   <body>
    
      <div class="col-md-1 addadmin">
         <a href="<?php echo base_url();?>callcenterpanel/register_new_user/" class="btn btn-success no-shadow">New Registration</a>
      </div>
      <table class="table table-striped table-bordered dt-responsive nowrap" id="table">
         <thead>
            <tr>
               <th class="text-center bluehd">Sr. No.</th>
               <th class="text-center bluehd">Name</th>
               <th class="text-center bluehd">Mobile</th>
               <th class="text-center bluehd">Email</th>
               <th class="text-center bluehd">Action</th>
                
            </tr>
         </thead>
         <tbody>
            <?php if(count($users) > 0){
               foreach($users as $key=>$user){ ?>
            <tr>
               <td><?=$key ?></td>
               <td><?=$user['user_name'] ?></td>
               <td><?=$user['user_mobile'] ?></td>
               <td><?=$user['user_email'] ?></td>
               <td><a href='<?php echo base_url('callcenterpanel/user_details/').$user["user_id"] ?>' class="btn btn-primary">Show grievance</a>
                <a href='<?php echo base_url('callcenterpanel/add_grevience/').$user["user_id"] ?>' class="btn btn-primary">Add grievance</a>
               </td>
            </tr>
            <?php }} ?>
         </tbody>
      </table>
   </body>
   <script>
      $(document).ready(function(){
          $('#table').DataTable({
          dom: 'Bfrtip', //to show the buttons on the screen
              buttons: [    // which buttons should display
              ]
        });
      });
   </script>
</html>

