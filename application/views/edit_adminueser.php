<div style="text-align:center;">

<?php if ($this->session->flashdata('success_message') != '') { ?>

     <div class="alert alert-success" style="font-size:17px;">

        <button data-dismiss="alert" class="close" type="button" id="edit_admin_close">×</button>

         <?php echo $this->session->flashdata('success_message') ?></br>

          <div><a href="<?php echo base_url();?>index.php/subadmin/view/" style="text-decoration:none; text-align:">Go Back</a></div>

      </div>

    <? } ?>

  </div>

  

   <?php 

if ($this->session->flashdata('success_message') != '') 

{ $display='style="display:none;"';

 }

 else

 {

$display='style="display:block;"';

}?>

  <div id="editadmin_id" <?php echo $display;?>>

<section id="content" class="min380">

  <section class="main padder">

    <div class="row">

      <div class="col-sm-6 col-xs-offset-3 m-t-large">

        <section class="panel m-t-large grdbg">

          <header class="panel-heading text-center"><i class="fa fa-edit"></i>Edit Admin</header>

          <div class="panel-body">

            <form class="form-horizontal" method="post" data-validate="parsley">

              <div class="form-group">

                <label class="col-lg-4 control-label">Category <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please Select the category"></i>

                <div class="col-lg-4">

                  <select name="category" id="selectcat" class="form-control w80">

                    <option value="">Select</option>

					 <?php if(count($department) > 0)

                     {

                         foreach($department as $dept)

                         {?>

                                 <option value="<?php echo $dept['id'];?>" <?php if($existinguser[0]['type']==$dept['id']){echo 'selected="selected"';}?>><?php echo ucfirst($dept['type']);?></option>

              <?php }

                     }?>

                  </select>

                 <div id="error_account" style="color:#F00">

				 <?php 

				 if(form_error('category')!='')

				 {

						$department_name=$this->master_model->getRecords('departments',array('id'=>set_value('category')),'type');

						echo $department_name[0]['type'].' already exist';

				 }?>

				 </div>

                </div>

              </div>

              <div class="form-group">

                <label class="col-lg-4 control-label">Name <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please Enter Username"></i>

                <div class="col-lg-7">

                  <input type="text" name="name" data-required="true" class="form-control" value="<?php echo $existinguser[0]['adminuser'];?>" id="name"> 

                     <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('name');?></div>

                </div>

              </div>

              <div class="form-group">

                <label class="col-lg-4 control-label">Email <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please Enter email Address"></i>

                <div class="col-lg-7">

                  <input type="text" name="email" class="form-control" data-required="true" data-type="email" value="<?php echo $existinguser[0]['email'];?>" id="email">

                   <div class="m-t m-t-mini" style="color:#F00"><?php echo form_error('email');?></div>

                </div>

              </div>

              <div class="form-group">

                <label class="col-lg-4 control-label">Second officer name <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please Enter Second officer name"></i>

                <div class="col-lg-7">

                  <input type="text" name="namecc" data-required="true" class="form-control" value="<?php echo $existinguser[0]['namecc'];?>" id="namecc"> 

                     <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('namecc');?></div>

                </div>

              </div>

              <div class="form-group">

                <label class="col-lg-4 control-label">Second officer email <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please Enter Second officer email Address"></i>

                <div class="col-lg-7">

                  <input type="text" name="emailcc" class="form-control" data-required="true" data-type="email" value="<?php echo $existinguser[0]['emailcc'];?>" id="emailcc">

                   <div class="m-t m-t-mini" style="color:#F00"><?php echo form_error('emailcc');?></div>

                </div>

              </div>

              <div class="form-group">

              <label class="col-lg-4 control-label"><input type="checkbox" name="enpass" id="checkpass" style="margin-right:7px; vertical-align:middle; margin-bottom:7px;">Password <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please Enter password"></i>

                <div class="col-lg-7">

                  <input type="password" name="password" data-required="true" class="form-control" id="passreset" disabled="disabled"> 

                  Please select checkbox if you want to edit password.

                     <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('password');?></div>

                </div>

              </div>

              <div class="form-group">

                <div class="col-lg-8 col-lg-offset-4">

                  <button type="submit" id="btn_editid" class="btn btn-primary no-shadow" name="btn_edit">Submit</button>

                  <a href="<?php echo base_url();?>index.php/subadmin/view/" class="btn btn-white">Cancel</a>

                </div>

              </div>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
            </form>

          </div>

        </section>

      </div>

    </div>

  </section>

</section>

</div>

<script>

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();



	$('#edit_admin_close').on('click',function(){

		$("#editadmin_id").css("display", "block");

		})

	



$('#checkpass').bind('change', function () {

   if ($(this).is(':checked'))

    {

     $("#passreset").removeAttr("disabled"); 

	 var checkedValue = $('#passreset').val().length;

	   if(checkedValue==''){ $("#btn_editid").attr("disabled", "disabled"); }

	}else{

    $("#passreset").attr("disabled", "disabled"); 

    $("#btn_editid").removeAttr("disabled"); 

   }

   

});

 $('#passreset').keyup(function() {

	  var checkedValue = $('#passreset').val().length;

	  		    if(checkedValue==0){ $("#btn_editid").attr("disabled", "disabled"); }

			else if(checkedValue>0){$("#btn_editid").removeAttr("disabled");  }

       });

	   });

</script>