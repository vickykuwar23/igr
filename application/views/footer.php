<!-- footer -->
    <footer>
        <!-- Footer Area Start -->
        <section class="footer-Content">
        </section>
        <!-- Footer area End -->

        <!-- Copyright Start  -->
   <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="footer-links-wrpr">
                            <li><a href="<?php echo base_url('home/copyright'); ?>" class="footer-links">Copyright</a></li>
                            <li><a href="<?php echo base_url('home/disclaimer'); ?>" class="footer-links">Disclaimer</a></li>
                            <li><a href="<?php echo base_url('home/hyperlinkpolicy'); ?>" class="footer-links">Hyper Link Policy</a></li>
                            <li><a href="<?php echo base_url('home/privacypolicy'); ?>" class="footer-links">Privacy Policy</a></li>
                            <li><a href="<?php echo base_url('home/termscondition'); ?>" class="footer-links">Terms and Condition</a></li>
                            <li><a href="<?php echo base_url('home/contactus'); ?>" class="footer-links">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>Website Designed, Developed, Hosted & maintained by <a href="https://esds.co.in/"
                                target="_blank" class="cmny-name">ESDS Software
                                Solutions Pvt. Ltd</a> </p>
                      <p>Content provided by Department of Registration & Stamps, Government of Maharashtra.</p>                
                      <p>Copyright © <?php echo date('Y'); ?> Department of Registration and Stamps, Government of Maharashtra</p>  	
                    </div>
                </div>
                <div class="row"> 
                  <div class="col-sm-12">
                      <ul class="footer-icons"> 
                        <li><img src="<?php echo base_url();?>img/ico-xhtml.png" class="img-fluid"></li>
                        <li><img src="<?php echo base_url();?>img/ico-css.png" class="img-fluid"></li>
                      </ul>
                  </div>
                </div>
            </div>
        </div>
        <!-- Copyright End -->
    </footer>
    <!-- //footer end here -->
<!-- Bootstrap --> <!-- app --> 

<!-- Script Files -->
    <!-- ==== jQuery Library ==== -->
    <!-- ==== Bootstrap Framework ==== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>


<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js "></script>


    <script src="<?php echo base_url();?>js/owl.carousel.min.js"></script>
    <!-- marquee -->
    <script src="<?php echo base_url();?>js/jquery.modern-ticker.min.js"></script>
    <!-- === custom script === -->
    <script src="<?php echo base_url();?>js/custom.js"></script>
    <script src="<?php echo base_url();?>js/jquery.modern-ticker.min.js"></script>
  
  <script src="<?php echo base_url();?>js/marquee.js"></script>
  
<script>
    $(document).ready(function(){
        $('#table').DataTable({
        dom: 'Bfrtip', //to show the buttons on the screen
            buttons: [    // which buttons should display
            ]
      });
    });
</script>

    <script>

        $(function () {
            $('.simple-marquee-container').SimpleMarquee({
              duration:50000, 
            });
        });

        // $(function () {
        //     $(".ticker1").modernTicker(
        //         {
        //             effect: "scroll",
        //             scrollType: "continuous",
        //             scrollStart: "inside",
        //             scrollInterval: 20,
        //             transitionTime: 500,
        //             autoplay: true
        //         }
        //     );
        // });

    </script>
<script>
$( "#selectcat" ).change(function() {
 if($('#selectcat').val()=='Business Client')
  {
	$('#fdc').hide();
    $('#company').show();
  }
   if($('#selectcat').val()=='Share Holder')
  {
	$('#fdc').hide();
	$('#company').hide();
  }
   if($('#selectcat').val()=='Investor')
  {
	$('#fdc').show();
	$('#company').hide();
  }
   if($('#selectcat').val()=='Bond Holder')
  {
	$('#fdc').show();
	$('#company').hide();
  }
   if($('#selectcat').val()=='Ex Employee')
  {
	$('#fdc').hide();
	$('#company').hide();
  }
   if($('#selectcat').val()=='Other')
  {
	$('#fdc').hide();
	$('#company').hide();
  }
});

$('#btn_submit').bind('click',function(){
 var flag=1;
 var msg=$('#msgcomplain').val(); 
 $('#error_account').html('');
 $('#msg_error').html('');
 
 if($('#selectcat').val()=='')
  {
		$('#fdc_id').val('');
		$('#error_account').html('Please select category');
		flag=0;
  }
 if($('#selectcat').val()=='Business Client')
  {
	var company=$('#company_val_id').val();
	$('#comp_id').html('');
	if(company=='')
	{
		$('#fdc_id').val('');
		$('#comp_id').html('Please enter company name');
		
		flag=0;
	}
  }
   if($('#selectcat').val()=='Investor')
  {
	  $('#company_val_id').val('');
	var company=$('#fdc_id').val();
	$('#error_fdc_id').html('');
	if(company=='')
	{
		$('#error_fdc_id').html('Please enter Folio No. / DP ID / Client ID');
		flag=0;
	}
  }
   if($('#selectcat').val()=='Bond Holder')
  {
	var company=$('#fdc_id').val();
	$('#error_fdc_id').html('');
	if(company=='')
	{
		$('#error_fdc_id').html('Please enter Folio No. / DP ID / Client ID');
		flag=0;
	}
	
  }
   if($('#selectcat').val()=='Share Holder')
  {
	$('#company_val_id').val('');
	$('#fdc_id').val('');
  }
  
   if($('#selectcat').val()=='Ex Employee')
  {
	$('#company_val_id').val('');
	$('#fdc_id').val('');
  }
   if($('#selectcat').val()=='Other')
  {
	$('#company_val_id').val('');
	$('#fdc_id').val('');
  }
  
	if(msg=='')
	{
		$('#msg_error').html('Please enter your Message');
		flag=0;
	}
	else if(msg.length > 1000)
	{
			$('#msg_error').html('The Message field can not exceed 1000 characters in length');
		flag=0;
	}
	

	if(flag==1)
	{
		return true;
	}else
	{
		return false;
	}
	});
</script>
</body>
<!-- Mirrored from flatfull.com/themes/first/signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Jun 2015 05:30:10 GMT -->
</html>