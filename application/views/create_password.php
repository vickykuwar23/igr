<style>

form ul.helper-text {
  display: block;
  margin-top: 6px;
  font-size: 12px;
  line-height: 22px;
  color: #808080;
}
form ul.helper-text li.valid {
  color: #1fd34a;
}
form.valid input {
  border: 2px solid #1fd34a;
}
.text-red{ color:#F00}
.text-green{ color:#090}
</style>
<div style="text-align:center;">
 <?php if ($this->session->flashdata('success_message') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
         <button data-dismiss="alert" class="close" type="button" id="forget_close">×</button>
         <?php echo $this->session->flashdata('success_message') ?>
         </br>
           <div><a href="<?php echo base_url();?>index.php/userlogin/" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
      <div class="min380"></div>
    <?php } ?>
    <?php if ($this->session->flashdata('failsend') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
         <button data-dismiss="alert" class="close" type="button" id="forget_close">×</button>
         <?php echo $this->session->flashdata('failsend') ?>
      </div>
    <?php } ?>
    
    <?php if ($this->session->flashdata('wrongemail') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
         <button data-dismiss="alert" class="close" type="button" id="forget_close">×</button>
         <?php echo $this->session->flashdata('wrongemail') ?>
      </div>
    <?php } ?>
    </div>
      <?php if ($this->session->flashdata('forget_galat_jawab') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button" id="newpass_id">×</button>
         <?php echo $this->session->flashdata('forget_galat_jawab') ?>
           </br>
           <div><a href="<?php echo base_url();?>index.php/userlogin/" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
    <?php } ?></div>
    <?php 
	$display= "";
/*if ($this->session->flashdata('success_message') != '') 
{ $display='style="display:none;"';
 }
 else
 {
$display='style="display:block;"';
}*/?>
<div <?php echo $display;?> id="set_newpass_id">   
 <section id="content" class="login-wrapper">
  <section class="main padder">
    <div class="d-flex justify-content-center h-100">
      <div class="user_card">
      <div class="d-flex justify-content-center form_container">
            <form class="form-horizontal w-100" method="post" data-validate="parsley" autocomplete="off">
              <div class="form-heading">
                <h3>Set Password</h3>
              </div>
              <div class="form-grp-filed">
           <!--   <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label col-lg-offset-0 align-self-center" >Phone number<span style="color:red">*</span></label>
                    <div class="col-md-8">
                      <input type="text" name="phonenumber" data-required="true" class="form-control" autofocus  required> 
                        <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('phonenumber');?></div>
                    </div>
                  </div>
              </div> -->
               <div class="form-group">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                  <div class="row">
                    <label class="col-md-4 control-label align-self-center" >Password<span style="color:red">*</span></label>
                    <div class="col-md-8">
                       <input type="password" id="password1" name="pass" data-required="true" class="form-control password" autofocus > 
                        <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('pass');?></div>
                    </div>
                  </div>
              </div>

               <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label align-self-center" >Confirm Password <span style="color:red">*</span></label>
                    <div class="col-md-8">
                        <input type="password" id="password2" name="con_password" class="form-control"  />
                        <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('con_password');?></div>
                    </div>
                  </div>
              </div>
            

               <!-- <div class="form-group">
                    <label class="control-label loginlable d-block">Enter Captcha Code</label>
                    <div class="row">
                    <label class="col-md-4 pr-0 control-label col-lg-offset-0 align-self-center cptcha_img" ><?php echo $image; ?></label>
                    <div class="col-md-8 pl-0">
                       <input type="text" name="code" placeholder="" class="form-control"  id="code">  
                        <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('code');?></div>
                    </div>
                    </div>
                   
                </div> -->

              <div class="form-group row captcha">
                <label class="col-lg-4 control-label text-left cptcha_img">
                  <span id="captImg"><?php echo $image; ?></span>
                    
                  </label>
                <div class="col-lg-6">
                  <input type="text" name="code" placeholder="" class="form-control"  id="code"> 
                    <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('code');?></div>
                   <p class="text-left blue">Please enter the text you see in the image below into the text box provided.</p>
                </div>
                <div class="col-lg-2">
                  <a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh"></i></a> 
                </div>
              </div>

            
             
              <div class="form-group">
                
                  <span id="error_pass" style="color:red;font-size: 12px" ></span>
               
              </div>
              <div class="form-group">
                <div class="">
                     
                </div>
                </div>

                <div class="mb-3">
                                <div class="col-lg-12 mx-auto d-flex justify-content-center">
                                  <ul class="frm-btn-grp">
                                    <li> <button type="submit" class="btn btn_submit " name="btn_set" id="submitpass">Submit</button> </li>
                                    <li><button type="submit" class="btn_cancel btn btn-white">Cancel</button>  </li>
                                   </ul>
                                </div>
                            </div>


              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  
                  
                </div>
              </div>
            </div>
            </form>
          </div>
      </div>
    </div>
</section>
</div>

<script>
  $(document).ready(function(){
    $("#submitpass").on('click',function(){
      // checkPasswordMatch();
      validate= true;
      $("#password1").css('border-color','#495057');
      $("#password2").css('border-color','#495057');
      $("#error_pass").html('')
      if ($("#password1").val() == "") {
        $("#password1").css('border-color','red');
        validate= false;
      }
       if ($("#password2").val() == "") {
        $("#password2").css('border-color','red');
        validate= false;
      }

      var pass = $("#password1").val();
      var confirmPassword = $("#password2").val();

      var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/; 
        if( ! regex.test(pass) ) {
          $("#error_pass").html('Password must be minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.')
            $("#password1").css('border-color','red');
            validate= false;
       }

        if(pass !='' && confirmPassword !=''){ 
        if (pass != confirmPassword)
          {
              $("#password2").css('border-color','red');
              $("#error_pass").html('Passwords does not match!');
              //$('#submitpass').prop("disabled", true);
              validate= false;
          }
        }
if ( validate== false){
  return false;
}
  else{
        if($('.password1').val() != "" && $("#password2").val() != ""){
          var pswd = $("#password1").val();
          var salt = pswd.length;
          var salt1 = sha1(salt);
          var password = sha256(sha1(salt1 + pswd));
          $("#password1").val(password);

          var pswd_2 = $("#password2").val();
          var salt_2 = pswd_2.length;
          var salt2 = sha1(salt_2);
          var password2 = sha256(sha1(salt2 + pswd_2));
          $("#password2").val(password2);


          // var inputhashed = sha1($("#inputPassword").val());
          // var salt = sha1(inputhashed + '<?php echo $this->config->item('salt'); ?>');
          return true;
        }else{
          return false;
        }
        }
    })

  
  })
</script>


<script>
         $(document).ready(function(){
             $('.refreshCaptcha').on('click', function(){
                 $.get('<?php echo base_url().'create/refresh'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
         });
</script>
