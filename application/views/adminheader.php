<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Grievance Redressal System IGR.</title>

<meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="shortcut icon" href="<?php echo base_url();?>img/icon.png" type="image/vnd.microsoft.icon" />

<link rel="stylesheet" href="<?php echo base_url();?>css/font.css">

<link rel="stylesheet" href="<?php echo base_url();?>css/app.v2.css" type="text/css" />

<link href="<?php echo base_url();?>css/custom.css" rel="stylesheet">

<link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">

<link href="<?php echo base_url();?>css/resizer.css" rel="stylesheet">

<!--[if lt IE 9]> <script src="js/ie/respond.min.js"></script> <script src="js/ie/html5.js"></script> <![endif]-->

<!-- basic libraries required for datatable Funcyions include : search,pagination,no of records -->

<script src="<?php echo base_url();?>js/jquery.min.js"></script>
<script src="<?php echo base_url();?>js/encrypt.js"></script>
<script src="<?php echo base_url();?>js/validation.js"></script>


</head>

<body>

<!-- #header-top -->

<div id="header-top" class="clearfix">

  <div class="container">

    <!-- #header-top-inside -->

    <div id="header-top-inside" class="clearfix">

      <div class="row">

        <div class="col-md-9">

        <!-- #header-top-left -->

        <div id="header-top-left" class="clearfix">

          <div class="row">

            <div class="pull-left nopadright m-r-t-0 col-md-12">

              <div class="content">

                <div class="row">

                  <div class="col-md-12">

                    <ul class="topleftmenu"><li><a href="#main-content">Skip to main content</a>|</li>

                       <li><a href="http://grievanceigr.maharashtra.gov.in">Home</a>|</li>

                      <li><a href="http://grievanceigr.maharashtra.gov.in/griev/">IGR Regionwise Login</a>|</li>


                      <li><a href="http://grievanceigr.maharashtra.gov.in/webcontent/IGR_CitizenUserManual.pdf" target="_blank">Help</a>|</li>

                      <!--<ul class="resizer pull-left nopad">

                        <li><a class="sm" href="#">A-</a>|</li>

                        <li><a class="md" href="#">A</a>|</li>

                        <li><a class="lg" href="#">A+</a>|</li>

                      </ul>-->

                    </ul>

                  </div><!--/.col-md-12--> 

                </div><!--/.row-->  

              </div><!--/.content-->

            </div><!--/.nopadright-->

          </div><!--/.row -->

        </div><!-- #header-top-left -->

        </div><!--/.col-md-8 -->

        <div class="col-md-3">

    	

<?php if($this->session->userdata('adminid')!='')
		 	{?>
				<a href="<?php echo base_url();?>index.php/adminlogin/change_password" class="logoutbtn">  Change Password</a>


    	     	  <a href="<?php echo base_url();?>index.php/adminlogin/adminlogout" class="logoutbtn"><i class="fa fa-sign-out"></i> Logout</a>
                  
                  

<?php }?>

               </div>

      </div><!--/.row -->

    </div><!-- #header-top-inside -->

  </div>

</div>

<!-- EOF: #header-top --> 

<!-- header -->

<header id="header" role="banner" class="clearfix">

  <div class="container">

    <!-- #header-inside -->

    <div id="header-inside" class="clearfix">

      <div class="row">

       <div class="col-md-12">

  <div class="col-md-3">

          <div id="logo">

            <a rel="home" title="Home" href='<?php echo base_url() ?>'> 

            <img alt="Home" height="85" src="<?php echo base_url()?>img/igr_logo.jpg"> </a>

          </div>

  </div>

  <div class="col-md-3 pull-right m-t-5" id="main-content">
  <img alt="" src="<?php echo base_url()?>img/grievance_img.png">
  </div>

        <!--<div class="content">

        <div class="clock-clocks"><div class="clock-clock-wrapper clock-clock-wrapper-1"><div class="clock-clock clock-clock-1"><span class="clock-date clock-date-1">10:35:16 Tuesday 29 Sep 2015</span></div></div></div>  </div>-->

        </div>



    </div>

    <!-- EOF: #header-inside -->

  </div>

</header>

<!-- EOF: #header -->

<!-- #main-navigation --> 

<div id="main-navigation" class="clearfix" style="min-height:30px;">

<div class="container">

<!-- #main-navigation-inside -->

  <div id="main-navigation-inside" class="clearfix">

    <div class="row">

      <div class="col-md-12">

        <nav role="navigation">

          <div class="content">

          <p class="grievancehd">GRIEVANCE REDRESSAL PORTAL</p>

          </div>

        </nav>

      </div>

    </div>

    </div>

    <!-- EOF: #main-navigation-inside -->

    </div>

  </div>

<!-- EOF: #main-navigation -->