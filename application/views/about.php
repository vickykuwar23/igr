

    <!-- carousel -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="banner-bg" 

                style="background-image: url('<?php echo base_url() ?>img/fix-bg.jpg');"></div>
            </div>
        </div>
    </div>
    <!-- end carousel -->

    <!-- about redresel -->
    <section class="complain-wrapper" id="complain-wrapper">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-12">
                    <div class="title-heading">
                        <h1 class="heading"> About Complaint Redressal System</h1>
                    </div>
					<p class="mt-4">Complaint Redressal System is a Suite of eServices offered by the Department of Registration & Stamps Government of Maharashtra to its Citizens.</p>
                </div>
            </div>
			
			<div class="row mb-5 align-items-center pb-2">
				<div class="col-md-5 text-center">

					<img src="<?php echo base_url() ?>img/vission.png" class="img-fluid" />
				</div>
                <div class="col-md-7">
					<div class="row mx-0">
						<div class="title-heading">
							<h1 class="heading"> The Mission</h1>
						</div>
						<p class="mt-4">To bring to every citizen the benefits of advances in technology by identifying, harnessing and employing state-of-the-art comlaint Redressal System...</p>
					</div>
                </div>
            </div>
			</div>
            <!-- complaint-list -->
			 <!--<div class="bg_light">
			 <div class="container">
			 <div class="row mt-5">
                <div class="col-md-12">
                    <div class="title-heading mt-4">
                        <h1 class="heading"> Citizens Services</h1>
                    </div>
					<p class="mt-4">The following Citizens Services are part of the Complaint Redressal System.</p>
                </div>
            </div>
			
            <div class="row citizen_services">
                <div class="col-md-4">
                   <h2>Document Registration</h2>
					   <ul>
						<li><a href="javascript:void(0);">e-registration Process</a></li>
						<li><a href="javascript:void(0);">Online e-Payment</a></li>
						<li><a href="javascript:void(0);">Online e-Registration</a></li>
						<li><a href="javascript:void(0);">Online PDE for Registration</a></li>
						<li><a href="javascript:void(0);">Online e-Step In</a></li>
					   </ul>
                </div>
				<div class="col-md-4">
                   <h2>Filing of Notices</h2>
					   <ul>
						<li><a href="javascript:void(0);">Filing Process</a></li>
						<li><a href="javascript:void(0);">Online e-Filing</a></li>
						<li><a href="javascript:void(0);">Online e-Payment</a></li>
						<li><a href="javascript:void(0);">Online e-Step In</a></li>
						<li><a href="javascript:void(0);">Online PDE for Filing</a></li>
					   </ul>
                </div>
				<div class="col-md-4">
                   <h2>Marriage Registration</h2>
					   <ul>
						<li><a href="javascript:void(0);">Marriage Regisrtation Process</a></li>
						<li><a href="javascript:void(0);">Online e-Payment</a></li>
						<li><a href="javascript:void(0);">Online e-Step In</a></li>
						<li><a href="javascript:void(0);">Online e-Filing</a></li>
					   </ul>
                </div>
            </div>
			<div class="row citizen_services">
                <div class="col-md-4">
                   <h2>Copy and Search</h2>
					   <ul>
						<li><a href="javascript:void(0);">Copy & Search Process</a></li>
						<li><a href="javascript:void(0);">Online e-Search</a></li>
						<li><a href="javascript:void(0);">Online e-Payment</a></li>
					   </ul>
                </div>
				<div class="col-md-4">
                   <h2>Deemed Conveyance</h2>
					   <ul>
						<li><a href="javascript:void(0);">Valuation Process</a></li>
						<li><a href="javascript:void(0);">Online e-Payment</a></li>
						<li><a href="javascript:void(0);">Online e-Step In</a></li>
					   </ul>
                </div>
				<div class="col-md-4">
                   <h2>Stamp Duty Refund</h2>
					   <ul>
						<li><a href="javascript:void(0);">Stamp Duty Refund Process</a></li>
						<li><a href="javascript:void(0);">Online e-Filing</a></li>
						<li><a href="javascript:void(0);">Online e-Filing</a></li>
					   </ul>
                </div>
            </div>
			<div class="row citizen_services border-0">
                <div class="col-md-4">
                   <h2>Valuation of property</h2>
					   <ul>
						<li><a href="javascript:void(0);">Valuation Process</a></li>
						<li><a href="javascript:void(0);">Online e-Payment</a></li>
						<li><a href="javascript:void(0);">Online e-Step In</a></li>
					   </ul>
                </div>
				<div class="col-md-4">
                   <h2>Stamp Duty Collection</h2>
					   <ul>
						<li><a href="javascript:void(0);">Stamp Duty Collection Process</a></li>
						<li><a href="javascript:void(0);">Online e-Payment</a></li>
					   </ul>
                </div>
				<div class="col-md-4">
                   <h2>Registration of Will</h2>
					   <ul>
						<li><a href="javascript:void(0);">Will Registration Process</a></li>
						<li><a href="javascript:void(0);">Online e-Payment</a></li>
						<li><a href="javascript:void(0);">Online e-Step In</a></li>
					   </ul>
                </div>
            </div>
			 </div>
        </div>-->
            <!-- //complaint-list -->
			
			<div class="container">
			<div class="row mt-5">
                <div class="col-lg-8">

					<!-- <img src="<?php echo base_url() ?>images/treeStructure.png" class="img-fluid" /> -->

					<img src="<?php echo base_url() ?>img/treeStructure.png" class="img-fluid" />

				</div>
				<div class="col-lg-4">
					<div class="treeStructure-insturctions">
                        <ul>
                            <li> <span>IGR : </span> </span><span class="val-txt">Inspector General of Registration</span></li>
                            <li> <span>ACS : </span> <span class="val-txt">Additional Controller of Stamps</span></li>
                            <li><span>JDTP : </span> <span class="val-txt">Joint Director of Town Planning</span></li>
                            <li> <span>DIG : </span><span class="val-txt">Deputy Inspector General of Registration</span></li>
                            <li> <span>COS : </span><span class="val-txt">Collector of Stamps</span></li>
                            <li> <span>JDR : </span><span class="val-txt">Joint District Registrar</span></li>
                            <li> <span>DDTP : </span><span class="val-txt">Deputy Director of Town Planning</span></li>
                            <li> <span>ADTP : </span><span class="val-txt"> Assistant Director of Town Planning</span></li>
                            <li> <span>SRO : </span><span class="val-txt">Sub registrar's Office</span></li>
                        </ul>
                    </div>
				</div>
			</div>
			</div>
    
    
    </section>
    <!-- //about redresel -->

