<style>
.er-success{color:#00FF00;}
.er-error{color:red;}
</style>
<div style="text-align:center;">
  <?php if($error_msg != "") { ?>
        <div class="alert alert-error">
        <button data-dismiss="alert" class="close" type="button" id="register_close">×</button>
        <strong>Warning!</strong> <?php echo $error_msg ?>
        </div>
  <?php } ?>  
 
<?php if ($this->session->flashdata('success_message') != '') { ?>
     <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button" id="register_close">×</button>
         <?php echo $this->session->flashdata('success_message') ;
		      $this->session->sess_destroy();?>
         </br>
           <div><a href="<?php echo base_url();?>index.php/userlogin/" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
      <div class="min380"></div>
    <?php } ?></div>
    
    <?php if ($this->session->flashdata('success_message_sms') != '') { 
	?>
     <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button" id="register_close">×</button>
         <?php echo $this->session->flashdata('success_message_sms');
		 $this->session->sess_destroy(); ?>
         </br>
           <div><a href="<?php echo base_url();?>index.php/userlogin/" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
      <div class="min380"></div>
    <?php } ?></div>
 <?php 
if ($this->session->flashdata('success_message') != '') 
{
	 $display='style="display:none;"';
 }
 else
 {
$display='style="display:block;"';
}?>

<div <?php echo $display;?> id="register_id">   
<section id="content"  class="login-wrapper">
    <div class="d-flex justify-content-center h-100">
      <div class="user_card">
          <div class="d-flex justify-content-center form_container">
            <form class="w-100 form-horizontal" method="post" data-validate="parsley" autocomplete="off">
                <div class="form-heading">
                  <h3>User Registration Form</h3>
                </div>
            <div class="form-grp-filed">
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
              <div class="form-group row">
                <label class="col-lg-4 align-self-center align-self-center control-label text-left">Name <span style="color:red">*</span></label>
                <div class="col-lg-8">
                  <input type="text" name="name" data-required="true" class="form-control" value="<?php echo set_value('name')?>" id="name"> 
                     <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('name');?></div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-4 align-self-center control-label text-left">Username <span style="color:red">*</span></label>
                <div class="col-lg-8">
                  <input type="text" name="username" data-required="true" class="form-control" value="<?php echo set_value('username')?>" id="username" maxlength="50"> 
                     <div class="m-t m-t-mini error" style="color:#F00" id="usererror"> <?php echo form_error('username');?></div>
                     <div class="m-t m-t-mini " id="usernamechk"  style="text-align:left"></div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-4 align-self-center control-label text-left">Email <span style="color:red">*</span></label>
                <div class="col-lg-8">
                  <input type="text" name="email" class="form-control" data-required="true" data-type="email" value="<?php echo set_value('email')?>" id="email">
                   <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('email');?></div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-4 align-self-center control-label text-left">Mobile No. <span style="color:red">*</span></label>
                <div class="col-lg-8">
                  <input type="text" maxlength="10" name="mobile" class="form-control" value="<?php echo set_value('mobile')?>" id="mobile">
                  <div class="m-t m-t-mini error" style="color:#F00"><?php echo form_error('mobile');?></div>
                  <!--<div class="line line-dashed m-t-large"></div>-->
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-4 align-self-center control-label text-left">Landline No. </label>
                <div class="col-lg-8">
                  <input type="text" name="landline" class="form-control" value="<?php echo set_value('landline')?>" id="landline">
                    <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('landline');?></div>
                  <!--<div class="line line-dashed m-t-large"></div>-->
                </div>
              </div>
               <div class="form-group row">
                <label class="col-lg-4 align-self-center control-label text-left">Aadhar card number</label>
                <div class="col-lg-8">
                  <input type="text" name="aadhar_number" maxlength="16" class="form-control" value="<?php echo set_value('aadhar_number')?>">
                     <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('aadhar_number');?></div>
                  <!--<div class="line line-dashed m-t-large"></div>-->
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-4 align-self-center control-label text-left">Address Line 1 <span style="color:red">*</span></label>
                <div class="col-lg-8">
                  <input type="text" name="address1" class="form-control" value="<?php echo set_value('address1')?>" id="address1">
                   <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('address1');?></div>
                  <!--<div class="line line-dashed m-t-large"></div>-->
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-4 align-self-center control-label text-left">Address Line 2</label>
                <div class="col-lg-8">
                  <input type="text" name="address2" class="form-control" value="<?php echo set_value('address2')?>" id="address2">
                     <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('address2');?></div>
                  <!--<div class="line line-dashed m-t-large"></div>-->
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-4 align-self-center control-label text-left">State <span style="color:red">*</span></label>
                <div class="col-lg-8">
                  <select class="form-control states" name="state" class="states">
                  <option value="">Select State</option>
				  <?php foreach($state as $states) {?>
                    <option value="<?php echo $states['ch2']?>" <?php if($states['ch2']==set_value('state')){echo "selected='selected'";}?>><?php echo $states['ch3']?></option>
                  <?php } ?>
                  </select>
                   <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('state');?></div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-4 align-self-center control-label text-left">City <span style="color:red">*</span></label>
                <div class="col-lg-8">
                  <select class="form-control" name="city" id="selectcity">
               	 <option value="">Select City</option>
                    <?php 
									if(count($city) > 0)
        					{
        						foreach($city as $res)
        						{?>
        							<option value="<?php echo $res['ch4']?>" <?php if($res['ch4']==set_value('city')){echo "selected='selected'";}?>><?php echo $res['ch4'];?></option>
        					<?php }
        					}?>
                  </select>
                    <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('city');?></div>
                </div>
              </div>
              
              <div class="form-group row">
                <label class="col-lg-4 align-self-center control-label text-left">Pincode <span style="color:red">*</span></label>
                <div class="col-lg-8">
                  <input type="text" name="pincode"  placeholder="" class="form-control" value="<?php echo set_value('pincode')?>" id="pincode"> 
                    <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('pincode');?></div>
                 
                </div>
              </div>
              
              <div class="form-group row captcha">
                <label class="col-lg-4 control-label text-left cptcha_img">
                  <span id="captImg"><?php echo $image; ?></span>
                    
                  </label>
                <div class="col-lg-6">
                  <input type="text" name="code" placeholder="" class="form-control"  id="code"> 
                    <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('code');?></div>
					         <p class="text-left blue">Please enter the text you see in the image below into the text box provided.</p>
                </div>
                <div class="col-lg-2">
                  <a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh"></i></a> 
                </div>
              </div>

              <div class="mb-3">
                <div class="col-lg-12 mx-auto d-flex justify-content-center">
                                  <ul class="frm-btn-grp">
                                     <li> <button type="submit" name="btn_reg" class="btn_submit btn">Submit</button></li>
                                    <li> <button type="submit" class="btn_cancel btn btn-white">Cancel</button></li>
                                   </ul>
                </div>
              </div>
            </div>
            <div class="d-flex justify-content-center links frgt-pswd">
                        <a class="" href="<?php echo base_url();?>index.php/userlogin">Go back</a>
          </div>
            </form>
          </div>
      </div>
    </div>
</section>
</div>
<script type="text/javascript">

$(document).ready(function(){

  var state = '<?php echo $this->input->post('state') ?>';
  var city = '<?php echo $this->input->post('city') ?>';
  if(state){
  
  var datastring='city='+ city + '&stateid='+ state + '&' + csrfName + '='+csrfHash;
   
   $.ajax({
        type: 'POST',
        data:datastring,
        url: "<?php echo base_url();?>userlogin/getcity_selected",
        success: function(res){ 
          $('#selectcity').html(res);
        }
    });
  }
  
  var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
  $(".states").on('change',function(){
    var datastring='stateid='+ $(this).val() + '&' + csrfName + '='+csrfHash;
    $.ajax({
        type: 'POST',
        data:datastring,
        url: "<?php echo base_url();?>index.php/userlogin/getcity",
        success: function(res){ 
          $('#selectcity').html(res);
        }
      });
  })


	$('#register_close').on('click',function(){
		$("#register_id").css("display", "block");
		});
//to seee the existing user
   $("#username").keyup(function() {
	   $("#usernamechk").html("");
	    $("#usererror").html("");
	   
        //gets the value of the field
        var username = $("#username").val();
        //here would be a good place to check if it is a valid email before posting to your db
        //displays a loader while it is checking the database
        //$("#usernamechk").html('<img alt="" src="/images/loader.gif" />');
		
        //here is where you send the desired data to the PHP file using ajax
        $.post("<?php echo base_url();?>index.php/userlogin/chkusername", {username:username, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'},
            function(result) {
				var regex = /^[a-z0-9]+$/i;// number pattern
 				var flag=0;
				if(regex.test(username))
				{
					flag=1
				}
				else if(flag==0 && username!='')
				{
					$("#usernamechk").html("Please type Alphanumeric values only.");
					$("#usernamechk").addClass('er-error');
					$("#usernamechk").removeClass('er-success');
				}
                if(result == 1 && flag == 1) {
                    //the user is available
                    $("#usernamechk").html("User name is available.");
					$("#usernamechk").addClass('er-success');
                	$("#usernamechk").removeClass('er-error');
				}
                else if(result == 0 && username!=''){
                    //the user is not available
                    $("#usernamechk").html("Username is  not available.");
					$("#usernamechk").addClass('er-error');
					$("#usernamechk").removeClass('er-success');
                }
            });
       });		
	});
</script>
<script>
         $(document).ready(function(){
             $('.refreshCaptcha').on('click', function(){
                 $.get('<?php echo base_url().'userlogin/refresh_reg'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
         });
</script>
