<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                        <ul class="aside-list">							
                           <li><a href="<?php echo base_url('publication/acts'); ?>" <?php if($subtitle == "Acts"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Acts</a></li>
                            <li><a href="<?php echo base_url('publication/schedules'); ?>" <?php if($subtitle == "Schedules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Schedules</a></li>
                            <li><a href="<?php echo base_url('publication/rules'); ?>" <?php if($subtitle == "Rules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Rules</a></li>
                            <li><a href="<?php echo base_url('publication/notifications'); ?>" <?php if($subtitle == "Notifications"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Notifications</a></li>
                            <li><a href="<?php echo base_url('publication/grs'); ?>" <?php if($subtitle == "GRS"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>GRs</a></li>
                            <li><a href="<?php echo base_url('publication/circulars'); ?>" <?php if($subtitle == "Circulars"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Circulars</a></li>
							<li><a href="<?php echo base_url('publication/asr'); ?>" <?php if($subtitle == "ASR"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>ASR Guidelines</a></li>
                            <!--<li><a href="<?php echo base_url('publication/fee_structure'); ?>" <?php if($subtitle == "Fee Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Fee Structure</a></li>
                             <li><a href="<?php echo base_url('publication/reports'); ?>" <?php if($subtitle == "Reports"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Reports</a></li>
                              <li><a href="<?php echo base_url('publication/dept_exam_results'); ?>" <?php if($subtitle == "Department Exam Results"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Department Exam Results</a></li>-->
                         </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3 mb-3">				
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('publication'); ?>">Publications</a></li>
					<li class="breadcrumb-item active" aria-current="page">Acts</li>
				  </ol>
				</nav>
                    <div id="accordion">
						  <div class="card">
							<div class="card-header" id="headingOne">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								  Registration
								</button>
							  </h5>
							</div>

							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>/pdf/Registration-Act-1908.pdf" target="_blank">Registration Act, 1908</a></p>
								
							 </div>
							</div>
						  </div>
						  <div class="card">
							<div class="card-header" id="headingTwo">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								  Stamps
								</button>
							  </h5>
							</div>
							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>/pdf/MSA_(SECOND)_AMENDMENT_2017.pdf" target="_blank">Maharashtra Stamp (Second Amendment) Act 2017</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/GazetteSearch.pdf" target="_blank">Maharshtra Stamp (Amendment) Act 2015</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/THE_MAHARASHTRA_STAMP_ACT-2016-revised_sections.pdf" target="_blank">Maharashtra Stamp Act</a></p>	
								<p><a href="<?php echo base_url(); ?>/pdf/The-Maharashtra-Court-Fees-Act.pdf" target="_blank">The Maharashtra Court Fees Act</a></p>	
								<p><a href="<?php echo base_url(); ?>/pdf/Indian_Stamp_Act_1899-Index.pdf" target="_blank">Indian Stamp Act</a></p>		
							 </div>
							</div>
						  </div>
						  <div class="card">
							<div class="card-header" id="headingThree">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								  Marriage
								</button>
							  </h5>
							</div>
							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>/pdf/SPMACT1954.pdf" target="_blank">Special Marriage Act, 1954</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/MahMarrigRegAct1988.pdf" target="_blank">Maharashtra Registration of Marriages Act, 1998</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/MarriageAct1998_Eng.pdf" target="_blank">The Maharashtra Regulation of Marriage Bureaus and Registration of Marriages Act ,1998 (English)</a></p>	
								<p><a href="<?php echo base_url(); ?>/pdf/CHRISTIAN-ACT-1872.pdf" target="_blank">Christian ACT Marriage 1872</a></p>	
								<p><a href="<?php echo base_url(); ?>/pdf/SPM_ACT_MARATHI_1998.pdf" target="_blank">SPM ACT 1998(Marathi)</a></p>		
							 
							</div>
							</div>
						  </div>						  
						</div>
                </div>
            </div>
        </div>
    </section>





