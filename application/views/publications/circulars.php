<style>
.hide{display:none;}
.show{display:block;}
</style>
<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                        <ul class="aside-list">							
                            <li><a href="<?php echo base_url('publication/acts'); ?>" <?php if($subtitle == "Acts"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Acts</a></li>
                            <li><a href="<?php echo base_url('publication/schedules'); ?>" <?php if($subtitle == "Schedules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Schedules</a></li>
                            <li><a href="<?php echo base_url('publication/rules'); ?>" <?php if($subtitle == "Rules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Rules</a></li>
                            <li><a href="<?php echo base_url('publication/notifications'); ?>" <?php if($subtitle == "Notifications"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Notifications</a></li>
                            <li><a href="<?php echo base_url('publication/grs'); ?>" <?php if($subtitle == "GRS"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>GRs</a></li>
                            <li><a href="<?php echo base_url('publication/circulars'); ?>" <?php if($subtitle == "Circulars"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Circulars</a></li>
							<li><a href="<?php echo base_url('publication/asr'); ?>" <?php if($subtitle == "ASR"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>ASR Guidelines</a></li>
                            <!--<li><a href="<?php echo base_url('publication/fee_structure'); ?>" <?php if($subtitle == "Fee Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Fee Structure</a></li>
                             <li><a href="<?php echo base_url('publication/reports'); ?>" <?php if($subtitle == "Reports"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Reports</a></li>
                              <li><a href="<?php echo base_url('publication/dept_exam_results'); ?>" <?php if($subtitle == "Department Exam Results"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Department Exam Results</a></li>-->
                         </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3 mb-3">				
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('publication'); ?>">Publications</a></li>
					<li class="breadcrumb-item active" aria-current="page">Circulars</li>
				  </ol>
				</nav>
                    <div id="accordion">
						<div class="card">
							<div class="card-header" id="headingOne">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								  Registration
								</button>
							  </h5>
							</div>
							<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
							  <div class="card-body">
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">A Patrak</a>
									<div class="data-contents">
										<table class="table table-bordered"><tbody><tr><th style="text-align: center;">Sr no</th>      <th style="text-align: center;">Subject </th>
                                        <th style="text-align: center;">Date</th>
                                                    <th style="text-align: center;">Download</th>
                                    </tr>

										<tr>
                                        <td>1</td>
                                        <td>Sending the A Patrak and C.T.S. Patrak in prescribed time to Tahasildars and City Survey Officer.</td>
                                        <td>26/09/1997</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/APATRKA.pdf" target="_blank">
                                           <i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                            
                                </tbody></table>
									</div>
								</div>	
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Acton under Sec 82 in case of False Personation in Registration</a>
									<div class="data-contents">
										<table class="table table-bordered">
											<tbody><tr>
												<th style="text-align: center;">Sr no</th>
												<th style="text-align: center;">Subject</th>
												<th style="text-align: center;">Date</th>
															<th style="text-align: center;">Download</th>
											</tr>

										  <tr>
												<td>1</td>
												<td>Proper and Effective Implementation of Section 82 of Registration Act 1908</td>
												<td>30/11/2013</td>
												<td align="center">
												 <a href="<?php echo base_url(); ?>pdf/newpdf.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
												
												</td>
											</tr>

										</tbody></table>
									</div>
								</div>	
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Bond Writer</a>
									<div class="data-contents">
										<table class="table table-bordered">
											<tbody><tr>
												<th style="text-align: center;">Sr no</th>
												<th style="text-align: center;">Subject </th>
												<th style="text-align: center;">Date</th>
															<th style="text-align: center;">Download</th>
											</tr>

											<tr>
												<td>1</td>
												<td>Rates of writing the documents</td>
												<td>26/09/1997</td>
												<td align="center">
													<a href="<?php echo base_url(); ?>pdf/RATE1997.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
												</td>
											</tr>
													<tr>
												<td>2</td>
												<td>Making rules or amendment in rules for giving new license or renewal of Bond writer’s license</td>
												<td>21/06/2001</td>
												<td align="center">
													<a href="<?php echo base_url(); ?>pdf/RULE2001.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
												</td>
											</tr>
													<tr>
												<td>3</td>
												<td>Renewal of Bond writer’s license</td>
												<td>14/02/2011</td>
												<td align="center">
													<a href="<?php echo base_url(); ?>pdf/RULE2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
												</td>
											</tr>
										</tbody></table>
									</div>
								</div>

								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Confirmation Deed & Declaration</a>
									<div class="data-contents">
										<table class="table table-bordered">
											<tbody><tr>
												<th style="text-align: center;">Sr no</th>
												<th style="text-align: center;">Subject</th>
												<th style="text-align: center;">Date</th>
															<th style="text-align: center;">Download</th>
											</tr>

											<tr>
												<td>1</td>
												<td>Registration of Confirmation deed or Declaration deed</td>
												<td>22/12/2011</td>
												<td align="center">
													<a href="<?php echo base_url(); ?>pdf/CIR.22.12.2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
												</td>
											</tr>

											<tr>
												<td>2</td>
												<td>Registration of Confirmation deed or Declaration deed - Explanation</td>
												<td>28/05/2012</td>
												<td align="center">
													<a href="<?php echo base_url(); ?>pdf/EXPLANATION.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
												</td>
											</tr>

											<tr>
												<td>3</td>
												<td>Registration of Confirmation deed or Declaration deed - Explanation</td>
												<td>30/11/2013</td>
												<td align="center">
													 <a href="<?php echo base_url(); ?>pdf/RIVISE.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
												</td>
											</tr>


										</tbody></table>
									</div>
								</div>

								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Documents Executed on Basis of Power of Attorney</a>
									<div class="data-contents">
										<table class="table table-bordered">										
                                    <tbody><tr>
                                        <th style="text-align: center;">Sr no</th>
                                        <th style="text-align: center;">Subject</th>
                                        <th style="text-align: center;">Date</th>
                                                    <th style="text-align: center;">Download</th>
                                    </tr>

                               <tr>
                                        <td>1</td>
                                        <td>Attestation and Authentication of Special and General Power of Attorney by Sub-Registrar and acceptance of other documents for Registration and Stamp duty liable for that  </td>
                                        <td>11/06/2001</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/POA23022007.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
<tr>
                                        <td>2</td>
                                        <td>Making the Copy of Power of Attorney with document as a part of document</td>
                                        <td>18/02/2003</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/POA29012007.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
<tr>
                                        <td>3</td>
                                        <td>Taking the Photo and thumb print of Power of Attorney holder on the document registered through Power of Attorney</td>
                                        <td>30/09/2003</td>
                                        <td align="center">
                                           <a href="<?php echo base_url(); ?>pdf/POA11.06.2001.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
<tr>
                                        <td>4</td>
                                        <td>Admission of execution of document executed through General Power of Attorney </td>
                                        <td>13/04/2005</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/POA13042005.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
<tr>
                                        <td>5</td>
                                        <td>About Power of Attorney used in Registration process  </td>
                                        <td>29/01/2007</td>
                                        <td align="center">
                                           <a href="<?php echo base_url(); ?>pdf/POA18012008 .pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
<tr>
                                        <td>6</td>
                                        <td>About Power of Attorney used in Registration process  </td>
                                        <td>23/02/2007</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/POA18022003.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
<tr>
                                        <td>7</td>
                                        <td>Admission of execution of document executed through General Power of Attorney and taking precaution while Identification in the document while registering  </td>
                                        <td>18/01/2008</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/POA30092013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                </tbody></table>
									</div>
								</div>	
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Filing of Notice of Intimation</a>
									<div class="data-contents">
										<table class="table table-bordered">
                                    <tbody><tr>
                                        <th style="text-align: center;">Sr no</th>
                                        <th style="text-align: center;">Subject</th>
                                        <th style="text-align: center;">Date</th>
                                                    <th style="text-align: center;">Download</th>
                                    </tr>

                                 <tr>
                                        <td>1</td>
                                        <td>Implementation of Amendment in Registration Act – 2010</td>
                                        <td>25/03/2013</td>
                                        <td align="center">
                                           <a href="<?php echo base_url(); ?>pdf/05.07.2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

<tr>
                                        <td>2</td>
                                        <td>To Prescribe the Sub-Registrar and Joint Sub-Registrar Offices for the e-Registration and e-filing through e-Registration and e-filing Module</td>
                                        <td>28/03/2013</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/25.03.2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

<tr>
                                        <td>3</td>
                                        <td>Filing of Notice of Intimation According to the Section 89 B of Registration Act 1908</td>
                                        <td>25/04/2013</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/25.04.2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

<tr>
                                        <td>4</td>
                                        <td>To Prescribe the Sub-Registrar and Joint Sub-Registrar Offices for the e-Registration and e-filing through e-Registration and e-filing Module</td>
                                        <td>05/07/2013</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/28.03.2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Home Visit</a>
									<div class="data-contents">
										<table class="table table-bordered">                                    
                                    <tbody><tr>
                                        <th style="text-align: center;">Sr no</th>
                                        <th style="text-align: center;">Subject</th>
                                        <th style="text-align: center;">Date</th>
                                                    <th style="text-align: center;">Download</th>
                                    </tr>

                                  <tr>
                                        <td>1</td>
                                        <td>Home visit for Registration of Document.</td>
                                        <td>12/06/2013</td>
                                        <td align="center">
                                             <a href="<?php echo base_url(); ?>pdf/12.06.2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Identification While Registration</a>
									<div class="data-contents">
										<table class="table table-bordered">                                    
                                   
                                    <tbody><tr>
                                        <th style="text-align: center;">Sr no</th>
                                        <th style="text-align: center;">Subject</th>
                                        <th style="text-align: center;">Date</th>
                                                    <th style="text-align: center;">Download</th>
                                    </tr>

                                  <tr>
                                        <td>1</td>
                                        <td>To Prevent Registration of document through false person</td>
                                        <td>06/06/2007</td>
                                        <td align="center">
                                             <a href="<?php echo base_url(); ?>pdf/05.09.2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
   <tr>
                                        <td>2</td>
                                        <td>To Prevent Registration of document through false person</td>
                                        <td>09/07/2007</td>
                                        <td align="center">
                                             <a href="<?php echo base_url(); ?>pdf/06.06.2007.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
   <tr>
                                        <td>3</td>
                                        <td>To Prevent Registration of document through false person</td>
                                        <td>14/01/2009</td>
                                        <td align="center">
                                             <a href="<?php echo base_url(); ?>pdf/09.07.2007.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
   <tr>
                                        <td>4</td>
                                        <td>To Prevent Registration of document through false person</td>
                                        <td>23/08/2013</td>
                                        <td align="center">
                                             <a href="<?php echo base_url(); ?>pdf/14.012009.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
   <tr>
                                        <td>5</td>
                                        <td>To Prevent Registration of document through false person</td>
                                        <td>05/09/2013</td>
                                        <td align="center">
                                             <a href="<?php echo base_url(); ?>pdf/23.08.2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Refusal for Registration</a>
									<div class="data-contents">
										<table class="table table-bordered">
                                    <tbody><tr>
                                        <th style="text-align: center;">Sr no</th>
                                        <th style="text-align: center;">Subject</th>
                                        <th style="text-align: center;">Date</th>
                                                    <th style="text-align: center;">Download</th>
                                    </tr>

                              <tr>
                                        <td>1</td>
                                        <td>Procedure  According to the Provision of Section 71 of Registration Act 1908 </td>
                                        <td>30/04/2005</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/CIR.30.04.2005.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
<tr>
                                        <td>2</td>
                                        <td>Procedure for Refusal of Registration of documents</td>
                                        <td>27/07/2009</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/CIR.27.07.2009.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                </tbody></table>
									</div>
								</div>

								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Registration of Document</a>
									<div class="data-contents">
										<table class="table table-bordered">
                                   
                                    <tbody><tr>
                                        <th style="text-align: center;">Sr no</th>
                                        <th style="text-align: center;">Subject</th>
                                        <th style="text-align: center;">Date</th>
                                                    <th style="text-align: center;">Download</th>
                                    </tr>

                              <tr>
                                        <td>1</td>
                                        <td>Maharashtra Registration (Amendment) Rule 2005</td>
                                        <td>23/08/2006</td>
                                        <td align="center">
                                             <a href="<?php echo base_url(); ?>pdf/REG.CIR.23.08.2006.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
<tr>
                                        <td>2</td>
                                        <td>Registration of documents of Wakf property</td>
                                        <td>21/9/2007</td>
                                        <td align="center">
                                              <a href="<?php echo base_url(); ?>pdf/REG.CIR.29.09.2007.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
          <tr>
                                        <td>3</td>
                                        <td>Maharashtra Registration (Amendment) Rule 2005 </td>
                                        <td>04/12/2007</td>
                                        <td align="center">
                                              <a href="<?php echo base_url(); ?>pdf/REG.CIR.04.12.2007.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
<tr>
                                        <td>4</td>
                                        <td>Registration of documents of transfer of occupancy rights in property of Scheduled Tribes in favor of non Scheduled Tribes without permission of State Government or Collector according to the Rule 36 and 36 A of Maharashtra Land Revenue Act 1966 </td>
                                        <td>25/03/2013</td>
                                        <td align="center">
                                              <a href="<?php echo base_url(); ?>pdf/REG.CIR.25.03.2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
          <tr>
                                        <td>5</td>
                                        <td>Implementation of Rule 44 of Maharashtra Registration Rule 1961</td>
                                        <td>02/11/2013</td>
                                        <td align="center">
                                             <a href="<?php echo base_url(); ?>pdf/REG.CIR.02.11.2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                </tbody></table>
									</div>
								</div>	
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Relevant Provisions in Income Tax Act</a>
									<div class="data-contents">
										<table class="table table-bordered">
                                    <tbody><tr>
                                        <th style="text-align: center;">Sr no</th>
                                        <th style="text-align: center;">Subject</th>
                                        <th style="text-align: center;">Date</th>
                                                    <th style="text-align: center;">Download</th>
                                    </tr>

                                 <tr>
                                        <td>1</td>
                                        <td>Mentioning the PAN in the document according to the provision of the Income Tax Act, 1961 </td>
                                        <td>28/09/2003</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/02.112010.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

<tr>
                                        <td>2</td>
                                        <td>Mentioning the PAN in the document according to the provision of the Income Tax Act, 1961</td>
                                        <td>27/01/2004</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/04.04.2005.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

<tr>
                                        <td>3</td>
                                        <td>Providing  information to the Income Tax Department</td>
                                        <td>04/04/2005</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/27.01.2004.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

<tr>
                                        <td>4</td>
                                        <td>Verification that PAN properly mentioned in the document and PAN Card is to be made part of the document according to the provision of the Income Tax Act, 1961 </td>
                                        <td>02/11/2010</td>
                                        <td align="center">
                                            <a href="<?php echo base_url(); ?>pdf/28.09.2003.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">About the contents in the Notice of Lis- Pendence</a>
									<div class="data-contents">
										<table class="table table-bordered">                                  
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.</th>
                                        <th style="text-align: center;">विषय</th>
                                        <th style="text-align: center;">दिनांक</th>
                                                    <th style="text-align: center;">Download</th>
                                    </tr>
                                      
                                 <tr>
                                        <td>1</td>
                                        <td>About the contents in the Notice of Lis- Pendence.</td>
                                        <td>03/08/2015</td>
                                        <td align="center">
                                           <a href="#" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>



                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Use of iSARITA system</a>
									<div class="data-contents">
										<table class="table table-bordered">
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>Use of iSARITA system (Web based Registration System)</td>
                                        <td>26/07/2012</td>
                                        <td>D-3/pra.ka./2012/309</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/Office_Circular_1.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
									
							 </div>
							</div>
						 </div>
						<div class="card">
							<div class="card-header" id="headingTwo">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
								  Stamps
								</button>
							  </h5>
							</div>
							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							  <div class="card-body">
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Abhay Yojna</a>
									<div class="data-contents">
										<table class="table table-bordered">                                    
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>माफी योजना सन 2008</td>
                                        <td>6/9/2008</td>
                                        <td>का5/ मुद्रांक 2008/ प्र क्र 16/07/08/ 556-575 </td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/June2008.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>2</td>
                                        <td>मुंबई मुद्रांक सुधारणा 2008</td>
                                        <td>6/5/2014</td>
                                        <td>मु 2007/2910/प्रक्र 474/278/14</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/2-OrderJune2014.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>3</td>
                                        <td>अभय योजना 2019</td>
                                        <td>1/3/2019</td>
                                        <td>RNI No. MAHBIL/2009/37831</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/amnesty_2019.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Adjucdication</a>
									<div class="data-contents">
										<table class="table table-bordered"> 
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>कलम 31 खाली अभिनिणयासाठी दाखल होण्या-या प्रकरणाबाबत</td>
                                        <td>5/30/2011</td>
                                        <td>का 5/अभि/ प्र.क्र.20/11/651-659/11</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/1-Circular.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>कलम 31 खाली अभिनिणयासाठी परिपत्रक</td>
                                        <td>7/27/2011</td>
                                        <td>का 5/अभि/ मु.11/प्र.क.23/11/924-932</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/2-Circular2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>कलम 31,32अ,32ब व 33बाबत</td>
                                        <td>10/21/2011</td>
                                        <td>का 5/अभि/ मु.11/प्र.क.59/11/1504/11</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/3-Circular2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>कलम 31 खाली अभिनिणयासाठी परिपत्रक</td>
                                        <td>12/5/2011</td>
                                        <td>का 5/अभि/ मु.11/प्र.क.20/11/1624/11</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/4-Circular2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>महाराष्ट्र अदयोगीक विकास महामंडळ/ शासकीय निमशासकीय संस्था/ स्थानिक स्वराज्य संस्था महामंडळे यांचेकडून केल्या जाणा-या भाडेपट्टयाच्या दस्तऐवजांचे अभिनिर्णयबाबत</td>
                                        <td>5/8/2012</td>
                                        <td>का5/मु11/प्रक्र 23/11</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/5-Circular2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>विभागाची सर्व काया्रलयात एकसुत्रता आणण्यासाठी प्रत्येक कार्यालयासाठी संकेतांक कोड देण्याबाबत</td>
                                        <td>8/10/2012</td>
                                        <td>का5/संकेता कोड/ प्र क्र 26/781/म -1</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/6-Circular2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>मु मु अधिनियम 1958 चे कलम 41 खाली प्रमाणपत्र दिल्यांनतर नविन मु्&zwnj;द्दा समाविष्ट करण्याबाबत </td>
                                        <td>8/23/2012</td>
                                        <td>का 5/ अभिनिर्णय/ प्र क्र 27/12/ 804/ 12</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/7-CircularAug2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Alternate Agreement</a>
									<div class="data-contents">
										<table class="table table-bordered"> 
										
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>मुळमालकास मिळणा-या बांधीव क्षेत्रावर मुद्रांक शुल्काची आकारणी </td>
                                        <td>5/9/2014</td>
                                        <td>क्र याचिका2013/ 1425/प्र क्र 260/ म-1/</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/1-Order2014.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl_stamps_altAgreement">Regarding Stamp Duty on the document in favour of member of the society’s redevelopment</span>
                                        </td>
                                        <td>30/3/2017</td>
                                        <td>
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_Label68">Desk-5/Stamp-17/C.R.No.10/13/303/17.</span></td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/Regarding_Stamp_Duty_on_redevelopment.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Appeal</a>
									<div class="data-contents">
										<table class="table table-bordered"> 
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>अपीलाबाबत</td>
                                        <td>7/18/2003</td>
                                        <td>शासन परिपत्रक क्र. 1847/म</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/Circular2003.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Deem Conveyance</a>
									<div class="data-contents">
										<table class="table table-bordered"> 
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>संमती पत्र् व घोषणापत्र् </td>
                                        <td>12/22/2011</td>
                                        <td>क्र का 4/ प्र क/ 617/ 2011/ 3008</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/1-Circular2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>मोफा अअधिनिमय 1963 मधील तरतूदीनुसर सहकारी गृहनिर्माणसंस्थाच्या मानीव अभिहस्तांतरणपत्राच्या नोंदणीसाठी कालबध्द काय्रक्रम </td>
                                        <td>11/26/2012</td>
                                        <td>मु 2012/1046/ प्र क्र 294/ म -1/ </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/2-Government2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>डिम कन्व्हेन्स </td>
                                        <td>3/18/2014</td>
                                        <td>क्र क 5/मु 14/प्र क्र 44/11/105/14</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/3-Circular2014.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>डिम कन्व्हेन्स </td>
                                        <td>4/4/2014</td>
                                        <td>जा क्र 15/मानीव खरेदीखत/सु के /मु/288</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/4-Circular2014.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Evaision</a>
									<div class="data-contents">
										<table class="table table-bordered">
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>

                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>मुद्रांकशुल्क वसुलीबाबत</td>
                                        <td>4/12/2011</td>
                                        <td>का 16/सं.वि.प्र.क्र.01/11/502-559/11</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/1-Circular12Apiral2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>मुद्रांकशुल्क चिकवीण्यावर नियंत्रण ठेवणेवर</td>
                                        <td>5/20/2011</td>
                                        <td>का 16/सं.वि.प्र.क्र.01/11/502-559/11</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/2-Circular20May2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>कलम 31,32अ,32ब व 33बाबत.</td>
                                        <td>10/21/2011</td>
                                        <td>का 5/ मु.11/प्र.क.59/11/1504/11</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/3-Circular21Oct2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>गहाणखताचे प्रत्यांतरण पत्र</td>
                                        <td>12/13/2011</td>
                                        <td>का 5/मु.11/प्र.क.1101/2000/1649/11</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/4-Circular13Dec2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>विभागाची सर्व काया्रलयात एकसुत्रता आणण्यासाठी प्रत्येक कार्यालयासाठी संकेतांक कोड देण्याबाबत </td>
                                        <td>8/10/2012</td>
                                        <td>का5/संकेता कोड/ प्र क्र 26/781/म -1</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/5-Circular0Aug2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>मु मु अधिनियम 1958 चे कलम 41 खाली प्रमाणपत्र दिल्यांनतर नविन मु्&zwnj;द्दा समाविष्ट करण्याबाबत </td>
                                        <td>8/23/2012</td>
                                        <td>का 5/ अभिनिर्णय/ प्र क्र 27/12/ 804/ 12 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/6-Circular23Aug2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>मुद्रांकाच्या गैरव्यवहारास प्रतिबंधाबाबत</td>
                                        <td>12/17/2013</td>
                                        <td>का 5/मु.13/प्र.क.28/13/844/13</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/7-Circular17 Dec2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Franking</a>
									<div class="data-contents">
										<table class="table table-bordered"> 
                                    
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>बोंगस फ्रॅकिंगबाबत प्रतिबंध </td>
                                        <td>1/27/2011</td>
                                        <td>क्र का 5/बो फ्रॅ/प्र क्र 70/ 2004/ 121/11</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/1-Circu2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Government Circular</a>
									<div class="data-contents">
										<table class="table table-bordered"> 
                                   
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>प्रमुख खानपटटा करारनामा मु.शु.अकारणीबाबत</td>
                                        <td>9/5/1992</td>
                                        <td>शासन परिपत्रक /एमएनजी/0411प्रक्र.27उद्योग-1</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/1-Order1992.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>मृत गिरणी कामगार हक्कसोड पत्र</td>
                                        <td>3/15/2013</td>
                                        <td>शा प मु 2013/प्रक्र 309/म-1</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/2-Order2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Inspection</a>
									<div class="data-contents">
										<table class="table table-bordered"> 
                                 
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>महालेखापाल महाराष्ट्र राज्य नागपूर यांच्या स्थानिक तपासणी अहवालावरील अभिप्रायाबाबत</td>
                                        <td>1/12/1986</td>
                                        <td>क्र 6/सीआर 2048/परिपत्रक/ 2656 -96</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/1-P1986.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>अंतर्गत लेखा परीक्षा पथक</td>
                                        <td>8/25/1987</td>
                                        <td>क्र नोमनि </td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/2-P1987.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>स्थानिक तपासणी अहवाल </td>
                                        <td>12/23/1987</td>
                                        <td>क्र 6/सीआर 2048/परिपत्रक/ 2255</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/3-P1987.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>नोंदणी महानिरीक्षक /सहाय्यक नोंदणी महानिरीक्षक/ नोंदणी उपमहानिरीक्षक (विभागीय) / सह जिल्हा निबधंक/ जिल्हा निबंधक यांनी विविध कार्यालयांवर काढलेल्या निरीक्षण टिपण्यांचे अनुपालनाबाबत.</td>
                                        <td>9/5/1996</td>
                                        <td>क्र का 11/प्रक्र 1985/96/213- 596 </td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/4-P1996.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>महालेखापाल यांनी काढलेल्या स्थानिक तपासणी अहवालामधील मान्य केलेल्या परिच्छेदातील वसुलीबाबत</td>
                                        <td>10/14/1998</td>
                                        <td>क्र 6/सीआर 2048/परिपत्रक/ 2283-264</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/5-P1998.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>500 पेक्षा कमी वसुलीची प्रकरणे निर्लेखित</td>
                                        <td>1/18/2007</td>
                                        <td>शा.नि.क्र. लोलस06/3184/प्रक्र 471/म1</td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/6-Gov.Order2007.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">LBT</a>
									<div class="data-contents">
										<table class="table table-bordered"> 
                                  
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>विक्री देणगी किंवा फलोपभोग गहाणखताच्या संलेखावर एलबीटी लागू करणेबाबत</td>
                                        <td>11/7/2013</td>
                                        <td>का5/मुद्रांक13/ प्र क्र 22/13/756 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/1-Circular2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>पुणे नागपूर ठाणे नविमुंबई पिंपरी चिंचवड महानगरपालिका स्थांनिक संस्थकर लागू करणे बाबत</td>
                                        <td>4/17/2014</td>
                                        <td>का5/मुद्रांक13/ एलबीटी/प्र क्र 22/12/356/13</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/2-Circular2014.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Metro Cess</a>
									<div class="data-contents">
										<table class="table table-bordered"> 
                                 
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>Thane Metro Notification</td>
                                        <td style="text-align: center">31-07-2019</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/ThaneMetroNotification.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>2</td>
                                        <td>Maharashtra Municipal Corporation (Second Amendment ) Act 2015</td>
                                        <td style="text-align: center">-</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/1.Maharashtra_Muncipal_Corporation_(Second_Amendment_)_Act_2015.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>3</td>
                                        <td>Notification of effect to Maharashtra Municipal Corporation Amendment Act</td>
                                        <td style="text-align: center">-</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/2.Notification_of_effect_to_Maharashtra_Muncipal_Corporation_Amendment_Act.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>4</td>
                                        <td>Pune Metro Notification</td>
                                        <td>10/05/2018</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/3.Pune_Metro_Notification_10052018.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>5</td>
                                        <td>Mumbai Municipal Corporation (Second Amendment) Act 2018</td>
                                        <td style="text-align: center">-</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/4.Mumbai_Muncipal_Corporation_(Second_Amendment)_Act_2018.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>6</td>
                                        <td>Notification of effect to Mumbai Muncipal Corporation Amendment Act</td>
                                        <td style="text-align: center">-</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/5.Notification_of_effect_to_Mumbai_Muncipal_Corporation_Amendment_Act.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>7</td>
                                        <td>Mumbai Metro Notification</td>
                                        <td>01/03/2017</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/6.Mumbai_Metro_Notification_01032017.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>


                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Online GRAS Circular</a>
									<div class="data-contents">
										<table class="table table-bordered">
									
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>फ्रॅकिंग मशिन व्दारे मुशु ग्रास व्दारे जमा करण्याबाबत</td>
                                        <td>2/23/2011</td>
                                        <td>क्रका5/2004/ प्र क्र 70/2004/225-11</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/Circular2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>फ्रॅकिंग मशिन व्दारे मुशु ग्रास व्दारे जमा करण्याबाबत</td>
                                        <td>2/23/2011</td>
                                        <td>क्रका5/2004/ प्र क्र 70/2004/225-11</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/2-CircularforGRAS.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Gras माध्यमातून मुशु नोंफी भरणाबाबत </td>
                                        <td>10/6/2013</td>
                                        <td>क्रका5/ पेमेंट/esbtr/  प्र क्र </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/3-Circular2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>मुद्रांक विक्रेत्यांना चलन विरुपित करुन तात्काळ सेवा देण्याची सुविधा उपलब्ध करुन देणेबाबत</td>
                                        <td>10/30/2013</td>
                                        <td>क्र व्हर्च्युअल/2013/ न क्र 119/557</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/4-Circular2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Refund</a>
									<div class="data-contents">
										<table class="table table-bordered"> 
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>परतावा प्रकरणात छाननीसाठी स्विकारावयाची कार्यपध्दती व काळजी </td>
                                        <td>11/24/1986</td>
                                        <td>का 5/मुद्रांक परतावा/ 1409- 1440 </td>
                                        <td style="text-align: center">
                                             <a href="<?php echo base_url(); ?>pdf/1-Circular1.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>मु अधि 1958 महाराष्ट्र अधिनियम क्र 1990 अन्वये केलेल्या सुधारणाप्रमाणे जास्त धरलेल्या मुद्रांक शुल्काचा परतावा मंजुर करण्याची कार्यपध्दती </td>
                                        <td>6/26/1990</td>
                                        <td>क्र मु 1090/ प्र क्र 245/म 1</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/2-Circular1990.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>3</td>
                                        <td>मु अधि 1958 महाराष्ट्र अधिनियम क्र 1990 अन्वये केलेल्या सुधारणाप्रमाणे जास्त धरलेल्या मुद्रांक शुल्काचा परतावा मंजुर करण्याची कार्यपध्दती </td>
                                        <td>9/27/1990</td>
                                        <td>क्र मु 1090/ प्र क्र 345/म 1</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/3-Circular1990.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>कोर्ट फी सर्टीफिकेट परताव्याबाबत कार्यपध्दती</td>
                                        <td>10/26/1990</td>
                                        <td>का 5/रिफंड/ कोर्ट फी सर्टीफिकेट/ 1984 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/4-Circular1990.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>परताव्याच्या तरतूदीचे काटेकोर पालन करणेबाबत </td>
                                        <td>2/20/1991</td>
                                        <td>मु 109/1099/ प्र क्र 641/म-1 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/5-Circular1991.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>6</td>
                                        <td>परताव्या बाबातचे तरतुदीचंे काटेकोरपणे पालन करणेबाबत.</td>
                                        <td>21/03/1991</td>
                                        <td>का 5/मु/प्रक्र 1137/परिपत्रक/ 91/ 639 -88</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/6-Circular1991.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>भाडेपट्टयाच्या दस्तावर ज्यादा भरलेल्या मुद्रांक शुल्काचा परतावा चारकोप सहाकरी गृहनिर्माण संस्था मर्या मुंबई </td>
                                        <td>9/19/1991</td>
                                        <td>मु 1389/3598/ प्र क्र 769/ म -1 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/7-Circular1991.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>मुद्रांक अधिनिमय 1958 , महाराष्ट्र अधिनियम क्र नऊ 1990 अन्वये केलेल्या सुधारणे प्रमाणे जास्त भरलेल्या मुद्रांक शुल्काचा परतावा मंजुर करण्याची कार्यपध्दती </td>
                                        <td>12/11/1991</td>
                                        <td>का12/ परतावा प्र क्र 805/3319- 3369 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/8-Circular1991.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>9</td>
                                        <td>मुद्रांकाच्या परतावाबाबत </td>
                                        <td>12/4/1992</td>
                                        <td>का 12/मुद्रांक परतावा/ 2127-56</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/9-Circular1992.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>मुद्रांक परतावा मंजुर करणेबाबत </td>
                                        <td>3/31/1996</td>
                                        <td>का 12/मुद्रांक परतावा/ 649-679</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/10-Circular1993.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>मुद्रांकाचा परतावा मंजुर करण्याच्या प्रकरणी मुद्रांकाच्या खरेपणाबाबत छाननी करणे </td>
                                        <td>4/16/1993</td>
                                        <td>मु 1092/ 3977/प्र क्र 1113/म 1</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/11-Circular1993.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>12</td>
                                        <td>मुद्रांकाचा परतावा मंजुर करण्याच्या प्रकरणी मुद्रांकाच्या खरेपणाबाबत छाननी करणे </td>
                                        <td>5/13/1993</td>
                                        <td>का 12/मुद्रांक परतावा/ 1910- 62</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/12-CircularP1993.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>13</td>
                                        <td>मुद्रांक परतावा प्रतिज्ञापत्राबाबत</td>
                                        <td>8/29/1994</td>
                                        <td>का 12/मु/ परतावा/ 1996-2025</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/13-Circular1994.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>14</td>
                                        <td>मुद्रांक परतावा प्रतिज्ञापत्राबाबत</td>
                                        <td>7/2/1995</td>
                                        <td>का 12/मु/ परतावा/  </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/14-Circular1995.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>15</td>
                                        <td>मुद्रांकाचा परतावा मंजुर करण्याच्या प्रकरणी मुद्रांकाच्या खरेपणाबाबत छाननी करणे </td>
                                        <td>9/11/1995</td>
                                        <td>का 12/मुद्रांक परतावा/ 1782-1820 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/15-Circular1995.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>16</td>
                                        <td>मुद्रांक परतावा प्रकरणांमध्ये निष्कारण होणारा विलंब टाळणेबाबत</td>
                                        <td>3/25/1997</td>
                                        <td>क्र का 12/मुद्रांक परतावा/ 1064- 1103 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/16-Circular1997.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>17</td>
                                        <td>परताव्यास पात्र असलेल्या मुद्रांक शुल्काचा परतावा विहीत मुदतीत करण्याबाबत</td>
                                        <td>5/15/2006</td>
                                        <td>शा प मु 2005/प्र क्र 512/म -1/ </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/17-Circular2006.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>18</td>
                                        <td>मुद्रांक शुल्क परताव्याची देयके तयार करणे व कोषागारात सादर करुन धनादेश प्राप्त करुन घेउुन पक्षकारास देण्याबाबत</td>
                                        <td>8/3/2006</td>
                                        <td>का12/मुद्रांक परतावा/ 265/06 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/18-CircularP2006.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>19</td>
                                        <td>परतावा मुद्रांकाच्या खरेपणाबाबत छाननी</td>
                                        <td>7/13/2011</td>
                                        <td>क्र का 12/मु प/मुपस/18/11 परतावा/983-11 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/19-Circular-2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>20</td>
                                        <td>विभागाची सर्व काया्रलयात एकसुत्रता आणण्यासाठी प्रत्येक कार्यालयासाठी संकेतांक कोड देण्याबाबत </td>
                                        <td>8/10/2012</td>
                                        <td>का5/संकेता कोड/ प्र क्र 26/781/म -1</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/20-Circular2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>21</td>
                                        <td>मु.शु.परतावा  आन लाईन कार्यवाही बाबत</td>
                                        <td>1/3/2014</td>
                                        <td>क्र का 12/मु प/आन लाईन/7/2014 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/21-Circular2014.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>

                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Registration</a>
									<div class="data-contents">
										<table class="table table-bordered">									
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>ख्रिश्चन विवाहबाबत </td>
                                        <td>4/30/1985</td>
                                        <td>का 7/विवाह प्रकरण/ 37/85/ 30-71 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/01-Circular1985.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>मुख्यालय दुय्यम निबधक यांचे किरकोळ रजेच्या मुदतीत कार्यभार देणेबाबत</td>
                                        <td>10/14/1986 </td>
                                        <td>क्र अ 11/मुल्यांकन/कार्यभार/ 3005 -34 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/02-Circular1986.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>आयकर अधिनियम कलम 269 यु एस 1 अन्वये ना हरकत प्रमाणपत्र</td>
                                        <td>11/1/1986</td>
                                        <td>का 4/आयकर/ 262 यु / प्रकरण 155 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/03-Circular1986.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>टंकलिखीत दस्तऐवजाबाबत स्वीकारावयाची कार्यपध्दती </td>
                                        <td>1/21/1988</td>
                                        <td>नोंमनि/आरएस/ 1638/ 383-733 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/04-Circular1988.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>छापील दस्तऐवजाच्या नोदंणीबाबत </td>
                                        <td>1/25/1988</td>
                                        <td>क्र नोमनि 4/1638/ 33 ते 382 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/05-Circular1988.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>शासकीय कर्मचा-यांच्या सहकारी गृह निर्माण संस्थेने नेांदणी केलेल्या गहाणखतांच्या प्रमाणपत्राबाबत </td>
                                        <td>10/20/1993</td>
                                        <td>का 4/ प्र क्र 2743/6171 ते 6520 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/06-Circular1993.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>आयसरीता प्रणाली बदला बाबत</td>
                                        <td>7/26/2012</td>
                                        <td>का 3/ 2012/308 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/07-Circular2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>प्रोत्साहन योजना 2007 </td>
                                        <td>8/31/2012</td>
                                        <td>पिएसआय/12/ प्र क्र 80/उदयोग 8 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/08-Circular2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>अदिवासी व्यक्तीचें जमिनीची कुमु व विकसन</td>
                                        <td>3/25/2013</td>
                                        <td>का 4/प्र क्र 247/12/707 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/09-Circular2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>बोगस व्यक्ती व्दारे मिळकत विक्रीस प्रतिबंध</td>
                                        <td>8/23/2013</td>
                                        <td>का 4/प्र क्र 172/07/12/13/1751/13 </td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/10-Circular2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>बोगस व्यक्ती व्दारे मिळकत विक्रीस प्रतिबंध</td>
                                        <td>9/5/2013</td>
                                        <td>का 4/प्र क्र 172/07/12/13/1822/13</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/11-Circular.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>12</td>
                                        <td>घोषणापत्र् व संमतीपत्राबाबत </td>
                                        <td>11/30/2013</td>
                                        <td>का 4/प्र क्र 617/11/भाग -1/</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/12-Circular2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Stamp Change Article</a>
									<div class="data-contents">
										<table class="table table-bordered">									
										
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>मु मु अधिनियम 1958 चे अनुसुची 1 चे अनु 25 मधील सुधारणाबबात </td>
                                        <td>4/20/2012</td>
                                        <td>क्र.का.5/मुद्रांक-12/सुधारणा अनु.25/सुधारणाध/2012-13</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/Circular20Apirl2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
								<div class="main-p"><a href="javascript:void(0);" class="inner-click">Stamp Vendor</a>
									<div class="data-contents">
										<table class="table table-bordered">									
									
                                    <tbody><tr>
                                        <th style="text-align: center;">अ.क्र.&nbsp;</th>
                                        <th style="text-align: center;">विषय </th>
                                        <th style="text-align: center;">दिनांक</th>
                                        <th style="text-align: center;">संदर्भ </th>
                                        <th style="text-align: center;">Download</th>
                                    </tr>

                                    <tr>
                                        <td>1</td>
                                        <td>बोगस फॅकिंगला आळा घालण्यासाठी उपाय योजना</td>
                                        <td>1/27/2011</td>
                                        <td>का 5/बो.फ्रॅ.प्र.क्र./ 70/04/ 121/11</td>
                                        <td style="text-align: center">
                                            <a href="<?php echo base_url(); ?>pdf/1-Circular27Jan2011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>कमीर्शानीय किंमतीचे मुद्रांक उपलब्ध् होत नसले बाबत.</td>
                                        <td>9/15/2011</td>
                                        <td>का 5/मु.11/1299</td>
                                        <td style="text-align: center">
                                           <a href="<?php echo base_url(); ?>pdf/2-Circular201122.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </tbody></table>
									</div>
								</div>
								
							 </div>
							</div>
						 </div>
						 <div class="card">
							<div class="card-header" id="headingThree">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
								  Valuation of Property
								</button>
							  </h5>
							</div>
							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							  <div class="card-body">
								<p>Circulars issued by Inspector General of Regestration</p>
									
								<table class="table table-bordered">
									<tbody><tr>
										<th style="text-align: center;">अ.क्र.&nbsp;</th>
										<th style="text-align: center;">विषय </th>
										<th style="text-align: center;">दिनांक</th>
										<th style="text-align: center;">संदर्भ </th>
										<th style="text-align: center;">Download</th>
									</tr>

									
									<tr>
										<td>01</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label44">Values fixed by Government or Semi-Government bodies.</span></td>
										<!-- name -->
										<td>30/06/2005</td>
										<!-- date -->
										<td>Desk-15/ ASR/sale cert./ 372/2005 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_1.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>02</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label45">Procedure to be followed in respect of adjudication cases u/s 31 of the Maharashtra Stamp Act 1958.</span></td>
										<!-- name -->
										<td>20/10/2005</td>
										<!-- date -->
										<td>Desk-15/Adj/ Cr.No.9/583/ 2005 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->											
												<a href="<?php echo base_url(); ?>pdf/cir_val_2.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
									</tr>
									<tr>
										<td>03</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label46">Market value in respect of lease documents of plots alloted by Government or Semi-Government bodies</span></td>
										<!-- name -->
										<td>16/03/2006</td>
										<!-- date -->
										<td>Desk-15/Adj/ Cr.No.3/Lease/173/ 2005
										</td>
										<!-- ref no -->
										<td style="text-align: center">
											<a href="<?php echo base_url(); ?>pdf/cir_val_3.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>04</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label47">Valuation in respect of agriculture land having well / tubewell</span></td>
										<!-- name -->
										<td>02/05/2006</td>
										<!-- date -->
										<td>Desk-15/Adj/ Cr.No.1/valu.dedu/16/ 2006/281 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<a href="<?php echo base_url(); ?>pdf/cir_val_4.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>05</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label48">Valuation in respect of bulk land in Maharashtra excluding Mumbai.</span></td>
										<!-- name -->
										<td>29/03/2011</td>
										<!-- date -->
										<td>Desk-15/ASR-2011/Guidenine no.17/233 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_5.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>06</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label49">T.D.R.potential in respect of conveyance deed in favour of Housing Societies by the Developer</span></td>
										<!-- name -->
										<td>03/11/2012</td>
										<!-- date -->
										<td>Desk-15/Soci.dev. docu/TDR Potential/563 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<a href="<?php echo base_url(); ?>pdf/cir_val_6.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>07</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label50">Clarification in respect of guideline No.2.1 &amp; 2.2 applicable for Island City of Mumbai &amp; sub-urbs.</span></td>
										<!-- name -->
										<td>25/11/2013</td>
										<!-- date -->
										<td>Desk-15/ASR/Gen instruction/682 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_7.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>08</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label51">Guidelines &amp; new construction rates issued with Annual Statement of Rates-2014.</span></td>
										<!-- name -->
										<td>17/01/2014</td>
										<!-- date -->
										<td>Desk-15/ASR/Gen instruction/44 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											
												<a href="<?php echo base_url(); ?>pdf/cir_val_8.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
									</tr>
									<tr>
										<td>09</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label52">Points to be considered for valuation of deemed conveyance</span></td>
										<!-- name -->
										<td>04/04/2014</td>
										<!-- date -->
										<td>Desk-15/deemed conveyance/ Amenity Area/ val/288 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											
												<a href="<?php echo base_url(); ?>pdf/cir_val_9.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
									</tr>
									<tr>
										<td>10</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label53">Clarification in respect of guideline No.2.1 applicable for Island city of Mumbai &amp; sub-urbs.</span></td>
										<!-- name -->
										<td>28/08/2014</td>
										<!-- date -->
										<td>Desk-15/ASR/Gen instruction/665 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
																						
												<a href="<?php echo base_url(); ?>pdf/cir_val_10.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
									</tr>
									<tr>
										<td>11</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label54">Considering value of Fungible F.S.I. for valuation of Development Agreement documents in Mumbai city &amp; Sub-urban</span></td>
										<!-- name -->
										<td>1/11/2014</td>
										<!-- date -->
										<td>Desk-15/val/Adj cases/Fungible FSI /circular/822</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_11.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>12</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label58">Valuation in respect of lands purchased/transferred in favour of company/societies for commercial plantation of vegetables/ Horticulture/ Floriculture.</span></td>
										<!-- name -->
										<td>05/12/2014</td>
										<!-- date -->
										<td>ASR-2015/ circular/934 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_12.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>13</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label57">Rate to be considered for valuation of fallow land / non- cultivable land/ hilly fallow land.</span></td>
										<!-- name -->
										<td>05/12/2014</td>
										<!-- date -->
										<td>ASR-2015/ circular/935 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_13.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
									</tr>
									<tr>
										<td>14</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label56">Rates for missing C.T.S.No./S.No../Gat No./F.P. No. in Annual Statement of Rates.</span></td>
										<!-- name -->
										<td>5/12/2014</td>
										<!-- date -->
										<td>ASR-2015/ circular/936 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_14.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>15</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label55">Rate to be considered for determination of value / valuation in respect of C.T.S.No./ S.No./ G.No./F.P.No. falling under more than one value zone according to its merits/demerits in Annual Statement of Rates.</span></td>
										<!-- name -->
										<td>5/12/2014</td>
										<!-- date -->
										<td>ASR-2015/ circular/937 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_15.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>16</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label59">Items to be considered for valuation in respect  of slum Rehabilitation project in Mumbai city &amp; Suburbs.</span></td>
										<!-- name -->
										<td>26/12/2014</td>
										<!-- date -->
										<td>Desk-15/slum Re.dev/1016 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_16.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>17</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label60">Items to be considered for valuation in respect  of Redevelopment of Cessed Building Project in Island city of Mumbai.</span></td>
										<!-- name -->
										<td>26/12/2014</td>
										<!-- date -->
										<td>Desk-15/Sec-31/ Island city/1020 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_17.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>18</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label61">Valuation in respect of Flats / Units kept by Developer in accordance with the Development Agreement.</span></td>
										<!-- name -->
										<td>30/12/2014</td>
										<!-- date -->
										<td>Desk-15/ Dev. Purchase/1021 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_18.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>19</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label62">Procedure to be adopted in respect of valuation of documents received under section 31, 32A, 33 &amp; 33A.</span></td>
										<!-- name -->
										<td>31/12/2014</td>
										<!-- date -->
										<td>Desk-15/Sec-31, 32, 33/1023 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_19.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>20</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label63">Valuation in respect of Development Agreement where TDR is being used on Plots / lands.</span></td>
										<!-- name -->
										<td>31/12/2014</td>
										<!-- date -->
										<td>Desk-15/TDR/ 1022 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_20.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>21</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label64">Valuation in respect of Development Agreement constructed Built up area sharing &amp; Revenue sharing.</span></td>
										<!-- name -->
										<td>31/12/2014</td>
										<!-- date -->
										<td>Desk-15/ Dev. agree.Built-up area /1026 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_21.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>22</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label65">General guideline (Annexure A) &amp; Construction rates (Annexure B) for valuation</span></td>
										<!-- name -->
										<td>31/12/2014</td>
										<!-- date -->
										<td>Desk-15/ASR/Gen instruction/1027 </td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_22.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>23</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label66">Valuation Guideline No.2.2 for Mumbai &amp; Rest of Maharashtra.</span></td>
										<!-- name -->
										<td>07/02/2015</td>
										<!-- date -->
										<td>Guideline/2015/JDTP/Circular/124</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_23.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>24</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label67">Valuation Guideline No.17 – Valuation in respect of lands/ plots in separate value zone.</span></td>
										<!-- name -->
										<td>09/02/2015</td>
										<!-- date -->
										<td>Guideline/2015/JDTP/Circular/132</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
												<a href="<?php echo base_url(); ?>pdf/cir_val_24.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>25</td>
										<!-- sr no -->
										<td>सहकारी गृहन&zwj;िर्माण संस्थेच्या जुन्या इमारतीच्या पुनर्व&zwj;िकास प्रकल्पामध्ये नवीन इमारतीत सभासदांना जागा देताना करावयाचे मूल्यांकन</td>
										<!-- name -->
										<td>23/06/2015</td>
										<!-- date -->
										<td style="text-align: center">-</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_25.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>26</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_26">Valuation of area of building terrace utilized for installation of mobile tower.</span></td>
										<!-- name -->
										<td>10/07/2015</td>
										<!-- date -->
										<td>--</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
												<a href="<?php echo base_url(); ?>pdf/cir_val_26.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>27</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_27">Valuation in respect of development agreement for Redevelopment for MHADA building under section 31 of the Maharashtra Stamp Act 1958.</span></td>
										<!-- name -->
										<td>14/08/2015</td>
										<!-- date -->
										<td>--</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
													<a href="<?php echo base_url(); ?>pdf/cir_val_27.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>28</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_28">Clarification &amp; revision regarding Levy of Stamp Duty in respect of value determined by the Semi Government Bodies.</span></td>
										<!-- name -->
										<td>08/09/2015</td>
										<!-- date -->
										<td>--</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
														<a href="<?php echo base_url(); ?>pdf/cir_val_28.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>29</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_29">Revised procedure to be followed regarding valuation of document received under section 31, 32A, 33 &amp; 33A of Maharashtra Stamp Act 1958.</span></td>
										<!-- name -->
										<td>09/09/2015</td>
										<!-- date -->
										<td>--</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/cir_val_29.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>30</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_30">Valuation of College buildings.</span></td>
										<!-- name -->
										<td>28/09/2015</td>
										<!-- date -->
										<td>--</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
												<a href="<?php echo base_url(); ?>pdf/cir_val_30.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>31</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_31">Valuation of College buildings – Corrigendum.</span></td>
										<!-- name -->
										<td>05/10/2015</td>
										<!-- date -->
										<td>--</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
													<a href="<?php echo base_url(); ?>pdf/cir_val_31.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
									</tr>
									<tr>
										<td>32</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_32">इमारतीच्या पुर्वायुष्यानुसार घसारा वजावटीनंतरचे मुल्यांकनाबाबत</span></td>
										<!-- name -->
										<td>04/11/2015</td>
										<!-- date -->
										<td>--</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
													<a href="<?php echo base_url(); ?>pdf/cir_val_32.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>33</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_33">१२.0 मी व त्यापेक्षा जास्त रुंदीच्या एकापेक्षा जास्त रुंदीच्या रस्त्यास सन्मुख भूखंडाचे (corner plot) / जमिनीचे / corner वरील दुकानगाळयांचे मुल्यांकनाबाबत.</span></td>
										<!-- name -->
										<td>17/12/2015</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl">का.15/Corner Plot व दुकान/मुल्यांकन /परिपत्रक /1281</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val 33.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>34</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_34">भाडेकरू असलेल्या जुन्या इमारतींचे अभिहस्तांतरण (खरेदी दस्त) प्रकरणी मूल्यांकन.</span></td>
										<!-- name -->
										<td>31/12/2015</td>
										<!-- date -->
										<td>--</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir Val 34.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>35</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_35">भाडेकरू असलेल्या जुन्या इमारतींचे अभिहस्तांतरण (खरेदी दस्त) प्रकरणी मूल्यांकन.</span></td>
										<!-- name -->
										<td>31/12/2015</td>
										<!-- date -->
										<td>--</td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir Val 35.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>36</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label18">Annual Statements of Rates</span></td>
										<!-- name -->
										<td>31/12/2015</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label23">का.15/वामुदत-2016/1340</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir Val 36.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>37</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label19">General valuation guidelines (Annexure A) &amp; construction rates (Annexure B) for valuation</span></td>
										<!-- name -->
										<td>31/12/2015</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label22">का.15/वामुद/सर्वसाधारण सुचना/परीपत्रक/1341</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir Val 37.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>38</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label20">Applicability of updated &amp; revised general valuation guidelines (Annexure A) for valuation according to ASR.</span></td>
										<!-- name -->
										<td>01/01/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label21">का.15/सर्वसाधारण सुचना/मूल्यांकन/परीपत्रक/1</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir Val 38.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>39</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl39">Valuation of College buildings – Corrigendum.</span></td>
										<!-- name -->
										<td>30/01/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label25">का.15/महाविद्यालय इमारत/ मूल्यांकन/शुध्दीपत्रक/69</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val-39.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>40</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl40">Valuation in respect of terrace area for installation of Mobile Tower</span></td>
										<!-- name -->
										<td>26/02/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label26">का.15/मोबाईल टॉवर/ मूल्यांकन/परिपत्रक/178</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_val_40.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>41</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl41">Sanction to the Annual Statements of Rates of Immovable properties for year 2016-17</span></td>
										<!-- name -->
										<td>31/03/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label27">का.15/वामूदत-2016-17/307</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
										<a href="<?php echo base_url(); ?>pdf/Cir_val_41.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>42</td>
										<!-- sr no -->
										<td>
											<!-- valuation circular for mumbai 2016 -->
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl42">General Guidelines for valuation (Annexure-A) &amp; New construction rates (Annexure-B) for year 2016-17 (Mumbai)</span></td>
										<!-- name -->
										<td>31/03/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label29">का.15/वामूद/सर्वसाधारणसूचना/परिपत्रक/301</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/ASR Guidelines construction rates of mumbai for year 2016-17.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>43</td>
										<!-- sr no -->
										<td>
											<!-- valuation circular for rest of maharashtra 2016 -->
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl43">General Guidelines for valuation (Annexure-A) &amp; New construction rates (Annexure-B) for year 2016-17 (Rest of Maharashtra)</span></td>
										<!-- name -->
										<td>31/03/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label28">का.15/वामूद/सर्वसाधारणसूचना/परिपत्रक/301</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/rest_maha_2016.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>44</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl44">Valuation in respect of land purchased by Govt. approved Educational institutions for agricultural college.</span></td>
										<!-- name -->
										<td>31/03/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label32">का.15/वामूदत-2016-17/ मार्गदर्शक सूचना/परिपत्रक/304</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_val_43.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>45</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl45">Valuation in respect of Highway Frontage land in Rural area (National &amp; State High way)</span></td>
										<!-- name -->
										<td>05/04/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label30">का.15/हायवे सन्मुख जमिनी/ मूल्यांकन/परिपत्रक/348</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/circular_no_45.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>46</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl46">Guidelines (Annexure-A) &amp; New construction rates (Annexure-B) for Annual Statement of Rates 2016-17</span></td>
										<!-- name -->
										<td>27/04/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label33">का.15/पालघर जिल्हा/मनपा/ परिशिष्ट-ब/शुध्दीपत्रक/397</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_val_46.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
									</tr>

									<tr>
										<td>47</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl47">Valuation with 1 FSI in Mumbai City as per Annual Statement of Rates</span></td>
										<!-- name -->
										<td>23/05/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label34">का.15/मुंबई शहर/च.क्षे.नि./ दर/मूल्यांकन/परिपत्रक/512</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_47.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
									</tr>

									<tr>
										<td>48</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl48">Valuation in respect of land purchased / sell for well</span></td>
										<!-- name -->
										<td>27/05/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label35">का.15/विहिर/मूल्यांकन/परिपत्रक/539</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_val_48.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>49</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl49">To provide copy of extract of Annual Statement of Rates to the citizens by Sub Registrars.</span></td>
										<!-- name -->
										<td>13/06/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label36">का.15/अधिप्रमाणित प्रत/ परिपत्रक/580</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_49.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									
									<tr>
										<td>50</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl50">Valuation in respect of buildings other than R.C.C. structure and Annual Statements of Rates</span></td>
										<!-- name -->
										<td>20/08/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label37">का.15/वामुदत दर/ परिपत्रक/804</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_No.50_0001.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>51</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl51">Guideline No.17
			Valuation in respect of lands / plots in separate road value zone.</span></td>
										<!-- name -->
										<td>14/10/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label38">का.15/मा.सू./उर्व. महाराष्ट्र/शुध्दीपत्रक/965</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_51.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>52</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl52">Guideline No.3 applicable for Mumbai (Island City &amp; Sub-urban area)
			Valuation of land having T.D.R. potential</span></td>
										<!-- name -->
										<td>17/12/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label39">का.15/मुंबई शहर व उपनगर/मार्ग.सू.क्र./ टी.डी.आर./परिपत्रक/ 1186</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
												<a href="<?php echo base_url(); ?>pdf/Cir_Val_52.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>53</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl53">Facts &amp; Discription mentioned in document to be taken into account while valuing &amp; charging stamp duty.</span></td>
										<!-- name -->
										<td>17/12/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label40">का.15/दस्त/मु.शु. /परिपत्रक/1187</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_53.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>54</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl54">Valuation of buildable reservation having T.D.R. potential</span></td>
										<!-- name -->
										<td>23/12/2016</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label41">का.15/मुंबई शहर व उपनगर/आरक्षण/ टी.डी.आर./परिपत्रक/ 1234</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_54.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>55</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl55">Valuation in respect of conveyance of land having  Government interest.</span></td>
										<!-- name -->
										<td>07/01/2017</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label42">का.15/अनर्जित रक्कम/ मूल्यांकन/परिपत्रक/41</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
												<a href="<?php echo base_url(); ?>pdf/Cir_Val_55.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>56</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl56">Sanctioned Order of ASR 2017-18</span></td>
										<!-- name -->
										<td>31/03/2017</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label69">का.15/वामूदत 2017-18/414</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
												<a href="<?php echo base_url(); ?>pdf/ASR%20-%202017-18%20CIRCULAR_56.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>57</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lbl57">ASR 2017-18 Valuation Guidelines for year Revised Schedule B for Rest of Maharashtra</span></td>
										<!-- name -->
										<td>03/04/2017</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label71">का.15/सर्वसाधारण मार्गदर्शक सूचना/मूल्यांकन/परिपत्रक/431</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/c.rates%20new.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>58</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_58">ASR 2017-18 Valuation of immovable property.</span></td>
										<!-- name -->
										<td>19/05/2017</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label70">का.15/वामुदत-2017-18/574</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/ASR-2017-18StayOrderCir-58.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>59</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_59">Regarding Annual Valuation of Real Estate for 2017-2018</span></td>
										<!-- name -->
										<td>19/06/2017</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label72">जा . क्र . का . १५/ मामूदात - २०१७-१८/६६१</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_No_59.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
											
										</td>
									</tr>

									<tr>
										<td>60</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_60">Guideline No.24 for Mumbai &amp;  Guide no.33 for Rest of Maharashtra Valuation in respect of Development Agreement, Revenue Sharing</span></td>
										<!-- name -->
										<td>26/07/2017</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label74">का.15/परिपत्रक/वि.क.दस्त/ मूल्यांकन/774</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_No.60.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>61</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_61">ASR 2017-18 Valuation of immovable property.</span></td>
										<!-- name -->
										<td>19/08/2017</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label76">का.15/वामुदत-2017-18/851</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Stay%20Order.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>62</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_62">Land rates for lands under acquisition for big projects undertaken by Maharashtra Industrial Development Corporation and other Government / Semi Government Authority / corporatrions</span></td>
										<!-- name -->
										<td>18/10/2017</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label73">जा.क्र.का.15/वामुदत/संपादन/ 1011</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_62.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>63</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_63">Details of S.No./Gat no. abutting highways in Rural &amp; Influence towns in Maharashtra.</span></td>
										<!-- name -->
										<td>16/11/2017</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label75">का-15/परिपत्रक/मूल्यांकन/1072</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
															<a href="<?php echo base_url(); ?>pdf/Cir_Val_63.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
									</tr>
									<tr>
										<td>64</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_64">Calculation of Buitup area Considering Rera Act</span></td>
										<!-- name -->
										<td>02/01/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_Label77">का-15/परिपत्रक/रेरा/कार्पेट एरिया/3</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
															<a href="<?php echo base_url(); ?>pdf/Cir_Val_64.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>65</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_65">Guidelines for Mumbai Corporation Area -
			Valuation of Conveyance in respect of  old tenanted Building.</span></td>
										<!-- name -->
										<td>31/03/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_65_sandarbha">जा.क्र. का-15/परिपत्रक/ मुंबई/ भाडेकरु/मूल्यांकन/347</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
															<a href="<?php echo base_url(); ?>pdf/Cir_Val_65.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>66</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_66">Guidelines No.21 for Mumbai &amp; No.28 for Rest of Maharashtra - Valuation of Development Agreement of Redevelopement of old buildings of Housing society.</span></td>
										<!-- name -->
										<td>31/03/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_66_sandarbha">जा.क्र. का-15/परिपत्रक/ मूल्यांकन/351</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
										<a href="<?php echo base_url(); ?>pdf/Cir_Val_66.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>
									<tr>
										<td>67</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_67">Annual Statement of Rates for Financial year 2018-19</span></td>
										<!-- name -->
										<td>31/03/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_67_sandarbha">जा.क्र. का-15/परिपत्रक/ मूल्यांकन/353</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_67.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>68</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_68">Valuation of developed plots of forest house</span></td>
										<!-- name -->
										<td>29/05/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_68_sandarbha">जा.क्र. का-15/वनघर/मूल्यांकन दर/532</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_68.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>69</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_69">Guidelines for rest of Maharashtra.

			Valuation of land in Joint Development Agreement in Integrated Township Project</span></td>
										<!-- name -->
										<td>30/06/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_69_sandarbha">जा.क्र. का-15/परिपत्रक/विशेष नगर वसाहत/628</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_69.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>70</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_70">Land rate in respect of Biodiversity Park reservation having Hill top / Hill slope character situated within Pune Municipal Corporation area.</span></td>
										<!-- name -->
										<td>02/08/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_70_sandarbha">जा.क्र. का-15/परिपत्रक/ डोंगरमाथा-डोंगरउतारा/742</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_70.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>71</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_71">म्हाडा इमारतींचा पुनर्विकास प्रकल्प विकसन करारनामा दस्त नोंदणी / अभिनिर्णय प्रकरणी करावयाचे मुल्यांकन.</span></td>
										<!-- name -->
										<td>12/11/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_71_sandarbha">जा.क्र. का.15/परिपत्रक/म्हाडा पुनर्विकास प्रकल्प /1023</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_71.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>72</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_72">बृहन्मुंबई महानगरपालिका क्षेत्रासाठीची सर्वसाधारण मार्गदर्शक सूचना क्र.1 व 2 भाडेकरू असलेल्या जुन्या इमारतीचे खरेदी दस्त / पुनर्विकास प्रकल्प विकसन करारनामा मुल्यांकनाबाबत</span></td>
										<!-- name -->
										<td>12/11/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_72_sandarbha">जा.क्र. का.15/परिपत्रक/मुंबई/भाडेकरू/मुल्यांकन/1024</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_72.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>73</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_73">मुंबई शहरासाठीची मार्गदर्शक सूचना क्र. 17 स्थूल जमिनीचे मुल्यांकनाबाबत</span></td>
										<!-- name -->
										<td>12/11/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_73_sandarbha">जा.क्र. का.15/परिपत्रक/मुंबई शहर/सू.क्र.17/1025</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_73.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>74</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_74">उर्वरित महाराष्ट्रासाठी लागू असलेल्यामार्गदर्शक सूचना विशेषनगर वसाहतक्षेत्राखालील जमिनींच्या संयुक्त विकसनकरारनामा दस्ताचे मुल्यांकनाबाबत</span></td>
										<!-- name -->
										<td>12/11/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_74_sandarbha">परिपत्रक क्र.क्रा. 15/परिपत्रक/विशेष नगर वसाहत/628, दि. 30/6/2018</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_74.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

									<tr>
										<td>75</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_75">Valuation in respect of land purchased by Institution / Company (other than Agruculturist) for other residential / commercial / Industrial purpose &amp; transfer / sale to other Agriculturist in rural &amp; influence area.</span></td>
										<!-- name -->
										<td>17/12/2018</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_lblCirc_75_sandarbha">जा.क्र. का-15/परिपत्रक/ कंपनी /1122</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Cir_Val_75.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>


									<tr>
										<td>76</td>
										<!-- sr no -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_LabelCirc76">Annual Statement of Rates for Financial year 2019-20</span></td>
										<!-- name -->
										<td>01/04/2019</td>
										<!-- date -->
										<td>
											<span id="ContentPlaceHolder1_ContentPlaceHolder1_LabelSand76">जा.क्र. का-15/वामुदत 2019-20/ 355</span></td>
										<!-- ref no -->
										<td style="text-align: center">
											<!-- link -->
											<a href="<?php echo base_url(); ?>pdf/Val_Cir_76.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
											
										</td>
									</tr>

								</tbody></table>
									
								<br />
								<p><b>Circulars issued by Govt. for valuation</b></p>
								<p><a href="<?php echo base_url(); ?>pdf/shasanParipatrak.pdf" target="_blank">1. शासकीय जमिनीचे वार्षिक मूल्य दर तक्त्यानुसार मूल्यांकन करणेबाबत</a></p>
								<p><a href="<?php echo base_url(); ?>pdf/Circular-Valuation-30-01-2014.pdf" target="_blank">2. शासकीय जमीनीचा आगाऊ ताबा दिल्यानंतर कब्जेहक्काच्या किंमतीवर आकारावयाच्या व्याजाच्या दराबाबत व भाडेपटयाने द्यावयाच्या जमिनीच्या वार्षिक भाडेपटयाच्या दरानुषंगाने स्टेट बँक आँफ इंडिया चा “पी.एल.आर. (एसबीएआर/बेस रेट)” दराबाबत.(30-01-2014)</a></p>								
							 </div>
							</div>
						 </div>
						<div class="card">
							<div class="card-header" id="headingFour">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
								  Marriage
								</button>
							  </h5>
							</div>
							<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>pdf/ClerificationOfCircularOfDivorce_11-09-2009.pdf" target="_blank">Clarification of Divorce circular 11.09.2009</a></p>
								<p><a href="<?php echo base_url(); ?>pdf/CircularOfDivorceDoc_13-12-2004.pdf" target="_blank">Circular of Divorce dated 13.12.2004</a></p>	
								<p><a href="<?php echo base_url(); ?>" target="_blank">नोंदणी व मुद्रांक विभागाच्या विशेष विवाह नोंदणी संदर्भातील वृत्त</a></p>		
							 </div>
							</div>
						 </div>
						<div class="card">
							<div class="card-header" id="headingFive">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
								  Other
								</button>
							  </h5>
							</div>
							<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
							  <div class="card-body">
								<table class="table table-bordered">
		<tbody><tr style="color:White;background-color:#333333;font-weight:bold;">
			<th class="text-center" scope="col">Sr. No</th><th class="text-center" scope="col">Date</th><th class="text-center" scope="col">Subject</th><th scope="col" abbr="Download">Download</th>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            1
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_0" class="text-center" style="font-size:Small;">06-02-2019</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_0" class="text-center" style="font-size:Small;">Government of Maharashtra Revenue and Forest Department: letter no. Establishment-2019/3978 /Proc.02 /M1-Dated:06.02.19</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/takrar_14feb2019.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            2
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_1" class="text-center" style="font-size:Small;">26-10-2018</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_1" class="text-center" style="font-size:Small;">सर्व प्रकारचे विकसन करार / विकसन करारनाम्याचे हक्काच्या हस्तांतरणाचे दस्त, विकसन करारनाम्याचे पुरवणी करारनांमे</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Deed_of_Assignment.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            3
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_2" class="text-center" style="font-size:Small;">30-01-2017</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_2" class="text-center" style="font-size:Small;">पदनामाबाबत</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Designation.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            4
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_3" class="text-center" style="font-size:Small;">24-11-2014</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_3" class="text-center" style="font-size:Small;">तात्काळ तपासणीचे परीपत्रक.</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/tatkal%20tpasni02222016180433.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            5
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_4" class="text-center" style="font-size:Small;">14-07-2014</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_4" class="text-center" style="font-size:Small;">अधिसूचना नोंदणी अधीनियम 1908</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/CIRCULAR.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            6
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_5" class="text-center" style="font-size:Small;">09-05-2014</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_5" class="text-center" style="font-size:Small;">Circular reg court case of Prabha Ghate</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Circular%20reg%20court%20case%20of%20Prabha%20Ghate.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            7
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_6" class="text-center" style="font-size:Small;">01-04-2014</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_6" class="text-center" style="font-size:Small;">Circular Input Form </span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/circular%20input%20form.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            8
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_7" class="text-center" style="font-size:Small;">26-03-2014</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_7" class="text-center" style="font-size:Small;">Deemed Conveyance Circular</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Deem%20Convence%20Circular.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            9
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_8" class="text-center" style="font-size:Small;">15-03-2014</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_8" class="text-center" style="font-size:Small;">Franking ammendment above RS 5000</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Franking%20ammendment.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            10
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_9" class="text-center" style="font-size:Small;">03-03-2014</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_9" class="text-center" style="font-size:Small;">Circular Regarding Refund Of ePayment</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Refund_ePayment.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            11
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_10" class="text-center" style="font-size:Small;">17-01-2014</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_10" class="text-center" style="font-size:Small;">वार्षिक मुल्यदर तक्त्यासोबतच्या सन 2014 च्या मार्गदर्शक सूचना व नवीन बांदकांम दराबाबत</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Circular.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            12
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_11" class="text-center" style="font-size:Small;">17-12-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_11" class="text-center" style="font-size:Small;">शासनाच्या म्ह्सुलीच्या गळती रोखणे व मुद्रांकाच्या गैरवेहवारास प्रतिबांध घालण्याबाबत</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/circular17122013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            13
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_12" class="text-center" style="font-size:Small;">09-12-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_12" class="text-center" style="font-size:Small;">Using Bar - code before handing over document for scanning</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/scanning%20document.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            14
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_13" class="text-center" style="font-size:Small;">09-12-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_13" class="text-center" style="font-size:Small;">Annexure Regarding registration of confirmation deed and declaration deed</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/circular%20confirmation%20deed.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            15
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_14" class="text-center" style="font-size:Small;">05-12-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_14" class="text-center" style="font-size:Small;">Use of option view 7/12 in iSARITA</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/7_12_circular.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            16
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_15" class="text-center" style="font-size:Small;">30-11-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_15" class="text-center" style="font-size:Small;">Regarding registration of confirmation deed and declaration deed</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/desk_4_circular-A.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            17
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_16" class="text-center" style="font-size:Small;">30-11-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_16" class="text-center" style="font-size:Small;">implementation of section 82 of the registration act 1908</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/desk_4_circular%20b.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            18
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_17" class="text-center" style="font-size:Small;">25-11-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_17" class="text-center" style="font-size:Small;">मुंबई शहर व उपनगरकरिता लागू असलेली मार्गदर्शक सूचना क्र2(2.1व2.2)चे स्पष्टीकरना बाबत</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/scanning%20_002.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            19
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_18" class="text-center" style="font-size:Small;">07-11-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_18" class="text-center" style="font-size:Small;">LBT Circular</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/Local%20Tax%20lbt.doc.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            20
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_19" class="text-center" style="font-size:Small;">07-11-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_19" class="text-center" style="font-size:Small;">मुंबई शहर व उपनगरकरिता लागू असलेली मार्गदर्शक सूचना क्र2(2.1व2.2)चे स्पष्टीकरना बाबत</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/scanning%20_001.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            21
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_20" class="text-center" style="font-size:Small;">06-10-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_20" class="text-center" style="font-size:Small;">Payment of StampDuty and Registration Fee using GRAS eChallan or eSBTR</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/e-payment%20circular%20No.%20PK-43-11-699%20dated%2005.10.2013.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            22
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_21" class="text-center" style="font-size:Small;">06-10-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_21" class="text-center" style="font-size:Small;">ePayment using GRAS Annexure1</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Annexures%20to%20ePayment%20circular.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            23
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_22" class="text-center" style="font-size:Small;">05-09-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_22" class="text-center" style="font-size:Small;">Clarification to the instructions regarding identification</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/id.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            24
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_23" class="text-center" style="font-size:Small;">23-08-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_23" class="text-center" style="font-size:Small;">Precaution at the time of Admission and Identification</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Circular%20on%20Identification.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            25
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_24" class="text-center" style="font-size:Small;">12-06-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_24" class="text-center" style="font-size:Small;">Circular About Audio Video Shooting Of  Home Visit Proceedings</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/circularforHomeVisit.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            26
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_25" class="text-center" style="font-size:Small;">14-05-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_25" class="text-center" style="font-size:Small;">Scanning Old Document related</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/scanning%2054%20office.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            27
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_26" class="text-center" style="font-size:Small;">25-04-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_26" class="text-center" style="font-size:Small;">Circular for filing of Notice of intimation under sec. 89B in SR Offices - physically</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/89-b.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            28
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_27" class="text-center" style="font-size:Small;">28-03-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_27" class="text-center" style="font-size:Small;">Order of designated offices for filing of Notices of Intimation</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/designated%20Sub%20Regsistrar%20offices%20for%20filing.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            29
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_28" class="text-center" style="font-size:Small;">25-03-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_28" class="text-center" style="font-size:Small;">Regarding development agreement or  POA of Tribal Land</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/APPOINTMENTApplication%20Form.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            30
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_29" class="text-center" style="font-size:Small;">25-03-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_29" class="text-center" style="font-size:Small;">नोंदणी  (महाराष्ट्र) सुधारणा कायदा २०१०</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/1908%20calam%2017%20cir.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            31
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_30" class="text-center" style="font-size:Small;">04-03-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_30" class="text-center" style="font-size:Small;">eStamp Circular</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/estampcircular.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            32
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_31" class="text-center" style="font-size:Small;">28-02-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_31" class="text-center" style="font-size:Small;">Sub Registrar Type Plan</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/SROTYPE.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            33
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_32" class="text-center" style="font-size:Small;">10-01-2013</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_32" class="text-center" style="font-size:Small;">regarding precaution while Data Entry and Scanning of Document</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Circular10.1.13.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            34
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_33" class="text-center" style="font-size:Small;">04-10-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_33" class="text-center" style="font-size:Small;">Minutes of meeting</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
											<a href="<?php echo base_url(); ?>pdf/sushobhikaran.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            35
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_34" class="text-center" style="font-size:Small;">04-10-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_34" class="text-center" style="font-size:Small;">अभ्यास गट नेमण्याबाबत.</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/abhyasgat.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            36
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_35" class="text-center" style="font-size:Small;">04-10-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_35" class="text-center" style="font-size:Small;">isarita dataentry correction related </span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/dataentry.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            37
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_36" class="text-center" style="font-size:Small;">26-07-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_36" class="text-center" style="font-size:Small;">iSarita Implementation and Change in Work Process Regarding</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/I%20Sarita%20circular.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            38
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_37" class="text-center" style="font-size:Small;">08-07-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_37" class="text-center" style="font-size:Small;">Project Take Over Form</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/take-over%20form.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            39
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_38" class="text-center" style="font-size:Small;">08-07-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_38" class="text-center" style="font-size:Small;">Project Take Over Form</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/take-over%20form.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            40
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_39" class="text-center" style="font-size:Small;">30-06-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_39" class="text-center" style="font-size:Small;">Admin approval of New Construction </span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/Admin%20Approval%20of%20new%20const.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            41
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_40" class="text-center" style="font-size:Small;">27-06-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_40" class="text-center" style="font-size:Small;">Circular of Sushobhikaran</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/ka-13%20paripatrak.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            42
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_41" class="text-center" style="font-size:Small;">13-06-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_41" class="text-center" style="font-size:Small;">Tersting</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/cardboard-house.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            43
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_42" class="text-center" style="font-size:Small;">28-05-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_42" class="text-center" style="font-size:Small;">Regarding Registration of Confirmation Deed or Declaration</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Confermation%20Deed%20Circuler-1.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            44
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_43" class="text-center" style="font-size:Small;">08-05-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_43" class="text-center" style="font-size:Small;">MIDC - Related to Lease Document</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/MIDC%20Cricular%2008-May-2012.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            45
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_44" class="text-center" style="font-size:Small;">26-04-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_44" class="text-center" style="font-size:Small;">List of MKCL Franchises </span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Centers%20registered%20for%20Barcode%20project_tehsilwise.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            46
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_45" class="text-center" style="font-size:Small;">20-04-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_45" class="text-center" style="font-size:Small;">Sim Card Number List</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/sim%20no.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            47
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_46" class="text-center" style="font-size:Small;">18-04-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_46" class="text-center" style="font-size:Small;">S.R.Gr.1 Selection list</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/Sr%20Gr%201%20Selection%20list.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            48
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_47" class="text-center" style="font-size:Small;">12-04-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_47" class="text-center" style="font-size:Small;">Deemed Conveyance Document Registration</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Deemed%20convence.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            49
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_48" class="text-center" style="font-size:Small;">16-03-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_48" class="text-center" style="font-size:Small;">Requisition of land for Sub Registrar Office</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Letter%20from%20Divn%20Commr%20regarding%20%20Govt%20Land.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            50
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_49" class="text-center" style="font-size:Small;">01-03-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_49" class="text-center" style="font-size:Small;">Internal Audit Circular</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/InternalAudit%20Circular.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            51
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_50" class="text-center" style="font-size:Small;">21-01-2012</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_50" class="text-center" style="font-size:Small;">Willingness to participate in study group</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/study%20group.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            52
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_51" class="text-center" style="font-size:Small;">22-12-2011</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_51" class="text-center" style="font-size:Small;">Regarding Registration of Confirmation Deed/Declaration</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/D-4Circularofdt-22122011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            53
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_52" class="text-center" style="font-size:Small;">09-12-2011</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_52" class="text-center" style="font-size:Small;">Electrification in Govt. Premises</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/letter%20rega%20govt%20premi-1.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            54
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_53" class="text-center" style="font-size:Small;">09-12-2011</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_53" class="text-center" style="font-size:Small;">Model Estimates for Electrification in Govt. Premises</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										
										<a href="<?php echo base_url(); ?>pdf/Esteemate%20Government.PDF" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            55
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_54" class="text-center" style="font-size:Small;">09-12-2011</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_54" class="text-center" style="font-size:Small;">Electrification in Private Premises</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/letter%20rega%20prvt%20%20premi.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            56
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_55" class="text-center" style="font-size:Small;">09-12-2011</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_55" class="text-center" style="font-size:Small;">Model Estimates for Electrification in Private Premises</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Esteemate%20Private.PDF" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            57
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_56" class="text-center" style="font-size:Small;">02-09-2011</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_56" class="text-center" style="font-size:Small;">Distribution of Hardware</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/hardware%20distribution.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            58
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_57" class="text-center" style="font-size:Small;">02-09-2011</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_57" class="text-center" style="font-size:Small;">SARITA-3 Implementation</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										
										<a href="<?php echo base_url(); ?>pdf/circular%20for%20pdf%20revised.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            59
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_58" class="text-center" style="font-size:Small;">17-08-2011</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_58" class="text-center" style="font-size:Small;">Result of the Departmental Exam 2011 </span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/Exam%20Result%202011.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										
										</td>
		</tr><tr>
			<td class="text-center" align="center" valign="middle" style="font-size:Small;width:45px;">
                                            60
                                        </td><td class="text-center" align="center" valign="middle" style="font-size:Small;width:110px;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_59" class="text-center" style="font-size:Small;">04-05-2011</span>
                                        </td><td class="text-left" align="justify" valign="middle" style="font-size:Small;">
                                            <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_59" class="text-center" style="font-size:Small;">DIG,JDR,SR office revised office space norms</span>
                                        </td><td class="text-center" align="center" valign="middle" style="width:55px;">
										<a href="<?php echo base_url(); ?>pdf/DIG%20JDR%20SR%20office%20revised%20space%20norms.pdf" target="_blank">
												<i class="fa fa-download"></i></a>
										</td>
		</tr>
	</tbody></table>
	
									
							 </div>
							</div>
						 </div> 
						<div class="card">
							<div class="card-header" id="headingSix">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
								  EoDB
								</button>
							  </h5>
							</div>
							<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>pdf/01-TDS_not_mandatory.pdf" target="_blank">TDS not mandatory for Registration [18-Dec-17]</a></p>	
							 </div>
							</div>
						 </div> 						 
					</div>
                </div>
            </div>
        </div>
    </section>
<script>
//inner-click
$( document ).ready(function() {
   $(".data-contents").addClass('hide');
});
$("a.inner-click").click(function(e) {
    e.preventDefault();	
	$('.main-p').find(".data-contents").addClass('hide');
	$('.main-p').find(".data-contents").removeClass('show');
	$(this).closest('.main-p').find(".data-contents").addClass('show');	
});
</script>




