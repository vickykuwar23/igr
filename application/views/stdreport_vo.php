<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<!-- Latest compiled and minified CSS -->
<!-- jQuery library -->
<!-- Latest compiled JavaScript -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- basic libraries required for datatable Funcyions include : search,pagination,no of records -->
<script src="//cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css" >
<link rel="stylesheet" href="<?php echo base_url();?>js/datatables/bootstrap.min.css">

<!-- datatable buttons-->
<script src="https://cdn.datatables.net/buttons/1.0.1/js/dataTables.buttons.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.0.1/css/buttons.dataTables.min.css" >
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.colvis.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.html5.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.flash.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>js/datepicker/datepicker.js"></script>
<!--Responsive table-->
<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.min.css" >
<script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.bootstrap.min.css" >-->

<!--Which column should display-->
<script src="//cdn.datatables.net/buttons/1.0.1/js/buttons.colVis.min.js"></script>

<script>
   $(function() {
 	 $( "#datepickerFrom,#datepickerTo" ).datepicker({
       dateFormat: 'd M, yy',
	   defaultDate: "+1w",
       changeMonth: true,
 	   changeYear: true,
       numberOfMonths: 1
     });
   });
   </script>
   
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.2.0/css/colReorder.dataTables.min.css" >
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
</head>
<body>
<div class="row">      
  <div class="col-md-4 col-md-offset-4 m-t-large">  
    <form method="post">
      <div class="form-group clearfix">         
        <label class="col-md-6">Grievance Status : </label>  
        <div class="col-md-6">
          <select class="form-control"  name="grievance_type" id="grievance_type" style="background-color:#dbf4ff;">
          <option value="" <?php if($type=='1'){echo "selected='selected'";}?>>Select</option>
          <option value="2" <?php if($type=='2'){echo "selected='selected'";}?>>Closed</option>
          <option value="3" <?php if($type=='3'){echo "selected='selected'";}?>>Open</option>
          </select>
        </div>
      </div><!--./form-group-->
    
      <div class="form-group clearfix">
        <label class="col-md-6">Grievance Category : </label>  
        <div class="col-md-6">
          <select class="form-control"  name="grievance_cat" id="grievance_cat" style="background-color:#dbf4ff;">
            <option value="" <?php if($cat==''){echo "selected='selected'";}?>>Select</option>
            <option value="1" <?php if($cat=='1'){echo "selected='selected'";}?>>Business Client</option>
            <option value="2" <?php if($cat=='2'){echo "selected='selected'";}?>>Share Holder</option>
            <option value="3" <?php if($cat=='3'){echo "selected='selected'";}?>>Investor</option>
            <option value="4" <?php if($cat=='4'){echo "selected='selected'";}?>>Bond Holder</option>
            <option value="5" <?php if($cat=='5'){echo "selected='selected'";}?>>Ex Employee</option>
            <option value="6" <?php if($cat=='6'){echo "selected='selected'";}?>>Others</option>
          </select>
        </div>
      </div><!--./form-group-->
      
    <div class="form-group clearfix">
    <div class="col-md-offset-6 col-md-6">
    <input  type="text" name="from_date" id="datepickerFrom" class="datepick form-control" data-rule-required="true" value="<?php if($searchfrom!=''){echo $searchfrom;}?>" 
    data-msg-required="Please select from date" placeholder="Please select from date">
    
    
    <input  type="text" name="to_date" id="datepickerTo" class="datepick form-control m-t" data-rule-required="true" value="<?php if($searchto!=''){echo $searchto;}?>" 
    data-msg-required="Please select from date" placeholder="Please select to date">
    </div>
    </div>
    
    <div class="form-group clearfix no-mar">
    <div class="col-md-6 col-md-offset-3">
    <input type="submit" class="btn btn-success no-shadow" name="submitform" value="Submit" style="border-radius:4px !important;" />
    <a href="<?php echo base_url();?>index.php/report/generaterepo/" class="btn btn-default no-shadow">Reset</a>
    </div>
    </div>
    
    </form>
  </div>
</div><!--./row-->
<table class="table table-striped table-bordered dt-responsive nowrap" id="table">
									<thead>
									<tr>
										<th class="text-center bluehd">
											Sr. No. 
										</th>
                                          <th class="text-center bluehd">
											Grievance ID
										</th>
                                         <th class="text-center bluehd">
											Complainant </br>name
										</th>
										<th class="text-center bluehd">
											Mobile No.
										</th>
										<th class="text-center bluehd">
											Grievance Type
										</th>
										<th class="text-center bluehd">
										Complaint Date
										</th>
                                        <th class="text-center bluehd">
										 State
										</th>
                                         <th class="text-center bluehd">
										 city
										</th>
                                         <th class="text-center bluehd">
										 Grievance Status
										</th>
							</tr>
									</thead>
									<tbody>
							<?php if(count($complaint) > 0){
								 $srno=1;
								foreach($complaint as $res)
								{
									$username=$this->master_model->getRecords('userregistration',array('user_id'=>$res['user_id']));
									//$type=$this->master_model->getRecords('usercomplainbox');
									?>	
                            	    	<tr>
								    	<td class="text-center">
										<?php echo $srno?>
										</td>
                                        <td class="text-left">
										<?php echo $res['pattern'];?>
										</td>
                                        <td class="text-left">
										<?php echo ucfirst($username[0]['user_name']);?>
										</td>
										<td>
										<?php echo $username[0]['user_mobile'];?>
                                        </td>
                                        <td  class="text-center">
											<?php
											$categoty=$this->master_model->getRecords('departments',array('id'=>$res['category']));	
											 echo ucfirst($categoty[0]['type']);?>
										</td>
										<td class="text-center">
											<?php echo date('d M Y',strtotime($res['registerdate']));?>	
										</td>
                                       <?php  $state=$this->master_model->getRecords('states',array('ch2'=>$username[0]['user_state']));?>
                                       <td>
										<?php if($state[0]['ch3']){echo $state[0]['ch3'];}?>
                                        </td>
                                        <td>
										<?php echo $username[0]['user_city'];?>
                                        </td>
                                       <td class="text-center"><?php if($res['reply_status']==0) {echo 'Closed' ;}else{ echo 'Open';}?></td>
									</tr>
								<?php 
							$srno++;	}
							}?>
									</tbody>
</table>
</body>

<script>
$(document).ready(function(){
    $('#table').DataTable({
		dom: 'Bfrtip', //to show the buttons on the screen
         buttons: [  
		 'print',
		 'excel',
		 'pdf',
		 'colvis'
        ]
	});
});

</script>

</html>