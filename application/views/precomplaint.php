
<div class="container">
<div class="datatable table-responsive">
<table class="table table-striped table-bordered dt-responsive nowrap" id="table">
							<thead>
									<tr>
										<th class="text-center bluehd">
											Sr. No.
										</th>
                                        <th class="text-center bluehd">
											Grievance ID
										</th>
                                         <th class="text-center bluehd">
											Complainant </br>name
										</th>
                                        
                                        <th class="text-center bluehd">
											Complaint </br>Message
										</th>
										<th class="text-center bluehd">
											Complaint</br> date
										</th>
										<th class="text-center bluehd">
											Category
										</th>
										<th class="text-center bluehd">
											Reply Status
										</th>
                                        <th class="text-center bluehd">
											Grievance Status
										</th>
										<th class="text-center bluehd">
											Replied by
										</th>
									</tr>
									</thead>
									<tbody>
								  <?php $sr=0;?>
								  <?php foreach($result as $details) {
									  $username=$this->master_model->getRecords('userregistration',array('user_id'=>$details['user_id']),'user_name');?>
                                    <tr>
                                    <td class="text-center"><?php echo $sr+1;?></td>
                                    <td class="text-left"><?php echo $details['pattern'];?></td>
                                      <td class="text-left"><?php echo ucfirst($username[0]['user_name']);?></td>
                                    <td><a style="text-decoration:none;" title="<?php echo $details['msg_content'];?>" href="<?php echo base_url()?>index.php/userprofile/usercomplaindetail/<?php echo $details['msgid'];?>" ><?php echo substr($details['msg_content'],'0',30).'...';?></a></td>
                                    <td class="text-center"><?php echo date('d M Y',strtotime($details['registerdate']))?></td>
                                    <td class="text-center"><?php 
											$categoty=$this->master_model->getRecords('departments',array('id'=>$details['category']));	
											 echo ucfirst($categoty[0]['type']);
									?></td>
                                    <td class="text-center"><?php if($details['status']==0) {echo 'Not Replied' ;}else{ echo 'Replied';}?></td>
                                     <td class="text-center"><?php if($details['reply_status']==0) {echo 'Closed' ;}else{ echo 'Open';}?></td>
                                    <td class="text-center"><?php 
									if($details['replyby']!='' && $details['replyby']!='0')
									{
										if($details['replyby']!='')
										{
											$repliedarray=array();
											$repliedbyid=explode(',',$details['replyby']);
											if(count($repliedbyid) >0)
											{
												foreach($repliedbyid as $replyidres)
												{
													$adminusername=$this->master_model->getRecords('adminlogin',array('id'=>$replyidres));
													if(count($adminusername) > 0)
													{
														$repliedarray[]=ucfirst($adminusername[0]['adminuser']);
													}
												}
												if(count($repliedarray) > 0)
												{
													echo implode(',',$repliedarray);
												}
											}
											else
											{
												echo 'User not found.';
											}
										}
										else
										{
											echo 'User not found.';
										}
									}?></td>
                                    </tr>
                                   <?php $sr++?>
                                   <?php }?>
									</tbody>
</table>
</div>
<div class="row mx-0 mt-3" style="justify-content:flex-end">
<div class="divider"></div>

<a class="btn btn btn-primary" href="<?php echo base_url();?>index.php/userprofile/user/"><i class="fa fa-angle-double-left"></i>&nbsp;&nbsp; Go back</a></div>

</div>
