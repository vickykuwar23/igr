<div style="text-align:center;">
<?php if ($this->session->flashdata('success_message') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button" id="addadmin_close">×</button>
         <?php echo $this->session->flashdata('success_message') ?><br>
         <div><a href="<?php echo base_url();?>index.php/subadmin/" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
    <?php } ?>
  </div>
  
  <?php 
if ($this->session->flashdata('success_message') != '') 
{ $display='style="display:none;"';
 }
 else
 {
$display='style="display:block;"';
}?>
	<div <?php echo $display;?> id="adminadd_id">
<section id="content" class="min380">
  <section class="main padder">
    <div class="row">
      <div class="col-sm-6 col-xs-offset-3 m-t-large">
        <section class="panel m-t-large">
          <header class="panel-heading text-center"><i class="fa fa-edit"></i>Admin Registration form</header>
          <div class="panel-body">
            <form class="form-horizontal" method="post" data-validate="parsley">
              <div class="form-group">
                <label class="col-lg-4 control-label">Category <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please select the category"></i>
                <div class="col-lg-4">
                  <select name="category" id="selectcat" class="form-control w70">
                    <option value="">Select</option>
                 <?php if(count($department) > 0)
				 {
					 foreach($department as $dept)
					 {?>
					   				 <option value="<?php echo $dept['id'];?>" <?php if(set_value('category')==$dept['id']){echo 'selected="selected"';}?>><?php echo ucfirst($dept['type']);?></option>
				<?php }
				 }?>
                  </select>
                 <div id="error_account" style="color:#F00"><?php 
				 if(form_error('category')!='')
				 {
					if(form_error('category')=="<p>The Category field is required.</p>")
					{
						echo 'The Category field is required';
					}
					else
					{
						$department_name=$this->master_model->getRecords('departments',array('id'=>set_value('category')),'type');
						echo $department_name[0]['type'].' already exist';
					}
				 }?></div>
                
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-4 control-label">Name <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please enter username"></i>
                <div class="col-lg-7">
                  <input type="text" name="name"  data-required="true" class="form-control" value="<?php echo set_value('name')?>" id="name"> 
                     <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('name');?></div>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-lg-4 control-label">Email <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please enter email address"></i>
                <div class="col-lg-7">
                  <input type="text" name="email" class="form-control" data-required="true" data-type="email" value="<?php echo set_value('email')?>" id="email">
                   <div class="m-t m-t-mini" style="color:#F00"><?php echo form_error('email');?></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-4 control-label">Second officer name<span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please enter Second officer name"></i>
                <div class="col-lg-7">
                  <input type="text" name="namecc"  data-required="true" class="form-control" value="<?php echo set_value('namecc')?>" id="name"> 
                     <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('namecc');?></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-4 control-label">Second officer email <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please enter Second officer email"></i>
                <div class="col-lg-7">
                  <input type="text" name="emailcc"  data-required="true" class="form-control" value="<?php echo set_value('emailcc')?>" id="name"> 
                     <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('emailcc');?></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-4 control-label">Password <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please enter password"></i>
                <div class="col-lg-7">
                  <input type="password" name="password" data-required="true" class="form-control"> 
                  <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('password');?></div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-6 col-lg-offset-3 text-center m-t-large">
                  <button type="submit" class="btn btn-primary no-shadow m-r" name="submit">Submit</button>
                  <button type="submit" class="btn btn-white">Cancel</button>
                </div>
              </div>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
            </form>
          </div>
        </section>
      </div>
    </div>
     <div class="row">
    <div class="col-md-2 col-md-offset-5 m-b">
    <a class="btn btn-info no-shadow m-t" href="<?php echo base_url();?>index.php/subadmin/view/"><i class="fa fa-angle-double-left"></i> View existing user</a>
    </div>
    </div>
  </section>
</section>
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
	$('#addadmin_close).on('click',function(){
		$("#adminadd_id").css("display", "block");
		})
	});
</script>