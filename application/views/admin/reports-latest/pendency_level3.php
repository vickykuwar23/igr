<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        District Pendency Report
         <small>List</small>
         
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php }   
			   $type = $this->session->userdata('type');
			   $box = "";
			   if($type == 1 || $type == 7 || $type == 4 || $type == 5 || $type == 10){
				   $box = "display:none";
			   }
			   ?>
				  
		<form name="frm_expr" id="frm_expr" method="post" action="<?php //echo base_url('admin/sro_panel'); ?>">
          
          <div class="form-group-fileds">            
            <div class="row">
			  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" id="csrf_token" value="<?php echo $this->security->get_csrf_hash();?>">	
              <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">From Date</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" name="frm_date" id="frm_date" class="form-control inputval datepicker" value="" placeholder="dd-mm-yyyy"/>
                  </div>
                </div>
              </div>
			  <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">To Date</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" name="to_date" id="to_date" class="form-control inputval datepicker" value="" placeholder="dd-mm-yyyy"/>
                  </div>
                </div>
              </div>
			  <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">Region Name</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select name="region_id" id="region_id" class="w-100 js-example-basic-single form-control">
						<option value="">ALL</option>
						<?php foreach($region_list as $regions){ ?>
						<option value="<?php echo $regions['religion_id']; ?>"><?php echo $regions['region_division_name']; ?></option>
						<?php } ?>
					</select>
                  </div>
                </div>
              </div>
			  <div class="col-md-3" >
                <label class="col-md-12" for="exampleFormControlSelect1">District Name</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select name="district_id" id="district_id" class="w-100 js-example-basic-single form-control">
						<option value="">-- Select --</option>						
					</select>
                  </div>
                </div>
              </div>
			 <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">&nbsp;</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="button" name="btn_comp" id="btn_comp" class="btn btn-success btn-sm btn-level-1" value="Submit" />
					<button type="submit" name="btn_export" id="btn_export" class="btn btn-warning btn-sm " value="Excel" data-toggle="tooltip" data-placement="top" title="Export To Excel" /><i class="fa fa-file-excel-o"></i></button>
					<button type="submit" name="btn_pdf" id="btn_pdf" class="btn btn-warning btn-sm"  value="PDF" data-toggle="tooltip" data-placement="top" title="Export To PDF"/><i class="fa fa-file-pdf-o"></i></button>
                 
				 </div>
                </div>
              </div>  
			  
			  
            </div>			
          </div>
		  </form>  
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
          
            <table class="table table-striped table-bordered dt-responsive nowrap" id="table-pendency-list-igr">
                <thead>
                    <tr>
                         <th>Sr. No.</th>
						 <th>Region Name</th>
						 <th>District Name</th>
						 <th>Level</th>
						 <th>Office Type</th>
						 <th>Complaint Received in Selected Period</th>
						 <th>Total Complaint Received</th>
						 <th>Complaint Resolved in Selected Period</th>
						 <th>Total Complaint Resolved</th>
						 <th>Complaint Pending for the Selected Period</th>
						 <th>Total Complaint Pending </th>
                         <th>Percentage of complaint resolution for the selected period</th>
						 <th>Overall percentage of complaint resolution</th>                         
                    </tr>
                </thead>
                <tbody>                      
                </tbody>
            </table>
                  <!-- main content end -->

               </div>
			   		<!-- <div id="piechart_div"></div>-->
                 <div class="col-xs-12 panel box box-primary">
                	<div id="bar_chart_div"></div>          
                	<div class="bar_chart_div_img" style="display:none;"></div>
                	<div class="download_btn" style="padding: 0px 0px 12px 12px;"></div>
                </div>
                
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<script>
$(document).ready(function(){
	
$("#region_id").change(function(){
	
	var region_id = $(this).val();
	var csrf_test_name = $("#csrf_token").val();
	var base_url = '<?php echo base_url() ?>';
				
		if(region_id != '0')
		{
			$.ajax({
				type: 'POST',
				url: base_url+'admin/reports/district_data',
				data: {region_id:region_id, grievance_token:csrf_test_name},
				async: false,
				success: function(res){
					var json = $.parseJSON(res);
					$('#csrf_token').val(json.csrf_token);					
					$("#district_id").html(json.options);
				}
			});
		}
		else
		{
			$("#district_id").html('');
		}
	});

});
</script>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
<script>
var base_url = '<?php echo base_url(); ?>';
var csrf_test_name = $("#csrf_token").val();
// Code by Manoj
google.charts.load('current', {packages: ['corechart','line']});  
google.charts.setOnLoadCallback(get_chart);
function get_chart(frm_date,to_date,region_id,district_id){
	
	var region_id 		= $("#region_id").val();
	var district_id 	= $("#district_id").val();	
	
  var jsonData = $.ajax({
		  url: base_url+"admin/reports/pendency_level_3",
		  dataType: "json",
		  type: "POST",
		  data:{frm_date: frm_date, to_date: to_date, region_id: region_id,district_id:district_id,is_graph:1, grievance_token:csrf_test_name},
		  async: false,
		  success: function(jsonData){
			  //console.log(jsonData.length);
				if(jsonData.length > 1){
				var data = new google.visualization.arrayToDataTable(jsonData);	
				
				var chart = new google.visualization.BarChart(document.getElementById('bar_chart_div'));
				
				google.visualization.events.addListener(chart, 'ready', function () {
					//bar_chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';			
					$(".bar_chart_div_img").html( '<img src="' + chart.getImageURI() + '">');
					$(".download_btn").html( '<a class="btn btn-primary down_chart" href="' + chart.getImageURI() + '" download target="_blank"> Downlaod </a> &nbsp; <span class="btn btn-primary print_chart" onClick="print_chart();" > Print </span> ');
					//console.log(chart.getImageURI());
	 			});
				
				chart.draw(data,
						   {title:"District Pendency Report",
							width:800, height:600,
							colors: ['#ff0000','#008000','#FFBF00','#0000FF','#GGG'],
							vAxis: {title: "Office"}, isStacked: true,
							hAxis: {title: "Count",fontSize: 5}}
					  );	
				}else{
					$("#bar_chart_div").html('No data Avaialbel');
				}
			}
  });
}
	

</script>