<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Subject
         <small>Edit</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>subject"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  
                  <!-- main content  -->
          
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" >              
              <div class="form-group" id="subject_name_div">
                <label class="col-lg-3 control-label col-lg-offset-1">Subject Name <span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <textarea type="text" name="subject_name"  class="form-control" id="subject_name"><?php echo $subject[0]['subject_name'] ?></textarea>
                  <div id="error_subject_name" style="color:#F00"> <?php echo form_error('subject_name'); ?></div>
                </div>
              </div>
             
              <div class="form-group" id="complaint_type_div" >
                <label class="col-lg-3 control-label col-lg-offset-1">Complaint Type <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <select name="complaint_type" id="complaint_type" class="form-control">
                  <option value="">Please select</option>
                  <?php foreach($complaint_type as $c_type){ ?>
                  <option value="<?=$c_type['complaint_type_id'] ?>" 
                    <?php if($subject[0]['complaint_type']==$c_type['complaint_type_id'] ) echo 'selected=selected' ?> ><?=$c_type['complaint_type_name'] ?></option>
                  <?php } ?>
                </select>
                <div id="complaint_type_error" style="color:#F00"><?php echo form_error('complaint_type'); ?> </div>
                </div>
              </div>

              <div class="form-group" id="complaint_sub_type_div" >
                <label class="col-lg-3 control-label col-lg-offset-1">Office Type <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <select name="complaint_sub_type" id="complaint_sub_type" class="form-control">
                  <option value="">Please select</option>
                  
                  <?php foreach($complaint_sub_type as $sub_type){ ?>
                  <option value="<?=$sub_type['complaint_sub_type_id'] ?>" 
                    <?php if($subject[0]['office_type']==$sub_type['complaint_sub_type_id'] ) echo 'selected=selected' ?> ><?=$sub_type['complaint_sub_type_name'] ?></option>
                  <?php } ?>

                </select>
                <div id="complaint_sub_type_error" style="color:#F00"><?php echo form_error('complaint_sub_type'); ?> </div>
                </div>
              </div>
             
             
              
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" id="csrf_token" value="<?php echo $this->security->get_csrf_hash();?>">
             
              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <input type="submit" name="submit" value="Submit" class="btn btn-success no-shadow" id="btn_submit"/>

                </div>
              </div>
            </form>

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>



<script type="text/javascript">

$(document).ready(function(){
  var csrf_test_name = $("#csrf_token").val();
  $("#complaint_type").on('change',function(){
    var datastring='complaint_id='+ $(this).val() + '&' + grievance_token + '='+csrf_test_name;
    $.ajax({
        type: 'POST',
        data:datastring,
        url: "<?php echo base_url();?>subject/get_complaint_sub_type",
        success: function(res){ 
          var json = $.parseJSON(res);
          $('#csrf_token').val(json.csrf_token);
          $('#complaint_sub_type').html(json.str);
        }
      });
  })
})
</script>


