<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Role 
         <small>List</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>role/add"><button class="btn bg-primary margin " >Add  New</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
          
                  <table class="table table-striped table-bordered dt-responsive nowrap" id="table">
	<thead>
			<tr>
				<th class="text-center bluehd" style="width:5% !important;">
					Sr. No.
				</th>
				<th class="text-center bluehd">
					Role Name
				</th>				
				<th class="text-center bluehd">
				 Status
				</th>
				<th class="text-center bluehd">
					Action
				</th>
			</tr>
			</thead>
			<tbody>
		  <?php $sr=0;?>
		  <?php foreach($roles as $role) {
			  
			  ?>

			<tr>
			<td class="text-center"><?php echo $sr+1;?></td>
			<td><?php echo ucfirst($role['role_name']);?></td>
			<td>                     
			<a href="<?php echo base_url('role/change_status/').$role['role_id'].'/'.$role['status']; ?>"
			  class="btn btn-sm confirm_change_status <?php echo($role['status']=='1' ? "btn-success" : "btn-danger"); ?>"
			   data-msg="Are you sure you want to change the status of this user?"
			  >
			<?php if($role['status'] == 1){echo 'Active';}else{echo 'Inactive';};?>
			</a>                       
             </td>

			<td class="text-center">
			<a href="<?php echo base_url();?>role/edit/<?php echo $role['role_id'];?>" title=""><i class="fa fa-pencil"></i></a>
			<a href="<?php echo base_url();?>role/delete/<?php echo $role['role_id'];?>" title="" 
			onclick="javascript:return deletconfirm();"><i class="ui-tooltip fa fa-trash-o"  data-original-title="Delete"></i></a></td>
			</tr>
		   <?php $sr++?>
		   <?php }?>
			</tbody>
</table>          	     
   

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>

