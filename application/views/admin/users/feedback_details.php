<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Feedback Details
         <small>List</small>
         <div style="float:right; padding:2px;">
            <a href="javascript:void(0)" onclick="window.history.back();"><button class="btn bg-primary margin ">Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  
                  <!-- main content  -->
          
                    <table class="table table-striped">
                        <tbody>
                          <tr>
                            <td style="width:200px"><strong>Grievance ID</strong></td>
                            <td><?php echo $data[0]['comp_code'] ?></td>
                            
                          </tr>
                          <tr>
                            <td><strong>Rating</strong></td>
                            <td><?php echo $data[0]['rating']."Stars" ?></td>
                           
                          </tr>
                          <tr>
                            <td><strong>Message</strong></td>
                            <td><?php echo $data[0]['message'] ?></td>
                           
                          </tr>
                          <tr>
                            <td><strong>Date</strong></td>
                            <td><?php echo date('d-m-Y',strtotime($data[0]['created_at'])) ?></td>
                           
                          </tr>
                           <tr>
                            <td>
                      <?php 
                      $c_code=trim($data[0]["comp_code"]);
                      $complaint=$this->master_model->getRecords('gri_complaint',array('comp_code'=>$c_code));
                        if(count($complaint)){
                          ?>

                              <a href="<?php echo base_url('admin/sro_panel/show_prev_reply/'.$complaint[0]['c_id']) ?>" target="_blank" class="btn btn-primary">View History</a>
                           <?php  } else{?>
                            <td><strong>Invalid Complaint Code</strong></td>
                           <?php } ?>
                            </td>
                           
                          </tr>

                          

                          
                        </tbody>
                      </table>
   					

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>

<script>
$(document).on('click','.view-user',function(){
  var id = $(this).data('id');
  var base_url = '<?php echo base_url() ?>';    
    $.ajax({
      type: "POST",
      url: base_url+"admin/users/details/", 
      async: false, 
      data: {id: id},
      dataType: "html",
      success: function(result){
      $('#view_details').html(result);
      $('#helpModal').modal('show');      
    }});  
});
</script>
