<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Feedback
         <small>List</small>
         <div style="float:right; padding:2px;">
             <!-- <a  href="<?php echo base_url(); ?>admin/callcenterpanel/register_new_user"><button class="btn bg-primary margin " >Add  New User</button></a>
 -->         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  
                  <!-- main content  -->
          
            <table class="table table-striped table-bordered dt-responsive nowrap" id="table">
                   <thead>
                      <tr>
                         <th class="text-center">Sr. No.</th>
                         <th class="text-center">Complaint Code</th>
                         <th class="text-center">Rating</th>
                         <th class="text-center">Message</th>
                         <th class="text-center">Date</th>
                         <th class="text-center">Action</th>
                          
                      </tr>
                   </thead>
                   <tbody>
                      <?php if(count($data) > 0){
                         foreach($data as $key=>$row){ ?>
                      <tr>
                         <td class="text-center"><?php echo $key+1 ?></td>
                         <td class="text-center"><?php echo $row['comp_code'] ?></td>
                         <td class="text-center"><?php echo $row['rating']."Stars" ?></td>
                         <td class="text-center"><?php echo substr($row['message'],'0',25).'...'; ?></td>
                         <td class="text-center"><?php echo date('d-m-Y',strtotime($row['created_at'])) ?></td>
                         <td class="text-center"><a href="<?php echo base_url('admin/users/feedback_details/'.$row['id']) ?>"><i class="fa fa-eye"></i></a></td>
                      </tr>
                      <?php } //end Foreach
					  
					  } // End If Count ?>
                   </tbody>
                </table> 	         
   					

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>

<script>
$(document).on('click','.view-user',function(){
  var id = $(this).data('id');
  var base_url = '<?php echo base_url() ?>';    
    $.ajax({
      type: "POST",
      url: base_url+"admin/users/details/", 
      async: false, 
      data: {id: id},
      dataType: "html",
      success: function(result){
      $('#view_details').html(result);
      $('#helpModal').modal('show');      
    }});  
});
</script>
