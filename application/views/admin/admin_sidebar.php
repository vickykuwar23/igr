<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">

      <!-- Sidebar user panel -->

        <div class="user-panel">

        <div class="pull-left image">

          <img src="<?php echo base_url(); ?>assets/images/user.png" class="img-circle" alt="User Image">

        </div>

        <div class="pull-left info">

          <!-- <p><?php echo @$this->session->userdata('admin_username'); ?></p> -->
          
          
          <a href="#"><i class="fa fa-circle text-success"></i> <strong>
             <?php  
			 error_reporting(0);
			 if ($this->session->userdata('type')=='999'){
					$designation =  "Superadmin";
					  }else{
							$role=$this->master_model->getRecords('gri_roles',array('role_id'=>$this->session->userdata('type') ) );
						$designation =  $role[0]['role_name']; 
					  }            
					echo $this->session->userdata('fullname') . "<br />&nbsp;[".$designation."]";
              ?>
            </strong>
          </a>

        </div>

      </div>

       <?php $url1 = $this->uri->segment(1) ; $url2 = $this->uri->segment(2) ; $url3 = $this->uri->segment(3) ; $url4 = $this->uri->segment(4) ;  ?>

      <ul class="sidebar-menu">

         
     <li class="treeview <?php if ($url1.'/'.$url2  == "admin/dashboard"){ ?> active <?php } ?>" >

      <a href="<?php echo base_url(); ?>admin/dashboard">

        <i class="fa fa-book"></i>

        <span>Dashboard</span>

      </a>

      </li>

        <!--*******************************Careers******************************************-->
        <?php if ($this->session->userdata('type')=='999'): ?>
          
        
	<?php
	
	if($master_mod =='Master' ): $active = 'active'; $subAct = 'menu-open'; else: $active = ''; $subAct = ''; endif; ?>
     <li class="treeview <?php echo $active; ?>" >

        <a href="javascript:void(0)">

            <i class="fa fa-briefcase"></i>

            <span>Masters</span>

             <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

         </a>

          <ul class="treeview-menu <?php echo $subAct; ?>">

            <!--<li><a href="<?php echo base_url(); ?>webmanager/careers/all"><i class="fa fa-circle-o"></i> Active Careers and Job</a></li>-->
			<li><a href="<?php echo base_url(); ?>subadmin"><i class="fa fa-circle-o"></i> System Users </a></li> 
			<!--<li><a href="<?php echo base_url(); ?>module"><i class="fa fa-circle-o"></i> Module List </a></li>
            <li><a href="<?php echo base_url(); ?>permission/add"><i class="fa fa-circle-o"></i> Permission </a></li>-->
			<li><a href="<?php echo base_url(); ?>complainttype"><i class="fa fa-circle-o"></i> Complaint Type </a></li>
            <li><a href="<?php echo base_url(); ?>complaintsubtype"><i class="fa fa-circle-o"></i> Office Type </a></li> 
            <li><a href="<?php echo base_url(); ?>subject"><i class="fa fa-circle-o"></i> Subject </a></li>	           
            <li><a href="<?php echo base_url(); ?>region"><i class="fa fa-circle-o"></i> Regions </a></li>		
            <li><a href="<?php echo base_url(); ?>district"><i class="fa fa-circle-o"></i> District </a></li>
			            <li><a href="<?php echo base_url(); ?>sro_offices"><i class="fa fa-circle-o"></i> SRO Offices</a></li>


            <!-- <li><a href="<?php echo base_url(); ?>webmanager/careers/archive"><i class="fa fa-circle-o"></i> Archive Careers and Job</a></li> -->


           </ul>

       </li>
	   

     <?php endif; ?>

  <?php 
  $user_type = $this->session->userdata('type');
  if($user_type=='1' || $user_type=='2' || $user_type=='999' || $user_type=='3' || $user_type=='4' || $user_type=='7' || $user_type=='9' || $user_type=='10' || $user_type=='11' || $user_type=='12' || $user_type=='13' || $user_type=='14' || $user_type=='15' || $user_type=='16'): ?>
      <li class="treeview <?php if ($url1.'/'.$url2  == "admin/sro_panel"){ ?> active <?php } ?>" >

      <a href="<?php echo base_url(); ?>admin/sro_panel">

        <i class="fa fa-book"></i>

        <span>Grievance</span>

      </a>

      </li>
	  
	  
	  <?php endif;

	 if($user_type=='1' || $user_type=='2' || $user_type=='999' || $user_type=='3' || $user_type=='4' || $user_type=='7' || $user_type=='9' || $user_type=='10' || $user_type=='11' || $user_type=='12' || $user_type=='13' || $user_type=='14' || $user_type=='15' || $user_type=='16' || $user_type=='18'): ?>
	 
	  <?php if($master_mod =='Reports' ): $activec = 'active'; $subAct1 = 'menu-open'; else: $activec = ''; $subAct1 = ''; endif; ?>
	   <li class="treeview <?php echo $activec; ?>" >

        <a href="javascript:void(0)">

            <i class="fa fa-book"></i>

            <span>MIS Reports</span>

             <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

         </a>

          <ul class="treeview-menu <?php echo $subAct1; ?>">                       
            <?php
			$type = $this->session->userdata('type');	
			if($type == 2 || $type == 9):
			?>
			<li><a href="<?php echo base_url('admin/reports/complaint_overview'); ?>"><i class="fa fa-circle-o"></i> Complaint Overview Report </a></li>
			<li><a href="<?php echo base_url('admin/reports/pendency_level_2'); ?>"><i class="fa fa-circle-o"></i> Pendency Report </a></li>
			<li><a href="<?php echo base_url('admin/reports/performance_level_2'); ?>"><i class="fa fa-circle-o"></i> Performance Report </a></li>
			<?php 
			endif;
			?>
			<?php 
			if($type == 1 || $type == 7 || $type == 15):
			?>
			<!--<li><a href="<?php echo base_url('admin/reports/complaint_overview'); ?>"><i class="fa fa-circle-o"></i> Complaint Overview Report </a></li>-->
			<li><a href="<?php echo base_url('admin/reports/pendency_level_2'); ?>"><i class="fa fa-circle-o"></i> Pendency Report </a></li>
			
			<?php 
			endif;
			?>
			<?php 			
			if($type == 999  || $type == 11 || $type == 12 || $type == 13 || $type == 14 || $type == 16): ?>
			<li><a href="<?php echo base_url('admin/reports/pendency_report'); ?>"><i class="fa fa-circle-o"></i>  Pendency Report</a></li>
            <li><a href="<?php echo base_url('admin/reports/pendency_level_3'); ?>"><i class="fa fa-circle-o"></i> District Pendency Report  </a></li>
			<li><a href="<?php echo base_url('admin/reports/performance_level_3'); ?>"><i class="fa fa-circle-o"></i> Performance Ranking Report </a></li>
		    <li><a href="<?php echo base_url('admin/reports/complaint_overview_level_2'); ?>"><i class="fa fa-circle-o"></i> Complaint Overview JDR/ACS </a></li>
		    <li><a href="<?php echo base_url('admin/reports/complaint_overview_level_1'); ?>"><i class="fa fa-circle-o"></i> Complaint Overview SRO/COS </a></li>
		   <?php endif; ?>
		   
		   <?php if($type == 3): ?>		   
		   <li><a href="<?php echo base_url('admin/reports/pendency_report'); ?>"><i class="fa fa-circle-o"></i>  Pendency Report</a></li>
		   <?php endif; ?>
		   
		   <?php if($type == 18): ?>		   
		   <li><a href="<?php echo base_url('admin/reports/cc_report'); ?>"><i class="fa fa-circle-o"></i>  Call Center Report</a></li>
		   <?php endif; ?>
		   
		   </ul>

       </li>
  <?php endif; ?>


  <?php if ( $this->session->userdata('type')=='999' ): ?>
      <li class="treeview <?php if ($url2.'/'.$url3  == "users/list"){ ?> active <?php } ?>" >

      <a href="<?php echo base_url(); ?>admin/users/list">

        <i class="fa fa-book"></i>

        <span>Citizen Users</span>

      </a>

      </li>
     
  <?php endif; ?>

  <?php 
      $type = $this->session->userdata('type');
      if($type == 999 || $type == 3 || $type == 11 || $type == 12 || $type == 13 || $type == 14 || $type == 16 ): ?>
         <li class="treeview <?php if ($url2.'/'.$url3  == "users/feedback" || $url3=="feedback_details" ){ ?> active <?php } ?>" >

      <a href="<?php echo base_url(); ?>admin/users/feedback">

        <i class="fa fa-book"></i>

        <span>Feedback</span>

      </a>

      </li>
 
 <?php endif; ?>


  		<!--*******************************End******************************************-->

    <?php if ($this->session->userdata('type')=='5'  || $this->session->userdata('type')=='18'): ?>
      <li class="treeview <?php if ($url1.'/'.$url2  == "admin/callcenterpanel"){ ?> active <?php } ?>" >

      <a href="<?php echo base_url(); ?>admin/callcenterpanel/users">

        <i class="fa fa-book"></i>

        <span>Grievance</span>

      </a>

      </li>
    <?php endif; ?>   
	<li class="treeview" >

      <a href="<?php echo base_url(); ?>user_manual/System_User_Manual.pdf" target="_blank">

        <i class="fa fa-book"></i>

        <span>User Manual</span>

      </a>

      </li>




	   

	 

 
       	</ul>

    </section> 

  </aside>