<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Region
         <small>Add</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>region"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body ">
                  
            <form class="form-horizontal" method="post" enctype="multipart/form-data" >              
              <!--<div class="form-group" id="person_name_div">
                <label class="col-lg-3 control-label col-lg-offset-1">Person Name <span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <input type="text" name="person_name"  class="form-control" id="person_name" value="<?php echo $this->input->post('person_name'); ?>">
                  <div id="error_person_name" style="color:#F00"> <?php echo form_error('person_name'); ?></div>
                </div>
              </div>
			  <div class="form-group" id="phone_no_div" >
                <label class="col-lg-3 control-label col-lg-offset-1">Phone No <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <input type="text" name="phone_no" class="form-control" id="phone_no" value="<?php echo $this->input->post('phone_no'); ?>">
                <div id="phone_no" style="color:#F00"><?php echo form_error('phone_no'); ?> </div>
                </div>
              </div>

              <div class="form-group" id="emailid_div" >
                <label class="col-lg-3 control-label col-lg-offset-1">Email <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <input type="text" name="emailid" class="form-control" id="emailid" value="<?php echo $this->input->post('emailid'); ?>">
                <div id="emailid" style="color:#F00"><?php echo form_error('emailid'); ?> </div>
                </div>
              </div>
			  -->
              <div class="form-group" id="region_division_name_div" >
                <label class="col-lg-3 control-label col-lg-offset-1">Region Name <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <input type="text" name="region_division_name" class="form-control" id="region_division_name" maxlength="200" value="<?php echo $this->input->post('region_division_name'); ?>">
                <div id="region_division_name" style="color:#F00"><?php echo form_error('region_division_name'); ?> </div>
                </div>
              </div>
             
              
              
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
             
              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <button type="submit" name="submit" class="btn btn-success no-shadow" id="btn_submit">Submit</button>
                </div>
              </div>
            </form>


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>


