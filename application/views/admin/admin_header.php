<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

 <title>Grievance Redressal Portal </title> 

  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.6 -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap/css/bootstrap.min.css">
  

  <!-- Font Awesome -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

       folder instead of downloading all of them to reduce the load. -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin/_all-skins.min.css"> 

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin/blue.css">



  <!-- Date Picker -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>js/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin/bootstrap-datepicker.css">


 <link href="<?php echo base_url(); ?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

 

  

  <!-- jQuery 2.2.3 -->

<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>js/sha256.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=vxkp4h6cttuz6xg870unyu5yhtui78h3mak4yqzfoc652b98"></script>-->

  

  <!--  <script src="<?php echo base_url(); ?>assets/js/tinymce/js/tinymce/tinymce.min.js"></script>

  	<!--<script>tinymce.init({ 

	

	selector: 'textarea.mceEditor',theme: 'modern', plugins: [

    'template paste textcolor colorpicker textpattern imagetools codesample toc help emoticons hr code link'

  ],

  toolbar: 'undo redo | fontselect | formatselect | bold italic  strikethrough  forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',

    

	font_formats: "Andale Mono=andale mono,times;"+

        "Arial=arial,helvetica,sans-serif;"+

        "Arial Black=arial black,avant garde;"+

        "Book Antiqua=book antiqua,palatino;"+

        "Comic Sans MS=comic sans ms,sans-serif;"+

        "Courier New=courier new,courier;"+

        "Georgia=georgia,palatino;"+

        "Helvetica=helvetica;"+

        "Impact=impact,chicago;"+

        "Symbol=symbol;"+

        "Tahoma=tahoma,arial,helvetica,sans-serif;"+

        "Terminal=terminal,monaco;"+

        "Times New Roman=times new roman,times;"+

        "Trebuchet MS=trebuchet ms,geneva;"+

        "Verdana=verdana,geneva;"+

        "Webdings=webdings;"+

        "Wingdings=wingdings,zapf dingbats"

,

  image_advtab: true,

    menubar: false,

 	custom_undo_redo : true

  //edit format

  });</script>
-->
<style>
    table.fixed { table-layout:fixed; }
table.fixed td { overflow: auto; }
.modal-body {
  overflow-x: auto;
  .modal-ku {
  width: 750px;
  margin: auto;
}
}
.main-header .logo {
font-size: 15px;
  }
</style>
</head> 



<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">



  <header class="main-header">

    <!-- Logo -->

    <a href="<?php echo base_url('admin/dashboard');?>" class="logo">

     

      <p ><strong>Grievance Redressal Portal</strong></p>

    </a>

    <!-- Header Navbar: style can be found in header.less -->

    <nav class="navbar navbar-static-top">

      <!-- Sidebar toggle button-->

      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">

        <span class="sr-only">Toggle navigation</span>

      </a>



      <div class="navbar-custom-menu">

        <ul class="nav navbar-nav">

          

          

          <li class="dropdown user user-menu">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

              <img src="<?php echo base_url();?>assets/images/user.png" class="user-image" alt="User Image">

              <span class="hidden-xs"><?php echo ucfirst($this->session->userdata('admin_username'));  ?></span>

            </a>

            <ul class="dropdown-menu">

              <!-- User image -->

              <li class="user-header">

                <img src="<?php echo base_url();?>assets/images/user.png" class="img-circle" alt="User Image">



                <p>

                 <!-- <?php echo ucfirst($this->session->userdata('admin_username'));  ?>   -->
				<?php
					if($this->session->userdata('type') == '999'){
						$adminName = "Super Admin";
					} else{
						$role_det=$this->master_model->getRecords('gri_roles',array('role_id'=>$this->session->userdata('type')));
						$adminName = $role_det[0]['role_name'];
					}
			
				?>
                  <small><?php echo $adminName; ?></small>

                </p>

              </li>

              <!-- Menu Body -->

             

              <!-- Menu Footer-->

              <li class="user-footer">

                <div class="pull-left">

                  <a href="<?php echo base_url(); ?>admin/profile/change_password" class="btn btn-default btn-flat">Change Password</a>

                </div>

                <div class="pull-right">

                  <a href="<?php echo base_url(); ?>superadminlogin/logout" class="btn btn-default btn-flat">Sign out</a>

                </div>

              </li>

            </ul>

          </li>

      

        </ul>

      </div>

    </nav>

  </header>