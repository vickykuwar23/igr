<?php error_reporting(0); 
//print_r($this->session->userdata());
$type = $this->session->userdata('type');
$district_id = $this->session->userdata('district_id');
$districtHide 	= "";
$com_type 		= "";
$off_type 		= "";
$off_sub_type 	= "";
if($type == 1 || $type == 7 || $type == 15){
	$districtHide = "display:none;";
	$com_type = "display:none;";
	$off_type = "display:none;";
	$off_sub_type = "display:none;";
} else if($type == 2){
	$districtHide = "display:none";
} else if($type == 9){
	$districtHide = "display:none";
	$off_type = "display:none;";
	$off_sub_type = "display:none;";
}
?>
<style>
 .w-100 {width: 100%;}
.btn-grp-list {display: flex;flex-wrap: wrap;justify-content: flex-end;margin: 20px 15px;}
.btn-grp-list .btn:last-child {margin-left: 10px !important;}
.mb-1 {margin-bottom: 10px;}
</style>
<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Grievance
         <small>List</small>
         <div style="float:right; padding:2px;">
            <!--  <a  href="<?php echo base_url(); ?>district/add"><button class="btn bg-primary margin " >Add  New</button></a> -->
         </div>
      </h1>
   </section>
   <section class="content">
        <div class="box box-primary">
		<form name="frm_expr" id="frm_expr" method="post" action="<?php //echo base_url('admin/sro_panel'); ?>">
          <div class="btn-grp-list">
				<div class="btngrp">
				 <!--<button type="submit" class="btn btn-primary" name="btn_export" id="btn_export" value="Export"> Export </button>-->
				  <input class="btn btn-primary" type="submit" name="btn_export" id="btn_export" value="Export"/>
				  <!-- <button class="btn btn-primary" name="btn_export" id="btn_export"> Export </button>
				  <button class="btn btn-primary"> Cancel</button>-->
				</div>
			  </div>
          <div class="form-group-fileds">
            <div class="row mb-1">
              <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">Complaint Code :</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" name="com_code" id="com_code"  class="w-100 searchVal form-control" value=""/>
                  </div>
                </div>
              </div>
			  <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">Fullname :</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" name="username" id="username"  class="w-100 searchVal form-control" value=""/>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">Complaint Status :</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="w-100 js-example-basic-single valid searchVal form-control" id="c_status" name="c_status">
					  <option value="-1">All</option>
					  <option value="1">Pending</option>
					  <option value="3">Closed</option>
					  <option value="2">Fake</option>
					</select>
                  </div>
                </div>
              </div>
				<div class="col-md-3" style="<?php echo $districtHide; ?>">
                <label class="col-md-12" for="exampleFormControlSelect1">District :</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="w-100 js-example-basic-single valid searchVal form-control" id="district_id" name="district_id">
					  <option value="">-- Select --</option>
					  <?php foreach($distict_details as $district){ ?>
					  <option value="<?php echo $district['district_id']; ?>"><?php echo $district['district_name']; ?></option>
					  <?php } ?>
					</select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
				
			  <div class="col-md-3" id="type-re" style="<?php echo $com_type; ?>">
                <label class="col-md-12" for="exampleFormControlSelect1">Complaint Type :</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="w-100 js-example-basic-single valid searchVal form-control" id="c_type" name="c_type">
					  <option value="">-- Select --</option>
					  <option value="2">Specific Office</option>
					  <option value="1">Office Service</option>
					</select>
                  </div>
                </div>
              </div>	
              
              <div class="col-md-3" style="<?php echo $off_type; ?>">
                <label class="col-md-12" for="exampleFormControlSelect1">Office Type :</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="w-100 js-example-basic-single valid searchVal form-control" id="c_subtype"
                      name="c_subtype">
                      <option value="">-- Select --</option>
                    </select>
                  </div>
                </div>
              </div>
			 
              <div class="col-md-3" id="response-div" style="<?php echo $off_sub_type; ?>">
                <label class="col-md-12" for="exampleFormControlSelect1">Office Sub Type :</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="js-example-basic-single valid searchVal w-100 form-control" id="sro_office"
                      name="sro_office">
                      <option value="">-- Select --</option>                      
                    </select>
                  </div>
                </div>
              </div>
			  
            </div>			
          </div>
		  </form>
			<div class="box-body table-responsive">
                  
                  <!-- main content  -->
          
                  <table class="table table-striped table-bordered dt-responsive nowrap" id="table-sro">
						<thead>
						<tr>
							<th class="text-center bluehd" style="width:5% !important;">
								Sr. No.
							</th>
							<th class="text-center bluehd">
								Comp code
							</th>
							<th class="text-center bluehd">
								Complainant’s Name
							</th>							
							<?php if($this->session->userdata('type') == '999' || $this->session->userdata('type') == '3'){ ?>
							<th class="text-center bluehd">
								District
							</th>
							<?php } ?>
							<th class="text-center bluehd">
								Complaint type
							</th>
							<?php if($this->session->userdata('type') == '999' || $this->session->userdata('type') == '3' || $this->session->userdata('type') == '2' || $this->session->userdata('type') == '9' || $this->session->userdata('type') == '4'){ ?>
							<th class="text-center bluehd">
								Office Type
							</th>
							<?php } ?>
							<?php if($this->session->userdata('type') != '4'){ ?>
							<th class="text-center bluehd">
								Subject
							</th>
							<?php } ?>	
							<th class="text-center bluehd">
								Assign To
							</th>							
							<?php if($this->session->userdata('type') == '999' || $this->session->userdata('type') == '3' || $this->session->userdata('type') == '2' || $this->session->userdata('type') == '9'){ ?>	
							<th class="text-center bluehd">
								Office Address
							</th>
							<?php } ?>									
							<th class="text-center bluehd">
								Reply Status
							</th>							  
							<th class="text-center bluehd">
							  Action
							</th>
						</tr>
						</thead>
						<tbody>		  
						</tbody>
					</table> 
                  <!-- main content end -->
               </div>
        </div>

        <!-- /.box -->

        <!--/.col (left) -->
        <!-- right column -->
        <!--/.col (right) -->

        <!-- /.row -->
      </section>
   <!-- /.content -->
</div>
<script>
$(document).ready(function(){	
	
	var typeuser = '<?php echo $type; ?>';
	
	if(typeuser == 2 || typeuser == 9){
		// Office List Showing
		$("#c_subtype").change(function(){			
			var getCsubtype = $(this).val();
			var d_id = '<?php echo $district_id; ?>';	
			var base_url = '<?php echo base_url() ?>';		
			if(getCsubtype != '0')
			{
				$.ajax({
					type: 'POST',
					url: base_url+'admin/sro_panel/getOfficeslist',
					data: {office_sub_id:getCsubtype, district_id: d_id},
					async: false,
					success: function(data){					
						$("#sro_office").html(data);
					}
				});
			}
			else
			{
				$("#sro_office").html('');
			}
		});
	}
	
	
	if(typeuser == 4){
		$("#type-re").hide();
		var base_url = '<?php echo base_url() ?>';
		var getCtype = 1;
		$.ajax({
				type: 'POST',
				url: base_url+'admin/sro_panel/getOfficesub',
				data: {office_type_id:getCtype},
				async: false,
				success: function(data){					
					$("#c_subtype").html(data);
				}
			});
	}
	
	// Sub Type List Showing	
	$("#c_type").change(function(){			
		var getCtype = $(this).val();
		var base_url = '<?php echo base_url() ?>';
		if(getCtype == 1){
			$("#response-div").hide();
		} else {
			$("#response-div").show();
		}	
		if(getCtype != '0')
		{
			$.ajax({
				type: 'POST',
				url: base_url+'admin/sro_panel/getOfficesub',
				data: {office_type_id:getCtype},
				async: false,
				success: function(data){					
					$("#c_subtype").html(data);
				}
			});
		}
		else
		{
			$("#c_subtype").html('');
		}
	});
	
	// Office List Showing
	$("#c_subtype").change(function(){			
		var getCsubtype = $(this).val();
		var getdistrict = $("#district_id").val();		
		var base_url = '<?php echo base_url() ?>';		
		if(getCsubtype != '0')
		{
			$.ajax({
				type: 'POST',
				url: base_url+'admin/sro_panel/getOfficeslist',
				data: {office_sub_id:getCsubtype, district_id: getdistrict},
				async: false,
				success: function(data){					
					$("#sro_office").html(data);
				}
			});
		}
		else
		{
			$("#sro_office").html('');
		}
	});
	
	$('#frm_expr').keypress(function (e) {
		if (e.which === 13) {		
			e.preventDefault(); 
		 }
	 });
		
});
</script>

