<style> 
header.panel-heading.bypassme {
    font-size: 28px;
    text-align: center;
    font-weight: 600;
}

.col-md-6.m-t-large {
    margin-top: 20px;
    margin-bottom: 20px;
}

.block.m-b {
    margin-bottom: 8px;
}

div#replies_id_2 > div {
    margin-bottom: 10px;
}

form.m-b {
    margin-left: 15px;
}

form.m-b div.hidethis {
    margin-top: 10px;
}

.col-md-6.m-t-large > h3 {
    margin-left: 0;
}
</style>
<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
          Complaint Details
         <div style="float:right; padding:2px;">
            <a  href="javascript:void(0)" onclick="window.history.back();"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <i class="fa fa-check"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  <!-- main content  -->
                  <div class="main padder">
                     <div class="row">
                        <div class="col-lg-12 m-t-large">
                           <section class="panel grdbg" id="content">
                              
                              <?php 
                                 $userinfo=$this->master_model->getRecords('userregistration',array('user_id'=>$complaint[0]['user_id']));
                                 ?>
                              <div class="col-md-6 m-t-large">
                                 <div class="line line-dashed"></div>
                                 <h4> <strong >User Details :</strong></h4>
                                 <div class="line line-dashed"></div>
                                 <div class="block m-b">
                                    <label class="control-label">Name :</label>
                                    <?php echo ucfirst($userinfo[0]['user_name']);?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Email ID :</label>
                                    <?php echo $userinfo[0]['user_email'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Contact No :</label>
                                    <?php echo $userinfo[0]['user_landline'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Mobile No :</label>
                                    <?php echo $userinfo[0]['user_mobile'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Address :</label>
                                    <?php echo $userinfo[0]['user_address1'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">City :</label>
                                    <?php echo $userinfo[0]['user_city'];?>
                                 </div>
                                 <div class="line line-dashed"></div>
                                 <h4> <strong>Complaint / Grievance Details :</strong></h4>
                                 <br>
                                 <div class="line line-dashed"></div>
                                 <div class="block m-b">
                                    <label class="control-label">Complaint Type :</label>
                                    <?php echo ucfirst($complaint[0]['complaint_type_name']);?>
                                 </div>

                                 <div class="block m-b">
                                    <label class="control-label">Complaint Office :</label>
                                    <?php echo ucfirst($complaint[0]['complaint_sub_type_name']);?>
                                 </div>

                                 <div class="block m-b">
                                    <label class="control-label">Complaint Referenc ID : </label>
                                    <?php echo $complaint[0]['comp_code'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Complaint Subject : </label>
                                    <?php echo $complaint[0]['complaint_sub_type_name'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Complaint Date :</label>
                                    <?php echo date('d M Y',strtotime($complaint[0]['complaint_date']));?> 
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Complaint Details :</label>
                                    <?php echo $complaint[0]['grievence_details'];?>
                                 </div>
                              <?php if ($complaint[0]['religion_id']): 
                                 $region=$this->master_model->getRecords('gri_region_division',array('religion_id'=>$complaint[0]['religion_id']));
                                    ?>
                                    <div class="block m-b">
                                    <label class="control-label">Region :</label>
                                    <?php echo $region[0]['region_division_name'];?>
                                 </div>
                                 <?php endif ?>

                                 <?php if ($complaint[0]['district_id']): 
                                 $dist=$this->master_model->getRecords('gri_district',array('district_id'=>$complaint[0]['district_id']));
                                    ?>
                                    <div class="block m-b">
                                    <label class="control-label">District :</label>
                                    <?php echo $dist[0]['district_name'];?>
                                 </div>
                                 <?php endif ?>

                                 <?php if ($complaint[0]['sro_office_id']): 
                                 $sro=$this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$complaint[0]['sro_office_id']));
                                    ?>
                                    <div class="block m-b">
                                    <label class="control-label">Office Sub Type :</label>
                                    <?php echo $sro[0]['office_name'];?>
                                 </div>
                                 <?php endif ?>
                              
                               <?php 

                                  $images=$this->master_model->getRecords('gri_complaint_images',array('c_id'=>$complaint[0]['c_id']));
                                  if (count($images)) { 
                                    foreach($images as $key=>$image){
                                    ?>
                                   
                                    <div class="block m-b">
                                       <label class="control-label">File <?php echo $key+1 ?> :</label>
                                       <a href="<?php echo base_url('images/complaint_images/').$image["image_name"] ?>" target="_blank">View File</a>
                                      <!--  <img src='<?php echo base_url('images/complaint_images/').$image["image_name"] ?>' alt=""> -->
                                    </div>
                                    
                                 <?php } } ?>

                                 <div class="clear"></div>
                              </div>
                              <br>
                               
                              <div class="col-md-6 m-t-large" style="border-left:1px dashed #6faee8; min-height:450px;">
                                <?php 
                                if($complaint[0]['r_comp_code']){
                                $pre_comp=$this->master_model->getRecords('gri_complaint',array('comp_code'=>$complaint[0]['r_comp_code']));
                                ?>
                                <a href="<?php echo base_url('admin/sro_panel/show_prev_reply/'.$pre_comp[0]['c_id']) ?>" class="btn btn-primary">View Previous Reply</a>
                              <?php } ?>
                                 <h4><strong> REPLY SECTION : </strong></h4>
                                 <div style="text-align:center;" id="reply_id">
                                    <?php if ($this->session->flashdata('success_reply') != '') { ?>
                                    <div class="alert alert-success" style="font-size:17px;">
                                       <button data-dismiss="alert" class="close" type="button">×</button>
                                       <?php echo $this->session->flashdata('success_reply') ?>
                                    </div>
                                    <?php } ?>
                                 </div>
                                 <div class="clear"></div>
                                 <div class="line line-dashed"></div>
                                 <?php 
                                   
                                  $replymsg=$this->master_model->getRecords('complainreply',array('complain_id'=>$this->uri->segment(4)),'',array('reply_id'=>'ASC'));
                                   if(count($replymsg) > 0)
                                    {
                                    foreach($replymsg as $rlpy)
                                    {
                                    $this->db->join('gri_roles','gri_roles.role_id=adminlogin.role_id');
$officer_details=$this->master_model->getRecords('adminlogin',array('id' => $rlpy['user_id']),'role_name');
                                    ?>
                                    <form class="panel-body" method="post" action="<?php echo base_url();?>index.php/adminpanel/complaindetail/<?php echo $this->uri->segment(3);?>">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                    <input type="hidden" name="reply_id" value="<?php echo $rlpy['reply_id'];?>"> 
                                    <div class="clear"></div>
                                    <div id="replies_id_<?php echo $rlpy['reply_id'];?>" class="replied_static_msg">
                                    <div><strong>Replied Details :</strong><?php echo $rlpy['reply_msg'];?></div>
                                    <div><strong>Replied By :</strong><?php echo $officer_details[0]['role_name'];?></div>
                                    <?php if ($rlpy["reply_file"]) { ?>
                             
                                       <div><strong>File : </strong>
                                       <a href="<?php echo base_url('images/reply_uploads/').$rlpy["reply_file"] ?>" target="_blank">View File</a>
                                       </div>
                                      
                                  <?php   } ?>
                                       <strong>Replied On</strong> : <?php echo date('d M, Y',strtotime($rlpy['reply_date']));?>
                                       <div class="clear"></div>
                                       <div class="line line-dashed"></div>
                                    </div>
                                   
                                 </form>
                                 <?php }
                                    }
                                 ?>
                                 <div class="clear"></div>

                               
                                    <!--following div is for pdf only-->       
                                    <div class="block m-b hidethis"  >
                                       <label class="control-label" ><strong> Status</strong> :</label>
                                       <?php if($complaint[0]['reply_status']=='1'){echo 'Open'; } else if($complaint[0]['reply_status']=='2'){ echo 'Fake';} else {echo 'close' ; } ?>
                                    </div>
                                    
                              </div>
                              <div class="clr"></div>
                           </section>
                           <center style=" margin:0 auto 20px;">
                           
                           </center>
                        </div>
                     </div>
                  </div>
                  <!-- main content end -->
               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>


