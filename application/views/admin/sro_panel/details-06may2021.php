<style> 
header.panel-heading.bypassme {
    font-size: 28px;
    text-align: center;
    font-weight: 600;
}

.col-md-6.m-t-large {
    margin-top: 20px;
    margin-bottom: 20px;
}

.block.m-b {
    margin-bottom: 8px;
}

div#replies_id_2 > div {
    margin-bottom: 10px;
}

form.m-b {
    margin-left: 15px;
}

form.m-b div.hidethis {
    margin-top: 10px;
}

.col-md-6.m-t-large > h3 {
    margin-left: 0;
}
</style>
<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
          Complaint Details
         <div style="float:right; padding:2px;">
            <a  href="javascript:void(0)" onclick="window.history.back();"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <i class="fa fa-check"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  <!-- main content  -->
                  <div class="main padder">
                     <div class="row">
                        <div class="col-lg-12 m-t-large">
                           <section class="panel grdbg" id="content">
                              
                              <?php 
                                 $userinfo=$this->master_model->getRecords('userregistration',array('user_id'=>$complaint[0]['user_id']));
                   $comp_details=$this->master_model->getRecords('gri_complaint',array('c_id'=>$this->uri->segment(4)) );  
                   // echo "<pre>";
                   // print_r($comp_details);            
                 // Get Complaint Assign Officer ID
                $getAssigneedetails = $this->master_model->getRecords('gri_complaint_assign',array('comp_code'=>$complaint[0]['comp_code'], 'status' => '1'));
                $assigneeID = $getAssigneedetails[0]['assign_user_id']; 
                 // Get Admin Details
                $getAdminType = $this->master_model->getRecords('adminlogin',array('id'=>$assigneeID, 'status' => '1'));
                $assigneeName = $getAdminType[0]['role_id'];
                $officeId     = $getAdminType[0]['sro_id'];
                // Get Roll Name
                $getAdminName = $this->master_model->getRecords('gri_roles',array('role_id'=>$assigneeName, 'status' => '1'));
                $assignName = $getAdminName[0]['role_name'];
                 
                 ?>
                              <div class="col-md-6 m-t-large">
                                 <div class="line line-dashed"></div>
                                 <h4> <strong >User Details :</strong></h4>
                                 <div class="line line-dashed"></div>
                                 <div class="block m-b">
                                    <label class="control-label">Name :</label>
                                    <?php echo ucfirst($userinfo[0]['user_name']);?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Email ID :</label>
                                    <?php echo $userinfo[0]['user_email'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Contact No :</label>
                                    <?php echo $userinfo[0]['user_landline'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Mobile No :</label>
                                    <?php echo $userinfo[0]['user_mobile'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Address :</label>
                                    <?php echo $userinfo[0]['user_address1'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">City :</label>
                                    <?php echo $userinfo[0]['user_city'];?>
                                 </div>
                                 <div class="line line-dashed"></div>
                                 <h4> <strong>Complaint / Grievance Details :</strong></h4>
                                 <br>
                                 <div class="line line-dashed"></div>
                                 <div class="block m-b">
                                    <label class="control-label">Complaint Type :</label>
                                    <?php echo ucfirst($complaint[0]['complaint_type_name']);?>
                                 </div>
                 
                 <div class="block m-b">
                                    <label class="control-label">Complaint Assign To :</label>
                                    <?php echo $assignName;?>
                                 </div>

                                 <div class="block m-b">
                                    <label class="control-label">Complaint Office :</label>
                                    <?php echo ucfirst($complaint[0]['complaint_sub_type_name']);?>
                                 </div>

                                 <div class="block m-b">
                                    <label class="control-label">Complaint Code : </label>
                                    <?php echo $complaint[0]['comp_code'];?>
                                 </div>
                                 <?php if ($complaint[0]['r_comp_code']): ?>

                                  <div class="block m-b">
                                    <label class="control-label">Complaint Reference Code : </label>
                                    <?php echo $complaint[0]['r_comp_code'];?>
                                 </div>
                                  <?php ?>
                                   
                                 <?php endif ?>
                                 <div class="block m-b">
                                    <label class="control-label">Complaint Subject : </label>
                                    <?php echo $complaint[0]['subject_name'];?>
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Complaint Date :</label>
                                    <?php echo date('d M Y',strtotime($complaint[0]['complaint_date']));?> 
                                 </div>
                                 <div class="block m-b">
                                    <label class="control-label">Complaint Details :</label>
                                    <?php echo $complaint[0]['grievence_details'];?>
                                 </div>
                                 <?php if ($complaint[0]['relief_required']): ?>
                                   <div class="block m-b">
                                    <label class="control-label">Relief required :</label>
                                    <?php echo $complaint[0]['relief_required'];?>
                                 </div>
                                 <?php endif ?>
                                
                              <?php if ($complaint[0]['religion_id']): 
                                 $region=$this->master_model->getRecords('gri_region_division',array('religion_id'=>$complaint[0]['religion_id']));
                                    ?>
                                    <div class="block m-b">
                                    <label class="control-label">Region :</label>
                                    <?php echo $region[0]['region_division_name'];?>
                                 </div>
                                 <?php endif ?>

                                 <?php if ($complaint[0]['district_id']): 
                                 $dist=$this->master_model->getRecords('gri_district',array('district_id'=>$complaint[0]['district_id']));
                                    ?>
                                    <div class="block m-b">
                                    <label class="control-label">District :</label>
                                    <?php echo $dist[0]['district_name'];?>
                                 </div>
                                 <?php endif ?>

                                 <?php if ($complaint[0]['sro_office_id']): 
                                 $sro=$this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$complaint[0]['sro_office_id']));
                                    ?>
                                    <div class="block m-b">
                                    <label class="control-label">Office Sub Type :</label>
                                    <?php echo $sro[0]['office_name'];?>
                                 </div>
                                 <?php endif ?>
								<?php if($complaint[0]['compaint_added_by'] > 0){?>
								 <div class="block m-b">
                                    <label class="control-label" style="font-size:16px;">Complaint Added By :</label>
                                    <b style="font-size:16px;"><?php 
									 $getCCName = $this->master_model->getRecords('adminlogin',array('id'=>$complaint[0]['compaint_added_by']));
									//$addedUserDetail = $getCCName[0]['role_id'];
									echo $getCCName[0]['namecc'] .'(Callcenter User)';?></b>
                                 </div>
                                 <?php } ?>
								 <?php if($complaint[0]['compaint_closed_by'] > 0){?>
								 <div class="block m-b">
                                    <label class="control-label" style="font-size:16px;">Complaint Closed By :</label>
                                    <b style="font-size:16px;"><?php 
									 $getcloserName = $this->master_model->getRecords('adminlogin',array('id'=>$complaint[0]['compaint_closed_by']));
									//$addedUserDetail = $getCCName[0]['role_id'];
									$getRName = $this->master_model->getRecords('gri_roles',array('role_id'=>$getcloserName[0]['role_id']));
									
									echo $getcloserName[0]['namecc'].'('.$getRName[0]['role_name'].')';?></b>
                                 </div>
                                 <?php } ?>
                               <?php 

                                  $images=$this->master_model->getRecords('gri_complaint_images',array('c_id'=>$complaint[0]['c_id']));
                                  if (count($images)) { 
                                    foreach($images as $key=>$image){
                                    ?>
                                   
                                    <div class="block m-b">
                                       <label class="control-label">File <?php echo $key+1 ?> :</label>
                                       <a href="<?php echo base_url('images/complaint_images/').$image["image_name"] ?>" target="_blank">View File</a>
                                      <!--  <img src='<?php echo base_url('images/complaint_images/').$image["image_name"] ?>' alt=""> -->
                                    </div>
                                    
                                 <?php } } ?>

                                 <div class="clear"></div>
                              </div>
                              <br>
                               
                              <div class="col-md-6 m-t-large" style="border-left:1px dashed #6faee8; min-height:450px;">
                                <?php
                                 $user_type = $this->session->userdata('type'); 
                                if( ($user_type==3 || $user_type==2 || $user_type==9 || $user_type=='11' || $user_type=='12' || $user_type=='13' || $user_type=='14') && $complaint[0]['r_comp_code']){
                                $pre_comp=$this->master_model->getRecords('gri_complaint',array('comp_code'=>$complaint[0]['r_comp_code']));
                                ?>
                                <a href="<?php echo base_url('admin/sro_panel/show_prev_reply/'.$pre_comp[0]['c_id']) ?>" class="btn btn-primary">View History</a>
                                <?php } 
                
                
                ?>
                             
                                <div>
                                 <h4><strong> REPLY SECTION : </strong></h4>
                                 <div style="text-align:center;" id="reply_id">
                                    <?php if ($this->session->flashdata('success_reply') != '') { ?>
                                    <div class="alert alert-success" style="font-size:17px;">
                                       <button data-dismiss="alert" class="close" type="button">×</button>
                                       <?php echo $this->session->flashdata('success_reply') ?>
                                    </div>
                                    <?php } ?>
                                 </div>
                                 <div class="clear"></div>
                                 <div class="line line-dashed"></div>
                                 <?php 
                                   
                                  $replymsg=$this->master_model->getRecords('complainreply',array('complain_id'=>$this->uri->segment(4)),'',array('reply_id'=>'ASC'));
                                   if(count($replymsg) > 0)
                                    {
                                    foreach($replymsg as $rlpy)
                                    {
                                    
                                    $this->db->join('gri_roles','gri_roles.role_id=adminlogin.role_id');
                                    $officer_details=$this->master_model->getRecords('adminlogin',array('id' => $rlpy['user_id']),'role_name');
                                    ?>
                                    <form class="panel-body" method="post" action="<?php echo base_url();?>index.php/adminpanel/complaindetail/<?php echo $this->uri->segment(3);?>">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                    <input type="hidden" name="reply_id" value="<?php echo $rlpy['reply_id'];?>"> 
                                    <div class="clear"></div>
                                    <div id="replies_id_<?php echo $rlpy['reply_id'];?>" class="replied_static_msg">
                                    <div><strong>Replied Details :</strong><?php echo $rlpy['reply_msg'];?></div>
                                    <div><strong>Replied By :</strong>
                                      <?php  
                                        echo  $officer_details[0]['role_name'];
                                      
                                      ?>
                                      <?php //echo $officer_details[0]['role_name'];?>

                                    </div>
                                    <?php if ($rlpy["reply_file"]) { ?>
                             
                                       <div><strong>File : </strong>
                                       <a href="<?php echo base_url('images/reply_uploads/').$rlpy["reply_file"] ?>" target="_blank">View File</a>
                                       </div>
                                      
                                  <?php   } ?>
                                       <strong>Replied On</strong> : <?php echo date('d M, Y',strtotime($rlpy['reply_date']));?>
                                       <div class="clear"></div>
                                       <div class="line line-dashed"></div>
                                    </div>
                                    

                                    <div id="replies_id_view_<?php echo $rlpy['reply_id'];?>" style="display:none;" class="replied_class">
                                       <div class="block m-b">
                                          <label class="control-label" style="vertical-align:top; margin-top:20px;"><strong>Reply to Complaint :</strong></label>
                                          <textarea name="update_reply_msg" id="reply_msg" rows="6" data-rangelength="[10,200]" data-trigger="keyup" style="width: 490px; height: 178px;"><?php echo $rlpy['reply_msg'];?></textarea>
                                          <div id="reply_msg_account" style="color:#F00"><?php echo form_error('reply_msg');?></div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="col-lg-8 col-lg-offset-0">
                                          <button id="btn_submit" class="btn btn-regi no-shadow" name="btn_update" type="submit">Update</button>
                                          <a href="javascript:void(0)" class="btn btn-white bypassme" onclick="return showhide_cancel_repliedmsg('replies_id',<?php echo $rlpy['reply_id'];?>,'replied_class','replied_static_msg');">Cancel</a>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="line line-dashed"></div>
                                    </div>
                                 </form>
                                 <?php }
                                    }
                                 ?>
                                 <div class="clear"></div>
                                 
                                 <div style="text-align:center;" id="reply_status_id">
                                    <?php if ($this->session->flashdata('success_reply_status') != '') { ?>
                                    <div class="alert alert-success" style="font-size:17px;">
                                       <button data-dismiss="alert" class="close" type="button">×</button>
                                       <?php echo $this->session->flashdata('success_reply_status') ?>
                                    </div>
                                    <?php } ?>
                                 </div>
                                 
                            <?php if($user_type!='999' && $user_type!='11' && $user_type!='12' && $user_type!='13' && $user_type!='14'){   

                                $loginID = $this->session->userdata('supadminid');
                                $type = $this->session->userdata('type');
                  
                               if($assigneeID == $loginID || ($comp_details[0]['complaint_type_id']==1 && $type==5) ){
                                 ?>
                                 <form method="post" class="m-b">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">      
                                    <?php
                                       if(count($replymsg)<=0)
                                       {
                                         $update_arr=array('reply_status'=>'1',
                                                          'closed_date'=>'0001-01-01',
                                                          'status'=>'0');
                                           $this->master_model->updateRecord('gri_complaint',$update_arr,array('c_id'=>$this->uri->segment(4)));
                                         $disabled='';       
                                       }
                                       else if($complaint[0]['reply_status']==0)
                                       {
                                         $disabled='disabled';
                                       }
                                       else if($complaint[0]['reply_status']==2)
                                       {
                                         $disabled='disabled';
                                       }
                                       else if($complaint[0]['reply_status']==3)
                                       {
                                         $disabled='disabled';
                                       }
                                       ?>

                                    <strong class="bypassme">Reply Status : </strong>  
                                    <select class="bypassme form-control"  name="reply_type_status" id="reply_type_status" style="width:40%; background-color:#dbf4ff;" <?php echo $disabled;?>>
                                       <option value="1" <?php if($complaint[0]['reply_status']=='1'){echo "selected='selected'";}?>>Open</option>
                                       
                                       
                                       <?php if(count($replymsg)>0)
                                          {?>
                                        <option value="2" <?php if($complaint[0]['reply_status']=='2'){echo "selected='selected'";}?>>Fake</option>
                                       <!-- <option value="3" <?php if($complaint[0]['reply_status']=='3'){echo "selected='selected'";}?>>Query to Complainant</option>   -->  
                                       <option value="0" <?php if($complaint[0]['reply_status']=='0'){echo "selected='selected'";}?>>Closed</option>
                                       
                                       <?php }?>
                                    </select>
                                    <!--following div is for pdf only-->       
                                    <div class="block m-b hidethis"  >
                                       <label class="control-label" ><strong>Reply Status</strong> :</label>
                                       <?php 
                                   if($complaint[0]['reply_status']=='1'){echo 'Open'; }
                                    else if($complaint[0]['reply_status']=='2'){ echo 'Fake';}
                                    else if($complaint[0]['reply_status']=='3'){ echo 'Query to Complainant';} 
                                    else {echo 'closed' ; } ?>
                                    </div>
                                    
                                    <?php  
                                       if($complaint[0]['reply_status']==0)
                                       {
                                        $disabled_sts='disabled';
                                       } 
                                       else if($complaint[0]['reply_status']==2)
                                       {
                                        $disabled_sts='disabled';
                                       }
                                       else if($complaint[0]['reply_status']==3)
                                       {
                                        $disabled_sts='disabled';
                                       }
                                       else
                                       {
                                        $disabled_sts='';
                                       }
                                       ?>
                                    <?php 
                                      if( $complaint[0]['reply_status']!='0' && $complaint[0]['reply_status']!='2' && $complaint[0]['reply_status']!='3'){?>
                                      <?php if(count($replymsg)>0){?>
                                       <input type="submit" value="Submit" name="status_btn"    id="status_btn" class="bypassme btn btn-primary">
                                      <?php }
                                    else
                                     {  ?>
                                        <a class="btn btn-info no-shadow bypassme" >Submit</a>
                                    <?php }?>
                                    <?php }
                                     else if($complaint[0]['reply_status']=='1'){
                                      ?>
                                    <input type="submit" value="Submit" name="status_btn" disabled="<?php echo $disabled_sts;?>" id="status_btn"  >
                                    <?php  
                                    }?>
                                    <div class="clear"></div>
                                  </form>
                                  <?php } ?>
                                
                                 <?php
                
                 if($complaint[0]['reply_status']=='1' || count($replymsg)<=0)
                                 {
                  
                  $loginID = $this->session->userdata('supadminid');
                  $type = $this->session->userdata('type');
                  
                if($assigneeID == $loginID || ($comp_details[0]['complaint_type_id']==1 && $type==5) ){
                  ?>
                                 <form class="panel-body" method="post" action="" enctype="multipart/form-data">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                    <div class="block m-b">
                                       <label class="control-label" style="vertical-align:top; margin-top:20px;">Reply to Complaint :</label>
                                       <textarea name="reply_msg" id="reply_msg" rows="6" data-rangelength="[10,200]" data-trigger="keyup" style="width: 80%; height: 178px;"></textarea>
                                       <div id="reply_msg_account" style="color:#F00"><?php echo form_error('reply_msg');?></div>
                                    </div>
                  
                                     <div class="block m-b">
                                       <label class="control-label" style="vertical-align:top; margin-top:20px;">Upload File :</label>
                                       <input type="file" name="reply_file" id="reply_file">
                                       <div id="reply_msg_account" style="color:#F00"><?php echo $error;?></div>
                                       <div><span id="file_err" style="color:red" ></span></div>
                                    </div>
                                    <div class="clear"></div>
                                    <button name="reply" class="btn btn-comn btn btn-primary  m-r-large m-b no-shadow bypassme" type="submit" id="reply" >Reply</button>
                                 </form>
                                 <?php } 
                 
                 } ?>
                              
                              </div>
                              <?php } ?>
                              </div>
                              <div class="clr"></div>
                           </section>
                           <center style=" margin:0 auto 20px;">
                           
                           </center>
                        </div>
                     </div>
                  </div>
                  <!-- main content end -->
               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<script>
  $(document).ready(function(){
    $('#reply').click(function(){
    $("#file_err").html('');
      var file  = $("#reply_file").val();
      if(file != ""){
                        var ext = $('#reply_file').val().split('.').pop().toLowerCase();

                        if ($.inArray(ext, ['pdf', 'PDF','mp3']) == -1) {
                            $("#file_err").html("invalid extension ! only pdf,mp3 files are allowed");
                            return false
                        }

                        
                }

        });
    });
</script>

