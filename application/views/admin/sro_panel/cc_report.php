<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Call Center Report
         <small>List</small>
         
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php }   
			   $type = $this->session->userdata('type');
			   $box = "";
			   if($type == 1 || $type == 7 || $type == 4 || $type == 5 || $type == 15){
				   $box = "display:none";
			   }
			   ?>
				  
		<form name="frm_expr" id="frm_expr" method="post" action="<?php //echo base_url('admin/sro_panel'); ?>">
          
          <div class="form-group-fileds">            
            <div class="row">
			  	  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" id="csrf_token" value="<?php echo $this->security->get_csrf_hash();?>">
              <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">From Date</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" name="frm_date" id="frm_date" class="form-control inputval datepicker" value="" placeholder="dd-mm-yyyy"/>
                  </div>
                </div>
              </div>
			  <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">To Date</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" name="to_date" id="to_date" class="form-control inputval datepicker" value="" placeholder="dd-mm-yyyy"/>
                  </div>
                </div>
              </div>
			  <div class="col-md-3" >
                <label class="col-md-12" for="exampleFormControlSelect1">User Type</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select name="user_type" id="user_type" class="w-100 js-example-basic-single form-control">
						<option value="">ALL</option>
						<option value="1">Call Center</option>
						<option value="2">Citizen</option>
					</select>
                  </div>
                </div>
              </div>
			  <div class="col-md-3" id="type-re">
                <label class="col-md-12" for="exampleFormControlSelect1">Complaint Type :</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="w-100 js-example-basic-single valid searchVal form-control" id="c_type" name="c_type">
					  <option value="">-- Select --</option>
					  <option value="2">Specific Office</option>
					  <option value="1">Office Service</option>
					</select>
                  </div>
                </div>
              </div>
			  
			  <div class="col-md-3" >
                <label class="col-md-12" for="exampleFormControlSelect1">Office Type</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select name="c_subtype" id="c_subtype" class="w-100 js-example-basic-single form-control">
						 <option value="">-- Select --</option>
					</select>
                  </div>
                </div>
              </div>
			  <!--<div class="col-md-3" style="<?php echo $box; ?>">
                <label class="col-md-12" for="exampleFormControlSelect1">Office Subtype</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select name="office_subtype" id="office_subtype" class="w-100 js-example-basic-single form-control">
						<option value="">ALL</option>
						<?php foreach($officeList as $offices){ ?>
						<option value="<?php echo $offices->sro_office_id; ?>"><?php echo $offices->office_name; ?></option>
						<?php } ?>
					</select>
                  </div>
                </div>
              </div>-->
			  <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">Complaint Status :</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="w-100 js-example-basic-single valid searchVal form-control" id="c_status" name="c_status">
					  <option value="-1">All</option>
					  <option value="1">Pending</option>
					  <option value="4">Closed</option>
					  <!-- <option value="3">Query To Complainant</option> -->
					  <option value="2">Fake</option>
					</select>
                  </div>
                </div>
              </div>
			 <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">&nbsp;</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="button" name="btn_comp" id="btn_comp" class="btn btn-success btn-sm btn-call" value="Submit" />
					<button type="submit" name="btn_export" id="btn_export" class="btn btn-warning btn-sm " value="Excel" data-toggle="tooltip" data-placement="top" title="Export To Excel" /><i class="fa fa-file-excel-o"></i></button>
					<button type="submit" name="btn_pdf" id="btn_pdf" class="btn btn-warning btn-sm"  value="PDF" data-toggle="tooltip" data-placement="top" title="Export To PDF"/><i class="fa fa-file-pdf-o"></i></button>
                 
				 </div>
                </div>
              </div>  
			  
			  
            </div>			
          </div>
		  </form>  
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
          
            <table class="table table-striped table-bordered dt-responsive nowrap" id="call-list-table">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
						<th>Complaint Code</th>
                        <th>Complaint Date</th>
						<th>Created By</th>											
						<th>User Type</th>
						<th>Complaint type</th>
						<th>Office type</th>
						<!--<th>Complainant’s Name</th>	<th>Closed By</th>-->
						<th>Status</th>
						<th>Action</th>                        
                    </tr>
                </thead>
                <tbody>                      
                </tbody>
            </table>
                  <!-- main content end -->

               </div>
			  <!-- <div id="piechart_div"></div>-->
                <!--<div class="col-xs-12 panel box box-primary">
                	<div id="bar_chart_div"></div>          
                	<div class="bar_chart_div_img" style="display:none;"></div>
                	<div class="download_btn" style="padding: 0px 0px 12px 12px;"></div>
                </div>-->
		  
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>

<!-- For PDF File -->
<?php if(isset($pdf_file_name) && $pdf_file_name != "")
{ ?>
  <script>
    window.open('<?php echo base_url($pdf_file_name) ?>','_blank');
  </script>
<?php } ?>

<!-- For Excel File -->
<?php if(isset($excel_file_name) && $excel_file_name != "")
{ ?>
  <script>
    window.open('<?php echo base_url($excel_file_name) ?>','_blank');
  </script>
<?php } ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
<script>
/*var base_url = '<?php echo base_url(); ?>';
var csrf_test_name = $("#csrf_token").val();
// Code by Manoj
google.charts.load('current', {packages: ['corechart','line']});  
google.charts.setOnLoadCallback(get_chart);
function get_chart(frm_date,to_date,office_subtype){
  var jsonData = $.ajax({
		  url: base_url+"admin/reports/cc_reports_result",
		  dataType: "json",
		  type: "POST",
		  data:{frm_date: frm_date, to_date: to_date, office_subtype: office_subtype,is_graph:1, grievance_token:csrf_test_name},
		  async: false,
		  success: function(jsonData){
			  //console.log(jsonData.length);
				if(jsonData.length > 1){
				var data = new google.visualization.arrayToDataTable(jsonData);	
				
				var chart = new google.visualization.BarChart(document.getElementById('bar_chart_div'));
				
				google.visualization.events.addListener(chart, 'ready', function () {
					//bar_chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';			
					$(".bar_chart_div_img").html( '<img src="' + chart.getImageURI() + '">');
				$(".download_btn").html( '<a class="btn btn-primary down_chart" href="' + chart.getImageURI() + '" download target="_blank"> Downlaod </a> &nbsp; <span class="btn btn-primary print_chart" onClick="print_chart();" > Print </span> ');
					//console.log(chart.getImageURI());
					//console.log(chart.getImageURI());
	 			});
				
				chart.draw(data,
						   {title:"Call Center Report",
							width:800, height:500,
							//colors: ['#ff0000','#008000','#FFBF00','#0000FF','#GGG'],
							colors: ['#FFBF00','#008000','#ff0000'],
							vAxis: {title: "Office"}, isStacked: true,
							hAxis: {title: "Count"}}
					  );	
				}else{
					$("#bar_chart_div").html('No data Avaialbel');
				}
			}
  });
}*/


$(document).ready(function(){
// Sub Type List Showing	
	$("#c_type").change(function(){			
		var getCtype = $(this).val();
		var csrf_test_name = $("#csrf_token").val();
		var base_url = '<?php echo base_url() ?>';
			
		if(getCtype != '0')
		{
			$.ajax({
				type: 'POST',
				url: base_url+'admin/sro_panel/getOfficesub',
				data: {office_type_id:getCtype,grievance_token:csrf_test_name},
				async: false,
				success: function(res){
					var json = $.parseJSON(res);
					$('#csrf_token').val(json.csrf_token);				
					$("#c_subtype").html(json.options);
				}
			});
		}
		else
		{
			$("#c_subtype").html('');
		}
	});
});	
</script>