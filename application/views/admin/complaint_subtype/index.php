﻿<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Office Type
         <small>List</small>
         <!--<div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>complaintsubtype/add"><button class="btn bg-primary margin " >Add  New</button></a>
         </div>-->
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
          		<table class="table table-striped table-bordered dt-responsive nowrap" id="table">
	<thead>
			<tr>
				<th class="text-center bluehd" style="width:5% !important;">
					Sr. No.
				</th>
				<th class="text-center bluehd">
					Complaint Type
				</th>
				<th class="text-center bluehd">
					Office Type
				</th>				
				<th class="text-center bluehd">
					Status
				</th>
				<th class="text-center bluehd">
					Action
				</th>
			</tr>
			</thead>
			<tbody>
		  <?php $sr=0;?>
		  <?php foreach($complaint_subtype_list as $details) {
			  $complaint_type=$this->master_model->getRecords('gri_complaint_type',array('complaint_type_id'=>$details['complaint_type_id']));
			  $record_cnt=$this->master_model->getRecords('gri_complaint',array('complaint_sub_type_id'=>$details['complaint_sub_type_id']));
			  $rec_cnt = count($record_cnt);
			  ?>
			<tr>
			<td class="text-center"><?php echo $sr+1;?></td>
			<td><?php echo ucfirst($complaint_type[0]['complaint_type_name']);?></td>
			<td class="text-center"><?php echo $details['complaint_sub_type_name'];?></td>
			<!-- <td class="text-center"><?php if($details['status'] == 1){echo 'Active';}else{echo 'Inactive';};?></td> -->
			<td class="text-center">
                     
              <a href="<?php echo base_url('complaintsubtype/change_status/').$details['complaint_sub_type_id'].'/'.$details['status']; ?>"
                class="btn btn-sm confirm_change_status <?php echo($details['status']=='1' ? "btn-success" : "btn-danger"); ?>"
                 data-msg="Are you sure you want to change the status of this user?"
                >
              <?php if($details['status'] == 1){echo 'Active';}else{echo 'Inactive';};?>
              </a>
                       
         </td>
			<td class="text-center">
			<a href="<?php echo base_url();?>complaintsubtype/edit/<?php echo $details['complaint_sub_type_id'];?>" title=""><i class="fa fa-pencil"></i></a>
			&nbsp;&nbsp;|&nbsp;&nbsp;
			<?php if($rec_cnt > 0){ ?>
				<a href="javascript:void(0);" title="" 
			 class="open-popup"><i class="ui-tooltip fa fa-trash-o "  data-original-title="Delete"></i></a>
			<?php } else { ?>
				<a href="<?php echo base_url();?>complaintsubtype/delete/<?php echo $details['complaint_sub_type_id'];?>" title="" 
			onclick="javascript:return deletconfirm();"><i class="ui-tooltip fa fa-trash-o"  data-original-title="Delete"></i></a>
			<?php } ?>
			</td>
			</tr>
		   <?php $sr++?>
		   <?php }?>
			</tbody>
</table>
                                 
   		

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<div id="helpModal" class="modal fade">
 <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Alert</h4>
		</div>
		<div class="modal-body" id="view_details">
			<p>Sorry, Complaint already exist related this office type. So that you can't delete this office type.</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>
	
</div>
</div>





