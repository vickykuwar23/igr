   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $total_comp_count[0]['total_comp_count'] ?></h3>

              <p><strong>Grievances Received</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <!-- <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $closed_count[0]['closed_count'] ?></h3>
              <p><strong>Grievances Resolved</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-android-chat"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
        <?php 
         $type = $this->session->userdata('type');
         if ($type==1 || $type==7  || $type==15 ): ?>
           <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $escalated_to_jdr[0]['escalated_to_jdr'] ?></h3>


              <p><strong>Grievances Escalated to JDR/ACS</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <?php endif ?>
        
        <?php 
         $type = $this->session->userdata('type');
         if ($type==2 || $type==9): ?>
           <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow" style="height: 120px;">
            <div class="inner">
              <h3><?php echo $escalated_to_igr[0]['escalated_to_igr'] ?></h3>


              <!-- <p><strong>Grievances Escalated to IGRA</strong></p> -->
              <p><strong>Grievances Raised to Divisional Commissioner (Konkan Division)</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <?php endif ?>
       
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $open_count[0]['open_count'] ?></h3>


              <p><strong>Pending Grievances</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->

         <?php 
         $type = $this->session->userdata('type');
         if ($type==999 || $type==11 || $type==12 || $type==13 || $type==14 || $type==16): ?>
           <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $open_at_sro_level[0]['open_at_sro_level'] ?></h3>


              <p><strong>Grievances Pending at SRO/COS/Help Desk Level</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3><?php echo $open_at_jdr_level[0]['open_at_jdr_level'] ?></h3>


              <p><strong>Grievances Pending at JDR/ACS Level</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>

         <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange" style="height: 120px;">
            <div class="inner">
              <h3><?php echo $escalated_to_igr[0]['escalated_to_igr'] ?></h3>


              <p><strong>Grievances Raised to Divisional Commissioner (Konkan Division)</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>

         <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo number_format(  $closed_count[0]['closed_count'] / $total_comp_count[0]['total_comp_count'] *100,2) ?>%</h3>

              <p><strong> Grievance Resolution</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>

        

        <?php endif ?>
        
         <?php 
         $type = $this->session->userdata('type');
         if ($type!=999 && $type != 11 && $type != 12 && $type != 13 && $type != 14 && $type != 16 && $type!=5 && $type!=18): ?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red" >
            <div class="inner">
              <h3><?php echo $open_more_days[0]['open_more_days'] ?></h3>
              <p><strong>Grievances pending for <?=$more_days ?> days or more</strong></p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3><?php echo $open_more_between_days[0]['open_more_between_days'] ?></h3>

              <p><strong>Grievances pending for <?=$between_days ?> days</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue" >
            <div class="inner">
              <h3><?php echo $open_less_than_days[0]['open_less_than_days'] ?></h3>

              <p><strong>Grievances pending for <?=$less_days ?> days or less</strong></p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
        <?php endif ?>
        <?php
        if ($type==5 || $type==18): ?>
           <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3><?php echo $closed_by_cc[0]['closed_by_cc'] ?></h3>


              <p><strong>Grievances Closed By Call Center User</strong></p>
              <br>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="<?php echo base_url('admin/sro_panel') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <?php endif ?>



      </div>
       
    </section>
  </div>