<style>
   .alert>p, .alert>ul {
   text-align: center;
   }
   label#complaint_type-error {
   margin-left: 233px;
   }
    .error{color:red;}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
         Grievance 
         <small>Add</small>
         <div style="float:right; padding:2px;">
            <a  href="<?php echo base_url(); ?>admin/callcenterpanel/users"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="row">
               <?php if ($this->session->flashdata('success_message') != '') { ?>
               <div class="col-md-12 mx-auto text-center mt-3">
                  <div class="alert alert-success mb-0" style="font-size:17px;">
                     <span> <i class="fa fa-tick pr-2"></i> <?php echo $this->session->flashdata('success_message') ?> </span>       
                     <button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
                  </div>
               </div>
               <?php } ?> 
               <?php if ($this->session->flashdata('successmsgsend') != '') { ?>
               <div class="col-md-4 mx-auto text-center mt-3">
                  <div class="alert alert-success mb-0" style="font-size:17px;">
                     <span> <i class="fa fa-tick pr-2"></i> <?php echo $this->session->flashdata('successmsgsend') ?> </span>
                     <button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
                  </div>
               </div>
               <?php } ?>
               <?php 
                  if ($imageError != '') { ?>
               <div class="col-md-4 mx-auto text-center mt-3">
                  <div class="alert alert-error mb-0" style="font-size:17px;">
                     <span> <i class="fa fa-warning pr-2"></i> <?php echo $imageError; ?> </span>
                     <button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
                  </div>
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('error') != '') { ?>
               <div class="col-md-4 mx-auto text-center mt-3">
                  <div class="alert alert-danger mb-0" style="font-size:17px;">
                     <button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
                     <?php echo $this->session->flashdata('error') ?>
                  </div>
               </div>
               <?php } ?>
               </div>

               <div class="box-body">
                  <div class="panel panel-default">
                     <div class="panel-body">
                        
                        <p><strong>User Details</strong></p>
                        <p> Name: <?=$user_info[0]['user_name']; ?></p>
                        <p> Mobile: <?=$user_info[0]['user_mobile']; ?></p>
                        <p> Email: <?=$user_info[0]['user_email']; ?></p>

                     </div>
                  </div>
                  <!-- main content  -->
                  <form method="post" id="complaintform" name="complaintform"  enctype="multipart/form-data" >
              
                  <div class="form-grp-filed">
                     <div class="form-group" id="regions">
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Region<span style="color:red">*</span></label>
                           </div>
                           <div class="col-md-8">
                              <select name="region" class="form-control listout" id="region">
                                 <option value="">--Select--</option>
                                 ';
                                 <?php if(count($get_regions)>0){
                                    foreach($get_regions as $region){ ?>
                                 <option value="<?php echo $region['religion_id'] ?>" <?php if($region['religion_id'] == $this->input->post('region')){ echo 'selected'; } ?>><?php echo $region['region_division_name']; ?></option>
                                 <?php } 
                                    } else { ?>
                                 <option value="0">--Select--</option>
                                 <?php } ?>
                              </select>
                           </div>
                           <div id="error_complaint_type" class="error" style="color:#F00"> <?php echo form_error('region'); ?></div>
                        </div>
                     </div>
                     <div class="form-group" id="regions-list">
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">District<span style="color:red">*</span></label>
                           </div>
                           <div class="col-md-8">
                              <select name="district" class="form-control districtout" id="district">
                                 <option value="">--Select--</option>
                              </select>
                           </div>
                           <div id="error_complaint_type" class="error" style="color:#F00"> <?php echo form_error('district'); ?></div>
                        </div>
                     </div>
                     <div class="form-group" id="complaint_type_name">
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Complaint Type<span style="color:red">*</span></label>
                           </div>
                           <div class="col-md-8">
                              <div class="radio complaint-rdb">
                                 <?php foreach($complaint_type_list as $typelist){ ?>
                                 <label class="lbl-rdb radio-inline">

                                 <input type="radio" name="complaint_type" class="click-event" id="complaint_type"
                                    value="<?php echo $typelist['complaint_type_id']; ?>"
                                    <?php if($this->input->post('complaint_type') == $typelist['complaint_type_id']){ ?>
                                    checked="checked" <?php } ?>> <?php echo $typelist['complaint_type_name']; ?>
                                 </label>
                                    
                                 <?php } ?>
                              </div>
                              <div id="error_complaint_type" class="error" class="some-class" style="color:#F00"> <?php echo form_error('complaint_type'); ?></div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group" id="response_time" >
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Office Type<span style="color:red">*</span></label>
                           </div>
                           <div class="col-md-8">
                              <select id="office_type" class="form-control choosesub" name="office_type">
                                 <option value=""> -- Select --</option>
                                 <?php if(count($complaint_subtype_details) > 0){
                                    foreach($complaint_subtype_details as $officeType){  ?>
                                 <option value="<?php echo $officeType['complaint_sub_type_id']; ?>" <?php if($officeType['complaint_sub_type_id'] == $this->input->post('office_type')){ echo 'selected'; } ?>> <?php echo $officeType['complaint_sub_type_name']; ?> </option>
                                 <?php }
                                    } ?>
                              </select>
                              <div id="office_type" class="error" style="color:#F00"><?php echo form_error('office_type'); ?></div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group" id="response_times" >
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Services<span style="color:red">*</span></label>
                           </div>
                           <div class="col-md-8">
                              <select id="office_service" class="form-control choosesub" name="office_service">
                                 <option value=""> -- Select --</option>
                                 <?php if(count($complaint_subtype_service) > 0){        
                                    foreach($complaint_subtype_service as $officeService){  ?>
                                 <option value="<?php echo $officeService['complaint_sub_type_id']; ?>" <?php if($officeService['complaint_sub_type_id'] == $this->input->post('office_service')){ echo 'selected'; } ?>> <?php echo $officeService['complaint_sub_type_name']; ?> </option>
                                 <?php }
                                    } ?>
                              </select>
                              <div id="office_type" class="error" style="color:#F00"><?php echo form_error('office_service'); ?></div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group" id="officesub-list" >
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Office Sub Type<span style="color:red">*</span></label>
                           </div>
                           <div class="col-md-8">
                              <select name="sro_office" class="form-control " id="sro_office">
                                 <option value="">--Select--</option>
                              </select>
                           </div>
                           <div id="error_sro_office" style="color:#F00" class="error"> <?php echo form_error('sro_office'); ?></div>
                        </div>
                     </div>
                     <div class="form-group" id="subjectilist" >
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Subject<span style="color:red">*</span></label>
                           </div>
                           <div class="col-md-8">
                              <select name="subject" class="form-control otherContent" id="subjectlist">
                                 <option value="">--Select--</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="form-group" id="othr" >
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Other</label>
                           </div>
                           <div class="col-md-8">
                              <textarea name="othertext" id="othertext" class="form-control"></textarea>            
                           </div>
                        </div>
                     </div>
                     <!--<div id="result"> </div>
                        <div id="othr"> </div>-->
                     <div class="form-group" id="response_time">
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Grievance Details <small>(Maximum 4000 Characters) </small><span style="color:red">*</span> </label>
                           </div>
                           <div class="col-md-8">
                              <textarea rows="3" cols="1" name="grievance_details" class="txt-area form-control" id="grievance_details" maxlength="4000"
                                 onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9-\+\,()*._:;@\n' ]/g,'')"><?php echo $this->input->post('grievance_details'); ?></textarea>
                              <div id="grievance_details" class="error" style="color:#F00"><?php echo form_error('grievance_details'); ?> </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group" id="response_time">
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Relief Required </label>
                           </div>
                           <div class="col-md-8">
                              <textarea  rows="3" cols="1" name="relief_required" class="txt-area form-control"
                                 id="relief_required"><?php echo $this->input->post('relief_required'); ?></textarea>
                              <div id="relief_required" class="error" style="color:#F00"><?php echo form_error('response_time'); ?> </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group" id="response_time">
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Upload File </label>
                           </div>
                           <div class="col-md-8">
                              <input type="file" name="upload_file"  class="form-control" id="upload_file" value="">[Accept only PDF, size upto 2MB]
                              <div id="upload_file" class="error" style="color:#F00"><?php echo form_error('upload_file'); ?> </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group" id="response_time">
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Complaint Date </label>
                           </div>
                           <div class="col-md-8">
                              <input type="test" name="comp_date"  class="form-control datepicker" id="comp_date" value="" required="required">
                              <div id="comp_date" style="color:#F00"><?php echo form_error('comp_date'); ?> </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group" id="response_time">
                        <div class="row">
                           <div class="col-md-4">
                              <label class="control-label">Acknowledgement <span style="color:red">*</span></label>
                           </div>
                           <div class="col-md-8">
                              <input required="required" type="checkbox" name="acknowldgement"  class="" id="acknowldgement"  value="1" <?php if($this->input->post('acknowldgement') == 1){ echo 'checked'; } ?>>&nbsp;&nbsp;<span style="font-size:14px;">I do hereby declare that all the information given above is true.</span>
                              <div id="acknowldgement" class="error" style="color:#F00"><?php echo form_error('acknowldgement'); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <input type="hidden" id="csrf_token"  name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                     <div class="mb-3">
                        <div class="col-lg-12 mx-auto d-flex justify-content-center">
                           <button type="submit" name="submit" class="btn btn_submit" id="btn_submit">Submit</button>
                        </div>
                     </div>
                  </div>
                  <!-- main content end -->
               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<script>
   validation();
   function validation(){
   
     
   $("#complaintform").validate(
   {
     rules:{
   
       complaint_type:{
   
       required: true
   
       },
      office_type:{
   
       required: true
   
       },  
      subject:{ 
   
       required: true
   
       },
     region:{
   
       required: true
   
       },
     district:{
   
       required: true
   
       },  
       sro_office:{
   
       required: true
   
       },    
     grievance_details:{
   
       required: true
   
       },
     acknowldgement:{
   
       required: true
   
       }   
     },
   
     messages:{
   
       complaint_type:{
   
       required: "The field is mandatory!"
   
       }, 
       office_type:{
   
       required: "The field is mandatory!"
   
       }, 
       subject:{
   
       required: "The field is mandatory!"
   
       },
       region:{
   
       required: "The field is mandatory!"
   
       },
       district:{
   
       required: "The field is mandatory!"
   
       },
       sro_office:{
   
       required: "The field is mandatory!"
   
       },
       grievance_details:{
   
       required: "The field is mandatory!"
   
       },
       acknowldgement:{
   
       required: "The field is mandatory!"
   
       }
   
     }, 
   
     errorPlacement: function(error, element) {
   
       if (element.attr("type") == "radio") { 
        error.insertAfter( element.parent().parent());
        } if (element.attr("type") == "checkbox") {
        error.insertAfter( element.parent().parent());
        } else {
          error.insertAfter(element);
   
       }
     },
     submitHandler: function(form) {
           //return false;
           form.submit();
         }
   
     });
     
   }
   
   
   $(document).ready(function(){
     var complaint_types = $("input[name='complaint_type']:checked").val();
     if(complaint_types == '2'){
       $("#response_time").show();
       $("#subjectilist").show();
       $("#response_times").hide();
       $("#othr").hide();
     } else if(complaint_types == '1'){
       $("#response_time").hide();
       $("#subjectilist").hide();
       $("#officesub-list").hide();
       $("#response_times").show();
       $("#othr").hide();
     } else {
       $("#response_time").show();
       $("#subjectilist").show();
       $("#response_times").hide();
       $("#othr").hide();
     }
     
       $(".click-event").click(function() {      
       var complaint_types = $("input[name='complaint_type']:checked").val();      
       if(complaint_types == 1){       
         $("#response_time").hide();
         $("#officesub-list").hide();
         $("#response_times").show();
         $("#subjectilist").hide();      
       } else if(complaint_types == 2){
         
         $("#response_time").show();
         $("#officesub-list").show();
         $("#subjectilist").show();  
         $("#response_times").hide();
       } 
       
     });
     
     // Onload Selected Values   
     var complaint_types = $("input[name='complaint_type']:checked").val();
     var office_type = $("#office_type").val();
     var regionID = $("#region").val();
     var base_url = '<?php echo base_url() ?>';
       var postDist = '<?php echo $this->input->post('district') ?>';
     var subID = '<?php echo $this->input->post('subject') ?>';
     var sroOff = '<?php echo $this->input->post('sro_office') ?>';
     $.ajax({
       type: "POST",
       url: base_url+"admin/callcenterpanel/getDistrict/", 
       async: false, 
       data: {complaint_types:complaint_types, office_type:office_type, regionid:regionID,district_id:postDist},
       success: function(res){
         $('#district').html(res);
         validation(); 
       }});  
   
     $.ajax({
       type: "POST",
       url: base_url+"admin/callcenterpanel/getSubject/", 
       async: false, 
       data: {complaint_types:complaint_types, office_type:office_type, subject_id:subID},
       success: function(response){
       $('#subjectlist').html(response);
       validation(); 
     }});
     
     $.ajax({
       type: "POST",
       url: base_url+"admin/callcenterpanel/getSroOffice/", 
       async: false, 
       data: {office_type:office_type, region:regionID, district:postDist, complaint_types:complaint_types, sro_off:sroOff},
       success: function(response){
       $('#sro_office').html(response);
       validation(); 
     }});  
     // End  
   });
   
   $(document).on('change','.choosesub',function(){
     
     var complaint_types = $("input[name='complaint_type']:checked").val();
     var office_type = $("#office_type").val();
     var regionID  = $("#region").val();
     var district  = $("#district").val(); 
      var csrf_test_name = $("#csrf_token").val();
     var base_url  = '<?php echo base_url() ?>'; 
   
     $.ajax({
       type: "POST",
       url: base_url+"admin/callcenterpanel/getSubject/", 
       async: false, 
       data: {complaint_types:complaint_types, office_type:office_type,grievance_token:csrf_test_name},
       success: function(response){
       $('#subjectlist').html(response);
       validation(); 
     }});
     
     $.ajax({
       type: "POST",
       url: base_url+"admin/callcenterpanel/getSroOffice/", 
       async: false, 
       data: {office_type:office_type, region:regionID, district:district, complaint_types:complaint_types, grievance_token:csrf_test_name},
       success: function(response){
       $('#sro_office').html(response);
       validation(); 
     }});  
     
   });
   
   // For District Call 
   $(document).on('change','.listout',function(){  
     var complaint_types = $("input[name='complaint_type']:checked").val();
     var office_type = $("#office_type").val();
     var regionID = $("#region").val();
     var base_url = '<?php echo base_url() ?>';  
      var csrf_test_name = $("#csrf_token").val();
     $.ajax({
       type: "POST",
       url: base_url+"admin/callcenterpanel/getDistrict/", 
       async: false, 
       data: {complaint_types:complaint_types, office_type:office_type, regionid:regionID, grievance_token:csrf_test_name},
       success: function(res){
         //$('#district').html(res);
		 var json = $.parseJSON(res);
		 $('#csrf_token').val(json.csrf_token); 
		 $('#district').html(json.options);	
         validation(); 
       }});
       
   }); 
   
   // For Other Textarea Option 
   $(document).on('change','.otherContent',function(){ 
     var subjectid = $("#subjectlist").val();
     var base_url = '<?php echo base_url() ?>';
     if(subjectid == -1){ 
       $("#othr").show();    
     } else {    
       $("#othr").hide();
     } 
         
   });
   
   function isNumberKey(evt)
     {
       var a=0;   
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode < 47 || charCode > 45) && (charCode < 48 || charCode > 57))
          if(charCode==8 )
            return true;
          else
        return false;
   
     return true;
     }
   
    function isAlphaNumeric(evt)
     {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && ((charCode < 65 || charCode > 90) && (charCode < 96 || charCode > 122) && (charCode < 47 || charCode > 45) && (charCode < 48 || charCode > 57)))
         return false;
   
       return true;
     } 
   
</script>

