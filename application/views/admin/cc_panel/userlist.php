<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Users
         <small>List</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>admin/callcenterpanel/register_new_user"><button class="btn bg-primary margin " >Add  New User</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
          
            <table class="table table-striped table-bordered dt-responsive nowrap" id="table">
                   <thead>
                      <tr>
                         <th class="text-center bluehd">Sr. No.</th>
                         <th class="text-center bluehd">Name</th>
                         <th class="text-center bluehd">Mobile</th>
                         <th class="text-center bluehd">Email</th>
                         <th class="text-center bluehd">Office Dstrict</th>

                         <th class="text-center bluehd">Action</th>
                          
                      </tr>
                   </thead>
                   <tbody>
                      <?php if(count($users) > 0){
                         foreach($users as $key=>$user){ ?>
                      <tr>
                         <td><?=$key+1 ?></td>
                         <td><?=$user['user_name'] ?></td>
                         <td><?=$user['user_mobile'] ?></td>
                         <td><?=$user['user_email'] ?></td>
                          <td><?=$user['user_city'] ?></td>
                         <td><a href='<?php echo base_url('admin/callcenterpanel/user_details/').$user["user_id"] ?>' class="btn btn-primary">Show grievance</a>
                          <a href='<?php echo base_url('admin/callcenterpanel/add_grevience/').$user["user_id"] ?>' class="btn btn-primary">Add grievance</a>
                         </td>
                      </tr>
                      <?php }} ?>
                   </tbody>
                </table> 	         
   					

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<script>
</script>
