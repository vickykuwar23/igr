<style>
form ul.helper-text {
  display: block;
  margin-top: 6px;
  font-size: 12px;
  line-height: 22px;
  color: #808080;
}
form ul.helper-text li.valid {
  color: #1fd34a;
}
form.valid input {
  border: 2px solid #1fd34a;
}
.text-red{ color:#F00}
.text-green{ color:#090}
</style>
<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Change Password
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>admin/dashboard"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body col-md-10">
                  
                  <!-- main content  -->
                 <form method="post" data-validate="parsley">
                  
                  <div class="form-grp-filed">
                     <div class="form-group">
                        <div class="row">
                           <label class="col-md-4 control-label align-self-center" >Password<span style="color:red">*</span></label>
                           <div class="col-md-8">
                              <input type="password" id="password1" name="pass" data-required="true" class="form-control password" autofocus > 
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('pass');?></div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                           <label class="col-md-4 control-label align-self-center" >Confirm Password <span style="color:red">*</span></label>
                           <div class="col-md-8">
                              <input type="password" id="password2" name="con_password" class="form-control" onChange="checkPasswordMatch();" />
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('con_password');?></div>
                           </div>
                        </div>
                     </div>
                    
                     <div class="form-group">
                        <label class="col-lg-4 control-label"></label>
                        <div class="col-lg-7" id="divCheckPasswordMatch">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-5">
                           <ul class="helper-text passinst">
                              <li class="length">* Must be at least 8 characters long</li>
                              <li class="lowercase">* Must contain a lowercase letter</li>
                              <li class="uppercase">* Must contain an uppercase letter</li>
                              <li class="special">* Must contain a special character</li>
                              <li class="number">* Must contain a number </li>
                           </ul>
                        </div>
                     </div>
                     <div class="mb-3 text-center">
                        <div class="col-lg-12 mx-auto d-flex justify-content-center">
                           <button type="submit" class="btn btn-success" name="btn_set" id="submitpass">Submit</button> 
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-4">
                        </div>
                     </div>
                     <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                  </div>
               </form>
                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>


<script>
   (function(){
     var password = document.querySelector('.password');
   
     var helperText = {
       charLength: document.querySelector('.helper-text .length'),
       lowercase: document.querySelector('.helper-text .lowercase'),
       uppercase: document.querySelector('.helper-text .uppercase'),
       special: document.querySelector('.helper-text .special'),
       number: document.querySelector('.helper-text .number')
     };
     
     var pattern = {
       charLength: function() {
         if( password.value.length >= 8 ) {
           return true;
         }
       },
       lowercase: function() {
         var regex = /^(?=.*[a-z]).+$/; // Lowercase character pattern
   
         if( regex.test(password.value) ) {
           return true;
         }
       },
       uppercase: function() {
         var regex = /^(?=.*[A-Z]).+$/; // Uppercase character pattern
   
         if( regex.test(password.value) ) {
           return true;
         }
       },
       special: function() {
         var regex = /^(?=.*[_\W]).+$/; // Special character 
         
         if( regex.test(password.value) ) {
           return true;
         }
         
       } ,
       number: function() {
         var regex = /^(?=.*[0-9]).+$/; // number pattern
   
         if( regex.test(password.value) ) {
           return true;
         }
         
       }     
     };
     
     // Listen for keyup action on password field
     password.addEventListener('keyup', function (){
       // Check that password is a minimum of 8 characters
       patternTest( pattern.charLength(), helperText.charLength );
       
       // Check that password contains a lowercase letter    
       patternTest( pattern.lowercase(), helperText.lowercase );
       
       // Check that password contains an uppercase letter
       patternTest( pattern.uppercase(), helperText.uppercase );
       
       // Check that password contains  special character
       patternTest( pattern.special(), helperText.special );
       
       // Check that password contains a number
       patternTest( pattern.number(), helperText.number );
       
       // Check that all requirements are fulfilled
       if( hasClass(helperText.charLength, 'valid') &&
           hasClass(helperText.lowercase, 'valid') && 
           hasClass(helperText.uppercase, 'valid') && 
           hasClass(helperText.number, 'valid') && 
           hasClass(helperText.special, 'valid')
       ) {
         addClass(password.parentElement, 'valid');
         $('#submitpass').attr("disabled", false);
            
     }
       else {
       
       $('#submitpass').prop("disabled", true);
         removeClass(password.parentElement, 'valid');
       $('#error_pass').html('');
     
       }
     });
     
     function patternTest(pattern, response) {
       if(pattern) {
         addClass(response, 'valid');
       }
       else {
         removeClass(response, 'valid');
       }
     }
     
     function addClass(el, className) {
       if (el.classList) {
         el.classList.add(className);
       }
       else {
         el.className += ' ' + className;
       }
     }
     
     function removeClass(el, className) {
       if (el.classList)
           el.classList.remove(className);
         else
           el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
     }
     
     function hasClass(el, className) {
       if (el.classList) {
         console.log(el.classList);
         return el.classList.contains(className);  
       }
       else {
         new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className); 
       }
     }
     
   })();
   
   function checkPasswordMatch() {
       
     var password = $("#password1").val();
       var confirmPassword = $("#password2").val();
      if(confirmPassword =='')
      {
       $('#submitpass').prop("disabled", true);
      }
      else
      {
       $('#submitpass').prop("disabled", false);
     }
       
     if (password != confirmPassword)
     {
        $('#error_con_password').html('');
         $("#divCheckPasswordMatch").html("<span class='text-red'>Passwords does not match!</span>");
       $('#submitpass').prop("disabled", true);
       //$('#submitpass').prop("disabled", true);
     }
     else if(password == confirmPassword)
        {
          $('#submitpass').prop("disabled", false);
           $("#divCheckPasswordMatch").html("<span class='text-green'>Passwords match.</span>");
           //$('#submitpass').attr("disabled", false);
      }
   }
   
   $(document).ready(function () {
      $("#password2").keyup(checkPasswordMatch);
      $('#newpass_id').on('click',function(){
       $("#set_newpass_id").css("display", "block");
       })
   });
   
   
   function validatepass()
   {
   var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,100}/; // number pattern
       if( regex.test(password.value) ) {
         alert(1);
         return true;
        }
        else
        {
         alert(0);
         return false;  
        }
   }
   
   
</script>
<!-- <script>
   $(document).ready(function(){
     $("#submitpass").on('click',function(){
       if($('.password1').val() != "" && $("#password2").val() != ""){
         var pswd = $("#password1").val();
         var salt = pswd.length;
         var salt1 = sha1(salt);
         var password = sha256(sha1(salt1 + pswd));
         $("#password1").val(password);
   
         var pswd_2 = $("#password2").val();
         var salt_2 = pswd_2.length;
         var salt2 = sha1(salt_2);
         var password2 = sha256(sha1(salt2 + pswd_2));
         $("#password2").val(password2);
         return true;
       }else{
         return false;
       }
     })
   
   
   })
</script> -->

