<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Complaint Type
         <small>Add</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>complainttype"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  
                  <!-- main content  -->
                <form class="form-horizontal" method="post" enctype="multipart/form-data" >              
              <div class="form-group" id="complaint_type_name">
                <label class="col-lg-3 control-label col-lg-offset-1">Complaint Type Name <span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <input type="text" name="complaint_type_name"  class="form-control" id="complaint_type_name" value="<?php echo $this->input->post('complaint_type_name'); ?>">
                  <div id="error_complaint_type_name" style="color:#F00"> <?php echo form_error('complaint_type_name'); ?></div>
                </div>
              </div>
              <!--<div class="form-group" id="response_time" >
                <label class="col-lg-3 control-label col-lg-offset-1">Response Time <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <input type="text" name="response_time" class="form-control" id="response_time" value="<?php echo $this->input->post('response_time'); ?>">
                <div id="response_time" style="color:#F00"><?php echo form_error('response_time'); ?> </div>
                </div>
              </div>
             
              <div class="form-group" id="response_time" >
                <label class="col-lg-3 control-label col-lg-offset-1">Resident ID <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <input type="text" name="resident_id" class="form-control" id="resident_id" value="<?php echo $this->input->post('resident_id'); ?>">
                <div id="resident_id" style="color:#F00"><?php echo form_error('resident_id'); ?> </div>
                </div>
              </div>-->
              
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
             
              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <button type="submit" name="submit" class="btn btn-success no-shadow" id="btn_submit">Submit</button>
                </div>
              </div>
            </form>                
      

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>



