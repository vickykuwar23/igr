<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Reminder –Grievance Redressal Portal</title>
   

<body style="font-family:Tahoma, Geneva, sans-serif;">

<table >
    <tr>
        <td></td>
        <td width="600">
            <div>
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            Reminder –Grievance Redressal Portal</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><p>Dear Sir/Madam,</p></td>
                                </tr>
                                <tr></tr>
                                <tr>
                                    <td><p>Following Grievance is  pending for disposal at your end:</p></td>
                                </tr>
                                  <tr></tr>
                                    <tr></tr>
                                <tr>
                                    <td> <div>
                                    <?php echo $msg_content;?>
                                    </div></td>
                                </tr>
                                  <tr></tr>
                                <tr>
                                    <td><p>Please expedite the closure  of the said grievance by updating the status/reply in Grievance Redressal  Portal.</p></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </div>
        </td>
        <td></td>
    </tr>
</table>

</body>

</html>
