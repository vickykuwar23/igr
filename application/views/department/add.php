<div style="text-align:center;">
<?php if ($this->session->flashdata('success_message') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
           <?php echo $this->session->flashdata('success_message') ?>         
      </div>
    <?php } ?>
  </div>
<div style="text-align:center;">
<?php if ($this->session->flashdata('successmsgsend') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
         <?php echo $this->session->flashdata('successmsgsend') ?></br>
           <div><a href="<?php echo base_url();?>callcenterpanel/users" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
    <?php } ?>
  </div>
<div id="grivanceadd_id">
 <section id="content" >
  <section class="main padder">
    <div class="row">
      <div class="col-sm-6 col-xs-offset-3">
        <section class="panel m-t grdbg">
          <header class="panel-heading text-center"><i class="fa fa-edit"></i>Add Department</header>
        
          <div class="panel-body">
            <form class="form-horizontal" method="post" enctype="multipart/form-data" >              
              <div class="form-group" id="department_name">
                <label class="col-lg-3 control-label col-lg-offset-1">Complaint Type Name <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter Complaint Type" ></i>
                <div class="col-lg-7">
                  <textarea name="department_name" maxlength="25" class="form-control" id="department_name"><?php echo $this->input->post('department_name'); ?></textarea>
                  <div id="error_complaint_type_name" style="color:#F00"> <?php echo form_error('department_name'); ?></div>
                </div>
              </div>              
              
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
             
              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <button type="submit" class="btn btn-white">Cancel</button>
                  <button type="submit" name="submit" class="btn btn-regi no-shadow" id="btn_submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>
    
  </section>
</section>
</div>
</div>
<script>

</script>
