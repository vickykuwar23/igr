<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<!-- Latest compiled and minified CSS -->
<!-- jQuery library -->

<!-- Latest compiled JavaScript -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- basic libraries required for datatable Funcyions include : search,pagination,no of records -->
<script src="//cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css" >


<!--Responsive table-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.min.css" >
<script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.bootstrap.min.css" >


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Grievance Portal</title>
<style>
.dataTables_wrapper {
     width: 100%;
     margin: 0px !important; 
}
</style>
</head>
<body>
<div style="text-align:center;">
<?php if ($this->session->flashdata('success_message') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success_message') ?>
      </div>
    <?php } ?>
    <?php if ($this->session->flashdata('invalidcredential') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('invalidcredential') ?>
      </div>
    <?php } ?></div>
    
 <div class="col-md-1 addadmin">   
 <a href="<?php echo base_url();?>index.php/department/add" class="btn btn-warning no-shadow"><i class="fa fa-plus"></i>Add Department</a></div>
<table class="table table-striped table-bordered dt-responsive nowrap" id="table">
	<thead>
			<tr>
				<th class="text-center bluehd" style="width:5% !important;">
					Sr. No.
				</th>
				<th class="text-center bluehd">
					Department Name
				</th>				
				<th class="text-center bluehd">
				 Status
				</th>
				<th class="text-center bluehd">
					Action
				</th>
			</tr>
			</thead>
			<tbody>
		  <?php $sr=0;?>
		  <?php foreach($department_list as $details) {
			  //$category=$this->master_model->getRecords('departments',array('id'=>$details['type']));
			  ?>
			<tr>
			<td class="text-center"><?php echo $sr+1;?></td>
			<td><?php echo ucfirst($details['department_name']);?></td>			
			<td class="text-center"><?php if($details['status'] == 1){echo 'Active';}else{echo 'Inactive';};?></td>
			<td class="text-center">
			<a href="<?php echo base_url();?>index.php/department/edit/<?php echo $details['dept_id'];?>" title=""><i class="fa fa-pencil"></i></a>
			<a href="<?php echo base_url();?>index.php/department/delete/<?php echo $details['dept_id'];?>" title="" 
			onclick="javascript:return deletconfirm();"><i class="ui-tooltip fa fa-trash-o"  data-original-title="Delete"></i></a></td>
			</tr>
		   <?php $sr++?>
		   <?php }?>
			</tbody>
</table>
<div class="clr"></div>
<div style="display:block; margin:0 auto 20px; text-align:center;"><a class="btn btn-success no-shadow" href="<?php echo base_url();?>index.php/subadmin/"><i class="fa fa-angle-double-left"></i>Go back</a></div>
</body>
<script>
$(document).ready(function(){
    $('#table').DataTable({
		dom: 'Bfrtip', //to show the buttons on the screen
        buttons: [    // which buttons should display
        ]
	});
});
</script>
</html>