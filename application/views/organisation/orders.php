<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                       <ul class="aside-list">							
                            <li><a href="<?php echo base_url('organisation/vision'); ?>" <?php if($subtitle == "Vision"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Vision</a></li>
                            <li><a href="<?php echo base_url('organisation/history'); ?>" <?php if($subtitle == "History"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>History</a></li>
                            <li><a href="<?php echo base_url('organisation/structure'); ?>" <?php if($subtitle == "Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Structure</a></li>
                            <li><a href="<?php echo base_url('organisation/office'); ?>" <?php if($subtitle == "Offices"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Offices</a></li>
                            <li><a href="<?php echo base_url('organisation/functions'); ?>" <?php if($subtitle == "Functions"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Functions</a></li>
                            <li><a href="<?php echo base_url('organisation/orders'); ?>" <?php if($subtitle == "Orders"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Orders</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3">
                    <div class="col-md-9 mt-3">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="title-heading">
                                <h1 class="heading">Orders</h1>
                            </div>
                        </div>
                    </div>
                    <div class="inner-content">
							<ul class="points">
								<li><a href="<?php echo base_url(); ?>pdf/Drawing_order.pdf" target="_blank" >Drawing and Disbursing Officers Order</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/6_rti_Officers_list.pdf" target="_blank" >Public Information Officer and Appellate Authority</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/RTI_ORDER-2014.pdf" target="_blank" >RTI ORDER-2014</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/Deligation_order.pdf" target="_blank" >अधिकार प्रत्यार्पित करणेबाबत</a></li>
							</ul>
						</p>					
						
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>

