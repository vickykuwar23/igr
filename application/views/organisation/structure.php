<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                       <ul class="aside-list">							
                            <li><a href="<?php echo base_url('organisation/vision'); ?>" <?php if($subtitle == "Vision"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Vision</a></li>
                            <li><a href="<?php echo base_url('organisation/history'); ?>" <?php if($subtitle == "History"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>History</a></li>
                            <li><a href="<?php echo base_url('organisation/structure'); ?>" <?php if($subtitle == "Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Structure</a></li>
                            <li><a href="<?php echo base_url('organisation/office'); ?>" <?php if($subtitle == "Offices"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Offices</a></li>
                            <li><a href="<?php echo base_url('organisation/functions'); ?>" <?php if($subtitle == "Functions"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Functions</a></li>
                            <li><a href="<?php echo base_url('organisation/orders'); ?>" <?php if($subtitle == "Orders"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Orders</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3">
                    <div class="col-md-9 mt-3">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="title-heading">
                                <h1 class="heading"> Structure</h1>
                            </div>
							
                        </div>
                    </div>
                    <div class="inner-content">
                        <p><img src="../img/Org_Charts_RegistrationStructure.png" class="img-innerlogo"/></p>
                        
						<p>
						<ul>
							<li><b>IGR</b>  Inspector General of Registration</li>
							<li><b>ACS</b>  Additional Controller of Stamps</li>
							<li><b>JDTP</b> Joint Director of Town Planning</li>
							<li><b>DIG</b>  Deputy Inspector General of Registration</li>
							<li><b>COS</b>  Collector of Stamps</li>
							<li><b>JDR</b>  Joint District Registrar</li>
							<li><b>DDTP</b> Deputy Director of Town Planning</li>
							<li><b>ADTP</b> Assistant Director of Town Planning</li>
							<li><b>SRO</b>  Sub registrar's Office</li>
						</ul>
						</p>
						<p>
						<b>Administrative Structure of Registration and Stamp Department:-</b>
						</p>
						<p>
						<ul class="instruction-points">	
							<li>The Registration and Stamp Department is a Department under the Revenue Department of Government of Maharashtra and is under the control of the Minister (Revenue).</li>
							<li>At Mantralaya level, the Secretary (Revenue) is the Secretary of the Department.</li>
							<li>The Inspector General of Registration and Controller of Stamp, Maharashtra State (IGR) is the Head of this Department and his office is located at Pune. IGR is the chief of the machinery for registration of documents in the state. Similarly, as per Stamp Act, he is the Chief Controlling Revenue Authority.</li>
							<li>In the Head office, to assist IGR, there are posts of Superintendent of Stamps (HQ), Deputy Inspector General of Registration (HQ), Deputy Inspector General of Registration (Computerisation) and Deputy Director of Accounts. These officers are assisted by Desk Officers.</li>
							
						</ul>
						</p>
						<p>
						<b>At Field level, the structure of the Department is as follows:</b>
						</p>
						<ul class="instruction-points">	
							<li>There are 507 offices of Sub-Registrars in the entire state for registration of documents. In the rural areas, generally there is one office for each taluka.</li>
							<li>To monitor the offices of sub-Registrars, there are 34 offices of Joint District Registrars at District level.</li>
							<li>There are 8 Regional Divisions of the Department in the State at Mumbai, Pune, Thane, Nashik, Aurangabad, Nagpur, Amravati and Latur and they are under the control of Deputy Inspector General of Registration .</li>
							<li>The Collectors of Stamps of Mumbai City and Mumbai Suburban Districts are under the control of Additional Controller of Stamps, Mumbai.</li>
							<li>The Sub-Registrars of District Headquarters in the State perform the function of special marriages registration. However, there are independent offices of Marriage Officers for the 3 districts Mumbai City, Mumbai Suburban and Pune.</li>
							<li>There are 7 offices of Deputy Director/Assistant Director, Town Planning (Valuation) for preparing Annual Statement of Rates and they are under the control of Joint Director, Town Planning (Valuation) at State level.</li>
							<li>Government Photo Registration Office in Pune functions to preserve the records of documents photographed in the past.
</li>
							
						</ul>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>

