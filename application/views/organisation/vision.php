<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                       <ul class="aside-list">							
                            <li><a href="<?php echo base_url('organisation/vision'); ?>" <?php if($subtitle == "Vision"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Vision</a></li>
                            <li><a href="<?php echo base_url('organisation/history'); ?>" <?php if($subtitle == "History"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>History</a></li>
                            <li><a href="<?php echo base_url('organisation/structure'); ?>" <?php if($subtitle == "Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Structure</a></li>
                            <li><a href="<?php echo base_url('organisation/office'); ?>" <?php if($subtitle == "Offices"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Offices</a></li>
                            <li><a href="<?php echo base_url('organisation/functions'); ?>" <?php if($subtitle == "Functions"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Functions</a></li>
                            <li><a href="<?php echo base_url('organisation/orders'); ?>" <?php if($subtitle == "Orders"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Orders</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3">
                    <div class="col-md-9 mt-3">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <!--<div class="title-heading">
                                <h1 class="heading"> Complaint Redressal System</h1>
                            </div>-->
							<img src="../img/IGRlogo.png" class="img-innerlogo"/>
                        </div>
                    </div>
                    <div class="inner-content">
                        <p>The emblem of the Registration and Stamps Department has Marathi alphabets ‘नो’ and ‘मु’ embedded in a Swastik design. The emblem symbolises speedy and citizen friendly service. 
						The emblem suggests that the department is bound to provide the following:</p>
                        <p>
							<ul>
								<li>1. Efficient and speedy public service</li>
								<li>2. Completion of work in specified time</li>
								<li>3. Transparent work systems</li>
								<li>4. Equal and courteous treatment to all</li>
								<li>5. Citizen friendly seating arrangement</li>
								<li>6. Encouraging work environment</li>
							</ul>
						</p>
						<p>
						<b>Vision :</b> The Registration and Stamps Department provides document registration services to thepeople as per the Registration Act and collects revenue through stamp duty as per the Stamp Act. The vision of the department is to be the best department in providing services of document registration and revenue collection.

						</p>
						<p>
						<b>Mission : </b>The mission of the department is to effectively use modern technology to provide services of document registration and collection of stamp duty to the people using well defined procedures, with right means, in specific time frame and the transparent manner.
						</p>
						
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>

