    <!-- about redresel -->
    <section class="complain-wrapper" id="complain-wrapper">
	  <div class="container-fluid">
            <div class="row">
			 <div class="col-md-2">
			 </div>
        <div class="col-md-8 mt-3">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="title-heading">
                                <h1 class="heading">Terms And Conditions</h1>
                            </div>
                        </div>
                    </div>
                    <div class="inner-content">
                        <p>This Website is designed & developed by National Informatics Center and maintained by Department of Registration and Stamps, Government of Maharashtra.</p>
                        <p>Though all efforts have been made to ensure the accuracy and currency of the content on this website, the same should not be construed as a statement of law or used for any legal purposes. In case of any ambiguity or doubts, users are advised to verify / check with the Department and / or other source, and to obtain appropriate professional advice.</p>
						<p>Under no circumstances with this Department be liable for any expense, loss or damage including, without limitation, indirect or consequential loss or damage, or any expense, loss or damage whatsoever arising from use, or loss of use, of data, arising out of or in connection with the use of this website.</p>
						<p>These terms and conditions shall be governed by and construed in accordance with the Indian Laws. Any dispute arising under these terms and conditions shall be subject to the jurisdiction of the courts of India.</p>
						<p>The information posted on this website could include hypertext links or pointers to information created and maintained by non-Government/private organisations. Department of Registration and Stamps is providing these links and pointers solely for your information and convenience. When you select a link to an outside website, you are leaving the Department of Registration and Stamps website and are subject to the privacy and security policies of the owners / sponsors of the outside website.</p>
						<p>Department of Registration and Stamps, does not guarantee the availability of such linked pages at all times.</p>
					</div>
                </div>
				<div class="col-md-2">
			 </div>
			 </div>
         </div>    
    </section>
    <!-- //about redresel -->

