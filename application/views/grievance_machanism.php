    <!-- about redresel -->
    <section class="complain-wrapper" id="complain-wrapper">
	  <div class="container-fluid">
            <div class="row">
			 <div class="col-md-2">
			 </div>
        <div class="col-md-8 mt-3">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="title-heading">
                                <h1 class="heading">Grievance Redressal Mechanism</h1>
                            </div>
                        </div>
                    </div>
                    <div class="inner-content">
                        <p>Department of Registration and Stamps, Maharashtra State is proposing to set up three-tiered Grievance Redressal Mechanism the details are given below: </p>
                        <p>
						<table border="1" width="100%" class="table table-bordered">
							<tr>
								<th>Redressal Level</th>
								<th>Officer Designation</th>
								<th>Stipulated Timeline</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>SRO / Collector of Stamps</td>
								<td>7 working  days</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>JDR/ Additional Controller of Stamps</td>
								<td>10 working  days</td>
							</tr>
							<tr>
								<td>Level 3 <br />(Independent Grievance Redressal Authority)</td>
								<td>Divisional Commissioner (Konkan Division)</td>
								<td>15 working  days</td>
							</tr>
						</table>						
						</p>
						<p>	<b>Level 1: </b><br />
							Any complaint received from the citizen will have to be responded by the Sub Registrar Officer/ Collector of Stamps within 7 working  days. In case of no response, the complaint will be automatically escalated by the grievance redressal system.  to Joint District Registrar / Additional Controller of Stamps who will serve as the level 2 redressal officer.
						</p>
						<p>	<b>Level 2: </b><br />
							Level 2 officers i.e. JDR/ACS will have to respond to the complaint within 10 working  days of the receipt of the complaint. 
						</p>
						<p>	<b>Level 3: </b><br />
							Divisional Commissioner (Konkan Division) have been identified as the Independent Grievance Redressal Authorities (IGRA), by the Government of Maharashtra. The IGRA will handle the grievances in following scenarios:<br />
							<b>Case 1:</b> In case of no response, the system will not auto-escalate the grievance to the IGRA. Citizens will have an option to escalate the complaint to the respective Divisional Commissioner (Konkan Division), who have been declared as the IGRA by the Government of Maharashtra.<br />
							<b>Case 2:</b> In case of responded grievances, which are closed by Level 2 officers, but the resolution provided is not satisfactory to the citizen, citizens will have an option to escalate the complaint to the respective Divisional Commissioner (Konkan Division) i.e. IGRA.

						</p>
						<p>Currently grievances for <b>Mumbai City and Mumbai Suburban</b> areas can be lodged through this portal.</p>
						<p>The Government Resolution identifying the Divisional Commissioner (Konkan Division) as the IGRA is attached <a href="<?php echo base_url('user_manual/GR - Grievance Redressal System.pdf') ?>" target="_blank">here</a>.</p>
                    </div>
                </div>
				<div class="col-md-2">
			 </div>
			 </div>
         </div>    
    </section>
    <!-- //about redresel -->

