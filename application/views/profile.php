    <style>
     .form-group h4 {
                font-size: 16px;
                color: #0D4F89;
                font-weight: 500;
            }
            .profile-wrapper {
                padding: 40px 0 0;
            }
            .profile_userView{
              background-color: #FFF;
              position: relative;
              padding: 10px 15px 1px;
              border-radius: 10px;
              margin-bottom: 30px;
              box-shadow: 0 4px 5px 0 rgba(0,0,0,0.1), 0 0 5px 0 rgba(0,0,0,0.1);
            }
            .userPro-head h3{
              color: #0D4F89;
              font-size: 20px;
              margin-bottom: 15px;
              text-align: center;
            }


     </style>
    <div class="profile-wrapper">
            <div class="container bootstrap snippet">
                <div class="row">
                    
                    <!--/col-3-->
                    <div class="col-sm-8 mx-auto">
                      <div class="userPro-head"> 
                          <h3>User Profile </h3>
                        </div>
        <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
                        <div class="tab-content profile_userView">
                            <div class="tab-pane active" id="home">
                                <form class="form"  method="post" id="registrationForm">
                                 
                                    <div class="form-group">
                                      
                                        <div class="col-xs-6">
                                            <label for="first_name"><h4>Name</h4></label>
                                             <input type="text" name="name" data-required="true" class="form-control" value='<?php echo $user_info[0]['user_name']?>' id="name" maxlength="100"> 
                         <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('name');?></div>
                                        </div>
                                    </div>
                                    

     

                                    <div class="form-group">

     

                                        <div class="col-xs-6">
                                            <label for="phone"><h4>Username</h4></label>
                                            <input type="text" name="username" readonly="true" data-required="true" class="form-control" value='<?php echo $user_info[0]['uniqueusername']?>' id="username" maxlength="25"> 
                         <div class="m-t m-t-mini error" style="color:#F00" id="usererror"> <?php echo form_error('username');?></div>
                         <div class="m-t m-t-mini " id="usernamechk"  style="text-align:left"></div>
                                        </div>
                                    </div>

                                    <div class="form-group">

     

                                        <div class="col-xs-6">
                                            <label for="email"><h4>Email</h4></label>
                                            <input type="text" readonly="true" name="email" class="form-control" data-required="true" data-type="email" value='<?php echo $user_info[0]['user_email']?>' id="email">
                       <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('email');?></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-6">
                                            <label for="mobile"><h4>Mobile</h4></label>
                                           <input type="text" readonly="true" name="mobile" class="form-control" value='<?php echo $user_info[0]['user_mobile']?>' id="mobile" maxlength="10">
                      <div class="m-t m-t-mini error" style="color:#F00"><?php echo form_error('mobile');?></div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">

     

                                        <div class="col-xs-6">
                                            <label for="email"><h4>Landline No.</h4></label>
                                           <input type="text" name="landline" class="form-control" value='<?php echo $user_info[0]['user_landline']?>' id="landline" maxlength="15">
                                            <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('landline');?></div>
                                        </div>
                                    </div>
                                    <div class="form-group">

     
                                        <div class="col-xs-6">
                                            <label for="email"><h4>Aadhar card number</h4></label>
                                           <input type="text" name="aadhar_number" maxlength="16" class="form-control" value='<?php echo $user_info[0]['aadhar_number']?>'>
                         <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('aadhar_number');?></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-6">
                                            <label for="email"><h4>Address Line 1</h4></label>
                                           <input type="text" name="address1" class="form-control" value='<?php echo $user_info[0]['user_address1']?>' id="address1">
                       <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('address1');?></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-6">
                                            <label for="email"><h4>Address Line 2</h4></label>
                                           <input type="text" name="address2" class="form-control" value='<?php echo $user_info[0]['user_address2']?>' id="address2">
                       <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('address2');?></div>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <div class="col-xs-6">
                                            <label for="email"><h4>State</h4></label>
                                           <select class="form-control states" name="state" class="states">
                      <option value="">Select State</option>
                      <?php foreach($state as $states) {?>
                        <option value="<?php echo $states['ch2']?>" <?php if($states['ch2']==$user_info[0]['user_state']){echo "selected='selected'";}?>><?php echo $states['ch3']?></option>
                      <?php } ?>
                      </select>
                       <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('state');?></div>
                                        </div>
                                    </div>
                         <div class="form-group">
                                        <div class="col-xs-6">
                                            <label for="email"><h4>City</h4></label>
                                           <select class="form-control" name="city" id="selectcity">
                     <option value="">Select City</option>
                        <?php 
                                        if(count($city) > 0)
                                {
                                    foreach($city as $res)
                                    {?>
                                        <option value="<?php echo $res['ch4']?>" <?php if($res['ch4']==$user_info[0]['user_city']){echo "selected='selected'";}?>><?php echo $res['ch4'];?></option>
                                <?php }
                                }?>
                      </select>
                        <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('city');?></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-6">
                                            <label for="email"><h4>Pincode</h4></label>
                                          <input type="text" name="pincode"  placeholder="" class="form-control" value='<?php echo $user_info[0]['user_pincode']?>' id="pincode" maxlength="6"> 
                        <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('pincode');?></div>
                                        </div>
                                    </div>
                                    
                                   
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <br>
                                            <ul class="frm-btn-grp">
                                            <li><button type="submit" name="btn_reg" class="btn btn_submit" id="btn_submit">Update</button></li>
                                            <li><a class="btn_cancel btn btn-white" id="cancel" href="<?php echo base_url('complaint') ?> "><i class="fa fa-angle-double-left"></i>Go back</a></li> 
                                            <!-- <div style="display:block; margin:0 auto 20px; text-align:center;"></div> -->
                                          </ul>
                                            <!-- <button class="btn btn-submit" name="btn_reg" type="submit">Save</button> -->
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--/tab-pane-->
                    </div>
                    <!--/tab-content-->
                </div>
                <!--/col-9-->
            </div>
        </div>
        
        