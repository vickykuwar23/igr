 <div style="text-align:center;">
 <?php if($error_msg != "") { ?>
<div class="alert alert-error" style="font-size:17px;">
<button data-dismiss="alert" class="close" type="button">×</button>
<strong>Warning!</strong> <?php echo $error_msg ?>
</div>
<?php } ?>  

<?php 

if ($this->session->flashdata('expire_session') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('expire_session') ?>
      </div>
    <?php } ?>
    
    
<?php if ($this->session->flashdata('success_message') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success_message') ?>
      </div>
    <?php } ?>
    <?php if ($this->session->flashdata('invalidcredential') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('invalidcredential') ?>
      </div>
    <?php } ?>
    <?php if ($this->session->flashdata('olduser') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('olduser') ?>
      </div>
    <?php } ?>
  </div>

<!-- login form -->
    <div class="login-wrapper">
        <div class="d-flex justify-content-center h-100">
            <div class="user_card">
                <div class="d-flex justify-content-center form_container">
                    <form class="w-100" method="post" action="" id="my_form" autocomplete="off">
                        <div class="form-heading">
                            <h3>Citizen's Login</h3>
                        </div>
                        <div class="form-grp-filed">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                </div>
                                <input id="username" required name="username_mobile" autocomplete="off" type="text"
                                    name="" class="form-control input_user" value="" placeholder="username">
                            </div>
                            <div class="input-group mb-3" id="show_hide_password">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-key"></i></span>
                                </div>
                                <input id="inputPassword" required name="password" autocomplete="off" type="password"
                                    name="" class="form-control input_pass" value="" placeholder="password">
                                    <div class="input-group-addon eye_pswd">
        <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
      </div>
                            </div>
                            <div class="form-group mb-3">
                                <div class="row">
                                    <div class="col-sm-4 pr-0">
                                        <label class="loginlable control-label ">
                                            <?php //echo $image; ?>
                                             <span id="captImg"><?php echo $image; ?></span>
                                                  
                                            </label>
                                        <!--<img src="images/captcha.jpg" class="img-fluid" style="
    height: 45px;
    width: 100%;
"> -->
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <input type="text" id="code" name="code"
                                            class="form-control captcha_input input_pass" value=""
                                            placeholder="captcha code">
                                        <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('code');?>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 pl-0 align-self-center">
                                        <a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh"></i></a>  
                                    </div>
                                </div>
                            </div>
                            <div class="mt-3">
                                <button type="submit" name="button" class="btn login_btn" name="btn_submit"
                                    id="btn_submit">Sign In</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="mt-4">
                    <div class="d-flex justify-content-center links">
                        Don't have an account? <a href="<?php echo base_url();?>userlogin/registration"
                            class="ml-2 register-nw-lnk">Register Now</a>
                    </div>
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>"
                        value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="d-flex justify-content-center links frgt-pswd">
                        <a href="<?php echo base_url();?>userlogin/forgetpass/">Forgot your password?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //login form-->

 <script>
  $(document).ready(function(){

        $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
    
    // $("#btn_submit").click(function(){
        
    //               var pswd = $("#inputPassword").val();
    //               var salt = pswd.length;
    //               var salt1 = sha1(salt);
    //               var password = sha1(sha1(salt1 + pswd));
    //               $("#inputPassword").val(password);
    // })

    $('form').submit( function(event) {

    var formId = this.id,
        form = this;
    event.preventDefault();
          if($('.username').val() != "" && $("#inputPassword").val() != ""){
              
                //   var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
          var username=$("#username").val();
        //  var datastring='username='+ username;// + '&' + csrfName + '='+csrfHash;
        //   $.ajax({
            // url: "<?php echo base_url();?>userlogin/is_new",
            // type: 'POST',
            // data:datastring,
            // success : function(data){
            //   if(data){

                  var pswd = $("#inputPassword").val();
                  var salt = pswd.length;
                  var salt1 = sha1(salt);
                  var password = sha256(sha1(salt1 + pswd));
                  $("#inputPassword").val(password);
            //   }
              
            //   }
            // });
           
          }
        //   else{
        //     return false;
        //   }
    setTimeout( function () { 
        form.submit();
    }, 1000);
}); 


   

  
  });
 </script>

   <script>
         $(document).ready(function(){
             $('.refreshCaptcha').on('click', function(){
                 $.get('<?php echo base_url().'userlogin/refresh'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
         });
      </script>

