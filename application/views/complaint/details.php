
<div class="container">
	<div class="row">
		<div class="col-md-12 justify-content-end addadmin">   
		<a href="<?php echo base_url();?>complaint" class="btn btn-sm btn-warning no-shadow"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back To Listing</a></div>
 	</div>
</div>
<div class="container"> 
<div class="mb-4">
<div class="row mb-4">
	<div class="col-md-12">
		<div class="title-heading">
			<h1 class="heading"> Complaint Details</h1>
		</div>
	</div>
</div>
<?php 
error_reporting(0); 
$user_details=$this->master_model->getRecords('userregistration',array('user_id' => $complaint_details[0]['user_id']),'','');
$category_details=$this->master_model->getRecords('gri_complaint_type',array('complaint_type_id' => $complaint_details[0]['complaint_type_id']),'','');
$subcategory_details=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id' => $complaint_details[0]['complaint_sub_type_id']),'','');
$region_details=$this->master_model->getRecords('gri_region_division',array('religion_id' => $complaint_details[0]['religion_id']),'','');
$district_details=$this->master_model->getRecords('gri_district',array('district_id' => $complaint_details[0]['district_id']),'','');
$sro_details=$this->master_model->getRecords('gri_sro_offices',array('sro_office_id' => $complaint_details[0]['sro_office_id']),'','');

$subject=$this->master_model->getRecords('gri_subject',array('sub_id' => $complaint_details[0]['comp_subject']),'','');

$assign_details=$this->master_model->getRecords('gri_complaint_assign',array('comp_code' => $complaint_details[0]['comp_code'], 'status' => '1'),'','');
if($complaint_details[0]['reply_status'] == 1){$status = 'Pending';} else if($complaint_details[0]['reply_status'] == 2){$status = 'Fake';}else if($complaint_details[0]['reply_status'] == 3){$status = 'Query to Complainant';}else{$status = 'Closed';}
$officer_details=$this->master_model->getRecords('adminlogin',array('id' => $assign_details[0]['assign_user_id']),'','');
$role_details=$this->master_model->getRecords('gri_roles',array('role_id' => $officer_details[0]['role_id']),'','');
$doc_details=$this->master_model->getRecords('gri_complaint_images',array('comp_code' => $complaint_details[0]['comp_code']),'','');
$replymsg=$this->master_model->getRecords('complainreply',array('complain_id'=>$complaint_details[0]['c_id']),'',array('reply_id'=>'ASC'));
$enddate = $assign_details[0]['end_date'];
$e_Date 	= date('Y-m-d', strtotime($enddate));
$curr_date  = date('Y-m-d');
$assignTo = '';
if($role_details[0]['role_id'] == 1){
	$assignTo = 'Report TO JDR';
} else if($role_details[0]['role_id'] == 7){
	$assignTo = 'Report TO ACS';
}
?>
 <div class="complaint_details">
	<div class="row">
		<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Complaint Code</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><?php echo $complaint_details[0]['comp_code']; ?></label>
		</div>
		<!--<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Name</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><?php echo $user_details[0]['user_name']; ?></label>
		</div>-->
	</div>
	
	<div class="row">
		<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Complaint Belongs To</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><?php echo $category_details[0]['complaint_type_name']; ?></label>
		</div>
		<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Office Type</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><?php if($subcategory_details[0]['complaint_sub_type_name']): echo $subcategory_details[0]['complaint_sub_type_name']; else: echo 'None';  endif; ?></label>
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Region</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><?php echo $region_details[0]['region_division_name']; ?></label>
		</div>
		<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">District</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><?php echo $district_details[0]['district_name']; ?></label>
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Office Name</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><?php if($sro_details[0]['office_name']): echo $sro_details[0]['office_name']; else: echo 'None'; endif; ?></label>
		</div>
		<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Complaint Date</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><?php echo date('d-m-Y',strtotime($complaint_details[0]['complaint_date'])); ?></label>
		</div>
	</div>
	<?php if($subject){?>
	<div class="row">
		<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Subject</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><?php echo $subject[0]['subject_name']; ?></label>
		</div>
	</div>
	<?php } ?>
	<div class="row">
		<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Complaint Status</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><button type="button" class="btn btn-success btn-sm"><?php echo $status; ?></button></label>
		</div>
		<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Assign Officer</label>
			<?php echo $role_details[0]['role_name']; ?>&nbsp; 
			<?php
			
			if ($complaint_details[0]['complaint_type_id']!=1) { ?>
				<?php if($complaint_details[0]['is_level_1'] == 1 && $complaint_details[0]['reply_status'] == 0): ?><form method="post" name="fm" id="fm" action=""><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" /><label for="staticEmail" class="col-md-7 col-form-label"><input type="hidden" name="c_code" value="<?php echo $complaint_details[0]['comp_code']; ?>" /><input type="submit" name="report_to_next" id="report_to_next" class="btn btn-warning btn-sm" value="<?php echo $assignTo; ?>"></label></form><?php endif; ?>
			<?php }

			?>	
			
		</div>
		<!--<div class="form-group col-md-6 d-flex">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">Subject</label>
			<div class="col-md-7"><a href="#" type="button" class="btn btn-info">Download <i class="fas fa-file-download"></i></a></div>
		</div>-->
	</div>
	<hr/>
	<div class="row">
		<div class="form-group col-md-12 d-flex mb-0">
			<label for="staticEmail" class="col-md-3 col-form-label font-weight-bold">Grievance Details</label>
			<label for="staticEmail" class="col-md-9 col-form-label"><?php echo $complaint_details[0]['grievence_details']; ?></label>
		</div>
	</div>
	<?php if($complaint_details[0]['relief_required']!=""){ ?>
	<hr/>
	<div class="row">
		<div class="form-group col-md-12 d-flex mb-0">
			<label for="staticEmail" class="col-md-3 col-form-label font-weight-bold">Relief Details</label>
			<label for="staticEmail" class="col-md-9 col-form-label"><?php echo $complaint_details[0]['relief_required']; ?></label>
		</div>
	</div>
	<?php } ?>
	<?php if(count($doc_details) > 0){ ?>
	<hr/>
	<div class="row">
		<div class="form-group col-md-6 d-flex mb-0">
			<label for="staticEmail" class="col-md-5 col-form-label font-weight-bold">View Document</label>
			<label for="staticEmail" class="col-md-7 col-form-label"><a style="font-size: 24px;" href="<?php echo base_url('images/complaint_images/'.$doc_details[0]['image_name']); ?>" target = "_blank"><i class="fa fa-download"></i></a></label>
		</div>
	</div>
	<?php } ?>	
	
	<?php if(count($replymsg) > 0){ ?>
		<hr/>
		<div class="row">
			<div class="col-md-12">
				<div class="title-heading">					
					<h5 class="heading">&nbsp; Reply Details</h5>
				</div>
			</div>
		</div>
		
		
	<div class="row mx-0">	
		<?php		
	    
		foreach($replymsg as $replayinfo){
				$this->db->join('gri_roles','gri_roles.role_id=adminlogin.role_id');
$officer_details=$this->master_model->getRecords('adminlogin',array('id' => $replayinfo['user_id']),'role_name');
		?>
		<div class="col-md-6 reply_box">	
				<div class="row">
					<label for="staticEmail" class="col-md-4 col-form-label">Replied Details</label>
					<label for="staticEmail" class="col-md-8 col-form-label"><?php echo $replayinfo['reply_msg']; ?></label>
				</div>
				<div class="row">
					<label for="staticEmail" class="col-md-4 col-form-label">Replied By</label>
					<label for="staticEmail" class="col-md-8 col-form-label"><?php echo $officer_details[0]['role_name']; ?></label>
				</div>
				<div class="row">
					<label for="staticEmail" class="col-md-4 col-form-label">Replied On</label>
					<label for="staticEmail" class="col-md-8 col-form-label"><?php echo date('d M, Y',strtotime($replayinfo['reply_date'])); ?></label>
				</div>
				<?php if($replayinfo['reply_file']){ ?>
				<div class="row">
					<label for="staticEmail" class="col-md-4 col-form-label">Document View</label>
					<label for="staticEmail" class="col-md-8 col-form-label"><a style="font-size: 24px;" href="<?php echo base_url('images/reply_uploads/'.$replayinfo['reply_file']); ?>" target="_blank"><i class="fa fa-download"></i></a></label>
				</div>
			<?php } ?>
		</div>
		<?php	}	// Foreach End ?>
		</div>
		<?php } // Count End ?>
	
	
	
	
	</div>
	
</div>
</div>

<div class="clr"></div>

