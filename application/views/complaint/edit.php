<div style="text-align:center;">
<?php if ($this->session->flashdata('success_message') != '') { ?>
 <div class="alert alert-warning" style="font-size:17px;">
	   <?php echo $this->session->flashdata('success_message') ?>         
  </div>
<?php } ?>
</div>

<div id="grivanceadd_id">
 <section id="content" >
  <section class="main padder">
    <div class="row">
      <div class="col-sm-6 col-xs-offset-3">
        <section class="panel m-t grdbg">
          <header class="panel-heading text-center"><i class="fa fa-edit"></i>Update Complaint Type</header>
        
          <div class="panel-body">
            <form class="form-horizontal" method="post" enctype="multipart/form-data" >              
              <div class="form-group" id="complaint_type_name">
                <label class="col-lg-3 control-label col-lg-offset-1">Complaint Type Name <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter Complaint Type" ></i>
                <div class="col-lg-7">
                  <input type="text" name="complaint_type_name"  class="form-control" id="complaint_type_name" value="<?php echo $get_complaint_date[0]['complaint_type_name'];?>">
                  <div id="error_complaint_type_name" style="color:#F00"> <?php echo form_error('complaint_type_name'); ?></div>
                </div>
              </div>
              <div class="form-group" id="response_time" >
                <label class="col-lg-3 control-label col-lg-offset-1">Response Time <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter Response Time"></i>
                <div class="col-lg-7">
                <input type="text" name="response_time" class="form-control" id="response_time" value="<?php echo $get_complaint_date[0]['response_time'];?>">
                <div id="response_time" style="color:#F00"><?php echo form_error('response_time'); ?> </div>
                </div>
              </div>
             
              <div class="form-group" id="response_time" >
                <label class="col-lg-3 control-label col-lg-offset-1">Resident ID <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter Resident ID"></i>
                <div class="col-lg-7">
                <input type="text" name="resident_id" class="form-control" id="resident_id" value="<?php echo $get_complaint_date[0]['resident_id'];?>">
                <div id="resident_id" style="color:#F00"><?php echo form_error('resident_id'); ?> </div>
                </div>
              </div>
              
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
             
              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <button type="submit" class="btn btn-white">Cancel</button>
                  <button type="submit" name="submit" class="btn btn-regi no-shadow" id="btn_submit">Update</button>
                </div>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>
    
  </section>
</section>
</div>
</div>
<script>

</script>
