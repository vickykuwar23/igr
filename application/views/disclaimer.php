    <!-- about redresel -->
    <section class="complain-wrapper" id="complain-wrapper">
	  <div class="container-fluid">
            <div class="row">
			 <div class="col-md-2">
			 </div>
        <div class="col-md-8 mt-3">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="title-heading">
                                <h1 class="heading">Disclaimer</h1>
                            </div>
                        </div>
                    </div>
                    <div class="inner-content">
                        <p>This Website is designed, developed & hosted by ESDS Software Solutions Pvt. Ltd and maintained by Department of Registration & Stamps, Govt. of Maharashtra.</p>
						<p>The contents of this website are for information purposes only, enabling the public at large to have a quick and an easy access to information and do not have any legal sanctity. Though every effort is made to provide accurate and updated information, it is likely that the some details such as telephone numbers, names of the officer holding a post, etc may have changed prior to their updation in the website. Hence, we do not assume any legal liability on the completeness, accuracy or usefulness of the contents provided in this website.</p>
						<p>The links are provided to other external sites in some web pages/documents. We do not take responsibility for the accuracy of the contents in those sites. The hyperlinks given to external sites do not constitute an endorsement of information, products or services offered by these sites.</p>
						<p>Despite our best efforts, we do not guarantee that the documents in this site are free from infection by computer viruses etc.</p>
						<p>We welcome your suggestions to improve this website and request that error(if any) may kindly be brought to our notice.</p>
                        
                    </div>
                </div>
				<div class="col-md-2">
			 </div>
			 </div>
         </div>    
    </section>
    <!-- //about redresel -->

