<div style="text-align:center;">
 <?php if ($this->session->flashdata('success_message') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
         <button data-dismiss="alert" class="close" type="button" id="forget_close">×</button>
         <?php echo $this->session->flashdata('success_message') ?>
         </br>
           <div><a href="<?php echo base_url();?>superadminlogin/suplogin/" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
      <div class="min380"></div>
    <?php } ?>
    <?php if ($this->session->flashdata('failsend') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
         <button data-dismiss="alert" class="close" type="button" id="forget_close">×</button>
         <?php echo $this->session->flashdata('failsend') ?>
      </div>
    <?php } ?>
    
    <?php if ($this->session->flashdata('wrongemail') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
         <button data-dismiss="alert" class="close" type="button" id="forget_close">×</button>
         <?php echo $this->session->flashdata('wrongemail') ?>
      </div>
    <?php } ?>
    </div>
      <?php if ($this->session->flashdata('forget_galat_jawab') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button" id="newpass_id">×</button>
         <?php echo $this->session->flashdata('forget_galat_jawab') ?>
           </br>
           <div><a href="<?php echo base_url();?>index.php/userlogin/" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
    <?php } ?></div>
    <?php 
if ($this->session->flashdata('success_message') != '') 
{ $display='style="display:none;"';
 }
 else
 {
$display='style="display:block;"';
}?>
<div <?php echo $display;?> id="forget_id">   
 <section id="content" class="login-wrapper">
  <div class="d-flex justify-content-center h-100">
    <div class="user_card">
      <div class="d-flex justify-content-center form_container">
          <form class="w-100 form-horizontal" method="post" autocomplete="off">
             <div class="form-heading">
                <h3>Forgot password</h3>
              </div>
              <div class="form-grp-filed">
                <div class="mb-3">
                    <label class="control-label loginlable">Your Email</label>
                    <input type="email" placeholder="Email Id" class="form-control" name="email" required>
                 </div>
                  <div class="form-group row captcha">
                <label class="col-lg-4 control-label text-left cptcha_img">
                  <span id="captImg"><?php echo $image; ?></span>
                    
                  </label>
                <div class="col-lg-6">
                  <input type="text" name="code" placeholder="" class="form-control"  id="code"> 
                    <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('code');?></div>
                   <p class="text-left blue">Please enter the text you see in the image below into the text box provided.</p>
                </div>
                <div class="col-lg-2">
                  <a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh"></i></a> 
                </div>
              </div>
                <div class="mt-3">
                      <button type="submit" class="login_btn btn" name="submitforgetpass">Send mail</button>
                    </div>
            </div>
             <div class="d-flex justify-content-center links frgt-pswd">
                        <a href="<?php echo base_url();?>superadminlogin/suplogin">Go back</a>
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
          </div>
          </form>
         
      </div>
    </div>
  </div>
</section>
</div>

<script>
$(document).ready(function(){
	$('#forget_close').on('click',function(){
		$("#forget_id").css("display", "block");
		})
	});
	
</script>
<script>
         $(document).ready(function(){
             $('.refreshCaptcha').on('click', function(){
                 $.get('<?php echo base_url().'superadminlogin/refresh_forget'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
         });
</script>