    <!-- carousel -->
    <div id="demo" class="carousel slide banner-bg" data-ride="carousel"
        style="background-image: url(img/fix-bg.jpg);">
        <!-- Indicators -->
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>
        <div class="carousel-inner">
            <!-- slide 1 -->
            <div class="carousel-item active">
                <div class="container" id="sliderBox">
                    <div class="row">
                        <div class="col-sm-6" id="slideBoxLeft">
                            <div class="banner_img">
                                <img src="img/new_banner01.jpg" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-sm-6" id="slideBoxRight">
                            <div class="banner_img">
                                <img src="img/new_banner02.jpg" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- slide 2 -->
            <div class="carousel-item">
                <div class="container" id="sliderBox">
                    <div class="row">
                        <div class="col-sm-6" id="slideBoxLeft">
                            <div class="banner_img">
                                <img src="img/new_banner03.jpg" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-sm-6" id="slideBoxRight">
                            <div class="banner_img">
                                <img src="img/new_banner04.jpg" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- slide 3 -->
            <div class="carousel-item">
                <!-- <div class="banner-bg" style="background-image: url(images/fix-bg.jpg);"> -->
                <div class="container" id="sliderBox">
                    <div class="row">
                        <div class="col-sm-6" id="slideBoxLeft">
                            <div class="banner_img">
                                <img src="img/new_banner05.jpg" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-sm-6" id="slideBoxRight">
                            <div class="banner_img">
                                <img src="img/new_banner06.jpg" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- arrows slider -->
        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>

        <!-- //arrows slider -->
    </div>
    <!-- end carousel -->
	</div>

    <!-- news ticker -->

    <div class="ticker1 modern-ticker mt-round mt-0">
            <div class="content">
                <div class="simple-marquee-container">
                    <div class="marquee-sibling">
                        Latest News:
                    </div>
                    <div class="marquee">
                        <ul class="marquee-content-items">
                            <li> Grievance Redressal Portal of Department of Registration and Stamps.</li>
                           <!--  <li> The Portal is functional for Mumbai City and Mumbai Suburban’ regions only" & "Beta Mode. </li> -->
                           <li>Grievances for Mumbai City and Mumbai Suburban Regions can be lodged through this portal.</li>
                        </ul>
                    </div>
                </div>
            </div>
      <!--   <div class="mt-body">
            <div class="mt-news">
                <div class="marquee">
                <ul class="marquee-content-items">
                    <li><a href="javascript:void(0);">Grievance Redressal Portal of Department of Registration and Stamps.</a></li>
                 
                </ul>
                </div>
            </div>
        </div> -->
    </div>
    <!-- //news ticker -->

    <div class="clearfix"></div>

    <!-- main content -->
    <section class="complain-wrapper" id="complain-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-heading">
                        <h1 class="heading"> Grievance Redressal System</h1>
                    </div>
                </div>
            </div>
            <!-- complaint-list -->
            <div class="row mt-4">
                <div class="col-sm-4">
                    <div class="box-complaint">
                        <a href="<?php echo base_url('userlogin');?>" class="complaint_link">
                            <span class="complain-icon">
                                <img src="<?php echo base_url();?>img/ico-lodge.png" class="img-fluid">
                            </span>
                            <p class="complain-txt"> Lodge Your <br />Complaint</p>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-complaint">
                        <a href="<?php echo base_url('userlogin');?>" class="complaint_link">
                            <span class="complain-icon">
                                <img src="<?php echo base_url();?>img/ico-view.png" class="img-fluid">
                            </span>
                            <p class="complain-txt">View Status of Your Complaint</p>
                        </a>
                    </div>
                </div>
                
				<div class="col-sm-4">
                    <div class="box-complaint">
					<?php 
					 if($this->session->userdata('user_id')!= ""){
						$loginUrl = base_url('home/notifyigra'); 
					 } else {
						 $loginUrl = base_url('userlogin');
					 }
					?>
                        <a href="<?php echo $loginUrl;?>" class="complaint_link">
                            <span class="complain-icon">
                                <img src="<?php echo base_url();?>img/ico-lodge.png" class="img-fluid">
                            </span>
                            <p class="complain-txt">Report To <br />Independent Grievance Authority</p>
                        </a>
                    </div>
                </div>
            </div>
            <!-- //complaint-list -->
        </div>
    </section>
    <!-- //main content -->

    <!-- instruction content -->
    <section class="instruction-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-heading">
                        <h2 class="sub-heading">Please Lodge your complaints related to Department of Registration &
                            Stamps
                            Government of Maharashtra.</h2>
                        <strong>Please Read instructions carefully before registering your complaint</strong>
                    </div>
                </div>
                <div class="col-md-8 mt-3">
                    <ul class="instruction-points">
                        <li>Please write your complaint in brief and bullet points.</li>
                        <li>Please mention your mobile number which will help in communication through sms.</li>
                        <li>Specify the details of complaint,if already lodged.</li>
                        <li>Please avoid lodging complaints about pending/awaiting court matters.</li>
                        <li>Do specify your category of complaint .</li>
                        <li>Remember your complaint token number for further communication.</li>
                        <li>Use UNICODE font to lodge complaint in Marathi language</li>
                        <li>Complainer will be responsible for wrong complaints.</li>
                    </ul>
                </div>
                <div class="col-md-4 align-self-center">
                    <div class="values-insturctions">
                        <ul>
                            <!-- <li> Total Visitors : <span class="val-txt"><?php echo count($all_visit); ?></span></li> -->
                            <!-- <li> Today's Visitors : <span class="val-txt"><?php echo count($today_visit); ?></span></li> -->
							<li> Total Complaints : <span class="val-txt"><?php echo count($all_complaint); ?></span></li>
							<li> Today's Complaints : <span class="val-txt"><?php echo count($today_complaint); ?></span></li>
                            <li> Resolved Complaints : <span class="val-txt"><?php echo count($resolved_complaint); ?></span></li>
							<li> Pending Complaints : <span class="val-txt"><?php echo count($pending_complaint); ?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //instruction content