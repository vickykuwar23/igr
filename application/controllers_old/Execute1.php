<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Execute extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();		
    }
	
	public function index()
	{
		
		$current_date = date('Y-m-d 00:00:00');	 
		$this->db->select('*');
		$this->db->from('gri_complaint');
		$this->db->join('gri_complaint_assign', 'gri_complaint.comp_code = gri_complaint_assign.comp_code');
		$this->db->where('gri_complaint.reply_status', 1);
		$this->db->where('gri_complaint_assign.end_date <', $current_date);
		$this->db->where('gri_complaint_assign.status', 1);
		$query = $this->db->get();
		$num_rows = $query->num_rows();	
		$result = $query->result_array();
		//echo "<pre>";	
		//print_r($result);// die();
		if($num_rows > 0){
			
			foreach($result as $row){				
				
				// Online Service Part
				if($row['complaint_type_id'] == 1){
					
					$assignOnline = $this->master_model->getRecords('adminlogin',array('role_id'=>'4', 'status'=>'1'),'','');
					$assignRoleID = $assignOnline[0]['role_id'];
					
					// Current Date			
					$current_date = date('Y-m-d 00:00:00');
					
					//Means HelpDesk TO IGRA USER Role ID = 4 Means HelpDesk
					if($current_date > $row['end_date'] && $assignRoleID == '4'){				 
						
						//Old Status Update
						$update_status		=	array('status'	=> '0');						
						$this->master_model->updateRecord('gri_complaint_assign',$update_status,array('comp_code'=>"'".$row['comp_code']."'"));
						
						// Assign Complaint To IGRA User
						$getAssignuser = $this->master_model->getRecords('adminlogin',array('role_id'=>'3', 'status'=>'1'));
						$igraID 		= $getAssignuser[0]['id'];
						
						// Assign Days
						$date_com 	= date('Y-m-d');
						$endDate 	= date('Y-m-d', strtotime($date_com. ' + 17 days'));
						$createdAt	= date('Y-m-d H:i:s');
						
						// Insert Array
						$insert_arr = array('comp_code' 		=> $row['comp_code'],
											'assign_user_id' 	=> $igraID,
											'assign_date' 		=> $current_date,
											'end_date' 			=> $endDate,
											'status' 			=> '1',
											'createdat' 		=> $createdAt,
											'updatedat' 		=> $createdAt);					
						
						$igrQuery = $this->master_model->insertRecord('gri_complaint_assign',$insert_arr);
						
						//email admin sending code 
						/*$info_arr=array(
								'to'=>$front_email,					
								'cc'=>$sro_email,
								'from'=>$this->config->item('MAIL_FROM'),
								'subject'=>'Reminder – Grievance Redressal Portal',
								'view'=>'chron_mail'
								);

						$other_info=array('msg_content'=>$row['grievence_details']); 
						$emailsend=$this->email_sending->sendmail($info_arr,$other_info);
						if($emailsend)
						{				
							$this->session->set_flashdata('success_message','Your Complaint has been registered and your Grievance ID is <strong>'.$pattern.'</strong>');
							redirect(base_url().'complaint');
						}*/
						
					}					
					
				} else if($row['complaint_type_id'] == 2){
					
					// Specific Office Else Part
					$assignOnline = $this->master_model->getRecords('adminlogin',array('id'=>$row['assign_user_id'], 'status'=>'1'),'','');
					$assignRoleID = $assignOnline[0]['role_id'];
					
					// Current Date			
					$current_date = date('Y-m-d 00:00:00');
					
					//Means SRO TO JDR USER Role ID = 1 Means SRO
					if($current_date > $row['end_date'] && $assignRoleID == '1'){
						
						//Old Status Update
						$update_status		=	array('status'	=> '0');						
						$this->master_model->updateRecord('gri_complaint_assign',$update_status,array('comp_code'=>"'".$row['comp_code']."'"));
						
						// Assign Complaint SRO To JDR User
						$getAssignuser = $this->master_model->getRecords('adminlogin',array('region_id'=> $row['religion_id'], 'district_id'=> $row['district_id'], 'role_id'=>'2', 'status'=>'1'));
						$jdrID 		= $getAssignuser[0]['id'];
						
						// Assign Days
						$date_com 	= date('Y-m-d');
						$endDate 	= date('Y-m-d', strtotime($date_com. ' + 10 days'));
						$createdAt	= date('Y-m-d H:i:s');
						
						// Insert Array
						$insert_arr = array('comp_code' 		=> $row['comp_code'],
											'assign_user_id' 	=> $jdrID,
											'assign_date' 		=> $current_date,
											'end_date' 			=> $endDate,
											'status' 			=> '1',
											'createdat' 		=> $createdAt,
											'updatedat' 		=> $createdAt);					
						
						$jdrQuery = $this->master_model->insertRecord('gri_complaint_assign',$insert_arr);
						
					} else if($current_date > $row['end_date'] && $assignRoleID == '2'){
						
						//Old Status Update
						$update_status		=	array('status'	=> '0');						
						$this->master_model->updateRecord('gri_complaint_assign',$update_status,array('comp_code'=>"'".$row['comp_code']."'"));
						
						// Assign Complaint JDR To IGRA User
						$getAssignuser = $this->master_model->getRecords('adminlogin',array('role_id'=>'3', 'status'=>'1'));
						$igraID 		= $getAssignuser[0]['id'];
						
						// Assign Days
						$date_com 	= date('Y-m-d');
						$endDate 	= date('Y-m-d', strtotime($date_com. ' + 17 days'));
						$createdAt	= date('Y-m-d H:i:s');
						
						// Insert Array
						$insert_arr = array('comp_code' 		=> $row['comp_code'],
											'assign_user_id' 	=> $igraID,
											'assign_date' 		=> $current_date,
											'end_date' 			=> $endDate,
											'status' 			=> '1',
											'createdat' 		=> $createdAt,
											'updatedat' 		=> $createdAt);					
						
						$igraQuery = $this->master_model->insertRecord('gri_complaint_assign',$insert_arr);
						
					} else if($current_date > $row['end_date'] && $assignRoleID == '7'){
						
						//Old Status Update
						$update_status		=	array('status'	=> '0');						
						$this->master_model->updateRecord('gri_complaint_assign',$update_status,array('comp_code'=>"'".$row['comp_code']."'"));
						
						// Assign Complaint CSO To IGRA ACS
						$getAssignuser = $this->master_model->getRecords('adminlogin',array('region_id'=> $row['religion_id'], 'district_id'=> $row['district_id'], 'role_id'=>'9', 'status'=>'1'));
						$acsID 	= $getAssignuser[0]['id'];
						
						// Assign Days
						$date_com 	= date('Y-m-d');
						$endDate 	= date('Y-m-d', strtotime($date_com. ' + 7 days'));
						$createdAt	= date('Y-m-d H:i:s');
						
						// Insert Array
						$insert_arr = array('comp_code' 		=> $row['comp_code'],
											'assign_user_id' 	=> $acsID,
											'assign_date' 		=> $current_date,
											'end_date' 			=> $endDate,
											'status' 			=> '1',
											'createdat' 		=> $createdAt,
											'updatedat' 		=> $createdAt);					
						
						$acsQuery = $this->master_model->insertRecord('gri_complaint_assign',$insert_arr);
						
					} else if($current_date > $row['end_date'] && $assignRoleID == '9'){
						
						//Old Status Update
						$update_status		=	array('status'	=> '0');						
						$this->master_model->updateRecord('gri_complaint_assign',$update_status,array('comp_code'=>"'".$row['comp_code']."'"));
						
						// Assign Complaint ACS To IGRA 
						$getAssignuser = $this->master_model->getRecords('adminlogin',array('role_id'=>'3', 'status'=>'1'));
						$igraID = $getAssignuser[0]['id'];
						
						// Assign Days
						$date_com 	= date('Y-m-d');
						$endDate 	= date('Y-m-d', strtotime($date_com. ' + 17 days'));
						$createdAt	= date('Y-m-d H:i:s');
						
						// Insert Array
						$insert_arr = array('comp_code' 		=> $row['comp_code'],
											'assign_user_id' 	=> $igraID,
											'assign_date' 		=> $current_date,
											'end_date' 			=> $endDate,
											'status' 			=> '1',
											'createdat' 		=> $createdAt,
											'updatedat' 		=> $createdAt);					
						
						$igraQuery = $this->master_model->insertRecord('gri_complaint_assign',$insert_arr);
						
					} // Else If End						
					
				} // Complaint Type If End
				
			} // Foreach End
			
		} // If End			
		
	} // Index function End	
	
} // Class End

?>