<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
	function __construct()
	{
		 parent::__construct();
		 $this->session->unset_userdata('p_comp_code');

	}

		public function index()
		{
			 if (!empty($_SERVER['HTTP_CLIENT_IP']))   
			  {
				$ip_address = $_SERVER['HTTP_CLIENT_IP'];
			  }
			//whether ip is from proxy
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
			  {
				$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
			  }
			//whether ip is from remote address
			else
			  {
				$ip_address = $_SERVER['REMOTE_ADDR'];
			  }
			  
			$createdAt		=   date('Y-m-d');			 
			
			$data['today_visit']		= $this->master_model->getRecords('gri_visitors',array('created_at'=>$createdAt));
			$data['all_visit']			= $this->master_model->getRecords('gri_visitors');	
			$data['all_complaint']		= $this->master_model->getRecords('gri_complaint',array('reply_status !='=> '2'));
			$data['today_complaint']	= $this->master_model->getRecords('gri_complaint',array('complaint_date'=>$createdAt, 'reply_status '=> '1'));
			$data['pending_complaint']	= $this->master_model->getRecords('gri_complaint',array('reply_status '=> '1'));	
			$data['resolved_complaint']	= $this->master_model->getRecords('gri_complaint',array('reply_status '=> '0'));	
			
			$getIPexist					= $this->master_model->getRecords('gri_visitors',array('ip_address'=>$ip_address));
			if(count($getIPexist) == 0):
				$insert_arr	=	array(	'ip_address'	=>	$ip_address,
										'created_at'	=>	$createdAt
										);					  
				$visitorCount = $this->master_model->insertRecord('gri_visitors',$insert_arr);
			endif;
			
			$data['middlecontent']='index';			
			$this->load->view('template',$data);
			
			

		}

		public function about()
		{
			$data['middlecontent']='about';			
			$this->load->view('template',$data);		
		}

		public function copyright()
		{
			$data['middlecontent']='copyright';			
			$this->load->view('template',$data);		
		}	 
		
		public function disclaimer()
		{
			$data['middlecontent']='disclaimer';			
			$this->load->view('template',$data);		
		}	
		
		public function hyperlinkpolicy()
		{
			$data['middlecontent']='hyperlinkpolicy';			
			$this->load->view('template',$data);		
		} 
		
		public function privacypolicy()
		{
			$data['middlecontent']='privacypolicy';			
			$this->load->view('template',$data);		
		} 
		
		public function termscondition()
		{
			$data['middlecontent']='termscondition';			
			$this->load->view('template',$data);		
		} 
		
		public function contactus()
		{
			$data['middlecontent']='contactus';			
			$this->load->view('template',$data);		
		} 
		
		public function sitemap()
		{
			$data['middlecontent']='sitemap';			
			$this->load->view('template',$data);		
		} 
		
		public function grievance_machanism()
		{
			$data['middlecontent']='grievance_machanism';			
			$this->load->view('template',$data);		
		}	
		
		
		public function dateDiffInDays($date1, $date2)  
		{ 
			// Calulating the difference in timestamps 
			$diff = strtotime($date2) - strtotime($date1); 
			  
			// 1 day = 24 hours 
			// 24 * 60 * 60 = 86400 seconds 
			return abs(round($diff / 86400)); 
		}
		
		public function notifyigra()
		{
			
			$userId    = $this->session->userdata('user_id');
			
			if($userId == ""){
				
				redirect(base_url().'userlogin');
			}
			
			if(isset($_POST['submit'])) 
			{
				$comp_code = $this->input->post('c_code');
				
				$this->form_validation->set_rules('c_code', 'Complaint Code', 'required|xss_clean');
				
				if($this->form_validation->run())
				{
					
					
				
				$userId    = $this->session->userdata('user_id');
				$query = "SELECT c.comp_code, c.reply_status, ca.assign_user_id, ca.assign_date, ca.end_date, r.role_name, r.role_id FROM gri_complaint c 
						JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code
						JOIN adminlogin a ON ca.assign_user_id = a.id
						JOIN gri_roles r ON a.role_id = r.role_id	
						WHERE c.comp_code = '".$comp_code."' AND c.reply_status = '0' AND ca.status='1' AND c.user_id = '".$userId."'";
				
				$result 	= $this->db->query($query)->result();
				//echo ">>>>".count($result);die();	

				// If already assign to IGRA 
				$query_igra = "SELECT comp_code FROM gri_complaint	
						WHERE r_comp_code = '".$comp_code."'";				
				$result_igra 	= $this->db->query($query_igra)->num_rows();
				
					
				$query_new = "SELECT c.comp_code, c.reply_status, ca.assign_user_id, ca.assign_date, ca.end_date, r.role_name, r.role_id FROM gri_complaint c 
						JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code
						JOIN adminlogin a ON ca.assign_user_id = a.id
						JOIN gri_roles r ON a.role_id = r.role_id	
						WHERE c.comp_code = '".$comp_code."' AND c.reply_status = '1' AND ca.status='1' AND c.user_id = '".$userId."'";
				
				$result_new 	= $this->db->query($query_new)->result();
				
				$currentDate = date('Y-m-d');
				$dateDiff = $this->dateDiffInDays($end_date, $currentDate);
				
					if(count($result_new) > 0){
						
						$query_level = "SELECT comp_code FROM gri_complaint	
									WHERE r_comp_code = '".$comp_code."'";				
						$result_level	= $this->db->query($query_level)->num_rows();
						
						$c_status   = $result_new[0]->reply_status;
						$c_ref   	= $result_new[0]->r_comp_code;
						$r_id 		= $result_new[0]->role_id;
						$e_date 	= $result_new[0]->end_date;
						$f_Date 	= date('Y-m-d', strtotime($e_date));
						$c_Date		= date('Y-m-d');
						$st = 0;
						
						if($c_Date > $f_Date) {								
							$st = 1;
						}
						
						
						if($st == 1){
							
							$_SESSION['p_comp_code'] = $comp_code;
							$this->session->set_userdata($comp_code);
							redirect(base_url().'complaint/refComplaint');
							
						} else if($result_level > 0){
							
							$this->session->set_flashdata('error_message','The complaint is reported to next level');
							redirect(base_url().'home/notifyigra');
							
						}else {
							
							$this->session->set_flashdata('error_message','The complaint is in progress ');
							redirect(base_url().'home/notifyigra');
							
						}
						
					}
				
					if(count($result) > 0){
						
						$comp_status = $result[0]->reply_status;
						$roleID 	= $result[0]->role_id;
						$end_date 	= $result[0]->end_date;
						$formatDate = date('Y-m-d', strtotime($end_date));
						$currentDate= date('Y-m-d');
						
							
					
							if($currentDate > $formatDate) {								
								$st = 1;
							}
							/*if($currentDate > $formatDate ){
								$st = 1;
							}*/							
							
							if($comp_status == 0 && $result_igra == 0){	
								//$code = base64_encode($comp_code);
								$_SESSION['p_comp_code'] = $comp_code;
								$this->session->set_userdata($comp_code);
								redirect(base_url().'complaint/refComplaint'); 
							} else if($result_igra > 0) {
								
								$this->session->set_flashdata('error_message','The complaint is already assign to next level.');
								redirect(base_url().'home/notifyigra');
								
							} else {
								
								$this->session->set_flashdata('error_message','The complaint is in progress ');
								redirect(base_url().'home/notifyigra');
							}
						
					} else {
						
						$this->session->set_flashdata('error_message','Sorry, Enter complaint code does not exist.');
						redirect(base_url().'home/notifyigra');
					}
						
				
				}else{
					
					$this->session->set_flashdata('error_message','Please enter complaint code. ');
					redirect(base_url().'home/notifyigra');
					
				}
				//$rowCount 	= $this->db->query($query)->num_rows();	
			}
			$data['middlecontent']='notifyigra';			
			$this->load->view('template',$data);		
		} 
		
		/*public function notifyigra()
		{
			
			$userId    = $this->session->userdata('user_id');
			
			if($userId == ""){
				
				redirect(base_url().'userlogin');
			}
			
			if(isset($_POST['submit'])) 
			{
				$comp_code = $this->input->post('c_code');
				
				$this->form_validation->set_rules('c_code', 'Complaint Code', 'required|xss_clean');
				
				if($this->form_validation->run())
				{
					
					
				
				$userId    = $this->session->userdata('user_id');
				$query = "SELECT c.comp_code, c.reply_status, ca.assign_user_id, ca.assign_date, ca.end_date, r.role_name, r.role_id FROM gri_complaint c 
						JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code
						JOIN adminlogin a ON ca.assign_user_id = a.id
						JOIN gri_roles r ON a.role_id = r.role_id	
						WHERE c.comp_code = '".$comp_code."' AND c.reply_status = '0' AND ca.status='1' AND c.user_id = '".$userId."'";
				
				$result 	= $this->db->query($query)->result();
				//echo ">>>>".count($result);die();	

				// If already assign to IGRA 
				$query_igra = "SELECT comp_code FROM gri_complaint	
						WHERE r_comp_code = '".$comp_code."'";				
				$result_igra 	= $this->db->query($query_igra)->num_rows();
				
					
				$query_new = "SELECT c.comp_code, c.reply_status, ca.assign_user_id, ca.assign_date, ca.end_date, r.role_name, r.role_id FROM gri_complaint c 
						JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code
						JOIN adminlogin a ON ca.assign_user_id = a.id
						JOIN gri_roles r ON a.role_id = r.role_id	
						WHERE c.comp_code = '".$comp_code."' AND c.reply_status = '1' AND ca.status='1' AND c.user_id = '".$userId."'";
				
				$result_new 	= $this->db->query($query_new)->result();
				
				$currentDate = date('Y-m-d');
				$dateDiff = $this->dateDiffInDays($end_date, $currentDate);
				
					if(count($result_new) > 0){
						
						$c_status   = $result_new[0]->reply_status;
						$r_id 		= $result_new[0]->role_id;
						$e_date 	= $result_new[0]->end_date;
						$f_Date 	= date('Y-m-d', strtotime($e_date));
						$c_Date		= date('Y-m-d');
						$st = 0;
						
						if($c_Date > $f_Date) {								
							$st = 1;
						}
						
						
						if($st == 1){
							
							$_SESSION['p_comp_code'] = $comp_code;
							$this->session->set_userdata($comp_code);
							redirect(base_url().'complaint/refComplaint');
							
						} else {
							
							$this->session->set_flashdata('error_message','The complaint is in progress ');
							redirect(base_url().'home/notifyigra');
							
						}
						
					}
				
					if(count($result) > 0){
						
						$comp_status = $result[0]->reply_status;
						$roleID 	= $result[0]->role_id;
						$end_date 	= $result[0]->end_date;
						$formatDate = date('Y-m-d', strtotime($end_date));
						$currentDate= date('Y-m-d');
						
							
					
							if($currentDate > $formatDate) {								
								$st = 1;
							}
							
							if($comp_status == 0 && $result_igra == 0){	
								//$code = base64_encode($comp_code);
								$_SESSION['p_comp_code'] = $comp_code;
								$this->session->set_userdata($comp_code);
								redirect(base_url().'complaint/refComplaint'); 
							} else if($result_igra > 0) {
								
								$this->session->set_flashdata('error_message','The complaint is already assign to next level.');
								redirect(base_url().'home/notifyigra');
								
							} else {
								
								$this->session->set_flashdata('error_message','The complaint is in progress ');
								redirect(base_url().'home/notifyigra');
							}
						
					} else {
						
						$this->session->set_flashdata('error_message','Sorry, Enter complaint code does not exist.');
						redirect(base_url().'home/notifyigra');
					}
						
				
				}else{
					
					$this->session->set_flashdata('error_message','Please enter complaint code. ');
					redirect(base_url().'home/notifyigra');
					
				}
				//$rowCount 	= $this->db->query($query)->num_rows();	
			}
			$data['middlecontent']='notifyigra';			
			$this->load->view('template',$data);		
		} */
		
		
		
		public function test()
		{
			$data['middlecontent']='sidebar';			
			$this->load->view('sidebar',$data);
			
			

		}
				public function feedback()
		{
			if ($this->input->post())
			{

		$this->form_validation->set_rules('comp_code', 'Grievance ID', 'trim|required|xss_clean|min_length[5]|max_length[20]|callback_check_comp_code');
		$this->form_validation->set_rules('rating', 'Rating', 'trim|required|xss_clean');
		$this->form_validation->set_rules('code','Captcha Code','trim|xss_clean|callback_check_captcha|required');
				
				if($this->form_validation->run())
				{

				$comp_code 		= 	$this->input->post('comp_code');
				$rating 		= 	$this->input->post('rating');
				$message 		= 	$this->input->post('message');
				$createdAt			=   date('Y-m-d H:i:s');
				$insert_arr=array(
								'comp_code'=>$comp_code,
								'rating'=>$rating,
								'message'=>$message,
								 'created_at'=>$createdAt,
								  );
				$info = $this->master_model->insertRecord('gri_feedback',$insert_arr);
				// echo $this->db->last_query();
				// die;
				if ($info) {
				$this->session->set_flashdata('success_message','Feedback submitted successfully.');
				redirect(base_url().'home/feedback');
				}else{
				$this->session->set_flashdata('error_message','Something went wrong. Please try again');
				redirect(base_url().'home/feedback');
				}
				
				
				}
			}
			$vals = array(
			'img_path' => './images/captcha/',
			'img_url' => base_url().'images/captcha/',
			);
			$cap = create_captcha($vals);
			$data['image'] = $cap['image'];
			$_SESSION["mycaptcha"]=$cap['word'];

			$data['middlecontent']='feedback';			
			$this->load->view('template',$data);		
		}

		public function check_captcha($code) 

		{
			      // check if captcha is set -
					if($code == '' || $_SESSION["mycaptcha"] != $code  ||  $this->session->userdata("used")==1  )
					{
						$this->form_validation->set_message('check_captcha', 'Invalid %s.'); 
						$this->session->set_userdata("mycaptcha", rand(1,100000));
						$this->session->set_userdata("used",0);
						return false;
					}
					if($_SESSION["mycaptcha"] == $code && $this->session->userdata("used")==0)
					{
						$this->session->unset_userdata("mycaptcha");
						//$this->session->set_userdata("mycaptcha", rand(1,100000));
						$this->session->set_userdata("used", 1);
						return true;
					}
			
		}

		public function refresh(){
        // Captcha configuration
        $config = array(
            'img_path' => './images/captcha/',
			'img_url' => base_url().'images/captcha/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}

	public function check_comp_code($comp_code) 
	{

		$last_patternid=$this->master_model->getRecords('gri_complaint',array('comp_code'=>$comp_code),'c_id');

		if(count($last_patternid))
		{
			return true;
		}else{
			$this->form_validation->set_message('check_comp_code', 'Invalid %s.'); 
			return false;
		}
	}

	
	
	

	 
}

	

/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
