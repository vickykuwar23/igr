<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Create extends CI_Controller {

	public function password()
	{

		// if($this->session->userdata('emailverify')=='' || !$this->session->userdata('emailverify') || time() - $this->session->userdata('login_time') > 3600)
		// {
		// 	$verify=array('emailverify'=>'','login_time'=>'');
		// 	$this->session->unset_userdata($verify);
		// 	$this->session->set_flashdata('expire_session','Your session has been expired, please register once again');
		// 	redirect(base_url());
		//    }

		$user_id=$this->uri->segment(3);
		
		$data['error_msg']='';
		
		if($user_id=='')
		{
			$data['error_msg']='Please check your mail to activate account.';
		}
		if(isset($_POST['btn_set']) && $user_id!='')
		{
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_createpass');
			//$this->form_validation->set_rules('phonenumber','phone number','trim|required|exact_length[10]|xss_clean|numeric');
			$this->form_validation->set_rules('pass','Password','trim|required|min_length[8]|xss_clean');
			$this->form_validation->set_rules('con_password','Confirm Password','trim|required|xss_clean');
			if($this->form_validation->run())
			{
				$pass=$this->input->post('pass');
				//$phonenumber=htmlentities($this->input->post('phonenumber'));
				$con_password=$this->input->post('con_password');
				$update_arr=array('user_pass'=>$this->sha256($pass),
												 'verify_status'=>'1',
												 'block_status'=>'0',
												 'is_new_user'=>'1',
												 'password1'=>$this->sha256($pass)
												);
				//$getmobile=$this->master_model->getRecords('userregistration',array('user_mobile'=>$phonenumber));
				if($getmobile=$this->master_model->getRecords('userregistration',array('user_id'=>$user_id)))
                {				
					if($insert_id=$this->master_model->updateRecord('userregistration',$update_arr,array('user_id'=>$user_id)))
					{
						$this->session->set_flashdata('success_message','You have successfully set your password please access your account.');
						redirect(base_url('userlogin'));
					}
				}
				else
				{
					$this->session->set_flashdata('galat_jawab','You have entered wrong mobile number.');
					redirect(base_url().'create/password/'.$user_id);
				}
			}		
		}
			
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		  //$this->session->unset_userdata("mycaptcha");
	    $_SESSION["createpass"]=$cap['word'];
	    $data['middlecontent']='create_password';
		$this->load->view('template',$data);
	}
	
	public function forgetpassword()
	{
	 
		/*if($this->session->userdata('forgetemailverify')=='' || !$this->session->userdata('forgetemailverify') || time() - $this->session->userdata('forget_login_time') > 3600)
		{
			$verify=array('forgetemailverify'=>'','forget_login_time'=>'');
			$this->session->unset_userdata($verify);
			$this->session->set_flashdata('expire_session','Your session has been expired, please register once again');
			redirect(base_url());
	    }*/
		
		$data['error_msg']='';
		$user_id=$this->uri->segment(3);
		if($user_id=='')
		{
			$data['error_msg']='Please check your mail to reset your paswword.';
		}
		if(isset($_POST['btn_set']) && $user_id!='')
		{
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_forgetpass');
			$this->form_validation->set_rules('pass','Password','trim|required|min_length[8]|xss_clean');
			// $this->form_validation->set_rules('phonenumber','phone number','trim|required|exact_length[10]|xss_clean|numeric');
			$this->form_validation->set_rules('con_password','Confirm Password','trim|required|xss_clean');
			// $this->form_validation->set_rules('con_password','Confirm Password','trim|required|xss_clean|regex_match[((?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*\W).{8,100})]');

			if($this->form_validation->run())
			{ 
				
				$phonenumber=htmlentities($this->input->post('phonenumber'));
				$pass=$this->input->post('pass');
				$con_password=$this->input->post('con_password');
				$password=$this->master_model->getRecords('userregistration',array('user_id'=>$user_id));			
			
				if($getmobile=$this->master_model->getRecords('userregistration',array('user_id'=>$user_id)))
				{
						  $update_arr=array('user_pass'=>$this->sha256($pass),
											 'verify_status'=>'1',
											 'block_status'=>'0',
											  'is_new_user'=>'1',
											 'password2'=>$this->sha256($pass)
										);
						  $insert_id=$this->master_model->updateRecord('userregistration',$update_arr,array('user_id'=>$user_id));
						  if($insert_id!='')
						  {
							
							  $this->session->set_flashdata('success_message','Your password has been reset successfully');
							 redirect(base_url('userlogin'));	  
						  }
						
									
				}
				else
				{
					$this->session->set_flashdata('error_message','You have entered wrong mobile number.');
					redirect(base_url().'create/forgetpassword/'.$user_id);
				}
			}		
		}
		
			
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		  //$this->session->unset_userdata("mycaptcha");
	    $_SESSION["forgetpass"]=$cap['word'];
		
	    $data['middlecontent']='create_password';
		$this->load->view('template',$data);
	}

	public function refresh(){
        // Captcha configuration
        $this->load->helper('captcha');
        $config = array(
            'img_path' => './images/captcha/',
			'img_url' => base_url().'images/captcha/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('forgetpass');
        $this->session->set_userdata('forgetpass',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}	
	 public function check_captcha_forgetpass($code) 

	{
		      // check if captcha is set -
				if($code == '' || $_SESSION["forgetpass"] != $code )
				{
					$this->form_validation->set_message('check_captcha_forgetpass', 'Invalid %s.'); 
					$this->session->set_userdata("forgetpass", rand(1,100000));
					
					return false;
				}
				if($_SESSION["forgetpass"] == $code)
				{
					$this->session->unset_userdata("forgetpass");
					//$this->session->set_userdata("mycaptcha", rand(1,100000));
					return true;
				}
		
	}
	
	 public function check_captcha_createpass($code) 

	{
		      // check if captcha is set -
				if($code == '' || $_SESSION["createpass"] != $code )
				{
					$this->form_validation->set_message('check_captcha_createpass', 'Invalid %s.'); 
					$this->session->set_userdata("createpass", rand(1,100000));
					
					return false;
				}
				if($_SESSION["createpass"] == $code)
				{
					$this->session->unset_userdata("createpass");
					//$this->session->set_userdata("mycaptcha", rand(1,100000));
					return true;
				}
		
	}

	public function forgetpassword_superadmin()
	{
	
		$data['error_msg']='';
		$user_id=$this->uri->segment(3);
		$token=$this->uri->segment(4);
		

		

		// if ($token) {
		// 	$gettoken=$this->master_model->getRecords('adminlogin',array('token'=>$token));
		// 	if (count($gettoken)==0) {
		
		// 		$this->session->set_flashdata('error_message','Invalid Token.');
		// 	    redirect(base_url().'create/forgetpassword_superadmin/'.$user_id.'/45345');
		// 	}
		// }
		
		
		if(isset($_POST['btn_set']) && $user_id!='')
		{
			if ($token=='') {
				$this->session->set_flashdata('error_message','Invalid Token.');
			    redirect(base_url().'create/forgetpassword_superadmin/'.$user_id);
			}
			$gettoken=$this->master_model->getRecords('adminlogin',array('id'=>$user_id,'token'=>$token));
			if (count($gettoken)==0) {
				$this->session->set_flashdata('error_message','Invalid Token.');
			    redirect(base_url().'create/forgetpassword_superadmin/'.$user_id);
			}
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_forgetpass');
			$this->form_validation->set_rules('pass','Password','trim|required|min_length[8]|xss_clean');
			
			$this->form_validation->set_rules('con_password','Confirm Password','trim|required|xss_clean');

			if($this->form_validation->run())
			{ 
				
				$phonenumber=htmlentities($this->input->post('phonenumber'));
				$pass=$this->input->post('pass');
				$con_password=$this->input->post('con_password');
							
			
				if($getmobile=$this->master_model->getRecords('adminlogin',array('id'=>$user_id,'token'=>$token)))
				{
						  $update_arr=array(
										 'adminpass'=>sha1($pass),
										 'token'=>null
										);
						  $insert_id=$this->master_model->updateRecord('adminlogin',$update_arr,array('id'=>$user_id));
						  if($insert_id!='')
						  {
							
							  $this->session->set_flashdata('success_message','Your password has been reset successfully');
							 redirect(base_url('superadminlogin/suplogin'));	  
						  }
						
									
				}
				else
				{
					$this->session->set_flashdata('error_message','Invalid Token.');
					redirect(base_url().'create/forgetpassword_superadmin/'.$user_id);
				}
			}		
		}
		
			
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		  //$this->session->unset_userdata("mycaptcha");
	    $_SESSION["forgetpass"]=$cap['word'];
		
	    $data['middlecontent']='create_pass_superadmin';
		$this->load->view('template',$data);
	}

	 public function sha256($pass)
	{
		return hash('sha256',$pass);
	}
	
}

