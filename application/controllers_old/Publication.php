<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Publication extends CI_Controller {
	function __construct()
	{
		 parent::__construct();
		 

	}

		public function index()
		{
			
			 if (!empty($_SERVER['HTTP_CLIENT_IP']))   
			  {
				$ip_address = $_SERVER['HTTP_CLIENT_IP'];
			  }
			//whether ip is from proxy
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
			  {
				$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
			  }
			//whether ip is from remote address
			else
			  {
				$ip_address = $_SERVER['REMOTE_ADDR'];
			  }
			  
			$createdAt		=   date('Y-m-d'); 
				
			
			$getIPexist					= $this->master_model->getRecords('gri_visitors',array('ip_address'=>$ip_address));
			if(count($getIPexist) == 0):
				$insert_arr	=	array(	'ip_address'	=>	$ip_address,
										'created_at'	=>	$createdAt
										);					  
				$visitorCount = $this->master_model->insertRecord('gri_visitors',$insert_arr);
			endif;
			
			$data['middlecontent']='publications/index';
			$data['title']='Publication';	
			$data['subtitle']='Publication';	
			$this->load->view('template',$data);
			
			

		}	
		
		
		public function acts()
		{
			$data['subtitle']='Acts';	
			$data['middlecontent']='publications/acts';			
			$this->load->view('template',$data);		

		}
		
		public function schedules()
		{
			$data['subtitle']='Schedules';
			$data['middlecontent']='publications/schedules';			
			$this->load->view('template',$data);		

		}
		
		public function rules()
		{
			$data['subtitle']='Rules';
			$data['middlecontent']='publications/rules';			
			$this->load->view('template',$data);		

		}
		
		public function notifications()
		{
			$data['subtitle']='Notifications';
			$data['middlecontent']='publications/notifications';			
			$this->load->view('template',$data);		

		}
		
		public function grs()
		{
			$data['subtitle']='GRS';
			$data['middlecontent']='publications/grs';			
			$this->load->view('template',$data);		

		}
		
		public function circulars()
		{
			$data['subtitle']='Circulars';
			$data['middlecontent']='publications/circulars';			
			$this->load->view('template',$data);		

		}
		
		public function asr()
		{
			$data['subtitle']='ASR';
			$data['middlecontent']='publications/asr';			
			$this->load->view('template',$data);		

		}
		
		public function citizen()
		{
			$data['subtitle']='Citizen';
			$data['middlecontent']='publications/citizen';			
			$this->load->view('template',$data);		

		}
		
		public function sarathi()
		{
			$data['subtitle']='Sarathi';
			$data['middlecontent']='publications/sarathi';			
			$this->load->view('template',$data);		

		}
		
		// public function dept_exam_results()
		// {
		// 	$data['subtitle']='Exam';
		// 	$data['middlecontent']='publications/dept_exam_results';			
		// 	$this->load->view('template',$data);		

		// }

		public function fee_structure()
		{
			$data['subtitle']='Fee Structure';
			$data['middlecontent']='publications/fee_structure';			
			$this->load->view('template',$data);		

		}

		public function reports()
		{
			$data['subtitle']='Reports';
			$data['middlecontent']='publications/reports';			
			$this->load->view('template',$data);		

		}

		public function dept_exam_results()
		{
			$data['subtitle']='Department Exam Results';
			$data['middlecontent']='publications/dept_exam_results';			
			$this->load->view('template',$data);		

		}
	
	

	 
}

	

/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
