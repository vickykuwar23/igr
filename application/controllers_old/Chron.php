<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Chron extends CI_Controller 
{
   function __construct()
	{
		parent::__construct();
	}
	public function notrerply()
	{
			
		$reply=array();
		$category=$this->master_model->getRecords('departments');
		if(is_array($category) && count($category) > 0)
		{
			foreach($category as $row)
			{
				$this->db->where('category',$row['id']);
				$this->db->where('reply_status','1');//reply_ststus : 1-open 0-closed grievance for not reply:status : 1-replied status= 0-not replied
				$this->db->where("`registerdate` < DATE_SUB(CURDATE(), INTERVAL 7 DAY)");
				$this->db->join('userregistration', 'userregistration.user_id = usercomplainbox.user_id');
				$reply[$row['id']]=$this->master_model->getRecords('usercomplainbox');	
			}
		}
		/*echo '<pre>';
		print_r($reply);exit;*/
		
		if(is_array($reply) && count($reply) > 0)
		{
			foreach($reply as $k=>$v)
			{
			$str='';
			$adminmailid=$this->master_model->getRecords('adminlogin',array('type'=>$k),'email,emailcc');	
			if(is_array($v) && count($v) > 0)
			{	
				$str.='<table style="border-top:1px solid #ccc; border-left:1px solid #ccc;" cellpadding="0" cellspacing="0" align="center" width="100%">
					<thead>	<tr>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:5%;">Sr.No.</th>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:20%;">Reference Number</th>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:30%;">Name</th>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:20%;">Mobile</th>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:10%;">Email</th>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:15%;">Status</th>
						</tr></thead>';
				$i=1;
				foreach($v as $kval=>$vval)
				{
					$str.='<tr>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">'.$i.'</td>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">'.$vval['pattern'].'</td>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">'.$vval['user_name'].'</td>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">'.$vval['user_mobile'].'</td>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">'.$vval['user_email'].'</td>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">Pending</td>';
					$str.='</tr>';
					$i++;
				}
				
			$str.='</table>';
				
				$info_arr=array(
												'to'=>$adminmailid[0]['email'],//
												'from'=>$this->config->item('MAIL_FROM'),//$admin_email[0]['email']
												'cc'=>$this->config->item('MAIL_FROM'),//vijay.pal@ifciltd.co.in
												'subject'=>'Reminder - Grievance Redressal Portal',
												'view'=>'chron_mail'
												);
				$other_info=array('msg_content'=>$str);
				//echo '**************************************************************************************<br>';
				//echo '<pre>';
				//print_r($info_arr);
				//print_r($other_info);
				//exit;
			$this->email_sending->sendmail($info_arr,$other_info);
			//echo $this->email->print_debugger();
			 }
			}
		
		}
		echo $str;
		exit;		
	}
	
	public function everymonth()
	{
			
		$reply=array();
		$category=$this->master_model->getRecords('departments');
		if(is_array($category) && count($category) > 0)
		{
			foreach($category as $row)
			{
				$this->db->where('category',$row['id']);
				$this->db->where('reply_status','1');//reply_ststus : 1-open 0-closed grievance for not reply:status : 1-replied status= 0-not replied
			//	$this->db->where("`registerdate` < DATE_SUB(CURDATE(), INTERVAL 7 DAY)");
				$this->db->join('userregistration', 'userregistration.user_id = usercomplainbox.user_id');
				$reply[$row['id']]=$this->master_model->getRecords('usercomplainbox');	
			}
		}
		/*echo '<pre>';
		print_r($reply);exit;*/
		
		if(is_array($reply) && count($reply) > 0)
		{
			foreach($reply as $k=>$v)
			{
			$str='';
			$adminmailid=$this->master_model->getRecords('adminlogin',array('type'=>$k),'email,emailcc');	
			if(is_array($v) && count($v) > 0)
			{	
				$str.='<table style="border-top:1px solid #ccc; border-left:1px solid #ccc;" cellpadding="0" cellspacing="0" align="center" width="100%">
					<thead>	<tr>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:5%;">Sr.No.</th>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:20%;">Reference Number</th>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:30%;">Name</th>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:20%;">Mobile</th>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:10%;">Email</th>
						<th style="background:#5B9BD5;color:#fff; padding:5px 0;width:15%;">Status</th>
						</tr></thead>';
				$i=1;
				foreach($v as $kval=>$vval)
				{
					$str.='<tr>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">'.$i.'</td>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">'.$vval['pattern'].'</td>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">'.$vval['user_name'].'</td>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">'.$vval['user_mobile'].'</td>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">'.$vval['user_email'].'</td>';
					$str.='<td style="text-align:center; background:#f3f3f3;padding:3px 0;border-bottom:1px solid #ccc; border-right:1px solid #ccc;">Pending</td>';
					$str.='</tr>';
					$i++;
				}
				
			$str.='</table>';
				
				$info_arr=array(
												'to'=>$adminmailid[0]['email'],//$adminmailid[0]['email']
												'from'=>$this->config->item('MAIL_FROM'),//$admin_email[0]['email']
												'cc'=>$this->config->item('MAIL_FROM'),//vijay.pal@ifciltd.co.in
												'subject'=>'Reminder - Grievance Redressal Portal',
												'view'=>'chron_mail'
												);
				$other_info=array('msg_content'=>$str);
				//echo '**************************************************************************************<br>';
				//echo '<pre>';
				//print_r($info_arr);
				//print_r($other_info);
				//exit;
			$this->email_sending->sendmail($info_arr,$other_info);
			//echo $this->email->print_debugger();
			 }
			}
		
		}
		echo $str;
		exit;		
	}
	
	
	
	public function notclosed()
	{
		$this->db->where('status','0');
		$this->db->where("`registerdate` < DATE_SUB(CURDATE(), INTERVAL 7 DAY)");
		$last_seven_close=$this->master_model->getRecords('usercomplainbox');
	
	}
}