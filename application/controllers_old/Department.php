<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		$this->chk_session->chk_superadmin_session();
    }
	
	public function index()
	{
		$data['department_list']=$this->master_model->getRecords('gri_department','','',array('dept_id'=>'DESC'));
		$data['middlecontent']='department/index';
	    $this->load->view('superadmintemplate',$data);
	}

	public function add()
	{	
		$this->form_validation->set_rules('department_name', 'Department Name', 'trim|required|xss_clean');
		if($this->form_validation->run())
		{
			
			$c_dept_name 		= 	htmlentities($this->input->post('department_name'));
			$createdAt			=   date('Y-m-d H:i:s');
			$insert_arr=array('department_name'=>$c_dept_name,
							  'status'=>1,
							  'createdAt'=>$createdAt,
							  'updatedAt'=>'',
							  );
			$dept_info = $this->master_model->insertRecord('gri_department',$insert_arr);
			$this->session->set_flashdata('success_message','Department has been created');
			redirect(base_url().'department/add');
		}
		
		$data['middlecontent']='department/add';	
	 	$this->load->view('superadmintemplate',$data);
	}
	
/****** Edit Functionality ******/ 
	
	public function edit()
	{
		$data['error_msg']='';
		$updateid=$this->uri->segment('3');
		
			$this->form_validation->set_rules('department_name', 'Department Name', 'trim|required|xss_clean');
			if($this->form_validation->run())
			{
				$c_dept_name 		= 	htmlentities($this->input->post('department_name'));
				$updatedAt			=   date('Y-m-d H:i:s');
				
				$update_arr=array('department_name'=>$c_dept_name,
								  'updatedAt'=>$updatedAt,
								  );
				
				if($this->master_model->updateRecord('gri_department',$update_arr,array('dept_id'=>"'".$updateid."'")))
				{
					$this->session->set_flashdata('success_message','Department has been updated successfully.');
					redirect(base_url().'department/edit/'.$updateid);
				}
				else
				{
					$data['error_msg']='Error while updating record.';
				}
			}
		

		$data['get_dapt_data']=$this->master_model->getRecords('gri_department',array('dept_id'=>$updateid));
		
		$data['middlecontent']='department/edit';
	    $this->load->view('superadmintemplate',$data);
	}
	
	public function delete()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'complainttype');
		}
		if($this->master_model->deleteRecord('gri_complaint_type','complaint_type_id',$id))
		{
		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'complainttype');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'complainttype');
		}
	
	}
	
} // Class End

?>