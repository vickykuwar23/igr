<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Region extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		$this->chk_session->chk_superadmin_session();
    }
	
	public function index()
	{
		$data['regions']=$this->master_model->getRecords('gri_region_division','','',array('religion_id'=>'DESC'));
		$data['middlecontent']='region/index';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}

	public function add()
	{	
		//$this->form_validation->set_rules('person_name', 'Person Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('region_division_name', 'Division Name', 'required|trim|required|xss_clean|max_length[200]');
		//$this->form_validation->set_rules('phone_no', 'Phone No', 'trim|required|min_length[10]|max_length[15]|numeric|xss_clean');
		//$this->form_validation->set_rules('emailid', 'Email ID', 'trim|required|valid_email|xss_clean');	
		
		if($this->form_validation->run())
		{
			$person_name 			= 	$this->input->post('region_division_name');
			$region_division_name 	= 	$this->input->post('region_division_name');
			$phone_no		= 	htmlentities($this->input->post('phone_no'));
			$emailid		= 	htmlentities($this->input->post('emailid'));
			$createdAt			=   date('Y-m-d H:i:s');			
			$insert_arr=array('person_name'=>'',
							  'region_division_name'=>$region_division_name,
							  'phone_no'=>'',
							  'emailid'=>'',
							  'status'=>'1',
							  'createdat'=>	$createdAt,
							  'updatedat'=>	$createdAt 	
							  );
			if($this->master_model->insertRecord('gri_region_division',$insert_arr)){
			 //add logs
				$json_encode_data = json_encode($insert_arr);
				$log_user_id      = $this->session->userdata('supadminid');
				$ipAddr			  = $this->get_client_ip();
				$logDetails = array(
									'module_name' 	=> "Region",
									'action_name' 	=> "Add",
									'json_response'	=> $json_encode_data,
									'login_id'		=> $log_user_id,
									'role_type'		=> $this->session->userdata('role_name'),
									'createdAt'		=> date('Y-m-d H:i:s'),
									'ip_addr'		=> $ipAddr
									);
				$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
				//end add logs	
			$this->session->set_flashdata('success_message','The region has been created');
			redirect(base_url().'region');
		}else{
			$this->session->set_flashdata('error_message','Error while processing your request');
			redirect(base_url().'region/add');
		}
		}
		
		$data['middlecontent']='region/add';
		$data['master_mod'] = 'Master';		
	 	$this->load->view('admin/admin_combo',$data);
	}
	
/****** Edit Functionality ******/ 
	
	public function edit()
	{
		$data['error_msg']='';
		$updateid=$this->uri->segment('3');
		
			//$this->form_validation->set_rules('person_name', 'Person Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('region_division_name', 'Division Name', 'trim|required|xss_clean|max_length[200]');
			//$this->form_validation->set_rules('phone_no', 'Phone No', 'trim|required|min_length[10]|max_length[15]|numeric|xss_clean');
			//$this->form_validation->set_rules('emailid', 'Email ID', 'trim|required|valid_email|xss_clean');		
			if($this->form_validation->run())
			{
				$person_name 		= 	htmlentities($this->input->post('person_name'));
				$region_division_name 	= 	$this->input->post('region_division_name');
				$phone_no		= 	htmlentities($this->input->post('phone_no'));
				$emailid		= 	htmlentities($this->input->post('emailid'));
				
				$update_arr=array('person_name'=>$person_name,
								  'region_division_name'=>$region_division_name,
								  'phone_no'=>$phone_no,
								  'emailid'=>$emailid,
							  );
				
				if($this->master_model->updateRecord('gri_region_division',$update_arr,array('religion_id'=>"'".$updateid."'")))
				{
					//add logs
					$for_id=array('record_id'=>$updateid);
					$array_first = array_merge($update_arr,$for_id);
					$json_encode_data = json_encode($array_first);
					$log_user_id      = $this->session->userdata('supadminid');
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Region",
										'action_name' 	=> "Edit",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> $this->session->userdata('role_name'),
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
					//end add logs
					$this->session->set_flashdata('success_message',' Region has been updated successfully.');
					redirect(base_url().'region/edit/'.$updateid);
				}
				else
				{
					$data['error_msg']='Error while updating record.';
				}
			}
		

		$data['info']=$this->master_model->getRecords('gri_region_division',array('religion_id'=>$updateid));
		if(count($data['info']) <= 0)
		{
			redirect(base_url().'region');
		}
			
		$data['middlecontent']='region/edit';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	public function delete()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'region');
		}
		if($this->master_model->deleteRecord('gri_region_division','religion_id',$id))
		{
			  // logs
			$for_id=array('record_id'=>$id);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Region",
								'action_name' 	=> "Delete",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			// end logs	
		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'region');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'region');
		}
	
	}

	public function change_status()
	{
		 $id = $this->uri->segment(3);
		 $status = $this->uri->segment(4);
	
		if($status=='1'){$newstatus = '0'; }
		else if($status=='0'){$newstatus = '1';}
		if($this->master_model->updateRecord("gri_region_division",array('status'=>$newstatus)
		,array('religion_id'=>$id)))
		{
			// logs
			$for_id=array('record_id'=>$id,'status'=>$status);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Region",
								'action_name' 	=> "change status",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			// logs end
			$this->session->set_flashdata('success_message','Status changed successfully.');						           
			redirect(base_url().'region');
		}
		else
		{
			$this->session->set_flashdata('error_message','Error while updating user status.');										            
			redirect(base_url().'region');
		}
	}

	function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }
	
} // Class End

?>