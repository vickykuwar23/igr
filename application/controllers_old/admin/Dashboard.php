        <?php
        defined('BASEPATH') OR exit('No direct script access allowed');

        class Dashboard extends CI_Controller 
        {

            function __construct()
            {
                parent::__construct();
                $this->load->model('chk_session');
                if($this->session->userdata('type')=='')
                {
                    redirect(base_url().'superadminlogin/suplogin');
                }
                
            }

            public function index($value='')
            {

                $adminid = $this->session->userdata('supadminid');
                $sro_id = $this->session->userdata('sro_id');
                $district_id = $this->session->userdata('district_id');
                

                $where = array();
                $type = $this->session->userdata('type');

                // sro user
                if ($type==1) {
                    $where=array('gri_complaint.comp_code !=' => '','complaint_assign_to'=>$sro_id);
                }

                // mrg off user
                if ($type==15) {
                    $where=array('gri_complaint.comp_code !=' => '','complaint_assign_to'=>$sro_id);
                }
                // jdr user
                if ($type==2) {
                    $where=array('gri_complaint.comp_code !=' => '','district_id'=>$district_id,'complaint_sub_type_id !='=>2,'gri_complaint.is_level_2'=>'1');
                }
                
                // igra user
                if ($type==3  ) {
                    $where=array('gri_complaint.comp_code !=' => '','gri_complaint.is_level_3'=>'1');
                    // $condition .= " AND  = '1' ";
                }


                 // igra dgit etc user
                if ($type==999 || $type==11 || $type==12 || $type==13 || $type==14 || $type==16 ) {
                    $where=array('gri_complaint.comp_code !=' => '');
                    // $condition .= " AND  = '1' ";
                }

                // help desk user
                if ($type==4) {
                    $where=array('gri_complaint.comp_code !=' => '','gri_complaint.complaint_type_id'=>1);
                }

                // CSO
                if ($type==7) { 
                    $where=array('gri_complaint.comp_code !=' => '','complaint_assign_to'=>$sro_id);
                }

                // ACS user
                if ($type==9) {
                    $where=array('gri_complaint.comp_code !=' => '','district_id'=>$district_id,'complaint_sub_type_id'=>2,'gri_complaint.is_level_2'=>'1');
                }

                if ($type==5) {
                    $where=array('gri_complaint.comp_code !=' => '');
                }
               
                
                // all complaints

             
                if($type==1 || $type==7 || $type==15 || $type==2 || $type==4 || $type==9){
                    $where["gri_complaint_assign.status"] = 1;
                }
                   
                $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                $data['total_comp_count']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as total_comp_count' );
                //end all complaints

                // echo $this->db->last_query();
                

                //closed complaints
                
                if($type==1 || $type==2 || $type==4 || $type==3 || $type==7 || $type==9 || $type==15){
                  
                    $where["gri_complaint.compaint_closed_by"]=$adminid;
                    $where["gri_complaint_assign.status"]='1';
                } 
                if ($type==999 || $type==11 || $type==12 || $type==13 || $type==14 || $type==16 || $type==5) {

                    unset($where["gri_complaint.compaint_closed_by"]);
                    unset($where["gri_complaint_assign.status"]);
                    $where["gri_complaint.reply_status"] = 0;
                }
               
                $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                $data['closed_count']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as closed_count' );
                //end closed complaints

                // echo $this->db->last_query();
                
                //open complaints
                $where["gri_complaint.reply_status"] = 1;
                if( $type==2 ||  $type==4 || $type==9 ){
                        unset($where["gri_complaint.compaint_closed_by"]);
                        $where["gri_complaint_assign.assign_user_id!="] = 8;
                        $where["gri_complaint_assign.status"] = 1;
                } 
                
                if ($type==3) {
                    unset($where["gri_complaint.compaint_closed_by"]);  
                     $where["gri_complaint.is_level_3"] = '1';
                }
                
                if ($type==1 || $type==7 || $type==15) {
                      unset($where["gri_complaint.compaint_closed_by"]);  
                     $where["gri_complaint.is_level_1"] = '1';
                     $where["gri_complaint_assign.status"] = 1;
                }
                if ($type==5) {
                    unset($where["gri_complaint.reply_status"]);
                     $where["gri_complaint.reply_status"] = 1;
                }

                $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                $data['open_count']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as open_count' );
                //end open complaints
                //echo $this->db->last_query();

                 if ($type==5) {
                    unset($where["gri_complaint.reply_status"]);
                     $where["gri_complaint.compaint_closed_by"]=$adminid;
                    }

                $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                $data['closed_by_cc']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as closed_by_cc' );

                // echo $this->db->last_query();
                
                //day wise open
                $where["gri_complaint.reply_status"] = 1;
                if($type==999 || $type==11 || $type==12 || $type==13 || $type==14 || $type==16){
                    $data['more_days']='12';
                    $twelve_days_ago = date('Y-m-d', strtotime('-12 days', time()));
                    $where["gri_complaint_assign.assign_date <="] = $twelve_days_ago;
                }
                 if($type==3 ){
                    $data['more_days']='12';
                    $twelve_days_ago = date('Y-m-d', strtotime('-12 days', time()));
                    $where["gri_complaint_assign.assign_date <="] = $twelve_days_ago;
                }
                if($type==2 || $type==9){
                    $data['more_days']='7';
                    $seven_days_ago = date('Y-m-d', strtotime('-7 days', time()));
                    $where["gri_complaint_assign.assign_date <"] = $seven_days_ago;
                }
                if($type==1 || $type==7 || $type==15 || $type==4 ){
                    $data['more_days']='5';
                    $five_days_ago = date('Y-m-d', strtotime('-5 days', time()));
                    $where["gri_complaint_assign.assign_date <="] = $five_days_ago;
                }
                $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                $data['open_more_days']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as open_more_days' );
                
                // echo $this->db->last_query();
                // echo "pre";
                // print_r($data);


                    $where["gri_complaint.reply_status"] = 1;
                    if($type==999 || $type==11 || $type==12 || $type==13 || $type==14 || $type==16){
                        $data['between_days']='5-11';
                        $five_days_ago = date('Y-m-d', strtotime('-5 days', time()));
                        $eleven_days_ago = date('Y-m-d', strtotime('-11 days', time()));
                        $where["gri_complaint_assign.assign_date <="] = $five_days_ago;
                        $where["gri_complaint_assign.assign_date >="] = $eleven_days_ago;
                    }
                    if($type==3){
                        $data['between_days']='5-11';
                        unset($where["gri_complaint_assign.assign_date <"]);
                        $five_days_ago = date('Y-m-d', strtotime('-5 days', time()));
                        $eleven_days_ago = date('Y-m-d', strtotime('-11 days', time()));
                        $where["gri_complaint_assign.assign_date <="] = $five_days_ago;
                        $where["gri_complaint_assign.assign_date >="] = $eleven_days_ago;
                    }
                    if($type==2 || $type==9){
                        $data['between_days']='4-7';
                        unset($where["gri_complaint_assign.assign_date <"]);
                        $four_days_ago = date('Y-m-d', strtotime('-4 days', time()));
                        $seven_days_ago = date('Y-m-d', strtotime('-7 days', time()));
                        $where["gri_complaint_assign.assign_date <="] = $four_days_ago;
                        $where["gri_complaint_assign.assign_date >="] = $seven_days_ago;
                    }
                    if($type==1 || $type==7 || $type==15 || $type==4 ){
                        $data['between_days']='3-4';
                        unset($where["gri_complaint_assign.assign_user_id!="]);
                        unset($where["gri_complaint_assign.assign_date <="]);
                        unset($where["gri_complaint_assign.assign_date >="]);
                        $three_days_ago = date('Y-m-d', strtotime('-3 days', time()));
                        $four_days_ago = date('Y-m-d', strtotime('-4 days', time()));
                        $where["gri_complaint_assign.assign_date <="] = $three_days_ago;
                        $where["gri_complaint_assign.assign_date >="] = $four_days_ago;
                    }
                    $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                    $data['open_more_between_days']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as open_more_between_days' );

           //  echo $this->db->last_query();
                // echo "pre";
                // print_r($data);

            
                    $where["gri_complaint.reply_status"] = 1;
                     if( $type==999 || $type==11 || $type==12 || $type==13 || $type==14 || $type==16){
                        $data['less_days']='4';
                        unset($where["gri_complaint_assign.assign_date <="]);
                        unset($where["gri_complaint_assign.assign_date >="]);
                        unset($where["gri_complaint_assign.assign_date <"]);
                       
                        $four_days_ago = date('Y-m-d', strtotime('-4 days', time()));
                         $where["gri_complaint_assign.assign_date >="] = $four_days_ago;
                       
                    }
                    if($type==3 ){
                        $data['less_days']='4';
                        unset($where["gri_complaint_assign.assign_date <="]);
                        unset($where["gri_complaint_assign.assign_date >="]);
                        unset($where["gri_complaint_assign.assign_date <"]);
                 
                        $four_days_ago = date('Y-m-d', strtotime('-4 days', time()));
                         $where["gri_complaint_assign.assign_date >="] = $four_days_ago;
                       
                    }
                    if($type==2 || $type==9){
                        unset($where["gri_complaint_assign.assign_date <="]);
                        unset($where["gri_complaint_assign.assign_date >="]);
                         unset($where["gri_complaint_assign.assign_date <"]);
                        $data['less_days']='3';
                    
                        $three_days_ago = date('Y-m-d', strtotime('-3 days', time()));
                         $where["gri_complaint_assign.assign_date >="] = $three_days_ago;
                       
                    }
                    if($type==1 || $type==7 || $type==15 || $type==4 ){
                        $data['less_days']='2';
                      unset($where['gri_complaint_assign.assign_date <=']);
                     
                        $two_days_ago = date('Y-m-d', strtotime('-2 days', time()));
                         $where["gri_complaint_assign.assign_date >="] = $two_days_ago;
                       
                    }
                    $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                    $data['open_less_than_days']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as open_less_than_days' );

            //echo $this->db->last_query();
                // echo "pre";
                // print_r($data);

            //pending at sro level
                $where["gri_complaint.reply_status"] = 1;
                if($type==999 || $type==11 || $type==12 || $type==13 || $type==14 || $type==16){
                   unset($where["gri_complaint_assign.assign_date >="]);
                   $data['less_days']='4';
                   $four_days_ago = date('Y-m-d', strtotime('-4 days', time()));
                   $where["gri_complaint.is_level_1"] = '1';
                   
                }
               
                $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                $data['open_at_sro_level']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as open_at_sro_level' );

                  // echo $this->db->last_query();
            //        echo "<br>";
            // print_r($data['open_at_sro_level']);
            // die;


            //pending at jdr level
                $where["gri_complaint.reply_status"] = 1;
                if($type==999 || $type==11 || $type==12 || $type==13 || $type==14 || $type==16){
                    $data['less_days']='4';
                  unset($where["gri_complaint.is_level_1"]);
                   $four_days_ago = date('Y-m-d', strtotime('-4 days', time()));
                   $where["gri_complaint.is_level_2"] = '1';
                   
                }
           
                $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                $data['open_at_jdr_level']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as open_at_jdr_level' );
                     //echo $this->db->last_query();
            // print_r($data['open_at_sro_level']);
            // die;


                if($type==1 || $type==7 || $type==15 || $type==4 ){
                    unset($where["gri_complaint_assign.assign_date >="]);
                    unset($where["gri_complaint.reply_status"]);
                    unset($where["gri_complaint.is_level_1"]);
                    unset($where["gri_complaint_assign.status"]);
                    $where["gri_complaint.is_level_2"] = '1';
                 }
                   
                $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                $data['escalated_to_jdr']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as escalated_to_jdr' );
                // echo $this->db->last_query();
                 // print_r($data['escalated_to_jdr']);
                 // die;

                if($type==999 || $type==2 || $type==9 || $type==11 || $type==12 || $type==13 || $type==14 || $type==16){
                    unset($where["gri_complaint.is_level_1"]);
                    unset($where["gri_complaint.is_level_2"]);
                    unset($where["gri_complaint_assign.assign_date <="]);
                    unset($where["gri_complaint_assign.assign_date >="]);
                    unset($where["gri_complaint_assign.assign_user_id!="]);
                    unset($where["gri_complaint_assign.status"]);
                    unset($where["gri_complaint.reply_status"]);
                    $where["gri_complaint.is_level_3"] = '1';
                 }
                   
                $this->db->join('gri_complaint_assign','gri_complaint.comp_code = gri_complaint_assign.comp_code');
                $data['escalated_to_igr']=$this->master_model->getRecords('gri_complaint',$where,'COUNT(DISTINCT(gri_complaint.comp_code)) as escalated_to_igr' );
                 // echo $this->db->last_query();
                 // print_r($data['escalated_to_igr']);
                 // die;
                // print_r($data['open_les_than_4_days']);
                //die;
                // echo "<pre>";
                // print_r($data);
                // die;

                $data['users_count']=$this->master_model->getRecords('userregistration');
                // echo "<pre>";
                // print_r($data);
                $data['middlecontent']='dashboard';
                $this->load->view('admin/admin_combo',$data);
            }
            
        }