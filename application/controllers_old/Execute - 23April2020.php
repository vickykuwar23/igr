<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// 1=SRO, 2=JDR, 3=IGRA, 4=HD, 5=CC, 7=CSO, 9=ACS, 10=DO9
class Execute extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();		
    }
	
	public function index()
	{
		date_default_timezone_set('Asia/Kolkata');	
		//$this->sampleMail();
		$current_date = date('Y-m-d 00:00:00');	 
		$this->db->select('gri_complaint.complaint_type_id, gri_complaint.grievence_details, gri_complaint.religion_id, gri_complaint.district_id, gri_complaint_assign.assign_user_id, gri_complaint_assign.end_date, gri_complaint.comp_code');
		$this->db->from('gri_complaint');
		$this->db->join('gri_complaint_assign', 'gri_complaint.comp_code = gri_complaint_assign.comp_code');
		$this->db->where('gri_complaint.reply_status', '1');
		$this->db->where('gri_complaint_assign.end_date <', $current_date);
		$this->db->where('gri_complaint_assign.status', '1');
		$this->db->where('gri_complaint.r_comp_code', '');
		$query = $this->db->get();
		$num_rows = $query->num_rows();	
		$result = $query->result_array();
		
		$subMail 	= 'Reminder – Grievance Redressal Portal';
		$fromEmail  = $this->config->item('MAIL_FROM');						 
		//$emailSend = $this->sentEmail('ABC1235', 'Test Messages! good one', $fromEmail, 'rohit.sharmar7777@gmail.com', $subMail, 'Rohit Sharma');
		//die();
		
		if($num_rows > 0){
			
			foreach($result as $row){				
				
				// Service Part
				if($row['complaint_type_id'] == 2){
					
					// Specific Office Else Part
					$assignOnline = $this->master_model->getRecords('adminlogin',array('id'=>$row['assign_user_id'], 'status'=>'1'),'','');
					$assignRoleID = $assignOnline[0]['role_id'];
					
					// Current Date			
					$current_date = date('Y-m-d 00:00:00');
					
					//Means SRO TO JDR USER Role ID = 1 Means SRO
					if($current_date > $row['end_date'] && $assignRoleID == '1'){
						
						//Old Status Update
						$update_status		=	array('status'	=> '0');						
						$this->master_model->updateRecord('gri_complaint_assign',$update_status,array('comp_code'=>"'".$row['comp_code']."'"));
						
												// Assign Complaint SRO To JDR User
						$getAssignuser = $this->master_model->getRecords('adminlogin',array('region_id'=> $row['religion_id'], 'district_id'=> $row['district_id'], 'role_id'=>'2', 'status'=>'1'));
						$jdrID 		= $getAssignuser[0]['id'];
						$jdremail	= $getAssignuser[0]['email'];
						$jdrname	= $getAssignuser[0]['namecc'];
						$contact_no	= $getAssignuser[0]['contact_no'];
						// Assign Days
						$date_com 	= date('Y-m-d');
						$endDate 	= date('Y-m-d', strtotime($date_com. ' + 10 days'));
						$createdAt	= date('Y-m-d H:i:s');
						
						$this->master_model->updateRecord('gri_complaint',array('is_level_1'=>'0','is_level_2'=>'1'),array('comp_code'=>"'".$row['comp_code']."'"));
						
						// Insert Array
						$insert_arr = array('comp_code' 		=> $row['comp_code'],
											'assign_user_id' 	=> $jdrID,
											'assign_date' 		=> $current_date,
											'end_date' 			=> $endDate,
											'status' 			=> '1',
											'createdat' 		=> $createdAt,
											'updatedat' 		=> $createdAt);					
						
						$jdrQuery = $this->master_model->insertRecord('gri_complaint_assign',$insert_arr);
						
						// Log Table Data added
						$insert_log = array('comp_code' 		=> $row['comp_code'],
											'assign_user_id' 	=> $jdrID,
											'assign_date' 		=> $current_date,
											'end_date' 			=> $endDate,
											'createdat' 		=> $createdAt,
											'role_name' 		=> 'SRO To JDR');					
						
						$logQuery  	= $this->master_model->insertRecord('cron_execution',$insert_log);							
						// Notification Send Via Email
						//$emailSend 	= $this->sentEmail($row['comp_code'], $row['grievence_details'], $fromEmail, $jdremail, $subMail, $jdrname);

							$info_arr=array(
							'to'=>$jdremail,					
							'cc'=>'',
							'from'=>$this->config->item('MAIL_FROM'),
							'subject'=>$subMail,
							'view'=>'execute_email'
							);

							$other_info=array('pattern'=>$row['comp_code'],'msg_content'=>$row['grievence_details'],'name'=>$jdrname); 
							$emailsend=$this->email_sending->sendmail($info_arr,$other_info);
						
						// Notification Send On Mobile Phone
						$smsSend 	= $this->send_sms_sro($contact_no, $row['comp_code']);					
						
					} else if($current_date > $row['end_date'] && $assignRoleID == '7'){
						
						$this->master_model->updateRecord('gri_complaint',array('is_level_1'=>'0','is_level_2'=>'1'),array('comp_code'=>$row['comp_code']));
						
						//Old Status Update
						$update_status		=	array('status'	=> '0');						
						$this->master_model->updateRecord('gri_complaint_assign',$update_status,array('comp_code'=>"'".$row['comp_code']."'"));
						
						// Assign Complaint CSO To ACS
						$getAssignuser 	= $this->master_model->getRecords('adminlogin',array('region_id'=> $row['religion_id'], 'district_id'=> $row['district_id'], 'role_id'=>'9', 'status'=>'1'));
						$acsID 			= $getAssignuser[0]['id'];
						$acsemail		= $getAssignuser[0]['email'];
						$acsaname		= $getAssignuser[0]['namecc'];
						$contact_no		= $getAssignuser[0]['contact_no'];
						
						// Assign Days
						$date_com 	= date('Y-m-d');
						$endDate 	= date('Y-m-d', strtotime($date_com. ' + 7 days'));
						$createdAt	= date('Y-m-d H:i:s');
						
						// Insert Array
						$insert_arr = array('comp_code' 		=> $row['comp_code'],
											'assign_user_id' 	=> $acsID,
											'assign_date' 		=> $current_date,
											'end_date' 			=> $endDate,
											'status' 			=> '1',
											'createdat' 		=> $createdAt,
											'updatedat' 		=> $createdAt);					
						
						$acsQuery = $this->master_model->insertRecord('gri_complaint_assign',$insert_arr);
						
						// Log Table Data added
						$insert_log = array('comp_code' 		=> $row['comp_code'],
											'assign_user_id' 	=> $jdrID,
											'assign_date' 		=> $current_date,
											'end_date' 			=> $endDate,
											'createdat' 		=> $createdAt,
											'role_name' 		=> 'CSO To ACS');					
						
						$logQuery  = $this->master_model->insertRecord('cron_execution',$insert_log);							
						// Notification Send Via Email
						//$emailSend = $this->sentEmail($row['comp_code'], $row['grievence_details'], $fromEmail, $acsemail, $subMail, $acsaname);
						$info_arr=array(
							'to'=>$acsemail,					
							'cc'=>'',
							'from'=>$this->config->item('MAIL_FROM'),
							'subject'=>$subMail,
							'view'=>'execute_email'
							);

							$other_info=array('pattern'=>$row['comp_code'],'msg_content'=>$row['grievence_details'],'name'=>$acsaname); 
							$emailsend=$this->email_sending->sendmail($info_arr,$other_info);
						
						// Notification Send On Mobile Phone
						$smsSend 	= $this->send_sms_sro($contact_no, $row['comp_code']);	
											
					} // Else If End 						
					
				} // Complaint Type If End
				
			} // Foreach End
			
		} // If End			
		
	} // Index function End	
	
	function send_sms_sro($mobile=NULL,$pattern=NULL)
 	{
 		if($mobile!=NULL && $pattern!=NULL)
 		{
			$mobile = '9595868692';
 			$text = "Complaint assign to you, on IGR Redressal System. Grievance Number: ".$pattern;
 			$msg = urlencode($text);
 			$url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH";

 			$string = preg_replace('/\s+/', '', $url);
 			$x = curl_init($string);
 			curl_setopt($x, CURLOPT_HEADER, 0);	
 			curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
 			curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);	
 			curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);		
 			$reply = curl_exec($x);
 		
 		}
 	}
	
	public function sentEmail($code, $details, $from, $to, $subject, $name){
		//echo $from."==".$to.">>>>".$name;die();
		$to 	= 'rohit.sharmar7777@gmail.com';	
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: '. $from . "\r\n" ;
		$headers .='Reply-To: '. $to . "\r\n" ;
		$headers .='X-Mailer: PHP/' . phpversion(); 	
		 
		// Compose a simple HTML email message
		$message = '<html><body>';		
		$message .= '<table>
						<tr>
							<td></td>
							<td width="600">
								<div>
									<table class="main" width="100%" cellpadding="0" cellspacing="0">															
										<tr>
											<td>
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr>
														<td><p>Dear '.$name.',</p></td>
													</tr>
													<tr></tr>
													<tr>
														<td><p>Following Grievance is pending for disposal at your end:</p></td>
													</tr>
													  <tr></tr>
														<tr></tr>
													<tr>
														<td> <div>
														Complaint No. :- '.$code.'
														</div></td>
													</tr>
													  <tr></tr>
													  <tr>
														<td> <div>
														Complaint No. :- '.$details.'
														</div></td>
													</tr>
													<tr>
														<td><p>Please expedite the closure of the said grievance by updating the status/reply in Grievance Redressal Portal.</p></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									</div>
							</td>
							<td></td>
						</tr>
					</table>';
		$message .= '</body></html>';
		 
		// Sending email
		if(mail($to, $subject, $message, $headers)){
			return true;
		} else{
			return false;
		}

		
		
	}
	
	
	public function sampleMail(){
		//$createdAt	= date('Y-m-d H:i:s');
		date_default_timezone_set('Asia/Kolkata');
		$createdAt = date("Y-m-d H:i:s");
		$to = "rohit.sharmar7777@gmail.com";
		$subject = "Cron Execution Time Reminder Email";
		$txt = "Hi Cron File Execute : " .$createdAt;
		$headers = "From: webmaster@example.com" . "\r\n" .
		"CC: somebodyelse@example.com";
		mail($to,$subject,$txt,$headers);
	}
	
	
	
} // Class End

?>