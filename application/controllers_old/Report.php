<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends CI_Controller 
{
   function __construct()
	{
		parent::__construct();
		/*$this->load->model('chk_session');
		 $this->load->model('master_model');
		 $this->chk_session->chk_admin_session();*/
		 $this->load->model('filters');
	}
	public function generaterepo() // Generating Standard Report
		{
		if($this->session->userdata('adminid')=='')
			{
			  redirect(base_url().'index.php/adminlogin/');
			}
			$cat=$type = $searchfrom=$searchto='';	;
			$data['error_msg'] = $data['error_up']='';
			/*if (!isset($_POST['grievance_cat']))
			{
				$cat='';
			}
			
			if (!isset($_POST['grievance_type']))
			{
				$type = '';	
			}*/
			if (isset($_POST['submitform']))
			{
				if (isset($_POST['grievance_type']))
				{
					$type = $this->input->post('grievance_type');
				}
				
				if ($type != '')
				{
					if ($type == 1)
					{
						
					}
					if ($type == 2)
					{
						$this->db->where('reply_status', '0');
					}
				
					if ($type == 3)
					{
						$this->db->where('reply_status', '1');
					}
				}
				
				if (isset($_POST['grievance_cat']))
				{
					$cat = $this->input->post('grievance_cat');
				}
				
				if ($cat != '')
				{
					if ($cat == 1)
					{
						$this->db->where('category', '1');
					}
				
					if ($cat == 2)
					{
						$this->db->where('category', '2');
					}
				
					if ($cat == 3)
					{
						$this->db->where('category', '3');
					}
				
					if ($cat == 4)
					{
						$this->db->where('category', '4');
					}
				
					if ($cat == 5)
					{
						$this->db->where('category', '5');
					}
				
					if ($cat == 6)
					{
						$this->db->where('category', '6');
					}
				}
				
				$searchfrom=trim($this->input->post('from_date'));
				$searchto=trim($this->input->post('to_date'));
				if($searchfrom!='' && $searchto!='')
				{
					$this->db->where('CAST(registerdate As Date) >=', date('Y-m-d',strtotime($searchfrom)));
					$this->db->where('CAST(registerdate As Date) <=', date('Y-m-d',strtotime($searchto)));
				}
			}
			if($searchfrom!='' && $searchto!='')
			{
				$this->db->order_by('msgid','ASC');
			}
			else
			{
				$this->db->order_by('msgid','DESC');
			}
				
			$data['complaint'] = $this->master_model->getRecords('usercomplainbox');
			// echo $this->db->last_query();die();
			$data['type'] = $type;
			$data['cat'] = $cat;
			$data['searchfrom']=$searchfrom;
			$data['searchto']=$searchto;
			$data['middlecontent'] = 'custrepo';
			$this->load->view('admintemplate', $data);
			}
	
	public function generaterepostd() // Generating Standard Report
	{  
	if($this->session->userdata('adminid')=='')
			{
			  redirect(base_url().'index.php/adminlogin/');
			}
	 
		$data['report_date']='';
		$this->form_validation->set_rules('report_date','Date','trim|xss_clean');
		if($this->form_validation->run())
        {
			if(isset($_POST['search_btn']))
			{
				$data['report_date']=$this->input->post('report_date');
			}
		}
		$this->db->like('type', 'business client');
		$this->db->or_like('type', 'share holder');
		$this->db->or_like('type', 'investor');
		$this->db->or_like('type', 'bond holder');
		$this->db->or_like('type', 'ex employee');
		$this->db->or_like('type', 'others');
		$data['category']=$this->master_model->getRecords('departments','','',array('id'=>'ASC'));
  	 	$data['middlecontent']='stdreport';
	    $this->load->view('template',$data);
	}
}