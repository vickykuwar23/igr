<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Subadmin extends CI_Controller 
{
   function __construct()
	{
		 parent::__construct();
		 $this->load->model('chk_session');
		 $this->chk_session->chk_superadmin_session();
	}
	
	public function add()
	{   
		//$data['roles'] = $this->master_model->getRecords('gri_roles',array('status'=>'1'));
		$data['roles'] = $this->master_model->getRecords('gri_roles',array('role_id!='=>'999','status'=>'1'));
		$data['get_regions']=$this->master_model->getRecords('gri_region_division',array('religion_id'=>'4'),'',array('religion_id'=>'ASC'));
	    $data['complaint_subtype_details']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => '2', 'status' => '1'),'',array('complaint_type_id'=>'DESC'));
		//echo "<pre>";print_r($data['complaint_subtype_details']);exit;
	  if(isset($_POST['submit']))
		{
		   
			if($this->input->post('role_id') == '1' || $this->input->post('role_id') == '7' || $this->input->post('role_id') == '15'){	

				$this->form_validation->set_rules('region_id', 'Region', 'required');
				$this->form_validation->set_rules('district_id', 'District', 'required');
				$this->form_validation->set_rules('sro_id', 'Sro Office', 'required');
				
			
			} else if($this->input->post('role_id') == '2'){
				
				if($this->input->post('region_id') == 4){					
					$this->form_validation->set_rules('office_id', 'Office Type', 'required');
				}
				$this->form_validation->set_rules('region_id', 'Region', 'required');
				$this->form_validation->set_rules('district_id', 'District', 'required');
				
			} else if($this->input->post('role_id') == 3 || $this->input->post('role_id') == 11 || $this->input->post('role_id') == 12 || $this->input->post('role_id') == 13 || $this->input->post('role_id') == 14 || $this->input->post('role_id') == 16){	
				
				//$this->form_validation->set_rules('region_id', 'Region', 'required');
				
			} else if($this->input->post('role_id') == '5'){
				
				//$this->form_validation->set_rules('region_id', 'Region', 'required');
				
			} else if($this->input->post('role_id') == '4'){
				
				//$this->form_validation->set_rules('region_id', 'Region', 'required');
				
			} else {
				
				$this->form_validation->set_rules('region_id', 'Region', 'required');
				$this->form_validation->set_rules('district_id', 'District', 'required');
				$this->form_validation->set_rules('sro_id', 'Sro Office', 'required');

			}		
			
			
			$this->form_validation->set_rules('role_id', 'Role', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|is_unique[adminlogin.adminuser]');
			$this->form_validation->set_rules('namecc', 'Fullname', 'trim|required|xss_clean');
			$this->form_validation->set_rules('address', 'Address', 'trim|required|xss_clean');
			$this->form_validation->set_rules('contact_no', 'Contact Number', 'trim|required|xss_clean|min_length[10]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|is_unique[adminlogin.email]');
			
			//$this->form_validation->set_rules('name', 'Username', 'trim|required|xss_clean|is_unique[adminlogin.adminuser]');
			//$this->form_validation->set_rules('namecc', 'Second Officer name', 'trim|required|xss_clean|is_unique[adminlogin.namecc]');
			//$this->form_validation->set_rules('emailcc', 'Second Officer email', 'trim|required|xss_clean|valid_email|is_unique[adminlogin.emailcc]');
			$existOfficer = 0;
			if($this->form_validation->run())
			{			
				//print_r($this->input->post());exit;
				if($this->input->post('role_id') == '1' || $this->input->post('role_id') == '7' || $this->input->post('role_id') == '15'){	
				
					if($this->input->post('region_id') == 4){
						if($this->input->post('office_id') == 3){
							$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'office_id'=>$this->input->post('office_id')));	
						} else if($this->input->post('office_id') == -1){
							
							$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'office_id !='=>'3'));
						}
						
					}
					$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'sro_id'=>$this->input->post('sro_id')));
				
				} else if($this->input->post('role_id') == '2'){
					
					if($this->input->post('region_id') == 4){
						if($this->input->post('office_id') == 3){
							$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'office_id'=>$this->input->post('office_id')));	
						} else if($this->input->post('office_id') == -1){
							
							$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'office_id !='=>'3'));
						}
						
					}
					
					$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id')));
					
				} else if($this->input->post('role_id') == '9'){
					
					if($this->input->post('region_id') == 4){
						if($this->input->post('office_id') == 3){
							$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'office_id'=>$this->input->post('office_id')));	
						} 						
					}
					
					$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id')));
					
				//} else if($this->input->post('role_id') == '3'){
				} else if($this->input->post('role_id') == 3 || $this->input->post('role_id') == 11 || $this->input->post('role_id') == 12 || $this->input->post('role_id') == 13 || $this->input->post('role_id') == 14 || $this->input->post('role_id') == 16){	
					
					$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id')));
					
				} else if($this->input->post('role_id') == '4' || $this->input->post('role_id') == '5'){
					
					$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id')));
					//$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'sro_id'=>$this->input->post('sro_id')));
				}  
				
				if(count($existOfficer) > 0){					
					
						$this->session->set_flashdata('error_message','The user already created for this role.');
						redirect(base_url().'subadmin/add');
						
				}
				
				//print_r($this->input->post());die();
				$_userName 		= 	htmlentities($this->input->post('username'));
				$_passWord 		= 	$this->input->post('password');
				$contact_no		= 	$this->input->post('contact_no');
				$address		= 	htmlentities($this->input->post('address'));
				$email 			=   htmlentities($this->input->post('email'));
				$fullname 		=   htmlentities($this->input->post('fullname'));
				$role_id 		= 	$this->input->post('role_id');
				$region_id 		= 	$this->input->post('region_id');
				$district_id 	= 	$this->input->post('district_id');
				$sro_id 		= 	$this->input->post('sro_id');
				$fullname 		= 	$this->input->post('namecc');
				$dateNow		= date('Ymd');
				$office_id 		= 	$this->input->post('office_id');
				//print_r($this->input->post());die();
				
				if($region_id!=""){$rid = $region_id;}else{$rid = "0";}
				if($district_id!=""){$did = $district_id;}else{$did = "0";}
				if($office_id!=""){$oid = $office_id;}else{$oid = "0";}
				if($sro_id!=""){$sid = $sro_id;}else{$sid = "0";}
				$insert_arr		=	array('adminuser'=>$_userName,
										  'adminpass'=>sha1($_passWord),
										  'email'=>$email,
										  'con_address'=>$address,
										  'namecc'=>$fullname,
										  'emailcc'=>'',
										  'contact_no'=>$contact_no,
										  'type'=>$role_id,
										  'role_id'=>$role_id,
										  'region_id'=>$rid,
										  'district_id'=>$did,
										  'sro_id'=>$sid,
										  'status'=>'1',
										  'created_at' => $dateNow,
										  'office_id' => $oid
										  );
				$user_info=$this->master_model->insertRecord('adminlogin',$insert_arr);

				 //add logs
				$json_encode_data = json_encode($insert_arr);
				$log_user_id      = $this->session->userdata('supadminid');
				$ipAddr			  = $this->get_client_ip();
				$logDetails = array(
									'module_name' 	=> "Subadmin",
									'action_name' 	=> "Add",
									'json_response'	=> $json_encode_data,
									'login_id'		=> $log_user_id,
									'role_type'		=> $this->session->userdata('role_name'),
									'createdAt'		=> date('Y-m-d H:i:s'),
									'ip_addr'		=> $ipAddr
									);
				$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
				//end add logs
				if(count($user_info))
				{ 
					$department_name=$this->master_model->getRecords('departments',array('id'=>$category),'type');
					$this->session->set_flashdata('success_message','The '.ucfirst($department_name[0]['type']).' user has been created');
				    redirect(base_url().'subadmin');
				}
			}
		}
	
		$data['department']=$this->master_model->getRecords('departments');
		$data['middlecontent']='admin_type/add';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
		
    }//F_login
	
	public function getdistrict(){
		$region_id 	= 	$this->input->post('region_id');
		$data['district_details']=$this->master_model->getRecords('gri_district',array('religion_id' => $region_id),'',array('religion_id'=>'DESC'));
		
				
		$options = '<div class="form-group" id="district_ids" >
                <label class="col-lg-3 control-label col-lg-offset-1">District <span style="color:red">*</span></label>
                <div class="col-lg-7">
						  <select id="district_id" class="form-control choosesub" name="district_id">
										<option value=""> -- Select --</option>';
						if(count($data['district_details'])>0)	{
							foreach($data['district_details'] as $district){
								$options .= '<option value="'.$district['district_id'].'">'.$district['district_name'].'</option>';
							}
						} else {
							$options .= '<option value="0">None</option>';
						}						
		$options .= '<div id="district_ids" style="color:#F00">'. form_error('district_id').' </div>
                </div>
              </div>';				
		echo $options;				
	}
	
	
	public function getDistrictlist(){
		
		$c_regionid 		= 	$this->input->post('regionid');
		$c_district_id		= 	$this->input->post('district_id');
		
		$data['distict_details'] = $this->master_model->getRecords('gri_district',array('religion_id' => $c_regionid),'',array('district_id'=>'ASC'));
		$options = '<option value="">-- Select --</option>';
		
		if($c_district_id!=""){
			if(count($data['distict_details'])>0)	{
				foreach($data['distict_details'] as $district){
					if($district['district_id'] == $c_district_id)
					{
						$sel = 'selected';
					} else {
						$sel = '';
					}
					$options .= '<option value="'.$district['district_id'].'" '.$sel.'>'.$district['district_name'].'</option>';
				}			
			} else {
				$options .= '<option value="">None</option>';
			}
		} else {
			
			if(count($data['distict_details'])>0)	{
				foreach($data['distict_details'] as $district){
					$options .= '<option value="'.$district['district_id'].'">'.$district['district_name'].'</option>';
				}			
			} else {
				$options .= '<option value="">None</option>';
			}
		}		
		
		echo $options;
		
	}
	
	// Submit Office List
	public function getSroOffice(){
		$c_office_type 		= 	$this->input->post('office_id');
		$c_regionid 		= 	$this->input->post('region_id');
		$c_district_id 		= 	$this->input->post('district_id');
		$c_sro_id			= 	$this->input->post('sro_off');
		
		
		$data['sro_office'] =	$this->master_model->getRecords('gri_sro_offices',array('district_id' => $c_district_id, 'office_sub_type' => $c_office_type, 'status' => '1'),'',array('sro_office_id'=>'ASC'));
		
		$options = '<option value="">-- Select --</option>';
		if(count($data['sro_office'])>0)	{
			foreach($data['sro_office'] as $office){
				if($office['sro_office_id'] == $c_sro_id){
					$oType = 'selected';
				} else {
					$oType = '';
				}
				$options .= '<option value="'.$office['sro_office_id'].'" '.$oType.'>'.$office['office_name'].'</option>';
			}			
		} else {
			$options .= '<option value="0">None</option>';
		}
		echo $options;	
	
	}
	
	// SRO Offices List
	public function getOffice(){
		$district_id 	= 	$this->input->post('district_id');
		$office_id 	= 	$this->input->post('office_id');
		$role_id 	= 	$this->input->post('role_id');
		
		$data['sro_details']=$this->master_model->getRecords('gri_sro_offices',array('district_id' => $district_id, 'office_sub_type' => $office_id),'',array('sro_office_id'=>'DESC'));
		//echo $this->db->last_query();die();
		$options = '';
		if(count($data['sro_details'])>0)	{
			$options .= "<option value=''>--Select--</option>";
			foreach($data['sro_details'] as $sro){
				$options .= '<option value="'.$sro['sro_office_id'].'">'.$sro['office_name'].'</option>';
			}
		} else {
			$options .= '<option value="0">None</option>';
		}	
		echo $options;				
	}
	
	public function offType(){
		$role_id 	= 	$this->input->post('role_id');
		$region_id 	= 	$this->input->post('region_id');
		$options = "";		
		if($role_id == 2 && $region_id == 4){
			
			$options .= "<option value=''>--Select--</option><option value='-1'>Sub Registrar & Marriage Registrar Office</option>";			
		
		} else if(($role_id == 9 && $region_id == 4) || ($role_id == 7 && $region_id == 4)){
			
			$options .= "<option value=''>--Select--</option><option value='2'>Collector Of Stamp Office</option>";			
		
		} else {
			
			$data['off_type']=$this->master_model->getRecords('gri_complaint_sub_type',array('status' => '1', 'complaint_type_id' => '2'),'',array('complaint_sub_type_id'=>'DESC'));
			$options = "<option value=''>--Select--</option>";
			foreach($data['off_type'] as $offt){
				$options .= "<option value='".$offt['complaint_sub_type_id']."'>".$offt['complaint_sub_type_name']."</option>";
			}
		}	
		
		echo $options;
	}
	
	public function index()
	{	
		$data['adminuser']=$this->master_model->getRecords('adminlogin',array('role_id !='=>'999'),'',array('id'=>'DESC'));
		$data['middlecontent']='admin_type/index';
		$data['master_mod'] = 'Master';	
	    $this->load->view('admin/admin_combo',$data);
	}
	
	public function edit()
	{	
		$data['error_msg']='';
		$adminid=$this->uri->segment('3');
		//$data['roles'] = $this->master_model->getRecords('gri_roles',array('status'=>'1'));
		$data['roles'] = $this->master_model->getRecords('gri_roles',array('role_id!='=>'999','status'=>'1'));
		$data['get_regions']=$this->master_model->getRecords('gri_region_division',array('religion_id'=>'4'),'',array('religion_id'=>'ASC'));
	  	$data['get_adminlist']=$this->master_model->getRecords('adminlogin',array('id'=>$adminid),'','');	
		$data['complaint_subtype_details']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => '2', 'status' => '1'),'',array('complaint_type_id'=>'DESC'));
		
		if(isset($_POST['btn_edit']))
		{		
			
			if($this->input->post('role_id') == '1' || $this->input->post('role_id') == '7' || $this->input->post('role_id') == '15'){	

				$this->form_validation->set_rules('region_id', 'Region', 'required');
				$this->form_validation->set_rules('district_id', 'District', 'required');
				$this->form_validation->set_rules('sro_id', 'Sro Office', 'required');
				
			
			} else if($this->input->post('role_id') == '2' || $this->input->post('role_id') == '9'){
				
				$this->form_validation->set_rules('region_id', 'Region', 'required');
				$this->form_validation->set_rules('district_id', 'District', 'required');
				
			//} else if($this->input->post('role_id') == '3'){
			} else if($this->input->post('role_id') == 3 || $this->input->post('role_id') == 11 || $this->input->post('role_id') == 12 || $this->input->post('role_id') == 13 || $this->input->post('role_id') == 14 || $this->input->post('role_id') == 16){		
				
				//$this->form_validation->set_rules('region_id', 'Region', 'required');
				
			} else if($this->input->post('role_id') == '5' || $this->input->post('role_id') == '4'){
				
				//$this->form_validation->set_rules('region_id', 'Region', 'required');
				
			} else {
				
				$this->form_validation->set_rules('region_id', 'Region', 'required');
				$this->form_validation->set_rules('district_id', 'District', 'required');
				$this->form_validation->set_rules('sro_id', 'Sro Office', 'required');

			}		
			
			
			$this->form_validation->set_rules('role_id', 'Role', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|callback_exists_username');
			$this->form_validation->set_rules('namecc', 'Fullname', 'trim|required|xss_clean');
			$this->form_validation->set_rules('address', 'Address', 'trim|required|xss_clean');
			$this->form_validation->set_rules('contact_no', 'Contact Number', 'trim|required|xss_clean|min_length[10]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|callback_exists_email');
						
			
			if($this->form_validation->run())
			{
				
				if($this->input->post('role_id') == '1' || $this->input->post('role_id') == '7' || $this->input->post('role_id') == '15'){		
				
					if($this->input->post('region_id') == 4){
						if($this->input->post('office_id') == 3){
							$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'office_id'=>$this->input->post('office_id'), 'id !='=>$adminid));	
						} else if($this->input->post('office_id') == -1){
							
							$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'office_id !='=>'3', 'id !='=>$adminid));
						}
						
					}
					$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'sro_id'=>$this->input->post('sro_id'), 'id !='=>$adminid));
					
				} else if($this->input->post('role_id') == '2'){
					
					if($this->input->post('region_id') == 4){
						if($this->input->post('office_id') == 3){
							$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'office_id'=>$this->input->post('office_id'), 'id !='=>$adminid));	
						} else if($this->input->post('office_id') == -1){
							
							$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'office_id !='=>'3', 'id !='=>$adminid));
						}
						
					}
					
					$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'id !='=>$adminid));
					
				} else if($this->input->post('role_id') == '9'){
					
					if($this->input->post('region_id') == 4){
						if($this->input->post('office_id') == 3){
							$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'office_id'=>$this->input->post('office_id'), 'id !='=>$adminid));	
						} 						
					}
					
					$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'region_id'=>$this->input->post('region_id'), 'district_id'=>$this->input->post('district_id'), 'id !='=>$adminid));
					
				} else if($this->input->post('role_id') == 3 || $this->input->post('role_id') == 11 || $this->input->post('role_id') == 12 || $this->input->post('role_id') == 13 || $this->input->post('role_id') == 14 || $this->input->post('role_id') == 16){	
					
					$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'id !='=>$adminid));
					
				} else if($this->input->post('role_id') == '4' || $this->input->post('role_id') == '5'){
					
					$existOfficer = $this->master_model->getRecords('adminlogin',array('role_id'=>$this->input->post('role_id'), 'id !='=>$adminid));
					
				}
				//print_r($existOfficer);die();
				if(count($existOfficer) > 0){					
					
						$this->session->set_flashdata('error_message','The user already assign for this role.');
						redirect(base_url().'subadmin/edit/'.$adminid);
						
				}
				
				$_userName 		= 	htmlentities($this->input->post('username'));
				$_passWord 		= 	trim($this->input->post('password'));
				$contact_no		= 	$this->input->post('contact_no');
				$address		= 	htmlentities($this->input->post('address'));
				$email 			=   htmlentities($this->input->post('email'));
				$fullname 		=   htmlentities($this->input->post('fullname'));
				$role_id 		= 	$this->input->post('role_id');
				$region_id 		= 	$this->input->post('region_id');
				$district_id 	= 	$this->input->post('district_id');
				$sro_id 		= 	$this->input->post('sro_id');
				$fullname 		= 	$this->input->post('namecc');
				if($region_id!=""){$rid = $region_id;}else{$rid = "0";}
				if($district_id!=""){$did = $district_id;}else{$did = "0";}
				if($office_id!=""){$oid = $office_id;}else{$oid = "0";}
				if($sro_id!=""){$sid = $sro_id;}else{$sid = "0";}
				
				
				
				if($_passWord!="")
				{ 
					$password= $this->input->post('password');
					
					if($this->input->post('role_id') == '1' || $this->input->post('role_id') == '7'){
						
						$update_arr		=	array('adminuser'	=>$_userName,
												 'adminpass'	=>sha1($_passWord),
												  'email'		=>$email,
												  'con_address'	=>$address,
												  'namecc'		=>$fullname,
												  'contact_no'	=>$contact_no,
												  'type'		=>$role_id,
												  'role_id'		=>$role_id,
												  'region_id'	=>$rid,
												  'district_id'	=>$did,
												  'sro_id'		=>$sro_id
										  );
							} else {
								
								$update_arr		=	array('adminuser'	=>$_userName,
													  'adminpass'	=>sha1($_passWord),
													  'email'		=>$email,
													  'con_address'	=>$address,
													  'namecc'		=>$fullname,
													  'contact_no'	=>$contact_no,
													  'type'		=>$role_id,
													  'role_id'		=>$role_id,
													  'region_id'	=>$rid,
													  'district_id'	=>$did
													  
													  );
								
							}
							
							if($this->master_model->updateRecord('adminlogin',$update_arr,array('id'=>"'".$adminid."'")))
							{	//echo "+++++++++++++++++++++++++";die();
								//$department_name=$this->master_model->getRecords('departments',array('id'=>$category),'type');
								//add logs
								$for_id=array('update_record_id'=>$adminid);
								$array_first = array_merge($update_arr,$for_id);
								$json_encode_data = json_encode($array_first);
								$log_user_id      = $this->session->userdata('supadminid');
								$ipAddr			  = $this->get_client_ip();
								$logDetails = array(
													'module_name' 	=> "Subadmin",
													'action_name' 	=> "Edit",
													'json_response'	=> $json_encode_data,
													'login_id'		=> $log_user_id,
													'role_type'		=> $this->session->userdata('role_name'),
													'createdAt'		=> date('Y-m-d H:i:s'),
													'ip_addr'		=> $ipAddr
													);
								$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								//end add logs
								$this->session->set_flashdata('success_message','Admin user has been updated successfully.');
								redirect(base_url().'subadmin');
							}
							else
							{
								$data['error_msg']='Error while updating record.';
							}
					
					
				} else {  
			
						if($this->input->post('role_id') == '1' || $this->input->post('role_id') == '7'){
						
						$update_arr		=	array('adminuser'	=>$_userName,
											  'email'		=>$email,
											  'con_address'	=>$address,
											  'namecc'		=>$fullname,
											  'contact_no'	=>$contact_no,
											  'type'		=>$role_id,
											  'role_id'		=>$role_id,
											  'region_id'	=>$rid,
											  'district_id'	=>$did,
											  'sro_id'		=>$sro_id,
										  );
							} else {
								
								$update_arr		=	array('adminuser'	=>$_userName,
														  'email'		=>$email,
														  'con_address'	=>$address,
														  'namecc'		=>$fullname,
														  'contact_no'	=>$contact_no,
														  'type'		=>$role_id,
														  'role_id'		=>$role_id,
														  'region_id'	=>$rid,
														  'district_id'	=>$did
												  );
								
							}
							$this->master_model->updateRecord('adminlogin',$update_arr,array('id'=>"'".$adminid."'"));
							//echo $this->db->last_query(); die();
							if($this->master_model->updateRecord('adminlogin',$update_arr,array('id'=>"'".$adminid."'")))
							{	
								$for_id=array('update_record_id'=>$adminid);
								$array_first = array_merge($update_arr,$for_id);
								$json_encode_data = json_encode($array_first);
								$log_user_id      = $this->session->userdata('supadminid');
								$ipAddr			  = $this->get_client_ip();
								$logDetails = array(
													'module_name' 	=> "Subadmin",
													'action_name' 	=> "Edit",
													'json_response'	=> $json_encode_data,
													'login_id'		=> $log_user_id,
													'role_type'		=> $this->session->userdata('role_name'),
													'createdAt'		=> date('Y-m-d H:i:s'),
													'ip_addr'		=> $ipAddr
													);
								$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								//$department_name=$this->master_model->getRecords('departments',array('id'=>$category),'type');
								$this->session->set_flashdata('success_message','Admin user has been updated successfully.');
								redirect(base_url().'subadmin');
							}
							else
							{
								$data['error_msg']='Error while updating record.';
							}
					
					}
							
							//echo "<pre>";print_r($update_arr);die();
					
			}
		}
		
		$data['department']=$this->master_model->getRecords('departments');
		$data['middlecontent']='admin_type/edit';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	public function exists_username($username){
		$record_id = $this->uri->segment('3');
        $value = $this->master_model->getRecords('adminlogin',array('adminuser '=>$username, 'id !=' =>  $record_id),'','');	
		
	   if (count($value) == 0)
        {
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('exists_username', 'Username already exist.');
            return FALSE;
        }
	} 
	
	public function exists_email($email){
		$record_id = $this->uri->segment('3');
		
        $value = $this->master_model->getRecords('adminlogin',array('email'=>$email, 'id !=' =>  $record_id),'','');	
        if (count($value) == 0)
        {
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('exists_email', 'Email already exist.');
            return FALSE;
        }
	}
	
	public function delete()
	{
		$adminid=$this->uri->segment('3');
		if(!is_int($adminid) && $adminid=='')
		{
			redirect(base_url().'subadmin');
		}
		if($this->master_model->deleteRecord('adminlogin','id',$adminid))
		{
			// logs
			$for_id=array('record_id'=>$adminid);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Subadmin",
								'action_name' 	=> "Delete",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);

		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'subadmin');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'subadmin');
		}
	
	}
	
	public function change_status()
	{
		 $id = $this->uri->segment(3);
		 $status = $this->uri->segment(4);
	
		if($status=='1'){$newstatus = '0'; }
		else if($status=='0'){$newstatus = '1';}
		if($this->master_model->updateRecord("adminlogin",array('status'=>$newstatus)
		,array('id'=>$id)))
		{	
			// logs
			$for_id=array('record_id'=>$id,'status'=>$status);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Subadmin",
								'action_name' 	=> "change status",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			$this->session->set_flashdata('success_message','Status changed successfully.');						           
			redirect(base_url().'subadmin');
		}
		else
		{
			$this->session->set_flashdata('error_message','Error while updating user status.');										            
			redirect(base_url().'subadmin');
		}
	}

	function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }
	
	
}//C_welcome