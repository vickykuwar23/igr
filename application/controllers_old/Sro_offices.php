<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sro_offices extends CI_Controller 
{

	// function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->model('chk_session');
	// 	$this->chk_session->chk_superadmin_session();
 //    }
	
	public function index()
	{
		$data['records']=$this->master_model->getRecords('gri_sro_offices','','',array('sro_office_id'=>'DESC'));
		$data['middlecontent']='sro_offices/index';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}

	public function add()
	{	
		$this->form_validation->set_rules('office_name', 'SRO Office name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('district_id', 'District', 'trim|required|numeric|xss_clean');
		//$this->form_validation->set_rules('pincode', 'Pincode', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('office_id', 'Office Sub Type', 'trim|required');
		$data['complaint_subtype_details']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => '2', 'status' => '1'),'',array('complaint_type_id'=>'DESC'));
		if($this->form_validation->run())
		{
			$office_name 	= 	htmlentities($this->input->post('office_name'));
			$district_id 	= 	$this->input->post('district_id');
			$pincode 		= 	$this->input->post('pincode');
			$office_id 		= 	$this->input->post('office_id');
			$createdAt			=   date('Y-m-d H:i:s');
			$insert_arr=array('office_name'=>$office_name,
							  'district_id'=>$district_id,
							  'office_sub_type'=>$office_id,
							  'pincode'=>$pincode,
							  'status'=>'1',
							  'createdat'=>	$createdAt,
							  'updatedat'=>	$createdAt
							  );

			if($this->master_model->insertRecord('gri_sro_offices',$insert_arr)){
				//add logs
				$json_encode_data = json_encode($insert_arr);
				$log_user_id      = $this->session->userdata('supadminid');
				$ipAddr			  = $this->get_client_ip();
				$logDetails = array(
									'module_name' 	=> "Sro_offices",
									'action_name' 	=> "Add",
									'json_response'	=> $json_encode_data,
									'login_id'		=> $log_user_id,
									'role_type'		=> $this->session->userdata('role_name'),
									'createdAt'		=> date('Y-m-d H:i:s'),
									'ip_addr'		=> $ipAddr
									);
				$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
				//end add logs	
			$this->session->set_flashdata('success_message','Sro office has been created');
			redirect(base_url().'sro_offices');
		}else{
			$this->session->set_flashdata('error_message','Error while processing your request');
			redirect(base_url().'sro_offices/add');
		}
		}
		$data['master_mod'] = 'Master';
		$data['districts']=$this->master_model->getRecords('gri_district','','',array('district_id'=>'DESC'));
		$data['middlecontent']='sro_offices/add';	
	 	$this->load->view('admin/admin_combo',$data);
	}
	
/****** Edit Functionality ******/ 
	
	public function edit()
	{
		$data['error_msg']='';
		$updateid=$this->uri->segment('3');
		$data['complaint_subtype_details']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => '2', 'status' => '1'),'',array('complaint_type_id'=>'DESC'));
		$data['info']=$this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$updateid),'','');	
		$this->form_validation->set_rules('office_name', 'SRO Office name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('district_id', 'District', 'trim|required|numeric|xss_clean');	
		$this->form_validation->set_rules('office_id', 'Office Sub Type', 'trim|required');
		if($this->form_validation->run())
			{
				$office_name 		= 	htmlentities($this->input->post('office_name'));
				$district_id 	= 	$this->input->post('district_id');
				$pincode 		= 	$this->input->post('pincode');
				$officeSub 		= 	$this->input->post('office_id');
				$createdAt			=   date('Y-m-d H:i:s');
				$update_arr=array('office_name'=>$office_name,
								  'district_id'=>$district_id,
								  'office_sub_type'=>$officeSub,
									'updatedat'=>	$createdAt
								  );
				
				if($this->master_model->updateRecord('gri_sro_offices',$update_arr,array('sro_office_id'=>"'".$updateid."'")))
				{
					//add logs
					$for_id=array('record_id'=>$updateid);
					$array_first = array_merge($update_arr,$for_id);
					$json_encode_data = json_encode($array_first);
					$log_user_id      = $this->session->userdata('supadminid');
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Sro_offices",
										'action_name' 	=> "Edit",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> $this->session->userdata('role_name'),
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
					//end add logs
					$this->session->set_flashdata('success_message',' SRO office has been updated successfully.');
					redirect(base_url().'sro_offices/edit/'.$updateid);
				}
				else
				{
					$this->session->set_flashdata('error_message','Error while processing your request');
					redirect(base_url().'sro_offices/edit');
				}
			}
		

		// $data['info']=$this->master_model->getRecords('gri_region_division',array('religion_id'=>$updateid));
		// if(count($data['info']) <= 0)
		// {
		// 	redirect(base_url().'region');
		// }
		$data['districts']=$this->master_model->getRecords('gri_district','','',array('district_id'=>'DESC'));
		$data['master_mod'] = 'Master';
		$data['middlecontent']='sro_offices/edit';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	public function delete()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'sro_offices');
		}
		if($this->master_model->deleteRecord('gri_sro_offices','sro_office_id',$id))
		{
			// logs
			$for_id=array('record_id'=>$id);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Sro_offices",
								'action_name' 	=> "Delete",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			// end logs	
		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'sro_offices');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'sro_offices');
		}
	
	}

	public function change_status()
	{
		 $id = $this->uri->segment(3);
		 $status = $this->uri->segment(4);
	
		if($status=='1'){$newstatus = '0'; }
		else if($status=='0'){$newstatus = '1';}
		if($this->master_model->updateRecord("gri_sro_offices",array('status'=>$newstatus)
		,array('sro_office_id'=>$id)))
		{
			// logs
			$for_id=array('record_id'=>$id,'status'=>$status);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Sro_offices",
								'action_name' 	=> "change status",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			// logs end
			$this->session->set_flashdata('success_message','Status changed successfully.');						           
			redirect(base_url().'sro_offices');
		}
		else
		{
			$this->session->set_flashdata('error_message','Error while updating user status.');										            
			redirect(base_url().'sro_offices');
		}
	}

	function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }
	
} // Class End

?>