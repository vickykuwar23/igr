<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		$this->chk_session->chk_superadmin_session();
    }
	
	public function index()
	{
		$data['roles']=$this->master_model->getRecords('gri_roles',array('role_id !='=>'999'),'',array('role_id'=>'ASC'));
		$data['middlecontent']='role/index';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}

	public function add()
	{	
		$this->form_validation->set_rules('role_name', 'Role', 'trim|required|xss_clean');
		if($this->form_validation->run())
		{
			$c_role_name 		= 	htmlentities($this->input->post('role_name'));
			
			$createdAt			=   date('Y-m-d H:i:s');
			$insert_arr=array('role_name'=>$c_role_name,							  
							  'createdat'=>$createdAt,
							  'updatedat'=>$createdAt
							  );
			$complaintType_info = $this->master_model->insertRecord('gri_roles',$insert_arr);
			$this->session->set_flashdata('success_message','The role has been created');
			redirect(base_url().'role');
		}
		
		$data['middlecontent']='role/add';	
		$data['master_mod'] = 'Master';
	 	$this->load->view('admin/admin_combo',$data);
	}
	
/****** Edit Functionality ******/ 
	
	public function edit()
	{
		$data['error_msg']='';
		$updateid=$this->uri->segment('3');
		
			$this->form_validation->set_rules('role_name', 'Role', 'trim|required|xss_clean');
			if($this->form_validation->run())
			{
				$c_role_name 		= 	htmlentities($this->input->post('role_name'));
				$updatedAt			=   date('Y-m-d H:i:s');
				
				$update_arr=array('role_name'=>$c_role_name,
								  'updatedat'=>$updatedAt,
								  );
				
				if($this->master_model->updateRecord('gri_roles',$update_arr,array('role_id'=>"'".$updateid."'")))
				{
					$this->session->set_flashdata('success_message','Role has been updated successfully.');
					redirect(base_url().'role/edit/'.$updateid);
				}
				else
				{
					$data['error_msg']='Error while updating record.';
				}
			}
		

		$data['role_data']=$this->master_model->getRecords('gri_roles',array('role_id'=>$updateid));
		if(count($data['role_data']) <= 0)
		{
			redirect(base_url().'role/view/');
		}
			
		$data['middlecontent']='role/edit';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	public function delete()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'role');
		}
		if($this->master_model->deleteRecord('gri_roles','role_id',$id))
		{
		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'role');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'role');
		}
	
	}

	public function change_status()
	{
		 $id = $this->uri->segment(3);
		 $status = $this->uri->segment(4);
	
		if($status=='1'){$newstatus = '0'; }
		else if($status=='0'){$newstatus = '1';}
		if($this->master_model->updateRecord("gri_roles",array('status'=>$newstatus)
		,array('role_id'=>$id)))
		{
			$this->session->set_flashdata('success_message','Status changed successfully.');						           
			redirect(base_url().'role');
		}
		else
		{
			$this->session->set_flashdata('error_message','Error while updating user status.');										            
			redirect(base_url().'role');
		}
	}
	
} // Class End

?>