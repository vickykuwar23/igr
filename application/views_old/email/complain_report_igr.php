<html>
<body style="font-family:Tahoma, Geneva, sans-serif;">
<table style="border:1px solid #0b95c3;  font-size:14px !important; line-height:18px; background-color:#e9faff; padding:10px;" width="381">
<tr><td>Greetings!</td></tr>
<tr>
  <td>Grievance has been successfully reported to IGRA. Following are the details:</td></tr>
</table>
<table style="border-top:1px solid #0b95c3; border-left:1px solid #0b95c3; font-size:14px !important; line-height:18px; background-color:#e9faff; margin-top:10px; margin-bottom:10px;" width="381" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="175" style="border-right:1px solid #0b95c3; border-bottom:1px solid #0b95c3; padding:5px;">Reference Number</td>
<td width="206" style="color:#09F; border-right:1px solid #0b95c3; border-bottom:1px solid #0b95c3; padding:5px;"><b><?php echo $pattern;?></b></td>
</tr>
<tr>
<td style="border-right:1px solid #0b95c3; border-bottom:1px solid #0b95c3; padding:5px;">Name</td>
<td style="color:#09F; border-right:1px solid #0b95c3; border-bottom:1px solid #0b95c3; padding:5px;"><?php echo ucfirst($name);?></td>
</tr>
<tr>
<td style="border-right:1px solid #0b95c3; border-bottom:1px solid #0b95c3; padding:5px;">Email</td>
<td style="color:#09F; border-right:1px solid #0b95c3; border-bottom:1px solid #0b95c3; padding:5px;"><?php echo $user_email;?></td>
</tr>
<tr>
<td style="border-right:1px solid #0b95c3; border-bottom:1px solid #0b95c3; padding:5px;">Mobile</td>
<td style="color:#09F; border-right:1px solid #0b95c3; border-bottom:1px solid #0b95c3; padding:5px;"><?php echo $user_mobile;?></td>
</tr>

</table>


</body>
</html>