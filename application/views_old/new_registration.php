<style>
   .er-success{color:#00FF00;}
   .er-error{color:red;}
</style>


<div id="register_id">
   <section id="content">
      <section class="main padder">
         <div class="row">
            <div class="col-sm-6 col-xs-offset-3">
               <section class="panel m-t grdbg">
                  <header class="panel-heading text-center"><i class="fa fa-edit"></i>User Registration Form</header>
                  <div class="panel-body">
                     <form class="form-horizontal text-center" method="post" data-validate="parsley">
                        <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1">Name <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter name as per Aadhar card / PAN card"></i>
                           <div class="col-lg-7">
                              <input type="text" name="name" data-required="true" class="form-control" value="<?php echo set_value('name')?>" id="name"> 
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('name');?></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1">Username <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter username"></i>
                           <div class="col-lg-7">
                              <input type="text" name="username" data-required="true" class="form-control" value="<?php echo set_value('username')?>" id="username"> 
                              <div class="m-t m-t-mini" style="color:#F00" id="usererror"> <?php echo form_error('username');?></div>
                              <div class="m-t m-t-mini" id="usernamechk"  style="text-align:left"></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1">Email <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your email address"></i>
                           <div class="col-lg-7">
                              <input type="text" name="email" class="form-control" data-required="true" data-type="email" value="<?php echo set_value('email')?>" id="email">
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('email');?></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1">Mobile No. <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your mobile number"></i>
                           <div class="col-lg-7">
                              <input type="text" name="mobile" class="form-control" value="<?php echo set_value('mobile')?>" id="mobile">
                              <div class="m-t m-t-mini" style="color:#F00"><?php echo form_error('mobile');?></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1">Landline No. </label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your Landline number"></i>
                           <div class="col-lg-7">
                              <input type="text" name="landline" class="form-control" value="<?php echo set_value('landline')?>" id="landline">
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('landline');?></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1">Address Line 1 <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your  address"></i>
                           <div class="col-lg-7">
                              <input type="text" name="address1" class="form-control" value="<?php echo set_value('address1')?>" id="address1">
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('address1');?></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1">Address Line 2</label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your alternate address"></i>
                           <div class="col-lg-7">
                              <input type="text" name="address2" class="form-control" value="<?php echo set_value('address2')?>" id="address2">
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('address2');?></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1">State <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Select state"></i>
                           <div class="col-lg-7">
                              <select class="form-control states" name="state" class="states">
                                 <option value="">Select State</option>
                                 <?php foreach($state as $states) {?>
                                 <option value="<?php echo $states['ch2']?>" <?php if($states['ch2']==set_value('state')){echo "selected='selected'";}?>><?php echo $states['ch3']?></option>
                                 <?php } ?>
                              </select>
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('state');?></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1">City <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Select city"></i>
                           <div class="col-lg-7">
                              <select class="form-control" name="city" id="selectcity">
                                 <option value="">Select City</option>
                                 <?php 
                                    if($state!='')
                                    {
                                      $city= $this->master_model->getRecords('city',array('ch3'=>set_value('state')));
                                    }
                                    if(count($city) > 0)
                                    {
                                      foreach($city as $res)
                                      {?>
                                 <option value="<?php echo $res['ch4']?>" <?php if($res['ch4']==set_value('city')){echo "selected='selected'";}?>><?php echo $res['ch4'];?></option>
                                 <?php }
                                    }?>
                              </select>
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('city');?></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1">Pincode <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter Pincode"></i>
                           <div class="col-lg-7">
                              <input type="text" name="pincode"  placeholder="" class="form-control" value="<?php echo set_value('pincode')?>" id="pincode"> 
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('pincode');?></div>
                           </div>
                        </div>

                         <div class="form-group">
                           <label class="col-lg-3 control-label col-lg-offset-1"> Password <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter Password"></i>
                           <div class="col-lg-7">
                              <input type="text" name="password"  placeholder="" class="form-control" value="<?php echo set_value('password')?>" id="password"> 
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('password');?></div>
                           </div>
                        </div>
                      
                        <div class="form-group">
                           <div class="col-lg-6 col-lg-offset-3">
                              <button type="submit" class="btn btn-white">Cancel</button>
                              <button type="submit" class="btn btn-primary no-shadow" name="btn_reg">Submit</button>
                              <a class="btn btn-primary no-shadow" href="<?php echo base_url();?>callcenterpanel/users">Go back</a>
                           </div>
                        </div>
                        
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

                     </form>
                  </div>
               </section>
            </div>
         </div>
      </section>
   </section>
</div>
<script type="text/javascript">
   $(document).ready(function(){
     var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
     $(".states").on('change',function(){
       var datastring='stateid='+ $(this).val() + '&' + csrfName + '='+csrfHash;
       $.ajax({
           type: 'POST',
           data:datastring,
           url: "<?php echo base_url();?>index.php/userlogin/getcity",
           success: function(res){ 
             $('#selectcity').html(res);
           }
         });
     })
   
   
    $('#register_close').on('click',function(){
      $("#register_id").css("display", "block");
      });
   //to seee the existing user
      $("#username").keyup(function() {
       $("#usernamechk").html("");
        $("#usererror").html("");
       
           var username = $("#username").val();
   
      
           $.post("<?php echo base_url();?>index.php/userlogin/chkusername", {username:username, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'},
               function(result) {
          var regex = /^[a-z0-9]+$/i;// number pattern
            var flag=0;
          if(regex.test(username))
          {
            flag=1
          }
          else if(flag==0 && username!='')
          {
            $("#usernamechk").html("Please type Alphanumeric values only.");
            $("#usernamechk").addClass('er-error');
            $("#usernamechk").removeClass('er-success');
          }
                   if(result == 1 && flag == 1) {
                       //the user is available
                       $("#usernamechk").html("User name is available.");
            $("#usernamechk").addClass('er-success');
                    $("#usernamechk").removeClass('er-error');
          }
                   else if(result == 0 && username!=''){
                       //the user is not available
                       $("#usernamechk").html("Username is  not available.");
            $("#usernamechk").addClass('er-error');
            $("#usernamechk").removeClass('er-success');
                   }
               });
          });   
    });
</script>

