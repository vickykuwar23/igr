<div style="text-align:center;">
<?php if ($this->session->flashdata('successmsgsend') != '') { ?>
     <div class="alert alert-success" style="font-size:17px; margin-bottom:450px;">
        <button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
         <?php echo $this->session->flashdata('successmsgsend') ?></br>
           <div><a href="<?php echo base_url();?>callcenterpanel/users" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
    <? } ?>
  </div>
  <div style="text-align:center;">
<?php if ($this->session->flashdata('upload_err') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px; margin-bottom:450px;">
           <?php echo $this->session->flashdata('upload_err') ?></br>
           <div><a href="<?php echo base_url();?>callcenterpanel/users" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
    <? } ?>
  </div>
  <?php echo $file_error;?>
   <?php if($file_error != "") { ?>
<div class="alert alert-error" style="font-size:17px;">
<button data-dismiss="alert" class="close" type="button">×</button>
<strong>Warning!</strong> <?php echo $file_error ?>
</div>
<?php } ?>  
<?php 
if ($this->session->flashdata('successmsgsend') != '') 
{ $display='style="display:none;"';
 }
 else
 {
$display='style="display:block;"';
}?>
	 	<div <?php echo $display;?> id="grivanceadd_id">
        	<section id="content" >
  <section class="main padder">
    <div class="row">
      <div class="col-sm-6 col-xs-offset-3">
        <section class="panel m-t grdbg">
          <header class="panel-heading text-center"><i class="fa fa-edit"></i>Grievance Redressal Form</header>
          
                  <div class="panel panel-default">
                    <div class="panel-body">
                    <p><strong>Userdetais</strong></p>
                    <p> Name: <?=$user_info[0]['user_name']; ?></p>
                    <p> Mobile: <?=$user_info[0]['user_mobile']; ?></p>
                    </div>
                  </div>

          <div class="panel-body">
            <form class="form-horizontal" method="post" enctype="multipart/form-data" >
              <div class="form-group">
                <label class="col-lg-3 control-label col-lg-offset-1">Type <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please select the category"></i>
                <div class="col-lg-4">
                  <select name="account" id="selectcat" class="form-control no-padder w70">
                    <option value="">Select</option>
                   <?php if(count($category) > 0)
				      {
						  foreach($category as $res)
						  { ?>
								<option value="<?php echo $res['id'];?>"><?php echo ucfirst($res['type']);?></option>
			  <?php }
					}?>
                  </select>
                 <div id="error_account" style="color:#F00"></div>
                </div>
              </div>
              <div class="form-group" id="office_type" style="display:none">
                <label class="col-lg-3 control-label col-lg-offset-1">Office Type <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter office type" ></i>
                <div class="col-lg-7">
                  <input type="text" name="fdc_id"  class="form-control" id="fdc_id">
                  <div id="error_fdc_id" style="color:#F00"> </div>
                </div>
              </div>
              <div class="form-group" id="online_type" style="display:none">
                <label class="col-lg-3 control-label col-lg-offset-1">Online Sub-type <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter type"></i>
                <div class="col-lg-7">
                <input type="text" name="company_name" class="form-control" id="company_val_id" value="">
                <div id="comp_id" style="color:#F00"> </div>
                </div>
              </div>
             
              <div class="form-group">
                <label class="col-lg-3 control-label col-lg-offset-1">Message <span style="color:red">*</span></label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your grievance in not more than 500 characters"></i>
                <div class="col-lg-7">
                  <textarea onblur="textCounter(this,this.form.counter,500);" onkeyup="textCounter(this,this.form.counter,500);" id="msgcomplain" name="message" rows="5" class="form-control"  data-rangelength="[10,500]"></textarea>
                  <div class="remain">
                  <input onblur="textCounter(this.form.recipients,this,500);" disabled  onfocus="this.blur();" tabindex="999" maxlength="3" size="3" value="500" name="counter">Remaining characters </div>
                  <div id="msg_error" style="color:#F00"><?php echo form_error('message')?></div>
                </div>
              </div>
			 <!--  <div class="form-group">
                <label class="col-lg-3 control-label col-lg-offset-1">Upload File :</label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Please select file to upload"></i>
                <div class="col-lg-7">
                  <input type="file" name="userfile" id="userfile" onchange="javascript:return validate()"/>
                  <div id="file_error" style="color:#F00"><?php if($file_error!='') {echo $file_error;}?></div>
          	    </div>
              </div>
              <div class="form-group">
                    <label class="col-lg-3 control-label col-lg-offset-1"></label>
               <div class="col-lg-5 uploadmsg">
         		     <strong>Note : Allowed file types [.pdf, .doc, .docx, .txt, .xls, .xlsx] <br />Maximum size allowed : 2MB  </strong>        
          	    </div>
              </div>-->
              
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
             
              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <button type="submit" class="btn btn-white">Cancel</button>
                  <button type="submit" name="submit" class="btn btn-regi no-shadow" id="btn_submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
    <div class="col-md-2 col-md-offset-5 m-b">
    <a href="<?php echo base_url();?>index.php/userprofile/pre/<?php echo $this->uri->segment(3)?>" class="btn btn-comn no-shadow"><i class="fa fa-angle-double-left"></i> Previous Complaints</a>
    </div>
    </div>
  </section>
</section>
</div>
</div>
<script>
function validate() {
	var flag=1;
	$("#file_error").html("");
	  var file_size = $('#userfile')[0].files[0].size;
	  var file_upload=document.getElementById('userfile').value;
      var ext=file_upload.substring(file_upload.lastIndexOf('.')+1);

	if(file_size>2097152) {
		$("#file_error").html("File size should not be greater than 2MB");
		flag=0
	}
	else 
	if(file_upload!="" && (ext!="doc"  && ext!="docx" && ext!="xls" && ext!="pdf"  && ext!="txt" && ext!="PDF" && ext!="DOC" && ext!="DOCX" && ext!="TXT" && ext!="XLS" && ext!="XLSX"))
	{
		$('#file_error').html("Please select valid document.");
		flag=0;
	}
	if(flag==1){ 
	 $('#btn_submit').attr("disabled", false);
	 return true;
	}
	else
	{
		$('#btn_submit').attr("disabled", true);
		 return false;
	}
}

$(document).ready(function(){
	$('#user_close').on('click',function(){
		$("#grivanceadd_id").css("display", "block");
		})
	});
function textCounter( field, countfield, maxlimit ) {
 if ( field.value.length > maxlimit ) {
  field.value = field.value.substring( 0, maxlimit );
  field.blur();
  field.focus();
  return false;
 } else {
  countfield.value = maxlimit - field.value.length;
 }
}
</script>
