<div style="text-align:center;">
    <?php if ($this->session->flashdata('successmsgsend') != '') { ?>
    <div class="alert alert-success" style="font-size:17px; margin-bottom:450px;">
        <button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
        <?php echo $this->session->flashdata('successmsgsend') ?></br>
        <div><a href="<?php echo base_url();?>index.php/userprofile/user/" style="text-decoration:none; text-align:">Go
                Back</a></div>
    </div>
    <?php } ?>
</div>
<div style="text-align:center;">
    <?php if ($this->session->flashdata('upload_err') != '') { ?>
    <div class="alert alert-warning" style="font-size:17px; margin-bottom:450px;">
        <?php echo $this->session->flashdata('upload_err') ?></br>
        <div><a href="<?php echo base_url();?>index.php/userprofile/user/" style="text-decoration:none; text-align:">Go
                Back</a></div>
    </div>
    <?php } ?>
</div>
<?php echo $file_error;?>
<?php if($file_error != "") { ?>
<div class="alert alert-error" style="font-size:17px;">
    <button data-dismiss="alert" class="close" type="button">×</button>
    <strong>Warning!</strong> <?php echo $file_error ?>
</div>
<?php } ?>
<?php 
    if ($this->session->flashdata('successmsgsend') != '') 
    { $display='style="display:none;"';
     }
     else
     {
    $display='style="display:block;"';
    }?>
<div <?php echo $display;?> id="grivanceadd_id">
    <section id="content" class="login-wrapper">
        <div class="d-flex justify-content-center h-100">
            <div class="user_card">
                <div class="d-flex justify-content-center form_container">
                    <form class="w-100 form-horizontal" method="post" enctype="multipart/form-data">
                        <div class="form-heading">
                            <h3>Grievance Redressal Form</h3>
                        </div>
                        <div class="form-grp-filed">
                            <div class="mb-3 row">
                              <div class="col-md-4">
                                 <label class="control-label">Type <span style="color:red">*</span></label>
                              </div>
                              <div class="col-md-8">
                                <div class="">
                                    <select name="account" id="selectcat" class="form-control no-padder w-100">
                                        <option value="">Select</option>
                                        <?php if(count($category) > 0)
                          {
                              foreach($category as $res)
                              { ?>
                                        <option value="<?php echo $res['id'];?>"><?php echo ucfirst($res['type']);?>
                                        </option>
                                        <?php }
                        }?>
                                    </select>
                                    <div id="error_account" style="color:#F00"></div>
                                </div>
                              </div>
                            </div>
                            <div class="mb-3 row" id="office_type" style="display:none">
                               <div class="col-md-4">
                                    <label class="control-label">Office Type <span
                                            style="color:red">*</span></label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="fdc_id" class="form-control" id="fdc_id">
                                    <div id="error_fdc_id" style="color:#F00"> </div>
                                </div>
                            </div>
                            <div class="mb-3 row" id="online_type" style="display:none">
                               <div class="col-md-4">
                                  <label class="control-label">Online Sub-type <span
                                          style="color:red">*</span></label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="company_name" class="form-control" id="company_val_id"
                                        value="">
                                    <div id="comp_id" style="color:#F00"> </div>
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <div class="col-md-4">
                                <label class="control-label">Message <span
                                        style="color:red">*</span></label>
                                      </div>
                                <div class="col-md-8">
                                    <textarea onblur="textCounter(this,this.form.counter,500);"
                                        onkeyup="textCounter(this,this.form.counter,500);" id="msgcomplain"
                                        name="message" rows="5" class="txt-area"
                                        data-rangelength="[10,500]"></textarea>
                                    <div class="remain">
                                        <input onblur="textCounter(this.form.recipients,this,500);" disabled
                                            onfocus="this.blur();" tabindex="999" maxlength="3" size="3" value="500"
                                            name="counter">Remaining characters </div>
                                    <div id="msg_error" style="color:#F00"><?php echo form_error('message')?></div>
                                </div>
                            </div>
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>"
                                value="<?php echo $this->security->get_csrf_hash();?>">
                            <div class="mb-3">
                                <div class="col-lg-12 mx-auto d-flex justify-content-center">
                                  <ul class="frm-btn-grp">
                                     <li> <button type="submit" name="submit" class="btn_submit  btn" id="btn_submit">Submit</button></li>
                                    <li> <button type="submit" class="btn_cancel btn btn-white">Cancel</button></li>
                                   </ul>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center links frgt-pswd">
                        <a href="<?php echo base_url();?>index.php/userprofile/pre/<?php echo $this->uri->segment(3)?>"
                    ><i class="fa fa-angle-double-left"></i> Previous Complaints</a>
          </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
          
        </div>
    </section>
</div>
</div>
<script>
    function validate() {
        var flag = 1;
        $("#file_error").html("");
        var file_size = $('#userfile')[0].files[0].size;
        var file_upload = document.getElementById('userfile').value;
        var ext = file_upload.substring(file_upload.lastIndexOf('.') + 1);

        if (file_size > 2097152) {
            $("#file_error").html("File size should not be greater than 2MB");
            flag = 0
        }
        else
            if (file_upload != "" && (ext != "doc" && ext != "docx" && ext != "xls" && ext != "pdf" && ext != "txt" && ext != "PDF" && ext != "DOC" && ext != "DOCX" && ext != "TXT" && ext != "XLS" && ext != "XLSX")) {
                $('#file_error').html("Please select valid document.");
                flag = 0;
            }
        if (flag == 1) {
            $('#btn_submit').attr("disabled", false);
            return true;
        }
        else {
            $('#btn_submit').attr("disabled", true);
            return false;
        }
    }

    $(document).ready(function () {
        $('#user_close').on('click', function () {
            $("#grivanceadd_id").css("display", "block");
        })
    });
    function textCounter(field, countfield, maxlimit) {
        if (field.value.length > maxlimit) {
            field.value = field.value.substring(0, maxlimit);
            field.blur();
            field.focus();
            return false;
        } else {
            countfield.value = maxlimit - field.value.length;
        }
    }
</script>