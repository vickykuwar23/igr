<style>
        .form-group h4 {
            font-size: 16px;
            color: #0D4F89;
            font-weight: 500;
        }

        .profile-wrapper {
            padding: 40px 0 0;
        }

        .profile_userView {
            background: #fff;
            position: relative;
            padding: 10px 15px 1px;
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.1), 0 0px 5px 0 rgba(0, 0, 0, 0.1);
            border-radius: 10px;
            margin-bottom: 30px;
        }
        .fedback-frm-title{
            font-size: 20px;
            color:#0D4F89;
            margin-bottom: 20px;
            
        }


        /* rating css */

        /****** Style Star Rating Widget *****/

        .rating {
            border: none;
            float: left;
        }

        .rating>input {
            display: none;
        }

        .rating>label:before {
            margin: 0 5px;
            font-size: 1.25em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }

        .rating>.half:before {
            content: "\f089";
            position: absolute;
        }

        .rating>label {
            color: #ddd;
            float: right;
            margin-bottom: 0px;
        }

        /***** CSS Magic to Highlight Stars on Hover *****/

        .rating>input:checked~label,
        /* show gold star when clicked */
        .rating:not(:checked)>label:hover,
        /* hover current star */
        .rating:not(:checked)>label:hover~label {
            color: #FFD700;
        }

        /* hover previous stars in list */

        .rating>input:checked+label:hover,
        /* hover current star when changing rating */
        .rating>input:checked~label:hover,
        .rating>label:hover~input:checked~label,
        /* lighten current selection */
        .rating>input:checked~label:hover~label {
            color: #FFED85;
        }
        .input-w-80{
            width: 90%;
            float: left;
        }
        .input-w-20{
            width: 10%;
            float: right;
            text-align: right;
        }
        #feedbck-frm span#captImg > img{
            height: 38px !important;

        }
        @media only screen and (max-width: 767px) {
            .input-w-80{
                width: 100%;
                float: left;
            }
            .input-w-20{
                width: 100%;
                float: left;
                text-align: left;
            }
        }
    </style>
        <div class="profile-wrapper">
        <div class="container bootstrap snippet">
            <div class="row">
                <div class="col-sm-8 mx-auto">
                     <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
                    <h3 class="fedback-frm-title">Send us your Feedback :</h3>
                    <div class="tab-content profile_userView">
                        <div class="tab-pane active" id="home">
                            <form class="form" method="post" id="feedbck-frm">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label for="first_name">
                                            <h4>Grievance ID</h4>
                                        </label>
                                        <input type="text" name="comp_code" class="form-control" value="<?php echo $this->input->post('comp_code'); ?>">
                                        <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('comp_code');?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label for="phone">
                                            <h4>Rating Stars</h4>
                                        </label>
                                    </div>
                                    <div class="col-md-12">
                                         <div>   
                                        <fieldset class="rating">
                                            <input type="radio" id="star5" name="rating" value="5" /><label class="full"
                                                for="star5" title="5 stars"></label>
                                            
                                            <input type="radio" id="star4" name="rating" value="4" /><label class="full"
                                                for="star4" title="4 stars"></label>
                                            
                                            <input type="radio" id="star3" name="rating" value="3" /><label class="full"
                                                for="star3" title="3 stars"></label>
                                            
                                            <input type="radio" id="star2" name="rating" value="2" /><label class="full"
                                                for="star2" title="2 stars"></label>
                                            
                                            <input type="radio" id="star1" name="rating" value="1" /><label class="full"
                                                for="star1" title="1 star"></label>
                                           
                                        </fieldset>
                                        </div>
                                        <div class="clearfix"> </div>
                                        <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('rating');?></div>
                                       
                                    </div>
                                     
                                </div>
                                 <div class="clearfix"> </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label for="email">
                                            <h4>Your Message</h4>
                                        </label>
                                        <textarea class="form-control" name="message" rows="5" cols="1"><?php echo $this->input->post('message'); ?></textarea>
                                        <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('message');?></div>
                                    </div>
                                </div>
                                  <div class="form-group row captcha mb-0">
                                    <label class="col-lg-3 control-label text-left cptcha_img">
                                      <span id="captImg"><?php echo $image; ?></span>
                                        
                                      </label>
                                    <div class="col-lg-9">
                                        <div class="input-w-80"> 
                                            <input type="text" name="code" placeholder="" class="form-control"  id="code"> 
                                        <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('code');?></div>
                                       <p class="text-left blue">Please enter the text you see in the image below into the text box provided.</p>
                                        </div>
                                        <div class="input-w-20">
                                          <a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh"></i></a> 
                                        </div>
                                    </div>
                                    
                                  </div>
                                <div class="form-group">
                                    <div class="col-sm12">
                                        <br>
                                        <ul class="frm-btn-grp text-center">
                                            <li><button type="submit" name="btn_reg" class="btn btn_submit"
                                                    id="btn_submit">Submit</button></li>
                                            <li><a class="btn_cancel btn btn-white" id="cancel" href="javascript:void(0)" onClick="window.location.reload();">Cancel</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clearfix mb-3"> </div>
                        </div>
                        </form>
                    </div>
                </div>
                <!--/tab-pane-->
            </div>
            <!--/tab-content-->
        </div>
        <!--/col-9-->
    </div>
    </div>
<script>
         $(document).ready(function(){
             $('.refreshCaptcha').on('click', function(){
                 $.get('<?php echo base_url().'home/refresh'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
         });
</script>