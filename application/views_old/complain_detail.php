<div style="text-align:center;">
 <?php if($error_msg != "") { ?>
<div class="alert alert-error" style="font-size:17px;">
<button data-dismiss="alert" class="close" type="button">×</button>
<strong>Warning!</strong> <?php echo $error_msg ?>
</div>
<?php } ?>  
<?php if ($this->session->flashdata('success_message') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success_message') ?>
      </div>
    <? } ?>
    <?php if ($this->session->flashdata('invalidcredential') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('invalidcredential') ?>
      </div>
    <? } ?></div><section id="content" class="min380">

  <div class="main padder">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 m-t-large">
        <section class="panel grdbg" id="content">
           <header class="panel-heading bypassme"><center> Complaint Details</center></header> 
           <div class="hidethis"><center style="font-size:24px; color:#06C; margin-left:250px;">Complaint Details -  <?php echo $complaint[0]['pattern'];?></center></div>
           
    	 <?php 
		 $userinfo=$this->master_model->getRecords('userregistration',array('user_id'=>$complaint[0]['user_id']));
		 ?>
         <div class="col-md-6 m-t-large">
         <div class="line line-dashed"></div>
            <h3> <strong >User Details :</strong></h3>
         	<div class="line line-dashed"></div>
            <div class="block m-b">
            <label class="control-label">Name :</label>
             <?php echo ucfirst($userinfo[0]['user_name']);?>
            </div>
            <div class="block m-b">
              <label class="control-label">Email ID :</label>
              <?php echo $userinfo[0]['user_email'];?>
            </div>
             <div class="block m-b">
              <label class="control-label">Contact No :</label>
              <?php echo $userinfo[0]['user_landline'];?>
            </div>
            <div class="block m-b">
              <label class="control-label">Mobile No :</label>
              <?php echo $userinfo[0]['user_mobile'];?>
            </div>
            <div class="line line-dashed"></div>
            <h3> <strong>Complaint / Grievance Details :</strong></h3>
            <br>
         	<div class="line line-dashed"></div>
           
            <div class="block m-b">
              <label class="control-label">Complaint Type :</label>
              <?
			    $category=$this->master_model->getRecords('departments',array('id'=>$complaint[0]['category']));
				  echo ucfirst($category[0]['type']);?>
            </div>
            
            <?php if($complaint[0]['company']!='')
			 {?>
                    <div class="block m-b">
                    <label class="control-label" > Sub type :</label>
                    <?php if($complaint[0]['company']!=''){echo ucfirst($complaint[0]['company']);}?>
                    </div>
            <?php }?>
            
             <?php if($complaint[0]['fdc_id']!='')
			 {?>
                    <div class="block m-b">
                    <label class="control-label">Office type :</label>
                    <?php if($complaint[0]['fdc_id']!=''){echo ucfirst($complaint[0]['fdc_id']);}?>
                    </div>
            <?php }?>
            
               <?php if($complaint[0]['empcode']!='')
			 {?>
                    <div class="block m-b">
                    <label class="control-label">Employee Code :</label>
                    <?php if($complaint[0]['empcode']!=''){echo ucfirst($complaint[0]['empcode']);}?>
                    </div>
            <?php }?>
           
             <div class="block m-b">
              <label class="control-label">Grievance Referenc ID : </label>
              <?php echo $complaint[0]['pattern'];?>
            </div>
            
             <div class="block m-b">
              <label class="control-label">Complaint Date :</label>
             <?php echo date('d M Y',strtotime($complaint[0]['registerdate']));?>	
            </div>
            
             <div class="block m-b">
              <label class="control-label">Complaint Details :</label>
              <?php echo $complaint[0]['msg_content'];?>
            </div>
            
            <?php 
			if($complaint[0]['uploadpath']!='')
			{?>
           		 <div class="block m-b">
                  <label class="control-label">Attachment :</label>
                  <a href="<?php echo base_url();?>uploads/<?php echo $complaint[0]['uploadpath'];?>" target="_new" style="text-decoration:none;">View</a>
                  </div>
            <?php 
			}?>
           <div class="clear"></div>
            
            </div><br>
            <div class="col-md-6 m-t-large" style="border-left:1px dashed #6faee8; min-height:450px;">
           <h3><strong> REPLY SECTION : </strong></h3> 
           <div style="text-align:center;" id="reply_id">
<?php if ($this->session->flashdata('success_reply') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success_reply') ?>
      </div>
    <? } ?>
  </div>
            <div class="clear"></div>
            <div class="line line-dashed"></div> 
            <?php 
			/*$this->db->join('adminlogin','adminlogin.id=complainreply.user_id');
			$this->db->where('user_id',$this->session->userdata('adminid'));
			$this->db->or_where('adminlogin.type','7');*/
			$replymsg=$this->master_model->getRecords('complainreply',array('complain_id'=>$this->uri->segment(3)),'',array('reply_id'=>'ASC'));
     		if(count($replymsg) > 0)
			{
			foreach($replymsg as $rlpy)
			{
			?>
              <form class="panel-body" method="post" action="<?php echo base_url();?>index.php/adminpanel/complaindetail/<?php echo $this->uri->segment(3);?>">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
              <input type="hidden" name="reply_id" value="<?php echo $rlpy['reply_id'];?>"> 
            <div style="float:right;">
              <!--  <a title="Edit" href="javascript:void(0)" onclick="return showhide_repliedmsg('replies_id',<?php echo $rlpy['reply_id'];?>,'replied_class','replied_static_msg');"><i class="fa fa-pencil"></i></a>
                <a href="<?php echo base_url();?>index.php/adminpanel/delete/<?php echo $this->uri->segment(3);?>/<?php echo $rlpy['reply_id'];?>" title="Delete" 
                                    onclick="javascript:return deletconfirm();"><i class="ui-tooltip fa fa-trash-o"  data-original-title="Delete"></i></a>-->
              </div>
               <div class="clear"></div>
              <div id="replies_id_<?php echo $rlpy['reply_id'];?>" class="replied_static_msg">
                <div><strong>Replied Details :</strong><?php echo $rlpy['reply_msg'];?></div>
                <strong>Replied On</strong> : <?php echo date('d M, Y',strtotime($rlpy['reply_date']));?>
                 <div class="clear"></div>
              
                                    
                <div class="line line-dashed"></div>
                </div>
			  <div id="replies_id_view_<?php echo $rlpy['reply_id'];?>" style="display:none;" class="replied_class">
              <div class="block m-b">
           <label class="control-label" style="vertical-align:top; margin-top:20px;"><strong>Reply to Complaint :</strong></label>
          	<textarea name="update_reply_msg" id="reply_msg" rows="6" data-rangelength="[10,200]" data-trigger="keyup" style="width: 490px; height: 178px;"><?php echo $rlpy['reply_msg'];?></textarea>
              <div id="reply_msg_account" style="color:#F00"><?php echo form_error('reply_msg');?></div>
             </div>
              <div class="clear"></div>
            
          	  <div class="col-lg-8 col-lg-offset-0">
                  <button id="btn_submit" class="btn btn-regi no-shadow" name="btn_update" type="submit">Update</button>
                  <a href="javascript:void(0)" class="btn btn-white bypassme" onclick="return showhide_cancel_repliedmsg('replies_id',<?php echo $rlpy['reply_id'];?>,'replied_class','replied_static_msg');">Cancel</a>
                </div>
             <div class="clear"></div>
           <div class="line line-dashed"></div> 
            </div>
       	   </form>
			<?php }
			}
			?>
            
            <div class="clear"></div>
            <div style="text-align:center;" id="reply_status_id">
<?php if ($this->session->flashdata('success_reply_status') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success_reply_status') ?>
      </div>
    <? } ?>
  </div>
   		   <form method="post" class="m-b">   
         <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">      
	<?php
	
	if(count($replymsg)<=0)
	{
		$update_arr=array('reply_status'=>'1',
				 						 'closed_date'=>'0000-00-00',
										 'status'=>'0');
	    $this->master_model->updateRecord('usercomplainbox',$update_arr,array('msgid'=>$this->uri->segment(3)));
		$disabled='';				
	}
	else if($complaint[0]['reply_status']==0)
	{
		$disabled='disabled';
	}
	?>
    <strong class="bypassme">Reply Status : </strong>  
    <select class="bypassme"  name="reply_type_status" id="reply_type_status" style="width:40%; background-color:#dbf4ff;" <?php if($complaint[0]['reply_status']=='0'){echo $disabled;}?>>
    
    
    <option value="1" <?php if($complaint[0]['reply_status']=='1'){echo "selected='selected'";}?>>Open</option>
     <?php if(count($replymsg)>0)
	 {?>
      <option value="0" <?php if($complaint[0]['reply_status']=='0'){echo "selected='selected'";}?>>Closed</option>
    <?php }?>
        </select>
		<!--following div is for pdf only-->       
        <div class="block m-b hidethis"  >
        <label class="control-label" ><strong>Reply Status</strong> :</label>
        <?php if($complaint[0]['reply_status']=='1'){echo 'Open'; } else {echo 'close' ; } ?>
        </div>
        
        
     <?php  
	 if($complaint[0]['reply_status']==1)
	{
		$disabled_sts='disabled';
	}
	else
	{
		$disabled_sts='';
	}
	?>
       <?php 
	   if($complaint[0]['reply_status']!='0'){?>
        
       <?php if(count($replymsg)>0)
	 {?>
		    <input type="submit" value="Submit" name="status_btn"   disabled="<?php echo $disabled_sts;?>" id="status_btn" class="bypassme">
		<?php }
		else
		{?>
			<a class="btn btn-info no-shadow bypassme" >Submit</a>
		<?php }?>
    
       <?php }
	   else if($complaint[0]['reply_status']==1)
	   {?>
		 <input type="submit" value="Submit" name="status_btn" disabled="<?php echo $disabled_sts;?>" id="status_btn"  >
		<?php  }?>
        <div class="clear"></div>
      </form>
     
      <?php if($complaint[0]['reply_status']=='1' || count($replymsg)<=0)
	  {?>
        	 <form class="panel-body" method="post" action="<?php echo base_url();?>index.php/adminpanel/complaindetail/<?php echo $this->uri->segment(3);?>">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
            <div class="block m-b">
           <label class="control-label" style="vertical-align:top; margin-top:20px;">Reply to Complaint :</label>
             
          	<textarea name="reply_msg" id="reply_msg" rows="6" data-rangelength="[10,200]" data-trigger="keyup" style="width: 80%; height: 178px;"></textarea>
              <div id="reply_msg_account" style="color:#F00"><?php echo form_error('reply_msg');?></div>
            </div>
             <div class="clear"></div>
            <button name="reply" class="btn btn-comn  m-r-large m-b no-shadow bypassme" type="submit" id="reply" >Reply</button>
          </form>
       <?php }?></div>
       <div class="clr"></div>
          </section> 
         <center style=" margin:0 auto 20px;">
          <div style="display:inline;   "><a class="btn btn-success no-shadow bypassme" href="<?php echo base_url();?>index.php/adminpanel/"  ><i class="fa fa-angle-double-left"></i>Go back</a></div>
     
          <button style="display:inline; "  class="btn btn-info no-shadow  bypassme" id="cmd"><i class="fa fa-pdf"></i>generate PDF</button>
          </center>
          </div>
        
      </div>
    </div>
  </div>
</section>
 
<div id="editor"></div>


<script src="https://ifciltd.com/grievance/js/jspdf.min.js"></script>

<script>
 $('.hidethis').css('display','none');

$(function () {

    var specialElementHandlers = {
        '#editor': function (element,renderer) {
            return true;
        }
    };
 $('#cmd').click(function () {
        var doc =  new jsPDF();
		
		specialElementHandlers = {
                // element with id of "bypass" - jQuery style selector
                '.bypassme': function(element, renderer) {
                    // true = "handled elsewhere, bypass text extraction"
                    return true
                }
            };
			
			
	    doc.fromHTML(
            $('#content').html(), 15, 15, 
            { 'width': 170, 
			'elementHandlers': specialElementHandlers }, 
            function(){ doc.save('GRIEVANC_COMPLAINT_DETAILS.pdf'); }
        );
		
    });  
});

</script>
