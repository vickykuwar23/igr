<div style="text-align:center;">
 <?php if ($this->session->flashdata('invalidadminlogin') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('invalidadminlogin') ?>
      </div>
    <? }
	
	if ($this->session->flashdata('success_msg') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success_msg') ?>
      </div>
    <? }
	
	 ?>
 </div>

 <section id="content" class="min380">
  <div class="main padder">
    <div class="row">
      <div class="col-lg-4 col-lg-offset-4 m-t-large">
        
      
        <section class="panel grdbg m-t-large">
         
          <header class="panel-heading text-left">Admin - Change Password </header>
          <form class="panel-body" method="post" id="change_admin_pw">
            <div class="block m-b">
             <!-- <label class="control-label loginlable">Email</label>
              <input type="email" placeholder="test@example.com" class="form-control" name="username">-->
               <label class="control-label loginlable">Old Password </label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your username here"></i>
              <input type="password" class="form-control" name="old_pw" id="old_pw" required>
             <div class="m-t m-t-mini" style="color:#F00">  <?php echo form_error('old_pw');?></div>
            </div>
            <div class="block m-b">
              <label class="control-label loginlable">New Password</label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your password here" ></i>
              <input name="new_pw" id="new_pw" type="password"  class="form-control" required>
              <div class="m-t m-t-mini" style="color:#F00">  <?php echo form_error('new_pw');?></div>
            </div>
            
            
            <div class="block m-b">
              <label class="control-label loginlable">Confirm Password</label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your password here" ></i>
              <input name="c_pw" id="c_pw" type="password"   class="form-control" required>
              <div class="m-t m-t-mini" style="color:#F00">  <?php echo form_error('c_pw');?></div>
            </div>
            
            
              <p class="text-center blue">Please enter the text you see in the image below into the text box provided.</p>
              <div class="form-group">
                <label class="loginlable control-label"><?php echo $image; ?></label>
                <div class="col-lg-7">
                  <input type="text" name="code" placeholder="" class="form-control"  id="code"> 
                    <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('code');?></div>
                </div>
              </div>
             <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
            <button type="submit" id="submit" class="btn pull-right m-r-large m-b no-shadow greybg" name="submit">Sign in</button>
          </form>
        </section>
      </div>
    </div>
  </div>
</section>
  <script>
  
  $("submit").click(function(){
    		 if($('#old_pw').val() != "" && $("#new_pw").val() != "" && $("#c_pw").val() != "")
		  {
					var p1=$("#old_pw").val();
					var p2 = $("#new_pw").val();
					var p3 = $("#c_pw").val();
                 
					$("#old_pw").val(sha1(p1));
					$("#new_pw").val(sha1(p2));
					$("#c_pw").val(sha1(p3));
					return true;
            }
	}); 


 /* $(document).ready(function(){
      $('form').submit( function(event) {
     	var formId = this.id,
        form = this;
    	event.preventDefault();
          if($('#old_pw').val() != "" && $("#new_pw").val() != "" && $("#c_pw").val() != "")
		  {
					var p1=$("#old_pw").val();
					var p2 = $("#new_pw").val();
					var p3 = $("#c_pw").val();
                 
					$("#old_pw").val(sha1(p1));
					$("#new_pw").val(sha1(p2));
					$("#c_pw").val(sha1(p3));
            }
			setTimeout( function () { 
			$('#change_admin_pw').submit();
		}, 1000); 
}); 

  })*/
 </script>