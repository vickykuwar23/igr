    <section class="sitemap-wrapper" id="complain-wrapper">
        <div class="container">
            <div class="row mb-3">
                <div class="col-md-12">
                    <div class="title-heading">
                        <h1 class="heading">Site Map</h1>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <!-- links -->
                <div class="col-md-3 mb-3">
                    <div class="sitemap-links">
                        <strong class="mb-2 d-block">Organisation</strong>
                        <ul class="points">
                            <li><a href="<?php echo base_url('organisation/vision'); ?>" target="_blank">Vision</a> </li>
                            <li><a href="<?php echo base_url('organisation/history'); ?>" target="_blank">History</a> </li>
                            <li><a href="<?php echo base_url('organisation/structure'); ?>" target="_blank">Structure</a> </li>
                            <li><a href="<?php echo base_url('organisation/office'); ?>" target="_blank">Offices</a> </li>
                            <li><a href="<?php echo base_url('organisation/functions'); ?>" target="_blank">Functions</a> </li>
                            <li><a href="<?php echo base_url('organisation/orders'); ?>" target="_blank">Orders</a> </li>
                        </ul>
                    </div>
                </div>
                <!--links-->
                <!-- links -->
                <div class="col-md-3 mb-3">
                    <div class="sitemap-links">
                        <strong class="mb-2 d-block">Publications</strong>
                        <ul class="points">
                            <li><a href="<?php echo base_url('publication/acts'); ?>" target="_blank">Acts</a></li>
                            <li><a href="<?php echo base_url('publication/schedules'); ?>" target="_blank">Schedules</a></li>
                            <li><a href="<?php echo base_url('publication/rules'); ?>" target="_blank">Rules</a></li>
                            <li><a href="<?php echo base_url('publication/notifications'); ?>" target="_blank">Notifications</a></li>
                            <li><a href="<?php echo base_url('publication/grs'); ?>" target="_blank">GRs</a></li>
                            <li><a href="<?php echo base_url('publication/circulars'); ?>" target="_blank">Circulars</a></li>
                            <li><a href="<?php echo base_url('publication/asr'); ?>" target="_blank">ASR Guidelines</a></li>
                            <li><a href="<?php echo base_url('publication/citizen'); ?>" target="_blank">Citizen's Charter</a></li>
                            <li><a href="<?php echo base_url('publication/sarathi'); ?>" target="_blank">Sarathi</a></li>
                        </ul>
                    </div>
                </div>
                <!--links-->
                <!-- links -->
                <div class="col-md-3 mb-3">
                    <div class="sitemap-links">
                        <strong class="mb-2 d-block">Important Links</strong>
                        <ul class="points">
                            <li><a href="https://www.india.gov.in/" target="_blank">Government of India</a></li>
                            <li><a href="https://www.maharashtra.gov.in/1125/Home" target="_blank">Government of Maharashtra</a></li>
                            <li><a href="https://gras.mahakosh.gov.in/echallan/" target="_blank">GRAS</a></li>
                            <li><a href="http://mahabhulekh.maharashtra.gov.in/" target="_blank">Land Records</a></li>
                            <li><a href="https://it.maharashtra.gov.in/1035/Home" target="_blank">Department of IT</a></li>
                            <li><a href="http://igrmaharashtra.gov.in/frmUnderConstruction.aspx" target="_blank">Co-operation Department</a></li>
                        </ul>
                    </div>
                </div>
                <!--links-->  
                

            </div>
        </div>
    </section>

