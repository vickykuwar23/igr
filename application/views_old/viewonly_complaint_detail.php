<div style="text-align:center;">
 <?php if($error_msg != "") { ?>
<div class="alert alert-error" style="font-size:17px;">
<button data-dismiss="alert" class="close" type="button">×</button>
<strong>Warning!</strong> <?php echo $error_msg ?>
</div>
<?php } ?>  
<?php if ($this->session->flashdata('success_message') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success_message') ?>
      </div>
    <? } ?>
    <?php if ($this->session->flashdata('invalidcredential') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('invalidcredential') ?>
      </div>
    <? } ?></div>
<section id="content" class="min380">
  <div class="main padder">
    <div class="row">
      <div class="col-lg-6 col-lg-offset-4 m-t-large">
        <section class="panel grdbg">
          <header class="panel-heading text-center">Complaint Details</header>
    	 <?php 
		 $userinfo=$this->master_model->getRecords('userregistration',array('user_id'=>$complaint[0]['user_id']));
		 ?>
            <div class="line line-dashed"></div>
            <div class="block m-b">
            <label class="control-label">Name :</label>
             <?php echo ucfirst($userinfo[0]['user_name']);?>
            </div>
            <div class="block m-b">
              <label class="control-label">Email ID :</label>
              <?php echo $userinfo[0]['user_email'];?>
            </div>
            <div class="block m-b">
              <label class="control-label">Contact No :</label>
              <?php echo $userinfo[0]['user_landline'];?>
            </div>
            <div class="block m-b">
              <label class="control-label">Mobile No :</label>
              <?php echo $userinfo[0]['user_mobile'];?>
            </div>
            <div class="line line-dashed"></div>
            <strong>Complaint / Grievance Details :</strong>
         	<div class="line line-dashed"></div>
           
            <div class="block m-b">
              <label class="control-label">Complaint Type :</label>
              <?
			    $category=$this->master_model->getRecords('departments',array('id'=>$complaint[0]['category']));
				  echo ucfirst($category[0]['type']);?>
            </div>
            
            <?php if($complaint[0]['company']!='')
			 {?>
                    <div class="block m-b">
                    <label class="control-label">Company Name :</label>
                    <?php if($complaint[0]['company']!=''){echo ucfirst($complaint[0]['company']);}?>
                    </div>
            <?php }?>
            
             <?php if($complaint[0]['fdc_id']!='')
			 {?>
                    <div class="block m-b">
                    <label class="control-label">Folio No. / DP ID / Client ID :</label>
                    <?php if($complaint[0]['fdc_id']!=''){echo ucfirst($complaint[0]['fdc_id']);}?>
                    </div>
            <?php }?>
           
             <div class="block m-b">
              <label class="control-label">Grievance Referenc ID : </label>
              <?php echo $complaint[0]['pattern'];?>
            </div>
            
             <div class="block m-b">
              <label class="control-label">Complaint Date :</label>
             <?php echo date('d M Y',strtotime($complaint[0]['registerdate']));?>	
            </div>
            
             <div class="block m-b">
              <label class="control-label">Complaint Details :</label>
              <?php echo $complaint[0]['msg_content'];?>
            </div>
             <div class="clear"></div>
             <div class="line line-dashed"></div> 
             <strong> REPLY SECTION</strong>
             <div style="text-align:center;" id="reply_id">
<?php if ($this->session->flashdata('success_reply') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success_reply') ?>
      </div>
    <? } ?>
  </div>
             <div class="clear"></div>
             <div class="line line-dashed"></div> 
            <?php $replymsg=$this->master_model->getRecords('complainreply',array('complain_id'=>$this->uri->segment(3)),'',array('reply_id'=>'ASC'));
      
			if(count($replymsg) > 0)
			{
			foreach($replymsg as $rlpy)
			{
			?>
              <form class="panel-body" method="post" action="<?php echo base_url();?>index.php/adminpanel/complaindetail/<?php echo $this->uri->segment(3);?>">
              <input type="hidden" name="reply_id" value="<?php echo $rlpy['reply_id'];?>"> 
            <!--<div style="float:right;">
                <a title="Edit" href="javascript:void(0)" onclick="return showhide_repliedmsg('replies_id',<?php echo $rlpy['reply_id'];?>,'replied_class','replied_static_msg');"><i class="fa fa-pencil"></i></a>
                <a href="<?php echo base_url();?>index.php/adminpanel/delete/<?php echo $this->uri->segment(3);?>/<?php echo $rlpy['reply_id'];?>" title="Delete" 
                                    onclick="javascript:return deletconfirm();"><i class="ui-tooltip fa fa-trash-o"  data-original-title="Delete"></i></a>
              </div>-->
               <div class="clear"></div>
              <div id="replies_id_<?php echo $rlpy['reply_id'];?>" class="replied_static_msg">
                <div><strong>Replied Details :</strong><?php echo $rlpy['reply_msg'];?></div>
                <strong>Replied On</strong> : <?php echo date('d M, Y',strtotime($rlpy['reply_date']));?>
                 <div class="clear"></div>
                <div class="line line-dashed"></div>
                </div>
			  <div id="replies_id_view_<?php echo $rlpy['reply_id'];?>" style="display:none;" class="replied_class">
              <!--<div class="block m-b">
           <label class="control-label" style="vertical-align:top; margin-top:20px;">Reply to Complaint :</label>
          	<textarea name="update_reply_msg" id="reply_msg" rows="6" data-rangelength="[10,200]" data-trigger="keyup" style="width: 602px; height: 178px;"><?php echo $rlpy['reply_msg'];?></textarea>
              <div id="reply_msg_account" style="color:#F00"><?php echo form_error('reply_msg');?></div>
             </div>-->
              <div class="clear"></div>
            
          	  <!--<div class="col-lg-8 col-lg-offset-0">
                  <button id="btn_submit" class="btn btn-regi no-shadow" name="btn_update" type="submit">Update</button>
                  <a href="javascript:void(0)" class="btn btn-white" onclick="return showhide_cancel_repliedmsg('replies_id',<?php echo $rlpy['reply_id'];?>,'replied_class','replied_static_msg');">Cancel</a>
                </div>-->
             <div class="clear"></div>
           <div class="line line-dashed"></div> 
            </div>
       	   </form>
			<?php }
			}
			?>
            
            <div class="clear"></div>
            <div style="text-align:center;" id="reply_status_id">
<?php if ($this->session->flashdata('success_reply_status') != '') { ?>
     <div class="alert alert-success" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success_reply_status') ?>
      </div>
    <? } ?>
  </div>
   		    <form method="post" class="m-b">         
	<?php
	if(count($replymsg)<=0)
	{
		$update_arr=array('reply_status'=>'1',
				 						 'closed_date'=>'0000-00-00'
										 );
	    $this->master_model->updateRecord('usercomplainbox',$update_arr,array('msgid'=>$this->uri->segment(3)));
		$disabled='';				
	}
	else if($complaint[0]['reply_status']==0)
	{
		$disabled='disabled';
	}?>
    <!--<strong>Reply Status : </strong>  
    <select class="" onchange="javascript:this.form.submit();" name="reply_type_status" id="reply_type_status" style="width:40%; background-color:#dbf4ff;" <?php if($complaint[0]['reply_status']=='0'){echo $disabled;}?>>
    <option value="1" <?php if($complaint[0]['reply_status']=='1'){echo "selected='selected'";}?>>Open</option>
     <?php if(count($replymsg)>0)
	 {?>
      <option value="0" <?php if($complaint[0]['reply_status']=='0'){echo "selected='selected'";}?>>Closed</option>
    <?php }?>
        </select>-->
        <div class="clear"></div>
      </form>
     
          <div style="display:block; margin:0 auto 20px; text-align:center;"><a class="btn btn-success no-shadow" href="<?php echo base_url();?>index.php/viewonly/"><i class="fa fa-angle-double-left"></i>Go back</a></div>
        </section>
      </div>
    </div>
  </div>
</section>
