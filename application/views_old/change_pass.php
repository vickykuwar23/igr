<style>

form ul.helper-text {
  display: block;
  margin-top: 6px;
  font-size: 12px;
  line-height: 22px;
  color: #808080;
}
form ul.helper-text li.valid {
  color: #1fd34a;
}
form.valid input {
  border: 2px solid #1fd34a;
}
.text-red{ color:#F00}
.text-green{ color:#090}
</style>
<div style="text-align:center;">
 <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
    
    <?php if ($this->session->flashdata('wrongemail') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
         <button data-dismiss="alert" class="close" type="button" id="forget_close">×</button>
         <?php echo $this->session->flashdata('wrongemail') ?>
      </div>
    <?php } ?>
    </div>
      <?php if ($this->session->flashdata('forget_galat_jawab') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button" id="newpass_id">×</button>
         <?php echo $this->session->flashdata('forget_galat_jawab') ?>
           </br>
           <div><a href="<?php echo base_url();?>index.php/userlogin/" style="text-decoration:none; text-align:">Go Back</a></div>
      </div>
    <?php } ?></div>
    <?php 
	$display= "";
/*if ($this->session->flashdata('success_message') != '') 
{ $display='style="display:none;"';
 }
 else
 {
$display='style="display:block;"';
}*/?>
<div <?php echo $display;?> id="set_newpass_id">   
 <section id="content" class="login-wrapper">
  <section class="main padder">
    <div class="d-flex justify-content-center h-100">
      <div class="user_card">
      <div class="d-flex justify-content-center form_container">
            <form class="form-horizontal w-100" method="post" data-validate="parsley">
              <div class="form-heading">
                <h3>Change Password</h3>
              </div>
              <div class="form-grp-filed">
             
               <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label align-self-center" >Password<span style="color:red">*</span></label>
                    <div class="col-md-8">
                       <input type="password" required id="password1" name="pass"  class="form-control password" autofocus > 
                        <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('pass');?></div>
                    </div>
                  </div>
              </div>

               <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label align-self-center" >Confirm Password <span style="color:red">*</span></label>
                    <div class="col-md-8">
                        <input type="password" required id="password2" name="con_password" class="form-control"  />
                        <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('con_password');?></div>
                    </div>
                  </div>
              </div>
            

              <div class="form-group">
                
                  <span id="error_pass" style="color:red;font-size: 12px" ></span>
                
              </div>
              <div class="form-group">
                
               <!--  <div class="">
                      <ul class="helper-text passinst">
                        <li class="length">* Must be at least 8 characters long</li>
                        <li class="lowercase">* Must contain a lowercase letter</li>
                        <li class="uppercase">* Must contain an uppercase letter</li>
                        <li class="special">* Must contain a special character</li>
                         <li class="number">* Must contain a number </li>
                     </ul>
                </div> -->
                </div>

                <div class="mb-3">
                                <div class="col-lg-12 mx-auto d-flex justify-content-center">
                                  <ul class="frm-btn-grp">
                                            <li><button type="submit" name="btn_set" class="btn btn_submit" id="submitpass">Update</button></li>
                                            <li><a class="btn_cancel btn btn-white" id="cancel" href="<?php echo base_url('complaint') ?> "><i class="fa fa-angle-double-left"></i>Go back</a></li> 
                                            <!-- <div style="display:block; margin:0 auto 20px; text-align:center;"></div> -->
                                          </ul>
                                  <!-- <ul class="frm-btn-grp">
                                    <li> 
                                      <button type="submit" class="btn btn_submit " name="btn_set" id="submitpass">Submit</button> </li>
                                    <li>
                                      <button type="submit" class="btn_cancel btn btn-white">Cancel</button>  </li>
                                   </ul> -->
                                </div>
                            </div>


              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  
                  
                </div>
              </div>


              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
            </div>
            </form>
          </div>
      </div>
    </div>
</section>
</div>

<script>
  $(document).ready(function(){
    $("#submitpass").on('click',function(){
      // checkPasswordMatch();
      validate= true;
      $("#password1").css('border-color','#495057');
      $("#password2").css('border-color','#495057');
      $("#error_pass").html('')
      if ($("#password1").val() == "") {
        $("#password1").css('border-color','red');
        validate= false;
      }
       if ($("#password2").val() == "") {
        $("#password2").css('border-color','red');
        validate= false;
      }

      var pass = $("#password1").val();
      var confirmPassword = $("#password2").val();

      var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/; 
        if( ! regex.test(pass) ) {
          $("#error_pass").html('Password must be minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.')
            $("#password1").css('border-color','red');
            validate= false;
       }

        if(pass !='' && confirmPassword !=''){ 
        if (pass != confirmPassword)
          {
              $("#password2").css('border-color','red');
              $("#error_pass").html('Passwords does not match!');
              //$('#submitpass').prop("disabled", true);
              validate= false;
          }
        }
if ( validate== false){
  return false;
}
  else{
        if($('.password1').val() != "" && $("#password2").val() != ""){
          var pswd = $("#password1").val();
          var salt = pswd.length;
          var salt1 = sha1(salt);
          var password = sha256(sha1(salt1 + pswd));
          $("#password1").val(password);

          var pswd_2 = $("#password2").val();
          var salt_2 = pswd_2.length;
          var salt2 = sha1(salt_2);
          var password2 = sha256(sha1(salt2 + pswd_2));
          $("#password2").val(password2);


          // var inputhashed = sha1($("#inputPassword").val());
          // var salt = sha1(inputhashed + '<?php echo $this->config->item('salt'); ?>');
          return true;
        }else{
          return false;
        }
        }
    })

  
  })
</script>
