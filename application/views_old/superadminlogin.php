<?php error_reporting(0); ?>

<?php if ($this->session->flashdata('invalidadminlogin') != '') { ?>
<div class="col-md-4 mx-auto text-center mt-3">
     <div class="alert alert-error mb-0" style="font-size:17px;">
        <span> <i class="fa fa-tick pr-2"></i> <?php echo $this->session->flashdata('invalidadminlogin') ?> </span>       
		<button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
      </div>
 </div>
<?php } ?> 
<?php if ($this->session->flashdata('success_message') != '') { ?>
  <div class="col-md-4 mx-auto text-center mt-3">
     <div class="alert alert-success" style="font-size:17px;">
         <button data-dismiss="alert" class="close" type="button" id="forget_close">×</button>
         <?php echo $this->session->flashdata('success_message') ?>
           
      </div>
      </div>
    <?php } ?>
     <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
  <div class="col-md-4 mx-auto text-center mt-3">

               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
 </div>              
    <?php } ?>
 <section id="content" class="login-wrapper">
  <div class="d-flex justify-content-center h-100">
    <div class="user_card">
      <div class="d-flex justify-content-center form_container">
        <form class="w-100" method="post" autocomplete="off">
			<div class="form-heading">
                <h3>Officer's Login </h3>
            </div>
			<div class="form-grp-filed">
            
           <!--  <div class="block m-b row mb-3">
				<div class="col-md-4"> 
					<label class="control-label loginlable">Username</label>
				</div>
				<div class="col-md-8"> 
						<input type="text" class="form-control" name="username" required>
				</div>
            </div> -->

            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                </div>
                                <input id="username" required name="username" autocomplete="off" type="text"
                                    name="" class="form-control input_user" value="" placeholder="username">
    			</div>
            <!-- <div class="block m-b row mb-3">
				<div class="col-md-4"> 
					<label class="control-label loginlable">Password</label>
				</div>
				<div class="col-md-8"> 
					<input name="password" type="password" id="inputPassword" class="form-control" required>
				</div>
                           
            </div> -->
            <div class="input-group mb-3" id="show_hide_password">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-key"></i></span>
                                </div>
                                <input id="inputPassword" required name="password" autocomplete="off" type="password"
                                    name="" class="form-control input_pass" value="" placeholder="password">
                                      <div class="input-group-addon eye_pswd">
        <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
      </div>
                            </div>
			
              
        <div class="form-group row  mb-3">
				  <div class="col-md-4"> 
						<label class="control-label cptcha_img">
							<?php //echo $image; ?>
							<span id="captImg"><?php echo $image; ?></span>
							</label>
					</div>
					<div class="col-md-6">
					<input type="text" name="code"  class="form-control captcha_input input_pass"  id="code" placeholder="captcha code"> 
				
	                    <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('code');?></div>
					</div>
					<div class="col-md-2 pl-0">
						 <a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh"></i></a> 
					</div>
	                  
      </div>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
        <div class="mt-3 mb-3">
				  <button type="submit" class="btn login_btn" name="submit">Sign in</button> 
				</div>
			
          </div>
		  </form>
       
      </div>
      <div class="mt-4">
                    
                    <div class="d-flex justify-content-center links frgt-pswd">
                        <a href="<?php echo base_url();?>superadminlogin/forgetpass/">Forgot your password?</a>
                    </div>
                </div>
    </div>
  </div>
</section>

<script>
         $(document).ready(function(){

            // show hide password
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password input').attr("type") == "text"){
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass( "fa-eye-slash" );
                    $('#show_hide_password i').removeClass( "fa-eye" );
                }else if($('#show_hide_password input').attr("type") == "password"){
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "fa-eye-slash" );
                    $('#show_hide_password i').addClass( "fa-eye" );
                }
            });
            //end show hide password

             $('.refreshCaptcha').on('click', function(){
                 $.get('<?php echo base_url().'superadminlogin/refresh'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
         });
</script>