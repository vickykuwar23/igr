<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                        <ul class="aside-list">							
                            <li><a href="https://www.india.gov.in/" target="_blank"><i class="fa fa-fw"></i>National Portal Of India</a></li>
                            <li><a href="http://goidirectory.gov.in/index.php" target="_blank"><i class="fa fa-fw"></i>Govt. of India</a></li>
                            <li><a href="https://www.maharashtra.gov.in/1125/Home" target="_blank"><i class="fa fa-fw"></i>Govt. of Maharashtra</a></li>
                            <li><a href="https://gras.mahakosh.gov.in/echallan/" target="_blank" ><i class="fa fa-fw"></i>GRAS (e-Payment)</a></li>
                            <li><a href="http://igrmaharashtra.gov.in:8080/frmMap.aspx" target="_blank"><i class="fa fa-fw"></i>Property Valuation</a></li>
                            <li><a href="http://mahabhulekh.maharashtra.gov.in/" target="_blank"><i class="fa fa-fw"></i>Land Records</a></li>
							<li><a href="https://it.maharashtra.gov.in/1035/Home" target="_blank"><i class="fa fa-fw"></i>I.T Department</a></li>
							<li><a href="#" target="_blank"><i class="fa fa-fw"></i>CERSAI</a></li>
							<li><a href="#" target="_blank"><i class="fa fa-fw"></i>MCA</a></li>
							<li><a href="https://www.doingbusiness.org" target="_blank"><i class="fa fa-fw"></i>Ease of Doing Business </a></li>
							<li><a href="https://dipp.gov.in/" target="_blank"><i class="fa fa-fw"></i>DPIIT</a></li>
							<li><a href="http://di.maharashtra.gov.in/_layouts/15/DOIStaticSite/English/index.html" target="_blank"><i class="fa fa-fw"></i>Directorate of Industries – Govt. of Maharashtra</a></li>
							<li><a href="https://eodb.dipp.gov.in/" target="_blank"><i class="fa fa-fw"></i>Business Reforms Action Plan </a></li>
							<li><a href="https://gem.gov.in/" target="_blank"><i class="fa fa-fw"></i>Government eMarketplace </a></li>
						</ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3">
					
                    <ul class="instruction-points">						
                        <li><a href="https://www.india.gov.in/" target="_blank">National Portal Of India</a></li>
                        <li><a href="http://goidirectory.gov.in/index.php" target="_blank">Govt. of India</a></li>
						<li><a href="https://www.maharashtra.gov.in/1125/Home" target="_blank">Govt. of Maharashtra</a></li>
						<li><a href="https://gras.mahakosh.gov.in/echallan/" target="_blank">GRAS (e-Payment)</a></li>
						<li><a href="http://igrmaharashtra.gov.in:8080/frmMap.aspx" target="_blank">Property Valuation</a></li>
						<li><a href="http://mahabhulekh.maharashtra.gov.in/" target="_blank">Land Records</a></li>
						<li><a href="https://it.maharashtra.gov.in/1035/Home" target="_blank">I.T Department</a></li>
						<li><a href="#" target="_blank">CERSAI</a></li>
						<li><a href="#" target="_blank">MCA</a></li>
						<li><a href="https://www.doingbusiness.org" target="_blank">Ease of Doing Business</a></li>
						<li><a href="https://dipp.gov.in/" target="_blank">DPIIT</a></li>
						<li><a href="http://di.maharashtra.gov.in/_layouts/15/DOIStaticSite/English/index.html" target="_blank">Directorate of Industries – Govt. of Maharashtra</a></li>
						<li><a href="https://eodb.dipp.gov.in/" target="_blank">Business Reforms Action Plan </a></li>
						<li><a href="https://gem.gov.in/" target="_blank">Government eMarketplace </a></li>
					</ul>
                </div>
            </div>
        </div>
    </section>





