<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                       <ul class="aside-list">							
                            <li><a href="<?php echo base_url('organisation/vision'); ?>" <?php if($subtitle == "Vision"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Vision</a></li>
                            <li><a href="<?php echo base_url('organisation/history'); ?>" <?php if($subtitle == "History"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>History</a></li>
                            <li><a href="<?php echo base_url('organisation/structure'); ?>" <?php if($subtitle == "Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Structure</a></li>
                            <li><a href="<?php echo base_url('organisation/office'); ?>" <?php if($subtitle == "Offices"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Offices</a></li>
                            <li><a href="<?php echo base_url('organisation/functions'); ?>" <?php if($subtitle == "Functions"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Functions</a></li>
                            <li><a href="<?php echo base_url('organisation/orders'); ?>" <?php if($subtitle == "Orders"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Orders</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3">
                    <div class="col-md-9 mt-3">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="title-heading">
                                <h1 class="heading">Functions</h1>
                            </div>
                        </div>
                    </div>
                    <div class="inner-content">
                        
							<ul class="points">
								<li><a href="<?php echo base_url(); ?>pdf/SR_OFF.pdf" target="_blank">Sub Registrar Office</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/JDR_OFF.pdf" target="_blank">Joint District Registrar Office</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/COS_OFF.pdf" target="_blank">Collector of Stamp Office</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/MAR_OFF.pdf" target="_blank">Marriage Offices</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/DIG_OFF.pdf" target="_blank">Deputy IGR Office</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/ACS_OFF.pdf" target="_blank">Additional Controller of Stamps (ACS) Office</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/ADTP_OFF.pdf" target="_blank"> Assistant Director of Town Planning (ADTP) Office</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/JDTP_OFF.pdf" target="_blank">Joint Director of Town Planning (JDTP) Office</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/GPR_OFF.pdf" target="_blank">Government Photo Registry</a></li>
								<li><a href="<?php echo base_url(); ?>pdf/IGR_OFF.pdf" target="_blank"> Inspector General of Registration (IGR) Office</a></li>
							</ul>
						</p>					
						
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>

