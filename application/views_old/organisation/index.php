<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                       <ul class="aside-list">							
                            <li><a href="<?php echo base_url('organisation/vision'); ?>" <?php if($subtitle == "Vision"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Vision</a></li>
                            <li><a href="<?php echo base_url('organisation/history'); ?>" <?php if($subtitle == "History"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>History</a></li>
                            <li><a href="<?php echo base_url('organisation/structure'); ?>" <?php if($subtitle == "Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Structure</a></li>
                            <li><a href="<?php echo base_url('organisation/office'); ?>" <?php if($subtitle == "Offices"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Offices</a></li>
                            <li><a href="<?php echo base_url('organisation/functions'); ?>" <?php if($subtitle == "Functions"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Functions</a></li>
                            <li><a href="<?php echo base_url('organisation/orders'); ?>" <?php if($subtitle == "Orders"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Orders</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3">
                    <ul class="instruction-points">						
                        <li><a href="<?php echo base_url('organisation/vision'); ?>">Vision</a></li>
                        <li><a href="<?php echo base_url('organisation/history'); ?>">History</a></li>
                        <li><a href="<?php echo base_url('organisation/structure'); ?>">Structure</a></li>
                        <li><a href="<?php echo base_url('organisation/office'); ?>">Offices</a></li>
                        <li><a href="<?php echo base_url('organisation/functions'); ?>">Functions</a></li>
                        <li><a href="<?php echo base_url('organisation/orders'); ?>">Orders</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

