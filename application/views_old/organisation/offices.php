<style>
p.MsoNormal span {
    margin-left: 10px;
}
</style>
<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                       <ul class="aside-list">							
                            <li><a href="<?php echo base_url('organisation/vision'); ?>" <?php if($subtitle == "Vision"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Vision</a></li>
                            <li><a href="<?php echo base_url('organisation/history'); ?>" <?php if($subtitle == "History"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>History</a></li>
                            <li><a href="<?php echo base_url('organisation/structure'); ?>" <?php if($subtitle == "Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Structure</a></li>
                            <li><a href="<?php echo base_url('organisation/office'); ?>" <?php if($subtitle == "Office"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Offices</a></li>
                            <li><a href="<?php echo base_url('organisation/functions'); ?>" <?php if($subtitle == "Functions"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Functions</a></li>
                            <li><a href="<?php echo base_url('organisation/orders'); ?>" <?php if($subtitle == "Orders"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Orders</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3 mb-3">				
					<div class="row mb-4">
						<div class="col-md-12">
							<div class="title-heading">
								<h1 class="heading"> Offices</h1>
							</div>							
						</div>
                    </div>
					<div class="inner-content">
						<div id="accordion">
						  <div class="card">
							<div class="card-header" id="headingOne">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								  1. Office Address and Timings
								</button>
							  </h5>
							</div>

							<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
							  <div class="card-body">
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Amravati.pdf" target="_blank">Amravati</a></p>
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Aurangabad.pdf" target="_blank">Aurangabad</a></p>
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Latur.pdf" target="_blank">Latur</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Mumbai.pdf" target="_blank">Mumbai</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Nagpur.pdf" target="_blank">Nagpur</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Nashik.pdf" target="_blank">Nashik</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/PUne.pdf" target="_blank">Pune</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/stampOffice.pdf" target="_blank">stampOffice</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/thane.pdf" target="_blank">Thane</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/other.pdf" target="_blank">Other</a></p>	
							 </div>
							</div>
						  </div>
						<div class="card">
							<div class="card-header" id="headingTwo">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
								  2. List of offices in Concurrent Jurisdiction
								</button>
							  </h5>
							</div>
							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							  <div class="card-body">
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/State.pdf" target="_blank">State</a></p>
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Amravati_division.pdf" target="_blank">Amaravati Division</a></p>
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Aurangabad_divisiion.pdf" target="_blank">Aurangabad Division</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Latur_division.pdf" target="_blank">Latur Division</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Mumbai_division.pdf" target="_blank">Mumbai Division</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Nagpur_division.pdf" target="_blank">Nagpur Division</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Nasik_division.pdf" target="_blank">Nashik Division</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Pune_division.pdf" target="_blank">Pune Division</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Thane_division.pdf" target="_blank">Thane Division</a></p>
							 </div>
							</div>
						  </div> 
							<div class="card">
							<div class="card-header" id="headingThree">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
								  3. List of Triletters (Short Names) of Sub Registrar offices
								</button>
							  </h5>
							</div>
							<div id="collapseThree" class="collapse" aria-labelledby="collapseThree" data-parent="#accordion">
							  <div class="card-body">
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/TriLetters_Alphabetically.pdf" target="_blank">List of all offices in the state and their Tri-letters</a></p>
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/riLetters_Statewise.pdf" target="_blank">List of Tri-letters all offices in the state by Alphabetical order</a></p>
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Amravati_TriLetters.pdf" target="_blank">List of all offices in the Amaravati Division and their Tri-letters</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Aurangabad_TriLetters.pdf" target="_blank">List of all offices in the Aurangabad Division and their Tri-letters</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Latur_TriLetters.pdf" target="_blank">List of all offices in the Latur Division and their Tri-letters</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Mumbai_TriLetters.pdf" target="_blank">List of all offices in the Mumbai Division and their Tri-letters</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Nagpur_TriLetters.pdf" target="_blank">List of all offices in the Nagpur Division and their Tri-letters</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Nasik_TriLetters.pdf" target="_blank">List of all offices in the Nashik Division and their Tri-letters</a></p>	
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/pune_TriLetters.pdf" target="_blank">List of all offices in the Pune Division and their Tri-letters</a></p>
								<p class="lnk-accrdn"><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>/pdf/Thane_TriLetters.pdf" target="_blank">List of all offices in the Thane Division and their Tri-letters</a></p>
							 </div>
							</div>
						  </div>
						</div>
						<p><br /><img src="../img/Regional.gif" class="img-innerlogo img-fluid"/></p>
						<p>
						<div class="table-responsive">
						<table class="table table-bordered" width="100%">
							<tbody><tr>
								<th class="thead-dark text-center" colspan="2">									
										<b>Offices  of Department of Stamp &amp; Registration</b>									
								</th>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Sub
			  Registrar Offices</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: black">504</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Marriage
			  offices</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">3</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="right" style="text-align: right">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Total</span></b>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">507</span></b>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Joint
			  District Registrar Offices</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">34</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Collectors
			  Stamp Offices, Mumbai</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">6</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Deputy
			  Inspector General of Registration<br>  Regional Head Offices</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">8</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Joint
			  Director ,Valuation Office, Pune</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Deputy
			  Director Town Planning, valuation Office Mumbai</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Assistant
			  Director Town Planning, valuation Office</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">6</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">General
			  Stamp Office, Mumbai</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Government
			  Photo Registry Office, Pune</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Inspector
			  General of Registration Office&nbsp;</span>
									</p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="80%" style="width: 80.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal"><strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Total</span></strong></p>
								</td>
								<td width="20%" style="width: 20.0%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">566</span></b>
									</p>
								</td>
							</tr>
						</tbody></table>
						</div>
						
						</p>
						<p style="text-align:center;"><b>STATISTICAL BREIFINGS OF ALL THE REGIONS</b></p>
						<p>
							<div class="table-responsive">
							<table class="table table-bordered">
								<tbody><tr>
									<th width="29%" style="width: 29.84%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">REGIONS</span></strong>
										</p>
									</th>
									<th width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">IGR</span></strong>
										</p>
									</th>
									<th width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">DIG</span></strong>
										</p>
									</th>
									<th width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JDR</span></strong>
										</p>
									</th>
									<th width="10%" style="width: 10.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SRO</span></strong>
										</p>
									</th>
									<th width="15%" style="width: 15.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">OTHERS</span></strong>
										</p>
									</th>
									<th width="11%" style="width: 11.12%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">TOTAL</span></strong>
										</p>
									</th>
								</tr>
								<tr>
									<td width="29%" style="width: 29.84%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">AMRAVATI</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
										</p>
									</td>
									<td width="10%" style="width: 10.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">62</span>
										</p>
									</td>
									<td width="15%" style="width: 15.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.12%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">68</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="29%" style="width: 29.84%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">AURANGABAD</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">3</span>
										</p>
									</td>
									<td width="10%" style="width: 10.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">34</span>
										</p>
									</td>
									<td width="15%" style="width: 15.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.12%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">39</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="29%" style="width: 29.84%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">LATUR</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
										</p>
									</td>
									<td width="10%" style="width: 10.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">52</span>
										</p>
									</td>
									<td width="15%" style="width: 15.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">0</span>
										</p>
									</td>
									<td width="11%" style="width: 11.12%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">57</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="29%" style="width: 29.84%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">MUMBAI</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">2</span>
										</p>
									</td>
									<td width="10%" style="width: 10.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">24</span>
										</p>
									</td>
									<td width="15%" style="width: 15.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">10</span>
										</p>
									</td>
									<td width="11%" style="width: 11.12%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">37</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="29%" style="width: 29.84%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">NAGPUR</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">6</span>
										</p>
									</td>
									<td width="10%" style="width: 10.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">71</span>
										</p>
									</td>
									<td width="15%" style="width: 15.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.12%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">79</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="29%" style="width: 29.84%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">NASHIK</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
										</p>
									</td>
									<td width="10%" style="width: 10.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">75</span>
										</p>
									</td>
									<td width="15%" style="width: 15.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1&nbsp;</span>
										</p>
									</td>
									<td width="11%" style="width: 11.12%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">81</span>
										</p>
									</td>
								</tr>
								<tr style="height: 9.0pt">
									<td width="29%" style="width: 29.84%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">PUNE</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">6</span>
										</p>
									</td>
									<td width="10%" style="width: 10.06%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">110</span>
										</p>
									</td>
									<td width="15%" style="width: 15.06%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
										</p>
									</td>
									<td width="11%" style="width: 11.12%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">122</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="29%" style="width: 29.84%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">THANE</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">5</span>
										</p>
									</td>
									<td width="10%" style="width: 10.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">76</span>
										</p>
									</td>
									<td width="15%" style="width: 15.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="11%" style="width: 11.12%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">83</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="29%" style="width: 29.84%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">T0TAL</span></strong>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></strong>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">8</span></strong>
										</p>
									</td>
									<td width="11%" style="width: 11.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">34</span></strong>
										</p>
									</td>
									<td width="10%" style="width: 10.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: black">504</span></b>
										</p>
									</td>
									<td width="15%" style="width: 15.06%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">19</span></strong>
										</p>
									</td>
									<td width="11%" style="width: 11.12%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">566</span></b>
										</p>
									</td>
								</tr>
							</tbody></table>
							</div>
						</p>
						<p style="text-align:center;"><b>AMRAVATI REGION</b></p>
					
						<div class="table-responsive">
						<table class="table table-bordered" >
								<tbody><tr style="height: 19.0pt">
									<th width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in; height: 19.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">No</span></strong>
										</p>
									</th>
									<th width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in; height: 19.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">District</span></strong>
										</p>
									</th>
									<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 19.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">DIG</span></strong>
										</p>
									</th>
									<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 19.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JDR</span></strong>
										</p>
									</th>
									<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 19.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SRO</span></strong>
										</p>
									</th>
									<th width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 19.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Others</span></strong>
										</p>
									</th>
									<th width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 19.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Total</span></strong>
										</p>
									</th>
								</tr>
								<tr style="height: 10.0pt">
									<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">AMRAVATI</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">16</span>
										</p>
									</td>
									<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">19</span>
										</p>
									</td>
								</tr>
								<tr style="height: 10.0pt">
									<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">2</span>
										</p>
									</td>
									<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">AKOLA</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">15</span>
										</p>
									</td>
									<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
										</p>
									</td>
									<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">16</span>
										</p>
									</td>
								</tr>
								<tr style="height: 10.0pt">
									<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">3</span>
										</p>
									</td>
									<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">BULDHANA</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">14</span>
										</p>
									</td>
									<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
										</p>
									</td>
									<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 10.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">15</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
										</p>
									</td>
									<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">YAVATMAL</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">17</span>
										</p>
									</td>
									<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
										</p>
									</td>
									<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">18</span>
										</p>
									</td>
								</tr>
								<tr style="height: 9.0pt">
									<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
										</p>
									</td>
									<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">GRAND TOTAL</span></strong>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span></b>
										</p>
									</td>
									<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">62</span></b>
										</p>
									</td>
									<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
										</p>
									</td>
									<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 9.0pt">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">68</span></b>
										</p>
									</td>
								</tr>
							</tbody></table>
							</div>
						<p style="text-align:center;"><b>AURANGABAD REGION</b></p>
						<div class="table-responsive">
						<table class="table table-bordered">
								<tbody><tr>
									<th width="4%" style="width: 4.82%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">No</span></strong>
										</p>
									</th>
									<th width="27%" style="width: 27.46%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">District</span></strong>
										</p>
									</th>
									<th width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">DIG</span></strong>
										</p>
									</th>
									<th width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JDR</span></strong>
										</p>
									</th>
									<th width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SRO</span></strong>
										</p>
									</th>
									<th width="11%" style="width: 11.22%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Others</span></strong>
										</p>
									</th>
									<th width="28%" style="width: 28.66%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Total</span></strong>
										</p>
									</th>
								</tr>
								<tr>
									<td width="4%" style="width: 4.82%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="27%" style="width: 27.46%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">AURANGABAD</span>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">13</span>
										</p>
									</td>
									<td width="11%" style="width: 11.22%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="28%" style="width: 28.66%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">16</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="4%" style="width: 4.82%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">2</span>
										</p>
									</td>
									<td width="27%" style="width: 27.46%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">BEED</span>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">12</span>
										</p>
									</td>
									<td width="11%" style="width: 11.22%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
										</p>
									</td>
									<td width="28%" style="width: 28.66%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">13</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="4%" style="width: 4.82%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">3</span>
										</p>
									</td>
									<td width="27%" style="width: 27.46%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JALNA</span>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">9</span>
										</p>
									</td>
									<td width="11%" style="width: 11.22%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
										</p>
									</td>
									<td width="28%" style="width: 28.66%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">10</span>
										</p>
									</td>
								</tr>
								<tr>
									<td width="4%" style="width: 4.82%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
										</p>
									</td>
									<td width="27%" style="width: 27.46%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">GRAND TOTAL</span></strong>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">3</span></b>
										</p>
									</td>
									<td width="9%" style="width: 9.28%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">34</span></b>
										</p>
									</td>
									<td width="11%" style="width: 11.22%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
										</p>
									</td>
									<td width="28%" style="width: 28.66%; padding: 0in 0in 0in 0in">
										<p class="MsoNormal" align="center" style="text-align: center">
											<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">39</span></b>
										</p>
									</td>
								</tr>
							</tbody></table>
							</div>
						
						<p style="text-align:center;"><b>LATUR REGION</b></p>
						<div class="table-responsive">
						<table class="table table-bordered">
							<tbody><tr>
								<th width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">No</span></strong>
									</p>
								</th>
								<th width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">District</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">DIG</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JDR</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SRO</span></strong>
									</p>
								</th>
								<th width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Others</span></strong>
									</p>
								</th>
								<th width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Total</span></strong>
									</p>
								</th>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">LATUR</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">12</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">14</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">2</span>
									</p>
								</td>
								<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">NANDED</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">18</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">19</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">3</span>
									</p>
								</td>
								<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">OSMANABAD</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">8</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">9</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
									</p>
								</td>
								<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">PARBHANI</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">14</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">15</span>
									</p>
								</td>
							</tr>
							<tr style="height: 9.0pt">
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">GRAND TOTAL</span></strong>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">52</span></b>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span></b>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">57</span></b>
									</p>
								</td>
							</tr>
						</tbody></table>
						</div>
						<p style="text-align:center;"><b>MUMBAI REGION</b></p>
					<div class="table-responsive">
						<table class="table table-bordered">
							<tbody><tr>
								<th width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">No</span></strong>
									</p>
								</th>
								<th width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">District</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">DIG</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JDR</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SRO</span></strong>
									</p>
								</th>
								<th width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Others</span></strong>
									</p>
								</th>
								<th width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Total</span></strong>
									</p>
								</th>
							</tr>
							<tr style="height: 10.0pt">
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in; height: 10.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in; height: 10.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">MUMBAI</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 10.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">5</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 10.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">6</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in; height: 10.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">13</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">2</span>
									</p>
								</td>
								<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">BANDRA</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">19</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">24</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="31%" style="width: 31.02%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">GRAND TOTAL</span></strong>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">2</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">24</span></b>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">10</span></b>
									</p>
								</td>
								<td width="13%" style="width: 13.56%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">37</span></b>
									</p>
								</td>
							</tr>
						</tbody></table>
						</div>
						<p style="text-align:center;"><b>NAGPUR REGION</b></p>
						<div class="table-responsive">
						<table class="table table-bordered">
							<tbody><tr>
								<th width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">No</span></strong>
									</p>
								</th>
								<th width="34%" style="width: 34.5%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">District</span></strong>
									</p>
								</th>
								<th width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">DIG</span></strong>
									</p>
								</th>
								<th width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JDR</span></strong>
									</p>
								</th>
								<th width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SRO</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.86%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Others</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Total</span></strong>
									</p>
								</th>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="34%" style="width: 34.5%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">BHANDARA</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">15</span>
									</p>
								</td>
								<td width="12%" style="width: 12.86%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">16</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">2</span>
									</p>
								</td>
								<td width="34%" style="width: 34.5%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">CHANDRAPUR</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">16</span>
									</p>
								</td>
								<td width="12%" style="width: 12.86%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">17</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">3</span>
									</p>
								</td>
								<td width="34%" style="width: 34.5%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">GADCHIROLI</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">10</span>
									</p>
								</td>
								<td width="12%" style="width: 12.86%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">11</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
									</p>
								</td>
								<td width="34%" style="width: 34.5%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">NAGPUR</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">9</span>
									</p>
								</td>
								<td width="12%" style="width: 12.86%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">12</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">5</span>
									</p>
								</td>
								<td width="34%" style="width: 34.5%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">NAGPUR</span><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;"> GRAMIN</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">12</span>
									</p>
								</td>
								<td width="12%" style="width: 12.86%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="12%" style="width: 12.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">13</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">6</span>
									</p>
								</td>
								<td width="34%" style="width: 34.5%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">WARDHA</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">9</span>
									</p>
								</td>
								<td width="12%" style="width: 12.86%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">10</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.94%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="34%" style="width: 34.5%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">GRAND TOTAL</span></strong>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">6</span></b>
									</p>
								</td>
								<td width="11%" style="width: 11.6%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">71</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.86%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">79</span></b>
									</p>
								</td>
							</tr>
						</tbody></table>
						</div>
						<p style="text-align:center;"><b>NASHIK REGION</b></p>
						<div class="table-responsive">
						<table class="table table-bordered">
							<tbody><tr>
								<th width="5%" style="width: 5.1%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">No</span></strong>
									</p>
								</th>
								<th width="30%" style="width: 30.98%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">District</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">DIG</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JDR</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SRO</span></strong>
									</p>
								</th>
								<th width="13%" style="width: 13.52%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Others</span></strong>
									</p>
								</th>
								<th width="13%" style="width: 13.54%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Total</span></strong>
									</p>
								</th>
							</tr>
							<tr>
								<td width="5%" style="width: 5.1%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; font-weight: normal">1</span></strong>
									</p>
								</td>
								<td width="30%" style="width: 30.98%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">AHMEDNAGAR</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">17</span>
									</p>
								</td>
								<td width="13%" style="width: 13.52%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.54%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">18</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="5%" style="width: 5.1%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; font-weight: normal">2</span></strong>
									</p>
								</td>
								<td width="30%" style="width: 30.98%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">DHULE</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">13</span>
									</p>
								</td>
								<td width="13%" style="width: 13.52%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.54%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">14</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="5%" style="width: 5.1%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">3</span>
									</p>
								</td>
								<td width="30%" style="width: 30.98%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JALGAON</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">19</span>
									</p>
								</td>
								<td width="13%" style="width: 13.52%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.54%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">20</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="5%" style="width: 5.1%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
									</p>
								</td>
								<td width="30%" style="width: 30.98%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">NASHIK</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">26</span>
									</p>
								</td>
								<td width="13%" style="width: 13.52%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="13%" style="width: 13.54%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">29</span>
									</p>
								</td>
							</tr>
							<tr style="height: 9.0pt">
								<td width="5%" style="width: 5.1%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="30%" style="width: 30.98%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">GRAND TOTAL</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.28%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">75</span></b>
									</p>
								</td>
								<td width="13%" style="width: 13.52%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
									</p>
								</td>
								<td width="13%" style="width: 13.54%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">81</span></b>
									</p>
								</td>
							</tr>
						</tbody></table>
						</div>
						<p style="text-align:center;"><b>PUNE REGION</b></p>
						<div class="table-responsive">
						<table class="table table-bordered">
							<tbody><tr>
								<th width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">No</span></strong>
									</p>
								</th>
								<th width="32%" style="width: 32.7%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">District</span></strong>
									</p>
								</th>
								<th width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">DIG</span></strong>
									</p>
								</th>
								<th width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JDR</span></strong>
									</p>
								</th>
								<th width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SRO</span></strong>
									</p>
								</th>
								<th width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Others</span></strong>
									</p>
								</th>
								<th width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Total Offices</span></strong>
									</p>
								</th>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; font-weight: normal">1</span></strong>
									</p>
								</td>
								<td width="32%" style="width: 32.7%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">KOLHAPUR</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">18</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">19</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">2</span>
									</p>
								</td>
								<td width="32%" style="width: 32.7%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">PUNE</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">27</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">5</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">34</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">3</span>
									</p>
								</td>
								<td width="32%" style="width: 32.7%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">PUNE GRAMIN</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">21</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">22</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
									</p>
								</td>
								<td width="32%" style="width: 32.7%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SATARA</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">15</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">16</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">5</span>
									</p>
								</td>
								<td width="32%" style="width: 32.7%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SANGLI</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">13</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">14</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">6</span>
									</p>
								</td>
								<td width="32%" style="width: 32.7%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SOLHAPUR</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">16</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">17</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="32%" style="width: 32.7%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">GRAND TOTAL</span></strong>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">6</span></b>
									</p>
								</td>
								<td width="11%" style="width: 11.96%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">110</span></b>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">5</span></b>
									</p>
								</td>
								<td width="13%" style="width: 13.24%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">122</span></b>
									</p>
								</td>
							</tr>
						</tbody></table>
							</div>
						<p style="text-align:center;"><b>THANE REGION</b></p>
						<div class="table-responsive">
						<table class="table table-bordered">
							<tbody><tr>
								<th width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">No</span></strong>
									</p>
								</th>
								<th width="31%" style="width: 31.04%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">District</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">DIG</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">JDR</span></strong>
									</p>
								</th>
								<th width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SRO</span></strong>
									</p>
								</th>
								<th width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Others</span></strong>
									</p>
								</th>
								<th width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">Total</span></strong>
									</p>
								</th>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="31%" style="width: 31.04%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">RAIGARH</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">20</span>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">21</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">2</span>
									</p>
								</td>
								<td width="31%" style="width: 31.04%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">RATNAGIRI</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">9</span>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">10</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">3</span>
									</p>
								</td>
								<td width="31%" style="width: 31.04%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">SINDHUDURG</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">8</span>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">-</span>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">9</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">4</span>
									</p>
								</td>
								<td width="31%" style="width: 31.04%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">THANE</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">16</span>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">19</span>
									</p>
								</td>
							</tr>
							<tr>
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">5</span>
									</p>
								</td>
								<td width="31%" style="width: 31.04%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">THANE GRAMIN</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">23</span>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">24</span>
									</p>
								</td>
							</tr>
							<tr style="height: 9.0pt">
								<td width="4%" style="width: 4.9%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">&nbsp;</span>
									</p>
								</td>
								<td width="31%" style="width: 31.04%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<strong><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">GRAND TOTAL</span></strong>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">5</span></b>
									</p>
								</td>
								<td width="12%" style="width: 12.3%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">76</span></b>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">1</span></b>
									</p>
								</td>
								<td width="13%" style="width: 13.58%; padding: 0in 0in 0in 0in; height: 9.0pt">
									<p class="MsoNormal" align="center" style="text-align: center">
										<b><span style="font-size: 10.0pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;">83</span></b>
									</p>
								</td>
							</tr>
						</tbody></table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </section>

