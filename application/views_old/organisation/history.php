<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                       <ul class="aside-list">							
                            <li><a href="<?php echo base_url('organisation/vision'); ?>" <?php if($subtitle == "Vision"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Vision</a></li>
                            <li><a href="<?php echo base_url('organisation/history'); ?>" <?php if($subtitle == "History"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>History</a></li>
                            <li><a href="<?php echo base_url('organisation/structure'); ?>" <?php if($subtitle == "Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Structure</a></li>
                            <li><a href="<?php echo base_url('organisation/office'); ?>" <?php if($subtitle == "Offices"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Offices</a></li>
                            <li><a href="<?php echo base_url('organisation/functions'); ?>" <?php if($subtitle == "Functions"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Functions</a></li>
                            <li><a href="<?php echo base_url('organisation/orders'); ?>" <?php if($subtitle == "Orders"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Orders</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3">
                    <div class="col-md-12 mt-3">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="title-heading">
                                <h1 class="heading"> History</h1>
                            </div>
							
                        </div>
                    </div>
                    <div class="inner-content">
                        <p>Registration and Stamps Department is related to some of the most important events in person’s life. It incorporates buying and selling of immovable property, payment of stamp duty, marriage registration and such activities. It is estimated that about two crore citizens avail the services of the department annually.</p>
                        
						<p>
						The Registration came into force in India in 1908. However its foundation is seen in document registration system used by the then East India Company in the 18th century in areas under its rule. During the British period, the authority to register documents vested with the Magistrates. Later on, a separate system was evolved for this purpose and thus a separate registration department came into being.
						</p>
						<p>
						Collection of tax through Stamp Duty began in 1815 in Mumbai. Thereafter in 1827, separate system and offices were established for sale of stamps and collection of stamp duty in the Bombay region. Thus, the system of stamp duty collection has a history of about two centuries.
						</p>
						<p>After independence, the Registration Department functioned under the Revenue and Forest Department under the control of the Settlement Commissioner. Considering the nature of work, expanse and importance of Registration Department and Stamps Department, a new department was formed in 1988 by merging the two departments. This new unified Department of Registration and Stamps started functioning under control of Inspector General of Registration and Controller of Stamps.</p>
						<p>
						Since then, the graph of this department has been consistently on the rise. The new activities included streamlining of stamp duty rates from time to time, conceptualization of Market Value and implementation of annual statement of rates. These measures led to increase in the revenue of the department and in turn have significantly contributed in the development of the State. Today, the Registration and Stamps Department ranks second after Sales Tax Department in terms of revenue collection.
						</p>
						<p>
						In the last few years, the department has made significant changes in the system of document registration. ‘SARITA’ the computerized system for registration of document was introduced in 2002. In 2012 ‘i-SARITA’ a centralized web based online registration system was introduced for efficient registration. The department has also introduced online facilities like e-Payment, e-Search, e-ASR etc. The department has recently introduced ‘e-Registration’, an online registration system to facilitate registration of first sale of flats in large housing projects and also of leave and license agreements, without having visit to the Sub-Registrar’s office. The department is continuing its revolutionary steps for delivering speedy, efficient and citizen friendly service.
						</p>
						<hr/>
						<p style="text-align:center; font-weight:bold;">Name of officers holding the charge of the post of
						</p>
						<p><h4>Inspector General of Registration, Maharashtra State.</h4></p>
						<div class="table-responsive">
						<table class="table table-bordered" width="100%">
						<tbody>
							<tr>
						<th rowspan="2" align="center">Sr.no</th>
						<th rowspan="2" align="center">Name</th>
						<th colspan="2" align="center">Date</th>

						</tr>
						<tr>
						<th align="center">From</th>
						<th align="center">To</th>
						</tr>


						<tr>
						<td width="50px" align="center">1.</td>
						<td>Mr. V.N. Karandikar, <span style="font-size:8px;">I.A.S.</span> </td>
						<td width="150px" align="center">14-10-1988  </td>
						<td width="150px" align="center">30-04-1990</td>
						</tr>

						<tr>
						<td align="center">2.</td>
						<td>Mr. S.C. Kothari, <span style="font-size:8px;">I.A.S.</span>  </td>
						<td align="center">01-05-1990 </td>
						<td align="center">31-12-1992</td>
						</tr>

						<tr>
						<td align="center">3.</td>
						<td>Mr. Ghansham Talreja, <span style="font-size:8px;">I.A.S.</span> </td>
						<td align="center">25-01-1993 </td>
						<td align="center">19-07-1993 </td>
						</tr>

						<tr>
						<td align="center">4.</td>
						<td>Mr. A.S. Surve, <span style="font-size:8px;">I.A.S.</span> </td>
						<td align="center">24.07.1996 </td>
						<td align="center">29-03-2000 </td>
						</tr>

						<tr>
						<td align="center">5.</td>
						<td>Mr. K.B. Bhoge, <span style="font-size:8px;">I.A.S.</span>  </td>
						<td align="center">03-04-2000 </td>
						<td align="center">30-04-2000</td>
						</tr>
						<tr>
						<td align="center">6.</td>
						<td>Dr. Nitin Kareer, <span style="font-size:8px;">I.A.S.</span>  </td>
						<td align="center">01-06-2000</td>
						<td align="center">08-06-2004 </td>
						</tr>

						<tr>
						<td align="center">7.</td>
						<td>Mr. Om Prakash Gupta, <span style="font-size:8px;">I.A.S.</span></td>
						<td align="center"> 08-06-2004 </td>
						<td align="center">18-05-2007 </td>
						</tr>

						<tr>
						<td align="center">8.</td>
						<td>Mr. Ramrao Shingare, <span style="font-size:8px;">I.A.S.</span></td>
						<td align="center">18-05-2007 </td>
						<td align="center">30-08-2010</td>
						</tr>

						<tr>
						<td align="center">9.</td>
						<td> Mr. S. Chockalingam, <span style="font-size:8px;">I.A.S.</span></td>
						<td align="center">30-08-2010</td>
						<td align="center">10-02-2014 </td>
						</tr>

						<tr>
						<td align="center">10.</td>
						<td>Dr. Shrikar Pardeshi, <span style="font-size:8px;">I.A.S.</span></td>
						<td align="center">10-02-2014 </td>
						<td align="center">07-04-2015 </td>
						</tr>

						<tr>
						<td align="center">11. </td>
						<td>Dr. Ramaswami. N, <span style="font-size:8px;">I.A.S.</span> </td> 
						<td align="center">07-04-2015</td> 
						<td align="center">27-04-2017</td>
						</tr> 

						<tr>
						<td align="center">12. </td>
						<td>Mr. Anil Kawade <span style="font-size:8px;">I.A.S.</span> </td> 
						<td align="center">27-04-2017</td> 
						<td align="center">22-01-2020</td>
						</tr>

						<tr>
						<td align="center">13. </td>
						<td>Mr. Omprakash Deshmukh <span style="font-size:8px;">I.A.S.</span> </td> 
						<td align="center">22-01-2020</td> 
						<td align="center">Till Date</td>
						</tr>
						 
						</tbody></table>
						</div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>

