<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<!-- Latest compiled and minified CSS -->
<!-- jQuery library -->

<!-- Latest compiled JavaScript -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- basic libraries required for datatable Funcyions include : search,pagination,no of records -->
<script src="//cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css" >
<link rel="stylesheet" href="<?php echo base_url();?>js/datatables/bootstrap.min.css">

<!-- datatable buttons-->
<script src="https://cdn.datatables.net/buttons/1.0.1/js/dataTables.buttons.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.0.1/css/buttons.dataTables.min.css" >
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.colvis.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.html5.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.flash.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>js/jquery.dataTables.rowGrouping.js"></script>

<script src="<?php echo base_url();?>js/tableExport.js"></script>
<script src="<?php echo base_url();?>js/jquery.base64.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jspdf/jspdf.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jspdf/libs/base64.js"></script>

<!--Responsive table-->
<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.min.css" >
<script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.bootstrap.min.css" >-->
<!--Which column should display-->
<script src="//cdn.datatables.net/buttons/1.0.1/js/buttons.colVis.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.2.0/css/colReorder.dataTables.min.css" >

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reoprt</title>
<script>
   $(function() {
 	 $( "#datepickerFrom" ).datepicker({
       defaultDate: "+1w",
       changeMonth: true,
 	   changeYear: true,
       numberOfMonths: 1
     });
   });
   </script>
</head>
<body>
<div style="margin:2% 0 1% 10%;">
<form action="" method="post">
<input  type="text" name="report_date" id="datepickerFrom" class="datepick" data-rule-required="true" value="<?php if($report_date!=''){echo $report_date;}?>" 
data-msg-required="Please select from date" placeholder="Please select from date">
<input type="submit" name="search_btn" value="Search" id="report_search_btn" class="btn btn-info no-shadow">
<input type="submit" name="reset_btn" value="Reset" id="reset_btn" class="btn greybg">
  <div id="error_report_date" style="color:#F00"><?php echo form_error('report_date');?></div>
</form>
</div>
<div id="demo">
<div style="margin:1% 0 1% 10%;">
<a href="#" target="_blank" onClick ="$('#table').tableExport({type:'csv',escape:'false'});" class="btn btn-primary no-shadow">CSV</a>
<a href="#" target="_blank" onClick ="$('#table').tableExport({type:'txt',escape:'false'});" class="btn btn-warning no-shadow">TXT</a>
<a href="#" target="_blank" onClick ="$('#table').tableExport({type:'doc',escape:'false'});" class="btn btn-info no-shadow">DOC</a>
</div>
<table class="table table-striped table-bordered dt-responsive nowrap" id="table">
							<thead>
									<tr>
                                    <th class="text-center bluehd">
											Sr No.
										</th>
										<th class="text-center bluehd">
											Grievance<br /> Recieved From
										</th>
                                        <th class="text-center bluehd">
											Brought<br /> Forward
										</th>
										<th class="text-center bluehd">
											Reciept During <br />the Month
										</th>
										<th class="text-center bluehd">
											Disposed During <br />the month
										</th>
										<th class="text-center bluehd">
											Pending At the <br />till End of Month
										</th>
										<th class="text-center bluehd">
											No. of Grievances pending <br />for more than 60 days
										</th>
                                        <th class="text-center bluehd">
											No. of Grievances pending <br />for more than 1 Year
										</th>
									</tr>
									</thead>
									<tbody>
                                  <?php 
								  {
									 $i = 1;
									if($report_date!='')
									{
									 $to60=date('Y-m-d', strtotime($report_date));
									 $from60=date('Y-m-d',strtotime(''.$report_date.' -60 days'));
									 $to365=date('Y-m-d',strtotime($report_date));
									 $fromyr=date('Y-m-d',strtotime(''.$report_date.' -12 months'));
									 $from365=date('Y-m-d',strtotime(''.$fromyr.' -1 days'));
									 $month_submit_from=date('Y-m-01',strtotime($report_date));
								 	 $month_submit_to=date('Y-m-t',strtotime($report_date));
									 $brought_forward_from=date('Y-m-01',strtotime($report_date));
									}
									else
									{
									  $to60=date('Y-m-d');
									  $from60=date('Y-m-d',strtotime('-60 days'));
									  $to365=date('Y-m-d');
									  $fromyr=date('Y-m-d',strtotime('-12 months'));
									  $from365=date('Y-m-d',strtotime(''.$fromyr.' -1 days'));
									  $month_submit_from=date('Y-m-01');
									  $month_submit_to=date('Y-m-t');
									  $brought_forward_from=date('Y-m-01');
									}
									  foreach($category as $res)
									  {
										$brought_forward=0;
										?>
                     		         <tr>  
                                   <!--  Sr No-->
                                    <td  class="text-center"><?php echo  $i ;?></td>

                                   <!-- Grievance Recieved From-->
                                    <td  class="text-center"><?php echo ucfirst($res['type']);?></td>

                                    <!--Brought forward-->
                                    <td class="text-center">
                                    <?php 
									$query1='registerdate < CAST(\''.$brought_forward_from.'\' AS DATE)';
									$this->db->where($query1);
									$this->db->where('reply_status','1');
									$complaint_category1=$this->master_model->getRecords('usercomplainbox',array('category'=>$res['id']));
									$brought_forward=count($complaint_category1);
									echo $brought_forward;?>
                                    </td>

                                    <!--Reciept During the month-->
                                    <td class="text-center">
                                    <?php 
									$query2='registerdate BETWEEN CAST(\''.$month_submit_from.'\' AS DATE) AND CAST(\''.$month_submit_to.'\' AS DATE)';
									$this->db->where($query2);
									$complaint_category2=$this->master_model->getRecords('usercomplainbox',array('category'=>$res['id']));
									echo count($complaint_category2);?>
                                    </td>
                                    
                                    <!--Disposed During the month-->
                                    <td class="text-center"><?php 
									$query3='registerdate BETWEEN CAST(\''.$month_submit_from.'\' AS DATE) AND CAST(\''.$month_submit_to.'\' AS DATE)';
									$this->db->where($query3);
									$this->db->where('reply_status','0');
									$complaint_category3=$this->master_model->getRecords('usercomplainbox',array('category'=>$res['id']));
									echo count($complaint_category3);?></td>
                                    
                                   <!-- Pending At the till End of Month-->
                                    <td class="text-center">
                                    <?php 
									$query4='registerdate BETWEEN CAST(\''.$month_submit_from.'\' AS DATE) AND CAST(\''.$month_submit_to.'\' AS DATE)';
									$this->db->where($query4);
									$this->db->where('reply_status','1');
									$complaint_category4=$this->master_model->getRecords('usercomplainbox',array('category'=>$res['id']));
									echo ($brought_forward+count($complaint_category4));
									?>
                                    </td>
                                    
                                    <!--No. of Grievances pending for more than 60 days-->
                                    <td class="text-center"><?php 
									$query5='registerdate < CAST(\''.$from60.'\' AS DATE)';
									$this->db->where($query5);
									$this->db->where('reply_status','1');
									$complaint_category5=$this->master_model->getRecords('usercomplainbox',array('category'=>$res['id']));
									echo count($complaint_category5);
									?></td>
                                    
                                   <!-- No. of Grievances pending for more than 1 Year-->
                                    <td class="text-center"><?php 
									$query6='registerdate <= CAST(\''.$from365.'\' AS DATE)';
									$this->db->where($query6);
									$this->db->where('reply_status','1');
									$complaint_category6=$this->master_model->getRecords('usercomplainbox',array('category'=>$res['id']));
									echo count($complaint_category6);
									?></td>
                                    </tr>
									<?php $i++;}
								  }?>
                                    </tbody>
</table>
</div>
</body>
<script>
$(document).ready(function(){
   /* $('#table').DataTable({
		dom: 'Bfrtip', //to show the buttons on the screen
        buttons: [  
		 'print',
		 'pdf',
        ]
	});*/
	
    var oTable = $('#table').dataTable({
        'bPaginate': false,
  		dom: 'Bfrtip', //to show the buttons on the screen
        buttons: [  
		 'print',
		 'excel',
		 'pdf',
		 'colvis'
        ]
    });

    // Populate values
    var $rows = oTable.fnGetNodes();
    var values = {};
    var colnums = [2];

    for (var col = 0, n = colnums.length; col < n; col++) {

        var colnum = colnums[col];
        if (typeof values[colnum] === "undefined") values[colnum] = {};

        // Create Unique List of Values
        $('td:nth-child(' + colnum + ')', $rows).each(function () {
            values[colnum][$(this).text()] = 1;
        });

        // Create Checkboxes
        var labels = [];
        $.each(values[colnum], function (key, count) {
            var $checkbox = $('<input />', {
                'class': 'filter-column filter-column-' + colnum,
                    'type': 'checkbox',
                    'value': key
            });
            var $label = $('<label></label>', {
                'class': 'filter-container'
            }).append($checkbox).append(' ' + key);
            $checkbox.on('click', function () {
                oTable.fnDraw();
            }).data('colnum', colnum);
            labels.push($label.get(0));
        });
        var $sorted_containers = $(labels).sort(function (a, b) {
            return $(a).text().toLowerCase() > $(b).text().toLowerCase();
        });
        $('#demo').prepend($sorted_containers);
        $sorted_containers.wrapAll($('<div></div>', {
            'class': 'checkbox-group checkbox-group-column-' + colnum
        }));
    }

    $.fn.dataTableExt.afnFiltering.push(function (oSettings, aData, iDataIndex) {
        var checked = [];
        $('.filter-column').each(function () {
            var $this = $(this);
            if ($this.is(':checked')) checked.push($this);
        });

        if (checked.length) {
            var returnValue = false;
            $.each(checked, function (i, $obj) {
                if (aData[$obj.data('colnum') - 1] == $obj.val()) {
                    returnValue = true;
                    return false; // exit loop early
                }
            });

            return returnValue;
        }

        if (!checked.length) return true;
        return false;
    });
});


$('#reset_btn').click(function(){
            $('#configform')[0].reset();
 })

</script>
</html>