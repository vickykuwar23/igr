

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
      <!-- jQuery library -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <!-- Latest compiled JavaScript -->
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
      <!-- basic libraries required for datatable Funcyions include : search,pagination,no of records -->
      <script src="//cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css" >
      <link rel="stylesheet" href="<?php echo base_url();?>js/datatables/bootstrap.min.css">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Grievance Portal</title>
   </head>
   <body>
      <table class="table table-striped table-bordered dt-responsive nowrap" id="table">
         <thead>
            <tr>
               <th class="text-center bluehd">
                  Sr. No.
               </th>
               <th class="text-center bluehd">
                  Grievance ID
               </th>
               <th class="text-center bluehd">
                  Complainant </br>name
               </th>
               <th class="text-center bluehd">
                  Complaint </br>Message
               </th>
               <th class="text-center bluehd">
                  Complaint</br> date
               </th>
               <th class="text-center bluehd">
                  Category
               </th>
               <th class="text-center bluehd">
                  Reply Status
               </th>
               <th class="text-center bluehd">
                  Grievance Status
               </th>
               <th class="text-center bluehd">
                  Replied by
               </th>
            </tr>
         </thead>
         <tbody>
            <?php $sr=0;?>
            <?php foreach($result as $details) {
               $username=$this->master_model->getRecords('userregistration',array('user_id'=>$details['user_id']),'user_name');?>
            <tr>
               <td class="text-center"><?php echo $sr+1;?></td>
               <td class="text-left"><?php echo $details['pattern'];?></td>
               <td class="text-left"><?php echo ucfirst($username[0]['user_name']);?></td>
               <td><a style="text-decoration:none;" title="<?php echo $details['msg_content'];?>" href="<?php echo base_url()?>callcenterpanel/usercomplaindetail/<?php echo $details['msgid'];?>" ><?php echo substr($details['msg_content'],'0',30).'...';?></a></td>
               <td class="text-center"><?php echo date('d M Y',strtotime($details['registerdate']))?></td>
               <td class="text-center"><?php 
                  $categoty=$this->master_model->getRecords('departments',array('id'=>$details['category']));	
                   echo ucfirst($categoty[0]['type']);
                  ?></td>
               <td class="text-center"><?php if($details['status']==0) {echo 'Not Replied' ;}else{ echo 'Replied';}?></td>
               <td class="text-center"><?php if($details['reply_status']==0) {echo 'Closed' ;}else{ echo 'Open';}?></td>
               <td class="text-center"><?php 
                  if($details['replyby']!='' && $details['replyby']!='0')
                  {
                  	if($details['replyby']!='')
                  	{
                  		$repliedarray=array();
                  		$repliedbyid=explode(',',$details['replyby']);
                  		if(count($repliedbyid) >0)
                  		{
                  			foreach($repliedbyid as $replyidres)
                  			{
                  				$adminusername=$this->master_model->getRecords('adminlogin',array('id'=>$replyidres));
                  				if(count($adminusername) > 0)
                  				{
                  					$repliedarray[]=ucfirst($adminusername[0]['adminuser']);
                  				}
                  			}
                  			if(count($repliedarray) > 0)
                  			{
                  				echo implode(',',$repliedarray);
                  			}
                  		}
                  		else
                  		{
                  			echo 'User not found.';
                  		}
                  	}
                  	else
                  	{
                  		echo 'User not found.';
                  	}
                  }?></td>
            </tr>
            <?php $sr++?>
            <?php }?>
         </tbody>
      </table>
      <div class="row">
         <div class="col-md-12">
            <div class="divider"></div>
            <div class="row">
               <div class="col-md-2 text-center col-md-offset-5 m-t-b">
                  <a class="btn btn-comn no-shadow" href="<?php echo base_url();?>callcenterpanel/users"><i class="fa fa-angle-double-left"></i>Go back</a>
               </div>
            </div>
         </div>
      </div>
   </body>
   <script>
      $(document).ready(function(){
          $('#table').DataTable({
      		dom: 'Bfrtip', //to show the buttons on the screen
              buttons: [    // which buttons should display
              ]
      	});
      });
   </script>
</html>

