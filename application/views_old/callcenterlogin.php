<div style="text-align:center;">
 <?php if ($this->session->flashdata('invalidadminlogin') != '') { ?>
     <div class="alert alert-warning" style="font-size:17px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('invalidadminlogin') ?>
      </div>
    <? } ?></div>
 <section id="content" class="min380">
  <div class="main padder">
    <div class="row">
      <div class="col-lg-4 col-lg-offset-4 m-t-large">
        <section class="panel grdbg m-t-large">
          <header class="panel-heading text-left">Call Center - Sign in </header>
          <form class="panel-body" method="post">
            <div class="block m-b">
             <!-- <label class="control-label loginlable">Email</label>
              <input type="email" placeholder="test@example.com" class="form-control" name="username">-->
               <label class="control-label loginlable">Username</label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your username here"></i>
              <input type="text" class="form-control" name="username" required>
            </div>
            <div class="block m-b">
              <label class="control-label loginlable">Password</label><i class="fa fa-info-circle pull-left m-t-s" data-toggle="tooltip" title="Enter your password here" ></i>
              <input name="password" type="password" id="inputPassword" class="form-control" required>
            </div>
              <p class="text-center blue">Please enter the text you see in the image below into the text box provided.</p>
              <div class="form-group">
                <label class="loginlable control-label"><?php echo $image; ?></label>
                <div class="col-lg-7">
                  <input type="text" name="code" placeholder="" class="form-control"  id="code"> 
                    <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('code');?></div>
                </div>
              </div>
             <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
            <button type="submit" class="btn pull-right m-r-large m-b no-shadow greybg" name="submit">Sign in</button>
          </form>
        </section>
      </div>
    </div>
  </div>
</section>
