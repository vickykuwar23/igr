<style>
    .complaint_code_frm label {
        margin-bottom: 0;
        margin-top: 5px;
    }
</style>
<!-- asidebar error_message-->
    <section class="sitemap-wrapper">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-12">
                    <div class="title-heading">
                        <h1 class="heading">Report To IGRA</h1>
                    </div>
                </div>
            </div>
            <div class="row mb-12">
                <div class="col-sm-12">
                    <!-- <p>The Independent Grievance Redressal Authority (IGRA) is a competent authority appointed by Government of Maharashtra. This authority is not a part of the Department of Registration and Stamps and hence will work as an Independent authority. </p>
					<p>Presently, Hon'ble Divisional Commisioner is identifed as the IGRA who is the ultimate authority (Level 3) in the current Grievance Redressal Mechanism developed by Department of Registration and Stamps. If the resolution provided by Level 2 officers is not satisfactory then the citizens can report their grievances to IGRA through this facility.</p> 
					<p>Detailed Grievance Redressal workflow is given under the tab 'Grievance Redressal Mechanism' on this website. </p> -->
                    <p>
                            The Independent Grievance Redressal Authority (IGRA) is a competent authority appointed by Government of Maharashtra. This authority is not a part of the Department of Registration and Stamps and hence will work as an Independent authority. Presently, Hon'ble Divisional Commisioner is identifed as the IGRA who is the ultimate authority (Level 3) in the current Grievance Redressal Mechanism developed by Department of Registration and Stamps. If the resolution provided by Level 2 officers is not satisfactory then the citizens can report their grievances to IGRA through this facility. Detailed Grievance Redressal workflow is given under the tab 'Grievance Redressal Mechanism' on this website. 
                        </p>
                   
                </div>
            </div>
            <hr />
            <div class="row col-md-12">
                <form class="w-100" name="notificationfrm" id="notificationfrm" method="post"
                    action="">
					<?php if ($this->session->flashdata('error_message') != '') { ?>
                    <div class="row mb-5">
                        <div class="col-md-6">
                            <div class="alert alert-error mb-0" style="font-size:17px;">
                                <span> <i class="fa fa-tick pr-2"></i><?php echo $this->session->flashdata('error_message') ?> <strong>
                                    </strong></span><strong>
                                    <button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
                                </strong>
                            </div>
                        </div>
                    </div>
					<?php } ?>
                    <div class="complaint_code_frm">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <label class="control-label">Enter Complaint Code </label>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <input type="text" name="c_code" class="form-control"  id="c_code" value="" />
								<div id="c_code" class="error" style="color:#F00"><?php echo form_error('c_code'); ?> </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <button type="submit" name="submit" id="submit" class="btn btn_submit"
                                        id="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
	
    <!-- //asidebar -->