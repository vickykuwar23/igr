<?php
header('X-Frame-Options: DENY');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">

<title>Grievance Redressal System IGR.</title>



<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="shortcut icon" href="<?php echo base_url();?>img/icon.png" type="image/vnd.microsoft.icon" />

<link rel="stylesheet" href="<?php echo base_url();?>css/font.css">

<!-- <link rel="stylesheet" href="<?php echo base_url();?>css/app.v2.css" type="text/css" /> -->

<!-- ==== Bootstrap Framework ==== -->
<!-- basic libraries required for datatable Funcyions include : search,pagination,no of records -->

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css" >

    
  <link rel="stylesheet" href="<?php echo base_url();?>js/datatables/bootstrap.min.css">  --> 
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" >
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
    <!-- font awesome icons -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css">
    <!-- owl carousel  -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/owl.carousel.min.css">
    <!-- marquee -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/marquee.css">
    <!-- <link href="<?php echo base_url();?>css/custom.css" rel="stylesheet"> -->
    <!-- ==== Main Stylesheet ==== -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/styles.css">
<!-- 
<link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet"> -->

<link href="<?php echo base_url();?>css/resizer.css" rel="stylesheet">



<!--[if lt IE 9]> <script src="js/ie/respond.min.js"></script> <script src="js/ie/html5.js"></script> <![endif]-->

<!-- basic libraries required for datatable Funcyions include : search,pagination,no of records -->

<!-- <script src="<?php echo base_url();?>js/app.v2.js"></script> -->
<script src="<?php echo base_url();?>js/jquery.min.js"></script>

<script src="<?php echo base_url();?>js/encrypt.js"></script>
<script src="<?php echo base_url();?>js/validation.js"></script>
<script src="<?php echo base_url();?>js/sha256.js"></script>
<style>
.datatable thead {background-color: #24699e;padding: 10px 0 10px;color: #fff;}
.datatable .table-bordered td, .table-bordered th {border-bottom: 0 !important;}

        .row.citizen_services ul li a {
    color: #85c0ac;
    font-size: 14px;
}
.row.citizen_services h2 {
    font-size: 18px;padding-top: 1.1em;
}
.row.citizen_services ul li:before {
    content: "\f101";
    font-family: FontAwesome;
    padding-right: 0.7em;
    font-size: 14px;
    color: #999;
}
.row.citizen_services ul {
    padding-left: 1.5em;
}
.row.citizen_services {
    border-bottom: 1px solid #ddd;
    padding-bottom: 1.5em;
    
}
.row.citizen_services ul li:hover a {
    color: #3674a4;
}
.row.mt-4.citizen_services ul li {
    transition: 0.8s ease-in-out;
}
.row.citizen_services ul li:hover {
    padding-left: 0.8em;
    transition: 0.8s ease-in-out;
}
.bg_light{background-color: #f9f9f9;}

.treeStructure-insturctions {
    background-color: #fff;
    padding: 10px;
    box-shadow: 0 5px 10px rgba(0, 0, 0, .1);float: left;
}
.treeStructure-insturctions ul li {
    padding: 8px 0px;
    border-bottom: 1px dashed #3674a4;
    font-size: 16px;
    font-weight: 400;
    color: #2d2d2d;display: flex;
    align-items: center;

}
.treeStructure-insturctions span.val-txt {
    text-align: left;
    float: left;
    color: #3674a4;
    font-weight: 500;
}
.treeStructure-insturctions ul li > span:first-child {
    min-width: 65px;
}
.treeStructure-insturctions ul li:last-child {
    border: 0;
}

/*data table css*/
#table_wrapper thead > tr >th {
    font-weight: 400 !important;
}

#table_wrapper tr > td > a {
    color: #212529 !important;
}

    </style>
    
<script>
	$(document).ready(function () {
		$('#incfont').click(function () {
			//  debugger;
			curSize = parseInt($('.main_wrapper').css('font-size')) + 2;
            console.log(curSize);
			if (curSize <= 22)
				$('.main_wrapper').css('font-size', curSize);
		});
		$('#deffont').click(function () {
			// debugger;
			curSize = 14; //parseInt($('.accessibilityTxt').css('font-size')) + 2;
			//if (curSize <= 22)
			$('.main_wrapper').css('font-size', curSize);
		});
		$('#decfont').click(function () {
			curSize = parseInt($('.main_wrapper').css('font-size')) - 2;
			if (curSize >= 8)
				$('.main_wrapper').css('font-size', curSize);
		});
	});
</script>   

</head>

<body>
<div class="main_wrapper">
 <div class="top-strip">
        <div class="container">  
            <div class="row">
                <div class="col-sm-3">
                    <!--<a class="data-Now" href="#">17 February 2020</a>-->

                   <!--  <ul class="float-left left-nav accessibilityTxt">
                        <li><a href="#complain-wrapper">Skip to main content |</a> </li>

                    <!-- <ul class="float-left left-nav accessibilityTxt">
                        <li><a href="#complain-wrapper">Skip to main content |</a> </li>
                        <li><a href="<?php echo base_url('home/sitemap');?>">Sitemap</a></li>

                    </ul> -->
                </div>
                <div class="col-sm-9">          
                    <?php if($this->session->userdata('user_id')!= ""){ ?>
                    <ul class="right-nav accessibilityTxt">
                        <li> 
                            <div class="dropdown">
                                <a href="#" class="profile_link dropdown-toggle"  data-toggle="dropdown"><?php echo $this->session->userdata('uniqueusername');  ?></a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="<?php echo base_url('userprofile/profile');?>">Profile</a>
                                    <a class="dropdown-item" href="<?php echo base_url('userprofile/change_pass');?>">Change Password</a>
                                    <a class="dropdown-item" href="<?php echo base_url('userlogin/logout');?>">Logout</a>
                                    <!--<a class="dropdown-item" href="#">View Profile</a>
                                    <a class="dropdown-item" href="#">Link 3</a>-->
                                </div>
                            </div>
                        </li>
                    </ul>
					<?php  } ?>
                    <ul class="quick-nav accessibilityTxt">

                        <li> <a href="#complain-wrapper">Skip to main content |</a> </li>                       
                       
                        <li> <a href="<?php echo base_url('home/sitemap');?>">Sitemap |</a></li>
                       <!-- <li> <a href="#" class="eng-lang">English </a></li> -->

 
                        <li> <a href="#" class="eng-lang">English |</a></li>
                        

                         <?php if ($this->session->userdata('user_id')=='' ){?>
                        <li> <a href="<?php echo base_url('userlogin');?>">Login |</a></li>
                        <?php } ?>                        
                         <?php if ($this->session->userdata('user_id')=='' ){?>
                        <li> <a href="<?php echo base_url('userlogin/registration');?>">Register |</a></li>
                        <?php } ?>
						 
						<li> <a href="javascript:void(0);" id="incfont"> A+ |</a> </li>
						<li> <a href="javascript:void(0);" id="deffont">A |</a> </li>
						<li> <a href="javascript:void(0);" id="decfont">A- |</a> </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- center logo -->
    <div class="centerLogo text-center d-flex justify-content-center">
        <!-- <div class="beta-version">
            <img src="<?php echo base_url();?>img/beta-version.png" class="img-fluid">
         </div> -->
        <div class="logo-place align-self-center">
            <span class="logo-left">
                <img src="<?php echo base_url();?>img/emblemofindia.png" class="img-fluid">
            </span>
        </div>
        <div class="logo-label align-self-center">
            <!-- <h1>Department of Registration &amp; Stamps</h1>
            <h3>Government of Maharashtra</h3> -->

             <h3>Department of Registration and Stamps, Govt of Maharashtra</h3>

             
        </div>
        <div class="logo-place align-self-center">
            <span class="logo-right">
                <img src="<?php echo base_url();?>img/IGRlogo.png" class="img-fluid">
            </span>
        </div>
    </div>
    <!-- //center logo -->

    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <!-- <ul class="navbar-nav ml-auto accessibilityTxt">
                    <li class="nav-item dropdown ">
                        <a class="nav-link" href="<?php echo base_url();?>">
                            Home<span class="sr-only">(current)</span>
                        </a>
                    </li>
					<li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('organisation');?>">Organization</a>
                    </li>
					<li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('publication');?>">Publications</a>
                    </li>
					<li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('imp_links');?>">Important Links</a>
                    </li>
					<li class="nav-item">
                       <a class="nav-link" target="_blank" href="http://igrmahhelpline.gov.in/dashboard.php">FAQ's</a>
                    </li>
					<li class="nav-item ">
                        <a class="nav-link" href="<?php echo base_url('home/grievance_machanism');?>">
                        Grievance Redressal Mechanism</a>
                    </li>
                    
					<?php if($this->session->userdata('user_id') == ""){ ?>
					<li class="nav-item ">
                        <a class="nav-link" href="<?php echo base_url('superadminlogin/suplogin');?>">
                        Department Login</a>
                    </li>
					<?php } else { ?>
					<li class="nav-item ">
                        <a class="nav-link" href="<?php echo base_url('complaint');?>">
                        View Complaints</a>
                    </li>
					<?php } ?>
					
                </ul> -->

                  <ul class="navbar-nav ml-auto accessibilityTxt">
                    <?php
                     $url=$this->uri->segment(1); 
                    $url2=$this->uri->segment(2);
                    ?>
                    <li class="nav-item dropdown ">
                        <a class="nav-link <?php echo ($url=='') ? 'active' : '' ?>" href="<?php echo base_url();?>">
                            Home<span class="sr-only">(current)</span>
                        </a>
                    </li>

                    <li class="nav-item ">
                        <a class="nav-link <?php echo ($url=='organisation') ? 'active' : '' ?>" href="<?php echo base_url('organisation');?>">Organization</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo ($url=='publication') ? 'active' : '' ?>" href="<?php echo base_url('publication');?>">Publications</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo ($url=='imp_links') ? 'active' : '' ?>" href="<?php echo base_url('imp_links');?>">Important Links</a>
                    </li>
                    <li class="nav-item">
                       <a class="nav-link" target="_blank" href="http://igrmahhelpline.gov.in/dashboard.php">FAQ's</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link <?php echo ($url2=='grievance_machanism') ? 'active' : '' ?>" href="<?php echo base_url('home/grievance_machanism');?>">
                        Grievance Redressal Mechanism</a>
                    </li>
                    
                    <?php if($this->session->userdata('user_id') == ""){ ?>
                    <li class="nav-item ">
                        <a class="nav-link <?php echo ($url2=='suplogin') ? 'active' : '' ?>" href="<?php echo base_url('superadminlogin/suplogin');?>">
                        Department Login</a>
                    </li>
                    <?php } else { ?>
                    <li class="nav-item ">
                        <a class="nav-link <?php echo ($url=='complaint') ? 'active' : '' ?>" href="<?php echo base_url('complaint');?>">
                        View Complaints</a>
                    </li>
                    <?php } ?>
                    
                    
                </ul>
            </div>
        </div>
    </nav>
    <!-- //navigation -->