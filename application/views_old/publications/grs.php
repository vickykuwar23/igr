<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                        <ul class="aside-list">							
                            <li><a href="<?php echo base_url('publication/acts'); ?>" <?php if($subtitle == "Acts"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Acts</a></li>
                            <li><a href="<?php echo base_url('publication/schedules'); ?>" <?php if($subtitle == "Schedules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Schedules</a></li>
                            <li><a href="<?php echo base_url('publication/rules'); ?>" <?php if($subtitle == "Rules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Rules</a></li>
                            <li><a href="<?php echo base_url('publication/notifications'); ?>" <?php if($subtitle == "Notifications"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Notifications</a></li>
                            <li><a href="<?php echo base_url('publication/grs'); ?>" <?php if($subtitle == "GRS"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>GRs</a></li>
                            <li><a href="<?php echo base_url('publication/circulars'); ?>" <?php if($subtitle == "Circulars"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Circulars</a></li>
							<li><a href="<?php echo base_url('publication/asr'); ?>" <?php if($subtitle == "ASR"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>ASR Guidelines</a></li>
                            <!--<li><a href="<?php echo base_url('publication/fee_structure'); ?>" <?php if($subtitle == "Fee Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Fee Structure</a></li>
                             <li><a href="<?php echo base_url('publication/reports'); ?>" <?php if($subtitle == "Reports"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Reports</a></li>
                              <li><a href="<?php echo base_url('publication/dept_exam_results'); ?>" <?php if($subtitle == "Department Exam Results"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Department Exam Results</a></li>-->
                       </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3 mb-3">				
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('publication'); ?>">Publications</a></li>
					<li class="breadcrumb-item active" aria-current="page">GRs</li>
				  </ol>
				</nav>
                    <div id="accordion">
						<div class="card">
							<div class="card-header" id="headingOne">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								  Registration
								</button>
							  </h5>
							</div>
							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							  <div class="card-body">
								<p>
								<div>
										<table class="table">


											<tbody><tr>
												<th>Sr.No</th>
												<th>Subject</th>
												<th>Date</th>
												<th>Number</th>
												<th>Download</th>
											</tr>


											<tr>

												<td>1
												</td>
												<td>Abolishing of Section 22A
												</td>
												<td>8/11/2006
												</td>
												<td>Mudrank 2001/276/Case No 102/M-1
												</td>
												<td class="text-center">

													<a href="<?php echo base_url(); ?>pdf/2_Abolishing.pdf" target="_blank">
														<i class="fa fa-download"></i></a>
												</td>

											</tr>

											<tr>
												<td>2
												</td>
												<td>Document Handling charges
												</td>
												<td>20/12/2001
												</td>
												<td>Nondani/2006/case no 417/M-1
												</td>
												<td class="text-center">
													<a href="<?php echo base_url(); ?>pdf/aboutPdf.pdf" target="_blank">
														<i class="fa fa-download"></i></a>
												</td>
											</tr>

											   <tr>
												<td>3
												</td>
												<td>Aadhar Authentication Gazzzet 
												</td>
												<td>09/04/2019
												</td>
												<td class="text-center">-
												</td>
												<td class="text-center">
													<a href="<?php echo base_url(); ?>pdf/3_Aadhar_Verifcation_Gazzett.pdf" target="_blank">
														<i class="fa fa-download"></i></a>
												</td>
											</tr>


										</tbody>
										</table>
										
										
									</div>
								</p>								
							 </div>
							</div>
						 </div>
						<div class="card">
							<div class="card-header" id="headingTwo">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
								  Stamps
								</button>
							  </h5>
							</div>
							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							  <div class="card-body">							
								<p>
									<table class="table">
										<tbody><tr>
											<th style="text-align: center;">अ.क्र.&nbsp;</th>
											<th style="text-align: center;">विषय </th>
											<th style="text-align: center;">दिनांक</th>
											<th style="text-align: center;">संदर्भ </th>
											<th style="text-align: center;">Download</th>
										</tr>

										<tr>
											<td>1</td>
											<td>मुंबई पोर्ट अधिकारी योग्य प्राधिकारी घेाषीत </td>
											<td>07/02/2004</td>
											<td>मु 2004/3538/(03)प्र क्र 58/म-1</td>
											<td class="text-center">

												<a href="<?php echo base_url(); ?>pdf/1-Order.pdf" target="_blank">
													<i class="fa fa-download"></i>
												</a>
											</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Franking</td>
											<td>15/03/2014</td>
											<td>D-/STP/CASE No. 6/14/106/2014</td>
											<td class="text-center">
												<a href="<?php echo base_url(); ?>pdf/No_Franking_above_5000.pdf" target="_blank">
													<i class="fa fa-download"></i>
												</a>
											</td>
										</tr>
										<tr>
											<td>3</td>
											<td>500 पेक्षा कमी वसुलीची प्रकरणे निर्लेखित</td>
											<td>18/01/2007</td>
											<td>शा.नि.क्र. लोलस06/3184/प्रक्र 471/म1</td>
											<td class="text-center">
												<a href="<?php echo base_url(); ?>pdf/2-Gov.pdf" target="_blank">
													<i class="fa fa-download"></i>
												</a>
											</td>
										</tr>
										<tr>
											<td>4</td>
											<td>संस्थानिक संस्था कर लागू बाबत </td>
											<td>05/04/2011</td>
											<td>स्था सं क 2011/प्र क्र /33/11/नवि - 34</td>
											<td class="text-center">
												<a href="<?php echo base_url(); ?>pdf/3-G2011.pdf" target="_blank">
													<i class="fa fa-download"></i>
												</a>
											</td>
										</tr>
										<tr>
											<td>5</td>
											<td>भाडेपट्टयाने व कब्जेहक्काने प्रदान केलेल्या शासकीय जमिनी कर्ज उभारण्यासाठी वित्तीय संस्थाकडे तारण ठेवण्यास परवानगी देण्याबबात </td>
											<td>02/01/2012</td>
											<td>शा नि जमीन 11/2011/प्र क्र 166/ ज 1 </td>
											<td class="text-center">
												<a href="<?php echo base_url(); ?>pdf/4-Order.pdf" target="_blank">
													<i class="fa fa-download"></i>
												</a>
											</td>
										</tr>
										<tr>
											<td>6</td>
											<td>सामूहिक प्रोत्साहन योजना 2007 मध्ये सुधारणा</td>
											<td>31/01/2012</td>
											<td>शनि पीएसआय 1707/सीआर 50/उदयोग8</td>
											<td class="text-center">
												<a href="<?php echo base_url(); ?>pdf/5-Order.pdf" target="_blank">
													<i class="fa fa-download"></i>
												</a>
											</td>
										</tr>
										<tr>
											<td>7</td>
											<td>अभिनिर्णय प्रकरणबाबत मु जि अंमलबजावणी 1 व 2 यांना प्राधिकृत करणेबाबत</td>
											<td>30/08/2013</td>
											<td>आस्थापना 2012/480/प्रक्र 182/म-1</td>
											<td class="text-center">
												<a href="<?php echo base_url(); ?>pdf/6-Order.pdf" target="_blank">
													<i class="fa fa-download"></i>
												</a>
											</td>
										</tr>
									</tbody>
									</table>
								</p>	
							 </div>
							</div>
						 </div>	
						 <div class="card">
							<div class="card-header" id="headingThree">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
								  Valuation
								</button>
							  </h5>
							</div>
							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>pdf/GR-Valuation-29-04-2008.pdf" target="_blank">शासकीय जमीन विविध प्रयोजनासाठी देताना तसेच शासकीय जमिनीचे मूल्यांकन करताना वार्षिक बाजारमूल्य दर तक्त्यानुसार मूल्यांकन करणेबाबत 29-04-2008</a></p>
								
							 </div>
							</div>
						 </div>
						  
					</div>
                </div>
            </div>
        </div>
    </section>





