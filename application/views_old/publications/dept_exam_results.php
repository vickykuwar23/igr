<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                        <ul class="aside-list">							
                              <li><a href="<?php echo base_url('publication/acts'); ?>" <?php if($subtitle == "Acts"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Acts</a></li>
                            <li><a href="<?php echo base_url('publication/schedules'); ?>" <?php if($subtitle == "Schedules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Schedules</a></li>
                            <li><a href="<?php echo base_url('publication/rules'); ?>" <?php if($subtitle == "Rules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Rules</a></li>
                            <li><a href="<?php echo base_url('publication/notifications'); ?>" <?php if($subtitle == "Notifications"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Notifications</a></li>
                            <li><a href="<?php echo base_url('publication/grs'); ?>" <?php if($subtitle == "GRS"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>GRs</a></li>
                            <li><a href="<?php echo base_url('publication/circulars'); ?>" <?php if($subtitle == "Circulars"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Circulars</a></li>
							<li><a href="<?php echo base_url('publication/asr'); ?>" <?php if($subtitle == "ASR"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>ASR Guidelines</a></li>
                            <li><a href="<?php echo base_url('publication/fee_structure'); ?>" <?php if($subtitle == "Fee Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Fee Structure</a></li>
                             <li><a href="<?php echo base_url('publication/reports'); ?>" <?php if($subtitle == "Reports"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Reports</a></li>
                              <li><a href="<?php echo base_url('publication/dept_exam_results'); ?>" <?php if($subtitle == "Department Exam Results"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Department Exam Results</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3 mb-3">				
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('publication'); ?>">Publications</a></li>
					<li class="breadcrumb-item active" aria-current="page">Department Exam Results</li>
				  </ol>
				</nav>


                     <div id="accordion">
                          <div class="card">
                            <div class="card-header" id="headingOne">
                              <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  Department Examination Rules Amending- For granting exemption in candidates who are over aged
                                </button>
                              </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                              <div class="card-body">
                                <p><a href="<?php echo base_url(); ?>/pdf/vibhagiy_pariksha_Sut_1_03_2018.pdf" target="_blank">Vibhagiy Pariksha (1/03/2018) </a></p>
                             </div>
                            </div>
                         
                          </div>

                          <div class="card">
                            <div class="card-header" id="headingTwo">
                              <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                  Departmental Examination 2019 results
                                </button>
                              </h5>
                            </div>

                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                              <div class="card-body">
                                <p><a href="<?php echo base_url(); ?>/pdf/Dept_exam_2019_result.pdf" target="_blank">Departmental Examination 2019 results </a></p>
                             </div>
                            </div>
                         
                          </div>

                          <div class="card">
                            <div class="card-header" id="headingThree">
                              <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                  Departments Exam Results of Junior clerk 1981 to 2015
                                </button>
                              </h5>
                            </div>

                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                              <div class="card-body">
                                <p><a href="<?php echo base_url(); ?>/pdf/1981 September.pdf" target="_blank">1981 September</a></p>
                                <p><a href="<?php echo base_url(); ?>/pdf/1982 September.pdf" target="_blank">1982 September</a></p>
                                <p><a href="<?php echo base_url(); ?>/pdf/1983 October .pdf" target="_blank">1983 October</a></p>
                                <p><a href="<?php echo base_url(); ?>/pdf/1984 September.pdf" target="_blank">1984 September</a></p>
                                <p><a href="<?php echo base_url(); ?>/pdf/1985 October .pdf" target="_blank">1985 October</a></p>
                                <p><a href="<?php echo base_url(); ?>/pdf/1986 September .pdf" target="_blank">1986 September</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/1987 October .pdf" target="_blank">1987 October</a></p>
                               
                                <p><a href="<?php echo base_url(); ?>/pdf/1988 September.pdf" target="_blank">1988 September</a></p>
                               
                                <p><a href="<?php echo base_url(); ?>/pdf/1989 Septermber.pdf" target="_blank">1989 Septermber</a></p>
                               
                                <p><a href="<?php echo base_url(); ?>/pdf/1990 September.pdf" target="_blank">1990 September</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/1991 October .pdf" target="_blank">1991 October</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/1992 September.pdf" target="_blank">1992 September</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/1993 November .pdf" target="_blank">1993 November</a></p>
                               
                                <p><a href="<?php echo base_url(); ?>/pdf/1994 October .pdf" target="_blank">1994 October</a></p>
                               
                                <p><a href="<?php echo base_url(); ?>/pdf/1995 November.pdf" target="_blank">1995 November</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/1996 November.pdf" target="_blank">1996 November</a></p>
                               
                                <p><a href="<?php echo base_url(); ?>/pdf/1997 November .pdf" target="_blank">1997 November</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/1998 April .pdf" target="_blank">1998 April</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/1999 May.pdf" target="_blank">1999 May</a></p>
                               
                                <p><a href="<?php echo base_url(); ?>/pdf/2002 May.pdf" target="_blank">2002 May</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/2005 September.pdf" target="_blank">2005 September</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/2007 April.pdf" target="_blank">2007 April</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/2009%20 December .pdf" target="_blank">2009 December</a></p>
                               
                                <p><a href="<?php echo base_url(); ?>/pdf/2011 August .pdf" target="_blank">2011 August</a></p>
                                
                                <p><a href="<?php echo base_url(); ?>/pdf/2015 April .pdf" target="_blank">2015 April</a></p>
                             </div>
                            </div>
                         
                          </div>

                          
                         
                          <div class="card">
                            <div class="card-header" id="headingFour">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                  Departments Exam Results of Stamp Sellers Clerk
                                </button>
                              </h5>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                              <div class="card-body">
                                <p><a href="<?php echo base_url(); ?>/pdf/1997December.pdf" target="_blank">1997 कलि व मुद्रांक विक्रते डीसेबर</a></p>
                               
                                <p><a href="<?php echo base_url(); ?>/pdf/1998November.pdf" target="_blank">1998 कलि च मुद्रांक विक्रता नोव्हेबर</a></p>
                                
                             </div>
                            </div>
                          </div>
                          <div class="card">
                            <div class="card-header" id="headingFive">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                  Departments Exam Results of SRO Grade - 2
                                </button>
                              </h5>
                            </div>
                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                              <div class="card-body">
                                <p><a href="<?php echo base_url(); ?>/pdf/1980Sept.pdf" target="_blank">1980 परिवीक्षाधीन दु नि लिपीक यांची सष्टेबर</a></p>
                                <p><a href="<?php echo base_url(); ?>/pdf/1994Feb.pdf" target="_blank">1994 परिक्षाधीन दु नि यांची फेबुवारी</a></p>
                                <p><a href="<?php echo base_url(); ?>/pdf/1994June.pdf" target="_blank">1994 परिवीक्षाधीन जुन</a></p> 
                                <p><a href="<?php echo base_url(); ?>/pdf/1995SROJale.pdf" target="_blank">1995 परिवीक्षाधीन दु नि जले</a></p>   
                            </div>
                            </div>
                          </div>

                          <div class="card">
                            <div class="card-header" id="headingSix">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                  Departments Exam Results of SRO Grade - 2
                                </button>
                              </h5>
                            </div>
                            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                              <div class="card-body">

                                <p><a href="<?php echo base_url(); ?>/pdf/class29jan2007.pdf" target="_blank">वर्ग-2 अधिका-यांची वि.प. निकालपत्र दि. 09-Jan-2007</a></p>
                                <p><a href="<?php echo base_url(); ?>/pdf/class216May2007.pdf" target="_blank">वर्ग-2 अधिका-यांची वि.प.निकालपत्र दि. 16-May-2007</a></p>
                                <p><a href="<?php echo base_url(); ?>/pdf/class220Jan2010.pdf" target="_blank">वर्ग-2 अधिका-यांची वि.प.निकालपत्र दि. 20-Jan-2010</a></p> 
                            </div>
                            </div>
                          </div>

                          <div class="card">
                            <div class="card-header" id="heading7">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                  Departments Exam Results of Junior clerk 1981 to 2016
                                </button>
                              </h5>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                              <div class="card-body">

                                <p><a href="<?php echo base_url(); ?>/pdf/2005_Result_corrigendum.pdf" target="_blank">2005 Result corrigendum</a></p>
                           
                            </div>
                            </div>
                          </div>

                          

                                                    
                        </div>
                </div>
            </div>
        </div>
    </section>





