<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                        <ul class="aside-list">							
                            <li><a href="<?php echo base_url('publication/acts'); ?>" <?php if($subtitle == "Acts"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Acts</a></li>
                            <li><a href="<?php echo base_url('publication/schedules'); ?>" <?php if($subtitle == "Schedules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Schedules</a></li>
                            <li><a href="<?php echo base_url('publication/rules'); ?>" <?php if($subtitle == "Rules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Rules</a></li>
                            <li><a href="<?php echo base_url('publication/notifications'); ?>" <?php if($subtitle == "Notifications"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Notifications</a></li>
                            <li><a href="<?php echo base_url('publication/grs'); ?>" <?php if($subtitle == "GRS"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>GRs</a></li>
                            <li><a href="<?php echo base_url('publication/circulars'); ?>" <?php if($subtitle == "Circulars"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Circulars</a></li>
							<li><a href="<?php echo base_url('publication/asr'); ?>" <?php if($subtitle == "ASR"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>ASR Guidelines</a></li>
                            <!--<li><a href="<?php echo base_url('publication/fee_structure'); ?>" <?php if($subtitle == "Fee Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Fee Structure</a></li>
                             <li><a href="<?php echo base_url('publication/reports'); ?>" <?php if($subtitle == "Reports"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Reports</a></li>
                              <li><a href="<?php echo base_url('publication/dept_exam_results'); ?>" <?php if($subtitle == "Department Exam Results"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Department Exam Results</a></li>-->
                       </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3">
                    <ul class="instruction-points">						
                        <li><a href="<?php echo base_url(); ?>pdf/Mumabi_2017.pdf">1. ASR Guidelines for Mumbai (Marathi) 2017-18</a></li>
                        <li><a href="<?php echo base_url(); ?>pdf/guidelines_2017.pdf">2. ASR Guidelines for Rest of Maharashtra (Marathi) 2017-18</a></li>
                        <li><a href="<?php echo base_url(); ?>pdf/ASR2016-17.pdf">3. ASR Guidelines for Mumbai (Marathi) 2016-17</a></li>
                        <li><a href="<?php echo base_url(); ?>pdf/rest_maha_2016.pdf">4. ASR Guidelines Circular for rest of Maharashtra 2016-17</a></li>
                        <li><a href="<?php echo base_url(); ?>pdf/Bazar_Mulya_dar_takta_suchana_mumbai_(marathi).pdf">5. बाजार मूल्य दर तक्ता सूचना मुंबई (मराठी)</a></li>
                        <li><a href="<?php echo base_url(); ?>pdf/Bazar_Mulya_dar_takta_suchana_urvarit_maharashtra_(marathi).pdf">6. बाजार मूल्य दर तक्ता सूचना उर्वरित महाराष्ट्र (मराठी)</a></li>
                        <li><a href="<?php echo base_url(); ?>pdf/ASR_Mumbai_2015.pdf">7. ASR Guidelines-2015 Mumbai (Marathi)</a></li>
                        <li><a href="<?php echo base_url(); ?>pdf/ASR_Rest_of_Mah_2015.pdf">8. ASR Guidelines-2015 Rest of Maharashtra (Marathi)</a></li>
						<li><a href="<?php echo base_url(); ?>pdf/MumbaiCR.pdf">9. Mumbai Construction Rate</a></li>
						<li><a href="<?php echo base_url(); ?>pdf/rest_of_mah_CR.pdf">10. Rest of Maharashtra Construction Rate</a></li>
						<li><a href="<?php echo base_url(); ?>pdf/1_ ASR.pdf">11. ASR Guidelines-2013 Mumbai (Marathi)</a></li>
						<li><a href="<?php echo base_url(); ?>pdf/2_ ASR.pdf">12. ASR Guidelines-2013 Rest of Maharashtra (Marathi)</a></li>
						<li>
						<div id="accordion">
						<div class="card">
							<div class="card-header" id="headingOne">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								 13. List of Villages in influenced Area
								</button>
							  </h5>
							</div>
							<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>/pdf/3_ASR_VillageList_Amravati.pdf" target="_blank">Amravati</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/3_ASR_VillageList_Aurangabad.pdf" target="_blank">Aurangabad</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/3_ASR_VillageList_Kokan.pdf" target="_blank">Kokan Division</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/3_ASR_VillageList_Nagpur.pdf" target="_blank">Nagpur</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/3_ASR_VillageList_Nasik.pdf" target="_blank">Nasik</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/3_ASR_VillageList_Pune.pdf" target="_blank">Pune</a></p>
							 </div>
							</div>
						  </div>
						 </div>
						</li>
				  </ul>
                </div>
            </div>
        </div>
    </section>





