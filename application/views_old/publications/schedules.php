<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                        <ul class="aside-list">							
                           <li><a href="<?php echo base_url('publication/acts'); ?>" <?php if($subtitle == "Acts"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Acts</a></li>
                            <li><a href="<?php echo base_url('publication/schedules'); ?>" <?php if($subtitle == "Schedules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Schedules</a></li>
                            <li><a href="<?php echo base_url('publication/rules'); ?>" <?php if($subtitle == "Rules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Rules</a></li>
                            <li><a href="<?php echo base_url('publication/notifications'); ?>" <?php if($subtitle == "Notifications"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Notifications</a></li>
                            <li><a href="<?php echo base_url('publication/grs'); ?>" <?php if($subtitle == "GRS"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>GRs</a></li>
                            <li><a href="<?php echo base_url('publication/circulars'); ?>" <?php if($subtitle == "Circulars"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Circulars</a></li>
							<li><a href="<?php echo base_url('publication/asr'); ?>" <?php if($subtitle == "ASR"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>ASR Guidelines</a></li>
                           <!--<li><a href="<?php echo base_url('publication/fee_structure'); ?>" <?php if($subtitle == "Fee Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Fee Structure</a></li>
                             <li><a href="<?php echo base_url('publication/reports'); ?>" <?php if($subtitle == "Reports"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Reports</a></li>
                              <li><a href="<?php echo base_url('publication/dept_exam_results'); ?>" <?php if($subtitle == "Department Exam Results"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Department Exam Results</a></li>-->
                       </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3 mb-3">				
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('publication'); ?>">Publications</a></li>
					<li class="breadcrumb-item active" aria-current="page">Schedule</li>
				  </ol>
				</nav>
                    <div id="accordion">
						<div class="card">
							<div class="card-header" id="headingOne">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								  Schedule
								</button>
							  </h5>
							</div>

							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>/pdf/The_Maharashtra_Act_Schedule_1_and_2.pdf" target="_blank">Schedule I of Maharashtra Stamp Act</a></p>								
							 </div>
							</div>
						  </div>  
					</div>
                </div>
            </div>
        </div>
    </section>





