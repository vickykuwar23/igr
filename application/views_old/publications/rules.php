<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                        <ul class="aside-list">							
                            <li><a href="<?php echo base_url('publication/acts'); ?>" <?php if($subtitle == "Acts"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Acts</a></li>
                            <li><a href="<?php echo base_url('publication/schedules'); ?>" <?php if($subtitle == "Schedules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Schedules</a></li>
                            <li><a href="<?php echo base_url('publication/rules'); ?>" <?php if($subtitle == "Rules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Rules</a></li>
                            <li><a href="<?php echo base_url('publication/notifications'); ?>" <?php if($subtitle == "Notifications"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Notifications</a></li>
                            <li><a href="<?php echo base_url('publication/grs'); ?>" <?php if($subtitle == "GRS"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>GRs</a></li>
                            <li><a href="<?php echo base_url('publication/circulars'); ?>" <?php if($subtitle == "Circulars"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Circulars</a></li>
							<li><a href="<?php echo base_url('publication/asr'); ?>" <?php if($subtitle == "ASR"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>ASR Guidelines</a></li>
                            <!--<li><a href="<?php echo base_url('publication/fee_structure'); ?>" <?php if($subtitle == "Fee Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Fee Structure</a></li>
                             <li><a href="<?php echo base_url('publication/reports'); ?>" <?php if($subtitle == "Reports"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Reports</a></li>
                              <li><a href="<?php echo base_url('publication/dept_exam_results'); ?>" <?php if($subtitle == "Department Exam Results"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Department Exam Results</a></li>-->
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3 mb-3">				
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('publication'); ?>">Publications</a></li>
					<li class="breadcrumb-item active" aria-current="page">Rules</li>
				  </ol>
				</nav>
                    <div id="accordion">
						  <div class="card">
							<div class="card-header" id="headingOne">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								  Registration
								</button>
							  </h5>
							</div>

							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>/pdf/mrr_v1.pdf" target="_blank">Maharashtra Registration Rules, 1961</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/mfnr.pdf" target="_blank">Maharashtra Filing of Notices & Copies of Court orders Rules, 2013</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/meregistration.pdf" target="_blank">Maharashtra e-Registration & e-Filing Rules, 2013</a></p>	
							 </div>
							</div>
						  </div>
						  <div class="card">
							<div class="card-header" id="headingTwo">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								  Stamps
								</button>
							  </h5>
							</div>
							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>/pdf/bss.pdf" target="_blank">1. Maharashtra Stamps Sale and Supply Rules 1934</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/bsr.pdf" target="_blank">2. Maharashtra Stamp Rules 1939</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/bsrr.pdf" target="_blank">3. Maharashtra Stamp Refund Rules 1963</a></p>	
								<p><a href="<?php echo base_url(); ?>/pdf/e_payment.pdf" target="_blank">4. The Maharashtra ePayment of Stamp Duty and Refund Rules 2014</a></p>	
							  </div>
							</div>
						  </div>
						  <div class="card">
							<div class="card-header" id="headingThree">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								  Valuation Of Property
								</button>
							  </h5>
							</div>
							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>/pdf/mahastamp.pdf" target="_blank">The Maharashtra Stamp (Determination of True Market Value of property) Rules, 1995</a></p>
							</div>
							</div>
						  </div>
						  <div class="card">
							<div class="card-header" id="headingFour">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
								  Marriage Registration
								</button>
							  </h5>
							</div>
							<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>/pdf/SPM-Rules1964.pdf" target="_blank">Maharashtra Special Marriage Rules, 1964</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/vivahregistration.pdf" target="_blank">Maharashtra Marriage Rules, 1999</a></p>
							</div>
							</div>
						  </div>
						</div>
                </div>
            </div>
        </div>
    </section>





