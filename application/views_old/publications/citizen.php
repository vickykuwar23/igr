<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                        <ul class="aside-list">							
                            <li><a href="<?php echo base_url('publication/acts'); ?>" <?php if($subtitle == "Acts"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Acts</a></li>
                            <li><a href="<?php echo base_url('publication/schedules'); ?>" <?php if($subtitle == "Schedules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Schedules</a></li>
                            <li><a href="<?php echo base_url('publication/rules'); ?>" <?php if($subtitle == "Rules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Rules</a></li>
                            <li><a href="<?php echo base_url('publication/notifications'); ?>" <?php if($subtitle == "Notifications"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Notifications</a></li>
                            <li><a href="<?php echo base_url('publication/grs'); ?>" <?php if($subtitle == "GRS"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>GRs</a></li>
                            <li><a href="<?php echo base_url('publication/circulars'); ?>" <?php if($subtitle == "Circulars"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Circulars</a></li>
                            <li><a href="<?php echo base_url('publication/asr'); ?>" <?php if($subtitle == "ASR"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>ASR Guidelines</a></li>
                            <!--<li><a href="<?php echo base_url('publication/fee_structure'); ?>" <?php if($subtitle == "Fee Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Fee Structure</a></li>
                             <li><a href="<?php echo base_url('publication/reports'); ?>" <?php if($subtitle == "Reports"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Reports</a></li>
                              <li><a href="<?php echo base_url('publication/dept_exam_results'); ?>" <?php if($subtitle == "Department Exam Results"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Department Exam Results</a></li>-->
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3">
					<p>Click following links to view the PDF of Citizen's Charter..</p>
                    <ul class="instruction-points">						
                        <li><a href="<?php echo base_url(); ?>pdf/citizen_charter_marathi.pdf" target="_blank">मराठी</a></li>
                        <li><a href="<?php echo base_url(); ?>pdf/Citizen_Charter_English.pdf" target="_blank">English</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>





