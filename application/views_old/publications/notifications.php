<section class="asidebar-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="aside">
                       <ul class="aside-list">							
                            <li><a href="<?php echo base_url('publication/acts'); ?>" <?php if($subtitle == "Acts"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Acts</a></li>
                            <li><a href="<?php echo base_url('publication/schedules'); ?>" <?php if($subtitle == "Schedules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Schedules</a></li>
                            <li><a href="<?php echo base_url('publication/rules'); ?>" <?php if($subtitle == "Rules"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Rules</a></li>
                            <li><a href="<?php echo base_url('publication/notifications'); ?>" <?php if($subtitle == "Notifications"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Notifications</a></li>
                            <li><a href="<?php echo base_url('publication/grs'); ?>" <?php if($subtitle == "GRS"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>GRs</a></li>
                            <li><a href="<?php echo base_url('publication/circulars'); ?>" <?php if($subtitle == "Circulars"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Circulars</a></li>
							<li><a href="<?php echo base_url('publication/asr'); ?>" <?php if($subtitle == "ASR"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>ASR Guidelines</a></li>
                            <!--<li><a href="<?php echo base_url('publication/fee_structure'); ?>" <?php if($subtitle == "Fee Structure"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Fee Structure</a></li>
                             <li><a href="<?php echo base_url('publication/reports'); ?>" <?php if($subtitle == "Reports"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Reports</a></li>
                              <li><a href="<?php echo base_url('publication/dept_exam_results'); ?>" <?php if($subtitle == "Department Exam Results"): ?> class="active-list" <?php endif; ?>><i class="fa fa-fw"></i>Department Exam Results</a></li>-->
                       </ul>
                    </div>
                </div>
                <div class="col-md-9 mt-3 mb-3">				
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('publication'); ?>">Publications</a></li>
					<li class="breadcrumb-item active" aria-current="page">Notifications</li>
				  </ol>
				</nav>
                    <div id="accordion">
						<div class="card">
							<div class="card-header" id="headingOne">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								  Registration
								</button>
							  </h5>
							</div>
							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							  <div class="card-body">
								<p>1. Sub Registrar Jurisdiction- Under Sec 5</p>								
							 </div>
							</div>
						 </div>
						<div class="card">
							<div class="card-header" id="headingTwo">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
								  Stamps
								</button>
							  </h5>
							</div>
							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							  <div class="card-body">
								<p>Pradhan Mantri Awas Yojana</p>
								<p>
									<div>
										<table class="table">
											<tbody><tr>
												<th style="text-align: center;">Sr.No&nbsp;</th>
												<th style="text-align: center;">Subject</th>
												<th style="text-align: center;">Date</th>
												<th style="text-align: center;">Download</th>
											</tr>

											<tr>
												<!-- sr no-->
												<td>1</td>

												<!-- title-->
												<td>Pradhan Mantri Awas Yojna</td>

												<!-- Date-->
												<td>20.02.2019</td>

												<!-- Link-->
												<td class="text-center">
													<a href="<?php echo base_url(); ?>/pdf/Pradhan_Mantri_Awas_Yojana_20.02.2019.pdf" target="_blank"><i class="fa fa-download"></i></a>
												</td>
											</tr>

											<tr>
												<!-- sr no-->
												<td>2</td>

												<!-- title-->
												<td>Pradhan Mantri Awas Yojna</td>

												<!-- Date-->
												<td>31.03.2018</td>

												<!-- Link-->
												<td class="text-center">
													<a href="<?php echo base_url(); ?>/pdf/Pradhan_Mantri_Awas_Yojana_31.03.2018.pdf" target="_blank"><i class="fa fa-download"></i></a>
													</td>
											</tr>

											<tr>
												<!-- sr no-->
												<td>3</td>

												<!-- title-->
												<td>Pradhan Mantri Awas Yojna</td>

												<!-- Date-->
												<td>01.12.2016</td>

												<!-- Link-->
												<td class="text-center">
													<a href="<?php echo base_url(); ?>/pdf/Pradhan_Mantri_Awas_Yojana_1.12.2016.pdf" target="_blank"><i class="fa fa-download"></i></a>
													</td>
											</tr>

										</tbody></table> <br />
										
										<table cellspacing="0" cellpadding="4" class="table">
		<tbody>
		<tr>
			<th scope="col">Sr. No</th><th scope="col">Date</th><th scope="col">Subject</th><th scope="col">Subject</th><th scope="col" abbr="Download">Download</th>
		</tr>
		<tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                1
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_0" style="font-size:Small;">14/08/2019</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_0" style="font-size:Small;">PSI 2013 Notification Dt. 14/08/2019                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_0" style="font-size:Small;">-                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;">
							<a href="<?php echo base_url(); ?>/pdf/PSI_2013-Dt_14082019.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                2
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_1" style="font-size:Small;">06/01/2015</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_1" style="font-size:Small;">शासन आदेश-महसूल व वन विभाग मंत्रालय, मुंबई-३२ दि.०६ जानेवारी, २०१५ बाबत                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_1" style="font-size:Small;">-                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/pdf2.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                3
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_2" style="font-size:Small;">30/09/2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_2" style="font-size:Small;">खरे बाजारमुल्य निश्चितीबाबत                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_2" style="font-size:Small;">मु 2012/692/ प्र क्र 237/म -1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/notificationofstamp.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                4
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_3" style="font-size:Small;">30/08/2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_3" style="font-size:Small;">अभिनिर्णय प्रकरणबाबत मु जि अंमलबजावणी 1 व 2 यांना प्राधिकृत करणेबाबत                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_3" style="font-size:Small;">क्र 2012/480/प्रक्र 182/म 1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/12notification.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                5
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_4" style="font-size:Small;">24/07/2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_4" style="font-size:Small;">मुद्रांक शुल्काचा भरणा इलेक्ट्रॉनिक पध्दतीने करणेबाबत                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_4" style="font-size:Small;">मु 2012/30/ प्रक्र 18/म-1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/11notification.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                6
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_5" style="font-size:Small;">01/09/2012</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_5" style="font-size:Small;">अनु 25da करीता मुद्रांक जिल्हाधिकारी घेाषीत करेणबाबत                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_5" style="font-size:Small;">का 5/ मुद्रांक/ 2012/प्रक्र 12/ 870/2012                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/10-Notification1Nov2012.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                7
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_6" style="font-size:Small;">15/07/2006</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_6" style="font-size:Small;">सयाजी उ बा खिन मे ट्रस्ट बाबत                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_6" style="font-size:Small;">मु 2004/2993/प्र क्र 507/म-1/                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/9-Notification.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                8
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_7" style="font-size:Small;">17/06/2006</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_7" style="font-size:Small;">31.3.06 ऐवजी 30.6.2006 मजकुर दाखल                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_7" style="font-size:Small;">मु 2006/प्र क्र 349/म-1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/8-Notification.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                9
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_8" style="font-size:Small;">04/06/2005</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_8" style="font-size:Small;">अनु 40, अनु54 मुद्रांक शुल्क सुट शैक्षणिक कर्ज                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_8" style="font-size:Small;">मु 2005/प्रक्र 204/म-1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/7-Notification.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                10
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_9" style="font-size:Small;">04/06/2005</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_9" style="font-size:Small;">अनु 40, अनु54 मुद्रांक शुल्क सुट शैक्षणिक कर्ज                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_9" style="font-size:Small;">मु 2005/प्रक्र 204/म-1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/7-Notification2.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr>
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                11
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_10" style="font-size:Small;">07/01/2004</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_10" style="font-size:Small;">Stamp duty exemption to Affidavits                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          </span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_10" style="font-size:Small;">Mudrank 2004/1636/pra.kra.436/M-1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/Affidiavit-Circular-English.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
							</tr>
							<tr>
								<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
													12
												</td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_11" style="font-size:Small;">07/01/2004</span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_11" style="font-size:Small;">प्रतिज्ञापत्रावर मुद्रांक शुल्क माफी                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_11" style="font-size:Small;">मुद्रांक 2004/1636/प्र.क्र.436/म-1 दिनांक 01/07/2004                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </span>
												</td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/Affidiavit-Circular-Marathi.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
							</tr><tr>
								<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
													13
												</td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_12" style="font-size:Small;">05/06/2002</span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_12" style="font-size:Small;">गृहकर्ज मुद्रांक शुल्क बाबत                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 </span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_12" style="font-size:Small;">क्र मु 2002/875/ प्र क्र 173-म1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             </span>
												</td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/1-Notification3.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
							</tr><tr>
								<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
													14
												</td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_13" style="font-size:Small;">05/06/2002</span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_13" style="font-size:Small;">गृहकर्ज मुद्रांक शुल्क बाबत                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 </span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_13" style="font-size:Small;">क्र मु 2002/875/ प्र क्र 173-म1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             </span>
												</td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/2-Notification22.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
							</tr><tr>
								<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
													15
												</td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_14" style="font-size:Small;">05/06/2002</span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_14" style="font-size:Small;">गृहकर्ज मुद्रांक शुल्क बाबत                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 </span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_14" style="font-size:Small;">क्र मु 2002/875/ प्र क्र 173-म1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             </span>
												</td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/3-Notification3.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
							</tr><tr>
								<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
													16
												</td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_15" style="font-size:Small;">05/06/2002</span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_15" style="font-size:Small;">गृहकर्ज मुद्रांक शुल्क बाबत                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 </span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_15" style="font-size:Small;">क्र मु 2002/875/ प्र क्र 173-म2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             </span>
												</td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/4-Notification4.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
							</tr><tr>
								<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
													17
												</td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblDated_16" style="font-size:Small;">05/06/2002</span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_16" style="font-size:Small;">सिकयुरीटी आफ लोन                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </span>
												</td><td align="justify" valign="middle" style="font-size:Smaller;">
													<span id="ContentPlaceHolder1_ContentPlaceHolder1_GVStamp_lblSub_16" style="font-size:Small;">क्र मु 2002/875/ प्र क्र 173-म2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             </span>
												</td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>/pdf/5-Notification5.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
							</tr>
						</tbody>
							</table>
									</div>
								</p>	
							 </div>
							</div>
						 </div>	
						 <div class="card">
							<div class="card-header" id="headingThree">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
								  Marriage
								</button>
							  </h5>
							</div>
							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							  <div class="card-body">
								<p><a href="<?php echo base_url(); ?>/pdf/NotificationOfMarriage _31Jan2001.pdf" target="_blank">Notification of Marriage Dated 31-01-2001</a></p>
								<p><a href="<?php echo base_url(); ?>/pdf/SPM1954NotificationFeeTable_23_03_2002.pdf" target="_blank">SPM 1954 - Notification fee table 23.03.2002</a></p>																
							 </div>
							</div>
						 </div>
						  <div class="card">
							<div class="card-header" id="headingFour">
							  <h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
								  Other
								</button>
							  </h5>
							</div>
							<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
							  <div class="card-body">
								<p>
								<div>
	<table class="table">
		<tbody><tr >
			<th scope="col">Sr. No</th><th scope="col">Date</th><th scope="col">Subject</th><th scope="col" abbr="Download">Download</th>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                1
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_0" style="font-size:Small;">01-07-2014</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_0" style="font-size:Small;">Abhay Yojana 2008 Order dated 2-06-2014</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/AbhayYojana2008.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                2
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_1" style="font-size:Small;">05-03-2014</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_1" style="font-size:Small;">New 23 SR Offices</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/Offices.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                3
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_2" style="font-size:Small;">02-11-2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_2" style="font-size:Small;">Clarification regarding sec 22.A </span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/Clarification.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                4
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_3" style="font-size:Small;">03-10-2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_3" style="font-size:Small;">Filing Rules under Sec 89 of Registration Act</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/Rules.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                5
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_4" style="font-size:Small;">24-07-2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_4" style="font-size:Small;">e-Payment Rules</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/e-peyment.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                6
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_5" style="font-size:Small;">25-04-2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_5" style="font-size:Small;">Circular for filing of Notice of intimation u/s 89B in SR Offices (physically)</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/89-b.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                7
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_6" style="font-size:Small;">30-03-2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_6" style="font-size:Small;">The Maharashtra e-Registration and e-Filing Rules 2013</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/Notification.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                8
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_7" style="font-size:Small;">30-03-2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_7" style="font-size:Small;">e-Registration and e-Filing Rules 2013</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/e-Registration2013.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                9
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_8" style="font-size:Small;">30-03-2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_8" style="font-size:Small;">Registration of Agreement OR filling of Notice of intimation</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/explenatorymotgage.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                10
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_9" style="font-size:Small;">13-03-2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_9" style="font-size:Small;">Maharashtra Muncipal Corporation Act</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/LBT.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                11
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_10" style="font-size:Small;">07-03-2013</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_10" style="font-size:Small;">Maharashtra Act No.  X of 2012</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/Amendment12.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                12
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_11" style="font-size:Small;">08-08-2011</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_11" style="font-size:Small;">Broadband Connectivity Permission for SR Offices Regd</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/BSNL.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                13
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_12" style="font-size:Small;">27-05-2009</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_12" style="font-size:Small;">CLR 1008 PK2 L1SEL</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/2009_CLR_1008_PK2_L1SEL.pdf" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                14
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_13" style="font-size:Small;">30-12-2002</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_13" style="font-size:Small;">Registration Fee Table Notification English version</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/NotificationENG30122002.PDF" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr><tr style="background-color:White;">
			<td align="center" valign="middle" style="font-size:X-Small;width:30px;">
                                15
                                
                            </td><td align="center" valign="middle" style="font-size:Smaller;width:110px;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblDated_14" style="font-size:Small;">30-12-2002</span>
                            </td><td align="justify" valign="middle" style="font-size:Smaller;">
                                <span id="ContentPlaceHolder1_ContentPlaceHolder1_GridView1_lblSub_14" style="font-size:Small;">Registration Fee Table Notification  Marathi</span>
                            </td><td align="center" valign="middle" style="font-size:X-Small;width:45px;"><a href="<?php echo base_url(); ?>pdf/NotificationMAR30122002.PDF" target="_blank" style="font-size:Small;"><i class="fa fa-download"></i></a></td>
		</tr>
	</tbody></table>
</div>
								</p>																
							 </div>
							</div>
						 </div>
					</div>
                </div>
            </div>
        </div>
    </section>





