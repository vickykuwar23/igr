<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Office Type
         <small>Edit</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>complaintsubtype"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  
                  <!-- main content  -->
              <form class="form-horizontal" method="post" enctype="multipart/form-data" >              
              <div class="form-group" id="complaint_sub_type_name">
                <label class="col-lg-3 control-label col-lg-offset-1">Office Type Name <span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <input type="text" name="complaint_sub_type_name"  class="form-control" id="complaint_sub_type_name" value="<?php echo $complaint_subtype_details[0]['complaint_sub_type_name'];?>">
                  <div id="error_complaint_type_name" style="color:#F00"> <?php echo form_error('complaint_sub_type_name'); ?></div>
                </div>
              </div>
        <div class="form-group" id="complaint_type_id" >
                <label class="col-lg-3 control-label col-lg-offset-1">Complaint Type Name <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <select name="complaint_type_id" id="complaint_type_id" class="form-control">
          <option value=""> -- Select --</option>
          <?php foreach($complaint_type_list as $complaintTypes){ ?>
          <option value="<?php echo $complaintTypes['complaint_type_id']; ?>" <?php if($complaintTypes['complaint_type_id'] == $complaint_subtype_details[0]['complaint_type_id']){ ?> selected="selected" <?php } ?>><?php echo $complaintTypes['complaint_type_name']; ?></option>
          <?php } ?>
        </select>
                <div id="complaint_type_id" style="color:#F00"><?php echo form_error('complaint_type_id'); ?> </div>
                </div>
              </div>
              <!--<div class="form-group" id="region" >
                <label class="col-lg-3 control-label col-lg-offset-1">Region <span style="color:red"></span></label>
                <div class="col-lg-7">
                <input type="checkbox" name="region" class="check" id="region" value="1" <?php if($complaint_subtype_details[0]['region'] == 1){ ?> checked="checked" <?php } ?>>
                <div id="response_time" style="color:#F00"><?php echo form_error('region'); ?> </div>
                </div>
              </div>
             
              <div class="form-group" id="district" >
                <label class="col-lg-3 control-label col-lg-offset-1">District <span style="color:red"></span></label>
                <div class="col-lg-7">
                <input type="checkbox" name="district" class="check" id="district" value="1" <?php if($complaint_subtype_details[0]['district'] == 1){ ?> checked="checked" <?php } ?>>
                <div id="resident_id" style="color:#F00"><?php echo form_error('district'); ?> </div>
                </div>
              </div>
              
        <div class="form-group" id="sro_offices" >
                <label class="col-lg-3 control-label col-lg-offset-1">SEO Offices <span style="color:red"></span></label>
                <div class="col-lg-7">
                <input type="checkbox" name="sro_offices" class="check" id="sro_offices" value="1" <?php if($complaint_subtype_details[0]['sro_offices'] == 1){ ?> checked="checked" <?php } ?>>
                <div id="resident_id" style="color:#F00"><?php echo form_error('sro_offices'); ?> </div>
                </div>
              </div>
        <div class="form-group" id="none" >
                <label class="col-lg-3 control-label col-lg-offset-1">None <span style="color:red"></span></label>
                <div class="col-lg-7">
                <input type="checkbox" name="none_record" class="uncheck" id="none_record" value="1" <?php if($complaint_subtype_details[0]['none_record'] == 1){ ?> checked="checked" <?php } ?>>
                <div id="none_record" style="color:#F00"><?php echo form_error('none_record'); ?> </div>
                </div>
              </div>-->
              
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
             
              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <button type="submit" name="submit" class="btn btn-success no-shadow" id="btn_submit">Update</button>
                </div>
              </div>
            </form>                  
      

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>







<script>
$(document).ready(function(){
    $(".uncheck").click(function() {
    $(".check").not(this).attr("checked", false); //uncheck all checkboxes
    $(this).attr("checked", true);  //check the clicked one
  });
  
  $(".check").click(function() {
    $(".uncheck").not(this).attr("checked", false); //uncheck all checkboxes
    $(this).attr("checked", true);  //check the clicked one
  });
});
</script>















