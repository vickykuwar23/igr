<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Pendency Report
         <small>List</small>
         <div style="float:right; padding:2px;">
              </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } ?>
				  
				<form name="frm_expr" id="frm_expr" method="post" action="<?php //echo base_url('admin/sro_panel'); ?>">
				<div class="form-group-fileds">            
					<div class="row">
						<div class="col-md-3" style="float:right;">
						<label class="col-md-12" for="exampleFormControlSelect1">&nbsp;</label>
						<div class="col-md-12">
						  <div class="form-group">
							
							<button type="submit" name="btn_export" id="btn_export" class="btn btn-warning btn-sm " value="Excel" data-toggle="tooltip" data-placement="top" title="Export To Excel" /><i class="fa fa-file-excel-o"></i></button>
							<button type="submit" name="btn_pendency_pdf" id="btn_pendency_pdf" class="btn btn-warning btn-sm"  value="PDF" data-toggle="tooltip" data-placement="top" title="Export To PDF"/><i class="fa fa-file-pdf-o"></i></button>
						 
						 </div>
						</div>
					  </div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label class="col-md-12" for="exampleFormControlSelect1" style="font-size: 12px; text-align: center;">Pending for 12 or more than 12 days</label>
							<div class="col-md-12" align="center" >
								<div style="background-color:red; color:#fff;height: 30px; width: 100px;">
									&nbsp;
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<label class="col-md-12" for="exampleFormControlSelect1" style="font-size: 12px; text-align: center;">Pending for 5 to 11 days</label>
							<div class="col-md-12" align="center" >
								<div style="background-color:#ffbf00; color:#fff; height: 30px; width: 100px;">
									&nbsp;
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<label class="col-md-12" for="exampleFormControlSelect1" style="font-size: 12px; text-align: center;">Pending for less than 4 days</label>
							<div class="col-md-12" align="center" >
								<div  style="background-color:green; color:#fff; height: 30px; width: 100px;">
									&nbsp;
								</div>
							</div>
						</div>
					</div>
              </div>
          		  
		  </form> 
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
          
            <table class="table table-striped table-bordered dt-responsive nowrap" id="table-pendency">
                   <thead>
                      <tr>
                         <th>Sr. No.</th>
                         <th>Complaint Code</th>
						 <th>Complainant’s Name</th>
                         <th>Complaint type</th>
						 <th>Reply Status</th>                          
                      </tr>
                   </thead>
                   <tbody>                      
                   </tbody>
                </table> 	         
   					

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!--<script>
$(document).ready(function(){
// Sub Type List Showing
	var type = '<?php echo $type; ?>';
	if(type == 4){
		
		var base_url = '<?php echo base_url() ?>';
		var getCtype = $("#cm_id").val();
		$.ajax({
				type: 'POST',
				url: base_url+'admin/sro_panel/getOfficesub',
				data: {office_type_id:getCtype},
				async: false,
				success: function(data){					
					$("#c_subtype").html(data);
				}
			});

	}
	
	
	if(type == 9){
		//alert(type);
		var base_url = '<?php echo base_url() ?>';
		var getCtype 	= $("#cm_id").val();
		var getdistrict = '<?php echo $getDid; ?>';
		//alert(getCtype);
		$.ajax({
				type: 'POST',
				url: base_url+'admin/reports/getOfficeslist',
				data: {office_sub_id:getCtype, district_id: getdistrict},
				async: false,
				success: function(data){					
					$("#sro_office").html(data);
				}
			});		
 	}
	
	$("#c_type").change(function(){			
		var getCtype = $(this).val();
		var base_url = '<?php echo base_url() ?>';
		if(getCtype == 1){
			$("#response-div").hide();
		} else {
			$("#response-div").show();
		}	
		
		if(getCtype != '0')
		{
			$.ajax({
				type: 'POST',
				url: base_url+'admin/sro_panel/getOfficesub',
				data: {office_type_id:getCtype},
				async: false,
				success: function(data){					
					$("#c_subtype").html(data);
				}
			});
		}
		else
		{
			$("#c_subtype").html('');
		}
	});
	
	// Office List Showing
	$("#c_subtype").change(function(){			
		var getCsubtype = $(this).val();
		var getdistrict = $("#district_id").val();
		if(getdistrict == ""){
			var getdistrict = $("#d_id").val();
		}				
		var base_url = '<?php echo base_url() ?>';		
		if(getCsubtype != '0')
		{
			$.ajax({
				type: 'POST',
				url: base_url+'admin/reports/getOfficeslist',
				data: {office_sub_id:getCsubtype, district_id: getdistrict},
				async: false,
				success: function(data){					
					$("#sro_office").html(data);
				}
			});
		}
		else
		{
			$("#sro_office").html('');
		}
	});
});
</script>-->