<?php 
error_reporting(0);
?>
<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        System Users 
         <small>List</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>subadmin/add"><button class="btn bg-primary margin " >Add  New</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
          		<table class="table table-striped table-bordered dt-responsive nowrap" id="table">
	<thead>
			<tr>
				<th class="text-center bluehd" style="width:5% !important;">
					Sr. No.
				</th>
				<th class="text-center bluehd">
					Username
				</th>
				<th class="text-center bluehd">
					Email
				</th>
				<th class="text-center bluehd">
					Role Name
				</th>
				<th class="text-center bluehd">
					Name
				</th>
				<th class="text-center bluehd">
					Region Name
				</th>
				<th class="text-center bluehd">
					District Name
				</th>
				<th class="text-center bluehd">
					SRO Office Name
				</th>
				<th class="text-center bluehd">
				  Created At
				</th>
				<th class="text-center bluehd">
				 Status
				</th>
				<th class="text-center bluehd">
					Action
				</th>
			</tr>
			</thead>
			<tbody>
		  <?php $sr=0;?>
		  <?php foreach($adminuser as $users) {
			  $role_type=$this->master_model->getRecords('gri_roles',array('role_id'=>$users['role_id']));
			  $region_name=$this->master_model->getRecords('gri_region_division',array('religion_id'=>$users['region_id']));
			  $district_name=$this->master_model->getRecords('gri_district',array('district_id'=>$users['district_id']));
			  $sro_office_name=$this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$users['sro_id']));
			  		
			 ?>
			<tr>
			<td class="text-center"><?php echo $sr+1;?></td>
			<td><?php echo $users['adminuser'];?></td>
			<td class="text-center"><?php echo $users['email'];?></td>
			<td class="text-center"><?php echo $role_type[0]['role_name'];?></td>
			<td class="text-center"><?php echo $users['namecc'];?></td>
			<td class="text-center"><?php echo $region_name[0]['region_division_name']; ?></td>
			<td class="text-center"><?php echo $district_name[0]['district_name'];?></td>
			<td class="text-center"><?php echo $sro_office_name[0]['office_name'];?></td>
			<td class="text-center"><?php echo $users['created_at'];?></td>
			<!-- <td class="text-center"><?php if($users['status'] == 1){echo 'Active';}else{echo 'Inactive';};?></td> -->
			<td>
                     
              <a href="<?php echo base_url('subadmin/change_status/').$users['id'].'/'.$users['status']; ?>"
                class="btn btn-sm confirm_change_status <?php echo($users['status']=='1' ? "btn-success" : "btn-danger"); ?>"
                 data-msg="Are you sure you want to change the status of this user?"
                >
              <?php if($users['status'] == 1){echo 'Active';}else{echo 'Inactive';};?>
              </a>
                       
         </td>
			<td class="text-center">
			<a href="<?php echo base_url();?>subadmin/edit/<?php echo $users['id'];?>" data-toggle='tooltip' data-placement='top' title='Edit'><i class="fa fa-pencil"></i></a>
			&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo base_url();?>subadmin/delete/<?php echo $users['id'];?>" data-toggle='tooltip' data-placement='top' title='Delete' 
			onclick="javascript:return deletconfirm();"><i class="ui-tooltip fa fa-trash-o"  data-original-title="Delete"></i></a>
					
			</td>
			</tr>
		   <?php $sr++?>
		   <?php }?>
			</tbody>
</table>
                                 
   		

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>





