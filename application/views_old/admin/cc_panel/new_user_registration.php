<style>
   .er-success{color:#00FF00;}
   .er-error{color:red;}
</style>
<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
         User
         <small>Add</small>
         <div style="float:right; padding:2px;">
            <a  href="<?php echo base_url(); ?>admin/callcenterpanel/users"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  <!-- main content  -->
                  <div>
                  <form  method="post">
                     <div class="form-heading">
                     </div>
                     <div class="form-grp-filed">
                        <div class="form-group row">
                           <label class="col-lg-2   control-label">Name <span style="color:red">*</span></label>
                           <div class="col-lg-5">
                              <input type="text" name="name" data-required="true" class="form-control" value="<?php echo set_value('name')?>" id="name"> 
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('name');?></div>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-lg-2  control-label">Username <span style="color:red">*</span></label>
                           <div class="col-lg-5">
                              <input type="text" name="username" data-required="true" class="form-control" value="<?php echo set_value('username')?>" id="username"> 
                              <div class="m-t m-t-mini" style="color:#F00" id="usererror"> <?php echo form_error('username');?></div>
                              <div class="m-t m-t-mini" id="usernamechk"  style="text-align:left"></div>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-lg-2  control-label">Email <span style="color:red">*</span></label>
                           <div class="col-lg-5">
                              <input type="text" name="email" class="form-control" data-required="true" data-type="email" value="<?php echo set_value('email')?>" id="email">
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('email');?></div>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-lg-2  control-label">Mobile No. <span style="color:red">*</span></label>
                           <div class="col-lg-5">
                              <input type="text" name="mobile" class="form-control" value="<?php echo set_value('mobile')?>" id="mobile">
                              <div class="m-t m-t-mini" style="color:#F00"><?php echo form_error('mobile');?></div>
                              <!--<div class="line line-dashed m-t-large"></div>-->
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-lg-2  control-label">Landline No. </label>
                           <div class="col-lg-5">
                              <input type="text" name="landline" class="form-control" value="<?php echo set_value('landline')?>" id="landline">
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('landline');?></div>
                              <!--<div class="line line-dashed m-t-large"></div>-->
                           </div>
                        </div>

                         <div class="form-group row">
                           <label class="col-lg-2  control-label">Aadhar card number </label>
                           <div class="col-lg-5">
                               <input type="text" name="aadhar_number" maxlength="16" class="form-control" value="<?php echo set_value('aadhar_number')?>">
                           <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('aadhar_number');?></div>
                              <!--<div class="line line-dashed m-t-large"></div>-->
                           </div>
                        </div>
                 
                        <div class="form-group row">
                           <label class="col-lg-2  control-label">Address Line 1 <span style="color:red">*</span></label>
                           <div class="col-lg-5">
                              <input type="text" name="address1" class="form-control" value="<?php echo set_value('address1')?>" id="address1">
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('address1');?></div>
                              <!--<div class="line line-dashed m-t-large"></div>-->
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-lg-2  control-label">Address Line 2</label>
                           <div class="col-lg-5">
                              <input type="text" name="address2" class="form-control" value="<?php echo set_value('address2')?>" id="address2">
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('address2');?></div>
                              <!--<div class="line line-dashed m-t-large"></div>-->
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-lg-2  control-label">State <span style="color:red">*</span></label>
                           <div class="col-lg-5">
                              <select class="form-control states" name="state" class="states">
                                 <option value="">Select State</option>
                                 <?php foreach($state as $states) {?>
                                 <option value="<?php echo $states['ch2']?>" <?php if($states['ch2']==set_value('state')){echo "selected='selected'";}?>><?php echo $states['ch3']?></option>
                                 <?php } ?>
                              </select>
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('state');?></div>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-lg-2  control-label">City <span style="color:red">*</span></label>
                           <div class="col-lg-5">
                              <select class="form-control" name="city" id="selectcity">
                                 <option value="">Select City</option>
                                 <?php 
                                    if($state!='')
                                     {
                                       $city= $this->master_model->getRecords('city',array('ch3'=>'21'));


                                     }
                                     if(count($city) > 0)
                                     {
                                       foreach($city as $res)
                                       {?>
                                 <option value="<?php echo $res['ch4']?>" <?php if($res['ch4']==set_value('city')){echo "selected='selected'";}?>><?php echo $res['ch4'];?></option>
                                 <?php }
                                    }?>
                              </select>
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('city');?></div>
                           </div>
                        </div>
                      
                        <div class="form-group row">
                           <label class="col-lg-2  control-label">Pincode <span style="color:red">*</span></label>
                           <div class="col-lg-5">
                              <input type="text" name="pincode"  placeholder="" class="form-control" value="<?php echo set_value('pincode')?>" id="pincode"> 
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('pincode');?></div>
                              <!--<div class="line line-dashed m-t-large"></div>-->
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-lg-2  control-label">Password <span style="color:red">*</span></label>
                           <div class="col-lg-5">
                             <input type="password" name="password"  placeholder="" class="form-control" value="<?php echo set_value('password')?>" id="password"> 
                              <div class="m-t m-t-mini" style="color:#F00"> <?php echo form_error('password');?></div>
                              <!--<div class="line line-dashed m-t-large"></div>-->
                              <input type="hidden" name="hidden_pass" id="hidden_pass" value="">
                           </div>
                        </div>

                        <div class="mb-3">
                           <div class="col-lg-7 mx-auto d-flex justify-content-center text-center">
                             
                                  <button type="submit" name="btn_reg" id="submit_btn" class="btn btn-primary">Submit</button>
                              
                           </div>
                        </div>

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                     </div>
                     
                  </form>
                </div>
                  <!-- main content end -->
               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>

<script>
   $(document).ready(function(){
     var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
     $(".states").on('change',function(){
       var datastring='stateid='+ $(this).val() + '&' + csrfName + '='+csrfHash;
       $.ajax({
           type: 'POST',
           data:datastring,
           url: "<?php echo base_url();?>index.php/userlogin/getcity",
           success: function(res){ 
             $('#selectcity').html(res);
           }
         });
     })
   
   
    $('#register_close').on('click',function(){
      $("#register_id").css("display", "block");
      });
   //to seee the existing user
      $("#username").keyup(function() {
       $("#usernamechk").html("");
        $("#usererror").html("");
       
           //gets the value of the field
           var username = $("#username").val();
           //here would be a good place to check if it is a valid email before posting to your db
           //displays a loader while it is checking the database
           //$("#usernamechk").html('<img alt="" src="/images/loader.gif" />');
      
           //here is where you send the desired data to the PHP file using ajax
           $.post("<?php echo base_url();?>index.php/userlogin/chkusername", {username:username, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'},
               function(result) {
          var regex = /^[a-z0-9]+$/i;// number pattern
            var flag=0;
          if(regex.test(username))
          {
            flag=1
          }
          else if(flag==0 && username!='')
          {
            $("#usernamechk").html("Please type Alphanumeric values only.");
            $("#usernamechk").addClass('er-error');
            $("#usernamechk").removeClass('er-success');
          }
                   if(result == 1 && flag == 1) {
                       //the user is available
                       $("#usernamechk").html("User name is available.");
            $("#usernamechk").addClass('er-success');
                    $("#usernamechk").removeClass('er-error');
          }
                   else if(result == 0 && username!=''){
                       //the user is not available
                       $("#usernamechk").html("Username is  not available.");
            $("#usernamechk").addClass('er-error');
            $("#usernamechk").removeClass('er-success');
                   }
               });
          });   
    });
</script>
<script src="<?php echo base_url('js/encrypt.js') ?>"></script>
<script>

  $(document).ready(function(){
    $("#submit_btn").on('click',function(){


      if($('#password').val() ){
        var pswd = $("#password").val();
        $("#hidden_pass").val(pswd);
        var salt = pswd.length;
        var salt1 = sha1(salt);
        var password = sha256(sha1(salt1 + pswd));
        $("#password").val(password);

        return true;
      }else{
        return false;
      }

    
    })

  
  })
</script>