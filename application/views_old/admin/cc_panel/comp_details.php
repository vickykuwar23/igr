<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Compliant
         <small>Details</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>admin/callcenterpanel/users"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  
                  <!-- main content  -->
                  
                     <div class="main padder">
      <div class="row">
         <div class="col-lg-8 m-t-large">
            <section class="panel grdbg">
               <?php if($countuser=='1'){ ?>
               <div class="block m-t-b">
                  <label class="control-label">Complaint By :</label>
                  <?php  $username=$this->master_model->getRecords('userregistration',array('user_id'=>$userinfo[0]['user_id']),'user_name');?>
                  <?php echo ucfirst($username[0]['user_name']);?>
               </div>

               <div class="block m-b">
                  <label class="control-label">SRO Offcie :</label>
                  <?php 
                     $sub=$this->master_model->getRecords('gri_subject',array('sub_id'=>$userinfo[0]['comp_subject']));
                      echo $sub[0]['subject_name'];?>
               </div>
               <div class="block m-b">
                  <label class="control-label">Complaint Message :</label>
                  <?php echo $userinfo[0]['grievence_details'];?>
               </div>
               <div class="block m-b">
                  <label class="control-label">Region :</label>
                  <?php 
                     $reg=$this->master_model->getRecords('gri_region_division',array('religion_id'=>$userinfo[0]['religion_id']));
                     echo ucfirst($reg[0]['region_division_name']);?>
               </div>

               <div class="block m-b">
                  <label class="control-label">District :</label>
                  <?php 
                      $dist=$this->master_model->getRecords('gri_district',array('district_id'=>$userinfo[0]['district_id']));
                      echo $dist[0]['district_name'];?>
               </div>

                <div class="block m-b">
                  <label class="control-label">Compiant Type :</label>
                  <?php 
                     $comp_type=$this->master_model->getRecords('gri_complaint_type',array('complaint_type_id'=>$userinfo[0]['complaint_type_id']));
                      echo $comp_type[0]['complaint_type_name'];?>
               </div>

               <div class="block m-b">
                  <label class="control-label">SRO Offcie :</label>
                  <?php 
                     $sro=$this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$userinfo[0]['sro_office_id']));
                      echo $sro[0]['office_name'];?>
               </div>
                
               
               <div class="block m-b">
                  <label class="control-label">Date of Submission : </label>
                  <?php echo date('d M, Y',strtotime($userinfo[0]['complaint_date']));?>
               </div>
               <?php 

                    $images=$this->master_model->getRecords('gri_complaint_images',array('c_id'=>$userinfo[0]['c_id']));
                    if (count($images)) { 
                      foreach($images as $key=>$image){
                      ?>
                     
                      <div class="block m-b">
                         <label class="control-label">File <?php echo $key+1 ?> :</label>
                         <a href="<?php echo base_url('images/complaint_images/').$image["image_name"] ?>" target="_blank">View File</a>
                        <!--  <img src='<?php echo base_url('images/complaint_images/').$image["image_name"] ?>' alt=""> -->
                      </div>
                      
                   <?php } } ?>
               <?php 
                  $replymsg=$this->master_model->getRecords('complainreply',array('complain_id'=>$this->uri->segment(4)),'',array('reply_id'=>'DESC'));
                  if(count($replymsg) > 0)
                  {
                  foreach($replymsg as $reply)
                  {
                  $repliedby=$this->master_model->getRecords('adminlogin',array('id'=>$reply['user_id']));
                  ?>
               <br/>
               <?php if($reply['reply_msg']){?>
               <div class="block m-b">
                  <label class="control-label">Replied Message :</label>
                  <?php echo $reply['reply_msg'];?>
               </div>
               <?php }?>
               <?php if($repliedby[0]['adminuser']!=''){?>
               <div class="block m-b">
                  <label class="control-label">Replied By :</label> <?php echo ucfirst($repliedby[0]['adminuser']);?>
               </div>
               <?php }?>
               <div class="block m-b">
                  <label class="control-label">Replied On :</label> <?php echo date('d M, Y',strtotime($reply['reply_date']));?>
               </div>
               <div class="clear"></div>
               <?php  }?>
               <?php }
                  else{?>
               <div class="block m-b">
                  <label class="control-label">Status : Not replied yet</label>
               </div>
               <?php }?>   
               <?php }else{?>
               <div class="block m-b">
                  <!-- <label class="control-label">Please go back !!!</label> -->
               </div>
               <?php }?>
            </section>
            
         </div>
      </div>
   </div>

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
