<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        User Grievance
         <small>List</small>
         <div style="float:right; padding:2px;">
           <a  href="<?php echo base_url(); ?>admin/callcenterpanel/users"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
          
                   <table class="table table-striped table-bordered dt-responsive nowrap" id="table">
         <thead>
            <tr>
               <th class="text-center bluehd">
                  Sr. No.
               </th>
               <th class="text-center bluehd">
                  Grievance ID
               </th>
               <th class="text-center bluehd">
                  Complainant </br>name
               </th>
               <th class="text-center bluehd">
                  Complaint </br>Message
               </th>
               <th class="text-center bluehd">
                  Complaint</br> date
               </th>
            <!--    <th class="text-center bluehd">
                  Category
               </th> -->
              <!--  <th class="text-center bluehd">
                  Reply Status
               </th> -->
               <th class="text-center bluehd">
                  Grievance Status
               </th>
                              <th class="text-center bluehd">
                  Action
               </th>
             <!--   <th class="text-center bluehd">
                  Replied by
               </th> -->
            </tr>
         </thead>
         <tbody>
            <?php $sr=0;?>
            <?php foreach($result as $details) {
               $username=$this->master_model->getRecords('userregistration',array('user_id'=>$details['user_id']),'user_name');
               if($details['comp_code']!=''){
                ?>
            <tr>
               <td class="text-center"><?php echo $sr+1;?></td>
               <td class="text-left"><?php echo $details['comp_code'];?></td>
               <td class="text-left"><?php echo ucfirst($username[0]['user_name']);?></td>
               <td><a style="text-decoration:none;" title="<?php echo $details['grievence_details'];?>" href="<?php echo base_url()?>admin/callcenterpanel/usercomplaindetail/<?php echo $details['c_id'];?>" ><?php echo substr($details['grievence_details'],'0',30).'...';?></a></td>
               <td class="text-center"><?php echo date('d M Y',strtotime($details['complaint_date']))?></td>
               <!-- <td class="text-center"><?php 
                  $categoty=$this->master_model->getRecords('departments',array('id'=>$details['category'])); 
                   echo ucfirst($categoty[0]['type']);
                  ?></td> -->
             <!--   <td class="text-center"><?php if($details['status']==0) {echo 'Not Replied' ;}else{ echo 'Replied';}?></td> -->
               <td class="text-center"><?php if($details['reply_status']==0) {echo 'Closed' ;}else{ echo 'Open';}?></td>
               <td class="text-center"><span><a href="<?php echo base_url()?>admin/callcenterpanel/complaindetail/<?php echo $details['c_id'];?>"><i class="fa fa-eye"></i></a></span></td>
               <!-- <td class="text-center"><?php 
                  if($details['replyby']!='' && $details['replyby']!='0')
                  {
                    if($details['replyby']!='')
                    {
                      $repliedarray=array();
                      $repliedbyid=explode(',',$details['replyby']);
                      if(count($repliedbyid) >0)
                      {
                        foreach($repliedbyid as $replyidres)
                        {
                          $adminusername=$this->master_model->getRecords('adminlogin',array('id'=>$replyidres));
                          if(count($adminusername) > 0)
                          {
                            $repliedarray[]=ucfirst($adminusername[0]['adminuser']);
                          }
                        }
                        if(count($repliedarray) > 0)
                        {
                          echo implode(',',$repliedarray);
                        }
                      }
                      else
                      {
                        echo 'User not found.';
                      }
                    }
                    else
                    {
                      echo 'User not found.';
                    }
                    }?>
                    
                  </td> -->
            </tr>
            <?php $sr++?>
            <?php } }?>
         </tbody>
      </table>         
   					

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
