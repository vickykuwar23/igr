<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Citizen Users
         <small>List</small>
         <div style="float:right; padding:2px;">
             <!-- <a  href="<?php echo base_url(); ?>admin/callcenterpanel/register_new_user"><button class="btn bg-primary margin " >Add  New User</button></a>
 -->         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
          
            <table class="table table-striped table-bordered dt-responsive nowrap" id="table">
                   <thead>
                      <tr>
                         <th>Sr. No.</th>
                         <th>Name</th>
                         <th>Mobile</th>
                         <th>Email</th>
                         <th>Address</th>
                         <th>Office District</th>
                         <th>Pincode</th>


                          
                      </tr>
                   </thead>
                   <tbody>
                      <?php if(count($users) > 0){
                         foreach($users as $key=>$user){ ?>
                      <tr>
                         <td class="text-center"><?php echo $key+1 ?></td>
                         <td class="text-center"><?php echo $user['user_name'] ?></td>
                         <td class="text-center"><?php echo $user['user_mobile'] ?></td>
                         <td class="text-center"><?php echo $user['user_email'] ?></td>
                         <td class="text-center"><?php echo $user['user_address1'] ?></td>
                         <td class="text-center"><?php echo $user['user_city'] ?></td>
                         <td class="text-center"><?php echo $user['user_pincode'] ?></td>
                      </tr>
                      <?php } //end Foreach
					  
					  } // End If Count ?>
                   </tbody>
                </table> 	         
   					

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>

<script>
$(document).on('click','.view-user',function(){
  var id = $(this).data('id');
  var base_url = '<?php echo base_url() ?>';    
    $.ajax({
      type: "POST",
      url: base_url+"admin/users/details/", 
      async: false, 
      data: {id: id},
      dataType: "html",
      success: function(result){
      $('#view_details').html(result);
      $('#helpModal').modal('show');      
    }});  
});
</script>
