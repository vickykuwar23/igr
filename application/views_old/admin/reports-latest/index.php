<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Top / Least Performer Reports
         <small>List</small>
         <div style="float:right; padding:2px;">
             <!-- <a  href="<?php echo base_url(); ?>admin/callcenterpanel/register_new_user"><button class="btn bg-primary margin " >Add  New User</button></a>
 -->         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
				  
				<form name="frm_expr" id="frm_expr" method="post" action="<?php //echo base_url('admin/sro_panel'); ?>">
          
          <div class="form-group-fileds">            
            <div class="row">
			<div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">From Date</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" name="frm_date" id="frm_date" class="form-control topval datepicker" value="" placeholder="dd-mm-yyyy"/>
                  </div>
                </div>
              </div>
			  <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">To Date</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" name="to_date" id="to_date" class="form-control topval datepicker" value="" placeholder="dd-mm-yyyy"/>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">&nbsp;</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="w-100 js-example-basic-single valid reportVal form-control" id="c_subtype"
                      name="c_subtype">
                      <option value="">-- System Admin --</option>
					  <option value="1">SRO</option>
					  <option value="2">JDR</option>
					  <option value="7">CSO</option>
					  <option value="9">ACS</option>
					  <option value="4">Help Desk</option>
					  <option value="5">Call Center</option>
                    </select>
                  </div>
                </div>
              </div>
			  <div class="col-md-3">
                <label class="col-md-12" for="exampleFormControlSelect1">&nbsp;</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="w-100 js-example-basic-single valid reportVal form-control" id="c_district"
                      name="c_district">
                      <option value="">-- Select District --</option>
					  <option value="1">Mumbai City</option>
					  <option value="2">Mumbai Suburban</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-3" id="response-div">
                <label class="col-md-12" for="exampleFormControlSelect1">&nbsp;</label>
                <div class="col-md-12">
                  <div class="form-group">
                    <input class="btn btn-primary" type="submit" name="btn_export" id="btn_export" value="Export"/>
                  </div>
                </div>
              </div>
			  
            </div>			
          </div>
		  </form>  
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
          
            <table class="table table-striped table-bordered dt-responsive nowrap" id="table-reports">
                   <thead>
                      <tr>
                         <th>Sr. No.</th>
                         <th>Employee Name</th>
						 <th>Designation</th>
                         <th>Mobile</th>
                         <th>Address</th>
                         <th>Office District</th>
						 <th>Total Complaint Assign</th>
						 <th>Total Close Complaint</th>
                         <th>Performance</th>


                          
                      </tr>
                   </thead>
                   <tbody>                      
                   </tbody>
                </table> 	         
   					

                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>