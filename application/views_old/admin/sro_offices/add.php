<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        SRO Offices
        <small>Add</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>sro_offices"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  
                  <!-- main content  -->
                  
            <form class="form-horizontal" method="post" enctype="multipart/form-data" >              
              <div class="form-group" id="office_name_div">
                <label class="col-lg-3 control-label col-lg-offset-1">SRO Office Name <span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <input type="text" name="office_name" maxlength="80"  class="form-control" id="office_name" value="<?php echo $this->input->post('office_name'); ?>">
                  <div id="error_office_name" style="color:#F00"> <?php echo form_error('office_name'); ?></div>
                </div>
              </div>
            
             <div class="form-group" id="district_id_div" >
                <label class="col-lg-3 control-label col-lg-offset-1">District Name<span style="color:red">*</span></label>
                <div class="col-lg-7">
                <select name="district_id" id="district_id" class="form-control" >
                  <option value="">Please select</option>
                  <?php foreach($districts as $district){ ?>
                  <option value="<?php echo $district['district_id'] ?>"><?php echo $district['district_name'] ?></option>
                <?php } ?>
                </select>
                <div id="district_id_error" style="color:#F00"><?php echo form_error('district_id'); ?> </div>
                </div>
              </div>
			  
			  <div class="form-group" id="district_id_div" >
                <label class="col-lg-3 control-label col-lg-offset-1">Office Sub Type<span style="color:red">*</span></label>
                <div class="col-lg-7">
                <select name="office_id" id="office_id" class="form-control" >
                  <option value="">Please select</option>
                  <?php foreach($complaint_subtype_details as $office_details){ ?>
                  <option value="<?php echo $office_details['complaint_sub_type_id'] ?>"><?php echo $office_details['complaint_sub_type_name'] ?></option>
                <?php } ?>
                </select>
                <div id="district_id_error" style="color:#F00"><?php echo form_error('office_id'); ?> </div>
                </div>
              </div>
			  <input type="hidden" onkeypress="return isNumberKey(event);"  name="pincode" maxlength="6"  class="form-control" id="pincode" value="">
			  <!--<div class="form-group" id="office_pin_div">
                <label class="col-lg-3 control-label col-lg-offset-1">Pincode <span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <input type="text" name="pincode" onkeypress="return isNumberKey(event);"  maxlength="6" class="form-control" id="pincode" value="<?php echo $this->input->post('pincode'); ?>">
                  <div id="error_pincode" style="color:#F00"> <?php echo form_error('pincode'); ?></div>
                </div>
              </div>-->

              
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
             
              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <!-- <button type="submit" class="btn btn-white">Cancel</button> -->
                  <button type="submit" name="submit" class="btn btn-success no-shadow" id="btn_submit">Submit</button>
                 
                </div>
              </div>
            </form>
                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<script>
function isNumberKey(evt)
	{
		var a=0;   
		 var charCode = (evt.which) ? evt.which : event.keyCode
		 if ((charCode < 47 || charCode > 45) && (charCode < 48 || charCode > 57))
			 if(charCode==8 )
				 return true;
			 else
		 return false;

	return true;
	}

</script>


