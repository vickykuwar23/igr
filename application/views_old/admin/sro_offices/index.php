<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        SRO Offices
      <small>List</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>sro_offices/add"><button class="btn bg-primary margin " >Add  New</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="success" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body table-responsive">
                  
                  <!-- main content  -->
                   <table class="table table-striped table-bordered dt-responsive nowrap" id="table">
         <thead>
            <tr>
               <th class="text-center bluehd" style="width:10% !important;">
                  Sr. No.
               </th>
               <th class="text-center bluehd">
                  Office Name
               </th>
               <th class="text-center bluehd">
                  District Name
               </th>
			   <th class="text-center bluehd">
                  Office Subtype
               </th>
               <th class="text-center bluehd">
                  Status
               </th>
               <th class="text-center bluehd">
                  Action
               </th>
            </tr>
         </thead>
         <tbody>
            <?php $sr=0;?>
            <?php foreach($records as $details) {
               $reg=$this->master_model->getRecords('gri_district',array('district_id'=>$details['district_id']));
				$officeType=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$details['office_sub_type']));
				$record_cnt=$this->master_model->getRecords('gri_complaint',array('sro_office_id'=>$details['sro_office_id']));
				$rec_cnt = count($record_cnt);
			  ?>
            <tr>
               <td class="text-center"><?php echo $sr+1;?></td>
               <td><?php echo ucfirst($details['office_name']);?></td>
               <td class="text-center"><?php echo $reg[0]['district_name'];?></td>
			    <td class="text-center"><?php echo $officeType[0]['complaint_sub_type_name'];?></td>
               <td>
                  <a href="<?php echo base_url('sro_offices/change_status/').$details['sro_office_id'].'/'.$details['status']; ?>"
                     class="btn btn-sm confirm_change_status <?php echo($details['status']=='1' ? "btn-success" : "btn-danger"); ?>"
                     data-msg="Are you sure you want to change the status of this user?" >
                  <?php if($details['status'] == 1){echo 'Active';}else{echo 'Inactive';};?>
                  </a>
               </td>
               <td class="text-center">
                  <a href="<?php echo base_url();?>sro_offices/edit/<?php echo $details['sro_office_id'];?>" title=""><i class="fa fa-pencil"></i></a>
                  &nbsp;&nbsp;|&nbsp;&nbsp;
					<?php if($rec_cnt > 0){ ?>
						<a href="javascript:void(0);" title="" 
					 class="open-popup"><i class="ui-tooltip fa fa-trash-o "  data-original-title="Delete"></i></a>
					<?php } else { ?>
						<a href="<?php echo base_url();?>sro_offices/delete/<?php echo $details['sro_office_id'];?>" title="" 
                     onclick="javascript:return deletconfirm();"><i class="ui-tooltip fa fa-trash-o"  data-original-title="Delete"></i></a>
					<?php } ?>
				 <!-- <a href="<?php echo base_url();?>sro_offices/delete/<?php echo $details['sro_office_id'];?>" title="" 
                     onclick="javascript:return deletconfirm();"><i class="ui-tooltip fa fa-trash-o"  data-original-title="Delete"></i></a>
					-->
			   </td>
            </tr>
            <?php $sr++?>
            <?php }?>
         </tbody>
      </table>
                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<div id="helpModal" class="modal fade">
 <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Alert</h4>
		</div>
		<div class="modal-body" id="view_details">
			<p>Sorry, Complaint already exist related this SRO office. So that you can't delete this SRO office.</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>
	
</div>
</div>





























