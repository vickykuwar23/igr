<style>
.table-inside{
	width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
}
td.tbl-inside-data {
    text-align: center;
}
td.tbl-inside-data {
    width: 16%;
	font-weight:700;
}
.tbl-bld{
	font-weight:700;
}
td.tbl-head {
    font-weight: 700;
    width: 15%;
}

</style>

<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        Permission
         <small>Update</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>permission/edit"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
                  ?>
               <div class="box-body">
                  
                  <!-- main content  -->
              <form class="form-horizontal" method="post" name="permissions" id="permissions" enctype="multipart/form-data" >              
				<div class="form-group" id="complaint_type_id">
						<label class="col-lg-3 control-label col-lg-offset-1">Role Name <span style="color:red">*</span></label>
						<div class="col-lg-7">
						<select name="role_id" id="role_id" class="form-control">
							<option value=""> -- Select --</option>
							<?php if(count($roles) > 0){
								foreach($roles as $role){ ?>
								<option value="<?php echo $role['role_id'] ?>" <?php if($this->uri->segment('3') == $role['role_id']){ ?> selected="selected" <?php } ?>><?php echo $role['role_name'] ?></option>	
							<?php	}
								
							} ?>
							
						  </select>
						<div id="complaint_type_id" style="color:#F00"> </div>
						</div>
					  </div>
				<table class="table">
					
					<tr>
						<td class="tbl-head">Role</td>
						<td>
						<p class="text-center tbl-bld">Permissions</p>
							<table class="table-inside"> 
								<tr>
									<td class="tbl-inside-data">Read</td>
									<td class="tbl-inside-data">Add</td>
									<td class="tbl-inside-data">Edit</td>
									<td class="tbl-inside-data">Delete</td>
									<td class="tbl-inside-data">Download</td>
									<td class="tbl-inside-data">Export</td>
								</tr>
							</table>
						</td>
					</tr>
					<?php
					error_reporting(0);
					if(count($module) > 0){
					$p = 0;		
					foreach($module as $mod) {
					$id=$this->uri->segment('3');	
					if($id){
						$permission_get = $this->master_model->getRecords('gri_permission',array('role_id'=>$id, 'module_id' => $mod['id']));
					}else{
						$permission_get = array();
					}
					
					if(count($permission_get) > 0){				
					
					?>
					<tr>
						<td class="tbl-head"><?php echo $mod['module_name']; ?><input type="hidden" name="id[]" id="id" value="<?php echo $mod['id']; ?>" /></td>
						<td>
							<table class="table-inside">
								<tr>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="read" <?php if($permission_get[0]['read'] == 1 && $permission_get[0]['module_id'] == $mod['id']){ ?> checked="checked" <?php } ?>/></td>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="add" <?php if($permission_get[0]['write'] == 1 && $permission_get[0]['module_id'] == $mod['id']){ ?> checked="checked" <?php } ?>/></td>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="edit" <?php if($permission_get[0]['edit'] == 1 && $permission_get[0]['module_id'] == $mod['id']){ ?> checked="checked" <?php } ?>/></td>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="delete" <?php if($permission_get[0]['delete'] == 1 && $permission_get[0]['module_id'] == $mod['id']){ ?> checked="checked" <?php } ?>/></td>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="download" <?php if($permission_get[0]['download'] == 1 && $permission_get[0]['module_id'] == $mod['id']){ ?> checked="checked" <?php } ?>/></td>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="export" <?php if($permission_get[0]['export'] == 1 && $permission_get[0]['module_id'] == $mod['id']){ ?> checked="checked" <?php } ?>/></td>
								</tr>
							</table>
						</td>								
					</tr>
					<?php $p++;
							} else { ?>
						<tr>
						<td  class="tbl-head"><?php echo $mod['module_name']; ?><input type="hidden" name="id[]" id="id" value="<?php echo $mod['id']; ?>" /></td>
						<td>
							<table class="table-inside">
								<tr>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="read" /></td>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="add" /></td>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="edit" /></td>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="delete" /></td>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="download" /></td>
									<td class="tbl-inside-data"><input type="checkbox" name="permission[<?php echo $mod['id']; ?>][]" value="export" /></td>
								</tr>
							</table>
						</td>								
					</tr>		
								
						<?php 	}					
						}  // Foreach
					
					} // IF  ?>
				</table>
			  
			  
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
             
              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <button type="submit" name="submit" class="btn btn-success no-shadow" id="btn_submit">Submit</button>
                </div>
              </div>
            </form> 
                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<script>
$(document).ready(function(){
    
	$('select').on('change', function() {
	  var id = this.value;
	  var url= "<?php echo base_url('permission/add/') ?>"+id; 
		window.location = url; 
	});
	
	$("#permissions").submit(function(){
		var checked = $("#permissions input:checked").length > 0;
		if (!checked){
			alert("Please check at least one checkbox");
			return false;
		}
	});
  
});

</script>
