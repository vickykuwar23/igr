<footer class="main-footer">
     <strong> Design and Developed by ESDS software solutions PVT. LTD.</strong>
</footer>
  <div class="control-sidebar-bg"></div>
</div>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script>
var base_path = '<?php echo base_url(); ?>';
</script>
<!--- Graph   --->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->

<!--- End Graph --->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/js/plugins/datepicker/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script> 
<!--- End Datatable JS ---->
<script src="<?php echo base_url(); ?>assets/css/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<script>
$( function() { 
  var date = new Date();
  var d = new Date();        
  d.setDate(date.getDate());

  $("#frm_date").datepicker({
    todayBtn:  false,
    autoclose: true,
	endDate: d,
	todayHighlight: true,
	format: 'dd-mm-yyyy',
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#to_date').datepicker('setStartDate', minDate);
    $('#to_date').datepicker('setDate', minDate); // <--THIS IS THE LINE ADDED
});

$("#to_date").datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	endDate: d,
}).on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('#frm_date').datepicker('setEndDate', maxDate);
    });
  
  
});
$( document ).ready(function() {
	
	
	$('#table').DataTable({
		"searching": true,
	});
	
	//Onload Page Listing
	var table_sro_list = $('#table-sro').DataTable({
			 
		"ordering":false,
		"searching": false,
		"language": {
		"zeroRecords":"No matching records found.",
		"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.		
		 
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"admin/sro_panel/filterdata",
		"type":"POST",
		"data":function(data) {			
			data.c_status 	= $("#c_status").val();
			data.c_type 	= $("#c_type").val();
			data.com_code 	= $("#com_code").val();
			data.username 	= $("#username").val();
			data.district_id = $("#district_id").val();
			data.c_subtype 	 = $("#c_subtype").val();
			data.sro_office  = $("#sro_office").val();
			//alert($("#com_code").val());
			},
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	});
	
	// Get Search Filter Values
	$('.searchVal').change(function(){			
		table_sro_list.ajax.reload();
	});
	
	$('.searchVal').keyup(function(){			
		table_sro_list.ajax.reload();	
	});
	
	//on Click Page Listing	Complaint Listing with New 21-04-2020	
	var table_comp_list = $('#table-complaint-list').DataTable({
				 
			"ordering":false,
			"searching": false,
			"language": {
			"zeroRecords":"No matching records found.",
			"infoFiltered":""
						},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			 
			// Load data for the table's content from an Ajax source
			"ajax": {
			"url": base_path+"admin/reports/complaint_reports",
			"type":"POST",
			"data":function(data) {			
				
				data.frm_date 			= $("#frm_date").val();
				data.to_date 			= $("#to_date").val();
				data.office_subtype		= $("#office_subtype").val();
				
				},
			"error":function(x, status, error) {
				
			},
			"statusCode": {
			401:function(responseObject, textStatus, jqXHR) {			
						},
					},
			}
			
		});
	
	//table_comp_list.ajax.reload();
	$('.btn-click').click(function(){
		var frm_date = $('#frm_date').val();
		var to_date = $('#to_date').val();	
		var office_subtype = $('#office_subtype').val();
		table_comp_list.ajax.reload();
		get_chart(frm_date,to_date,office_subtype);		
	
	});
	
	
	// Pendency Report On 25 April 2020 in Use
	var table_pendency_reports_list = $('#table-pendency').DataTable({
			 
		"ordering":false,
		"searching": false,
		"language": {
		"zeroRecords":"No matching records found.",
		"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.		
		 
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"admin/reports/pendencycomplaint",
		"type":"POST",
		"data":function(data) {			
			data.complaint_code 	= $("#complaint_code").val();
			data.frm_date 			= $("#frm_date").val();
			data.to_date 			= $("#to_date").val();
			data.citizen_name 		= $("#citizen_name").val();
			data.c_subtype 			= $("#c_subtype").val();
			data.sro_office			= $("#sro_office").val();
			data.district_id 		= $("#district_id").val();
			data.c_type 			= $("#c_type").val();
			data.cp_status 			= $("#cp_status").val();
			},
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	});
	
	
	$('.datepicker').datepicker({
	  autoclose : true,
	  format: 'yyyy-mm-dd',
	});
	
	$('#frm_date').change(function(){		
		table_pendency_reports_list.ajax.reload();
	});
	
	$('#to_date').change(function(){	
		table_pendency_reports_list.ajax.reload();
	});
	
	
	$('#frm_expr').on('keyup keypress', function(e) {	
		
	  var keyCode = e.keyCode || e.which;
	  if (keyCode === 13) { 
		e.preventDefault();
		return false;
	  }
	});
	
	///   Pending Complaint JDR/ACS 25th April 2020 //
	 var table_pendency_jdr_list = $('#table-pendency-list').DataTable({
			 
		"ordering":false,
		"searching": false,
		"language": {
		"zeroRecords":"No matching records found.",
		"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.		
		 
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"admin/reports/pendency_reports_level_2",
		"type":"POST",
		"data":function(data) {	
			
			data.frm_date 			= $("#frm_date").val();
			data.to_date 			= $("#to_date").val();
			data.office_subtype 	= $("#office_subtype").val();
			data.complaint_subtype 	= $("#complaint_subtype").val();
			
			},
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	});
	
	$('.btn-level-1').click(function(){
		var frm_date = $('#frm_date').val();
		var to_date = $('#to_date').val();
		var office_subtype = $('#office_subtype').val();
		var complaint_subtype = $('#complaint_subtype').val();
		table_pendency_jdr_list.ajax.reload();
		get_chart(frm_date,to_date,office_subtype,complaint_subtype);		
	
	});
	
	//////////////  END JDR Pending Reports//////////////////////
	
	
	///   Pending Complaint IGR/IGRA 25th April 2020 //
	 var table_pendency_igr_list = $('#table-pendency-list-igr').DataTable({
			 
		"ordering":false,
		"searching": false,
		"language": {
		"zeroRecords":"No matching records found.",
		"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.		
		 
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"admin/reports/pendency_reports_level_3",
		"type":"POST",
		"data":function(data) {	
			
			data.frm_date 		= $("#frm_date").val();
			data.to_date 		= $("#to_date").val();
			data.region_id 		= $("#region_id").val();
			data.district_id 	= $("#district_id").val();
			
			},
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	});
	
	$('.btn-level-1').click(function(){
		var frm_date = $('#frm_date').val();
		var to_date = $('#to_date').val();
		var office_subtype = $('#office_subtype').val();
		table_pendency_igr_list.ajax.reload();
		get_chart(frm_date,to_date,office_subtype);			
	
	});
	
	//////////////  END IGR Pending Reports 25th April 2020 ///////
	
	
	////// JDR/ACS Performance ////////
	 var table_performance_jdr_list = $('#table-performance-list').DataTable({
			 
		"ordering":false,
		"searching": false,
		"language": {
		"zeroRecords":"No matching records found.",
		"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.		
		 
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"admin/reports/performance_reports_level_2",
		"type":"POST",
		"data":function(data) {	
			
			data.frm_date 			= $("#frm_date").val();
			data.to_date 			= $("#to_date").val();
			data.office_subtype 	= $("#office_subtype").val();
			
			},
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	});
	
	$('.btn-level-1').click(function(){
		var frm_date = $('#frm_date').val();
		var to_date = $('#to_date').val();
		var office_subtype = $('#office_subtype').val();
		table_performance_jdr_list.ajax.reload();		
		get_chart(frm_date,to_date,office_subtype);		
	});
	
	//////////////  END JDR/ACS Performance Reports 25th April 2020 /////
	
	
	//////  IGR/Jr. HQ Performance Reports  ////////
	 var table_performance_igr_list = $('#table-performance-level3').DataTable({
			 
		"ordering":false,
		"searching": false,
		"language": {
		"zeroRecords":"No matching records found.",
		"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.		
		 
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"admin/reports/performance_ranking_3",
		"type":"POST",
		"data":function(data) {	
			
			data.frm_date 			= $("#frm_date").val();
			data.to_date 			= $("#to_date").val();
			data.region_id 			= $("#region_id").val();
			data.district_id		= $("#district_id").val();
			},
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	});
	
	$('.btn-level-3').click(function(){
		var frm_date 	= $('#frm_date').val();
		var to_date 	= $('#to_date').val();
		var region_id 	= $('#region_id').val();
		var district_id	= $('#district_id').val();
		table_performance_igr_list.ajax.reload();
		get_chart(frm_date,to_date,region_id,district_id);	
		get_chart(frm_date,to_date,office_subtype,complaint_subtype);			
	
	});
	
	//////////////  END IGR/Jr. HQ Performance Reports//////////////////////
	
	
	//////  IGR/Jr. HQ Complaint Reports  ////////
	 var table_complaint_igr_list_level_3 = $('#table-complaint-level-3').DataTable({
			 
		"ordering":false,
		"searching": false,
		"language": {
		"zeroRecords":"No matching records found.",
		"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.		
		 
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"admin/reports/complaint_pending_level_2",
		"type":"POST",
		"data":function(data) {	
			
			data.frm_date 			= $("#frm_date").val();
			data.to_date 			= $("#to_date").val();
			data.role_type 			= $("#role_type").val();
			},
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	});
	
	$('.btn-sub-level3').click(function(){
		var frm_date 	= $('#frm_date').val();
		var to_date 	= $('#to_date').val();
		var role_type 	= $('#role_type').val();
		table_complaint_igr_list_level_3.ajax.reload();	
		get_chart(frm_date,to_date,role_type);					
	
	});
	
	//////////////  END IGR/Jr. HQ Complaint Reports//////////////////////
	
	///   Pending Complaint Report To IGR/IGRA For SRO ////////
	 var table_complaint_srodata_list = $('#table-complaint-level-1').DataTable({
			 
		"ordering":false,
		"searching": false,
		"language": {
		"zeroRecords":"No matching records found.",
		"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.		
		 
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"admin/reports/complaint_reports_level_1",
		"type":"POST",
		"data":function(data) {	
			
			data.frm_date 		= $("#frm_date").val();
			data.to_date 		= $("#to_date").val();
			data.office_subtype = $("#office_subtype").val();
			
			},
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	});
	
	$('.btn-sub-level1').click(function(){
		var frm_date = $('#frm_date').val();
		var to_date = $('#to_date').val();
		var office_subtype = $('#office_subtype').val();
		table_complaint_srodata_list.ajax.reload();
		get_chart(frm_date,to_date,office_subtype);	
	
	});
	
	//////////////  END IGR Complaint overview Reports Level 1 //////////////////////
});


function print_chart(){	
	$(".bar_chart_div_img").show();
	var divContents = $(".bar_chart_div_img").html();
	var printWindow = window.open('', '', 'height=400,width=800');
	printWindow.document.write('<html><head><title>Graph Report</title>');
	printWindow.document.write('</head><body >');
	printWindow.document.write(divContents);
	printWindow.document.write('</body></html>');
	printWindow.document.close();
	printWindow.print();
	$(".bar_chart_div_img").hide();
}



//Modal Popup Open
$(document).on('click','.open-popup',function(){		
	$('#helpModal').modal('show');	
});

function deletconfirm()
{
  var x = confirm("Are you sure you want to delete?");
  if (x)
      return true;
  else
    return false;
}


</script>
</body>
</html>

