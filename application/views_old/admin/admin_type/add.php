<style>
/*.input-group .input-group-addon {
    width: 80%;
}*/
</style>
<div class="content-wrapper" style="min-height: 946px;">
   <section class="content-header">
      <h1>
        System Users
         <small>Add</small>
         <div style="float:right; padding:2px;">
             <a  href="<?php echo base_url(); ?>subadmin"><button class="btn bg-primary margin " >Back</button></a>
         </div>
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <?php if ($this->session->flashdata('error_message') != "") 
                  { ?>
               <div class="alert alert-danger alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
               <?php } ?>
               <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
                  <i class="fa fa-ban"></i>
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <b>Success!</b> <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
               <?php } 
			  // echo validation_errors();
			   $display = "";
			   $idid = "";
			   $offices = "";
			   //$offtypes = "";
			   if($this->input->post('role_id') == 2){
				   //$display="display:none";
				   //$idid =  "display:none";
				   $offices =  "display:none";
				   //$offtypes =  "display:none";
			   } else if($this->input->post('role_id') == 9){
				   $offices =  "display:none";
				   //$display="display:none";				   
			   } else if($this->input->post('role_id') == 3 || $this->input->post('role_id') == 11 || $this->input->post('role_id') == 12 || $this->input->post('role_id') == 13 || $this->input->post('role_id') == 14 || $this->input->post('role_id') == 16){	
				   $offices =  "display:none";
				   $display="display:none";				   
			   } else if($this->input->post('role_id') == 5 || $this->input->post('role_id') == 4){
				   $display="display:none";	
				   $offices =  "display:none";	
			   } else {
				   $display="";
			   }
			   ?>
               <div class="box-body">
                  
                  <!-- main content  -->
              <form class="form-horizontal" method="post" enctype="multipart/form-data" >              
              
				<div class="form-group" id="role_id" >
                <label class="col-lg-3 control-label col-lg-offset-1">Role <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <select name="role_id" id="role_id" class="form-control rolenames">
				  <option value=""> -- Select --</option>
				  <?php foreach($roles as $role){ ?>
				  <option value="<?php echo $role['role_id']; ?>" <?php if(set_value('role_id') == $role['role_id']){ ?> selected="selected" <?php } ?>><?php echo $role['role_name']; ?></option>
				  <?php } ?>
				</select>
                <div id="role_id" style="color:#F00"><?php echo form_error('role_id'); ?> </div>
                </div>
              </div>
			  
			  <div class="form-group" id="religion_id_div" style="<?php echo $display ?>">
                <label class="col-lg-3 control-label col-lg-offset-1">Region <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <select name="region_id" id="region_id" class="form-control chooseregion">
				  <option value=""> -- Select --</option>
				  <?php foreach($get_regions as $region){ ?>
				  <option value="<?php echo $region['religion_id']; ?>" <?php if(set_value('region_id') == $region['religion_id']){ ?> selected="selected" <?php } ?>><?php echo $region['region_division_name']; ?></option>
				  <?php } ?>
				</select>
                <div id="region_id" style="color:#F00"><?php echo form_error('region_id'); ?> </div>
                </div>
              </div>
			  
			  <div class="form-group" id="district_ids_div" style="<?php echo $display.$idid ?>">
                <label class="col-lg-3 control-label col-lg-offset-1">Office District <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <select name="district_id" id="district_id" class="form-control choosesub">
				  <option value=""> -- Select --</option>				 
				</select>
                <div id="district_ids" style="color:#F00"><?php echo form_error('district_id'); ?> </div>
                </div>
              </div>
			  
			  <div class="form-group" id="office_type_div" style="<?php echo $offices ?>">
                <label class="col-lg-3 control-label col-lg-offset-1">Office Type <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <select name="office_id" id="office_id" class="form-control officeType">
				  <option value=""> -- Select --</option>
				<?php 
					foreach($complaint_subtype_details as $officename){
						?>
						<option value="<?php echo $officename['complaint_sub_type_id']; ?>"><?php echo $officename['complaint_sub_type_name']; ?></option>	
					<?php	
					}
				?>	
				</select>
                <div id="office_ids" style="color:#F00"><?php echo form_error('office_id'); ?> </div>
                </div>
              </div>
			  
			  <div class="form-group" id="sr_id_div" style="<?php echo $offices ?>">
                <label class="col-lg-3 control-label col-lg-offset-1">SRO Office <span style="color:red">*</span></label>
                <div class="col-lg-7">
                <select name="sro_id" id="sro_id" class="form-control sroffice">
				  <option value=""> -- Select --</option>				 
				</select>
                <div id="sro_idl" style="color:#F00"><?php echo form_error('sro_id'); ?> </div>
                </div>
              </div>
			  
			  <div class="form-group" id="emailid">
                <label class="col-lg-3 control-label col-lg-offset-1">Fullname <span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <input type="text" name="namecc"  data-required="true" class="form-control" value="<?php echo set_value('namecc')?>" id="namecc"> 
                  <div id="error_namecc" style="color:#F00"> <?php echo form_error('namecc'); ?></div>
                </div>
              </div>

			  <div class="form-group" id="full_name">
                <label class="col-lg-3 control-label col-lg-offset-1">Username <span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <input type="text" name="username"  data-required="true" class="form-control" value="<?php echo set_value('username')?>" id="username" > 
                  <div id="username" style="color:#F00"> <?php echo form_error('username'); ?></div>
                </div>
              </div>
			  
			  <div class="form-group" id="emailid">
                <label class="col-lg-3 control-label col-lg-offset-1">Email ID <span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <input type="email" name="email"  data-required="true" class="form-control" value="<?php echo set_value('email')?>" id="email"> 
                  <div id="error_complaint_type_name" style="color:#F00"> <?php echo form_error('email'); ?></div>
                </div>
              </div>		  
			  
			  
			  <div class="form-group" id="emailid">
                <label class="col-lg-3 control-label col-lg-offset-1">Contact No. <span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <input type="text" name="contact_no" maxlength="10" onkeypress="return isNumberKey(event);"   data-required="true" class="form-control" value="<?php echo set_value('contact_no')?>" id="contact_no"> 
                  <div id="error_contact_no" style="color:#F00"> <?php echo form_error('contact_no'); ?></div>
                </div>
              </div>
			  
			  <div class="form-group" id="cc">
                <label class="col-lg-3 control-label col-lg-offset-1">Address<span style="color:red">*</span></label>
                <div class="col-lg-7">
                  <textarea name="address" id="address" class="form-control"><?php echo set_value('address')?></textarea> 
                  <div id="error_address" style="color:#F00"> <?php echo form_error('address'); ?></div>
                </div>
              </div> 
			  
              <div class="form-group" id="cc">
                <label class="col-lg-3 control-label col-lg-offset-1">Password<span style="color:red">*</span></label>
                <div class="col-lg-7">
                 <input type="password" id="password" name="password" data-required="true" class="form-control"> 
                  <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password" style="cursor:pointer;"></span>
				  <div id="error_password" style="color:#F00"> <?php echo form_error('password'); ?></div>
                </div>
              </div>
			  
			  <!--<div class="form-group" id="cc">
				<label class="col-lg-3 control-label col-lg-offset-1">Password<span style="color:red">*</span></label>
				<div class="input-group col-lg-6" >
				  <input type="password" id="password" name="password" data-required="true" class="form-control">
				  <div class="input-group-addon">
					<span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
				  </div>
				</div>
			  </div>-->
			  
			  <!--<div class="form-group">
				<label class="col-md-4 control-label">Password</label>
				<div class="col-md-6">
				  <input id="password-field" type="password" class="form-control" name="password" value="secret">
				  <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
				</div>
			  </div>-->
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
             
              <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <button type="submit" name="submit" class="btn btn-success no-shadow" id="btn_submit">Submit</button>
                </div>
              </div>
            </form> 
                  <!-- main content end -->


               </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>

<script>
$(document).ready(function(){
	
	$(".toggle-password").click(function() {

	  $(this).toggleClass("fa-eye fa-eye-slash");
	  var input = $($(this).attr("toggle"));
	  if (input.attr("type") == "password") {
		input.attr("type", "text");
	  } else {
		input.attr("type", "password");
	  }
	});
	
    $(".uncheck").click(function() {
	  $(".check").not(this).attr("checked", false); //uncheck all checkboxes
	  $(this).attr("checked", true);  //check the clicked one
	});
	
	$(".check").click(function() {
	  $(".uncheck").not(this).attr("checked", false); //uncheck all checkboxes
	  $(this).attr("checked", true);  //check the clicked one
	});
	
	$('#fullname').keypress(function (e) {
		var regex = new RegExp("^[a-zA-Z0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}

		e.preventDefault();
		return false;
	});
	
	$('#username').keypress(function (e) {
		var regex = new RegExp("^[a-zA-Z0-9_]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}

		e.preventDefault();
		return false;
	});
	
	  $(document).on('change','.rolenames',function(){
			var role_id = $('select#role_id option:selected').val(); //$("#role_id").val();
			var region_id = $("#region_id").val();
			var base_url = '<?php echo base_url() ?>';
			//alert(role_id);
			if(role_id == 2){ 
				$("#religion_id_div").show();
				$("#district_ids_div").show();
				$("#sr_id_div").hide();	
				$('#office_id').prop('selectedIndex',0);				
			} else if(role_id == 3 || role_id == 5 || role_id == 4 || role_id == 11 || role_id == 12 || role_id == 13 || role_id == 14 || role_id == 16){
				$("#religion_id_div").hide();
				$("#district_ids_div").hide();
				$("#sr_id_div").hide();
				$("#office_type_div").hide();
				$('#office_id').prop('selectedIndex',0);
			} else if(role_id == 9){
				$('#office_id').prop('selectedIndex',0);
				$("#religion_id_div").show();
				$("#district_ids_div").show();
				$("#office_type_div").show();				
				$("#sr_id_div").hide();
				var region_id = 4;
				$.ajax({
					type: "POST",
					url: base_url+"subadmin/offType", 
					async: false, 
					data: {role_id:role_id,region_id:region_id},
					success: function(res){
						$('#office_id').html(res);
				}});
				
			} else if(role_id == 7){
				$('#office_id').prop('selectedIndex',0);
				$("#religion_id_div").show();
				$("#district_ids_div").show();
				$("#office_type_div").show();				
				$("#sr_id_div").show();
				
			} else {
				$("#religion_id_div").show();
				$("#district_ids_div").show();
				$("#sr_id_div").show();
				$("#office_type_div").show();
				$('#office_id').prop('selectedIndex',0);
				$.ajax({
					type: "POST",
					url: base_url+"subadmin/offType", 
					async: false, 
					data: {role_id:role_id,region_id:region_id},
					success: function(res){
						$('#office_id').html(res);
				}});
			}
			
			
		});
		
		
		//Onload Selected Values 
		var region_id = $("#region_id").val(); 
		var district_id = $("#district_id").val();
		var base_url = '<?php echo base_url() ?>';
		
		// District List
		$.ajax({
		type: "POST",
		url: base_url+"subadmin/getDistrictlist", 
		async: false, 
		data: {regionid:region_id},
		success: function(res){
			$('#district_id').html(res);
			validation();	
		}});
		
		// Office Type List
		$.ajax({
			type: "POST",
			url: base_url+"subadmin/offTypeList", 
			async: false, 
			data: {district_id:district_id, region_id: region_id},
			success: function(res){
				$('#office_id').html(res);
		}});
  
});


$(document).on('change','.choosesub',function(){
	var region_id = $("#region_id").val();
	var role_id = $('select#role_id option:selected').val();
	var base_url = '<?php echo base_url() ?>';
	if(role_id != 3 || role_id != 5 || role_id != 4 || role_id != 11 || role_id != 12 || role_id != 13 || role_id != 14 || role_id != 16){
		$.ajax({
			type: "POST",
			url: base_url+"subadmin/offType", 
			async: false, 
			data: {role_id:role_id,region_id:region_id},
			success: function(res){
				$('#office_id').html(res);
		}});
	}
	
});

$(document).on('change','.chooseregion',function(){
	var region_id = $("#region_id").val();
	var role_id = $('select#role_id option:selected').val();
	var base_url = '<?php echo base_url() ?>';
	if(role_id != 3 || role_id != 5 || role_id != 4 || role_id != 11 || role_id != 12 || role_id != 13 || role_id != 14 || role_id != 16){
		$.ajax({
			type: "POST",
			url: base_url+"subadmin/getdistrict", 
			async: false, 
			data: {region_id:region_id},
			success: function(res){
				$('#district_id').html(res);
		}});	
	}
});

$(document).on('change','.officeType',function(){
	var district_id = $("#district_id").val();
	var office_id = $("#office_id").val();
	var role_id = $("#role_id").val();
	
	var base_url = '<?php echo base_url() ?>';
	$.ajax({
		type: "POST",
		url: base_url+"subadmin/getOffice", 
		async: false, 
		data: {district_id:district_id,office_id:office_id,role_id:role_id},
		success: function(res){
			$('#sro_id').html(res);
	}});
	
});

function isNumberKey(evt)
{
	var a=0;   
	 var charCode = (evt.which) ? evt.which : event.keyCode
	 if ((charCode < 47 || charCode > 45) && (charCode < 48 || charCode > 57))
		 if(charCode==8 )
			 return true;
		 else
	 return false;

return true;
}
function blockSpecialChar(e)
{
	var k;
	document.all ? k = e.keyCode : k = e.which;
	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
}
</script>
