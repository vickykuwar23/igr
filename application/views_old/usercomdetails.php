<!--<div style="text-align:center;">
<?php if($error_msg != "") { ?>
<div class="alert alert-error">
<button data-dismiss="alert" class="close" type="button">×</button>
<strong>Warning!</strong> <?php echo $error_msg ?>
</div>
<?php } ?>  
<?php if ($this->session->flashdata('success_message') != '') { ?>
     <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success_message') ?>
      </div>
    <? } ?>
    <?php if ($this->session->flashdata('invalidcredential') != '') { ?>
     <div class="alert alert-warning">
        <button data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('invalidcredential') ?>
      </div>
    <? } ?></div>-->
<section id="content" class="min380">
  <div class="main padder">
    <div class="row">
      <div class="col-lg-6 col-lg-offset-3 m-t-large">
        <section class="panel grdbg">
          <header class="panel-heading text-center"> Grievance Redressal System</header>
          <strong class="complaintdetails"> Complainant Details </strong>
            <?php if($countuser=='1'){ ?>
             <div class="block m-t-b">
            <label class="control-label">Complaint By :</label>
           <?php  $username=$this->master_model->getRecords('userregistration',array('user_id'=>$userinfo[0]['user_id']),'user_name');?>
             <?php echo ucfirst($username[0]['user_name']);?>
            </div>
            
             <div class="block m-b">
            <label class="control-label">Complaint Message :</label>
             <?php echo $userinfo[0]['msg_content'];?>
            </div>
            
             <div class="block m-b">
              <label class="control-label">Type :</label>
              <?php 
			  $category=$this->master_model->getRecords('departments',array('id'=>$userinfo[0]['category']));
			  echo ucfirst($category[0]['type']);?>
            </div>
            
            <?php if($userinfo[0]['fdc_id']!=''){?>
             <div class="block m-b">
              <label class="control-label">Sub Type :</label>
              <?php echo $userinfo[0]['fdc_id'];?>
            </div>
            <?php }?>
              <?php if($userinfo[0]['company']!=''){?>
             <div class="block m-b">
              <label class="control-label">Sub Type :</label>
              <?php echo $userinfo[0]['company'];?>
            </div>
            <?php }?>
            
             <div class="block m-b">
              <label class="control-label">Date of Submission : </label>
              <?php echo date('d M, Y',strtotime($userinfo[0]['registerdate']));?>
            </div>
             
                 <?php 
			if($userinfo[0]['uploadpath']!='')
			{/*?>
           		 <div class="block m-b">
                  <label class="control-label">Attachment :</label>
                  <a class="btn btn-success" href="<?php echo base_url();?>uploads/<?php echo $userinfo[0]['uploadpath'];?>" target="_new" style="text-decoration:none;">View</a>
                  </div>
            <?php 
		*/	} ?>
            
			 <?php 
			  $replymsg=$this->master_model->getRecords('complainreply',array('complain_id'=>$this->uri->segment(3)),'',array('reply_id'=>'DESC'));
			  if(count($replymsg) > 0)
			  {
				foreach($replymsg as $reply)
				{
					$repliedby=$this->master_model->getRecords('adminlogin',array('id'=>$reply['user_id']));
					?>
	                <br/>
                    <?php if($reply['reply_msg']){?>
                     <div class="block m-b">
                       <label class="control-label">Replied Message :</label>
                       <?php echo $reply['reply_msg'];?>
                     </div>
                     <?php }?>
                     <?php if($repliedby[0]['adminuser']!=''){?>
                     <div class="block m-b">
                    <label class="control-label">Replied By :</label> <?php echo ucfirst($repliedby[0]['adminuser']);?>
                    </div>
                    <?php }?>
                     <div class="block m-b">
                    <label class="control-label">Replied On :</label> <?php echo date('d M, Y',strtotime($reply['reply_date']));?>
                    </div>
                    
                    <div class="clear"></div>
	<?php 	}?>
 <?php }
 else{?>
      <div class="block m-b">
                       <label class="control-label">Status : Not replied yet</label>
                      
                     </div>
<?php }?>	 
<?php }else{?><div class="block m-b">
            <label class="control-label">Please go back !!!</label>
            </div><?php }?>

          </section>
          <div class="row">
<div class="col-md-12">
<div class="row">
<div class="col-md-2 text-center col-md-offset-5 m-t-b">
<a class="btn btn-comn no-shadow" href="<?php echo base_url();?>index.php/userprofile/pre/"><i class="fa fa-angle-double-left"></i>Go back</a></div>
</div>
</div>
</div>
      </div>
    </div>
  </div>
</section>
