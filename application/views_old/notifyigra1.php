<style>
    .complaint_code_frm label {
        margin-bottom: 0;
        margin-top: 5px;
    }
</style>
<!-- asidebar error_message-->
    <section class="sitemap-wrapper">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-12">
                    <div class="title-heading">
                        <h1 class="heading">Report To IGRA</h1>
                    </div>
                </div>
            </div>
            <div class="row mb-12">
                <div class="col-sm-12">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.” The </p>
                    <p>Purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page,
                        etc.) that doesn't distract from the layout.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.” The </p>
                </div>
            </div>
            <hr />
            <div class="row col-md-12">
                <form class="w-100" name="notificationfrm" id="notificationfrm" method="post"
                    action="">
					<?php if ($this->session->flashdata('error_message') != '') { ?>
                    <div class="row mb-5">
                        <div class="col-md-6">
                            <div class="alert alert-error mb-0" style="font-size:17px;">
                                <span> <i class="fa fa-tick pr-2"></i><?php echo $this->session->flashdata('error_message') ?> <strong>
                                    </strong></span><strong>
                                    <button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
                                </strong>
                            </div>
                        </div>
                    </div>
					<?php } ?>
                    <div class="complaint_code_frm">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <label class="control-label">Enter Complaint Code </label>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <input type="text" name="c_code" class="form-control"  id="c_code" value="" />
								<div id="c_code" class="error" style="color:#F00"><?php echo form_error('c_code'); ?> </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <button type="submit" name="submit" id="submit" class="btn btn_submit"
                                        id="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
	
    <!-- //asidebar -->