<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<!-- Latest compiled and minified CSS -->
<!-- jQuery library -->
<!-- Latest compiled JavaScript -->
<!-- Latest compiled and minified CSS -->

<!-- jQuery library -->
<script src="<?php echo base_url();?>js/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>js/datatables/bootstrap.min.css">

<!-- basic libraries required for datatable Funcyions include : search,pagination,no of records -->
<script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>js/dataTables.bootstrap.min.css" >

<!-- datatable buttons-->
<!--<script src="https://cdn.datatables.net/buttons/1.0.1/js/dataTables.buttons.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.0.1/css/buttons.dataTables.min.css" >
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.colvis.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.html5.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.1/js/buttons.flash.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>-->

<!--Responsive table-->
<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.min.css" >
<script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.bootstrap.min.css" >-->

<!--Which column should display
<script src="//cdn.datatables.net/buttons/1.0.1/js/buttons.colVis.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.2.0/css/colReorder.dataTables.min.css" >-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Grievance Admin Panel</title>
</head>
<body>
<div class="row">      
     <div class="col-md-4 col-md-offset-4 m-t">  
    <form method="post">         
	<strong>Grievance Status : </strong>  
    <select class="" onchange="javascript:this.form.submit();" name="grievance_type" id="grievance_type" style="width:40%; background-color:#dbf4ff;">
      <option value="" <?php if($type=='1'){echo "selected='selected'";}?>>Select</option>
      <option value="2" <?php if($type=='2'){echo "selected='selected'";}?>>Closed</option>
      <option value="3" <?php if($type=='3'){echo "selected='selected'";}?>>Open</option>
  </select>
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
      </form>
      </div>
      </div>
      <div class="col-md-1 addadmin">
         <a href="<?php echo base_url();?>index.php/report/generaterepo/" class="btn btn-success no-shadow" target="_new">View Custom Report</a></div>
         <!-- <div class="col-md-1 addadmin1">
<a href="<?php echo base_url();?>index.php/report/generaterepostd/" class="btn btn-info no-shadow" target="_new">View Standard Report</a></div> -->
<table class="table table-striped table-bordered dt-responsive nowrap" id="table">
									<thead>
									<tr>
										<th class="text-center bluehd">
											Sr. No. 
										</th>
                                          <th class="text-center bluehd">
											Grievance ID
										</th>
                                         <th class="text-center bluehd">
											Complainant </br>name
										</th>
										<th class="text-center bluehd">
											Complaint Message
										</th>
										<th class="text-center bluehd">
											Type
										</th>
										<th class="text-center bluehd">
										Complaint Date
										</th>
                                        <th class="text-center bluehd">
										 Reply Status
										</th>
                                          <th class="text-center bluehd">
										 Grievance Status
										</th>
							</tr>
									</thead>
									<tbody>
							<?php if(count($complaint) > 0){
								 $srno=1;
								foreach($complaint as $res)
								{
									$username=$this->master_model->getRecords('userregistration',array('user_id'=>$res['user_id']),'user_name');
									//$type=$this->master_model->getRecords('usercomplainbox');
									?>	
                            	    	<tr>
								    	<td class="text-center">
										<?php echo $srno?>
										</td>
                                        <td class="text-left">
										<?php echo $res['pattern'];?>
										</td>
                                        <td class="text-left">
										<?php echo ucfirst($username[0]['user_name']);?>
										</td>
										<td>
										 <a style="text-decoration:none;" href="<?php echo base_url()?>index.php/adminpanel/complaindetail/<?php echo $res['msgid'];?>">
										 <?php echo substr($res['msg_content'],'0',40).'...';?>	
										</a>
                                        </td>
                                        <td  class="text-center">
											<?php
											$categoty=$this->master_model->getRecords('departments',array('id'=>$res['category']));	
											 echo ucfirst($categoty[0]['type']);?>
										</td>
										<td class="text-center">
											<?php echo date('d M Y',strtotime($res['registerdate']));?>	
										</td>
                                        <td class="text-center">
											<?php 
											if($this->session->userdata('type')=='7')
											{
												$replied_status=$this->master_model->getRecords('complainreply',array('complain_id'=>$res['msgid']),'complaint_status');	
											}
											else
											{
												$replied_status=$this->master_model->getRecords('complainreply',array('complain_id'=>$res['msgid'],'user_id'=>$this->session->userdata('adminid')),'complaint_status');	
											}
				
											if(count($replied_status) > 0)
											{
												if($replied_status[0]['complaint_status']=='1')
												{
													echo "Replied";
												}
												else
												{
														echo "Not-replied";
												}
											}
											else
											{
											 	echo "Not-replied";
											}
											?>	
                                        </td>
                                       <td class="text-center"><?php if($res['reply_status']==0) {echo 'Closed' ;}else{ echo 'Open';}?></td>
									</tr>
								<?php 
							$srno++;	}
							}?>
									</tbody>
</table>
</body>

<script>
$(document).ready(function(){
    $('#table').DataTable({
		dom: 'Bfrtip', //to show the buttons on the screen
        buttons: [    // which buttons should display
        ]
	});
});
</script>
</html>