<style>
div#table_filter {
    margin: 0 0px 20px;
}
.modal-header {
	display: block;
}
</style>
<?php 
error_reporting(0);
if ($this->session->flashdata('success_message') != '') { ?>
	<div class="row">
<div class="col-md-7 mx-auto text-center mt-3">
     <div class="alert alert-success mb-0" style="font-size:17px;">
        <span> <i class="fa fa-tick pr-2"></i> <?php echo $this->session->flashdata('success_message') ?> </span>       
		<button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
      </div>
 </div>
 </div>
    <?php } ?> 
    <?php if ($this->session->flashdata('invalidcredential') != '') { ?>
    	<div class="row">
     <div class="col-md-4 mx-auto text-center mt-3">
     <div class="alert alert-warning mb-0" style="font-size:17px;">
        <span> <i class="fa fa-tick pr-2"></i> <?php echo $this->session->flashdata('invalidcredential') ?> </span>       
		<button data-dismiss="alert" class="close" type="button" id="user_close">×</button>
      </div>
 </div>
  </div>
    <?php } ?>
	
    
	
<div class="container">
	<div class="row">
		<div class="col-md-12 justify-content-end addadmin">   
		<a href="<?php echo base_url();?>complaint/add" class="btn btn-warning no-shadow"><i class="fa fa-plus"></i>Add Complaint</a></div>
 	</div>
</div>
 
<div class="container"> 
<div>
<table class="table table-striped table-bordered dt-responsive nowrap datatable" id="table">
	<thead>
			<tr>
				<th class="text-center bluehd" style="width:5% !important;">
					Sr. No.
				</th>
				<th class="text-center bluehd">
					Complaint Code
				</th>
				<th class="text-center bluehd">
					Complaint Belongs To
				</th>
				<th class="text-center bluehd">
					Office Type
				</th>
				<th class="text-center bluehd">
				 Grievance Details
				</th>
				<!-- <th class="text-center bluehd">
				 Relief Required
				</th> -->
				<th class="text-center bluehd">
				 Reply Status
				</th>
				<!-- <th class="text-center bluehd">
				 Complaint Status
				</th> -->				
				<th class="text-center bluehd">
					Action
				</th>
				<!-- <th class="text-center bluehd">
				 Status
				</th>
				<th class="text-center bluehd">
					Action
				</th> -->
			</tr>
			</thead>
			<tbody>
		  <?php $sr=0;?>
		  <?php foreach($complaint_listing as $details) {
			  	if ($details['comp_code']!='') {
			  		
				// Complaint Type Name
				$com_type_name=$this->master_model->getRecords('gri_complaint_type',array('complaint_type_id'=>$details['complaint_type_id']));
				
				// Get IGRA User ID
				$igraReport =$this->master_model->getRecords('gri_complaint_assign',array('comp_code'=>$details['comp_code'], 'assign_user_id' => '8', 'status' => '1'));
				
				$showButton = "";
				if($details['complaint_type_id'] == 1){
					
					$getJdr =$this->master_model->getRecords('gri_complaint_assign',array('comp_code'=>$details['comp_code'], 'status' => '1'));
					$assignDetail = $getJdr[0]['assign_user_id'];
					$jdr_Details =$this->master_model->getRecords('adminlogin',array('id'=>$assignDetail));
					
					$endDateFormat = strtotime($getJdr[0]['end_date']);
					$endDate = date('Y-m-d', $endDateFormat);
					$currentDate =   date('Y-m-d');						
					
					if(($jdr_Details[0]['id'] == $assignDetail) && ($currentDate > $endDate)){						
							
							$showButton = '&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void(0);" data-toggle="tooltip" data-id="'.$details['comp_code'].'" id="btn_report" style=""><span class="fa fa-bell"></span></a>';
										
					}
					
				} else {
					
					$getJdr =$this->master_model->getRecords('gri_complaint_assign',array('comp_code'=>$details['comp_code'], 'status' => '1'));
					$assignDetail = $getJdr[0]['assign_user_id'];
					$jdr_Details =$this->master_model->getRecords('adminlogin',array('id'=>$assignDetail));
					
					$endDateFormat = strtotime($getJdr[0]['end_date']);
					$endDate = date('Y-m-d', $endDateFormat);
					$currentDate =   date('Y-m-d');						
					
					if(($jdr_Details[0]['id'] == $assignDetail) && ($currentDate > $endDate)){						
							
							$showButton = '&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void(0);" data-toggle="tooltip" data-id="'.$details['comp_code'].'" id="btn_report" title="Report To IGRA"><span class="fa fa-bell"></span></a>';
										
					}
					
				}					

				if(count($igraReport) > 0){
					$showButton = "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:void(0);' data-toggle='tooltip' data-placement='top' title='Successfully Reported To IGRA'><span class='fa fa-bell-slash'></span></a>";
				}	
				
				if (strlen($details['grievence_details']) > 30) {
					$content = substr($details['grievence_details'], 0, 25). ' ...<a href="javascript:void(0);" class="view-mores" data-id="'.$details['c_id'].'">View More</a>';
				} else {
					$content = $details['grievence_details'];
				}
					
				if($details['complaint_sub_type_id'] == 0){
					$com_subtype_name=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$details['online_service_id']));
					$nameSubType = $com_subtype_name[0]['complaint_sub_type_name'];
				} else {
					$com_subtype_name=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$details['complaint_sub_type_id']));
					$nameSubType = substr($com_subtype_name[0]['complaint_sub_type_name'],'0',25).'...';
				}
			  
			  ?>
			<tr>
			<td class="text-center"><?php echo $sr+1;?></td>
			<td><?php echo $details['comp_code'];?></td>
			<td class="text-center"><?php echo $com_type_name[0]['complaint_type_name'];?></td>
			<!-- <td class="text-center"><?php echo $com_subtype_name[0]['complaint_sub_type_name'];?></td> -->
			<td class="text-center"><?php echo $nameSubType;?></td>

			
			<td class="text-center"><?php echo $content;?></td>
			<!-- <td class="text-center"><?php echo $details['relief_required'];?></td> -->
			
			<!-- <td class="text-center"><?php if($details['complaint_status'] == 1){echo 'Completed';}else{echo 'Pending';} ?></td> -->

			  <td class="text-center">
            <?php 

            $replied_status=$this->master_model->getRecords('complainreply',array('complain_id'=>$details['c_id']),'complaint_status');  
			
            if(count($replied_status) > 0)
            {
				if($replied_status[0]['complaint_status']=='1')
				{
					echo "Replied";
					
				} else {
					
					echo "Not-replied";
				}
				
            } else {
				echo "Not-replied";
            }
            ?>  
        </td>
		
		<td id="<?php echo $details['comp_code'] ?>" class="text-center">
			
			<a href="<?php echo base_url('complaint/complaint_detail/'.$details['c_id']);?>" data-id="<?php echo $details['c_id'];?>" data-toggle='tooltip' data-placement='top' title='View'><i class="fa fa-eye"></i></a>
			<!--<a href="javascript:void(0);" title="" data-id="<?php echo $details['c_id'];?>" class="view-complaint"><i class="fa fa-eye"></i>View</a>-->
			
			<!-- <span class="change-txt-<?php echo $details['comp_code'] ?>">		
				<?php echo $showButton; ?>
			</span> -->
			</td>
			<!-- <td class="text-center"><?php if($details['status'] == 1){echo 'Active';}else{echo 'Inactive';}?></td>
			<td class="text-center">
		
			<a href="javascript:void(0);" title="" data-id="<?php echo $details['c_id'];?>" class="view-complaint"><i class="fa fa-eye"></i></a>
			<a href="<?php echo base_url();?>index.php/complaint/delete/<?php echo $details['c_id'];?>" title="" 
			onclick="javascript:return deletconfirm();"><i class="ui-tooltip fa fa-trash-o"  data-original-title="Delete"></i></a>
			<a href="javascript:void(0);" title="" data-id="<?php echo $details['c_id'];?>" class="assign-complaint"><i class="fa fa-plus"></i></a>
			</td> -->
			
			</tr>
		   <?php $sr++?>
		   <?php } }?>
			</tbody>
</table>
</div>
</div>

<div id="helpModal" class="modal fade">
 <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Details</h4>
		</div>
		<div class="modal-body" id="view_details">
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>
	
</div>
</div>
<div class="clr"></div>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
$(document).on('click','.view-mores',function(){
	var id = $(this).data('id');
	var base_url = '<?php echo base_url() ?>';		
		$.ajax({
			type: "POST",
			url: base_url+"complaint/viewdetails/", 
			async: false, 
			data: {id: id},
			dataType: "html",
			success: function(result){
			$('#view_details').html(result);
			$('#helpModal').modal('show');			
		}});	
}); 

$(document).on('click','.view-complaint',function(){
	var id = $(this).data('id');
	var base_url = '<?php echo base_url() ?>';		
		$.ajax({
			type: "POST",
			url: base_url+"complaint/details/", 
			async: false, 
			data: {id: id},
			dataType: "html",
			success: function(result){
			$('#view_details').html(result);
			$('#helpModal').modal('show');			
		}});	
}); 

$(document).on('click','.assign-complaint',function(){
	var id = $(this).data('id');
	
	var base_url = '<?php echo base_url() ?>';		
		$.ajax({
			type: "POST",
			url: base_url+"complaint/officer_list/", 
			async: false, 
			data: {id: id},
			dataType: "html",
			success: function(result){	
			$('#helpModal').modal('show');		
			$('#view_details').html(result);
			//console.log(result);
					
		}});	
}); 

$(document).on('click','#btn_report',function(){
	var id = $(this).data('id');
	//alert(id);return false;
	var base_url = '<?php echo base_url() ?>';		
		$.ajax({
			type: "POST",
			url: base_url+"complaint/assignOther/", 
			async: false, 
			data: {id: id},
			dataType: "html",
			success: function(result){	
			//console.log(result);			
			$("span.change-txt-"+id).html("&nbsp;&nbsp;|&nbsp;&nbsp; <a href='javascript:void(0);' data-toggle='tooltip' title='Successfully Report To IGRA'><i class='fa fa-bell-slash'></i></a>");				
					
		}});	
});
</script>
