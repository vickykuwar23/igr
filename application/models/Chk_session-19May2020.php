<?php if(!defined('BASEPATH')){exit('No direct script access.');}
class chk_session extends CI_Model
{
	##---- check session of carrier  -------##
	public function chk_admin_session()
	{
		if($this->session->userdata('adminid')!='')
		{
			return true;
		}
		else
		{
			redirect(base_url().'adminlogin');
		}
	}
	##---- check session of shipper  -------##
	public function chk_user_session()
	{
		if($this->session->userdata('user_id')!='')
		{
			return true;
		}
		else
		{
			redirect(base_url().'userlogin');
		}
	}
	public function chk_superadmin_session()
	{
		if($this->session->userdata('type')=='999')
		{
			return true;
		}
		else
		{
			redirect(base_url().'superadminlogin/suplogin');
		}
	}

	public function chk_sro_session()
	{
		$user_type = $this->session->userdata('type');
		if($user_type=='1' || $user_type=='2' || $user_type=='4' || $user_type=='999' || $user_type=='3'  || $user_type=='7' || $user_type=='9'  || $user_type=='10' || $user_type=='11' || $user_type=='12' || $user_type=='13' || $user_type=='14' )
		{
			return true;
		}
		else
		{
			redirect(base_url().'superadminlogin/suplogin');
		}
	}

	public function chk_callcenter_session()
	{
		if($this->session->userdata('adminid')!='')
		{
			return true;
		}
		else
		{
			redirect(base_url().'callcenterlogin');
		}
	}
	
	
}	