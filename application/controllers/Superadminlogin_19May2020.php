<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class superadminlogin extends CI_Controller 
{
   function __construct()
	{
		 parent::__construct();
		 
	}
	public function suplogin()
	{   
    	$ip = $this->get_client_ip();
	   // if($ip!="115.124.115.71" && $ip!="59.145.23.38")
	   // {
	   //     	redirect(base_url());
	   // }
	   
   //   	if($this->session->userdata('supadminid')!='')
			// {
			//   redirect(base_url().'subadmin');
			// }
			
	   if(isset($_POST['submit']))
		{
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_superadminlogin');
			if($this->form_validation->run())
			{
				$_userName = 	$this->db->escape_str($this->input->post('username'));
				$_passWord = 	$this->input->post('password');
				$_admintype=	'superadmin';
				$input_array=array('adminuser'=>$_userName,'adminpass'=>sha1($_passWord));
				$user_info=$this->master_model->getRecords('adminlogin',$input_array);
				// echo $this->db->last_query(); exit;
				if(is_array($user_info) && count($user_info))
				{ 
					$this->db->join('gri_roles','gri_roles.role_id=adminlogin.role_id');
                    $role_details=$this->master_model->getRecords('adminlogin',array('id' => $user_info[0]['id']),'role_name');
					$user_data=array(
									'supusername'=>$user_info[0]['adminuser'],
									'supadminid'=>$user_info[0]['id'],
									'district_id'=>$user_info[0]['district_id'],
									'type'=>$user_info[0]['role_id'],
									'fullname'=>$user_info[0]['namecc'],
									'sro_id'=>$user_info[0]['sro_id'],
									'role_name'=>$role_details[0]['role_name']
								);
					$this->session->set_userdata($user_data);

					//add logs
					$json_encode_data = json_encode(array('username'=>$_userName));
					$log_user_id      = $this->session->userdata('supadminid');
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Superadminlogin",
										'action_name' 	=> "Success",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> $this->session->userdata('role_name'),
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
					//end add logs
					
					if( $user_info[0]['type']=='999' ){
						redirect(base_url().'admin/dashboard');
					}
					else if( $user_info[0]['role_id']=='5' ){
						redirect(base_url().'admin/callcenterpanel/users');
					}
					else if( $user_info[0]['role_id']=='1' || $user_info[0]['role_id']=='2' || $user_info[0]['role_id']=='3' || $user_info[0]['role_id']=='7' || $user_info[0]['role_id']=='9' || $user_info[0]['role_id']=='15' || $user_info[0]['role_id']=='4' || $user_info[0]['role_id']=='16'){
						redirect(base_url().'admin/sro_panel');
					}
					else
					{
						redirect(base_url().'superadminlogin/suplogin');
					}

			    }
			   else
			    { 
			    	//add logs
					$json_encode_data = json_encode(array('username'=>$_userName));
					$log_user_id      = null;
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Superadminlogin",
										'action_name' 	=> "Failed",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> "System User",
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
					//end add logs
					$this->session->set_flashdata('invalidadminlogin','Invalid credential');
					redirect(base_url().'superadminlogin/suplogin');
			    }
		    }
		}
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
					
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		 
		  //$this->session->unset_userdata("mycaptcha");
	    $_SESSION["superadminlogincaptcha"]=$cap['word'];
		$data['middlecontent']='superadminlogin';
	    $this->load->view('superadmintemplate',$data);
		
    }//F_login

    public function refresh(){
        // Captcha configuration
        	$this->load->helper('captcha');
        $config = array(
            'img_path' => './images/captcha/',
			'img_url' => base_url().'images/captcha/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('superadminlogincaptcha');
        $this->session->set_userdata('superadminlogincaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}	
	
    public function check_captcha_superadminlogin($code) 
	{
	  // check if captcha is set -
		if($code == '' || $_SESSION["superadminlogincaptcha"] != $code )
		{
			$this->form_validation->set_message('check_captcha_superadminlogin', 'Invalid %s.'); 
			$this->session->set_userdata("superadminlogincaptcha", rand(1,100000));
			
			return false;
		}
		if($_SESSION["superadminlogincaptcha"] == $code)
		{
			$this->session->unset_userdata("superadminlogincaptcha");
			//$this->session->set_userdata("mycaptcha", rand(1,100000));
			return true;
		}
		
	}
	
	public function supadminlogout()
	{
	   
	    $user_data=array('supusername'=>'',
									 'supadminid'=>'');
		$this->session->unset_userdata($user_data);
		session_destroy();
		redirect(base_url().'superadminlogin/suplogin');
	 }
	 
	 function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }

    public function logout()

	{
		
        session_destroy();

	    
         unset($_SESSION);
		redirect(base_url().'superadminlogin/suplogin');
	 }
	
	
}//C_welcome