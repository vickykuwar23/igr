<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		$this->chk_session->chk_superadmin_session();
    }
	
	public function index()
	{
		$data['module']=$this->master_model->getRecords('gri_module','','',array('id'=>'ASC'));
		$data['middlecontent']='module/index';
	    $this->load->view('admin/admin_combo',$data);
	}

	public function add()
	{	
		$this->form_validation->set_rules('module_name', 'Module Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('page_link', 'Page URL', 'trim|required|xss_clean');
		if($this->form_validation->run())
		{
			$c_module_name 		= 	htmlentities($this->input->post('module_name'));
			$c_module_url 		= 	$this->input->post('page_link');
			$createdAt			=   date('Y-m-d H:i:s');
			$insert_arr=array('module_name'=>$c_module_name,
							   'page_link'=>$c_module_url,
								'status'=>'1',
							  'createdat'=>$createdAt,
							  'updatedat'=>null,
							  );
			$complaintType_info = $this->master_model->insertRecord('gri_module',$insert_arr);
			$this->session->set_flashdata('success_message','The module has been created');
			redirect(base_url().'module');
		}
		
		$data['middlecontent']='module/add';	
	 	$this->load->view('admin/admin_combo',$data);
	}
	
/****** Edit Functionality ******/ 
	
	public function edit()
	{
		$data['error_msg']='';
		$updateid=$this->uri->segment('3');
		
			$this->form_validation->set_rules('module_name', 'Module Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('page_link', 'Page URL', 'trim|required|xss_clean');
			if($this->form_validation->run())
			{
				$c_module_name 		= 	htmlentities($this->input->post('module_name'));
				$updatedAt			=   date('Y-m-d H:i:s');
				$c_module_url 		= 	$this->input->post('page_link');
				$update_arr=array('module_name'=>$c_module_name,
									'page_link'=>$c_module_url,	
								  'updatedat'=>$updatedAt,
								  );
				
				if($this->master_model->updateRecord('gri_module',$update_arr,array('id'=>"'".$updateid."'")))
				{
					$this->session->set_flashdata('success_message','Module has been updated successfully.');
					redirect(base_url().'module/edit/'.$updateid);
				}
				else
				{
					$data['error_msg']='Error while updating record.';
				}
			}
		

		$data['role_data']=$this->master_model->getRecords('gri_module',array('id'=>$updateid));
		if(count($data['role_data']) <= 0)
		{
			redirect(base_url().'role/view/');
		}
			
		$data['middlecontent']='module/edit';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	public function delete()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'module');
		}
		if($this->master_model->deleteRecord('gri_module','id',$id))
		{
		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'module');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'module');
		}
	
	}

	public function change_status()
	{
		 $id = $this->uri->segment(3);
		 $status = $this->uri->segment(4);
	
		if($status=='1'){$newstatus = '0'; }
		else if($status=='0'){$newstatus = '1';}
		if($this->master_model->updateRecord("gri_module",array('status'=>$newstatus)
		,array('role_id'=>$id)))
		{
			$this->session->set_flashdata('success_message','Status changed successfully.');						           
			redirect(base_url().'module');
		}
		else
		{
			$this->session->set_flashdata('error_message','Error while updating user status.');										            
			redirect(base_url().'module');
		}
	}
	
} // Class End

?>