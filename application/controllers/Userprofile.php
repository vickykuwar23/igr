<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userprofile extends CI_Controller 

{

   function __construct()

	{

		 parent::__construct();

		 $this->load->model('chk_session');

		 $this->chk_session->chk_user_session();

		 // $cookie= $this->input->cookie('random_cookie', TRUE);
	  //      if(!$cookie){
	  //       redirect(base_url()."userlogin/logout");
	  //     }

      }

	public function user()

	{   

	   $data['file_error']='';

	   if(isset($_POST['submit']))

		{

			$x='';

			$this->form_validation->set_rules('account','Category','trim|required|xss_clean');

			$this->form_validation->set_rules('message','Message','trim|required|xss_clean|max_length[500]');
			
			$this->form_validation->set_rules('empcode','Employee code','trim|xss_clean');
			
			$this->form_validation->set_rules('fdc_id','Folio No. / DP ID / Client ID','trim|xss_clean');
			
			$this->form_validation->set_rules('company_name','Company name','trim|xss_clean');

			if($this->form_validation->run())

			{

				$custid=htmlentities($this->session->userdata('user_id'));

				$category = htmlentities($this->input->post('account'));

				$empcode = htmlentities($this->input->post('empcode'));

				$fdc = htmlentities($this->input->post('fdc_id'));

				$company_name=htmlentities($this->input->post('company_name'));

				$message=htmlentities($this->input->post('message'));

				//$uploadfile=$this->input->post('userfile');

				

				if($category=='1')

				{$x=1;}

				elseif($category=='2')

				{$x=2;}

				elseif($category=='3')

				{$x=3;}

				elseif($category=='4')

				{$x=4;}

				elseif($category=='5')

				{$x=5;}

				elseif($category=='6')

				{$x=6;}

				

				$filename='';

			    //$this->load->library('upload'); 	

				$config=array('upload_path'=>'uploads/',

									    'allowed_types'=>'doc|docx|pdf|txt|xls|PDF|DOC|DOCX|TXT|XLS',

									    'file_name'=>rand(1,9999),'max_size' => 2048);
									/*	echo '<pre>';
										print_r($config);die;*/
              
				//$this->upload->initialize($config);
				
				$this->load->library('upload', $config);
              
			     if(isset($_FILES['userfile']['name']) && ($_FILES['userfile']['name']!=''))

			          {

							if($this->upload->do_upload('userfile'))

								{

								  $dt = $this->upload->data();
								  
								//  var_dump($dt); exit();

								  $filename=$dt['file_name'];

								}

								else

								{

									$data['file_error']=$this->upload->display_errors();
									$this->session->set_flashdata('upload_err',$this->upload->display_errors());
									redirect(base_url().'userprofile/user/');
									//var_dump($data['file_error']);exit();

								}

							}

				 $insert_arr=array('category'=>$category,

				                              'company'=>$company_name,

											  'fdc_id'=>$fdc,

											  'empcode'=>$empcode,

											  'msg_content'=>$message,

											  'user_id'=>$custid,
											  
											  'ip_address'=>$this->get_client_ip(),

											  'registerdate'=>date('Y-m-d'),

											  'uploadpath'=>$filename

											 );

				

			 if($insert_id=$this->master_model->insertRecord('usercomplainbox',$insert_arr,true))

			 {

					$last_patternid=$this->master_model->getRecords('usercomplainbox',array('msgid'=>$insert_id),'msgid');

					if(count($last_patternid) > 0)

					{

						$last_count = 100+$last_patternid[0]['msgid']; 

					}

				  
				  $pattern='GR'.date('y').'0'.$x.'00'.$last_count;

				  if($this->master_model->updateRecord('usercomplainbox',array('pattern'=>$pattern),array('msgid'=>$insert_id)))

				    {
						
						$admin_email=$this->master_model->getRecords('adminlogin',array('type'=>$category));
						/*echo '<pre>';
						print_r($admin_email);die;*/
						
						$user_info=$this->master_model->getRecords('userregistration',array('user_id'=>$this->session->userdata('user_id')));

						//Sending mail to user from superadmin for grievance has bees posted successfully 

						$info_arr=array(

						'to'=>$user_info[0]['user_email'],						

						'from'=>$this->config->item('MAIL_FROM'),//$admin_email[0]['email']
						
						'cc'=>$admin_email[0]['emailcc'],

						'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$pattern,

						'view'=>'complain_email'

						);
					
						$other_info=array('name'=>$user_info[0]['user_name'],

														'pattern'=>$pattern,

														'user_email'=>$user_info[0]['user_email'],

														'user_mobile'=>$user_info[0]['user_mobile'],

														'msg_content'=>$message,

														'reply_status'=>'1');
														
   						
						if($this->email_sending->sendmail($info_arr,$other_info))

						{
							
							//Sending mail to admin user's from supeadmin for new  grievance has bees posted successfully 

							$admin_email_list=$this->master_model->getRecords('adminlogin',array('type'=>$category,'type !='=>'superadmin'));
							$super_admin_email=$this->master_model->getRecords('adminlogin',array('type'=>'superadmin'));

							if(count($admin_email_list) > 0)

							{

								foreach($admin_email_list as $res)

								{								

									$info_arr=array(

									'to'=>$res['email'],
									
									'cc'=>'',

									'from'=>$this->config->item('MAIL_FROM'),//$super_admin_email[0]['email']

									'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$pattern.'',

									'view'=>'complaint_email_to_admin'

									);

									$other_info=array('adminname'=>$res['adminuser'],

																	'pattern'=>$pattern,

																	'username'=>$user_info[0]['user_name'],

																	'user_email'=>$user_info[0]['user_email'],

																	'user_mobile'=>$user_info[0]['user_mobile'],

																	'msg_content'=>$message,

																	'reply_status'=>'1');

									$this->email_sending->sendmail($info_arr,$other_info);

								}

							}

							$this->session->set_flashdata('successmsgsend','Your grievance '.$pattern.' has been registered.');

							redirect(base_url().'userprofile/user/');

						}

					  }

				 }

			}

		}

		$this->db->like('type', 'Specific Office');

		$this->db->or_like('type', 'Online Office');

		$this->db->or_like('type', 'others');

		$data['category']=$this->master_model->getRecords('departments');

	    $data['middlecontent']='userprofile';

	 	$this->load->view('template',$data);

	}


	public function get_client_ip() {
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = '';
		return $ipaddress;
	}
	

	public function pre()

	{

		$user_id=array('user_id'=>$this->session->userdata('user_id'));

		$data['result']=$this->master_model->getRecords('usercomplainbox',$user_id);

        $data['middlecontent']='precomplaint';

	 	$this->load->view('template',$data);

	}

	public function usercomplaindetail()

	{

		$data['countuser']='';

	    $data['userinfo']=$this->master_model->getRecords('usercomplainbox',array('msgid'=>$this->uri->segment(3)));

	    //print_r($data['userinfo']);

		if(count($data['userinfo'])>0)

		{

			$data['countuser']='1';
			if($data['userinfo'][0]['user_id'] != $this->session->userdata('user_id')){
				redirect(base_url()."userprofile/pre/");
			}
			
			
		}

		$data['middlecontent']='usercomdetails';

	 	$this->load->view('template',$data);

	}

	function change_pass(){
		$user_id=$this->session->userdata('user_id');
		
		$data['error_msg']='';
	
		if(isset($_POST['btn_set']) && $user_id!='')
		{
			// $this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_createpass');
			// $this->form_validation->set_rules('phonenumber','phone number','trim|required|exact_length[10]|xss_clean|numeric');
			$this->form_validation->set_rules('pass','Password','trim|required|min_length[8]|xss_clean');
			$this->form_validation->set_rules('con_password','Confirm Password','trim|required|xss_clean');
			if($this->form_validation->run())
			{
				$pass=$this->input->post('pass');
				$con_password=$this->input->post('con_password');
				$update_arr=array('user_pass'=>$this->sha256($pass),
												 'password1'=>$this->sha256($pass)
												);
					
					if($insert_id=$this->master_model->updateRecord('userregistration',$update_arr,array('user_id'=>$user_id)))
					{
						$this->session->set_flashdata('success_message','You have successfully reset your password.');

						redirect(base_url('userprofile/change_pass'));
					}else{
							$this->session->set_flashdata('error_message','Error while processing your request.');
						redirect(base_url('userprofile/change_pass'));
					}
				
			}		
		}
			
		
		$data['middlecontent']='change_pass';

		$this->load->view('template',$data);
	} 

	public function sha256($pass)
	{
		return hash('sha256',$pass);
	}

	function profile(){
		$user_id=$this->session->userdata('user_id');

			if(isset($_POST['btn_reg']))
			{
				$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
				//$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|valid_email|required|edit_unique[userregistration.user_email.user_id.'.$user_id.']');
				//$this->form_validation->set_rules('mobile', 'Mobile No.', 'trim|required|xss_clean|min_length[10]|numeric|edit_unique[userregistration.user_mobile.user_id.'.$user_id.']');
				
				$this->form_validation->set_rules('address1', 'Address', 'trim|required|xss_clean');
				$this->form_validation->set_rules('aadhar_number', 'Aadhar card number', 'numeric|min_length[12]|max_length[16]|xss_clean');
				$this->form_validation->set_rules('state', 'State', 'trim|required|xss_clean');
				$this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');	
				$this->form_validation->set_rules('pincode', 'Pincode', 'trim|required|xss_clean|numeric');	
				$this->form_validation->set_message('is_unique', 'The %s already exists');
				$this->form_validation->set_message('edit_unique', 'The %s is already exists');

					if($this->form_validation->run())
					{				
						$name=htmlentities($this->input->post('name'));
						//$email=htmlentities($this->input->post('email'));
						//$mobile=htmlentities($this->input->post('mobile'));
						$landline=htmlentities($this->input->post('landline'));
						$aadhar_number=htmlentities($this->input->post('aadhar_number'));
						if ($aadhar_number) {
							$aadhar_no =$aadhar_number;
						}else{
							$aadhar_no = null;
						}
						$address1=htmlentities($this->input->post('address1'));
						$address2=htmlentities($this->input->post('address2'));
						$state=htmlentities($this->input->post('state'));
						$city=htmlentities($this->input->post('city'));
						$pincode=htmlentities($this->input->post('pincode'));
						
						$update_arr= array(
										'user_name'=>$name,
										//'user_email'=>$email,
										//'user_mobile'=>$mobile,
										'user_landline'=>$landline,
										'user_address1'=>$address1,
										'user_address2'=>$address2,
										'user_state'=>$state,
										'aadhar_number'=>$aadhar_no,
										'user_city'=>$city,
										'user_pincode'=>$pincode
										);
								  
						if($insert_id=$this->master_model->updateRecord('userregistration',$update_arr,array('user_id'=>$this->session->userdata('user_id') ) ) )
						{		  
							$this->session->set_flashdata('success_message','Profile updated successfully');
							redirect(base_url('userprofile/profile'));		  

						}

				
			   }		

			}


		$data['state']=$this->master_model->getRecords('states','','',array('ch3'=>'ASC'));
		$user_info=$this->master_model->getRecords('userregistration',array('user_id'=>$this->session->userdata('user_id')));
		 $sta = $user_info[0]["user_state"];

		$city=$this->master_model->getRecords('city',array('ch3'=>$sta));
		$data['city']=$city;

		$data['user_info']=$user_info;
		$data['middlecontent']='profile';
		$this->load->view('template',$data);
	}

}
//

?>