<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class superadminlogin extends CI_Controller 
{
   function __construct()
	{
		 parent::__construct();
	}
	public function suplogin()
	{  
    	$ip = $this->get_client_ip();	 
			
	   if(isset($_POST['submit']))
		{
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_superadminlogin');
			if($this->form_validation->run())
			{
				$_userName 		= 	$this->db->escape_str($this->input->post('username'));
				$_passWord 		= 	$this->input->post('password');
				$_admintype		=	'superadmin';
				$input_array	=	array('adminuser'=>$_userName,'adminpass'=>sha1($_passWord));
				$user_info		=	$this->master_model->getRecords('adminlogin',$input_array);
				// echo $this->db->last_query(); 
				// echo ">>>>".count($user_info);die();
				if(is_array($user_info) && count($user_info))
				{   //echo "<pre>";print_r($user_info);
					$this->db->join('gri_roles','gri_roles.role_id=adminlogin.role_id');
                    $role_details=$this->master_model->getRecords('adminlogin',array('id' => $user_info[0]['id']),'role_name');
					// echo $this->db->last_query(); 
					// echo "==<br /><pre>";print_r($role_details);die();
					$user_data=array(
									'supusername'=>$user_info[0]['adminuser'],
									'supadminid'=>$user_info[0]['id'],
									'district_id'=>$user_info[0]['district_id'],
									'type'=>$user_info[0]['role_id'],
									'fullname'=>$user_info[0]['namecc'],
									'sro_id'=>$user_info[0]['sro_id'],
									'role_name'=>$role_details[0]['role_name']
								);
					$this->session->set_userdata($user_data);

					//add logs
					$json_encode_data = json_encode(array('username'=>$_userName));
					$log_user_id      = $this->session->userdata('supadminid');
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Superadminlogin",
										'action_name' 	=> "Success",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> $this->session->userdata('role_name'),
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
					//end add logs
					//echo ">>>>".$user_info[0]['type'];die();
					if( $user_info[0]['type'] == '999' ){
						redirect(base_url().'admin/dashboard');die();
					}
					else if( $user_info[0]['role_id']=='5' || $user_info[0]['role_id']=='18' ){
						redirect(base_url().'admin/callcenterpanel/users');die();
					}
					else if( $user_info[0]['role_id']=='1' || $user_info[0]['role_id']=='2' || $user_info[0]['role_id']=='3' || $user_info[0]['role_id']=='7' || $user_info[0]['role_id']=='9' || $user_info[0]['role_id']=='15' || $user_info[0]['role_id']=='4' || $user_info[0]['role_id']=='11' || $user_info[0]['role_id']=='12' || $user_info[0]['role_id']=='13' || $user_info[0]['role_id']=='14' || $user_info[0]['role_id']=='16' ){
						
						redirect(base_url().'admin/sro_panel');die();
					}
					else
					{   //echo ">>>>>>>>>";die();
						redirect(base_url().'superadminlogin/suplogin');die();
					}

			    }
			   else
			    { //echo ">>>>>>>>>";die();
			    	//add logs
					$json_encode_data = json_encode(array('username'=>$_userName));
					$log_user_id      = null;
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Superadminlogin",
										'action_name' 	=> "Failed",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> "System User",
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
					//end add logs
					$this->session->set_flashdata('invalidadminlogin','Invalid credential');
					redirect(base_url().'superadminlogin/suplogin');die();
			    }
		    }
		}
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
					
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		 
		  //$this->session->unset_userdata("mycaptcha");
	    $_SESSION["superadminlogincaptcha"]=$cap['word'];
		$data['middlecontent']='superadminlogin';
	    $this->load->view('superadmintemplate',$data);
		
    }//F_login

    public function refresh(){
        // Captcha configuration
        	$this->load->helper('captcha');
        $config = array(
            'img_path' => './images/captcha/',
			'img_url' => base_url().'images/captcha/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('superadminlogincaptcha');
        $this->session->set_userdata('superadminlogincaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}	
	
    public function check_captcha_superadminlogin($code) 
	{
	  // check if captcha is set -
		if($code == '' || $_SESSION["superadminlogincaptcha"] != $code )
		{
			$this->form_validation->set_message('check_captcha_superadminlogin', 'Invalid %s.'); 
			$this->session->set_userdata("superadminlogincaptcha", rand(1,100000));
			
			return false;
		}
		if($_SESSION["superadminlogincaptcha"] == $code)
		{
			$this->session->unset_userdata("superadminlogincaptcha");
			//$this->session->set_userdata("mycaptcha", rand(1,100000));
			return true;
		}
		
	}
	
	public function supadminlogout()
	{
	   
	    $user_data=array('supusername'=>'',
									 'supadminid'=>'');
		$this->session->unset_userdata($user_data);
		session_destroy();
		redirect(base_url().'superadminlogin/suplogin');
	 }
	 
	 function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }

    public function forgetpass()

	{

		if(isset($_POST['submitforgetpass']))		

			{
			
				$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha');
				if($this->form_validation->run()){
				$email=htmlentities($this->input->post('email'));

				$user_info=$this->master_model->getRecords('adminlogin',array('email'=>$email));
				if(count($user_info)>0)	

				{

					$token = rand(1000000,9999999);
					$this->master_model->updateRecord('adminlogin',array('token'=>$token),array('email'=>"'".$email."'"));
					// echo $this->db->last_query();
					// die;

               		// $sql = "UPDATE adminlogin SET 
                 //  token='".$token."'
                 //  WHERE email='$email' ";		
					// $email='vishal.phadol27@gmail.com';
					$info_arr=array('to'=>$email,
					
		                           'cc'=>'',

								  'from'=>$this->config->item('MAIL_FROM'),

								  'subject'=>'Forget Password of '.$this->config->item('PROJECT_NAME').' Grievance Portal',

								  'view'=>'forgetpassmail_superadmin'

								  );

					$other_info=array('name'=>$user_info[0]['namecc'],
										'token'=>$token,
			                         'user_id'=>$user_info[0]['id']);

											  
					if($this->email_sending->sendmail($info_arr,$other_info))

					{
							
						
						//add logs
						$json_encode_data = json_encode(array('email'=>$email));
						$log_user_id      = null;
						$ipAddr			  = $this->get_client_ip();
						$logDetails = array(
											'module_name' 	=> "Superadminlogin",
											'action_name' 	=> "forgetpass",
											'json_response'	=> $json_encode_data,
											'login_id'		=> $log_user_id,
											'role_type'		=> "Citizen User",
											'createdAt'		=> date('Y-m-d H:i:s'),
											'ip_addr'		=> $ipAddr
											);
						$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
						//end add logs
						$this->session->set_flashdata('success_message','If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.');
						redirect(base_url().'superadminlogin/forgetpass/');

					}

					else

					{

						$this->session->set_flashdata('failsend','Something goes wrong please try again in some time. ');

						redirect(base_url().'superadminlogin/forgetpass/');

					}	
				

			}

			else

			{
				$this->session->set_flashdata('failsend','Email address not found.');
				redirect(base_url().'superadminlogin/forgetpass/');

			}



				}else{
						
						$this->session->set_flashdata('invalidcredential','Invalid credential');
						
				}

			}
			$this->load->helper('captcha');
			$vals = array(
				'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
			$cap = create_captcha($vals);
			$data['image'] = $cap['image'];
			//$this->session->unset_userdata("mycaptcha");
			unset($_SESSION["mycaptcha"]);			  
			$_SESSION["mycaptcha"]=$cap['word'];		

		$data['middlecontent']='forgetpass_superadmin';

		$this->load->view('template',$data);

	}
		public function refresh_forget(){
        // Captcha configuration
        $this->load->helper('captcha');
        $config = array(
            'img_path' => './images/captcha/',
			'img_url' => base_url().'images/captcha/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}
	public function check_captcha($code) 

	{
				// return true;
		      // check if captcha is set -
				if($code == '' || $_SESSION["mycaptcha"] != $code  ||  $this->session->userdata("used")==1  )
				{
					$this->form_validation->set_message('check_captcha', 'Invalid %s.'); 
					$this->session->set_userdata("mycaptcha", rand(1,100000));
					$this->session->set_userdata("used",0);
					return false;
				}
				if($_SESSION["mycaptcha"] == $code && $this->session->userdata("used")==0)
				{
					$this->session->unset_userdata("mycaptcha");
					//$this->session->set_userdata("mycaptcha", rand(1,100000));
					$this->session->set_userdata("used", 1);
					return true;
				}
		
	}

    public function logout()

	{
		
        session_destroy();

	    
         unset($_SESSION);
		redirect(base_url().'superadminlogin/suplogin');
	 }

	
	
}//C_welcome