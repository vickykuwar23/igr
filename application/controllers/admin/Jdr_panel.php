<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jdr_panel extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		$this->chk_session->chk_sro_session();
    }

    public function index($value='')
    {
    	$data['records']=$this->master_model->getRecords('gri_complaint','','',array('c_id'=>'DESC'));
		$data['middlecontent']='jdr_panel/index';
	    $this->load->view('admin/admin_combo',$data);
    }


    public function complaindetail()

	{

		$complain_id=$this->uri->segment(4);

		// if($complain_id=='')

		// {
		// 	redirect(base_url().'index.php/adminpanel');
		// }

		if(isset($_POST['reply']))

		{

			$this->form_validation->set_rules('reply_msg','Message','trim|required|xss_clean');
			if($this->form_validation->run())

			{

				 $reply_msg=$this->input->post('reply_msg');
				
				 $insert_arr=array('reply_msg'=>$reply_msg,

				 								// 'user_id'=>$this->session->userdata('adminid'),
				 								'user_id'=>52,

												'reply_date'=>date('Y-m-d'),

												'complain_id'=>$complain_id,

												'complaint_status'=>'1'

											   );

					if($insert_id=$this->master_model->insertRecord('complainreply',$insert_arr,true))
					{
						$complainid=array();

						$previous_reply=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id,'replyby !='=>''),'replyby');		

						if(is_array($previous_reply) && count($previous_reply) > 0)

						{

							$complainid=explode(',',$previous_reply[0]['replyby']);

						}

						if(is_array($complainid) && count($complainid) >0)

						{

							if(!in_array($this->session->userdata('adminid'),$complainid))

							{

								$complainid[]=$this->session->userdata('adminid');

							}

						$strrepliedid=implode(',',$complainid);

						}

						else

						{

							$strrepliedid=$this->session->userdata('adminid');

						}


						$update_arr=array('status'=>'1','replyby'=>$strrepliedid);
						$condition=array('c_id'=>"'".$complain_id."'");

						$this->master_model->updateRecord('gri_complaint',$update_arr,$condition);

					   //echo $this->db->last_query();exit;

						//get admin information	

						$this->db->join('departments','departments.id=adminlogin.type');

						$admin_email=$this->master_model->getRecords('adminlogin',array('adminlogin.id'=>$this->session->userdata('adminid')));						

					    // get user information

						$this->db->join('userregistration','userregistration.user_id=gri_complaint.user_id');

						$user_info=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id));

					

						//email admin sending code 

						$info_arr=array(

						'to'=>$user_info[0]['user_email'],

						'from'=>$this->config->item('MAIL_FROM'),//$admin_email[0]['email']
						
						// 'cc'=>$admin_email[0]['emailcc'],
						'cc'=>'vishal.phadol@esds.co.in',


						'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$user_info[0]['comp_code'].'',

						'view'=>'complain_reply_email'

						);

						$other_info=array('name'=>$user_info[0]['user_name'],

													 'pattern'=>$user_info[0]['comp_code'],

													 'user_email'=>$user_info[0]['user_email'],

													 'user_mobile'=>$user_info[0]['user_mobile'],

													 'msg_content'=>$user_info[0]['grievence_details'],

													 'replyremark'=>$reply_msg,

													'reply_status'=>$user_info[0]['reply_status']);
						//sending email with info to user

						if($this->email_sending->sendmail($info_arr,$other_info))

						{  
							$this->session->set_flashdata('success_message','Reply has been post successfully');
							 redirect(base_url().'admin/jdr_panel/complaindetail/'.$complain_id);
					    }

					}

				}

			}



		if(isset($_POST['btn_update']))

		{

			$this->form_validation->set_rules('update_reply_msg','Message','trim|required|xss_clean');

			if($this->form_validation->run())

			{

				 $reply_msg=$this->input->post('update_reply_msg');

				 $reply_id=$this->input->post('reply_id');

				

				 $update_arr=array('reply_msg'=>$reply_msg,

				 								'user_id'=>$this->session->userdata('adminid'),

												'reply_date'=>date('Y-m-d'),

												'complain_id'=>$complain_id,

												'complaint_status'=>'1'

											   );

					if($this->master_model->updateRecord('complainreply',$update_arr,array('reply_id'=>$reply_id)))

					{

						 $this->session->set_flashdata('success_reply','Replied message has been updated successfully');

						  redirect(base_url().'admin/jdr_panel/complaindetail/'.$complain_id.'#reply_id');

					}
 
				}

			}


		if(isset($_POST['status_btn']))

		{

				$reply_status=$this->input->post('reply_type_status');
				if($reply_status==0)
				{
					 $closed_date=date('Y-m-d');
					 $update_arr=array('reply_status'=>$reply_status,

				 								'closed_date'=>$closed_date
											   );
					if($this->master_model->updateRecord('gri_complaint',$update_arr,array('c_id'=>$complain_id)))

					{

						//get admin information	

						$this->db->join('departments','departments.id=adminlogin.type');

						$admin_email=$this->master_model->getRecords('adminlogin',array('adminlogin.id'=>$this->session->userdata('adminid')));	

						// get user information

						$this->db->join('userregistration','userregistration.user_id=gri_complaint.user_id');

						$user_info=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id));

					    $complainreply=$this->master_model->getRecords('complainreply',array('complain_id'=>$complain_id));

						//email admin sending code 

						$info_arr=array(

						'to'=>$user_info[0]['user_email'],

						'from'=>'esdstesting1@esds.co.in',//$admin_email[0]['email']
						
						// 'cc'=>$admin_email[0]['emailcc'],

						'cc'=>'visha.phadol@esds.co.in',


						'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$user_info[0]['comp_code'].'',

						'view'=>'complain_reply_email'

						);

						$other_info=array('name'=>$user_info[0]['user_name'],

													 'pattern'=>$user_info[0]['comp_code'],

													 'user_email'=>$user_info[0]['user_email'],

													 'user_mobile'=>$user_info[0]['user_mobile'],

													 'msg_content'=>$user_info[0]['grievence_details'],

													 'replyremark'=>$complainreply[0]['reply_msg'],

													'reply_status'=>$user_info[0]['reply_status']);

                         if($this->email_sending->sendmail($info_arr,$other_info))

						 {
							$superadmin_email=$this->master_model->getRecords('adminlogin',array('type'=>'superadmin'));	

							//email admin sending code 

							$admin_info_arr=array(

							'to'=>$admin_email[0]['email'],
							
							'cc'=>'',

							'from'=>'esdstesting1@esds.co.in',//$superadmin_email[0]['email']

							'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$user_info[0]['pattern'].'',

							'view'=>'complain_reply_email'

							);

							$admin_other_info=array('name'=>$user_info[0]['user_name'],

																'pattern'=>$user_info[0]['comp_code'],

																'user_email'=>$user_info[0]['user_email'],

																'user_mobile'=>$user_info[0]['user_mobile'],

																'reply_status'=>$user_info[0]['reply_status']);

					
							//sending email with info to admin

							if($this->email_sending->sendmail($admin_info_arr,$admin_other_info))
							{
								 $this->session->set_flashdata('success_reply_status','Grievance has been closed');
								 redirect(base_url().'admin/jdr_panel/complaindetail/'.$complain_id.'#reply_status_id');
							}
						 }
					}

				}
				else
				{
					 $this->session->set_flashdata('success_reply_status','Grievance alredy closed');
					  redirect(base_url().'admin/jdr_panel/complaindetail/'.$complain_id.'#reply_status_id');
								 
				}
		}

		

		$data['error_msg']='';

		$data['error_up']='';


		 // $user=$this->master_model->getRecords('userregistration',array('user_id'=>$details['user_id']));
		 //        $complaint_type=$this->master_model->getRecords('gri_complaint_type',array('complaint_type_id'=>$details['complaint_type_id']));
		 //        $office=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$details['complaint_sub_type_id']));
		 //        $subject=$this->master_model->getRecords('gri_subject',array('sub_id'=>$details['comp_subject']));

        	$this->db->join('gri_complaint_type','gri_complaint_type.complaint_type_id=gri_complaint.complaint_type_id');	
        	
        	$this->db->join('gri_complaint_sub_type','gri_complaint_sub_type.complaint_sub_type_id=gri_complaint.complaint_sub_type_id');	
			
			$this->db->join('gri_subject','gri_subject.sub_id=gri_complaint.comp_subject');
		$data['complaint']=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id));

		

		$data['middlecontent']='jdr_panel/details';
	    $this->load->view('admin/admin_combo',$data);

		

		}
	
}