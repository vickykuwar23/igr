<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends CI_Controller {

	public function change_password()
	{

		$user_id= $this->session->userdata('supadminid');
		
		$data['error_msg']='';
		
		if(isset($_POST['btn_set']) && $user_id!='')
		{
			// $this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_createpass');
			
			$this->form_validation->set_rules('pass','Password','trim|required|min_length[8]|xss_clean');
			$this->form_validation->set_rules('con_password','Confirm Password','trim|required|xss_clean');
			if($this->form_validation->run())
			{


				$pass=$this->input->post('pass');
				$con_password=$this->input->post('con_password');
				$update_arr=array(
										 'adminpass'=>sha1($pass)
										);
				if($insert_id=$this->master_model->updateRecord('adminlogin',$update_arr,array('id'=>$user_id)))
					{
						
						$this->session->set_flashdata('success_message','You have successfully changed your password.');
						redirect(base_url('admin/profile/change_password'));
					}
				
			}		
		}
			
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
	    $_SESSION["createpass"]=$cap['word'];
	   $data['middlecontent']='profile/change_password';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	
	 public function check_captcha_forgetpass($code) 

	{
		      // check if captcha is set -
				if($code == '' || $_SESSION["forgetpass"] != $code )
				{
					$this->form_validation->set_message('check_captcha_forgetpass', 'Invalid %s.'); 
					$this->session->set_userdata("forgetpass", rand(1,100000));
					
					return false;
				}
				if($_SESSION["forgetpass"] == $code)
				{
					$this->session->unset_userdata("forgetpass");
					return true;
				}
		
	}
	
	

	 public function sha256($pass)
	{
		return hash('sha256',$pass);
	}
	
}

