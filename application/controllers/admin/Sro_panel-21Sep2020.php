<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sro_panel extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		$this->chk_session->chk_sro_session();
		$this->load->helper('form');
    }

    public function index($value='')
    {
    	$adminid = $this->session->userdata('supadminid');
    	$sro_id = $this->session->userdata('sro_id');
    	$district_id = $this->session->userdata('district_id');
		$data['distict_details'] = $this->master_model->getRecords('gri_district',array('religion_id' => '4'),'',array('district_id'=>'ASC'));
    	$where = array();
    	$type = $this->session->userdata('type');
		
		if(isset($_POST['btn_export']) && !empty($_POST['btn_export'])) 
		{
			error_reporting(0);
			$c_status 	= @$this->input->post('c_status')?$this->input->post('c_status'):'1';
			$c_type 	= @$this->input->post('c_type')?$this->input->post('c_type'):'';
			$com_code 	= @$this->input->post('com_code')?$this->input->post('com_code'):'';			
			$district_id 	= @$this->input->post('district_id')?$this->input->post('district_id'):'';
			$c_subtype 		= @$this->input->post('c_subtype')?$this->input->post('c_subtype'):'';
			$sro_office_id 	= @$this->input->post('sro_office')?$this->input->post('sro_office'):'';
			$username 	= @$this->input->post('username')?$this->input->post('username'):'';
		
		$type = $this->session->userdata('type');
							 
		$condition = "SELECT c.comp_code, c.c_id, ct.complaint_type_name, c.comp_subject, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, u.user_name, c.reply_status, c.complaint_date FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id
						JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code		
						JOIN userregistration u ON c.user_id = u.user_id WHERE c.comp_code!='' AND ";
		
				if($c_status){	
					
					if($c_status == 4){
						
						$c_status = 0;
						$condition .= " c.reply_status = '".$c_status."'";
						
					} else if($c_status == -1){
						
						$condition .= " c.reply_status IN(0,1,2,3)";
						
					} else {
						
						$condition .= " c.reply_status = '".$c_status."'";
					}
					
				} 
				
				if($com_code){
					
					$condition .= " AND c.comp_code LIKE '%".strtoupper($com_code)."%'";
				}	
				
				if($username){
			
					$condition .= " AND u.user_name ILIKE '%".$username."%'";
				}
				
				
				
				if($c_type){
					
					$condition .= " AND c.complaint_type_id = '".$c_type."'";
				}
				
				if($district_id){
					
					$condition .= " AND c.district_id = '".$district_id."'";
				}
				
				if($c_subtype){
					
					if($c_type == 2){
						$condition .= " AND c.complaint_sub_type_id = '".$c_subtype."'";
					} else {
						$condition .= " AND c.online_service_id = '".$c_subtype."'";
					}
					
				}			
				
				
					// sro user
			    	if ($type==1) {
			    		
						$sro_id = $this->session->userdata('sro_id');						
						$condition .= " AND c.sro_office_id = '".$sro_id."' AND ca.status = '1'";

			    	}
					
					// marriage Officer user
					if ($type==15) {						
						$sro_id = $this->session->userdata('sro_id');
						$condition .= " AND c.sro_office_id = '".$sro_id."' AND ca.assign_user_id!= '8' AND ca.status = '1'";
					
					}

			    	// jdr user
			    	if ($type==2) {
			    		
						$district_id = $this->session->userdata('district_id');
						// $condition .= " AND c.district_id = '".$district_id."' AND c.complaint_sub_type_id != '2' AND ca.assign_user_id!= '8' AND ca.status = '1'";
						$condition .= " AND c.district_id = '".$district_id."'  AND c.complaint_sub_type_id != '2' AND ca.status = '1'";
						$condition .= " AND (c.is_level_2 = '1' OR c.is_level_3 = '1') ";
			    	}
					
					// igr user
			    	if ($type==3) {
			    		
			    		$condition .= " AND c.is_level_3 = '1' ";

			    	}

			    	// help desk user
					if($type == 4){
						$condition .= " AND c.complaint_type_id = '1' AND ca.status = '1'";
					} 

					//CSO
					if ($type==7) {
						$sro_id = $this->session->userdata('sro_id');
			    		$condition .= " AND c.sro_office_id = '".$sro_id."'  AND ca.status = '1'";

			    	}	
					
					
					//ACS
					if ($type==9) { 
						$district_id = $this->session->userdata('district_id');
						$condition .= " AND c.district_id = '".$district_id."' AND c.complaint_sub_type_id = '2'  AND ca.status = '1'";
						$condition .= " AND (c.is_level_2 = '1' OR c.is_level_3 = '1') ";
			    	}


				// superadmin user
				if ($type==999) {
					
					$condition .= "";
					
					if($sro_office_id){
					
						$condition .= " AND c.sro_office_id = '".$sro_office_id."'";
					}
					
				}
					
				
				$condition .= " GROUP BY c.comp_code, c.c_id, ct.complaint_type_name,u.user_name, c.comp_subject ORDER BY (CASE reply_status WHEN 1 THEN 0 WHEN 0 THEN 1 WHEN 2 THEN 2 WHEN 3 THEN 3 END) ASC,c.complaint_date ASC";
				
				//echo $condition; die();
				$result 	= $this->db->query($condition)->result();	
				$rowCount 	= $this->db->query($condition)->num_rows();					
				
				 require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
				 require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");

				 $objPHPExcel = new PHPExcel();

				 $objPHPExcel->getProperties()->setCreator("");
				 $objPHPExcel->getProperties()->setLastModifiedBy("");
				 $objPHPExcel->getProperties()->setTitle("");
				 $objPHPExcel->getProperties()->setSubject("");
				 $objPHPExcel->getProperties()->setDescription("");

				 $objPHPExcel->setActiveSheetIndex(0);

				 $sheet = $objPHPExcel->getActiveSheet();
				 
				 
				 if($this->session->userdata('type') == '999' || $this->session->userdata('type') == '3'){
					
						 $sheet->setCellValue("A1","Complaint Code");
						 $sheet->setCellValue("B1","Fullname");
						 $sheet->setCellValue("C1","District");
						 $sheet->setCellValue("D1","Complaint Type");
						 $sheet->setCellValue("E1","Office Type");
						 $sheet->setCellValue("F1","Subject");
						 $sheet->setCellValue("G1","Assign To");
						 $sheet->setCellValue("H1","Office Name");
						 $sheet->setCellValue("I1","Complaint Status");
					
					} else if($this->session->userdata('type') == '2' || $this->session->userdata('type') == '9'){
						
						 $sheet->setCellValue("A1","Complaint Code");
						 $sheet->setCellValue("B1","Fullname");
						 $sheet->setCellValue("C1","Complaint Type");
						 $sheet->setCellValue("D1","Office Type");
						 $sheet->setCellValue("E1","Subject");
						 $sheet->setCellValue("F1","Assign To");
						 $sheet->setCellValue("G1","Office Name");
						 $sheet->setCellValue("H1","Complaint Status");
						   
					}	else if($this->session->userdata('type') == '4'){
						
						 $sheet->setCellValue("A1","Complaint Code");
						 $sheet->setCellValue("B1","Fullname");
						 $sheet->setCellValue("C1","Complaint Type");
						 $sheet->setCellValue("D1","Office Type");
						 $sheet->setCellValue("E1","Assign To");
						 $sheet->setCellValue("F1","Complaint Status");	
						 
					} else {
						
						 $sheet->setCellValue("A1","Complaint Code");
						 $sheet->setCellValue("B1","Fullname");
						 $sheet->setCellValue("C1","Complaint Type");
						 $sheet->setCellValue("D1","Subject");
						 $sheet->setCellValue("E1","Assign To");
						 $sheet->setCellValue("F1","Office Name");
						 $sheet->setCellValue("G1","Complaint Status");						
					}

				 $row = 2;
				
				 foreach ($result as $key => $value)
				 { 		
				 
						$comCodes = $value->comp_code;
						$districtID = $value->district_id;
						$regionID = $value->religion_id;
						$subID = $value->comp_subject;
						if($value->complaint_sub_type_id == 0){
							$compSubType = $value->online_service_id;
						} else {
							$compSubType = $value->complaint_sub_type_id;
						}
						//$compSubType = $comData->complaint_sub_type_id;
						
						// Get Complaint Assign Officer ID
						$getAssigneedetails = $this->master_model->getRecords('gri_complaint_assign',array('comp_code'=>$comCodes, 'status' => '1'));
						$assigneeID = $getAssigneedetails[0]['assign_user_id'];	
						// Get Admin Details
						$getAdminType = $this->master_model->getRecords('adminlogin',array('id'=>$assigneeID, 'status' => '1'));
						$assigneeName = $getAdminType[0]['role_id'];
						$officeId 	  = $getAdminType[0]['sro_id'];
						// Get Roll Name
						$getAdminName = $this->master_model->getRecords('gri_roles',array('role_id'=>$assigneeName, 'status' => '1'));
						$assignName = $getAdminName[0]['role_name'];				
						// Get SRO Office Name
						$getOfficeName = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$officeId, 'status' => '1'));
						$officeName = $getOfficeName[0]['office_name'];				
						// Get District
						$getDistrict = $this->master_model->getRecords('gri_district',array('district_id'=>$districtID, 'status' => '1'));
						$districtName = $getDistrict[0]['district_name'];				
						// Get Region
						$getRegion = $this->master_model->getRecords('gri_region_division',array('religion_id'=>$regionID, 'status' => '1'));
						$regionName = $getRegion[0]['region_division_name'];
						// Get Subject
						$getSub = $this->master_model->getRecords('gri_subject',array('sub_id'=>$subID, 'status' => '1'));
						$subjectName = $getSub[0]['subject_name'];
						
						// Get Office Type
						$getOfficeDetail = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$compSubType, 'status' => '1'));
						$complaintSubTypeName = $getOfficeDetail[0]['complaint_sub_type_name'];
					
				 
					if($value->reply_status == 1){$status = 'Pending';} 
						else if($value->reply_status == 2){$status = 'Fake';}
						else if($value->reply_status == 0){$status = 'Closed';}
						else if($value->reply_status == 3){$status = 'Query To Complainant';}	

					

					if($this->session->userdata('type') == '999' || $this->session->userdata('type') == '3'){
					
						 $sheet->setCellValue("A".$row,$value->comp_code);
						 $sheet->setCellValue("B".$row,$value->user_name);
						 $sheet->setCellValue("C".$row,$districtName);
						 $sheet->setCellValue("D".$row,$value->complaint_type_name);
						 $sheet->setCellValue("E".$row,$complaintSubTypeName);
						 $sheet->setCellValue("F".$row,$subjectName);
						 $sheet->setCellValue("G".$row,$assignName);
						 $sheet->setCellValue("H".$row,$officeName);
						 $sheet->setCellValue("I".$row,$status);
						 $row++;
					
					} else if($this->session->userdata('type') == '2' || $this->session->userdata('type') == '9'){
						
						 $sheet->setCellValue("A".$row,$value->comp_code);
						 $sheet->setCellValue("B".$row,$value->user_name);
						 $sheet->setCellValue("C".$row,$value->complaint_type_name);
						 $sheet->setCellValue("D".$row,$complaintSubTypeName);
						 $sheet->setCellValue("E".$row,$subjectName);
						 $sheet->setCellValue("F".$row,$assignName);
						 $sheet->setCellValue("G".$row,$officeName);
						 $sheet->setCellValue("H".$row,$status);
						 $row++;
						   
					}	else if($this->session->userdata('type') == '4'){
						
								 
						 $sheet->setCellValue("A".$row,$value->comp_code);
						 $sheet->setCellValue("B".$row,$value->user_name);
						 $sheet->setCellValue("C".$row,$value->complaint_type_name);
						 $sheet->setCellValue("D".$row,$complaintSubTypeName);
						 $sheet->setCellValue("E".$row,$assignName);
						 $sheet->setCellValue("F".$row,$status);
						 $row++;
						
					}	else {
						
								 
						 $sheet->setCellValue("A".$row,$value->comp_code);
						 $sheet->setCellValue("B".$row,$value->user_name);
						 $sheet->setCellValue("C".$row,$value->complaint_type_name);
						 $sheet->setCellValue("D".$row,$subjectName);
						 $sheet->setCellValue("E".$row,$assignName);
						 $sheet->setCellValue("F".$row,$officeName);
						 $sheet->setCellValue("G".$row,$status);
						 $row++;
						
					}						
					 
					 
				 }

				
				 $filename = "File-Download-on-".date("Y-m-d-H-i-s").".xls";
				 $sheet->setTitle("Export File");
	
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");;
				header("Content-Disposition: attachment;filename=$filename");

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
				 //force user to download the Excel file without writing it to server's HD
				$objWriter->save('php://output');
				set_time_limit(30);
				exit;
			
		}

    	$data['records']=$this->master_model->getRecords('gri_complaint',$where,'',
    		array('createdat'=>'ASC',) );
    	
		$data['middlecontent']='sro_panel/index';
	    $this->load->view('admin/admin_combo',$data);
    }	

	public function filterdata(){	
		
		error_reporting(0);
		$c_status 		= @$this->input->post('c_status')?$this->input->post('c_status'):'1';
		$c_type 		= @$this->input->post('c_type')?$this->input->post('c_type'):'';
		$com_code 		= @$this->input->post('com_code')?$this->input->post('com_code'):'';		
		$district_id 	= @$this->input->post('district_id')?$this->input->post('district_id'):'';
		$c_subtype 		= @$this->input->post('c_subtype')?$this->input->post('c_subtype'):'';
		$sro_office_id 	= @$this->input->post('sro_office')?$this->input->post('sro_office'):'';
		$username 		= @$this->input->post('username')?$this->input->post('username'):'';
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');
		$csrf_token = $this->security->get_csrf_hash();
		
		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value
		$pageNo = ($_POST['start']/$_POST['length'])+1;		
		
		/*$condition = "SELECT DISTINCT(c.comp_code), c.c_id, ct.complaint_type_name, c.comp_subject, c.sro_office_id, c.district_id, c.complaint_type_id, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, u.user_name, c.reply_status, c.complaint_date FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code		
						JOIN userregistration u ON c.user_id = u.user_id WHERE c.comp_code!='' AND ";*/
						
		$condition = "SELECT c.comp_code, c.c_id, ct.complaint_type_name, c.comp_subject, c.sro_office_id, c.district_id, c.complaint_type_id, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, u.user_name, c.reply_status, c.complaint_date FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code		
						JOIN userregistration u ON c.user_id = u.user_id WHERE c.comp_code!='' AND ";
				
		
		if($c_status){	
			
			if($c_status == 4){
				
				$c_status = 0;
				$condition .= " c.reply_status = '".$c_status."'";
				
			} else if($c_status == -1){
				$condition .= " c.reply_status IN(0,1,2,3)";
			} else {
				
				$condition .= " c.reply_status = '".$c_status."'";
			}
			
		} 
		
		
		
		if($com_code){
			
			$condition .= " AND c.comp_code LIKE '%".strtoupper($com_code)."%'";
		}	
		
		if($username){
			
			$condition .= " AND u.user_name ILIKE '%".$username."%'";
		}
		
		
		
		if($c_type){
			
			$condition .= " AND c.complaint_type_id = '".$c_type."'";
		}
		
		if($district_id){
			
			$condition .= " AND c.district_id = '".$district_id."'";
		}
		
		if($c_subtype){
			
			if($c_type == 2){
				$condition .= " AND c.complaint_sub_type_id = '".$c_subtype."'";
			} else {
				$condition .= " AND c.online_service_id = '".$c_subtype."'";
			}
			
		}
		
		if($sro_office_id){
			
			$condition .= " AND c.sro_office_id = '".$sro_office_id."'";
		}
		
		// sro user
    	if ($type==1) {
    		
			$sro_id = $this->session->userdata('sro_id');
			$condition .= " AND c.sro_office_id = '".$sro_id."' AND ca.status = '1'";

    	}
		
		// marriage Officer user
		if ($type==15) {
			
			$sro_id = $this->session->userdata('sro_id');
			$condition .= " AND c.sro_office_id = '".$sro_id."' AND ca.assign_user_id!= '8' AND ca.status = '1'";
		
		}

    	// jdr user
    	if ($type==2) {
    		
			$district_id = $this->session->userdata('district_id');
			$condition .= " AND c.district_id = '".$district_id."'  AND c.complaint_sub_type_id != '2' AND ca.status = '1'";
			$condition .= " AND (c.is_level_2 = '1' OR c.is_level_3 = '1') ";
    	}
		
		// igr user
    	if ($type==3) {
    		
    		$condition .= " AND c.is_level_3 = '1' ";

    	}

    	// help desk user
		if($type == 4){
			
			$condition .= " AND c.complaint_type_id = '1' AND ca.status = '1'";

		} 

		//CSO
		if ($type==7) {
			$sro_id = $this->session->userdata('sro_id');
    		$condition .= " AND c.sro_office_id = '".$sro_id."'  AND ca.status = '1'";

    	}	
		
		
		//ACS
		if ($type==9) { 
			$district_id = $this->session->userdata('district_id');
			$condition .= " AND c.district_id = '".$district_id."' AND c.complaint_sub_type_id = '2'  AND ca.status = '1'";
			$condition .= " AND (c.is_level_2 = '1' OR c.is_level_3 = '1') ";
    	}


    	// superadmin user
    	if ($type==999) {
    		$condition .= "";
    	}
			
		$condition .= " GROUP BY c.comp_code, c.c_id, ct.complaint_type_name, u.user_name, c.comp_subject ORDER BY  (CASE reply_status WHEN 1 THEN 0 WHEN 0 THEN 1 WHEN 2 THEN 2 WHEN 3 THEN 3 END) ASC, c.complaint_date ASC ";
		// (CASE reply_status WHEN 1 THEN 0 WHEN 0 THEN 1 WHEN 2 THEN 2 WHEN 3 THEN 3 END) ASC, 
		//echo $condition;die();
		$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
		
		$result 	= $this->db->query($query)->result();		
				
		$rowCount = $this->getNumData($condition);
		
		$rowCnt = 0;
		$dataArr = array();
		
		if($rowCount > 0){
			
			$no    = $_POST['start'];
			foreach($result as $comData){
				
				$comCodes = $comData->comp_code;
				$districtID = $comData->district_id;
				$regionID = $comData->religion_id;
				$subID = $comData->comp_subject;
				if($comData->complaint_sub_type_id == 0){
					$compSubType = $comData->online_service_id;
				} else {
					$compSubType = $comData->complaint_sub_type_id;
				}
				//$compSubType = $comData->complaint_sub_type_id;
				
				// Get Complaint Assign Officer ID
				$getAssigneedetails = $this->master_model->getRecords('gri_complaint_assign',array('comp_code'=>$comCodes, 'status' => '1'));
				$assigneeID = $getAssigneedetails[0]['assign_user_id'];	
				// Get Admin Details
				$getAdminType = $this->master_model->getRecords('adminlogin',array('id'=>$assigneeID, 'status' => '1'));
				$assigneeName = $getAdminType[0]['role_id'];
				$officeId 	  = $getAdminType[0]['sro_id'];
				// Get Roll Name
				$getAdminName = $this->master_model->getRecords('gri_roles',array('role_id'=>$assigneeName, 'status' => '1'));
				$assignName = $getAdminName[0]['role_name'];				
				// Get SRO Office Name
				$getOfficeName = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$officeId, 'status' => '1'));
				$officeName = $getOfficeName[0]['office_name'];				
				// Get District
				$getDistrict = $this->master_model->getRecords('gri_district',array('district_id'=>$districtID, 'status' => '1'));
				$districtName = $getDistrict[0]['district_name'];				
				// Get Region
				$getRegion = $this->master_model->getRecords('gri_region_division',array('religion_id'=>$regionID, 'status' => '1'));
				$regionName = $getRegion[0]['region_division_name'];
				// Get Subject
				$getSub = $this->master_model->getRecords('gri_subject',array('sub_id'=>$subID, 'status' => '1'));
				$subjectName = $getSub[0]['subject_name'];
				
				// Get Office Type
				$getOfficeDetail = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$compSubType, 'status' => '1'));
				$complaintSubTypeName = $getOfficeDetail[0]['complaint_sub_type_name'];
				
				if($comData->reply_status == 1){$status = 'Pending';} 
				else if($comData->reply_status == 2){$status = 'Fake';}
				else if($comData->reply_status == 0){$status = 'Closed';}
				else if($comData->reply_status == 3){$status = 'Query To Complainant';}
				$url = base_url('admin/sro_panel/complaindetail/'. $comData->c_id);
				$action = '<a href="'. $url .'" title=""><i class="fa fa-eye"></i></a>';
				//$action = '';
				$no++;
				if($this->session->userdata('type') == '999' || $this->session->userdata('type') == '3'){
					$dataArr[] = array(
						  $no,				  
						  $comData->comp_code,
						  $comData->user_name,
						  $districtName,
						  $comData->complaint_type_name,
						  $complaintSubTypeName,
						  $subjectName,
						  $assignName,
						  $officeName,
						  $status,
						  $action
					   );
				} else if($this->session->userdata('type') == '2' || $this->session->userdata('type') == '9'){
					
						$dataArr[] = array(
						  $no,				  
						  $comData->comp_code,
						  $comData->user_name,
						  $comData->complaint_type_name,
						  $complaintSubTypeName,
						  $subjectName,
						  $assignName,
						  $officeName,
						  $status,
						  $action
					   );
					   
				}	else if($this->session->userdata('type') == '4'){
					
						$dataArr[] = array(
						  $no,				  
						  $comData->comp_code,
						  $comData->user_name,
						  $comData->complaint_type_name,
						  $complaintSubTypeName,
						  $assignName,
						  $status,
						  $action
					   );
					   
				} else {
					
					$dataArr[] = array(
						  $no,				  
						  $comData->comp_code,
						  $comData->user_name,
						  $comData->complaint_type_name,
						  $subjectName,
						  $assignName,						  
						  $status,
						  $action
					   );
					
				}
				
			   
			    $rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
				);
				
				
			}
			
		} else {
			
			$rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => 0,
				  "recordsFiltered" => 0,
				  "data" => $dataArr
				);
		}
		echo json_encode($response);
		
	}
	
	public function getNumData($query){
		
		return $rowCount = $this->db->query($query)->num_rows();
	}
	
	
	public function getOfficesub(){
		$csrf_token = $this->security->get_csrf_hash();
		$office_sub_type = $this->input->post('office_type_id');
		$options = "";
		$complaint_subtype_details = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => $office_sub_type, 'status' => '1'),'',array('complaint_type_id'=>'DESC'));
		
		if(count($complaint_subtype_details)>0)	{
			$options .= '<option value="">-- Select --</option>';
			foreach($complaint_subtype_details as $subType){				
				$options .= '<option value="'.$subType['complaint_sub_type_id'].'" >'.$subType['complaint_sub_type_name'].'</option>';
			}			
		}
		echo json_encode(['options' => $options, 'csrf_token' => $csrf_token ]);
	} 
	
	public function getOfficeslist(){
		$csrf_token = $this->security->get_csrf_hash();
		$office_sub_type = $this->input->post('office_sub_id');
		$c_district_id   = $this->input->post('district_id');
		$options = "";
		$sro_office =	$this->master_model->getRecords('gri_sro_offices',array('district_id' => $c_district_id, 'office_sub_type' => $office_sub_type, 'status' => '1'),'',array('sro_office_id'=>'ASC'));
		
		if(count($sro_office)>0)	{
			$options .= '<option value="">-- Select --</option>';	
			foreach($sro_office as $officeName){				
				$options .= '<option value="'.$officeName['sro_office_id'].'" >'.$officeName['office_name'].'</option>';
			}			
		}
		echo json_encode(['options' => $options, 'csrf_token' => $csrf_token ]);
	}

    public function complaindetail()
	{   //echo ">>>".$this->session->userdata('supadminid');
		$error = '';
		$complain_id=$this->uri->segment(4);

		if(isset($_POST['reply']))

		{


			$this->form_validation->set_rules('reply_msg','Message','trim|required|xss_clean');
			if($this->form_validation->run())

			{

				 $reply_msg=$this->input->post('reply_msg');
				
				 $insert_arr=array(
									'user_id'=>$this->session->userdata('supadminid'),
									'reply_msg'=>$reply_msg,
									'reply_date'=>date('Y-m-d'),
									'complain_id'=>$complain_id,
									'complaint_status'=>'1',
									'created_at'=>date('Y-m-d')
								   );
	         

                $this->load->library('upload');
                $config['upload_path'] 	 = 'images/reply_uploads';
                $config['allowed_types'] = '*';
                $config['max_size']	 = '500000000';
                $config['overwrite']	 = TRUE;
             
                if($_FILES['reply_file']['name'])
                {
	        		$file_dets1 = $_FILES["reply_file"]['name'];
	                $ext = pathinfo($file_dets1, PATHINFO_EXTENSION);
	                $file_dets1 ='file_'.round(microtime(true)).'.'.$ext;

                    $config['file_name'] = $file_dets1;
                    $file = $config['file_name'];
                    $this->upload->initialize($config); // Important

                    if(!$this->upload->do_upload('reply_file'))
                    {
                        $error.= $this->upload->display_errors();
                        $this->session->set_flashdata('error_message',$this->upload->display_errors());
                         redirect(base_url().'admin/sro_panel/complaindetail/'.$complain_id);
                    }
               	 	
               	 	$insert_arr['reply_file'] = $file;

                }


					if($insert_id=$this->master_model->insertRecord('complainreply',$insert_arr,true))
					{
				// add log
						$json_encode_data = json_encode($insert_arr);
						$log_user_id      = $this->session->userdata('supadminid');
						$ipAddr			  = $this->get_client_ip();
						$logDetails = array(
											'module_name' 	=> "Sro_panel",
											'action_name' 	=> "Add Reply",
											'json_response'	=> $json_encode_data,
											'login_id'		=> $log_user_id,
											'role_type'		=> $this->session->userdata('role_name'),
											'createdAt'		=> date('Y-m-d H:i:s'),
											'ip_addr'		=> $ipAddr
											);
						$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
						// end add log
						$complainid=array();

						$previous_reply=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id,'replyby !='=>null));		

						if(is_array($previous_reply) && count($previous_reply) > 0)

						{

							$complainid=explode(',',$previous_reply[0]['replyby']);

						}

						if(is_array($complainid) && count($complainid) >0)

						{

							if(!in_array($this->session->userdata('supadminid'),$complainid))

							{

								$complainid[]=$this->session->userdata('supadminid');

							}

						$strrepliedid=implode(',',$complainid);

						}

						else

						{

							$strrepliedid=$this->session->userdata('supadminid');

						}


						$update_arr=array('status'=>'1','replyby'=>$strrepliedid);
						$condition=array('c_id'=>"'".$complain_id."'");

						$this->master_model->updateRecord('gri_complaint',$update_arr,$condition);

						$admin_email=$this->master_model->getRecords('adminlogin',array('adminlogin.id'=>$this->session->userdata('adminid')));						

					    // get user information
						$this->db->join('userregistration','userregistration.user_id=gri_complaint.user_id');

						$user_info=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id));



					
						$user_email=$user_info[0]['user_email'];
						//email admin sending code 

						$info_arr=array(
							'to'=>$user_email,
							'cc'=>'',
							'from'=>$this->config->item('MAIL_FROM'),

							'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$user_info[0]['comp_code'].'',

							'view'=>'complain_reply_email'

						);

						$other_info=array('name'=>$user_info[0]['user_name'],

													 'pattern'=>$user_info[0]['comp_code'],

													 'user_email'=>$user_info[0]['user_email'],

													 'user_mobile'=>$user_info[0]['user_mobile'],

													 'msg_content'=>$user_info[0]['grievence_details'],

													 'replyremark'=>$reply_msg,

													'reply_status'=>$user_info[0]['reply_status']);


						//sending email with info to user

						if($this->email_sending->sendmail($info_arr,$other_info))

						{  

							$this->session->set_flashdata('success_message','Reply has been post successfully');
							 redirect(base_url().'admin/sro_panel/complaindetail/'.$complain_id);
					    }

						}
					}

			}



		if(isset($_POST['btn_update']))

		{

			$this->form_validation->set_rules('update_reply_msg','Message','trim|required|xss_clean');

			if($this->form_validation->run())

			{

				 $reply_msg=$this->input->post('update_reply_msg');

				 $reply_id=$this->input->post('reply_id');

				

				 $update_arr=array('reply_msg'=>$reply_msg,

				 								'user_id'=>$this->session->userdata('adminid'),

												'reply_date'=>date('Y-m-d'),

												'complain_id'=>$complain_id,

												'complaint_status'=>'1'

											   );

					if($this->master_model->updateRecord('complainreply',$update_arr,array('reply_id'=>$reply_id)))

					{

						 $this->session->set_flashdata('success_reply','Replied message has been updated successfully');

						  redirect(base_url().'admin/sro_panel/complaindetail/'.$complain_id.'#reply_id');

					}

				}

			}


		if(isset($_POST['status_btn']))

		{
				
				$reply_status=$this->input->post('reply_type_status');
				//echo $reply_status;die();
				if($reply_status==0)
				{
					 $closed_date=date('Y-m-d');
					 $update_arr=array(	'reply_status'=>$reply_status,
										'complaint_status'=>$reply_status,
		 								'closed_date'=>$closed_date,
		 								'compaint_closed_by'=>$this->session->userdata('supadminid')
											   );
					if($this->master_model->updateRecord('gri_complaint',$update_arr,array('c_id'=>$complain_id)))

					{ 
						// add log
						$json_encode_data = json_encode($update_arr);
						$log_user_id      = $this->session->userdata('supadminid');
						$ipAddr			  = $this->get_client_ip();
						$logDetails = array(
											'module_name' 	=> "Sro_panel",
											'action_name' 	=> "Close compaint",
											'json_response'	=> $json_encode_data,
											'login_id'		=> $log_user_id,
											'role_type'		=> $this->session->userdata('role_name'),
											'createdAt'		=> date('Y-m-d H:i:s'),
											'ip_addr'		=> $ipAddr
											);
						$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								// end add log

						//get admin information	

						$admin_email=$this->master_model->getRecords('adminlogin',array('adminlogin.id'=>$this->session->userdata('adminid')));	

						// get user information

						$this->db->join('userregistration','userregistration.user_id=gri_complaint.user_id');

						$user_info=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id));

					    $complainreply=$this->master_model->getRecords('complainreply',array('complain_id'=>$complain_id));

						$user_email=$user_info[0]['user_email'];

						$info_arr=array(
						'to'=>$user_email,
						'from'=>$this->config->item('MAIL_FROM'),
						'cc'=>'',
						'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$user_info[0]['comp_code'].'',
						'view'=>'complain_reply_email'
						);
						$other_info=array('name'=>$user_info[0]['user_name'],
													 'pattern'=>$user_info[0]['comp_code'],
													 'user_email'=>$user_info[0]['user_email'],
													 'user_mobile'=>$user_info[0]['user_mobile'],
													 'msg_content'=>$user_info[0]['grievence_details'],
													 'replyremark'=>$complainreply[0]['reply_msg'],
													'reply_status'=>$user_info[0]['reply_status']);
						
						$this->send_sms($user_info[0]['user_mobile'],$user_info[0]['comp_code']);	
                         if($this->email_sending->sendmail($info_arr,$other_info))

						 {

							 $this->session->set_flashdata('success_reply_status','Grievance has been closed');
							 redirect(base_url().'admin/sro_panel/complaindetail/'.$complain_id.'#reply_status_id');
							
						 }
					}

				} else if($reply_status==2){
					
					$closed_date=date('Y-m-d');
					 $update_arr=array('reply_status'=>$reply_status,
										'complaint_status'=>$reply_status,
		 								'closed_date'=>$closed_date,
		 								'compaint_closed_by'=>$this->session->userdata('supadminid')
										);
					$this->master_model->updateRecord('gri_complaint',$update_arr,array('c_id'=>$complain_id));	
					// add log
						$json_encode_data = json_encode($update_arr);
						$log_user_id      = $this->session->userdata('supadminid');
						$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Sro_panel",
										'action_name' 	=> "Fake compaint",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> $this->session->userdata('role_name'),
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								// end add log				
					$this->session->set_flashdata('success_reply_status','Grievance mark as a fake.');
					 redirect(base_url().'admin/sro_panel/complaindetail/'.$complain_id.'#reply_status_id');
						
				}

				else if($reply_status==3){
					$closed_date=date('Y-m-d');
					 $update_arr=array('reply_status'=>$reply_status,
										'complaint_status'=>$reply_status,
		 								'closed_date'=>$closed_date,
		 								'compaint_closed_by'=>$this->session->userdata('supadminid')
										);
					$this->master_model->updateRecord('gri_complaint',$update_arr,array('c_id'=>$complain_id));	
					// add log
					$json_encode_data = json_encode($update_arr);
					$log_user_id      = $this->session->userdata('supadminid');
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Sro_panel",
										'action_name' 	=> "Query to compainant",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> $this->session->userdata('role_name'),
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								// end add log				
					$this->session->set_flashdata('success_reply_status','Grievance mark as a Query to Complainant.');
					 redirect(base_url().'admin/sro_panel/complaindetail/'.$complain_id.'#reply_status_id');
						
				}
				else
				{
					 //$this->session->set_flashdata('success_reply_status','Grievance alredy closed');
					  redirect(base_url().'admin/sro_panel/complaindetail/'.$complain_id.'#reply_status_id');
								 
				}
		}

		

		$data['error_msg']='';

		$data['error_up']='';


			$this->db->join('gri_complaint_type','gri_complaint_type.complaint_type_id=gri_complaint.complaint_type_id','left');	
        	
        	$this->db->join('gri_complaint_sub_type','gri_complaint_sub_type.complaint_sub_type_id=gri_complaint.complaint_sub_type_id','left');	
			
			$this->db->join('gri_subject','gri_subject.sub_id=gri_complaint.comp_subject','left');
		$data['complaint']=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id));
		
		$data['error'] = $error;	

		$data['middlecontent']='sro_panel/details';
	    $this->load->view('admin/admin_combo',$data);

		

		}		
		function show_prev_reply($complain_id){

        	$this->db->join('gri_complaint_type','gri_complaint_type.complaint_type_id=gri_complaint.complaint_type_id','left');	
        	
        	$this->db->join('gri_complaint_sub_type','gri_complaint_sub_type.complaint_sub_type_id=gri_complaint.complaint_sub_type_id','left');	
			
			$this->db->join('gri_subject','gri_subject.sub_id=gri_complaint.comp_subject','left');
			$data['complaint']=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id));
			$data['middlecontent']='sro_panel/prev_reply';
	    	$this->load->view('admin/admin_combo',$data);
		}
			function send_sms($mobile=NULL,$pattern=NULL)
 	{
 		

 		if($mobile!=NULL && $pattern!=NULL)
 		{
 			$text = "Your Grievance Number ".$pattern." has been closed.";
 			$msg = urlencode($text);
 			$url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH";

 			$string = preg_replace('/\s+/', '', $url);
 			$x = curl_init($string);
 			curl_setopt($x, CURLOPT_HEADER, 0);	
 			curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
 			curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);	
 			curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);		
 			$reply = curl_exec($x);		
 		
 		}
 	}

 	function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }

	
}