﻿<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller 
{

	//global items
    public $adminArr;
	public $secondArr;
	public $allArr;
	public $firstLevel;
   function __construct()

	{

		 parent::__construct();
		 $this->load->model('chk_session');
		 $this->load->model('master_model');
		 $adminArr 			= array("999", "3", "11", "12", "13", "14", "16");
		 $secondArr 		= array('3','5','10','11','12','13','14','16','999');
		 $allArr			= array('999','1','3','4','5','7','10','11','12','13','14','15','16');
		 $firstLevel 		= array('999','2','3','4','5','9','10','11','12','13','14', '16');
		 $this->adminArr 	= $adminArr;
		 $this->secondArr 	= $secondArr;
		 $this->allArr 		= $allArr;
		 $this->firstLevel 	= $firstLevel;
		 
		 // $this->chk_session->chk_callcenter_session();
		if($this->session->userdata('type')==""){
			redirect('superadminlogin/suplogin');
		}
		
	}

	public function test_excel()
	{
		require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
		require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");			
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		->setCreator("Temporaris")
		->setLastModifiedBy("Temporaris")
		->setTitle("Template Relevé des heures intérimaires")
		->setSubject("Template excel")
		->setDescription("Template excel permettant la création d'un ou plusieurs relevés d'heures")
		->setKeywords("Template excel");
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', "12");

		// $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		
		$objWriter->save('some_excel_file.xlsx');
		// $writer->save('php://output');
	}
	
	public function download_file(){
		//PHP Version 7.2.28
		//phpinfo();die();
		error_reporting(E_ALL);	
		include(APPPATH."third_party/mpdf/mpdf.php");
		$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
		$mpdf->mirrorMargins = 1;
		
		// Set PDF Header Footer HTML
		$pdfHeader = $this->setPDF();		
				
		$mpdf->SetHTMLHeader($pdfHeader['header_1']);
		$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
		$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
		$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');
				
		$content = '<h1>Test</h1>';
		$mpdf->WriteHTML($content);
		$filename = "test-".date("Y-m-d-H-i-s").".pdf";
		$mpdf->Output();
		exit;				
			
	}
	
	public function pendency_report(){
		
		//print_r($this->adminArr);
		if(isset($_POST['btn_export']) && !empty($_POST['btn_export'])) 
		{
			
			error_reporting(0);
			
			$admin_id 		= $this->session->userdata('supadminid');
			$type 			= $this->session->userdata('type');
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('role_id'=>'3', 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];
		
							
			$getLogDetail   = $this->master_model->getRecords('adminlogin',array('role_id'=>$type, 'status' => '1'));
			$logID  		= $getLogDetail[0]['id'];
			
			//$list_ids = array("999", "3", "11", "12", "13", "14", "16");
		
			// Role ID
			if(in_array($type, $this->adminArr)){
				
				$condition = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status, ca.assign_date FROM gri_complaint c 
								JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
								JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code
								JOIN userregistration u ON c.user_id = u.user_id  WHERE ca.assign_user_id = '".$getAdminID."' AND c.reply_status = '1' AND ca.status = '1' AND c.comp_code!= '' ";	
			} else {
				
				$condition = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status, ca.assign_date FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code
							JOIN userregistration u ON c.user_id = u.user_id  WHERE ca.assign_user_id = '".$logID."' AND c.reply_status = '1' AND ca.status = '1' AND c.comp_code!= '' ";
			
			}
		
				
				
				$query = $condition.' ORDER BY c.complaint_date DESC';
				
				$rowCount = $this->getNumData($condition);
				
				$rowCnt = 0;
				$dataArr = array();						
				
				$result 	= $this->db->query($query)->result();			
				
				 require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
				 require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");

				 $objPHPExcel = new PHPExcel();

				 $objPHPExcel->getProperties()->setCreator("");
				 $objPHPExcel->getProperties()->setLastModifiedBy("");
				 $objPHPExcel->getProperties()->setTitle("");
				 $objPHPExcel->getProperties()->setSubject("");
				 $objPHPExcel->getProperties()->setDescription("");

				 $objPHPExcel->setActiveSheetIndex(0);

				 $sheet = $objPHPExcel->getActiveSheet();

				 $sheet->setCellValue("A1","Complaint Code");
				 $sheet->setCellValue("B1","Complainant’s Name");
				 $sheet->setCellValue("C1","Complaint Type");				
				 $sheet->setCellValue("D1","Reply Status");	

				 $row = 2;
				
				 foreach ($result as $key => $value)
				 { 		
				 
					$typeID = $value->complaint_type_id;
					$returnArr 	= $this->getCalculationResult($type, $value->assign_date);
				
					$st_c 		= $returnArr['btn_class'];
					$msg 		= $returnArr['msg'];
					$bgcolor 	= $returnArr['bgcolor'];
						
					 $sheet->setCellValue("A".$row,$value->comp_code);
					 $sheet->setCellValue("B".$row,$value->user_name);
					 $sheet->setCellValue("C".$row,$value->complaint_type_name);
					 $sheet->setCellValue("D".$row,$msg);
						 
					 $row++;
					 
				 }
				 
				 // $filename = "pendency-report-on-".date("Y-m-d-H-i-s").".xls";
				$sheet->setTitle("Export File");
				/*$filename = "complaint_excel/pendency-report-on-".date("Y-m-d-H-i-s").".xlsx";	
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");;
				header("Content-Disposition: attachment;filename=$filename");
				$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); */
				
				// create file name			
				header("Content-Type: application/vnd.ms-excel");	
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
				header('Content-Disposition: attachment;filename="pendency-report-on-'.time().'.xlsx"');
				header('Cache-Control: max-age=0');			
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				ob_end_clean();
				$objWriter->save('php://output');
				//$objWriter->save($fileName);
				exit; 
				
				 //force user to download the Excel file without writing it to server's HD
				// $objWriter->save('php://output');
				
				$data['excel_file_name'] = $filename;
				$objWriter->save($filename);
			
		}
		
		if(isset($_POST['btn_pendency_pdf']) && !empty($_POST['btn_pendency_pdf'])) 
		{
			
			error_reporting(E_ERROR | E_PARSE);		
			include(APPPATH."third_party/mpdf/mpdf.php");
			//$mpdf = new mPDF();
			
			$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
			$mpdf->mirrorMargins = 1;
			
			// Set PDF Header Footer HTML
			$pdfHeader = $this->setPDF();
				
			$mpdf->SetHTMLHeader($pdfHeader['header_1']);
			$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
			$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
			$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');

			$admin_id 		= $this->session->userdata('supadminid');
			$type 			= $this->session->userdata('type');
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('role_id'=>'3', 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];
						
			$getLogDetail   = $this->master_model->getRecords('adminlogin',array('role_id'=>$type, 'status' => '1'));
			$logID  		= $getLogDetail[0]['id'];
			
			//$list_ids = array("999", "3", "11", "12", "13", "14" , "16");
		
			// Role ID
			if(in_array($type, $this->adminArr)){
			
					$condition = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status, ca.assign_date FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code
						JOIN userregistration u ON c.user_id = u.user_id  WHERE ca.assign_user_id = '".$getAdminID."' AND c.reply_status = '1' AND ca.status = '1' AND c.comp_code!= '' ";
							
			} else {
				
				$condition = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status, ca.assign_date FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code
							JOIN userregistration u ON c.user_id = u.user_id  WHERE ca.assign_user_id = '".$logID."' AND c.reply_status = '1' AND ca.status = '1' AND c.comp_code!= '' ";
			
			}	
				
			$query = $condition.' ORDER BY c.complaint_date DESC';
			
			$rowCnt = 0;
			$dataArr = array();						
			
			$result 	= $this->db->query($query)->result();

			$table = '<h3>Pendency Report</h3>
						<table border="1" width="100%" class="table table-striped table-bordered dt-responsive nowrap">
								<tr>
								 <th>Sr.No.</th>
								 <th>Complaint Code</th>
								 <th>Complainant’s Name</th>
								 <th>Complaint Type</th>
								 <th>Reply Status</th>
							</tr>';
			
			$no = 1;
			
			 foreach ($result as $key => $value)
			 {
				$comCodes = $value->comp_code;
				$typeID = $value->complaint_type_id;
				
				$returnArr 	= $this->getCalculationResult($type, $value->assign_date);
				
				$st_c 		= $returnArr['btn_class'];
				$msg 		= $returnArr['msg'];
				$bgcolor 	= $returnArr['bgcolor'];
					 
				 $table .='<tr>
						<td align="center">'.$no.'</td>
						<td>'.$value->comp_code.'</td>
						<td align="center">'.$value->user_name.'</td>
						<td align="center">'.$value->complaint_type_name.'</td>
						<td align="center" style="'.$bgcolor.'">'.$msg.'</td>						
					</tr>';
					
					$no++;
				 
			 }
			 
			 $table .='</table>';

			$mpdf->WriteHTML($table);

			$filename = "complaint_pdf/pendency-report-on-".date("Y-m-d-H-i-s").".pdf";
			$data['pdf_file_name'] = $filename;
			$mpdf->Output($filename, 'D');
			
		}
		
		$data['middlecontent']='reports/pendency_report';
		$data['master_mod'] = 'Reports';
	    $this->load->view('admin/admin_combo',$data);
		
	}
	
	public function pendencycomplaint(){
		
		error_reporting(0);
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');
		$admin_id 		= $this->session->userdata('supadminid');
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('role_id'=>'3', 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
		
		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value
		$pageNo = ($_POST['start']/$_POST['length'])+1;
		
				
		$getLogDetail   = $this->master_model->getRecords('adminlogin',array('role_id'=>$type, 'status' => '1'));
		$logID  		= $getLogDetail[0]['id'];
		
		//$list_ids = array("999", "3", "11", "12", "13", "14", "16");
		
		// Role ID
		if(in_array($type, $this->adminArr)){
			
			$condition = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status, ca.assign_date FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code
						JOIN userregistration u ON c.user_id = u.user_id  WHERE ca.assign_user_id = '".$getAdminID."' AND c.reply_status = '1' AND ca.status = '1' AND c.comp_code!= '' ";
			
		} else {
			
			$condition = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status, ca.assign_date FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code
						JOIN userregistration u ON c.user_id = u.user_id  WHERE ca.assign_user_id = '".$logID."' AND c.reply_status = '1' AND ca.status = '1' AND c.comp_code!= '' ";
		}	
		
			
		$query = $condition.' ORDER BY c.complaint_date DESC LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
		
		$result 	= $this->db->query($query)->result();			
		
		$rowCount = $this->getNumData($condition);
		
		$rowCnt = 0;
		$dataArr = array();
		
		
			if($rowCount > 0){
			
			$no    = $_POST['start'];
			foreach($result as $comData){
				
				if($comData->complaint_sub_type_id == 0){
					$compSubType = $comData->online_service_id;
				} else {
					$compSubType = $comData->complaint_sub_type_id;
				}
				
				// Get Office Type
				$getOfficeDetail = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$compSubType, 'status' => '1'));
				$complaintSubTypeName = $getOfficeDetail[0]['complaint_sub_type_name'];
				
				$status = 'Pending';
				
				$no++;
				
				$returnArr = $this->getCalculationResult($type, $comData->assign_date);
				
				$st_c = $returnArr['btn_class'];
				
				$concat = '<span class="'.$st_c.'">'.$status.'</span>';	
				
					$dataArr[] = array(
						  $no,				  
						  $comData->comp_code,
						  $comData->user_name,
						  $comData->complaint_type_name,
						  $concat
					   );
				
				
			   
			    $rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
				);
				
				
			}
			
		} else {
			
			$rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => 0,
				  "recordsFiltered" => 0,
				  "data" => $dataArr
				);
		}
		echo json_encode($response);
		
	}
	
	public function getCalculationResult($type_id, $getDate){
		
		//$list_ids = array("999", "3", "11", "12", "13", "14", "16");
		
		$st_c = 'btn btn-light';
	
		if (in_array($type_id, $this->adminArr)){
				
			$concat = 'Pending';
			
			$currentDate = date('Y-m-d');
			
			$newDate = date("Y-m-d", strtotime($getDate));
			
			$dateDiff = $this->dateDiffInDays($getDate, $currentDate);		
			
			if($dateDiff >= 12){
				$st_c 		= 'btn btn-danger';
				$msg 		= 'Pending for 12 or more than 12 days';
				$bgcolor  	= 'background-color:red';	
			} 
			else if($dateDiff >= 5 && $dateDiff <= 11){
				$st_c 		= 'btn btn-warning';
				$msg 		= 'Pending for 5 to 11 days';
				$bgcolor  	= 'background-color:#ffbf00';
			} 
			else if($dateDiff <= 4){					
				$st_c 		= 'btn btn-success';
				$msg		= 'Pending for less than 4 days.';
				$bgcolor  	= 'background-color:green';
			} 
			
		} else {
			
			$currentDate = date('Y-m-d');
					
			$assign_date = $comData->assign_date;
			
			$newDate = date("Y-m-d", strtotime($assign_date));
			
			$dateDiff = $this->dateDiffInDays($assign_date, $currentDate);		
			
			if($dateDiff >= 12){
				$st_c 		= 'btn btn-danger';
				$status 	= 'Pending for 12 or more than 12 days';
				$bgcolor  	= 'background-color:red';	
			} 
			else if($dateDiff >= 5 && $dateDiff <= 11){
				$st_c 		= 'btn btn-warning';
				$msg 		= 'Pending for 5 to 11 days';
				$bgcolor  	= 'background-color:#ffbf00';
			} 
			else if($dateDiff <= 4){					
				$st_c 		= 'btn btn-success';
				$msg		= 'Pending for less than 4 days.';
				$bgcolor  	= 'background-color:green';
			} 
		}
		
		$returnArr = array("btn_class" => $st_c, "msg" => $msg, "bgcolor" => $bgcolor);
		
		return  $returnArr;
	}
	
	public function getOfficeslist(){
		$office_sub_type = $this->input->post('office_sub_id');
		$c_district_id   = $this->input->post('district_id');
		$options = "";
		$sro_office =	$this->master_model->getRecords('gri_sro_offices',array('district_id' => $c_district_id, 'office_sub_type' => $office_sub_type, 'status' => '1'),'',array('sro_office_id'=>'ASC'));
		
		
		if(count($sro_office)>0)	{
			$options .= '<option value="">-- Select --</option>';	
			foreach($sro_office as $officeName){				
				$options .= '<option value="'.$officeName['sro_office_id'].'" >'.$officeName['office_name'].'</option>';
			}			
		} else {
			$options .= '<option value="">-- Select --</option>';	
		}
		echo $options;
	}
	
	
	
	public function dateDiffInDays($date1, $date2)  
	{ 
		// Calulating the difference in timestamps 
		$diff = strtotime($date2) - strtotime($date1); 
		  
		// 1 day = 24 hours 
		// 24 * 60 * 60 = 86400 seconds 
		return abs(round($diff / 86400)); 
	} 

	public function index()
	{	
		$data['middlecontent']='reports/index';
		$data['master_mod'] = 'Reports';
	    $this->load->view('admin/admin_combo',$data);
			
	}
	
	
	public function cmp($a, $b)
	{
		if ($a['percentage'] == $b['percentage']) {
			return 0;
		}
		return ($a['percentage'] < $b['percentage']) ? -1 : 1;
	}

		
	public function getNumData($query){
		//echo $query;
		return $rowCount = $this->db->query($query)->num_rows();
	}
	
	
	// JDR/ACS - Complaint Overview Report 21/04/2020	
	public function pendency_level_2(){
		$type 			= $this->session->userdata('type');
		$dis_id			= $this->session->userdata('district_id');
		$ad_id			= $this->session->userdata('supadminid');	
		
		//$list_ids = array("999", "3", "11", "12", "13", "14");
		
		// Role ID
		if(in_array($type, $this->adminArr)){	
			
			$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE status = '1'";
			$data['officeList'] = $this->db->query($query_res)->result();
			
			$office_sub_list_1 = "SELECT complaint_sub_type_id, complaint_sub_type_name FROM gri_complaint_sub_type WHERE status = '1'";
			$data['office_sub_list_1'] = $this->db->query($office_sub_list_1)->result();
			
		} else {
			
			if($type == 2){
				
				$user_info=$this->master_model->getRecords('adminlogin',array('id'=>$ad_id, 'status' => '1'));
				if($user_info[0]['region_id'] == 4 && $user_info[0]['role_id'] == 2){ //2,6,7
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE office_sub_type IN(6,7) AND district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
					$office_sub_list_1 = "SELECT complaint_sub_type_id, complaint_sub_type_name FROM gri_complaint_sub_type WHERE complaint_sub_type_id IN('6', '7') AND status = '1'";
					$data['office_sub_list_1'] = $this->db->query($office_sub_list_1)->result();
				
				} else {
					
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
					
					$office_sub_list_1 = "SELECT complaint_sub_type_id, complaint_sub_type_name FROM gri_complaint_sub_type WHERE status = '1'";
					$data['office_sub_list_1'] = $this->db->query($office_sub_list_1)->result();
			
				} 
				
			} else if($type == 9){
				
				$user_info=$this->master_model->getRecords('adminlogin',array('id'=>$ad_id, 'status' => '1'));
				if($user_info[0]['region_id'] == 4 && $user_info[0]['role_id'] == 9){ //2,6,7
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE office_sub_type IN(2) AND district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
				
					$office_sub_list_1 = "SELECT complaint_sub_type_id, complaint_sub_type_name FROM gri_complaint_sub_type WHERE complaint_sub_type_id = '2' AND status = '1'";
					$data['office_sub_list_1'] = $this->db->query($office_sub_list_1)->result();
				
				} else {
					
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
					
					$office_sub_list_1 = "SELECT complaint_sub_type_id, complaint_sub_type_name FROM gri_complaint_sub_type WHERE status = '1'";
					$data['office_sub_list_1'] = $this->db->query($office_sub_list_1)->result();
			
				} 
				
			} else if($type == 5){
				
				$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE district_id = '".$dis_id."' AND status = '1'";
				$data['officeList'] = $this->db->query($query_res)->result();
				
				$office_sub_list_1 = "SELECT complaint_sub_type_id, complaint_sub_type_name FROM gri_complaint_sub_type WHERE status = '1'";
				$data['office_sub_list_1'] = $this->db->query($office_sub_list_1)->result();
				
				
			}			
			
		}
		
		//////////////////////  Download xls Functionality ////////////////////
		
		if(isset($_POST['btn_export']) && !empty($_POST['btn_export'])) 
		{			
			
			
			error_reporting(0);
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
			$complaint_subtype	= @$this->input->post('complaint_subtype')?$this->input->post('complaint_subtype'):'';
			
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');
			$admin_id 		= $this->session->userdata('supadminid');
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];
			
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;
			
									
			$off_id  = '';
			if($office_subtype){			
				
				$off_id  .= " AND sro_id = '".$office_subtype."'";
				
			}
			
			$a_com_type = "office_id IN(6,7)";
			$o_com_type = "";
			$c_com_type = "office_id = '2'";
			
			if($complaint_subtype){
				
				$c_sub_type = "AND c.complaint_sub_type_id = '".$complaint_subtype."'";
				$a_com_type = "office_id = '".$complaint_subtype."'";
				$o_com_type = "AND office_id = '".$complaint_subtype."'";
				$c_com_type = "office_id = '".$complaint_subtype."'";
			}
			
			if($type == 2){
				
				if($getAdminR == 4){
					
					$condition = "SELECT * FROM adminlogin WHERE ".$a_com_type." AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
										
				} else {				
					
					$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id." ".$o_com_type."";
					
				}
				
			} else if($type == 9){
				
				if($getAdminR == 4){
					
					$condition = "SELECT * FROM adminlogin WHERE ".$c_com_type." AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
					
					
				} else {
					
					$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id." ".$o_com_type."";
					
				}
				
			} else if($type == 1){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id='".$getAdminO."' AND district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";
				
				
			} else if($type == 15){
			
				$condition = "SELECT * FROM adminlogin WHERE office_id='".$getAdminO."' AND district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";
				
				
			} else if($type == 7){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id='".$getAdminO."' AND district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";
				
			} 

			$dateCon = '';
			if($frm_date){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
			}
			
			if($to_date){
				
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
			}
			
							
			$query = $condition;
			
			$result 	= $this->db->query($query)->result();

						
			$rowCount = $this->getNumData($condition);
			
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");

			 $objPHPExcel = new PHPExcel();

			 $objPHPExcel->getProperties()->setCreator("");
			 $objPHPExcel->getProperties()->setLastModifiedBy("");
			 $objPHPExcel->getProperties()->setTitle("");
			 $objPHPExcel->getProperties()->setSubject("");
			 $objPHPExcel->getProperties()->setDescription("");

			 $objPHPExcel->setActiveSheetIndex(0);

			 $sheet = $objPHPExcel->getActiveSheet();

			 $sheet->setCellValue("A1","Complaint Type");
			 $sheet->setCellValue("B1","Office Name");
			 $sheet->setCellValue("C1","Complaint Received In Selected Period");				
			 $sheet->setCellValue("D1","Total Complaint Received");
			 $sheet->setCellValue("E1","Complaint Resolved In Selected Period");
			 $sheet->setCellValue("F1","Total Complaint Resolved");
			 $sheet->setCellValue("G1","Complaint Pending For Selected Period");
			 $sheet->setCellValue("H1","Total Complaint Pending");
			 $sheet->setCellValue("I1","Percentage Of Complaint Resolution For Selected Period");
			 $sheet->setCellValue("J1","Overall Percentage Of Complaint Resolution");	

			 $row = 2;
			
			foreach($result as $key => $value){
					
				$complaint_type = 'Specific Office';
			
				// Complaint Sub Type Name
				$com_sub_details = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$value->office_id, 'status' => '1'));
				$getcom_subname = $com_sub_details[0]['complaint_sub_type_name'];

				// Get Office Address
				$getOfficeAdd   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$value->sro_id, 'status' => '1'));
				$getAdd  		= $getOfficeAdd[0]['office_name'];
				
				$get_details = $this->jdr_pendency_report($dateCon, $value->district_id, $value->sro_id, $c_sub_type);	
				
									
				 $sheet->setCellValue("A".$row,$complaint_type);
				 $sheet->setCellValue("B".$row,$getAdd);
				 $sheet->setCellValue("C".$row,$get_details['count_1']);
				 $sheet->setCellValue("D".$row,$get_details['count_2']);
				 $sheet->setCellValue("E".$row,$get_details['count_3']);
				 $sheet->setCellValue("F".$row,$get_details['count_4']);
				 $sheet->setCellValue("G".$row,$get_details['count_5']);
				 $sheet->setCellValue("H".$row,$get_details['count_6']);
				 $sheet->setCellValue("I".$row,$get_details['percentage']);
				 $sheet->setCellValue("J".$row,$get_details['overall_per']);
					 
					 
					 $row++;
				
			}
			
			// $filename = "complaint-overview-level-2-report-on-".date("Y-m-d-H-i-s").".xls";
			$sheet->setTitle("Export File");
			$filename = "complaint_excel/complaint-overview-level-2-report-on-".date("Y-m-d-H-i-s").".xlsx";	
			/*header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");;
			header("Content-Disposition: attachment;filename=$filename");
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);*/
			// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
			//  //force user to download the Excel file without writing it to server's HD
			// $objWriter->save('php://output');
			// set_time_limit(30);
			// exit;
			
			// create file name			
			header("Content-Type: application/vnd.ms-excel");	
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			header('Content-Disposition: attachment;filename="complaint-overview-level-2-report-on-'.time().'.xlsx"');
			header('Cache-Control: max-age=0');			
			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			ob_end_clean();
			$objWriter->save('php://output');
			//$objWriter->save($fileName);
			exit; 
			
			$data['excel_file_name'] = $filename;
			$objWriter->save($filename);
		
		}
		
		///////// Download PDF ////////////
		
		if( (isset($_POST['btn_pdf']) && !empty($_POST['btn_pdf'])) ||  !empty($_POST['is_graph']) )  
		{
			
			error_reporting(E_ERROR | E_PARSE);		
			include(APPPATH."third_party/mpdf/mpdf.php");
			//$mpdf=new mPDF('c');
			
			$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
			$mpdf->mirrorMargins = 1;
			
			// Set PDF Header Footer HTML
			$pdfHeader = $this->setPDF();		
					
			$mpdf->SetHTMLHeader($pdfHeader['header_1']);
			$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
			$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
			$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');
			
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
			$complaint_subtype	= @$this->input->post('complaint_subtype')?$this->input->post('complaint_subtype'):'';
			
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');
			$admin_id 		= $this->session->userdata('supadminid');
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];
			
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;
			
									
			$off_id  = '';
			if($office_subtype){			
				
				$off_id  .= " AND sro_id = '".$office_subtype."'";
				
			}
			
			$a_com_type = "office_id IN(6,7)";
			$o_com_type = "";
			$c_com_type = "office_id = '2'";
			
			if($complaint_subtype){
				
				$c_sub_type = "AND c.complaint_sub_type_id = '".$complaint_subtype."'";
				$a_com_type = "office_id = '".$complaint_subtype."'";
				$o_com_type = "AND office_id = '".$complaint_subtype."'";
				$c_com_type = "office_id = '".$complaint_subtype."'";
			}
			
			if($type == 2){
				
				if($getAdminR == 4){
					
					$condition = "SELECT * FROM adminlogin WHERE ".$a_com_type." AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
										
				} else {
					
					
					$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id." ".$o_com_type."";
					
				}
				
				
			
			} else if($type == 9){
				
				if($getAdminR == 4){
					
					$condition = "SELECT * FROM adminlogin WHERE ".$c_com_type." AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
										
				} else {
					
					$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id." ".$o_com_type."";
					
				}
				
			} else if($type == 1){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id='".$getAdminO."' AND district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";
				
				
			} else if($type == 15){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id='".$getAdminO."' AND district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";
				
				
			} else if($type == 7){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id='".$getAdminO."' AND district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";
				
				
			} 
			
			$dateCon = '';
			if($frm_date){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
			}
			
			if($to_date){
				
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
			}
			
							
			$query = $condition;
			
			$result 	= $this->db->query($query)->result();			
			
			$rowCount = $this->getNumData($condition);
			
			$table = '<h3>Complaint Overview Report</h3>
						<table border="1" class="table table-striped table-bordered dt-responsive nowrap">
						
							<tr>
								 <th>Sr.No.</th>
								 <th>Complaint Type</th>
								 <th>Office Name</th>
								 <th>Complaint Received in Selected Period</th>
								 <th>Total Complaint Received</th>
								 <th>Complaint Resolved in Selected Period</th>
								 <th>Total Complaint Resolved</th>
								 <th>Complaint Pending for the Selected Period</th>
								 <th>Total Complaint Pending </th>
								 <th>Percentage of complaint resolution for the selected period</th>
								 <th>Overall percentage of complaint resolution</th>
							</tr>';
			
			// Graph Title
			$data_graph[] = array('Designation Name', 'Complaint Received in Selected Period', 'Complaint Resolved in Selected Period', 'Complaint Pending for the Selected Period');				
			
			$sr = 1;
			foreach($result as $key => $value){
				
				$complaint_type = 'Specific Office';
			
				// Complaint Sub Type Name
				$com_sub_details = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$value->office_id, 'status' => '1'));
				$getcom_subname = $com_sub_details[0]['complaint_sub_type_name'];
				
				// Get Office Address
				$getOfficeAdd   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$value->sro_id, 'status' => '1'));
				$getAdd  		= $getOfficeAdd[0]['office_name'];

				$get_details = $this->jdr_pendency_report($dateCon, $value->district_id, $value->sro_id, $c_sub_type);	
				
											 
				 $table .='<tr>
							<td align="center">'.$sr.'</td>
							<td>'.$complaint_type.'</td>
							<td align="center">'.$getAdd.'</td>
							<td align="center">'.$get_details['count_1'].'</td>
							<td align="center">'.$get_details['count_2'].'</td>
							<td align="center">'.$get_details['count_3'].'</td>
							<td align="center">'.$get_details['count_4'].'</td>
							<td align="center">'.$get_details['count_5'].'</td>
							<td align="center">'.$get_details['count_6'].'</td>
							<td align="center">'.$get_details['percentage'].'</td>
							<td align="center">'.$get_details['overall_per'].'</td>
						</tr>';
					$data_graph[] = array($getAdd, $get_details['count_1'],$get_details['count_3'],$get_details['count_5']);	
				$sr++;			
				
			}
			
				$table .='</table>';					
				 
				// Added by Manoj  // GRAPH code here //
				if(@$this->input->post('is_graph')){		
					echo json_encode($data_graph);
					return true;
					exit;								
				}	
				
				$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins
		

				$mpdf->WriteHTML($table);

				$filename = "complaint_pdf/complaint-overview-level-2-report-on-".date("Y-m-d-H-i-s").".pdf";
				$data['pdf_file_name'] = $filename;
				$mpdf->Output($filename, 'D');
		
		}
		
		
		////////////////////////// End Functionality ///////////////////////
		
		$data['middlecontent']='reports/pendency_level2';
		$data['master_mod'] = 'Reports';
	    $this->load->view('admin/admin_combo',$data);
		
	}
	
	public function setPDF(){
		
		$headImg  = base_url('img/emblemofindia.png');
		$headImg2 = base_url('img/IGRlogo.png');		
		
		$img1 = '<img src="img/emblemofindia.png" height="80" width="80" />';
		$img2 = '<img src="img/IGRlogo.png" height="80" width="80" />';
		
		$header = '<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;"><tr>
					<td width="23%">'.$img1.'</td>
					<td width="23%" align="center" style="font-size:17px; color:#000;">Department of Registration and Stamps<br />
					under Revenue and Forest Department, Govt of Maharashtra
					</td>
					<td width="23%" style="text-align: right;">'.$img2.'</td>
					</tr></table>';
		$headerE = '<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;"><tr>
					<td width="23%">'.$img1.'</td>
					<td width="23%" align="center" style="font-size:17px; color:#000;">Department of Registration and Stamps<br />
					under Revenue and Forest Department, Govt of Maharashtra
					</td>
					<td width="23%" style="text-align: right;">'.$img2.'</td>
					</tr></table>';

		$footer = '<table width="100%" style="border-top: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;">
					<tr>
						<td width="33%"></td>
						<td width="33%" align="center">{PAGENO}/{nbpg}</td>
						<td width="33%" style="text-align: right;"></td>
					</tr>
				</table>';
		$footerE = '<table width="100%" style="border-top: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;">
					<tr>
						<td width="33%"></td>
						<td width="33%" align="center">{PAGENO}/{nbpg}</td>
						<td width="33%" style="text-align: right;"></td>
					</tr>
				</table>';
				
				
		$arrHeader = array('header_1' => $header, 'header_2' => $headerE, 'footer_1' => $footer, 'footer_2' =>$footerE);
		
		return $arrHeader;
	}
	
	
	// SRO/CSO/JDR/ACS Pendency Report 21/04/2020
	public function pendency_reports_level_2(){
		
		error_reporting(0);
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
		$complaint_subtype	= @$this->input->post('complaint_subtype')?$this->input->post('complaint_subtype'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');
		$admin_id 		= $this->session->userdata('supadminid');
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
		
		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value
		$pageNo = ($_POST['start']/$_POST['length'])+1;
		
								
		$off_id  = '';
		if($office_subtype){			
			
			$off_id  .= " AND sro_id = '".$office_subtype."'";
			
		}
		
		$a_com_type = "office_id IN(6,7)";
		$o_com_type = "";
		$c_com_type = "office_id = '2'";
		
		if($complaint_subtype){
			
			$c_sub_type = "AND c.complaint_sub_type_id = '".$complaint_subtype."'";
			$a_com_type = "office_id = '".$complaint_subtype."'";
			$o_com_type = "AND office_id = '".$complaint_subtype."'";
			$c_com_type = "office_id = '".$complaint_subtype."'";
		}
		
		if($type == 2){
			
			if($getAdminR == 4){
				
				$condition = "SELECT * FROM adminlogin WHERE ".$a_com_type." AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				
			} else {
				
				$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id." ".$o_com_type."";
				
			}
			
			
		
		} else if($type == 9){
			
			if($getAdminR == 4){
				
				$condition = "SELECT * FROM adminlogin WHERE ".$c_com_type." AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				
			} else {
				
				$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id." ".$o_com_type."";
				
			}
			
		} else if($type == 1){
			
			$condition = "SELECT * FROM adminlogin WHERE office_id='".$getAdminO."' AND district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";
			
		} else if($type == 15){
			
			$condition = "SELECT * FROM adminlogin WHERE office_id='".$getAdminO."' AND district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";
			
		} else if($type == 7){
			
			$condition = "SELECT * FROM adminlogin WHERE office_id='".$getAdminO."' AND district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";
						
		} 		

		$dateCon = '';
		if($frm_date){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
		}
		
		if($to_date){
			
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
		}
					
		$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
		
		
		$result 	= $this->db->query($query)->result();			
		
		$rowCount = $this->getNumData($condition);
		
		$rowCnt = 0;
		$dataArr = array();
		
		
			if($rowCount > 0){
			
			$no    = $_POST['start'];
			$i = 0;
			foreach($result as $adminData){
			
			$complaint_type = 'Specific Office';
			
			// Complaint Sub Type Name
			$com_sub_details = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$adminData->office_id, 'status' => '1'));
			$getcom_subname = $com_sub_details[0]['complaint_sub_type_name'];
			
			// Get Office Address
			$getOfficeAdd   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
			$getAdd  		= $getOfficeAdd[0]['office_name'];

			$get_details = $this->jdr_pendency_report($dateCon, $adminData->district_id, $adminData->sro_id, $c_sub_type);	
			
								
				$i++;
				
				
				$dataArr[] = array(
					  $i,				  
					  $complaint_type,
					  $getAdd,
					  $get_details['count_1'],
					  $get_details['count_2'],
					  $get_details['count_3'],
					  $get_details['count_4'],						  
					  $get_details['count_5'],
					  $get_details['count_6'],
					  $get_details['percentage'],
					  $get_details['overall_per']
				   );
			   
			    $rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
				);
				
			}
			
		} else {
			
			$rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => 0,
				  "recordsFiltered" => 0,
				  "data" => $dataArr
				);
		}
		echo json_encode($response);
		
	}
	
	public function jdr_pendency_report($dateCon, $district_id, $sro_id, $c_sub_type){
		
		// Complaint Received In Selected Period	
			$query_sro_1 = "SELECT c.c_id, c.complaint_sub_type_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE  c.comp_code!='' ".$dateCon." AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."' ".$c_sub_type."";
			$count_1 	= $this->getNumData($query_sro_1);
			
			// Total Complaint Resolved in Percentage Selected Period
			$query_sro_7 = "SELECT c.c_id, c.complaint_sub_type_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' AND c.comp_code!='' AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."' ".$dateCon." ".$c_sub_type."";
			$count_7 	= $this->getNumData($query_sro_7);
			
			// Total Complaint Received	
			$query_sro_2 = "SELECT c.c_id, c.complaint_sub_type_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.comp_code!='' AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."' ".$c_sub_type."";
			$count_2 	= $this->getNumData($query_sro_2);
			
			// Total Complaint Resolved In Selected Period	
			$query_sro_3 = "SELECT c.c_id, c.complaint_sub_type_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' ".$dateCon." AND c.comp_code!='' AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."' ".$c_sub_type."";
			$count_3 	= $this->getNumData($query_sro_3);
			
			// Total Complaint Resolved
			$query_sro_4 = "SELECT c.c_id, c.complaint_sub_type_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' AND c.comp_code!='' AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."' ".$c_sub_type."";
			$count_4 	= $this->getNumData($query_sro_4);
			
			// Total Complaint Pending In Selected Period
			$query_sro_5 = "SELECT c.c_id, c.complaint_sub_type_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='1' ".$dateCon." AND c.comp_code!='' AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."' ".$c_sub_type."";
			$count_5 	= $this->getNumData($query_sro_5);
			
			// Total Complaint Pending
			$query_sro_6 = "SELECT c.c_id, c.complaint_sub_type_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='1' AND c.comp_code!='' AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."' ".$c_sub_type."";
			$count_6 	= $this->getNumData($query_sro_6);				
				
			$percentage = ($count_3/$count_1)*100;
			$overAllper = ($count_4/$count_2)*100;			
			
			if (is_nan($percentage) || is_infinite($percentage)) {
				$percentage = "0";
			} else {
				$percentage = round($percentage, 2);
			}
			
			if (is_nan($overAllper) || is_infinite($overAllper)) {
				$overAllper = "0";
			} else {
				$overAllper = round($overAllper, 2);
			}
			
			$per_tage = $percentage.'%';
			$over_tage = $overAllper.'%';
			
			$returnArr= array('count_1' =>$count_1,
							  'count_2' =>$count_2,
							  'count_3' =>$count_3,
							  'count_4' =>$count_4,
							  'count_5' =>$count_5,
							  'count_6' =>$count_6,
							  'percentage' =>$per_tage,
							  'overall_per' =>$over_tage);
			
			return 	$returnArr;
				
	}
	
	// Ajax call for District
	public function district_data(){		
		$csrf_token = $this->security->get_csrf_hash();
		$c_region_id = 	$this->input->post('region_id');
		
		$options = '<option value="">-- Select --</option>';
		if($c_region_id > 0){
			$data['distict_details'] = $this->master_model->getRecords('gri_district',array('religion_id' => $c_region_id),'',array('district_id'=>'ASC'));
			if(count($data['distict_details'])>0)	{
				foreach($data['distict_details'] as $district){
					$options .= '<option value="'.$district['district_id'].'">'.$district['district_name'].'</option>';
				}			
			}
			
		}
		 
		// echo $options;
		echo json_encode(['options' => $options, 'csrf_token' => $csrf_token ], JSON_UNESCAPED_SLASHES);
	}
	
	// IGRA/IGR/DIG etc Pendency Report 21/04/2020
	public function pendency_level_3(){
		$csrf_token = $this->security->get_csrf_hash();
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');
		$admin_id 		= $this->session->userdata('supadminid');
		$data['region_list'] = $this->master_model->getRecords('gri_region_division',array('status' => '1'));
		$data['pdf_file_name'] = '';
		
		if(isset($_POST['btn_export']) && !empty($_POST['btn_export'])) 
		{
			
		error_reporting(0);
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$region_id_1	= @$this->input->post('region_id')?$this->input->post('region_id'):'';
		$district_id 	= @$this->input->post('district_id')?$this->input->post('district_id'):'';
		//$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');
		$admin_id 		= $this->session->userdata('supadminid');
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
		
		$impArr = implode(",", $this->secondArr);	
		//$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN('3','5','10','11','12','13','14','999') AND status = '1'";
		$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(".$impArr.") AND status = '1'";
		$officeTypesName = 'All';
		
		if($region_id_1){
			
			$condition .= " AND region_id = '".$region_id_1."'";
			
		}
		
		if($district_id){
			
			$condition .= " AND district_id = '".$district_id."'";
			
		}

		$dateCon = '';
		if($frm_date){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
		}
		
		if($to_date){
			
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
		}
			
		$query = $condition;
		
		$result 	= $this->db->query($query)->result();			
		
		$rowCount = $this->getNumData($condition);
		
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");

			 $objPHPExcel = new PHPExcel();

			 $objPHPExcel->getProperties()->setCreator("");
			 $objPHPExcel->getProperties()->setLastModifiedBy("");
			 $objPHPExcel->getProperties()->setTitle("");
			 $objPHPExcel->getProperties()->setSubject("");
			 $objPHPExcel->getProperties()->setDescription("");

			 $objPHPExcel->setActiveSheetIndex(0);

			 $sheet = $objPHPExcel->getActiveSheet();

			 $sheet->setCellValue("A1","Region");
			 $sheet->setCellValue("B1","District");
			 $sheet->setCellValue("C1","Level");
			 $sheet->setCellValue("D1","Office Address");
			 $sheet->setCellValue("E1","Complaint Received In Selected Period");				
			 $sheet->setCellValue("F1","Total Complaint Received");
			 $sheet->setCellValue("G1","Complaint Resolved In Selected Period");
			 $sheet->setCellValue("H1","Total Complaint Resolved");
			 $sheet->setCellValue("I1","Complaint Pending For Selected Period");
			 $sheet->setCellValue("J1","Total Complaint Pending");
			 $sheet->setCellValue("K1","Percentage Of Complaint Resolution For Selected Period");
			 $sheet->setCellValue("L1","Overall Percentage Of Complaint Resolution");	

			 $row = 2;			
			
			$i = 0;
			foreach($result as $adminData){
			
			$primaryID 			= $adminData->id;	
			$region_id  		= $adminData->region_id;
			$type_id 			= $adminData->role_id;
			$d_id 				= $adminData->district_id;
			
			// Level
			$getRoleName  = $this->master_model->getRecords('gri_roles',array('role_id'=>$type_id, 'status' => '1'));
			$roleDetails  = $getRoleName[0]['role_name'];
			
			// Get Region Name
			$getRegionName  = $this->master_model->getRecords('gri_region_division',array('religion_id'=>$region_id, 'status' => '1'));
			$getRegionDetail= $getRegionName[0]['region_division_name'];
			
			// Get Region Name
			$getDistrictName  = $this->master_model->getRecords('gri_district',array('district_id'=>$d_id, 'status' => '1'));
			$getDistriDetail  = $getDistrictName[0]['district_name'];			
			
			// Get Office Type Name
			$getOfficeTypeName  = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$adminData->office_id, 'status' => '1'));
			$getOfficeTypeName  = $getOfficeTypeName[0]['complaint_sub_type_name'];
			
			// Get Office Address
			$getOfficeAdd   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
			$getAdd  		= $getOfficeAdd[0]['office_name'];
						
			if($region_id == 4 && ($type_id == 2 || $type_id == 1)){
				
				$com_sub_type_con = "AND c.complaint_sub_type_id IN('6','7')";
				
				if($type_id == 2){
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getDistriDetail." - JDR";					
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = "", $online_id = false);
						
				} else if($type_id == 1){
				
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getOffice_Name;
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $d_id, $adminData->sro_id, $online_id = false);
					
				}
				
			} else if($region_id != 4 && ($type_id == 2 || $type_id == 1)){
				
				if($type_id == 2){
					
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getDistriDetail." - JDR";
					$com_sub_type_con = "";					
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = "", $online_id = false);
						
				} else if($type_id == 1){
					
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getOffice_Name;
					
					$com_sub_type_con = "";
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $d_id, $adminData->sro_id, $online_id = false);
					
				}
			} 
			
			// Marriage Officer
			if($type_id == 15){
					
				$com_sub_type_con = "AND c.complaint_sub_type_id IN(6)";
				$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $adminData->sro_id, $online_id = false);
				
			}
			
			

			if($region_id == 4 && ($type_id == 7 || $type_id == 9)){
				
				$com_sub_type_con = "AND c.complaint_sub_type_id IN(2)";
				
				if($type_id == 7){					
					
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getOffice_Name;
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $adminData->sro_id, $online_id = false);
												
				} else if($type_id == 9){
				
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getDistriDetail." - ACS";
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = '', $online_id = false);
									
				}
				
			} else if($region_id != 4 && ($type_id == 7 || $type_id == 9)){
				
				//$com_sub_type_con = "AND c.complaint_sub_type_id IN(2)";
				$com_sub_type_con = "";
				
				if($type_id == 7){					
					
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $adminData->sro_id, $online_id = false);
												
				} else if($type_id == 9){
					
					// Get Office Name					
					$designationD    = $getDistriDetail." - ACS";
				
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = '', $online_id = false);
									
				}
				
			}
			
			// Help Desk User
			if($type_id == 4){
				// Get Office Name
				$designationD    = " Help Desk";
				$getRegionDetail = 'MUMBAI REGION';
				$com_sub_type_con = "";
				$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = '', $online_id = true);
					
			}
				
									
				 $sheet->setCellValue("A".$row,$getRegionDetail);
				 $sheet->setCellValue("B".$row,$getDistriDetail);
				 $sheet->setCellValue("C".$row,$roleDetails);
				 $sheet->setCellValue("D".$row,$getAdd);
				 $sheet->setCellValue("E".$row,$gerArr['count_1']);
				 $sheet->setCellValue("F".$row,$gerArr['count_2']);
				 $sheet->setCellValue("G".$row,$gerArr['count_3']);
				 $sheet->setCellValue("H".$row,$gerArr['count_4']);
				 $sheet->setCellValue("I".$row,$gerArr['count_5']);
				 $sheet->setCellValue("J".$row,$gerArr['count_6']);
				 $sheet->setCellValue("K".$row,$gerArr['percentage']);
				 $sheet->setCellValue("L".$row,$gerArr['overall_per']);
					 
					 
				$row++;		
				
			}
			
			// $filename = "pendency-level3-report-on-".date("Y-m-d-H-i-s").".xls";
			$sheet->setTitle("Export File");
			$filename = "complaint_excel/pendency-level3-report-on-".date("Y-m-d-H-i-s").".xlsx";	
			/*header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");;
			header("Content-Disposition: attachment;filename=$filename");
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);*/
			// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
			//  //force user to download the Excel file without writing it to server's HD
			// $objWriter->save('php://output');
			// set_time_limit(30);
			// exit;
			
			
			// create file name			
			header("Content-Type: application/vnd.ms-excel");	
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			header('Content-Disposition: attachment;filename="pendency-level3-report-on-'.time().'.xlsx"');
			header('Cache-Control: max-age=0');			
			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			ob_end_clean();
			$objWriter->save('php://output');
			//$objWriter->save($fileName);
			exit; 
			
			$data['excel_file_name'] = $filename;
			$objWriter->save($filename);
			
		}
		
		// Download PDF 
		if( (isset($_POST['btn_pdf']) && !empty($_POST['btn_pdf'])) ||  !empty($_POST['is_graph']) )  
		{
			error_reporting(E_ERROR | E_PARSE);		
			include(APPPATH."third_party/mpdf/mpdf.php");
			//$mpdf=new mPDF('c');
			$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
			$mpdf->mirrorMargins = 1;
			
			// Set PDF Header Footer HTML
			$pdfHeader = $this->setPDF();		
					
			$mpdf->SetHTMLHeader($pdfHeader['header_1']);
			$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
			$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
			$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');
			
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$region_id_1	= @$this->input->post('region_id')?$this->input->post('region_id'):'';
		$district_id 	= @$this->input->post('district_id')?$this->input->post('district_id'):'';
		//$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');
		$admin_id 		= $this->session->userdata('supadminid');
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
		
		$impArr = implode(",", $this->secondArr);		
		//$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN('3','5','10','11','12','13','14','999') AND status = '1'";
		$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(".$impArr.") AND status = '1'";
		$officeTypesName = 'All';
		
		if($region_id_1){
			
			$condition .= " AND region_id = '".$region_id_1."'";
			
		}
		
		if($district_id){
			
			$condition .= " AND district_id = '".$district_id."'";
			
		}

		$dateCon = '';
		if($frm_date){
			
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			
			$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
		}
		
		if($to_date){
			
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
		}
		
			
		$query = $condition;
		
		$result 	= $this->db->query($query)->result();			
		
		$rowCount = $this->getNumData($condition);
		
				

			 $table = '<h3>District Pendency Report</h3>
						<table border="1" class="table table-striped table-bordered dt-responsive nowrap">
						
							<tr>
								 <th>Sr.No.</th>
								 <th>Region</th>
								 <th>District</th>
								 <th>Level</th>
								 <th>Office Address</th>
								 <th>Complaint Received in Selected Period</th>
								 <th>Total Complaint Received</th>
								 <th>Complaint Resolved in Selected Period</th>
								 <th>Total Complaint Resolved</th>
								 <th>Complaint Pending for the Selected Period</th>
								 <th>Total Complaint Pending </th>
								 <th>Percentage of complaint resolution for the selected period</th>
								 <th>Overall percentage of complaint resolution</th>
							</tr>';	
			
			$data_graph[] = array('Designation Name', 'Complaint Received in Selected Period', 'Complaint Resolved in Selected Period', 'Complaint Pending for the Selected Period');					
			
			$i = 1;
			foreach($result as $adminData){
			
			$primaryID 			= $adminData->id;	
			$region_id  		= $adminData->region_id;
			$type_id 			= $adminData->role_id;
			$d_id 				= $adminData->district_id;
			
			// Level
			$getRoleName  = $this->master_model->getRecords('gri_roles',array('role_id'=>$type_id, 'status' => '1'));
			$roleDetails  = $getRoleName[0]['role_name'];
			
			
			// Get Region Name
			$getRegionName  = $this->master_model->getRecords('gri_region_division',array('religion_id'=>$region_id, 'status' => '1'));
			$getRegionDetail= $getRegionName[0]['region_division_name'];
			
			// Get District Name
			$getDistrictName  = $this->master_model->getRecords('gri_district',array('district_id'=>$d_id, 'status' => '1'));
			$getDistriDetail  = $getDistrictName[0]['district_name'];			
			 
			// Get Office Type Name
			$getOfficeTypeName  = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$adminData->office_id, 'status' => '1'));
			$getOfficeTypeName  = $getOfficeTypeName[0]['complaint_sub_type_name'];
			
			// Get Office Address
			$getOfficeAdd   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
			$getAdd  		= $getOfficeAdd[0]['office_name'];
						
			if($region_id == 4 && ($type_id == 2 || $type_id == 1)){
				
				$com_sub_type_con = "AND c.complaint_sub_type_id IN('6','7')";
				
				if($type_id == 2){
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getDistriDetail." - JDR ";					
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = "", $online_id = false);
						
				} else if($type_id == 1){
				
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getOffice_Name;
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $d_id, $adminData->sro_id, $online_id = false);
					
				}
				
			} else if($region_id != 4 && ($type_id == 2 || $type_id == 1)){
				
				if($type_id == 2){
					
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getDistriDetail." - JDR";
					$com_sub_type_con = "";					
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = "", $online_id = false);
						
				} else if($type_id == 1){
					
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getOffice_Name;
					
					$com_sub_type_con = "";
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $d_id, $adminData->sro_id, $online_id = false);
					
				}
			} 
			
			// Marriage Officer
			if($type_id == 15){
				// Get Office Name
				$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
				$getOffice_Name  = $getOfficeName[0]['office_name'];
				$designationD    = $getDistriDetail."-".$getOffice_Name;
				$com_sub_type_con = "AND c.complaint_sub_type_id IN(6)";
				$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $adminData->sro_id, $online_id = false);
				
			}
			

			if($region_id == 4 && ($type_id == 7 || $type_id == 9)){
				
				$com_sub_type_con = "AND c.complaint_sub_type_id IN(2)";
				
				if($type_id == 7){					
					
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getOffice_Name;
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $adminData->sro_id, $online_id = false);
												
				} else if($type_id == 9){
				
					// Get Office Name
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$designationD    = $getDistriDetail." - ACS";
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = '', $online_id = false);
									
				}
				
			} else if($region_id != 4 && ($type_id == 7 || $type_id == 9)){
				
				//$com_sub_type_con = "AND c.complaint_sub_type_id IN(2)";
				$com_sub_type_con = "";
				
				if($type_id == 7){					
					
					$getOfficeName   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
					$getOffice_Name  = $getOfficeName[0]['office_name'];
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $adminData->sro_id, $online_id = false);
												
				} else if($type_id == 9){
					
					// Get Office Name					
					$designationD    = $getDistriDetail." - ACS";
				
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = '', $online_id = false);
									
				}
				
			}
			
			// Help Desk User
			if($type_id == 4){
				// Get Office Name
				$designationD    = " Help Desk";
				$getRegionDetail = 'MUMBAI REGION';
				$com_sub_type_con = "";
				$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = '', $online_id = true);
					
			}
			
			$table .='<tr>
						<td align="center">'.$i.'</td>
						<td>'.$getRegionDetail.'</td>
						<td align="center">'.$getDistriDetail.'</td>
						<td align="center">'.$roleDetails.'</td>
						<td align="center">'.$getAdd.'</td>
						<td align="center">'.$gerArr['count_1'].'</td>
						<td align="center">'.$gerArr['count_2'].'</td>
						<td align="center">'.$gerArr['count_3'].'</td>
						<td align="center">'.$gerArr['count_4'].'</td>
						<td align="center">'.$gerArr['count_5'].'</td>
						<td align="center">'.$gerArr['count_6'].'</td>
						<td align="center">'.$gerArr['percentage'].'</td>
						<td align="center">'.$gerArr['overall_per'].'</td>
					</tr>';
						
				$data_graph[] = array($designationD, $gerArr['count_1'],$gerArr['count_3'],$gerArr['count_5']);				
				
				$i++;
			}
			
			// GRAPH code here //
			if(@$this->input->post('is_graph')){		
				echo json_encode($data_graph);
				return true;
				exit;								
			}
			
			$table .='</table>';
			//$mpdf->WriteHTML($table);
			$mpdf->WriteHTML($table);

			$filename = "complaint_pdf/pendency-level3-report-on-".date("Y-m-d-H-i-s").".pdf";
			//$mpdf->Output();
			$data['pdf_file_name'] = $filename;
			$mpdf->Output($filename,'D');
			
		}
		
		$data['middlecontent']='reports/pendency_level3';
		$data['master_mod'] = 'Reports';
	    $this->load->view('admin/admin_combo',$data);
		
	}


	// public function test_pdf()
	// {
	// 	error_reporting(E_ERROR | E_PARSE);
	// 	ob_clean();
	// 	include(APPPATH."third_party/mpdf/mpdf.php");
	// 	$mpdf=new mPDF('c');
	// 	$mpdf->mirrorMargins = 1;
	// 	$pdfHeader = $this->setPDF();
	// 	$mpdf->WriteHTML('hi');
	// 	$mpdf->Output('test.pdf');

	// 	//$filename = "test.pdf";			
	// 	//$mpdf->Output($filename, 'D');
	// 	exit;			
	// }
	
	public function get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $district_id, $sro_id, $online_id){
		
		
		$percentage = 0;
		
		if($online_id == false) {
			
			/*if($type_id == 1 || $type_id == 7 || $type_id == 15)
			{
				$conSro = "AND c.sro_office_id = '".$sro_id."'";
				
			} else if($type_id == 2 || $type_id == 9){
				
				$conSro = '';
			}*/
			
			if($type_id == 1 || $type_id == 7 || $type_id == 15)
			{
				$conSro = "AND c.is_level_1 = '1' AND c.sro_office_id = '".$sro_id."'";
				
			} else if($type_id == 2 || $type_id == 9){
				
				$conSro = "AND c.is_level_2 = '1'";
			}
			// Complaint Received In Selected Period	
			$query_sro_1 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.comp_code!='' ".$dateCon." ".$com_sub_type_con." AND c.district_id = '".$district_id."' ".$conSro."";
						
			
			// Total Complaint Received	
			 $query_sro_2 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.comp_code!='' ".$com_sub_type_con." AND c.district_id = '".$district_id."' ".$conSro."";
			
			// Total Complaint Resolved In Selected Period	
			$query_sro_3 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' ".$dateCon." AND c.comp_code!='' ".$com_sub_type_con." AND c.district_id = '".$district_id."' ".$conSro."";
			
			// Total Complaint Resolved
			$query_sro_4 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' AND c.comp_code!='' ".$com_sub_type_con." AND c.district_id = '".$district_id."' ".$conSro."";
			
			// Total Complaint Pending In Selected Period
			$query_sro_5 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='1' ".$dateCon." AND c.comp_code!='' ".$com_sub_type_con." AND c.district_id = '".$district_id."' ".$conSro."";
								
			// Total Complaint Pending
			$query_sro_6 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='1' AND c.comp_code!='' ".$com_sub_type_con." AND c.district_id = '".$district_id."' ".$conSro."";
			
				
			// Total Complaint Resolved in Percentage Selected Period
			$query_sro_7 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
							JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
							JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' AND c.comp_code!='' ".$com_sub_type_con." AND c.district_id = '".$district_id."' ".$dateCon."";
	
			
		} else {
				
				// Complaint Received In Selected Period	
				 $query_sro_1 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
								JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
								JOIN userregistration u ON c.user_id = u.user_id WHERE c.comp_code!='' ".$dateCon." AND c.complaint_type_id = '1' ";
							
				
				// Total Complaint Received	
				 $query_sro_2 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
								JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
								JOIN userregistration u ON c.user_id = u.user_id WHERE c.comp_code!='' AND c.complaint_type_id = '1' ";
				
				// Total Complaint Resolved In Selected Period	
				$query_sro_3 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
								JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
								JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' ".$dateCon." AND c.comp_code!='' AND c.complaint_type_id = '1' ";
				
				// Total Complaint Resolved
				$query_sro_4 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
								JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
								JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' AND c.comp_code!='' AND c.complaint_type_id = '1'";
				
				// Total Complaint Pending In Selected Period
				$query_sro_5 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
								JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
								JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='1' ".$dateCon." AND c.comp_code!='' AND c.complaint_type_id = '1'";
									
				// Total Complaint Pending
				$query_sro_6 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
								JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
								JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='1' AND c.comp_code!='' AND c.complaint_type_id = '1' ";
				
					
				// Total Complaint Resolved in Percentage Selected Period
				$query_sro_7 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
								JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
								JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' AND c.comp_code!='' AND c.complaint_type_id = '1'  ".$dateCon."";
				
			
		}		
		
			$count_1 	= $this->getNumData($query_sro_1);
			$count_2 	= $this->getNumData($query_sro_2);
			$count_3 	= $this->getNumData($query_sro_3);			
			$count_4 	= $this->getNumData($query_sro_4);
			$count_5 	= $this->getNumData($query_sro_5);
			$count_6 	= $this->getNumData($query_sro_6);
			$count_7 	= $this->getNumData($query_sro_7);	
			
								 
			$percentage = ($count_3/$count_1)*100;
			$overAllper = ($count_4/$count_2)*100;
			//echo ">>".$percentage."==".$overAllper;
			
			if (is_nan($percentage) || is_infinite($percentage)) {
				$percentage = 0;
			} else {
				$percentage = round($percentage, 2);
			}
			
			if (is_nan($overAllper) || is_infinite($overAllper)) {
				$overAllper = 0;
			} else {
				$overAllper = round($overAllper, 2);
			}
			
			$per_tage = $percentage.'%';
			$over_tage = $overAllper.'%';

			//$per_tage = '0%';
			//$over_tage = '0%';
			
			$count_array = array('count_1' => $count_1,
								 'count_2' => $count_2,
								 'count_3' => $count_3,
								 'count_4' => $count_4,
								 'count_5' => $count_5,
								 'count_6' => $count_6,
								 'count_7' => $count_7,
								 'percentage' => $per_tage,
								 'overall_per' => $over_tage);	
			
			return $count_array;
	
	}
	
	
	// IGRA/IGR/DIG etc Pendency Report 21/04/2020
	public function pendency_reports_level_3(){
		
		error_reporting(0);
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$region_id_1	= @$this->input->post('region_id')?$this->input->post('region_id'):'';
		$district_id 	= @$this->input->post('district_id')?$this->input->post('district_id'):'';
		//$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');
		$admin_id 		= $this->session->userdata('supadminid');
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
		
		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value
		$pageNo = ($_POST['start']/$_POST['length'])+1;
		
		
			
		//$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN('3','5','10','11','12','13','14','999') AND status = '1'";
		$impArr = implode(",", $this->secondArr);
		$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(".$impArr.") AND status = '1'";
		
		$officeTypesName = 'All';
		
		if($region_id_1){
			
			$condition .= " AND region_id = '".$region_id_1."'";
			
		}
		
		if($district_id){
			
			$condition .= " AND district_id = '".$district_id."'";
			
		}

		$dateCon = '';
		if($frm_date){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
		}
		
		if($to_date){
			
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
		}
		
					
		$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
		
		
		
		$result 	= $this->db->query($query)->result();			
		
		$rowCount = $this->getNumData($condition);
		
		$rowCnt = 0;
		$dataArr = array();
		
		
			if($rowCount > 0){
			
			$no    = $_POST['start'];
			$i = 0;
			foreach($result as $adminData){
			
			$primaryID 			= $adminData->id;	
			$region_id  		= $adminData->region_id;
			$type_id 			= $adminData->role_id;
			$d_id 				= $adminData->district_id;
			
			// Level
			$getRoleName  = $this->master_model->getRecords('gri_roles',array('role_id'=>$type_id, 'status' => '1'));
			$roleDetails  = $getRoleName[0]['role_name'];
			
			// Get Region Name
			$getRegionName  = $this->master_model->getRecords('gri_region_division',array('religion_id'=>$region_id, 'status' => '1'));
			$getRegionDetail= $getRegionName[0]['region_division_name'];
			
			// Get District Name
			$getDistrictName  = $this->master_model->getRecords('gri_district',array('district_id'=>$d_id, 'status' => '1'));
			$getDistriDetail  = $getDistrictName[0]['district_name'];			
			
			// Get Office Type Name
			$getOfficeTypeName  = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$adminData->office_id, 'status' => '1'));
			$getOfficeTypeName  = $getOfficeTypeName[0]['complaint_sub_type_name'];	

			// Get Office Address
			$getOfficeAdd   = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$adminData->sro_id, 'status' => '1'));
			$getAdd  		= $getOfficeAdd[0]['office_name'];		
			
						
			if($region_id == 4 && ($type_id == 2 || $type_id == 1)){
				
				$com_sub_type_con = "AND c.complaint_sub_type_id IN('6','7')";
				
				if($type_id == 2){
										
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = "", $online_id = false);
						
				} else if($type_id == 1){
				
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $d_id, $adminData->sro_id, $online_id = false);
					
				}
				
			} else if($region_id != 4 && ($type_id == 2 || $type_id == 1)){
				
				if($type_id == 2){
					
					$com_sub_type_con = "";					
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = "", $online_id = false);
						
				} else if($type_id == 1){
					
					$com_sub_type_con = "";
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $d_id, $adminData->sro_id, $online_id = false);
					
				}
			} 
			
			// Marriage Officer
			if($type_id == 15){
					
				$com_sub_type_con = "AND c.complaint_sub_type_id IN(6)";
				$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $d_id, $adminData->sro_id, $online_id = false);
				
			}
			
			

			if($region_id == 4 && ($type_id == 7 || $type_id == 9)){
				
				$com_sub_type_con = "AND c.complaint_sub_type_id IN(2)";
				
				if($type_id == 7){					
					
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $adminData->sro_id, $online_id = false);
												
				} else if($type_id == 9){
				
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = '', $online_id = false);
									
				}
				
			} else if($region_id != 4 && ($type_id == 7 || $type_id == 9)){
				
				//$com_sub_type_con = "AND c.complaint_sub_type_id IN(2)";
				$com_sub_type_con = "";
				
				if($type_id == 7){					
					
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $adminData->sro_id, $online_id = false);
												
				} else if($type_id == 9){
				
					$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = '', $online_id = false);
									
				}
				
			}
			
			// Help Desk User
			if($type_id == 4){
				
				$getRegionDetail = 'MUMBAI REGION';
				$com_sub_type_con = "";
				$gerArr = $this->get_pendency_level_3($type_id, $dateCon, $com_sub_type_con, $adminData->district_id, $sro_id = '', $online_id = true);
					
			}
			
			
				$i++;
					$dataArr[] = array(
						  $i,
						  $getRegionDetail,
                          $getDistriDetail,						  
						  $roleDetails,
						  $getAdd,
						  $gerArr['count_1'],
						  $gerArr['count_2'],
						  $gerArr['count_3'],
						  $gerArr['count_4'],						  
						  $gerArr['count_5'],
						  $gerArr['count_6'],
						  $gerArr['percentage'],
						  $gerArr['overall_per']
					   );
				
				
			   
			    $rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
				);
				
				
			}
			
		} else {
			
			$rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => 0,
				  "recordsFiltered" => 0,
				  "data" => $dataArr
				);
		}
		echo json_encode($response);
		
	}
	
	
	
	// Performance Report For JDR/ACS 21/04/2020	
	public function performance_level_2(){
		
		$type 			= $this->session->userdata('type');
		$dis_id			= $this->session->userdata('district_id');
		$ad_id			= $this->session->userdata('supadminid');	
		
		//$list_ids = array("999", "3", "11", "12", "13", "14");
		
		// Role ID
		if(in_array($type, $this->adminArr)){		
			
			$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE status = '1'";
			$data['officeList'] = $this->db->query($query_res)->result();
			
		} else {
			
			if($type == 2){
				
				$user_info=$this->master_model->getRecords('adminlogin',array('id'=>$ad_id, 'status' => '1'));
				if($user_info[0]['region_id'] == 4 && $user_info[0]['role_id'] == 2){ //2,6,7
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE office_sub_type IN(6,7) AND district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
					
				
				} else {
					
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
				} 
				
			} else if($type == 9){
				
				$user_info=$this->master_model->getRecords('adminlogin',array('id'=>$ad_id, 'status' => '1'));
				if($user_info[0]['region_id'] == 4 && $user_info[0]['role_id'] == 9){ //2,6,7
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE office_sub_type IN(2) AND district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
				
				} else {
					
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
				} 
				
			} else if($type == 5){
				
				$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE district_id = '".$dis_id."' AND status = '1'";
				$data['officeList'] = $this->db->query($query_res)->result(); 
				
			}			
			
		}
		
		//////////////////////  Download xls Functionality ////////////////////
		
		if(isset($_POST['btn_export']) && !empty($_POST['btn_export'])) 
		{			
			
			error_reporting(E_ERROR | E_PARSE);		
						
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
			
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');
			$admin_id 		= $this->session->userdata('supadminid');
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];
										
			$off_id  = '';
			if($office_subtype){			
				
				$off_id  .= " AND sro_id = '".$office_subtype."'";
				
			}	
					
			if($type == 2){
				
				if($getAdminR == 4){
					
					$condition = "SELECT * FROM adminlogin WHERE office_id IN(6,7) AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
					
					
				} else {
					
					$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
					
				}
				
				
			
			} else if($type == 9){
				
				if($getAdminR == 4){
					
					$condition = "SELECT * FROM adminlogin WHERE office_id IN(2) AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
					
					
				} else {
					
					$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
					
				}
				
			}	

			$dateCon = '';
			$date_Con = '';
			
			if($frm_date){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
				$date_Con .= " AND complaint_date >= '".$frm_format_date."'";
			}
			
			if($to_date){
				
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
				$date_Con .= " AND complaint_date <= '".$to_format_date."'";
			}
			
			
			$query = $condition;
			
			$result 	= $this->db->query($query)->result();			
			
			$rowCount = $this->getNumData($condition);
			
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");

			 $objPHPExcel = new PHPExcel();

			 $objPHPExcel->getProperties()->setCreator("");
			 $objPHPExcel->getProperties()->setLastModifiedBy("");
			 $objPHPExcel->getProperties()->setTitle("");
			 $objPHPExcel->getProperties()->setSubject("");
			 $objPHPExcel->getProperties()->setDescription("");

			 $objPHPExcel->setActiveSheetIndex(0);

			 $sheet = $objPHPExcel->getActiveSheet();		 
			

			 $sheet->setCellValue("A1","Designation");
			 $sheet->setCellValue("B1","Complaint Received (For The Selected Period)");
			 $sheet->setCellValue("C1","Complaint Resolved (For The Selected Period)");				
			 $sheet->setCellValue("D1","Complaint Pending (For The Selected Period)");
			 $sheet->setCellValue("E1","Complaint Escalated To JDR/ACS");
			 $sheet->setCellValue("F1","Percentage of complaint resolution");	

			 $row = 2;
			
			foreach($result as $key => $value){
					
				$name = $value->namecc;
				$role_id = $value->role_id;					
					
				$getDesignation = $this->master_model->getRecords('gri_roles',array('role_id'=>$role_id, 'status' => '1'));
				$role_name 	= $getDesignation[0]['role_name'];
				
				$fullDesignation = $role_name.' - '.$name;

				$getArr = $this->get_level_2($value->district_id, $dateCon, $value->sro_id);
							
				 $sheet->setCellValue("A".$row,$fullDesignation);
				 $sheet->setCellValue("B".$row,$getArr['count_1']);
				 $sheet->setCellValue("C".$row,$getArr['count_3']);
				 $sheet->setCellValue("D".$row,$getArr['count_5']);
				 $sheet->setCellValue("E".$row,$getArr['escalate']);
				 $sheet->setCellValue("F".$row,$getArr['percentage']);
					 
				$row++;
				
			}
			
			// $filename = "performance-Ranking-Level-2-report-on-".date("Y-m-d-H-i-s").".xls";
			$sheet->setTitle("Export File");
			$filename = "complaint_excel/performance-Ranking-Level-2-report-on-".date("Y-m-d-H-i-s").".xlsx";	
			/*header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");;
			header("Content-Disposition: attachment;filename=$filename");
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);*/
			// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
			//  //force user to download the Excel file without writing it to server's HD
			// $objWriter->save('php://output');
			// set_time_limit(30);
			// exit;
			
			// create file name			
			header("Content-Type: application/vnd.ms-excel");	
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			header('Content-Disposition: attachment;filename="performance-Ranking-Level-2-report-on-'.time().'.xlsx"');
			header('Cache-Control: max-age=0');			
			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			ob_end_clean();
			$objWriter->save('php://output');
			//$objWriter->save($fileName);
			exit; 
			
			$data['excel_file_name'] = $filename;
			$objWriter->save($filename);
		
		}
		
		///////// Download PDF ////////////
		
		if( (isset($_POST['btn_pdf']) && !empty($_POST['btn_pdf'])) ||  !empty($_POST['is_graph']) )  
		{
			
			error_reporting(E_ERROR | E_PARSE);		
			include(APPPATH."third_party/mpdf/mpdf.php");
			//$mpdf=new mPDF('c');
			$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
			$mpdf->mirrorMargins = 1;
			// Set PDF Header Footer HTML
			$pdfHeader = $this->setPDF();		
					
			$mpdf->SetHTMLHeader($pdfHeader['header_1']);
			$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
			$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
			$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');
			
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
			
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');
			$admin_id 		= $this->session->userdata('supadminid');
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];
							
			$off_id  = '';
			if($office_subtype){			
				
				$off_id  .= " AND sro_id = '".$office_subtype."'";
				
			}	
					
			if($type == 2){
				
				if($getAdminR == 4){
					
					$condition = "SELECT * FROM adminlogin WHERE office_id IN(6,7) AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
					
					
				} else {
					
					$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
					
				}
				
				
			
			} else if($type == 9){
				
				if($getAdminR == 4){
					
					$condition = "SELECT * FROM adminlogin WHERE office_id IN(2) AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
					
					
				} else {
					
					$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
					
				}
				
			}	

			$dateCon = '';
			$date_Con = '';
			
			if($frm_date){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
				$date_Con .= " AND complaint_date >= '".$frm_format_date."'";
			}
			
			if($to_date){
				
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
				$date_Con .= " AND complaint_date <= '".$to_format_date."'";
			}
			
			$query = $condition;
			
			$result 	= $this->db->query($query)->result();			
			
			$rowCount = $this->getNumData($condition);
			
			$table = '<h3>Performance Report Level 1</h3>
						<table border="1" class="table table-striped table-bordered dt-responsive nowrap">
						
							<tr>
								 <th>Designation</th>
								 <th>Complaint Received (For The Selected Period)</th>
								 <th>Complaint Resolved (For The Selected Period)</th>
								 <th>Complaint Pending (For The Selected Period)</th>
								 <th>Complaint Escalated To JDR/ACS </th>
								 <th>Percentage of complaint resolution</th>
							</tr>';
			$data_graph[] = array('Designation Name', 'Complaint Resolved (For The Selected Period)', 'Complaint Pending (For The Selected Period)');				
		
			foreach($result as $key => $value){
					
				$name = $value->namecc;
				$role_id = $value->role_id;
					
				$getDesignation = $this->master_model->getRecords('gri_roles',array('role_id'=>$role_id, 'status' => '1'));
				$role_name 	= $getDesignation[0]['role_name'];
				
				$fullDesignation = $role_name.' - '.$name;
				
				$getArr = $this->get_level_2($value->district_id, $dateCon, $value->sro_id);
				 
				 $table .='<tr>
							<td>'.$fullDesignation.'</td>
							<td align="center">'.$getArr['count_1'].'</td>
							<td align="center">'.$getArr['count_3'].'</td>
							<td align="center">'.$getArr['count_5'].'</td>
							<td align="center">'.$getArr['escalate'].'</td>
							<td align="center">'.$getArr['percentage'].'</td>
						</tr>';				
					
					//$data_graph[] = array($fullDesignation, $getArr['count_1'],$getArr['count_3'],$getArr['count_5']);
					$data_graph[] = array($fullDesignation, $getArr['count_3'],$getArr['count_5']);
				}
				
				// GRAPH code here //
				if(@$this->input->post('is_graph')){		
					echo json_encode($data_graph);
					return true;
					exit;								
				}
			
				$table .='</table>';					
				  
				
				$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins
		

				$mpdf->WriteHTML($table);

				$filename = "complaint_pdf/performance-Ranking-Level-2-report-on-".date("Y-m-d-H-i-s").".pdf";
				$data['pdf_file_name'] = $filename;
				$mpdf->Output($filename, 'D');
		
		}
		
		
		////////////////////////// End Functionality ///////////////////////
		
		$data['middlecontent']='reports/performance_level2';
		$data['master_mod'] = 'Reports';
		$data['function']='PerformanceLevel_1';
	    $this->load->view('admin/admin_combo',$data);
		
	}
	
	public function get_level_2($district_id, $dateCon, $sro_id){
		
		// Complaint Received In Selected Period	
		$query_sro_1 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN userregistration u ON c.user_id = u.user_id WHERE c.district_id = '".$district_id."'  ".$dateCon." AND c.sro_office_id = '".$sro_id."'";
		$count_1 	= $this->getNumData($query_sro_1);
		
		// Total Complaint Resolved in Percentage Selected Period
		$query_sro_7 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."' ".$dateCon."";
		$count_7 	= $this->getNumData($query_sro_7);
		
		// Total Complaint Received	
		$query_sro_2 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN userregistration u ON c.user_id = u.user_id WHERE c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."'";
		$count_2 	= $this->getNumData($query_sro_2);
		
		// Total Complaint Resolved In Selected Period	
		$query_sro_3 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' ".$dateCon." AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."'";
		$count_3 	= $this->getNumData($query_sro_3);
		
		// Total Complaint Resolved
		$query_sro_4 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='0' AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."'";
		$count_4 	= $this->getNumData($query_sro_4);
		
		// Total Complaint Pending In Selected Period
		$query_sro_5 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='1' ".$dateCon." AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."'";
		$count_5 	= $this->getNumData($query_sro_5);
		
		// Total Complaint Pending
		$query_sro_6 = "SELECT c.c_id, ct.complaint_type_name, c.comp_subject, c.complaint_date, c.comp_code, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, c.sro_office_id, u.user_name, c.reply_status FROM gri_complaint c 
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id 	  
						JOIN userregistration u ON c.user_id = u.user_id WHERE c.reply_status='1' AND c.district_id = '".$district_id."' AND c.sro_office_id = '".$sro_id."'";
		$count_6 	= $this->getNumData($query_sro_6);
		
		$sql_escalate 		= "SELECT c_id FROM gri_complaint WHERE is_level_2 = '1' AND district_id = '".$district_id."' AND comp_code!='' AND sro_office_id = '".$sro_id."' ".$date_Con."";
		$count_esacalate 	= $this->getNumData($sql_escalate);	
			
		$percentage = ($count_3/$count_2)*100;
		$overAllper = ($count_4/$count_2)*100;
		
		
		if (is_nan($percentage) || is_infinite($percentage)) {
			$percentage = "0";
		} else {
			$percentage = round($percentage, 2);
		}
		
		if (is_nan($overAllper) || is_infinite($overAllper)) {
			$overAllper = "0";
		} else {
			$overAllper = round($overAllper, 2);
		}
		
		$per_tage = $percentage.'%';
		$over_tage = $overAllper.'%';
		
		$returnArr = array('count_1' 	=> $count_1,
						   'count_3' 	=> $count_3,
						   'count_5' 	=> $count_5,
						   'escalate' 	=> $count_esacalate,
						   'percentage' => $per_tage);
						   
		return 	$returnArr;
				
	}
		
	// Performance Report For JDF/ACS 21/04/2020	
	public function performance_reports_level_2(){
		
		error_reporting(0);
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');
		$admin_id 		= $this->session->userdata('supadminid');
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
		
		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value
		$pageNo = ($_POST['start']/$_POST['length'])+1;

						
		$off_id  = '';
		if($office_subtype){			
			
			$off_id  .= " AND sro_id = '".$office_subtype."'";
			
		}	
				
		if($type == 2){
			
			if($getAdminR == 4){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id IN(6,7) AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'Marriage & Sub Register Office';
				$complaint_Type = 'Specific Office';
				
			} else {
				
				$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'All';
				$complaint_Type = 'Specific Office';
			}
			
			
		
		} else if($type == 9){
			
			if($getAdminR == 4){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id IN(2) AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'Collector Of Stamp Office';
				$complaint_Type = 'Specific Office';
				
			} else {
				
				$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'All';
				$complaint_Type = 'Specific Office';
			}
			
		}	

		$dateCon = '';
		$date_Con = '';
		
		if($frm_date){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
			$date_Con .= " AND complaint_date >= '".$frm_format_date."'";
		}
		
		if($to_date){
			
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
			$date_Con .= " AND complaint_date <= '".$to_format_date."'";
		}
			
		$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
		
		$result 	= $this->db->query($query)->result();			
		
		$rowCount = $this->getNumData($condition);
		
		$rowCnt = 0;
		$dataArr = array();
		
		
			if($rowCount > 0){
			
			$no    = $_POST['start'];
			$i = 0;
			foreach($result as $adminData){
				
			$name 		= $adminData->namecc;
			$role_id 	= $adminData->role_id;	
			
			$getDesignation = $this->master_model->getRecords('gri_roles',array('role_id'=>$role_id, 'status' => '1'));
			$role_name 	= $getDesignation[0]['role_name'];
			
			$fullDesignation = $role_name.' - '.$name;
			
			$getArr = $this->get_level_2($adminData->district_id, $dateCon, $adminData->sro_id);
			
					
				$i++;
					$dataArr[] = array(
						  $i,				  
						  $fullDesignation,
						  $getArr['count_1'],
						  $getArr['count_3'],
						  $getArr['count_5'],
						  $getArr['escalate'],
						  $getArr['percentage']
					   );
				
				
			   
			    $rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
				);
				
				
			}
			
		} else {
			
			$rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => 0,
				  "recordsFiltered" => 0,
				  "data" => $dataArr
				);
		}
		echo json_encode($response);
		
	}
	
	
	// Complaint Pending Report JDR / ACS
	
	public function complaint_overview(){

		$csrf_token = $this->security->get_csrf_hash();
		
		$type 			= $this->session->userdata('type');
		$dis_id			= $this->session->userdata('district_id');
		$ad_id			= $this->session->userdata('supadminid');	
		
		//$list_ids = array("999", "3", "11", "12", "13", "14");
		
		// Role ID
		if(in_array($type, $this->adminArr)){		
			
			$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE status = '1'";
			$data['officeList'] = $this->db->query($query_res)->result();
			
		} else {
			
			if($type == 2){
				
				$user_info=$this->master_model->getRecords('adminlogin',array('id'=>$ad_id, 'status' => '1'));
				if($user_info[0]['region_id'] == 4 && $user_info[0]['role_id'] == 2){ //2,6,7
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE office_sub_type IN(6,7) AND district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
					
				
				} else {
					
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
				} 
				
			} else if($type == 9){
				
				$user_info=$this->master_model->getRecords('adminlogin',array('id'=>$ad_id, 'status' => '1'));
				if($user_info[0]['region_id'] == 4 && $user_info[0]['role_id'] == 9){ //2,6,7
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE office_sub_type IN(2) AND district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
				
				} else {
					
					
					$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE district_id = '".$dis_id."' AND status = '1'";
					$data['officeList'] = $this->db->query($query_res)->result();
				} 
				
			} else if($type == 5){
				
				$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE district_id = '".$dis_id."' AND status = '1'";
				$data['officeList'] = $this->db->query($query_res)->result(); 
				
			}
			
			//$data['officeList']	= $this->master_model->getRecords('gri_sro_offices',array('district_id'=>$dis_id,'status' => '1'));
		}
		
		if(isset($_POST['btn_export']) && !empty($_POST['btn_export'])) 
		{
			
		error_reporting(0);
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');		
		$admin_id 		= $this->session->userdata('supadminid');
			
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];

		$offType = "";
		$off_id = "";
		if($office_subtype){
			
			$offType .= " AND c.sro_office_id = '".$office_subtype."'";
			$off_id  .= " AND sro_id = '".$office_subtype."'";
			
		}	
		
		if($type == 2){
			
			if($getAdminR == 4){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id IN(6,7) AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'Marriage & Sub Register Office';
				$complaint_Type = 'Specific Office';
				
			} else {
				
				$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'All';
				$complaint_Type = 'Specific Office';
			}
			
			
		
		} else if($type == 9){
			
			if($getAdminR == 4){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id = '2' AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'Collector Of Stamp Office';
				$complaint_Type = 'Specific Office';
				
			} else {
				
				$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'All';
				$complaint_Type = 'Specific Office';
			}
			
		} else if($type == 1){
			
			$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";		
			
		
		} else if($type == 15){
			
			$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";		
			
		
		} else if($type == 7){
			
			$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";		
			
		
		}
		
		
		$dateCon = '';
		$date_Con = '';
		$dateContion ='';
		if($frm_date){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
			$date_Con .= " AND complaint_date >= '".$frm_format_date."'";
			$dateContion .= " AND complaint_date >= '".$frm_format_date."'";
		}
		
		if($to_date){
			
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
			$date_Con .= " AND complaint_date <= '".$to_format_date."'";
			$dateContion .= " AND complaint_date <= '".$to_format_date."'";
		}
		
					
		$query = $condition;
		
		$result = $this->db->query($query)->result();
		
		
		$rowCnt = 0;
		
				 require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
				 require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");

				 $objPHPExcel = new PHPExcel();

				 $objPHPExcel->getProperties()->setCreator("");
				 $objPHPExcel->getProperties()->setLastModifiedBy("");
				 $objPHPExcel->getProperties()->setTitle("");
				 $objPHPExcel->getProperties()->setSubject("");
				 $objPHPExcel->getProperties()->setDescription("");

				 $objPHPExcel->setActiveSheetIndex(0);

				 $sheet = $objPHPExcel->getActiveSheet();
				 
				 							
				 $sheet->setCellValue("A1","Office Subtype");
				 $sheet->setCellValue("B1","Total Complaint Received");
				 $sheet->setCellValue("C1","Total Complaint Resolved");
				 $sheet->setCellValue("D1","Pending For 5 Days Or More");
				 $sheet->setCellValue("E1","Pending For 3-4 Days");
				 $sheet->setCellValue("F1","Pending For 2 Days or less");
					

				 $row = 2;
				
				 foreach ($result as $key => $comData)
				 { 	
					
					
					// Role Name	
					$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$comData->role_id, 'status' => '1'));
					$roleName = $roleDetail[0]['role_name'];
					
					// Office Name
					$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$comData->sro_id, 'status' => '1'));
					$officeName   = $officeDetail[0]['office_name'];
					
					// District Name 
					$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$comData->district_id));
					$district_name		= $districtDetail[0]['district_name'];
					
					if($type == 999 || $type == 3 || $type == 11 || $type == 12 || $type == 13 || $type == 14){
				
								$designationName = $district_name." - ".$officeName;
								
							} else if($type==2 || $type==9){
								
								//$designationName = $district_name;
								$designationName = $officeName;
								
							} else if($type==1 || $type==7 || $type==15){
								
								$designationName = $district_name." - ".$officeName;
								
							} else if($type == 4 || $type == 5){
								
								$designationName = $roleName;
								
							}
							
							
							if($getAdminR == 4 && ($type==1 || $type==2)){
				
								if($type==2){					
									
									$con_mum = "AND c.complaint_sub_type_id IN('6','7')";	
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];					
									
								} else if($type==1){					
									
									$con_mum = "AND c.complaint_sub_type_id IN('7')";
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];
									
									
								} 					
									
								
							}
							
							if($getAdminR != 4 && ($type==1 || $type==2)){				
								
								$con_mum = "";
								
								if($type==2){
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];				
									
									
								} else if($type==1){					
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];
									
								} 				
									
								
							}
									
						if($getAdminR == 4 && ($type == 7 || $type == 9)){
							
							$con_mum = "AND c.complaint_sub_type_id IN('2')";	
							
							if($type == 9){
								
								$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
								
								$cntRecord 		= $getArr['count_allrecord'];
								$cntResolve 	= $getArr['count_resolved'];
								$cntPending_5 	= $getArr['count_pending'];
								
								
							} else if($type == 7){
								
								$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
								$cntRecord 		= $getArr['count_allrecord'];
								$cntResolve 	= $getArr['count_resolved'];
								$cntPending_5 	= $getArr['count_pending'];
											
								
							}
							 
							
						}
							
							if($getAdminR != 4 && ($type == 7 || $type == 9)){
								
								$con_mum = "";
								
								if($type == 9){
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];
									
									
								} else if($type == 7){
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];
									
								}					
									
								
							}
							
							if($type==15){					
								
								$con_mum = "AND c.complaint_sub_type_id IN('6')";
								
								$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
								$cntRecord 		= $getArr['count_allrecord'];
								$cntResolve 	= $getArr['count_resolved'];
								$cntPending_5 	= $getArr['count_pending'];
								
							}
					
					
						$r=0;
						$d=0;
						$s=0;								
					
						foreach($cntPending_5 as $pend5){
							
							$currentDate = date('Y-m-d');
							
							$assign_date = $pend5->assign_date;
							
							$newDate = date("Y-m-d", strtotime($assign_date));
							
							$dateDiff = $this->dateDiffInDays($assign_date, $currentDate);		
							
							if($dateDiff >= 5){
								$r++;					
							} 
							else if($dateDiff > 2 && $dateDiff <= 4){
								$d++;
							} 
							else if($dateDiff <= 2){					
								$s++;
							} 			
							
						}
					
					 $sheet->setCellValue("A".$row,$designationName);
					 $sheet->setCellValue("B".$row,$cntRecord);
					 $sheet->setCellValue("C".$row,$cntResolve);
					 $sheet->setCellValue("D".$row,$r);
					 $sheet->setCellValue("E".$row,$d);
					 $sheet->setCellValue("F".$row,$s);
					 
					 $row++;				
											
					 
					 
				 }

				 	
				 // $filename = "complaint-overview-report-level-2-on-".date("Y-m-d-H-i-s").".xls";
				 $sheet->setTitle("Export File");
				$filename = "complaint_excel/complaint-overview-report-on-".date("Y-m-d-H-i-s").".xlsx";
				 
				/*header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");;
				header("Content-Disposition: attachment;filename=$filename");
				$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);*/
				// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
				//  //force user to download the Excel file without writing it to server's HD
				// $objWriter->save('php://output');
				// set_time_limit(30);
				// exit;
				
				// create file name			
				header("Content-Type: application/vnd.ms-excel");	
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
				header('Content-Disposition: attachment;filename="complaint-overview-report-on-'.time().'.xlsx"');
				header('Cache-Control: max-age=0');			
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				ob_end_clean();
				$objWriter->save('php://output');
				//$objWriter->save($fileName);
				exit; 
				
				$data['excel_file_name'] = $filename;
				$objWriter->save($filename);
			
		}
		
		if( (isset($_POST['btn_pdf']) && !empty($_POST['btn_pdf'])) ||  !empty($_POST['is_graph']) )  
		{
			
		error_reporting(E_ERROR | E_PARSE);		
		include(APPPATH."third_party/mpdf/mpdf.php");
		//$mpdf=new mPDF('c');
		
		$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
		$mpdf->mirrorMargins = 1;
		
		// Set PDF Header Footer HTML
		$pdfHeader = $this->setPDF();		
				
		$mpdf->SetHTMLHeader($pdfHeader['header_1']);
		$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
		$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
		$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');
		
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');		
		$admin_id 		= $this->session->userdata('supadminid');
			
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];

		$offType = "";
		$off_id = "";
		if($office_subtype){
			
			$offType .= " AND c.sro_office_id = '".$office_subtype."'";
			$off_id  .= " AND sro_id = '".$office_subtype."'";
			
		}	
		
		if($type == 2){
			
			if($getAdminR == 4){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id IN(6,7) AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'Marriage & Sub Register Office';
				$complaint_Type = 'Specific Office';
				
			} else {
				
				$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'All';
				$complaint_Type = 'Specific Office';
			}
			
			
		
		} else if($type == 9){
			
			if($getAdminR == 4){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id = '2' AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'Collector Of Stamp Office';
				$complaint_Type = 'Specific Office';
				
			} else {
				
				$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'All';
				$complaint_Type = 'Specific Office';
			}
			
		} else if($type == 1){
			
			$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";		
			
		
		} else if($type == 15){
			
			$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";		
			
		
		} else if($type == 7){
			
			$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";		
			
		
		}	
		
		
		$dateCon = '';
		$date_Con = '';
		$dateContion ='';
		if($frm_date){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
			$date_Con .= " AND complaint_date >= '".$frm_format_date."'";
			$dateContion .= " AND complaint_date >= '".$frm_format_date."'";
		}
		
		if($to_date){
			
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
			$date_Con .= " AND complaint_date <= '".$to_format_date."'";
			$dateContion .= " AND complaint_date <= '".$to_format_date."'";
		}
		
					
		$query = $condition;
		
		$result = $this->db->query($query)->result();
		
		
		$rowCnt = 0;
				 
				 $table = '<h3>Complaint Overview Report</h3>
							<table border="1" class="table table-striped table-bordered dt-responsive nowrap">
							
								<tr>
									 <th>Office Subtype</th>
									 <th>Total Complaint Received</th>
									 <th>Total Complaint Resolved</th>
									 <th>Pending for 5 days or more</th>
									 <th>Pending for 3-4 days</th>
									 <th>Pending for 2 days or less</th>
								</tr>';
				
				$data_graph[] = array('Designation Name', 'Total Complaint Received', 'Pending From 5 Days Or More', 'Pending From 3-4 Days', 'Pending From 2 Days Or Less');
				
				 foreach ($result as $key => $comData)
				 { 	
					
					
					// Role Name	
					$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$comData->role_id, 'status' => '1'));
					$roleName = $roleDetail[0]['role_name'];
					
					// Office Name
					$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$comData->sro_id, 'status' => '1'));
					$officeName   = $officeDetail[0]['office_name'];
					
					// District Name 
					$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$comData->district_id));
					$district_name		= $districtDetail[0]['district_name'];
					
					
							//$list_ids = array("999", "3", "11", "12", "13", "14");
		
							// Role ID
							if(in_array($type, $this->adminArr)){	
					
								$designationName = $district_name." - ".$officeName;
								
							} else if($type==2 || $type==9){
								
								//$designationName = $district_name;
								$designationName = $officeName;
								
							} else if($type==1 || $type==7 || $type == 15){
								
								$designationName = $district_name." - ".$officeName;
								
							} else if($type == 4 || $type == 5){
								
								$designationName = $roleName;
								
							}
							
							
							if($getAdminR == 4 && ($type==1 || $type==2)){
				
								if($type==2){					
									
									$con_mum = "AND c.complaint_sub_type_id IN('6','7')";	
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];					
									
								} else if($type==1){					
									
									$con_mum = "AND c.complaint_sub_type_id IN('7')";
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];
									
									
								} 					
									
								
							}
							
							if($getAdminR != 4 && ($type==1 || $type==2)){				
								
								$con_mum = "";
								
								if($type==2){
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];				
									
									
								} else if($type==1){					
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];
									
								} 				
									
								
							}
									
						if($getAdminR == 4 && ($type == 7 || $type == 9)){
							
							$con_mum = "AND c.complaint_sub_type_id IN('2')";	
							
							if($type == 9){
								
								$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
								
								$cntRecord 		= $getArr['count_allrecord'];
								$cntResolve 	= $getArr['count_resolved'];
								$cntPending_5 	= $getArr['count_pending'];
								
								
							} else if($type == 7){
								
								$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
								$cntRecord 		= $getArr['count_allrecord'];
								$cntResolve 	= $getArr['count_resolved'];
								$cntPending_5 	= $getArr['count_pending'];
											
								
							}
							 
							
						}
							
							if($getAdminR != 4 && ($type == 7 || $type == 9)){
								
								$con_mum = "";
								
								if($type == 9){
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];
									
									
								} else if($type == 7){
									
									$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
									$cntRecord 		= $getArr['count_allrecord'];
									$cntResolve 	= $getArr['count_resolved'];
									$cntPending_5 	= $getArr['count_pending'];
									
								}					
									
								
							}
							
							if($type==15){					
								
								$con_mum = "AND c.complaint_sub_type_id IN('6')";
								
								$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
								$cntRecord 		= $getArr['count_allrecord'];
								$cntResolve 	= $getArr['count_resolved'];
								$cntPending_5 	= $getArr['count_pending'];
								
							}
					
						$r=0;
						$d=0;
						$s=0;								
					
						foreach($cntPending_5 as $pend5){
							
							$currentDate = date('Y-m-d');
							
							$assign_date = $pend5->assign_date;
							
							$newDate = date("Y-m-d", strtotime($assign_date));
							
							$dateDiff = $this->dateDiffInDays($assign_date, $currentDate);		
							
							if($dateDiff >= 5){
								$r++;					
							} 
							else if($dateDiff > 2 && $dateDiff <= 4){
								$d++;
							} 
							else if($dateDiff <= 2){					
								$s++;
							} 			
							
						}
						
					$data_graph[] = array($designationName, $cntRecord,$r,$d,$s);
					
					$table .='<tr>
								<td>'.$designationName.'</td>
								<td align="center">'.$cntRecord.'</td>
								<td align="center">'.$cntResolve.'</td>
								<td align="center">'.$r.'</td>
								<td align="center">'.$d.'</td>
								<td align="center">'.$s.'</td>							
							</tr>';
					 
				 }
				 
				  // GRAPH code here //
			   if(@$this->input->post('is_graph')){
		
					echo json_encode($data_graph);
					return true;
					exit;	
				}
				 
				 $table .='</table>';					
				  
				
				$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins
		

				$mpdf->WriteHTML($table);

				$filename = "complaint_pdf/complaint-overview-report-level-2-on-".date("Y-m-d-H-i-s").".pdf";
				$data['pdf_file_name'] = $filename;
				$mpdf->Output($filename, 'D');
			
		}
		
		$data['middlecontent']='reports/complaint_overview';
		$data['master_mod'] = 'Reports';
		$data['function']='ComplaintLevel';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	
	public function get_data_result($con_mum, $id, $dateContion, $offType){
		
		$getRegisterCnt = "SELECT c.comp_code FROM gri_complaint c 
								JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
					JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.comp_code!='' ".$con_mum." AND ca.assign_user_id = '".$id."' ".$dateContion." ".$offType."";
		$cntRecord = $this->db->query($getRegisterCnt)->num_rows();
		
		$getResolveCnt = "SELECT c.comp_code FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.reply_status='0' AND c.comp_code!='' ".$con_mum." AND c.compaint_closed_by = '".$id."' ".$dateContion." ".$offType."";
		$cntResolve = $this->db->query($getResolveCnt)->num_rows();
		
					
		$pending_days_5 = "SELECT c.comp_code, ca.assign_date FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id 
							WHERE c.reply_status='1' AND c.comp_code!='' AND ca.status = '1' ".$con_mum." AND ca.assign_user_id = '".$id."' ".$dateContion." ".$offType."";
		$cntPending_5 = $this->db->query($pending_days_5)->result();
		
		$resultArr = array( 'count_allrecord' => $cntRecord,
							'count_resolved'  => $cntResolve,
							'count_pending'	  => $cntPending_5);
							
		return 	$resultArr;
		
	}
	
	// Complaint Report Ajax Call 21/04/2020
	public function complaint_reports(){	
		
		error_reporting(0);
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');		
		$admin_id 		= $this->session->userdata('supadminid');
			
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		//print_r($getLogin);
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
		
		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value
		$pageNo = ($_POST['start']/$_POST['length'])+1;	

		$offType = "";
		$off_id = "";
		if($office_subtype){
			
			$offType .= " AND c.sro_office_id = '".$office_subtype."'";
			$off_id  .= " AND sro_id = '".$office_subtype."'";
			
		}	
		
		if($type == 2){
			
			if($getAdminR == 4){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id IN(6,7) AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'Marriage & Sub Register Office';
				$complaint_Type = 'Specific Office';
				
			} else {
				
				$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'All';
				$complaint_Type = 'Specific Office';
			}
			
			
		
		} else if($type == 9){
			
			if($getAdminR == 4){
				
				$condition = "SELECT * FROM adminlogin WHERE office_id = '2' AND district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'Collector Of Stamp Office';
				$complaint_Type = 'Specific Office';
				
			} else {
				
				$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND status = '1' ".$off_id."";
				$officeTypesName = 'All';
				$complaint_Type = 'Specific Office';
			}
			
		} else if($type == 1){
			
			$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";		
			
		
		} else if($type == 15){
			
			$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";		
			
		
		} else if($type == 7){
			
			$condition = "SELECT * FROM adminlogin WHERE district_id = '".$getAdminD."' AND sro_id = '".$getAdminS."' AND status = '1' ".$off_id."";		
			
		
		}	
		
		
		$dateCon = '';
		$date_Con = '';
		$dateContion ='';
		if($frm_date){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$dateCon .= " AND c.complaint_date >= '".$frm_format_date."'";
			$date_Con .= " AND complaint_date >= '".$frm_format_date."'";
			$dateContion .= " AND complaint_date >= '".$frm_format_date."'";
		}
		
		if($to_date){
			
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateCon .= " AND c.complaint_date <= '".$to_format_date."'";
			$date_Con .= " AND complaint_date <= '".$to_format_date."'";
			$dateContion .= " AND complaint_date <= '".$to_format_date."'";
		}				
			
		$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
		
		$result 	= $this->db->query($query)->result();		
		
		$rowCount = $this->getNumData($condition);
		
		$rowCnt = 0;
		$dataArr = array();
		
		if($rowCount > 0){
			
			$no    = $_POST['start'];
			
			foreach($result as $comData){
				
			// Role Name	
			$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$comData->role_id, 'status' => '1'));
			$roleName = $roleDetail[0]['role_name'];
			
			// Office Name
			$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$comData->sro_id, 'status' => '1'));
			$officeName   = $officeDetail[0]['office_name'];
			
			// District Name 
			$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$comData->district_id));
			$district_name		= $districtDetail[0]['district_name'];
			
			
			if($type == 999 || $type == 3 || $type == 11 || $type == 12 || $type == 13 || $type == 14 || $type == 16){
				
				$designationName = $district_name." - ".$officeName;
				
			} else if($type==2 || $type==9){
				
				//$designationName = $district_name;
				$designationName = $officeName;
				
			} else if($type==1 || $type==7 || $type==15){
				
				$designationName = $district_name." - ".$officeName;
				
			} else if($type == 4 || $type == 5){
				
				$designationName = $roleName;
				
			}
			
			
			if($getAdminR == 4 && ($type==1 || $type==2)){
				
				if($type==2){					
					
					$con_mum = "AND c.complaint_sub_type_id IN('6','7')";	
					
					$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
					
					$cntRecord 		= $getArr['count_allrecord'];
					$cntResolve 	= $getArr['count_resolved'];
					$cntPending_5 	= $getArr['count_pending'];					
					
				} else if($type==1){					
					
					$con_mum = "AND c.complaint_sub_type_id IN('7')";
					
					$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
					
					$cntRecord 		= $getArr['count_allrecord'];
					$cntResolve 	= $getArr['count_resolved'];
					$cntPending_5 	= $getArr['count_pending'];
					
					
				} 					
					
				
			}
			
			if($getAdminR != 4 && ($type==1 || $type==2)){				
				
				$con_mum = "";
				
				if($type==2){
					
					$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
					
					$cntRecord 		= $getArr['count_allrecord'];
					$cntResolve 	= $getArr['count_resolved'];
					$cntPending_5 	= $getArr['count_pending'];				
					
					
				} else if($type==1){					
					
					$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
					
					$cntRecord 		= $getArr['count_allrecord'];
					$cntResolve 	= $getArr['count_resolved'];
					$cntPending_5 	= $getArr['count_pending'];
					
				} 				
					
				
			}
					
		if($getAdminR == 4 && ($type == 7 || $type == 9)){
			
			$con_mum = "AND c.complaint_sub_type_id IN('2')";	
			
			if($type == 9){
				
				$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
				
				$cntRecord 		= $getArr['count_allrecord'];
				$cntResolve 	= $getArr['count_resolved'];
				$cntPending_5 	= $getArr['count_pending'];
				
				
			} else if($type == 7){
				
				$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
				$cntRecord 		= $getArr['count_allrecord'];
				$cntResolve 	= $getArr['count_resolved'];
				$cntPending_5 	= $getArr['count_pending'];
							
				
			}
			 
			
		}
			
			if($getAdminR != 4 && ($type == 7 || $type == 9)){
				
				$con_mum = "";
				
				if($type == 9){
					
					$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
					$cntRecord 		= $getArr['count_allrecord'];
					$cntResolve 	= $getArr['count_resolved'];
					$cntPending_5 	= $getArr['count_pending'];
					
					
				} else if($type == 7){
					
					$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
					$cntRecord 		= $getArr['count_allrecord'];
					$cntResolve 	= $getArr['count_resolved'];
					$cntPending_5 	= $getArr['count_pending'];
					
				}					
					
				
			}
			
			if($type==15){					
				
				$con_mum = "AND c.complaint_sub_type_id IN('6')";
				
				$getArr = $this->get_data_result($con_mum, $comData->id, $dateContion, $offType);
				$cntRecord 		= $getArr['count_allrecord'];
				$cntResolve 	= $getArr['count_resolved'];
				$cntPending_5 	= $getArr['count_pending'];
				
			}
			
			
			$no++;
				
			$r=0;
			$d=0;
			$s=0;
						
			//print_r($cntPending_5);
				foreach($cntPending_5 as $pend5){
					
					$currentDate = date('Y-m-d');
					
					$assign_date = $pend5->assign_date;
					
					$newDate = date("Y-m-d", strtotime($assign_date));
					
					$dateDiff = $this->dateDiffInDays($assign_date, $currentDate);		
					
					if($dateDiff >= 5){
						$r++;					
					} 
					else if($dateDiff > 2 && $dateDiff <= 4){
						$d++;
					} 
					else if($dateDiff <= 2){					
						$s++;
					} 			
					
				} 	
				
				$dataArr[] = array(
					  $no,				  
					  $designationName,
					  $cntRecord,
					  $cntResolve,
					  $r,
					  $d,						  
					  $s
				   );
				
			   
			    $rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
				);
				
				
			} // Foreach Close  
			
		} else {
			
			$rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => 0,
				  "recordsFiltered" => 0,
				  "data" => $dataArr
				);
		}
		
		echo json_encode($response);
		
	}
	
	//// Performance Report IGR/Jr HQ For Level 1 : 22/04/2020	
	public function performance_level_3(){
		
		$type 			= $this->session->userdata('type');
		$dis_id			= $this->session->userdata('district_id');
		$ad_id			= $this->session->userdata('supadminid');	
		$data['pdf_file_name'] = '';
			
		$data['region_list'] = $this->master_model->getRecords('gri_region_division',array('status' => '1'));
			
		
		//////////////////////  Download xls Functionality ////////////////////		
		if(isset($_POST['btn_export']) && !empty($_POST['btn_export'])) 
		{			
			
			error_reporting(E_ERROR | E_PARSE);
			
			$c_district 	= @$this->input->post('district_id')?$this->input->post('district_id'):'';
			$c_region_id	= @$this->input->post('region_id')?$this->input->post('region_id'):'';
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');		
			$admin_id 		= $this->session->userdata('supadminid');
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];
			
			$impArr = implode(",", $this->adminArr);			
			//$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(999,3,10,11,12,13,14) AND status = '1'";
			$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(".$impArr.") AND status = '1'";			
			if($c_district){
				
				$condition .= " AND district_id = '".$c_district."'";
			}
			
			if($c_region_id){
				
				$condition .= " AND region_id = '".$c_region_id."'";
			}	 	
			
			$dateContion = "";		
			
			
			if($frm_date!="" && $to_date!=""){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
			}
			
			$result 	= $this->db->query($condition)->result();
			
			$rowCount = $this->getNumData($condition);
			
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");

			 $objPHPExcel = new PHPExcel();

			 $objPHPExcel->getProperties()->setCreator("");
			 $objPHPExcel->getProperties()->setLastModifiedBy("");
			 $objPHPExcel->getProperties()->setTitle("");
			 $objPHPExcel->getProperties()->setSubject("");
			 $objPHPExcel->getProperties()->setDescription("");

			 $objPHPExcel->setActiveSheetIndex(0);

			 $sheet = $objPHPExcel->getActiveSheet();		 
			

			 $sheet->setCellValue("A1","Region");
			 $sheet->setCellValue("B1","District");
			 $sheet->setCellValue("C1","Office Address");
			 $sheet->setCellValue("D1","Complaint Received (For The Selected Period)");
			 $sheet->setCellValue("E1","Complaint Resolved (For The Selected Period)");				
			 $sheet->setCellValue("F1","Complaint Pending (For The Selected Period)");
			 $sheet->setCellValue("G1","Complaint Escalated To JDR/ACS");
			 $sheet->setCellValue("H1","Percentage of complaint resolution");	

			 
			
			foreach($result as $key => $value){
					
				$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$value->role_id, 'status' => '1'));
				$roleName   = $roleDetail[0]['role_name'];
				$roleID     = $roleDetail[0]['role_id'];
				
				// Region Name
				$regionDetail = $this->master_model->getRecords('gri_region_division',array('religion_id'=>$value->region_id, 'status' => '1'));
				$regionName   = $regionDetail[0]['region_division_name'];
				
				// District Name 
				$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$value->district_id));
				$district_name		= $districtDetail[0]['district_name'];
				
				// Complaint Sub Type name
				$officeSubType_Name = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$value->office_id, 'status' => '1'));
				$office_sub_name	= $officeSubType_Name[0]['complaint_sub_type_name'];
				
				// Office Address
				$officeAdd = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$value->sro_id, 'status' => '1'));
				$office_add	= $officeAdd[0]['office_name'];
										
				// Office name
				$officeDetail 	= $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$value->sro_id, 'status' => '1'));
				$office_name 	= $officeDetail[0]['office_name'];
				$designation	= $district_name." (".$roleName.")";
				$print_dest		= $district_name." (".$roleName.")";
				
				if($roleID == 5 || $roleID == 4){
					
					$regionName = 'MUMBAI REGION';
					
				}
				
					// Callback function to get Details
					$returnArr = $this->get_performance_result($roleID, $value->id, $dateContion);
				
					
					$value->percentage 		= $returnArr['percentage'];	
					$value->roleName 		= $roleName;
					$value->fullAdd 		= $fullAdd;
					$value->totalAssign 	= $returnArr['totalAssign'];
					$value->totalResolved 	= $returnArr['totalResolved'];
					$value->regionName		= $regionName;
					$value->district_name	= $designation;
					$value->cntPending		= $returnArr['totalPending'];
					$value->cntEscalation	= $returnArr['totalEscalation'];
					$value->officeSubTypesName= $office_sub_name;
					$value->print_dest		= $print_dest;
					$value->office_add		= $office_add;
					
					
					$sortedResults[] = $value; 
							
			} // First For Close

				$price = array();
				foreach ($sortedResults as $key => $row)
				{
					$price[$key] = $row->percentage;
				}
				array_multisort($price, SORT_DESC, $sortedResults);
				
				//echo "<pre>";print_r($sortedResults);die();
				$row = 2;
				foreach($sortedResults as $post){
					// do your html here e.g
					$per = $post->percentage."%";					   
				  
					 $sheet->setCellValue("A".$row,$post->regionName);
					 $sheet->setCellValue("B".$row,$post->district_name);
					 //$sheet->setCellValue("C".$row,$post->officeSubTypesName);
					 $sheet->setCellValue("C".$row,$post->office_add);
					 $sheet->setCellValue("D".$row,$post->totalAssign);
					 $sheet->setCellValue("E".$row,$post->totalResolved);
					 $sheet->setCellValue("F".$row,$post->cntPending);
					 $sheet->setCellValue("G".$row,$post->cntEscalation);
					 $sheet->setCellValue("H".$row,$per);	
						
					$row++;
						
				}	
			
			// $filename = "performance-level3-report-on-".date("Y-m-d-H-i-s").".xls";
			$sheet->setTitle("Export File");
			$filename = "complaint_excel/performance-level3-report-on-".date("Y-m-d-H-i-s").".xlsx";	
			/*header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header("Content-Disposition: attachment;filename=$filename");
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); */
			
			// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
			
			 //force user to download the Excel file without writing it to server's HD
			// $objWriter->save('php://output');
			// set_time_limit(30);
			// exit;
			
			// create file name			
			header("Content-Type: application/vnd.ms-excel");	
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			header('Content-Disposition: attachment;filename="performance-level3-report-on-'.time().'.xlsx"');
			header('Cache-Control: max-age=0');			
			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			ob_end_clean();
			$objWriter->save('php://output');
			//$objWriter->save($fileName);
			exit; 
			
			$data['excel_file_name'] = $filename;
			$objWriter->save($filename);
		
		}
		
		///////// Download PDF ////////////
		
		if( (isset($_POST['btn_pdf']) && !empty($_POST['btn_pdf'])) ||  !empty($_POST['is_graph']) )  
		{
			error_reporting(E_ERROR | E_PARSE);		
			include(APPPATH."third_party/mpdf/mpdf.php");
			//$mpdf=new mPDF('','A4');
			
			$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
			$mpdf->mirrorMargins = 1;
			
			// Set PDF Header Footer HTML
			$pdfHeader = $this->setPDF();		
					
			$mpdf->SetHTMLHeader($pdfHeader['header_1']);
			$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
			$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
			$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');
			
			$mpdf->simpleTables = true;
			$mpdf->packTableData = true;
			$keep_table_proportions = TRUE;
			$mpdf->shrink_tables_to_fit=1;
						
			$c_district 	= @$this->input->post('district_id')?$this->input->post('district_id'):'';
			$c_region_id	= @$this->input->post('region_id')?$this->input->post('region_id'):'';
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');		
			$admin_id 		= $this->session->userdata('supadminid');
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];			
			
			$impArr = implode(",", $this->adminArr);
			//$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(999,3,10,11,12,13,14) AND status = '1'";
			$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(".$impArr.") AND status = '1'";			
			if($c_district){
				
				$condition .= " AND district_id = '".$c_district."'";
			}
			
			if($c_region_id){
				
				$condition .= " AND region_id = '".$c_region_id."'";
			}	 	
			
			$dateContion = "";
			
			
			
			if($frm_date!="" && $to_date!=""){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
			}
			
			
			
			$result 	= $this->db->query($condition)->result();
			
			$i = 1;
						
			$table = '<h3>Performance Report Level 3</h3>
						<table border="1" style="width:100%;">						
							<tr> 
								 <th width="10%">Sr.No. </th>	
								 <th width="10%">Region </th>
								 <th width="10%">District</th>
								 <th width="20%">Office Address</th>
								 <th width="15%">Complaint Received</th>
								 <th width="15%">Complaint Resolved</th>
								 <th width="15%">Complaint Pending</th>
								 <th width="15%">Escalated To JDR/ACS</th>
								 <th width="10%">Percentage</th>
							</tr>';
		
				$data_graph[] = array('Designation Name', 'Complaint Received', 'Complaint Resolved', 'Complaint Pending');
			
			foreach($result as $key => $value){
					
				$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$value->role_id, 'status' => '1'));
				$roleName   = $roleDetail[0]['role_name'];
				$roleID     = $roleDetail[0]['role_id'];
				
				// Region Name
				$regionDetail = $this->master_model->getRecords('gri_region_division',array('religion_id'=>$value->region_id, 'status' => '1'));
				$regionName   = $regionDetail[0]['region_division_name'];
				
				// District Name 
				$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$value->district_id));
				$district_name		= $districtDetail[0]['district_name'];
				
				// Complaint Sub Type name
				$officeSubType_Name = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$value->office_id, 'status' => '1'));
				$office_sub_name	= $officeSubType_Name[0]['complaint_sub_type_name'];
										
				// Office name
				$officeDetail 	= $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$value->sro_id, 'status' => '1'));
				$office_name 	= $officeDetail[0]['office_name'];
				$designation	= $district_name."<br />(".$roleName.")";
				$print_dest		= $district_name." (".$roleName.")";
				
				if($roleID == 5 || $roleID == 4){
					
					$regionName = 'MUMBAI REGION';
					
				}
				
					// Callback function to get Details
					$returnArr = $this->get_performance_result($roleID, $value->id, $dateContion);
				
					
					$value->percentage 		= $returnArr['percentage'];	
					$value->roleName 		= $roleName;
					$value->fullAdd 		= $fullAdd;
					$value->totalAssign 	= $returnArr['totalAssign'];
					$value->totalResolved 	= $returnArr['totalResolved'];
					$value->regionName		= $regionName;
					$value->district_name	= $designation;
					$value->cntPending		= $returnArr['totalPending'];
					$value->cntEscalation	= $returnArr['totalEscalation'];
					$value->officeSubTypesName= $office_sub_name;
					$value->print_dest		= $office_name;
					$value->office_name		= $office_name;
					if($office_name == ""){
						$value->office_name		= $roleName;
					} else {
						$value->office_name		= $office_name;
					}
					
					$sortedResults[] = $value;
						
				}
				
				$price = array();
					foreach ($sortedResults as $key => $row)
					{
						$price[$key] = $row->percentage;
					}
					array_multisort($price, SORT_DESC, $sortedResults);
				
				foreach($sortedResults as $post){
					// do your html here e.g
					$per = $post->percentage."%";
						   
				   $table .='<tr>	
							<td align="center">'.$i.'</td>
							<td>'.$post->regionName.'</td>
							<td>'.$post->district_name.'</td>
							<td>'.$post->office_name.'</td>
							<td align="center">'.$post->totalAssign.'</td>
							<td align="center">'.$post->totalResolved.'</td>
							<td align="center">'.$post->cntPending.'</td>
							<td align="center">'.$post->cntEscalation.'</td>
							<td align="center">'.$per.'</td>
						</tr>';
						
					$i++;
					
					//$post->office_name					
					$data_graph[] = array($post->office_name, $post->totalAssign,$post->totalResolved,$post->cntPending);
					//$office_name = '';
						
				}
				
				// GRAPH code here //
			   if(@$this->input->post('is_graph')){		
					echo json_encode($data_graph);
					return true;
					exit;								
				}
			
				$table .='</table>';
				
				$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins
				
				$mpdf->WriteHTML($table);

				$filename = "complaint_pdf/performance-level3-report-on-".date("Y-m-d-H-i-s").".pdf";
				$data['pdf_file_name'] = $filename;
				//$mpdf->Output();
				$mpdf->Output($filename, 'D');
		
		}
		
		
		////////////////////////// End Functionality ///////////////////////
		
		$data['middlecontent']='reports/performance_level3';
		$data['master_mod'] = 'Reports';
		$data['function']='PerformanceLevel_3';
	    $this->load->view('admin/admin_combo',$data);
		
	}
	
	public function performance_ranking_3(){
			
		error_reporting(0);
		$c_district 	= @$this->input->post('district_id')?$this->input->post('district_id'):'';
		$c_region_id	= @$this->input->post('region_id')?$this->input->post('region_id'):'';
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');		
		$admin_id 		= $this->session->userdata('supadminid');
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
			
		$impArr = implode(",", $this->adminArr);
		//$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(999,3,10,11,12,13,14) AND status = '1'";
		$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(".$impArr.") AND status = '1'";
		
		
		if($c_district){
			
			$condition .= " AND district_id = '".$c_district."'";
		}
		
		if($c_region_id){
			
			$condition .= " AND region_id = '".$c_region_id."'";
		}	 	
		
		$dateContion = "";
		
		
		if($frm_date!="" && $to_date!=""){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
		}
		
		
		
		$result 	= $this->db->query($condition)->result();
		
		$rowCount = $this->getNumData($condition);
		
		$rowCnt = 0;
		$dataArr = array();
		
			
		if($rowCount > 0){
						
			foreach($result as $comData){				
				
			$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$comData->role_id, 'status' => '1'));
			$roleName   = $roleDetail[0]['role_name'];
			$roleID     = $roleDetail[0]['role_id'];
			
			// Region Name
			$regionDetail = $this->master_model->getRecords('gri_region_division',array('religion_id'=>$comData->region_id, 'status' => '1'));
			$regionName   = $regionDetail[0]['region_division_name'];
			
			// District Name 
			$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$comData->district_id));
			$district_name		= $districtDetail[0]['district_name'];
			
			// Office Sub Type Name
			$officeSubType_Name = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$comData->office_id, 'status' => '1'));
			$office_sub_name	= $officeSubType_Name[0]['complaint_sub_type_name'];
			
			// Office Name
			$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$comData->sro_id, 'status' => '1'));
			$office_name = $officeDetail[0]['office_name'];
			$designation = $district_name."<br />(".$roleName.")";	

			if($roleID == 5 || $roleID == 4){
				$regionName = 'MUMBAI REGION';
			}
			
			// Callback function to get Details
			$returnArr = $this->get_performance_result($roleID, $comData->id, $dateContion);
			
				$comData->percentage 	= $returnArr['percentage']; 	
				$comData->roleName 		= $roleName;
				$comData->totalAssign 	= $returnArr['totalAssign']; 
				$comData->totalResolved = $returnArr['totalResolved']; 
				$comData->regionName	= $regionName; 
				$comData->district_name	= $designation;
				$comData->cntPending	= $returnArr['totalPending']; 
				$comData->cntEscalation	= $returnArr['totalEscalation']; 
				$comData->officeSubTypesName= $office_sub_name;
				$comData->office_name	= $office_name;
			
				$sortedResults[] = $comData;
				
			}
			
			$price = array();
			foreach ($sortedResults as $key => $row)
			{
				$price[$key] = $row->percentage;
			}
			array_multisort($price, SORT_DESC, $sortedResults);
			
			
			$i = 1;			
			
			foreach($sortedResults as $post){
			  // do your html here e.g
			  $per = $post->percentage."%";
			  $dataArr[] = array(
					  $i,				  
					  $post->regionName,
					  $post->district_name,
					  $post->office_name,
					  $post->totalAssign,
					  $post->totalResolved,
					  $post->cntPending,
					  $post->cntEscalation,	
					  $per
				   );
				
			     $i++;
			    $rowCnt = $post->rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
				);
			}
		echo json_encode($response);
		
		}
	
	}
	
	// Performance Calculation Funtion
	public function get_performance_result($roleID, $id, $dateContion){
		
		$percentage = 0;
			
		// Not callcenter user
		/*if($roleID == 5){
			
			$whr = " AND c.compaint_added_by = '".$id."'";
			
			
		} else if($roleID == 4){ // Help Desk User
			
			$whr = " AND ca.assign_user_id = '".$id."' AND c.complaint_type_id = '1'";
			
		} else {
			
			$whr = " AND ca.assign_user_id = '".$id."'";
		}*/ 
		
		
		if($roleID == 5){
			
			$whr = " AND c.compaint_added_by = '".$id."'";
			$cntEscalation = 0;
			
		} else if($roleID == 4){ // Help Desk User
			
			$whr = " AND ca.assign_user_id = '".$id."' AND c.complaint_type_id = '1'";
			$cntEscalation = 0;
			
		} else if($roleID == 1 || $roleID == 7 || $roleID == 15){
			
			$whr = " AND ca.assign_user_id = '".$id."' AND c.is_level_1 = '1'";
			
			$cntEscalation = 0;
			
		} else if($roleID == 2 || $roleID == 9){
			
			$whr = " AND ca.assign_user_id = '".$id."' AND c.is_level_2 = '1'";
			
			// Complaint Escalated To JDR/ACS 			
			$getEscalationCnt = "SELECT DISTINCT(c.comp_code) FROM gri_complaint c 
								JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
								JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.reply_status='1' AND c.comp_code!='' ".$whr." ".$dateContion."";
			$cntEscalation = $this->db->query($getEscalationCnt)->num_rows();
			
			
			
		} 
		
		// Selected Date Complaint Register Count
		$getRegisterCnt = "SELECT DISTINCT(c.comp_code) FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.comp_code!='' ".$whr." ".$dateContion."";
		$cntRecord = $this->db->query($getRegisterCnt)->num_rows();
		
		
		// Resolved Count
		$getResolveCnt = "SELECT DISTINCT(c.comp_code) FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.reply_status='0' AND c.comp_code!='' AND c.compaint_closed_by = '".$id."' ".$dateContion." ".$whr." ";
		$cntResolve = $this->db->query($getResolveCnt)->num_rows();
		
		
		// Pending Complaint Count 
		$getPendingCnt = "SELECT DISTINCT(c.comp_code) FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.reply_status='1' AND c.comp_code!='' ".$whr." ".$dateContion."";
		$cntPending = $this->db->query($getPendingCnt)->num_rows();
		
		/*if($roleID == 2 || $roleID == 9){
			
			// Complaint Escalated To JDR/ACS 			
			$getEscalationCnt = "SELECT DISTINCT(c.comp_code) FROM gri_complaint c 
								JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
								JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.reply_status='1' AND c.is_level_2 = '1' AND c.comp_code!='' ".$whr." ".$dateContion."";
			$cntEscalation = $this->db->query($getEscalationCnt)->num_rows();
			
		} else {
			
			$cntEscalation = 0;
			
		}*/
					
		if($roleID != 5){
			$totalAssignCnt = $cntRecord;
		} else {
			$totalAssignCnt = 0;
		}
		
		
		// Calculate Percentage 
		$percentage = ($cntResolve/$cntRecord)*100;
		if (is_nan($percentage) || is_infinite($percentage)) {
			$percentage = "0";
		} else {
			$percentage = round($percentage, 2);
		}
		
		$returnArr = array( 'percentage' 	  => $percentage, 
							'totalAssign' 	  => $totalAssignCnt,
							'totalEscalation' => $cntEscalation,
							'totalResolved'   => $cntResolve,
							'totalPending'    => $cntPending);
		
		return  $returnArr;
		
	}
	
	// IGRA/IGR/DIG HQ/IT Complaint Overview Report For Level 2	
	public function complaint_overview_level_2(){
		
		//////////////////////  Download xls Functionality ////////////////////		
		if(isset($_POST['btn_export']) && !empty($_POST['btn_export'])) 
		{			
			
			error_reporting(E_ERROR | E_PARSE);
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$sel_type		= @$this->input->post('role_type')?$this->input->post('role_type'):'';
			
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');		
			$admin_id 		= $this->session->userdata('supadminid');
				
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];
			
			if($sel_type){
				
				$offType .= " role_id = '".$sel_type."'";
				$con_join = " role_id = '".$sel_type."'";
				
			} else {
				$impArr = implode(",", $this->allArr);
				$con_join = " role_id NOT IN(".$impArr.") ";
				
			}	
			
			$condition = "SELECT * FROM adminlogin WHERE ".$con_join." AND status = '1'";
			
			
			$dateContion = "";
			
			if($frm_date!="" && $to_date!=""){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
			}
			
			
			//echo $condition;die();
				
			$query = $condition;
			
			$result 	= $this->db->query($query)->result();
		
			
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");

			 $objPHPExcel = new PHPExcel();

			 $objPHPExcel->getProperties()->setCreator("");
			 $objPHPExcel->getProperties()->setLastModifiedBy("");
			 $objPHPExcel->getProperties()->setTitle("");
			 $objPHPExcel->getProperties()->setSubject("");
			 $objPHPExcel->getProperties()->setDescription("");

			 $objPHPExcel->setActiveSheetIndex(0);

			 $sheet = $objPHPExcel->getActiveSheet();		 
			

			 $sheet->setCellValue("A1","Sr.No.");
			 $sheet->setCellValue("B1","JDR/ACS");
			 $sheet->setCellValue("C1","Total Complaint Received");
			 $sheet->setCellValue("D1","Total Complaint Resolved");
			 $sheet->setCellValue("E1","Pending For 7 Days Or More");				
			 $sheet->setCellValue("F1","Pending For 4-7 Days");
			 $sheet->setCellValue("G1","Pending For 3 Days or Less");	

			 $row = 2;
			
			
			foreach($result as $key => $value){
				
				// Role Name	
				$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$value->role_id, 'status' => '1'));
				$roleName   = $roleDetail[0]['role_name'];
				
				// Office Name
				$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$value->sro_id, 'status' => '1'));
				$officeName   = $officeDetail[0]['office_name'];
				
				// District Name 
				$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$value->district_id));
				$district_name		= $districtDetail[0]['district_name'];			
				
				//$designationName = $district_name .' - '. $roleName."==".$comData->id;
				$designationName = $roleName .' - '. $district_name;	

				if($value->region_id == 4){
				
					if($value->role_id == 9){
						
						$c_sub_type_id = "c.complaint_sub_type_id IN('2')";				
						$get_register = $this->reports_mumbai_level_2($c_sub_type_id, $value->district_id, $value->id, $dateContion, $offType);
						
						$cntRecord 		= $get_register['register_cnt'];
						$cntResolve 	= $get_register['resolve_cnt'];
						$cntPending_5 	= $get_register['pending_cnt'];
						
					}
					
					if($value->role_id == 2){
						
						$c_sub_type_id = "c.complaint_sub_type_id IN('6','7')";	
								
						$get_register = $this->reports_mumbai_level_2($c_sub_type_id, $value->district_id, $value->id, $dateContion, $offType);
						
						$cntRecord 		= $get_register['register_cnt'];
						$cntResolve 	= $get_register['resolve_cnt'];
						$cntPending_5 	= $get_register['pending_cnt'];
						
					}
					
				} else {
					
					$get_register = $this->reports_other_level_2($value->district_id, $value->id, $dateContion, $offType);
						
					$cntRecord 		= $get_register['register_cnt'];
					$cntResolve 	= $get_register['resolve_cnt'];
					$cntPending_5 	= $get_register['pending_cnt'];
					
				}
				
				$r=0;
				$d=0;
				$s=0;
							
				//echo "<pre>";print_r($cntPending_5);die();
				
				foreach($cntPending_5 as $pend5){
					
					$currentDate = date('Y-m-d');
					
					$assign_date = $pend5->assign_date;
					
					$newDate = date("Y-m-d", strtotime($assign_date));
					
					$dateDiff = $this->dateDiffInDays($assign_date, $currentDate);		
					
					if($dateDiff >= 7){
						$r++;					
					} 
					else if($dateDiff > 4 && $dateDiff <= 7){
						$d++;
					} 
					else if($dateDiff <= 3){					
						$s++;
					} 			
					
				} 					
			  
					$no++;
					
					
					
				 $sheet->setCellValue("A".$row,$no);
				 $sheet->setCellValue("B".$row,$designationName);
				 $sheet->setCellValue("C".$row,$cntRecord);
				 $sheet->setCellValue("D".$row,$cntResolve);
				 $sheet->setCellValue("E".$row,$r);
				 $sheet->setCellValue("F".$row,$d);
				 $sheet->setCellValue("G".$row,$s);			 
			  
				
			    $row++;
							
			} // First For Close			
			
			
			// $filename = "complaint-overview-report-level-2-on-".date("Y-m-d-H-i-s").".xls";
			$sheet->setTitle("Export File");
			$filename = "complaint_excel/complaint-overview-report-on-".date("Y-m-d-H-i-s").".xlsx";	
			/*header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");;
			header("Content-Disposition: attachment;filename=$filename");
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);*/

			// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
			//  //force user to download the Excel file without writing it to server's HD
			// $objWriter->save('php://output');
			// set_time_limit(30);
			// exit;
			
			
			// create file name			
			header("Content-Type: application/vnd.ms-excel");	
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			header('Content-Disposition: attachment;filename="complaint-overview-report-on-'.time().'.xlsx"');
			header('Cache-Control: max-age=0');			
			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			ob_end_clean();
			$objWriter->save('php://output');
			//$objWriter->save($fileName);
			exit; 
						
			$data['excel_file_name'] = $filename;
			$objWriter->save($filename);
		
		}
		
		///////// Download PDF ////////////
		
		if( (isset($_POST['btn_pdf']) && !empty($_POST['btn_pdf'])) ||  !empty($_POST['is_graph']) )  
		{
			
			error_reporting(E_ERROR | E_PARSE);		
			include(APPPATH."third_party/mpdf/mpdf.php");
			//$mpdf=new mPDF('','A4');
			
			$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
			$mpdf->mirrorMargins = 1;
			
			// Set PDF Header Footer HTML
			$pdfHeader = $this->setPDF();		
					
			$mpdf->SetHTMLHeader($pdfHeader['header_1']);
			$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
			$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
			$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');
			
			$mpdf->simpleTables = true;
			$mpdf->packTableData = true;
			$keep_table_proportions = TRUE;
			$mpdf->shrink_tables_to_fit=1;
			
				
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$sel_type		= @$this->input->post('role_type')?$this->input->post('role_type'):'';
			
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');		
			$admin_id 		= $this->session->userdata('supadminid');
				
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];
			
			if($sel_type){
				
				$offType .= " role_id = '".$sel_type."'";
				$con_join = " role_id = '".$sel_type."'";
				
			} else {
				$impArr = implode(",", $this->allArr);
				$con_join = " role_id NOT IN(".$impArr.") ";
				
			}	
			
			$condition = "SELECT * FROM adminlogin WHERE ".$con_join." AND status = '1'";
			
				
			
			$dateContion = "";
			
			if($frm_date!="" && $to_date!=""){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
			}
			
			
			//echo $condition;die();
				
			$query = $condition;
			
			$result 	= $this->db->query($query)->result();
				

			$i = 1;
						
			$table = '<h3>Complaint Overview Report</h3>
						<table border="1" style="width:100%;">						
							<tr> 
								 <th width="10%">Sr.No. </th>	
								 <th width="10%">JDR/ACS </th>
								 <th width="10%">Total Complaint Received</th>
								 <th width="20%">Total Complaint Resolved</th>
								 <th width="15%">Pending for 7 days or more</th>
								 <th width="15%">Pending for 4-7 days</th>
								 <th width="15%">Pending for 3 days or less</th>
							</tr>';
		
			$data_graph[] = array('Designation Name', 'Pending From 7 Days Or More', 'Pending From 4-7 Days', 'Pending From 3 Days Or Less');
		
			foreach($result as $key => $value){
					
				// Role Name	
				$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$value->role_id, 'status' => '1'));
				$roleName   = $roleDetail[0]['role_name'];
				
				// Office Name
				$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$value->sro_id, 'status' => '1'));
				$officeName   = $officeDetail[0]['office_name'];
				
				// District Name 
				$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$value->district_id));
				$district_name		= $districtDetail[0]['district_name'];			
				
				//$designationName = $district_name .' - '. $roleName."==".$value->id;
				$designationName = $roleName .' - '. $district_name;

				if($value->region_id == 4){
				
					if($value->role_id == 9){
						
						$c_sub_type_id = "c.complaint_sub_type_id IN('2')";				
						$get_register = $this->reports_mumbai_level_2($c_sub_type_id, $value->district_id, $value->id, $dateContion, $offType);
						
						$cntRecord 		= $get_register['register_cnt'];
						$cntResolve 	= $get_register['resolve_cnt'];
						$cntPending_5 	= $get_register['pending_cnt'];
						
					}
					
					if($value->role_id == 2){
						
						$c_sub_type_id = "c.complaint_sub_type_id IN('6','7')";	
								
						$get_register = $this->reports_mumbai_level_2($c_sub_type_id, $value->district_id, $value->id, $dateContion, $offType);
						
						$cntRecord 		= $get_register['register_cnt'];
						$cntResolve 	= $get_register['resolve_cnt'];
						$cntPending_5 	= $get_register['pending_cnt'];
						
					}
					
				} else {
					
					$get_register = $this->reports_other_level_2($value->district_id, $value->id, $dateContion, $offType);
						
					$cntRecord 		= $get_register['register_cnt'];
					$cntResolve 	= $get_register['resolve_cnt'];
					$cntPending_5 	= $get_register['pending_cnt'];
					
				}	
				
				
				$r=0;
				$d=0;
				$s=0;
				
				foreach($cntPending_5 as $pend5){
					
					$currentDate = date('Y-m-d');
					
					$assign_date = $pend5->assign_date;
					
					$newDate = date("Y-m-d", strtotime($assign_date));
					
					$dateDiff = $this->dateDiffInDays($assign_date, $currentDate);		
					
					if($dateDiff > 7){
						$r++;					
					} 
					else if($dateDiff >= 4 && $dateDiff <= 7){
						$d++;
					} 
					else if($dateDiff <= 3){					
						$s++;
					} 			
					
				} 					
			  
					$no++;	

					$data_graph[] = array($designationName, $r,$d,$s);
					 
					 $table .='<tr>	
								<td align="center">'.$no.'</td>
								<td>'.$designationName.'</td>
								<td align="center">'.$cntRecord.'</td>
								<td align="center">'.$cntResolve.'</td>
								<td align="center">'.$r.'</td>
								<td align="center">'.$d.'</td>
								<td align="center">'.$s.'</td>
							</tr>';
							
					$i++;
				}
				
				// GRAPH code here //
				if(@$this->input->post('is_graph')){		
					echo json_encode($data_graph);
					return true;
					exit;								
				}
			
				$table .='</table>';
				
				$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins
				
				$mpdf->WriteHTML($table);

				$filename = "complaint_pdf/complaint-overview-report-level-2-on-".date("Y-m-d-H-i-s").".pdf";
				//$mpdf->Output();
				$data['pdf_file_name'] = $filename;
				$mpdf->Output($filename, 'D');
		
		}
		
		
		////////////////////////// End Functionality ///////////////////////
		
		$data['middlecontent']='reports/complaint_overview_level_3';
		$data['master_mod'] = 'Reports';
		$data['function']='Complaint_overview_level_3';
	    $this->load->view('admin/admin_combo',$data);
		
	}
	
	// Report section for IGR/DIG HQ/ DIG IT Level 2
	public function complaint_pending_level_2(){	
		
		error_reporting(0);
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$sel_type		= @$this->input->post('role_type')?$this->input->post('role_type'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');		
		$admin_id 		= $this->session->userdata('supadminid');
			
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
		
		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value
		$pageNo = ($_POST['start']/$_POST['length'])+1;	

		
		if($sel_type){
			
			$offType .= " role_id = '".$sel_type."'";
			$con_join = " role_id = '".$sel_type."'";
			
		} else {
			$impArr = implode(",", $this->allArr);
			$con_join = " role_id NOT IN(".$impArr.") ";
			
		}	
		
		$condition = "SELECT * FROM adminlogin WHERE ".$con_join." AND status = '1'";
		
		 	
		
		$dateContion = "";
		
		if($frm_date!="" && $to_date!=""){
			
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
		}
		
		
		//echo $condition;die();
			
		$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
		
		$result 	= $this->db->query($query)->result();		
		
		$rowCount = $this->getNumData($condition);
		
		$rowCnt = 0;
		$dataArr = array();
		
		if($rowCount > 0){
			
			$no    = $_POST['start'];
			
			foreach($result as $comData){
				
			// Role Name	
			$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$comData->role_id, 'status' => '1'));
			$roleName   = $roleDetail[0]['role_name'];
			
			// Office Name
			$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$comData->sro_id, 'status' => '1'));
			$officeName   = $officeDetail[0]['office_name'];
			
			// District Name 
			$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$comData->district_id));
			$district_name		= $districtDetail[0]['district_name'];			
			
			//$designationName = $district_name .' - '. $roleName."==".$comData->id;
			$designationName = $roleName .' - '. $district_name;

			if($comData->region_id == 4){
				
				if($comData->role_id == 9){
					
					$c_sub_type_id = "c.complaint_sub_type_id IN('2')";				
					$get_register = $this->reports_mumbai_level_2($c_sub_type_id, $comData->district_id, $comData->id, $dateContion, $offType);
					
					$cntRecord 		= $get_register['register_cnt'];
					$cntResolve 	= $get_register['resolve_cnt'];
					$cntPending_5 	= $get_register['pending_cnt'];
					
				}
				
				if($comData->role_id == 2){
					
					$c_sub_type_id = "c.complaint_sub_type_id IN('6','7')";	
							
					$get_register = $this->reports_mumbai_level_2($c_sub_type_id, $comData->district_id, $comData->id, $dateContion, $offType);
					
					$cntRecord 		= $get_register['register_cnt'];
					$cntResolve 	= $get_register['resolve_cnt'];
					$cntPending_5 	= $get_register['pending_cnt'];
					
				}
			} else {
				
				$get_register = $this->reports_other_level_2($comData->district_id, $comData->id, $dateContion, $offType);
					
				$cntRecord 		= $get_register['register_cnt'];
				$cntResolve 	= $get_register['resolve_cnt'];
				$cntPending_5 	= $get_register['pending_cnt'];
				
			}
			
			$r=0;
			$d=0;
			$s=0;
						
			
				foreach($cntPending_5 as $pend5){
					
					$currentDate = date('Y-m-d');
					
					$assign_date = $pend5->assign_date;
					
					$newDate = date("Y-m-d", strtotime($assign_date));
					
					$dateDiff = $this->dateDiffInDays($assign_date, $currentDate);		
					
					if($dateDiff > 7){
						$r++;					
					} 
					else if($dateDiff >= 4 && $dateDiff <= 7){
						$d++;
					} 
					else if($dateDiff <= 3){					
						$s++;
					} 			
					
				} 
			
				
				$no++;				
				
				$dataArr[] = array(
					  $no,				  
					  $designationName,
					  $cntRecord,
					  $cntResolve,
					  $r,
					  $d,						  
					  $s
				   );
				
			   
			    $rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
				);
				
				
			} // Foreach Close  
			
		} else {
			
			$rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => 0,
				  "recordsFiltered" => 0,
				  "data" => $dataArr
				);
		}
		
		echo json_encode($response);
		
	}
	
	
	public function reports_mumbai_level_2($c_sub_type_id, $district_id, $id, $date_condition, $off_type){
		
		/************** JDR/ACS ***************/
		$getRegisterCnt = "SELECT c.comp_code FROM gri_complaint c 
								JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
								JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.comp_code!='' AND ".$c_sub_type_id." AND c.district_id = '".$district_id."' AND ca.status = '1' AND c.is_level_2 = '1' AND ca.assign_user_id = '".$id."' ".$date_condition." ";
		$cntRecord = $this->db->query($getRegisterCnt)->num_rows();
		
		/*$getResolveCnt = "SELECT c.comp_code FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.reply_status='0' AND ".$c_sub_type_id." AND c.district_id = '".$district_id."' AND ca.status = '1' AND c.comp_code!='' AND c.is_level_2 = '1' AND c.compaint_closed_by = '".$id."' ".$date_condition." ";*/
		
		$getResolveCnt = "SELECT c.comp_code FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.reply_status='0' AND c.comp_code!='' AND ".$c_sub_type_id." AND c.district_id = '".$district_id."' AND c.is_level_2 = '1' AND c.compaint_closed_by = '".$id."' ".$date_condition." ";
		$cntResolve = $this->db->query($getResolveCnt)->num_rows();
		
					
		$pending_days_5 = "SELECT c.comp_code, ca.assign_date FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id 
							WHERE c.reply_status='1' AND c.comp_code!='' AND ".$c_sub_type_id." AND ca.status = '1' AND c.district_id = '".$district_id."' AND ca.assign_user_id = '".$id."' ".$date_condition." ";
		$cntPending_5 = $this->db->query($pending_days_5)->result();
		
		$returnArr = array('register_cnt' => $cntRecord, 'resolve_cnt' => $cntResolve, 'pending_cnt' => $cntPending_5);
		
		return $returnArr;
		
	}
	
	public function reports_other_level_2($district_id, $id, $date_condition, $off_type){
		
		/************** JDR/ACS ***************/
		$getRegisterCnt = "SELECT c.comp_code FROM gri_complaint c 
								JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
								JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.comp_code!='' AND c.district_id = '".$district_id."' AND ca.status = '1' AND ca.assign_user_id = '".$id."' ".$date_condition." ";
		$cntRecord = $this->db->query($getRegisterCnt)->num_rows();
		
		$getResolveCnt = "SELECT c.comp_code FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.reply_status='0' AND c.district_id = '".$district_id."' AND ca.status = '1' AND c.comp_code!='' AND c.compaint_closed_by = '".$id."' ".$date_condition." ";
		$cntResolve = $this->db->query($getResolveCnt)->num_rows();
		
					
		$pending_days_5 = "SELECT c.comp_code, ca.assign_date FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id 
							WHERE c.reply_status='1' AND c.comp_code!='' AND ca.status = '1' AND c.district_id = '".$district_id."' AND ca.assign_user_id = '".$id."' ".$date_condition." ";
		$cntPending_5 = $this->db->query($pending_days_5)->result();
		
		$returnArr = array('register_cnt' => $cntRecord, 'resolve_cnt' => $cntResolve, 'pending_cnt' => $cntPending_5);
		
		return $returnArr;
		
	}
	
	// IGRA/IGR/DIG HQ/IT Complaint Overview Report For Level 1 i.e. SRO/MO/CSO
	public function complaint_overview_level_1(){
		
		$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE status = '1'";
		$data['officeList'] = $this->db->query($query_res)->result();
		
		// Download Excel //
		
		if(isset($_POST['btn_export']) && !empty($_POST['btn_export'])) 
		{
			
			error_reporting(E_ERROR | E_PARSE);		
					
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
			
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');		
			$admin_id 		= $this->session->userdata('supadminid');
				
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];

			$offType = "";
			$off_id = "";
			if($office_subtype){
				
				$offType .= " AND c.sro_office_id = '".$office_subtype."'";
				$off_id  .= " AND sro_id = '".$office_subtype."'";
				
			}	
			$impArr = implode(",", $this->firstLevel);
			$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(".$impArr.") AND status='1' ".$off_id."";
						
			$dateContion = "";
			
			if($frm_date!="" && $to_date!=""){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
			}
				
			$query = $condition;
			
			$result 	= $this->db->query($query)->result();
				

			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");

			 $objPHPExcel = new PHPExcel();

			 $objPHPExcel->getProperties()->setCreator("");
			 $objPHPExcel->getProperties()->setLastModifiedBy("");
			 $objPHPExcel->getProperties()->setTitle("");
			 $objPHPExcel->getProperties()->setSubject("");
			 $objPHPExcel->getProperties()->setDescription("");

			 $objPHPExcel->setActiveSheetIndex(0);

			 $sheet = $objPHPExcel->getActiveSheet();		 
			

			 $sheet->setCellValue("A1","Sr.No.");
			 $sheet->setCellValue("B1","SRO/COS");
			 $sheet->setCellValue("C1","Office Subtype");
			 $sheet->setCellValue("D1","Total Complaint Received");
			 $sheet->setCellValue("E1","Total Complaint Resolved");
			 $sheet->setCellValue("F1","Pending For 7 Days Or More");				
			 $sheet->setCellValue("G1","Pending For 4-7 Days");
			 $sheet->setCellValue("H1","Pending For 3 Days or Less");	

			 $row = 2;
		
			foreach($result as $key => $value){
					
					// Role Name	
					$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$value->role_id, 'status' => '1'));
					$roleName   = $roleDetail[0]['role_name'];
					
					// Office Name
					$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$value->sro_id, 'status' => '1'));
					$officeName   = $officeDetail[0]['office_name'];
					
					// District Name 
					$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$value->district_id));
					$district_name		= $districtDetail[0]['district_name'];			
					
					//$designationName = $district_name .' - '. $roleName."==".$value->id;
					$designationName = $roleName .' - '. $district_name;

					if($value->region_id == 4 && $value->role_id == 7){
				
						$c_sub_type_id = '2';				
						$get_register = $this->reports_mumbai_level_1($c_sub_type_id, $value->district_id, $value->id, $dateContion, $offType);
						
						$cntRecord 		= $get_register['register_cnt'];
						$cntResolve 	= $get_register['resolve_cnt'];
						$cntPending_5 	= $get_register['pending_cnt'];
						
					}
					
					if($value->region_id == 4 && $value->role_id == 1){	

						$c_sub_type_id = '7';				
						$get_register = $this->reports_mumbai_level_1($c_sub_type_id, $value->district_id, $value->id, $dateContion, $offType);
						
						$cntRecord 		= $get_register['register_cnt'];
						$cntResolve 	= $get_register['resolve_cnt'];
						$cntPending_5 	= $get_register['pending_cnt'];
						
					}
					
					if($value->role_id == 15){

						$c_sub_type_id = '6';				
						$get_register = $this->reports_mumbai_level_1($c_sub_type_id, $value->district_id, $value->id, $dateContion, $offType);
						
						$cntRecord 		= $get_register['register_cnt'];
						$cntResolve 	= $get_register['resolve_cnt'];
						$cntPending_5 	= $get_register['pending_cnt'];
						
					}					
					
					$r=0;
					$d=0;
					$s=0;
								
					
						foreach($cntPending_5 as $pend5){
							
							$currentDate = date('Y-m-d');
							
							$assign_date = $pend5->assign_date;
							
							$newDate = date("Y-m-d", strtotime($assign_date));
							
							$dateDiff = $this->dateDiffInDays($assign_date, $currentDate);		
							
							if($dateDiff >= 7){
								$r++;					
							} 
							else if($dateDiff > 4 && $dateDiff <= 7){
								$d++;
							} 
							else if($dateDiff <= 3){					
								$s++;
							} 			
							
						} 
					
						
					$no++;							
					
					$sheet->setCellValue("A".$row,$no);
					$sheet->setCellValue("B".$row,$designationName);
					$sheet->setCellValue("C".$row,$officeName);
					$sheet->setCellValue("D".$row,$cntRecord);
					$sheet->setCellValue("E".$row,$cntResolve);
					$sheet->setCellValue("F".$row,$r);
					$sheet->setCellValue("G".$row,$d);
					$sheet->setCellValue("H".$row,$s);
							
					$row++;
				}
			
				$filename = "complaint_excel/complaint-overview-level-1-report-on-".date("Y-m-d-H-i-s").".xlsx";
				$sheet->setTitle("Export File");
					
				/*header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");;
				header("Content-Disposition: attachment;filename=$filename");
				$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);*/
				// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
				//  //force user to download the Excel file without writing it to server's HD
				// $objWriter->save('php://output');
				// set_time_limit(300);
				// exit;
				
				// create file name			
				header("Content-Type: application/vnd.ms-excel");	
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
				header('Content-Disposition: attachment;filename="complaint-overview-level-1-report-on-'.time().'.xlsx"');
				header('Cache-Control: max-age=0');			
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				ob_end_clean();
				$objWriter->save('php://output');
				//$objWriter->save($fileName);
				exit; 
				
				$data['excel_file_name'] = $filename;
				$objWriter->save($filename);
		
		}
		
		///////// Download PDF ////////////
		
		if( (isset($_POST['btn_pdf']) && !empty($_POST['btn_pdf'])) ||  !empty($_POST['is_graph']) )  
		{
			
			error_reporting(E_ERROR | E_PARSE);		
			include(APPPATH."third_party/mpdf/mpdf.php");
			//$mpdf=new mPDF('','A4');
			
			$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
			$mpdf->mirrorMargins = 1;
			
			// Set PDF Header Footer HTML
			$pdfHeader = $this->setPDF();		
					
			$mpdf->SetHTMLHeader($pdfHeader['header_1']);
			$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
			$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
			$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');
			
			$mpdf->simpleTables = true;
			$mpdf->packTableData = true;
			$keep_table_proportions = TRUE;
			$mpdf->shrink_tables_to_fit=1;
			
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
			
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');		
			$admin_id 		= $this->session->userdata('supadminid');
				
			
			// Get Login User Details
			$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
			$getAdminID = $getLogin[0]['id'];
			$getAdminR  = $getLogin[0]['region_id'];
			$getAdminD  = $getLogin[0]['district_id'];
			$getAdminS  = $getLogin[0]['sro_id'];
			$getAdminO  = $getLogin[0]['office_id'];

			$offType = "";
			$off_id = "";
			if($office_subtype){
				
				$offType .= " AND c.sro_office_id = '".$office_subtype."'";
				$off_id  .= " AND sro_id = '".$office_subtype."'";
				
			}	
			$impArr = implode(",", $this->firstLevel);
			$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(".$impArr.") AND status='1' ".$off_id."";
			
				
			
			$dateContion = "";
			
			if($frm_date!="" && $to_date!=""){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
			}
				
			$query = $condition;
			
			$result 	= $this->db->query($query)->result();
				

			$i = 1;
						
			$table = '<h3>Complaint Overview Report Level 1</h3>
						<table border="1" style="width:100%;">						
							<tr> 
								 <th width="5%">Sr.No. </th>	
								 <th width="10%">SRO/COS </th>
								 <th width="10%">Office Subtype </th>
								 <th width="10%">Total Complaint Received</th>
								 <th width="20%">Total Complaint Resolved</th>
								 <th width="15%">Pending for 5 days or more</th>
								 <th width="15%">Pending for 3-4 days</th>
								 <th width="15%">Pending for 2 days or less</th>
							</tr>';
		
			$data_graph[] = array('Designation Name', 'Pending From 5 Days Or More', 'Pending From 3-4 Days', 'Pending From 2 Days Or Less');
		
			foreach($result as $key => $value){
					
					// Role Name	
					$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$value->role_id, 'status' => '1'));
					$roleName   = $roleDetail[0]['role_name'];
					
					// Office Name
					$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$value->sro_id, 'status' => '1'));
					$officeName   = $officeDetail[0]['office_name'];
					
					// District Name 
					$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$value->district_id));
					$district_name		= $districtDetail[0]['district_name'];			
					
					//$designationName = $district_name .' - '. $roleName."==".$value->id;
					$designationName = $roleName .' - '. $district_name;

					if($value->region_id == 4 && $value->role_id == 7){
				
						$c_sub_type_id = '2';				
						$get_register = $this->reports_mumbai_level_1($c_sub_type_id, $value->district_id, $value->id, $dateContion, $offType);
						
						$cntRecord 		= $get_register['register_cnt'];
						$cntResolve 	= $get_register['resolve_cnt'];
						$cntPending_5 	= $get_register['pending_cnt'];
						
					}
					
					if($value->region_id == 4 && $value->role_id == 1){	

						$c_sub_type_id = '7';				
						$get_register = $this->reports_mumbai_level_1($c_sub_type_id, $value->district_id, $value->id, $dateContion, $offType);
						
						$cntRecord 		= $get_register['register_cnt'];
						$cntResolve 	= $get_register['resolve_cnt'];
						$cntPending_5 	= $get_register['pending_cnt'];
						
					}
					
					if($value->role_id == 15){

						$c_sub_type_id = '6';				
						$get_register = $this->reports_mumbai_level_1($c_sub_type_id, $value->district_id, $value->id, $dateContion, $offType);
						
						$cntRecord 		= $get_register['register_cnt'];
						$cntResolve 	= $get_register['resolve_cnt'];
						$cntPending_5 	= $get_register['pending_cnt'];
						
					}					
					
					$r=0;
					$d=0;
					$s=0;
								
					
						foreach($cntPending_5 as $pend5){
							
							$currentDate = date('Y-m-d');
							
							$assign_date = $pend5->assign_date;
							
							$newDate = date("Y-m-d", strtotime($assign_date));
							
							$dateDiff = $this->dateDiffInDays($assign_date, $currentDate);		
							
							if($dateDiff >= 7){
								$r++;					
							} 
							else if($dateDiff > 4 && $dateDiff <= 7){
								$d++;
							} 
							else if($dateDiff <= 3){					
								$s++;
							} 			
							
						} 
					
						
						$no++;			
					 
					 $table .='<tr>	
								<td align="center">'.$no.'</td>
								<td>'.$designationName.'</td>
								<td>'.$officeName.'</td>
								<td align="center">'.$cntRecord.'</td>
								<td align="center">'.$cntResolve.'</td>
								<td align="center">'.$r.'</td>
								<td align="center">'.$d.'</td>
								<td align="center">'.$s.'</td>
							</tr>';
							
					$data_graph[] = array($officeName, $r,$d,$s);		
							
					$i++;
				}
				
				// GRAPH code here //
				if(@$this->input->post('is_graph')){		
					echo json_encode($data_graph);
					return true;
					exit;								
				}
			
				$table .='</table>';
				
				$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins
				
				$mpdf->WriteHTML($table);

				$filename = "complaint_pdf/complaint-overview-level-1-report-on-".date("Y-m-d-H-i-s").".pdf";
				//$mpdf->Output();
				$data['pdf_file_name'] = $filename;
				$mpdf->Output($filename, 'D');
				
		}
		
		$data['middlecontent']='reports/complaint_overview_level_1';
		$data['master_mod'] = 'Reports';
		$data['function']='Complaint_overview_level_1';
	    $this->load->view('admin/admin_combo',$data);
		
	}
	
	public function complaint_reports_level_1(){
		
		error_reporting(0);
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');		
		$admin_id 		= $this->session->userdata('supadminid');
			
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
		
		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value
		$pageNo = ($_POST['start']/$_POST['length'])+1;	

		$offType = "";
		$off_id = "";
		if($office_subtype){
			
			$offType .= " AND c.sro_office_id = '".$office_subtype."'";
			$off_id  .= " AND sro_id = '".$office_subtype."'";
			
		}	
		
		$impArr = implode(",", $this->firstLevel);
		$condition = "SELECT * FROM adminlogin WHERE role_id NOT IN(".$impArr.") AND status='1' ".$off_id."";
		
		 	
		
		$dateContion = "";
			
		if($frm_date!="" && $to_date!=""){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
		}
		
		
		//echo $condition;die();
			
		$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
		
		$result 	= $this->db->query($query)->result();		
		
		$rowCount = $this->getNumData($condition);
		
		$rowCnt = 0;
		$dataArr = array();
		if($rowCount > 0){
			
			$no    = $_POST['start'];
			
			foreach($result as $comData){
				
			// Role Name	
			$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$comData->role_id, 'status' => '1'));
			$roleName   = $roleDetail[0]['role_name'];
			
			// Office Name
			$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$comData->sro_id, 'status' => '1'));
			$officeName   = $officeDetail[0]['office_name'];
			
			// District Name 
			$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$comData->district_id));
			$district_name		= $districtDetail[0]['district_name'];			
			
			//$designationName = $district_name .' - '. $roleName."==".$comData->id;
			$designationName = $roleName .' - '. $district_name;	

			if($comData->region_id == 4 && $comData->role_id == 7){
				
				$c_sub_type_id = '2';				
				$get_register = $this->reports_mumbai_level_1($c_sub_type_id, $comData->district_id, $comData->id, $dateContion, $offType);
				
				$cntRecord 		= $get_register['register_cnt'];
				$cntResolve 	= $get_register['resolve_cnt'];
				$cntPending_5 	= $get_register['pending_cnt'];
				
			}
			
			if($comData->region_id == 4 && $comData->role_id == 1){	

				$c_sub_type_id = '7';				
				$get_register = $this->reports_mumbai_level_1($c_sub_type_id, $comData->district_id, $comData->id, $dateContion, $offType);
				
				$cntRecord 		= $get_register['register_cnt'];
				$cntResolve 	= $get_register['resolve_cnt'];
				$cntPending_5 	= $get_register['pending_cnt'];
				
			}
			
			if($comData->role_id == 15){

				$c_sub_type_id = '6';				
				$get_register = $this->reports_mumbai_level_1($c_sub_type_id, $comData->district_id, $comData->id, $dateContion, $offType);
				
				$cntRecord 		= $get_register['register_cnt'];
				$cntResolve 	= $get_register['resolve_cnt'];
				$cntPending_5 	= $get_register['pending_cnt'];
				
			}
			
			/*if($comData->region_id != 4){
				
				$office_id = $comData->sro_id;
				$get_register = $this->reports_other_level_1($comData->district_id, $comData->id, $dateContion, $offType);
				
				$cntRecord 		= $get_register['register_cnt'];
				$cntResolve 	= $get_register['resolve_cnt'];
				$cntPending_5 	= $get_register['pending_cnt'];
				
			}*/
			
			$r=0;
			$d=0;
			$s=0;						
			
				foreach($cntPending_5 as $pend5){
					
					$currentDate = date('Y-m-d');
					
					$assign_date = $pend5->assign_date;
					
					$newDate = date("Y-m-d", strtotime($assign_date));
					
					$dateDiff = $this->dateDiffInDays($assign_date, $currentDate);		
					
					if($dateDiff >= 7){
						$r++;					
					} 
					else if($dateDiff > 4 && $dateDiff <= 7){
						$d++;
					} 
					else if($dateDiff <= 3){					
						$s++;
					} 			
					
				} 
			
				
				$no++;				
				
				$dataArr[] = array(
					  $no,				  
					  $designationName,
					  $officeName,
					  $cntRecord,
					  $cntResolve,
					  $r,
					  $d,						  
					  $s
				   );
				
			   
			    $rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
				);
				
				
			} // Foreach Close  
			
		} else {
			
			$rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => 0,
				  "recordsFiltered" => 0,
				  "data" => $dataArr
				);
		}
		
		echo json_encode($response);
		
	}
	
	//SRO/CSO/MO complaint_overview_level_1 for Mumbai Region
	public function reports_mumbai_level_1($c_sub_type_id, $district_id, $id, $date_condition, $off_type){
		
			
		/************** SRO/CSO/MO ***************/
		 $getRegisterCnt = "SELECT c.comp_code FROM gri_complaint c 
								JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
								JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.comp_code!='' AND c.complaint_sub_type_id = '".$c_sub_type_id."' AND c.district_id = '".$district_id."' AND ca.status = '1' AND ca.assign_user_id = '".$id."' ".$date_condition." ".$off_type."";
		$cntRecord = $this->db->query($getRegisterCnt)->num_rows();
		
		$getResolveCnt = "SELECT c.comp_code FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.reply_status='0' AND c.complaint_sub_type_id = '".$c_sub_type_id."' AND c.district_id = '".$district_id."' AND ca.status = '1' AND c.comp_code!='' AND c.compaint_closed_by = '".$id."' ".$date_condition." ".$off_type."";
		$cntResolve = $this->db->query($getResolveCnt)->num_rows();
		
					
		$pending_days_5 = "SELECT c.comp_code, ca.assign_date FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id 
							WHERE c.reply_status='1' AND c.comp_code!='' AND c.complaint_sub_type_id = '".$c_sub_type_id."' AND ca.status = '1' AND c.district_id = '".$district_id."' AND ca.assign_user_id = '".$id."' ".$date_condition." ".$off_type."";
		$cntPending_5 = $this->db->query($pending_days_5)->result();
		
		$returnArr = array('register_cnt' => $cntRecord, 'resolve_cnt' => $cntResolve, 'pending_cnt' => $cntPending_5);
		
		return $returnArr;
		
	}
	
	public function reports_other_level_1($district_id, $id, $date_condition, $off_type){
		
		$getRegisterCnt = "SELECT c.comp_code FROM gri_complaint c 
								JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
								JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.comp_code!='' AND c.district_id = '".$district_id."' AND ca.assign_user_id = '".$id."' ".$date_condition." ".$off_type."";
		$cntRecord = $this->db->query($getRegisterCnt)->num_rows();
		
		$getResolveCnt = "SELECT c.comp_code FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id WHERE c.reply_status='0' AND c.district_id = '".$district_id."' AND c.comp_code!='' AND c.compaint_closed_by = '".$id."' ".$date_condition." ".$off_type."";
		$cntResolve = $this->db->query($getResolveCnt)->num_rows();
		
					
		$pending_days_5 = "SELECT c.comp_code, ca.assign_date FROM gri_complaint c 
							JOIN gri_complaint_assign ca ON c.comp_code = ca.comp_code 	  
							JOIN adminlogin a ON ca.assign_user_id = a.id 
							WHERE c.reply_status='1' AND c.comp_code!='' AND ca.status = '1' AND c.district_id = '".$district_id."' AND ca.assign_user_id = '".$id."' ".$date_condition." ".$off_type."";
		$cntPending_5 = $this->db->query($pending_days_5)->result();
		
		$returnArr = array('register_cnt' => $cntRecord, 'resolve_cnt' => $cntResolve, 'pending_cnt' => $cntPending_5);
		
		return $returnArr;
		
	}
	
	// call center hod report 
	public function cc_report(){
		
		$query_res = "SELECT sro_office_id, office_name FROM gri_sro_offices WHERE status = '1'";
		$data['officeList'] = $this->db->query($query_res)->result();
		
		// Download Excel //
		
		if(isset($_POST['btn_export']) && !empty($_POST['btn_export'])) 
		{
			
			error_reporting(E_ERROR | E_PARSE);		
					
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$c_status		= @$this->input->post('c_status')?$this->input->post('c_status'):'-1';
			$c_type			= @$this->input->post('c_type')?$this->input->post('c_type'):'';
			$complaintCreatorType		= @$this->input->post('user_type')?$this->input->post('user_type'):'';
			$c_subtype		= @$this->input->post('c_subtype')?$this->input->post('c_subtype'):'';
			
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');		
			$admin_id 		= $this->session->userdata('supadminid');
				
			
			
			
			
			$offType = "";
		$off_id = "";
		if($office_subtype){
			
			$offType .= " AND c.sro_office_id = '".$office_subtype."'";
			$off_id  .= " AND sro_id = '".$office_subtype."'";
			
		}

		$sta = "";
		if($c_status){	
			
			if($c_status == 4){
				
				$c_status = 0;
				$sta .= " AND c.reply_status = '".$c_status."'";
				
			} else if($c_status == -1){
				$sta .= " AND c.reply_status IN(0,1,2,3)";
			} else {
				
				$sta .= " AND c.reply_status = '".$c_status."'";
			}
			
		} 

		$subt = "";
		if($c_type)
		{
			$subt .= " AND c.complaint_type_id = '".$c_type."'";
		}
		
		$c_subt = "";
		if($c_subtype)
		{
			$c_subt .= " AND c.complaint_sub_type_id = '".$c_subtype."'";
		}
				
		
		$dateContion = "";
			
		if($frm_date!="" && $to_date!=""){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
		}
		
		$this->db->select('id');
		$getCallCenterUser = $this->master_model->getRecords('adminlogin',array('role_id'=>5, 'status' => '1'));
		
		$cc_user_id = array();
		if(count($getCallCenterUser) > 0)
		{
			foreach($getCallCenterUser as $cc_list)
			{
				array_push($cc_user_id,$cc_list['id']);
			}
		}
		
		$impArr = implode(",", $cc_user_id);
		
		
		$creatorType = '';
		if($complaintCreatorType)
		{
			if($complaintCreatorType == 1){
				$creatorType .= " AND c.compaint_added_by IN (".$impArr.")";
			}
			else
			{
				$creatorType .= " AND c.compaint_added_by = '0'";
			}
		}	
		
		$condition = "SELECT c.comp_code, c.c_id, ct.complaint_type_name, c.comp_subject, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, u.user_name, c.reply_status, c.complaint_date, c.compaint_added_by, c.compaint_closed_by FROM gri_complaint c		
					JOIN userregistration u ON c.user_id = u.user_id
					JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id
					WHERE c.comp_code!='' ".$subt." ".$creatorType." ".$c_subt." ".$offType." ".$dateContion." ".$sta."";
			
		$query = $condition.' ORDER BY  (CASE c.reply_status WHEN 1 THEN 0 WHEN 0 THEN 1 WHEN 2 THEN 2 WHEN 3 THEN 3 END) ASC, c.complaint_date ASC';
		
		$rowCount = $this->getNumData($condition);			
		$result = $this->db->query($query)->result();
				

			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
			require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");

			 $objPHPExcel = new PHPExcel();

			 $objPHPExcel->getProperties()->setCreator("");
			 $objPHPExcel->getProperties()->setLastModifiedBy("");
			 $objPHPExcel->getProperties()->setTitle("");
			 $objPHPExcel->getProperties()->setSubject("");
			 $objPHPExcel->getProperties()->setDescription("");

			 $objPHPExcel->setActiveSheetIndex(0);

			 $sheet = $objPHPExcel->getActiveSheet();		 
			

			 $sheet->setCellValue("A1","Sr.No.");
			 $sheet->setCellValue("B1","Complaint Code");
			 $sheet->setCellValue("C1","Complaint Date");
			 $sheet->setCellValue("D1","Created By");
			 $sheet->setCellValue("E1","User Type");
			 $sheet->setCellValue("F1","Complaint Type");				
			 $sheet->setCellValue("G1","Office Type");
			 $sheet->setCellValue("H1","Status");	

			 $row = 2;
			$i=1;
			foreach($result as $comData){
					
			// Complaint Added By Name	
			$callcentername = $this->master_model->getRecords('adminlogin',array('id'=>$comData->compaint_added_by, 'status' => '1'));
			$openfullname = $callcentername[0]['namecc'];	
			
			// Complaint Closed By Name	
			$callcentername_2 = $this->master_model->getRecords('adminlogin',array('id'=>$comData->compaint_closed_by, 'status' => '1'));
			$closefullname = $callcentername_2[0]['namecc'];	
				
			// Role Name	
			$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$comData->role_id, 'status' => '1'));
			$roleName   = $roleDetail[0]['role_name']; 
			
			// Office Name
			$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$comData->sro_id, 'status' => '1'));
			$officeName   = $officeDetail[0]['office_name'];
			
			// District Name 
			$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$comData->district_id));
			$district_name		= $districtDetail[0]['district_name'];			
			
			//$designationName = $district_name .' - '. $roleName."==".$comData->id;
			$designationName = $roleName .' - '. $district_name;
			
			
			// Complaint Office Type name
			if($comData->complaint_sub_type_id > 0) 
			{
				$subTypeName = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$comData->complaint_sub_type_id, 'status' => '1'));
				$complaint_sub_type_name   = $subTypeName[0]['complaint_sub_type_name'];
			}				
			
			
			if($comData->reply_status == 1){$status = 'Pending';} 
			else if($comData->reply_status == 2){$status = 'Fake';}
			else if($comData->reply_status == 0){$status = 'Closed';}
			else if($comData->reply_status == 3){$status = 'Query To Complainant';}
			
				
				$no++;				
				
				$comDate = date('d-m-Y', strtotime($comData->complaint_date));
				
				   
				   if($comData->compaint_added_by == 0)
				   {
						$user_t = 'Citizen';
						$full_n = $comData->user_name;
				   }
				   else 
				   {
					    $user_t = 'Call Center';
						$full_n = $openfullname;
				   }
						
					$no++;							
					
					$sheet->setCellValue("A".$row,$i);
					$sheet->setCellValue("B".$row,$comData->comp_code);
					$sheet->setCellValue("C".$row,$comDate);
					$sheet->setCellValue("D".$row,$full_n);
					$sheet->setCellValue("E".$row,$user_t);
					$sheet->setCellValue("F".$row,$comData->complaint_type_name);
					$sheet->setCellValue("G".$row,$complaint_sub_type_name);
					$sheet->setCellValue("H".$row,$status);
						
					$i++;						
					$row++;
				}
			
				$filename = "complaint_excel/call-center-report-on-".date("Y-m-d-H-i-s").".xlsx";
				$sheet->setTitle("Export File");
					
				
				// create file name			
				header("Content-Type: application/vnd.ms-excel");	
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
				header('Content-Disposition: attachment;filename="call-center-report-on-'.time().'.xlsx"');
				header('Cache-Control: max-age=0');			
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				ob_end_clean();
				$objWriter->save('php://output');
				//$objWriter->save($fileName);
				exit; 
				
				$data['excel_file_name'] = $filename;
				$objWriter->save($filename);
		
		}
		
		///////// Download PDF ////////////
		
		if( (isset($_POST['btn_pdf']) && !empty($_POST['btn_pdf'])) ||  !empty($_POST['is_graph']) )  
		{
			
			error_reporting(E_ERROR | E_PARSE);		
			include(APPPATH."third_party/mpdf/mpdf.php");
			//$mpdf=new mPDF('','A4');
			
			$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
			$mpdf->mirrorMargins = 1;
			
			// Set PDF Header Footer HTML
			$pdfHeader = $this->setPDF();		
					
			$mpdf->SetHTMLHeader($pdfHeader['header_1']);
			$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
			$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
			$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');
			
			$mpdf->simpleTables = true;
			$mpdf->packTableData = true;
			$keep_table_proportions = TRUE;
			$mpdf->shrink_tables_to_fit=1;
			
			$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
			$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
			$c_status		= @$this->input->post('c_status')?$this->input->post('c_status'):'-1';
			$c_type			= @$this->input->post('c_type')?$this->input->post('c_type'):'';
			$complaintCreatorType		= @$this->input->post('user_type')?$this->input->post('user_type'):'';
			$c_subtype		= @$this->input->post('c_subtype')?$this->input->post('c_subtype'):'';
			
			$type 			= $this->session->userdata('type');
			$sro_id 		= $this->session->userdata('sro_id');		
			$admin_id 		= $this->session->userdata('supadminid');
			
			$offType = "";
			$off_id = "";
			if($office_subtype){
				
				$offType .= " AND c.sro_office_id = '".$office_subtype."'";
				$off_id  .= " AND sro_id = '".$office_subtype."'";
				
			}

			$sta = "";
			if($c_status){	
				
				if($c_status == 4){
					
					$c_status = 0;
					$sta .= " AND c.reply_status = '".$c_status."'";
					
				} else if($c_status == -1){
					$sta .= " AND c.reply_status IN(0,1,2,3)";
				} else {
					
					$sta .= " AND c.reply_status = '".$c_status."'";
				}
				
			} 

			$subt = "";
			if($c_type)
			{
				$subt .= " AND c.complaint_type_id = '".$c_type."'";
			}
			
			$c_subt = "";
			if($c_subtype)
			{
				$c_subt .= " AND c.complaint_sub_type_id = '".$c_subtype."'";
			}
					
			
			$dateContion = "";
				
			if($frm_date!="" && $to_date!=""){
				$frm_format_date = date('Y-m-d', strtotime($frm_date));
				$to_format_date = date('Y-m-d', strtotime($to_date));
				$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
			}
			
			$this->db->select('id');
			$getCallCenterUser = $this->master_model->getRecords('adminlogin',array('role_id'=>5, 'status' => '1'));
			
			$cc_user_id = array();
			if(count($getCallCenterUser) > 0)
			{
				foreach($getCallCenterUser as $cc_list)
				{
					array_push($cc_user_id,$cc_list['id']);
				}
			}
			
			$impArr = implode(",", $cc_user_id);
			
			
			$creatorType = '';
			if($complaintCreatorType)
			{
				if($complaintCreatorType == 1){
					$creatorType .= " AND c.compaint_added_by IN (".$impArr.")";
				}
				else
				{
					$creatorType .= " AND c.compaint_added_by = '0'";
				}
			}	
			
			$condition = "SELECT c.comp_code, c.c_id, ct.complaint_type_name, c.comp_subject, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, u.user_name, c.reply_status, c.complaint_date, c.compaint_added_by, c.compaint_closed_by FROM gri_complaint c		
						JOIN userregistration u ON c.user_id = u.user_id
						JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id
						WHERE c.comp_code!='' ".$subt." ".$creatorType." ".$c_subt." ".$offType." ".$dateContion." ".$sta."";
				
			$query = $condition.' ORDER BY  (CASE c.reply_status WHEN 1 THEN 0 WHEN 0 THEN 1 WHEN 2 THEN 2 WHEN 3 THEN 3 END) ASC, c.complaint_date ASC';
			
			$rowCount = $this->getNumData($condition);			
			$result = $this->db->query($query)->result();
				

			
						
			$table = '<h3>Call Center Report</h3>
						<table border="1" style="width:100%;">						
							<tr> 
								 <th width="5%">Sr.No. </th>	
								 <th width="10%">Complaint Code</th>
								 <th width="10%">Complaint Date</th>
								 <th width="10%">Created By</th>
								 <th width="20%">User Type</th>
								 <th width="15%">Complaint type</th>
								 <th width="15%">Office type</th>
								 <th width="15%">Status</th>
							</tr>';
			$no = 1;
			foreach($result as $comData)
			{
					
			// Complaint Added By Name	
			$callcentername = $this->master_model->getRecords('adminlogin',array('id'=>$comData->compaint_added_by, 'status' => '1'));
			$openfullname = $callcentername[0]['namecc'];	
			
			// Complaint Closed By Name	
			$callcentername_2 = $this->master_model->getRecords('adminlogin',array('id'=>$comData->compaint_closed_by, 'status' => '1'));
			$closefullname = $callcentername_2[0]['namecc'];	
				
			// Role Name	
			$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$comData->role_id, 'status' => '1'));
			$roleName   = $roleDetail[0]['role_name']; 
			
			// Office Name
			$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$comData->sro_id, 'status' => '1'));
			$officeName   = $officeDetail[0]['office_name'];
			
			// District Name 
			$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$comData->district_id));
			$district_name		= $districtDetail[0]['district_name'];			
			
			//$designationName = $district_name .' - '. $roleName."==".$comData->id;
			$designationName = $roleName .' - '. $district_name;
			
			
			// Complaint Office Type name
			if($comData->complaint_sub_type_id > 0) 
			{
				$subTypeName = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$comData->complaint_sub_type_id, 'status' => '1'));
				$complaint_sub_type_name   = $subTypeName[0]['complaint_sub_type_name'];
			}				
			
			
			if($comData->reply_status == 1){$status = 'Pending';} 
			else if($comData->reply_status == 2){$status = 'Fake';}
			else if($comData->reply_status == 0){$status = 'Closed';}
			else if($comData->reply_status == 3){$status = 'Query To Complainant';}
			
				
							
				
				$comDate = date('d-m-Y', strtotime($comData->complaint_date));
				
				   
				   if($comData->compaint_added_by == 0)
				   {
						$user_t = 'Citizen';
						$full_n = $comData->user_name;
				   }
				   else 
				   {
					    $user_t = 'Call Center';
						$full_n = $openfullname;
				   }	
					 
					 $table .='<tr>	
								<td align="center">'.$no.'</td>
								<td>'.$comData->comp_code.'</td>
								<td>'.$comDate.'</td>
								<td align="center">'.$full_n.'</td>
								<td align="center">'.$user_t.'</td>
								<td align="center">'.$comData->complaint_type_name.'</td>
								<td align="center">'.$complaint_sub_type_name.'</td>
								<td align="center">'.$status.'</td>
							</tr>';
							
						
							
					$no++;	
				}
				
				
			
				$table .='</table>';
				
				$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins
				
				$mpdf->WriteHTML($table);

				$filename = "complaint_pdf/call-center-report-on-".date("Y-m-d-H-i-s").".pdf";
				//$mpdf->Output();
				$data['pdf_file_name'] = $filename;
				$mpdf->Output($filename, 'D');
				
		}
		
		$data['middlecontent']='sro_panel/cc_report';
		$data['master_mod'] = 'Reports';
		$data['function']='cc_report';
	    $this->load->view('admin/admin_combo',$data);
		
	}
	
	public function cc_reports_result()
	{
		//print_r($this->session->userdata());exit();
		error_reporting(0);
		$frm_date 		= @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
		$to_date 		= @$this->input->post('to_date')?$this->input->post('to_date'):'';
		//$office_subtype	= @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';
		$c_status		= @$this->input->post('c_status')?$this->input->post('c_status'):'-1';
		$c_type		= @$this->input->post('c_type')?$this->input->post('c_type'):'';
		//$user_type		= @$this->input->post('user_type')?$this->input->post('user_type'):'';
		$complaintCreatorType		= @$this->input->post('user_type')?$this->input->post('user_type'):'';
		
		$c_subtype		= @$this->input->post('c_subtype')?$this->input->post('c_subtype'):'';
		
		$type 			= $this->session->userdata('type');
		$sro_id 		= $this->session->userdata('sro_id');		
		$admin_id 		= $this->session->userdata('supadminid');
			
		
		// Get Login User Details
		$getLogin = $this->master_model->getRecords('adminlogin',array('id'=>$admin_id, 'status' => '1'));
		$getAdminID = $getLogin[0]['id'];
		$getAdminR  = $getLogin[0]['region_id'];
		$getAdminD  = $getLogin[0]['district_id'];
		$getAdminS  = $getLogin[0]['sro_id'];
		$getAdminO  = $getLogin[0]['office_id'];
		
		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value
		$pageNo = ($_POST['start']/$_POST['length'])+1;	

		$offType = "";
		$off_id = "";
		if($office_subtype){
			
			$offType .= " AND c.sro_office_id = '".$office_subtype."'";
			$off_id  .= " AND sro_id = '".$office_subtype."'";
			
		}

		$sta = "";
		if($c_status){	
			
			if($c_status == 4){
				
				$c_status = 0;
				$sta .= " AND c.reply_status = '".$c_status."'";
				
			} else if($c_status == -1){
				$sta .= " AND c.reply_status IN(0,1,2,3)";
			} else {
				
				$sta .= " AND c.reply_status = '".$c_status."'";
			}
			
		} 

		$subt = "";
		if($c_type)
		{
			$subt .= " AND c.complaint_type_id = '".$c_type."'";
		}
		
		$c_subt = "";
		if($c_subtype)
		{
			$c_subt .= " AND c.complaint_sub_type_id = '".$c_subtype."'";
		}
				
		
		$dateContion = "";
			
		if($frm_date!="" && $to_date!=""){
			$frm_format_date = date('Y-m-d', strtotime($frm_date));
			$to_format_date = date('Y-m-d', strtotime($to_date));
			$dateContion .= " AND c.complaint_date BETWEEN '".$frm_format_date."' AND '".$to_format_date."'";
		}
		
		$this->db->select('id');
		$getCallCenterUser = $this->master_model->getRecords('adminlogin',array('role_id'=>5, 'status' => '1'));
		
		$cc_user_id = array();
		if(count($getCallCenterUser) > 0)
		{
			foreach($getCallCenterUser as $cc_list)
			{
				array_push($cc_user_id,$cc_list['id']);
			}
		}
		
		$impArr = implode(",", $cc_user_id);
		
		
		$creatorType = '';
		if($complaintCreatorType)
		{
			if($complaintCreatorType == 1){
				$creatorType .= " AND c.compaint_added_by IN (".$impArr.")";
			}
			else
			{
				$creatorType .= " AND c.compaint_added_by = '0'";
			}
		}	
		
		$condition = "SELECT c.comp_code, c.c_id, ct.complaint_type_name, c.comp_subject, c.online_service_id, c.complaint_sub_type_id, c.district_id, c.religion_id, u.user_name, c.reply_status, c.complaint_date, c.compaint_added_by, c.compaint_closed_by FROM gri_complaint c		
					JOIN userregistration u ON c.user_id = u.user_id
					JOIN gri_complaint_type ct ON c.complaint_type_id = ct.complaint_type_id
					WHERE c.comp_code!='' ".$subt." ".$creatorType." ".$c_subt." ".$offType." ".$dateContion." ".$sta."";
			
		$query = $condition.' ORDER BY  (CASE c.reply_status WHEN 1 THEN 0 WHEN 0 THEN 1 WHEN 2 THEN 2 WHEN 3 THEN 3 END) ASC, c.complaint_date ASC LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
		//echo $query;die();
		$result 	= $this->db->query($query)->result();		
		
		$rowCount = $this->getNumData($condition);
		
		$rowCnt = 0;
		$dataArr = array();
		if($rowCount > 0){
			
			$no    = $_POST['start'];
			
			foreach($result as $comData){
			
			// Complaint Added By Name	
			$callcentername = $this->master_model->getRecords('adminlogin',array('id'=>$comData->compaint_added_by, 'status' => '1'));
			$openfullname = $callcentername[0]['namecc'];	
			
			// Complaint Closed By Name	
			$callcentername_2 = $this->master_model->getRecords('adminlogin',array('id'=>$comData->compaint_closed_by, 'status' => '1'));
			$closefullname = $callcentername_2[0]['namecc'];	
				
			// Role Name	
			$roleDetail = $this->master_model->getRecords('gri_roles',array('role_id'=>$comData->role_id, 'status' => '1'));
			$roleName   = $roleDetail[0]['role_name']; 
			
			// Office Name
			$officeDetail = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$comData->sro_id, 'status' => '1'));
			$officeName   = $officeDetail[0]['office_name'];
			
			// District Name 
			$districtDetail 	= $this->master_model->getRecords('gri_district',array('district_id'=>$comData->district_id));
			$district_name		= $districtDetail[0]['district_name'];			
			
			//$designationName = $district_name .' - '. $roleName."==".$comData->id;
			$designationName = $roleName .' - '. $district_name;
			
			
			// Complaint Office Type name
			if($comData->complaint_sub_type_id > 0) 
			{
				$subTypeName = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$comData->complaint_sub_type_id, 'status' => '1'));
				$complaint_sub_type_name   = $subTypeName[0]['complaint_sub_type_name'];
			}
			else 
			{
				$complaint_sub_type_name = '-';
			}				
			
			
			if($comData->reply_status == 1){$status = 'Pending';} 
			else if($comData->reply_status == 2){$status = 'Fake';}
			else if($comData->reply_status == 0){$status = 'Closed';}
			else if($comData->reply_status == 3){$status = 'Query To Complainant';}
			
			$url = base_url('admin/sro_panel/complaindetail/'. $comData->c_id);
			$action = '<a href="'. $url .'" title=""><i class="fa fa-eye"></i></a>';
			//$view = '<a href="javascript:void(0)">View</a>';	
								
				
				$comDate = date('d-m-Y', strtotime($comData->complaint_date));
				/*$dataArr[] = array(
					  $no,				  
					  $comData->comp_code,
					  $comData->user_name,
					  $comData->complaint_type_name,
					  $openfullname,
					  $closefullname,
					  $status,						  
					  $action
				   );*/
				   
				   if($comData->compaint_added_by == 0)
				   {
						$user_t = 'Citizen';
						$full_n = $comData->user_name;
				   }
				   else 
				   {
					    $user_t = 'Call Center';
						$full_n = $openfullname;
				   }
				  
				   
				   
				   $dataArr[] = array(
					  $no,				  
					  $comData->comp_code,
					  $comDate,					  
					  $full_n,
					  $user_t,
					  $comData->complaint_type_name,
					  $complaint_sub_type_name,
					  $status,						  
					  $action
				   );
				
			   $no++;
			   
			    $rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
				);
				
				
			} // Foreach Close  
			
		} else {
			
			$rowCnt = $rowCount;
				$response = array(
				  "draw" => $draw,
				  "recordsTotal" => 0,
				  "recordsFiltered" => 0,
				  "data" => $dataArr
				);
		}
		
		echo json_encode($response);
	}
	
	
}