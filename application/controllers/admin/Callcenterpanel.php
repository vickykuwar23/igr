<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Callcenterpanel extends CI_Controller 

{

   function __construct()

	{
// 		ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
		 parent::__construct();
		 $this->load->model('chk_session');
		 $this->load->model('master_model');
		 // $this->chk_session->chk_callcenter_session();

		 /*if($this->session->userdata('type')!='5')
			{
			  redirect(base_url().'superadminlogin/suplogin');
			}*/
			
				$typeslist = array("5", "18");
		
			if (in_array($this->session->userdata('type'), $typeslist))
			{
			 
			}
			else
			{
				redirect(base_url().'superadminlogin/suplogin');
			}
	}

	
	public function users()

	{   
		$data['users']=$this->master_model->getRecords('userregistration','','',array('created_at'=>'DESC'));
		$data['middlecontent']='cc_panel/userlist';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	function register_new_user()
	{
		$data['error_msg']=$data['file_error']='';
		$state=$city='';$emailsend='';$email='';
		
		if(isset($_POST['btn_reg']))
		{
	       $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|is_unique[userregistration.user_email]');
			$this->form_validation->set_rules('mobile', 'Mobile No.', 'trim|required|xss_clean|min_length[10]|numeric|is_unique[userregistration.user_mobile]');
			//$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|is_unique[userregistration.uniqueusername]|alpha_numeric');
			//$this->form_validation->set_rules('address1', 'Address', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('state', 'State', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');	
			//$this->form_validation->set_rules('pincode', 'Pincode', 'trim|required|xss_clean|numeric');	
			//$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('aadhar_number', 'Aadhar card number', 'numeric|min_length[12]|max_length[16]|xss_clean');	
	     	$this->form_validation->set_message('is_unique', 'The %s already exists');
			
			$name_1=htmlentities($this->input->post('name'));
			$uniqueusername_1=htmlentities($this->input->post('username'));
			$user_pass_1=htmlentities($this->input->post('password'));
			if($uniqueusername_1)
			{
				$assign_user = $uniqueusername_1;
				if($user_pass_1!="")
				{
					$passwordNew1=$user_pass_1;
					$original_pass = htmlentities($this->input->post('hidden_pass'));
				}
				else 
				{
					$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				}				
			}
			else 
			{
				$new_str_1 = str_replace(' ', '', $name_1);
				$updatedUser = strtolower($new_str_1).rand(10,100);
				$assign_user = $updatedUser;
				// $passwordNew1 = $updatedUser;

				$salt = strlen($updatedUser);
				$salt1 = sha1($salt);
				$passwordNew1 = $this->sha256(sha1($salt1 . $updatedUser));

				$original_pass = $updatedUser;
			}

			if($this->form_validation->run())
			{
				//echo "+++++++".$assign_user;
				$name=htmlentities($this->input->post('name'));
				$uniqueusername=htmlentities($this->input->post('username'));
				$email=htmlentities($this->input->post('email'));
				$mobile=htmlentities($this->input->post('mobile'));
				$landline=htmlentities($this->input->post('landline'));
				//$address1=htmlentities($this->input->post('address1'));
				//$address2=htmlentities($this->input->post('address2'));
				//$state=htmlentities($this->input->post('state'));
				$aadhar_number=htmlentities($this->input->post('aadhar_number'));
				//$city=htmlentities($this->input->post('city'));
				//$pincode=htmlentities($this->input->post('pincode'));
				$user_pass=htmlentities($this->input->post('password'));
				$hidden_pass=$original_pass;
				$pincode	= ($this->input->post('pincode')) ? $this->input->post('pincode') : '';
				$aadhar_number=htmlentities($this->input->post('aadhar_number'));
				$city = ($this->input->post('city')) ? $this->input->post('city') : '';
				$state = ($this->input->post('state')) ? $this->input->post('state') : '0';
				$address1 = ($this->input->post('address1')) ? $this->input->post('address1') : '';
				$address2 = ($this->input->post('address2')) ? $this->input->post('address2') : '';
				$landline = ($this->input->post('landline')) ? $this->input->post('landline') : '';
				
				if ($aadhar_number) {
					$aadhar_no = $aadhar_number;
				}else{
					$aadhar_no = null;
				}

				$insert_arr= array('user_name'=>$name,
								  'uniqueusername'=>$assign_user,
	                              'user_email'=>$email,
								  'user_mobile'=>$mobile,
								  'user_landline'=>$landline,
								  'user_address1'=>$address1,
								  'user_address2'=>$address2,
								  'user_state'=>$state,
								  'user_city'=>$city,
								   'aadhar_number'=>$aadhar_no,
								  'user_pincode'=>$pincode,
								  'user_pass'=>$this->sha256($passwordNew1),
								   'user_added_by'       => $this->session->userdata('supadminid'),
								);

			

			if($insert_id=$this->master_model->insertRecord('userregistration',$insert_arr,true))

			{
				//Store Details in Log Table

				$json_encode_data = json_encode($insert_arr);
				$log_user_id      = $this->session->userdata('supadminid');
				$ipAddr			  = $this->get_client_ip();
				$logDetails = array(
									'module_name' 	=> "Callcenterpanel",
									'action_name' 	=> "Add user",
									'json_response'	=> $json_encode_data,
									'login_id'		=> $log_user_id,
									'role_type'		=> "Call Center User",
									'createdAt'		=> date('Y-m-d H:i:s'),
									'ip_addr'		=> $ipAddr
									);
				$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
				if($email!='')
				{
					$admin_email=$this->master_model->getRecords('adminlogin');	
					
					$info_arr=array(
						'to'      =>$email,
						'cc'      =>'',
						'from'    =>$this->config->item('MAIL_FROM'),
						'subject' =>'GRIEVANCE REDRESSAL PORTAL REGISTRATION',
						'view'    =>'register-email-callcenter'
					);

					/*$other_info=array('name'=>$name,
									  'user_id'=>$insert_id,
									  'uniqueusername'=>$uniqueusername,
									  'password'=>$hidden_pass
									);*/
					$other_info=array('name'=>$name,
									  'user_id'=>$insert_id,
									  'uniqueusername'=>$assign_user,
									  'password'=>$hidden_pass
									);
							
					
					$emailsend = $this->email_sending->sendmail($info_arr,$other_info);
					
					if($emailsend)
					{
											$verify = array('emailverify'=>$email,'login_time'=>time());
                        $this->session->set_userdata($verify);			
						$this->session->set_flashdata('success_message','User registration Success.Please add grievance');
				    	redirect(base_url().'admin/callcenterpanel/add_grevience/'.$insert_id);
				    	exit;
					}
				}

		    }
		   }		
		}
	
		$data['state']=$this->master_model->getRecords('states',array('id'=>13),'',array('ch3'=>'ASC'));
		

		$data['middlecontent']='cc_panel/new_user_registration';
	    $this->load->view('admin/admin_combo',$data);
	}

	/*function register_new_user()
	{
		$data['error_msg']=$data['file_error']='';
		$state=$city='';$emailsend='';$email='';
		
		if(isset($_POST['btn_reg']))
		{
	       $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|valid_email|is_unique[userregistration.user_email]');
			$this->form_validation->set_rules('mobile', 'Mobile No.', 'trim|required|xss_clean|min_length[10]|numeric|is_unique[userregistration.user_mobile]');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|is_unique[userregistration.uniqueusername]|alpha_numeric');
			$this->form_validation->set_rules('address1', 'Address', 'trim|required|xss_clean');
			$this->form_validation->set_rules('state', 'State', 'trim|required|xss_clean');
			$this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('pincode', 'Pincode', 'trim|required|xss_clean|numeric');	
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				$this->form_validation->set_rules('aadhar_number', 'Aadhar card number', 'numeric|min_length[12]|max_length[16]|xss_clean');	
	     	$this->form_validation->set_message('is_unique', 'The %s already exists');

			if($this->form_validation->run() )

			{

				$name=htmlentities($this->input->post('name'));
				$uniqueusername=htmlentities($this->input->post('username'));
				$email=htmlentities($this->input->post('email'));
				$mobile=htmlentities($this->input->post('mobile'));
				$landline=htmlentities($this->input->post('landline'));
				$address1=htmlentities($this->input->post('address1'));
				$address2=htmlentities($this->input->post('address2'));
				$state=htmlentities($this->input->post('state'));
				$aadhar_number=htmlentities($this->input->post('aadhar_number'));
				$city=htmlentities($this->input->post('city'));
				$pincode=htmlentities($this->input->post('pincode'));
				$user_pass=htmlentities($this->input->post('password'));
				$hidden_pass=htmlentities($this->input->post('hidden_pass'));

				$aadhar_number=htmlentities($this->input->post('aadhar_number'));
				if ($aadhar_number) {
					$aadhar_no =$aadhar_number;
				}else{
					$aadhar_no = null;
				}
				$insert_arr= array('user_name'=>$name,
								  'uniqueusername'=>$uniqueusername,
	                              'user_email'=>$email,
								  'user_mobile'=>$mobile,
								  'user_landline'=>$landline,
								  'user_address1'=>$address1,
								  'user_address2'=>$address2,
								  'user_state'=>$state,
								  'user_city'=>$city,
								   'aadhar_number'=>$aadhar_no,
								  'user_pincode'=>$pincode,
								  'user_pass'=>$this->sha256($user_pass),
								   'user_added_by'       => $this->session->userdata('supadminid'),
								);

			 	


			if($insert_id=$this->master_model->insertRecord('userregistration',$insert_arr,true))

			{
				//Store Details in Log Table
				
				$json_encode_data = json_encode($insert_arr);
				$log_user_id      = $this->session->userdata('supadminid');
				$ipAddr			  = $this->get_client_ip();
				$logDetails = array(
									'module_name' 	=> "Callcenterpanel",
									'action_name' 	=> "Add user",
									'json_response'	=> $json_encode_data,
									'login_id'		=> $log_user_id,
									'role_type'		=> "Call Center User",
									'createdAt'		=> date('Y-m-d H:i:s'),
									'ip_addr'		=> $ipAddr
									);
				$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
				if($email!='')
				{
					$admin_email=$this->master_model->getRecords('adminlogin');	
					
					$info_arr=array(
						'to'      =>$email,
						'cc'      =>'',
						'from'    =>$this->config->item('MAIL_FROM'),
						'subject' =>'GRIEVANCE REDRESSAL PORTAL REGISTRATION',
						'view'    =>'register-email-callcenter'
					);

					$other_info=array('name'=>$name,
									  'user_id'=>$insert_id,
									  'uniqueusername'=>$uniqueusername,
									  'password'=>$hidden_pass
									);
					
					$emailsend = $this->email_sending->sendmail($info_arr,$other_info);
					
					if($emailsend)
					{
						$verify = array('emailverify'=>$email,'login_time'=>time());
                        $this->session->set_userdata($verify);			
						$this->session->set_flashdata('success_message','User registration Success.Please add grievance');
				    	redirect(base_url().'admin/callcenterpanel/add_grevience/'.$insert_id);
				    	exit;
					}
				}

		    }
		   }		
		}
	
		$data['state']=$this->master_model->getRecords('states',array('id'=>13),'',array('ch3'=>'ASC'));
		

		$data['middlecontent']='cc_panel/new_user_registration';
	    $this->load->view('admin/admin_combo',$data);
	}*/

	
	
	
	public function add_grevience()
	{	
		$cust_id=$this->uri->segment(4);
		$r_comp_code = '';
		$is_level_1  = '1';
		$is_level_2  = '0';
		$is_level_3  = '0';		
		
		$userId = $cust_id; 

		$data['get_regions']=$this->master_model->getRecords('gri_region_division',array('status' => '1'),'',array('religion_id'=>'ASC'));	
		
		$data['complaint_subtype_details']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => '2', 'status' => '1'),'',array('complaint_type_id'=>'DESC'));
		
		$data['complaint_subtype_service']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => '1', 'status' => '1'),'',array('complaint_type_id'=>'DESC'));
		
		$x=1;

		$this->load->library('upload');	
		$this->form_validation->set_rules('complaint_type', 'Complaint Type', 'required|xss_clean');
		$this->form_validation->set_rules('grievance_details', 'Grievance Details', 'trim|required|xss_clean');
		$this->form_validation->set_rules('acknowldgement', 'Acknowledgement', 'required|xss_clean');
	
		$data['imageError'] = "";
		if($this->form_validation->run())
		{		
			
		if($_FILES['upload_file']['name']!=""){			
			
			$config['upload_path']      = './images/complaint_images';
			$config['allowed_types']    = '*';  
			$config['max_size']         = '5000';         
			$config['encrypt_name']     = TRUE;
						 
			$upload_files  = @$this->master_model->upload_file('upload_file', $_FILES, $config, FALSE);
		   $image = "";
			if(isset($upload_files[0]) && !empty($upload_files[0])){
				
				$image = $upload_files[0];
					
			} 			
			//print_r($upload_files);
			//$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png", "pdf" => "application/pdf", "doc" => "application/msword", 'docx'=>'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'txt'	=>	'text/plain', 'xlsx'	=>	array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip'), 'xls'	=>	array('application/excel', 'application/vnd.ms-excel', 'application/msexcel'), 'ppt'	=>	array('application/powerpoint', 'application/vnd.ms-powerpoint'), 'pptx'  =>  array('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/zip', 'application/msword','application/vnd.ms-powerpoint'), , 'mp3'	=>	'audio/mp3', 'wav' => 'audio/wav', '3gp' => 'audio/3gp');	
			$allowed = array("pdf" => "application/pdf");					 
			$ext = pathinfo($image[0], PATHINFO_EXTENSION);
			//echo "===".$ext;die();				
			if(!array_key_exists($ext, $allowed)){
					
					$data['imageError'] = "Upload file in pdf format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url().'admin/callcenterpanel/add_grevience/'.$cust_id);	
				}
						
				$filesize = $_FILES['upload_file']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
				   
				   if($filesize > $maxsize) 
				   {
					   $data['imageError'] = "File size is larger than the allowed limit";
						$this->session->set_flashdata('error',$data['imageError']);
						redirect(base_url().'admin/callcenterpanel/add_grevience/'.$cust_id);					 
				   } 
				$createdAt		=   date('Y-m-d H:i:s');
				$complaint_type = $this->input->post('complaint_type');
				$office_type 	= $this->input->post('office_type');
				$subject 		= $this->input->post('subject');
				$region 		= $this->input->post('region');
				$district 		= $this->input->post('district');
				$sro_office 	= $this->input->post('sro_office');
				$othertext 		= $this->input->post('othertext');
				$grie_details 	= $this->input->post('grievance_details');
				$relief_ed 		= $this->input->post('relief_required');
				$residentid 	= $this->input->post('residentid');
				$acknowldgement = $this->input->post('acknowldgement');
				$online_service_id = $this->input->post('office_service');
				
				$code_generate  = $this->random_strings(4);				
				
				if($residentid!=""){$resident_id = $residentid;}else{$resident_id = "";}
				if($othertext!=""){$otxt = $othertext;}else{$otxt = "";}
				if($region!=""){$rtxt = $region;}else{$rtxt = "0";}
				if($district!=""){$dtxt = $district;}else{$dtxt = "0";}
				if($sro_office!=""){$stxt = $sro_office;}else{$stxt = "0";}
				if($subject!=""){$strSub = $subject;}else{$strSub = "0";}
				if($online_service_id!=""){$osid = $online_service_id;}else{$osid = "0";}
				if($office_type!=""){$offid = $office_type;}else{$offid = "0";}
				
				if($region == 4){
					//5 = cc, 3= IGRA, 4 = hd, 
					if($complaint_type != 1){ // Specific Office						
						$users_data = $this->master_model->getRecords('adminlogin', array('region_id'=>$region, 'district_id' => $district, 'sro_id' => $sro_office, 'office_id' =>$office_type),'','');

						if($complaint_type==2 && count($users_data) == 0){
							$this->session->set_flashdata('error','Officer not assign for this complaint.');
							redirect(base_url().'admin/callcenterpanel/add_grevience/'.$cust_id);
						}

						$user_ID = $users_data[0]['id'];
						$sro_id2  = $users_data[0]['sro_id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					} else { // Online Office
						$users_data = $this->master_model->getRecords('adminlogin', array('role_id'=> '4'),'','');
						$sro_id2 = $users_data[0]['id'];
						$user_ID = $users_data[0]['id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					}
					
				} else {
					
					if($complaint_type != 1){ // Specific Office
						$users_data = $this->master_model->getRecords('adminlogin', array('region_id'=>$region, 'district_id' => $district, 'sro_id' => $sro_office, 'office_id' =>$office_type),'','');
						if($complaint_type==2 && count($users_data) == 0){
							$this->session->set_flashdata('error','Officer not assign for this complaint.');
							redirect(base_url().'admin/callcenterpanel/add_grevience/'.$cust_id);
						}
						$user_ID = $users_data[0]['id'];
						$sro_id2  = $users_data[0]['sro_id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					} else { // Online Office
						$users_data = $this->master_model->getRecords('adminlogin', array('role_id'=> '4'),'','');
						$sro_id2 = $users_data[0]['id'];
						$user_ID = $users_data[0]['id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					}
				}

				// For Mail And SMS sending
				$getUser 	 = $this->master_model->getRecords('userregistration',array('user_id'=>$userId));
				$front_email = $getUser[0]['user_email'];
				$user_name   = $getUser[0]['user_name'];
				$user_mobile = $getUser[0]['user_mobile'];
				
				
				if(!empty($sro_id2)){
					$sro_id = $sro_id2;
				} else {
					$sro_id = 0;
				}			
				
				
				$dateNow		= date('Ymd');
				$complaintDate  = date('Y-m-d');	
				$complaint_code = 'COM/'.$code_generate.'/'.$dateNow;
				$insert_arr=array('comp_code'				=>	'',
								  'user_id'					=>	$userId,
								  'complaint_type_id'		=>	$complaint_type,
								  'complaint_sub_type_id'	=>	$offid,
								  'comp_subject'			=>	$strSub,
								  'comp_other'				=>	$otxt,
								  'religion_id'				=>	$rtxt,
								  'district_id'				=>	$dtxt,
								  'sro_office_id'			=>	$stxt,
								  'grievence_details'		=>	$grie_details,
								  'relief_required'			=>	$relief_ed,
								  'residentid'				=>	$resident_id,
								  'acknowldgement'			=>	$acknowldgement,
								  'complaint_date'			=>	$complaintDate,
								  'complaint_status'		=>	'1',
								  'reply_status'            =>   '1',
								  'complaint_closed_date'	=>	null,
								  'complaint_assign_to'		=>	$sro_id,
								  'complaint_updated_by'	=>	'0',
								  'status'					=>	'1',
								  'compaint_added_by'       => $this->session->userdata('supadminid'),
								  'createdat'				=>	$createdAt,
								  'updatedat'				=>	$createdAt,
								  'online_service_id'		=>  $osid,
								  'r_comp_code'				=>  $r_comp_code,
								  'is_level_1'				=>  $is_level_1,
								  'is_level_2'				=>  $is_level_2,
								  'is_level_3'				=>  $is_level_3
								  );			
				  
			$complaint_add = $this->master_model->insertRecord('gri_complaint',$insert_arr);

			$last_patternid=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complaint_add),'c_id');

					if(count($last_patternid) > 0)

					{

						$last_count = 100+$last_patternid[0]['c_id']; 

					}

				  
			$pattern='GR'.date('y').'0'.$x.'00'.$last_count;

		  $this->master_model->updateRecord('gri_complaint',array('comp_code'=>$pattern),array('c_id'=>$complaint_add));
			
			//Images Added 
			if($complaint_add > 0){
				
				foreach($image as $img){
					
					$img_arr=array( 'c_id' 			=> $complaint_add,
									'comp_code' 	=> $pattern,
									'image_name' 	=> $img,
									'status' 		=> '1',
									'createdat' 	=> $createdAt);
					$images_add = $this->master_model->insertRecord('gri_complaint_images',$img_arr);
				}
					
				
			}

			$date_com = date('Y-m-d');
					//$endDate =  date('Y-m-d', strtotime($date_com. ' + 7 days'));
					if($complaint_type == 1){
					
						$endDate =  date('Y-m-d', strtotime($date_com. ' + 14 days'));
						
					} else {
						
						$endDate =  date('Y-m-d', strtotime($date_com. ' + 7 days'));
						
					}
					
					//$sro_id
					$comp_assign =array( 
										'comp_code' 	=> $pattern,
										'assign_user_id' => $user_ID,
										'assign_date' 	=> date('Y-m-d'),
										'end_date'		=> $endDate,
										'status'		=> '1',
										'createdat' 	=> $createdAt);
					$compassing = $this->master_model->insertRecord('gri_complaint_assign',$comp_assign);
			
			//email admin sending code 

			$info_arr=array(
					'to'=>$front_email,					
					'cc'=>'',
					'from'=>$this->config->item('MAIL_FROM'),
					'subject'=>'GRIEVANCE REDRESSAL PORTAL REGISTRATION - Complaint Register',
					'view'=>'complain_email'
					);

			
			
			$other_info=array('pattern'=>$pattern,'reply_status'=>'Open','msg_content'=>$grie_details,'name'=>$user_name,'user_email'=>$front_email,'user_mobile'=>$user_mobile,'user_id'=>$user_ID); 

			$emailsend=$this->email_sending->sendmail($info_arr,$other_info);

			$info_arr=array(
					'to'=>$sro_email,					
					'cc'=>'',
					'from'=>$this->config->item('MAIL_FROM'),
					'subject'=>'GRIEVANCE REDRESSAL PORTAL REGISTRATION - New Complaint Register',
					'view'=>'complain_email'
					);
			
			$other_info=array('pattern'=>$pattern,'reply_status'=>'Open','msg_content'=>$grie_details,'name'=>$user_name,'user_email'=>$front_email,'user_mobile'=>$user_mobile,'user_id'=>$user_ID); 
			$emailsend=$this->email_sending->sendmail($info_arr,$other_info);
			
			$this->send_sms($user_mobile,$pattern);

			$this->send_sms_sro($sro_mobile,$pattern);
			
			// add logs
			$array_first = array_merge($insert_arr,$comp_assign);
			$array_second = array_merge($array_first, $img_arr);
			$json_encode_data = json_encode($array_second);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Callcenterpanel",
								'action_name' 	=> "Add Complaint",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> "Call center user",
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);	

			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			//end add logs

			if($emailsend)
			{				
				$this->session->set_flashdata('success_message','Your Complaint has been registered and your Grievance ID is <strong>'.$pattern.'</strong>');
				redirect(base_url().'admin/callcenterpanel/users');
			}
			
			
			
			$this->session->set_flashdata('success_message','Your Complaint has been registered and your Grievance ID is <strong>'.$pattern.'</strong>');
			redirect(base_url().'admin/callcenterpanel/users');
			
			
		} else { 
				//echo "<pre>";
				//print_r($this->input->post());
				$createdAt		=   date('Y-m-d H:i:s');
				$complaint_type = $this->input->post('complaint_type');
				$office_type 	= $this->input->post('office_type');
				$subject 		= $this->input->post('subject');
				$region 		= $this->input->post('region');
				$district 		= $this->input->post('district');
				$sro_office 	= $this->input->post('sro_office');
				$othertext 		= $this->input->post('othertext');
				$grie_details 	= $this->input->post('grievance_details');
				$relief_ed 		= $this->input->post('relief_required');
				$residentid 	= $this->input->post('residentid');
				$acknowldgement = $this->input->post('acknowldgement');
				$online_service_id = $this->input->post('office_service');
				$code_generate  = $this->random_strings(4);
				if($subject!=""){$strSub = $subject;}else{$strSub = "0";}
				if($residentid!=""){$resident_id = $residentid;}else{$resident_id = "";}
				if($othertext!=""){$otxt = $othertext;}else{$otxt = "";}
				if($region!=""){$rtxt = $region;}else{$rtxt = "0";}
				if($district!=""){$dtxt = $district;}else{$dtxt = "0";}
				if($sro_office!=""){$stxt = $sro_office;}else{$stxt = "0";}
				if($online_service_id!=""){$osid = $online_service_id;}else{$osid = "0";}
				if($office_type!=""){$offid = $office_type;}else{$offid = "0";}
				
				if($region == 4){
					//5 = cc, 3= IGRA, 4 = hd, 
					if($complaint_type != 1){ // Specific Office						
						$users_data = $this->master_model->getRecords('adminlogin', array('region_id'=>$region, 'district_id' => $district, 'sro_id' => $sro_office, 'office_id' =>$office_type),'','');
						if($complaint_type==2 && count($users_data) == 0){
							$this->session->set_flashdata('error','Officer not assign for this complaint.');
							redirect(base_url().'admin/callcenterpanel/add_grevience/'.$cust_id);
						}
						$user_ID = $users_data[0]['id'];
						$sro_id2  = $users_data[0]['sro_id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					} else { // Online Office
						$users_data = $this->master_model->getRecords('adminlogin', array('role_id'=> '4'),'','');
						if($complaint_type==2 && count($users_data) == 0){
							$this->session->set_flashdata('error','Officer not assign for this complaint.');
							redirect(base_url().'admin/callcenterpanel/add_grevience/'.$cust_id);
						}
						$sro_id2 = $users_data[0]['id'];
						$user_ID = $users_data[0]['id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					}
					
				} else {
					
					if($complaint_type != 1){ // Specific Office
						$users_data = $this->master_model->getRecords('adminlogin', array('region_id'=>$region, 'district_id' => $district, 'sro_id' => $sro_office, 'office_id' =>$office_type),'','');
						$user_ID = $users_data[0]['id'];
						$sro_id2  = $users_data[0]['sro_id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					} else { // Online Office
						$users_data = $this->master_model->getRecords('adminlogin', array('role_id'=> '4'),'','');
						$sro_id2 = $users_data[0]['id'];
						$user_ID = $users_data[0]['id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					}
				}				
				
				
				// For Mail And SMS sending
				$getUser 	= $this->master_model->getRecords('userregistration',array('user_id'=>$userId ));
				$front_email = $getUser[0]['user_email'];
				$user_name = $getUser[0]['user_name'];
				$user_mobile = $getUser[0]['user_mobile'];
				if(!empty($sro_id2)){
					$sro_id = $sro_id2;
				} else {
					$sro_id = 0;
				}			
				
						
				
				$dateNow		= date('Ymd');
				$complaintDate  = date('Y-m-d');	
				$complaint_code = 'COM/'.$code_generate.'/'.$dateNow;
				$insert_arr=array('comp_code'				=>	'',
								  'user_id'					=>	$userId,
								  'complaint_type_id'		=>	$complaint_type,
								  'complaint_sub_type_id'	=>	$offid,
								  'comp_subject'			=>	$strSub,
								  'comp_other'				=>	$otxt,
								  'religion_id'				=>	$rtxt,
								  'district_id'				=>	$dtxt,
								  'sro_office_id'			=>	$stxt,
								  'grievence_details'		=>	$grie_details,
								  'relief_required'			=>	$relief_ed,
								  'residentid'				=>	$resident_id,
								  'acknowldgement'			=>	$acknowldgement,
								  'complaint_date'			=>	$complaintDate,
								  'complaint_status'		=>	'1',
								  'reply_status'            =>   '1',
								  'complaint_closed_date'	=>	null,
								  'complaint_assign_to'		=>	$sro_id,
								  'complaint_updated_by'	=>	'0',
								  'status'					=>	'1',
								  'compaint_added_by'       => $this->session->userdata('supadminid'),
								  'createdat'				=>	$createdAt,
								  'updatedat'				=>	$createdAt,
								  'online_service_id'		=>  $osid,
								  'r_comp_code'				=>  $r_comp_code,
								  'is_level_1'				=>  $is_level_1,
								  'is_level_2'				=>  $is_level_2,
								  'is_level_3'				=>  $is_level_3
								  );			
				  
			$complaint_add = $this->master_model->insertRecord('gri_complaint',$insert_arr);

			$last_patternid=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complaint_add),'c_id');

					if(count($last_patternid) > 0)

					{

						$last_count = 100+$last_patternid[0]['c_id']; 

					}

				  
				  $pattern='GR'.date('y').'0'.$x.'00'.$last_count;
				  
				  if($complaint_add > 0){
					$date_com = date('Y-m-d');
					//$endDate =  date('Y-m-d', strtotime($date_com. ' + 7 days'));
					if($complaint_type == 1){
						
						$endDate =  date('Y-m-d', strtotime($date_com. ' + 14 days'));
						
					} else {
						
						$endDate =  date('Y-m-d', strtotime($date_com. ' + 7 days'));
						
					}
					$comp_assign =array( 
											'comp_code' 	=> $pattern,
											'assign_user_id' => $user_ID,
											'assign_date' 	=> date('Y-m-d'),
											'end_date'		=> $endDate,
											'status'		=> '1',
											'createdat' 	=> $createdAt);
						$compassing = $this->master_model->insertRecord('gri_complaint_assign',$comp_assign);  
				  }
			//email admin sending code 
			$info_arr=array(
					'to'=>$front_email,					
					'cc'=>'',
					'from'=>$this->config->item('MAIL_FROM'),
					'subject'=>'GRIEVANCE REDRESSAL PORTAL - Complaint Register',
					'view'=>'complain_email'
					);

			$other_info=array('pattern'=>$pattern,'reply_status'=>'Open','msg_content'=>$grie_details,'name'=>$user_name,'user_email'=>$front_email,'user_mobile'=>$user_mobile,'user_id'=>$user_ID); 
			$emailsend=$this->email_sending->sendmail($info_arr,$other_info);

			$info_arr_1=array(
					'to'=>$sro_email,					
					'cc'=>'',
					'from'=>$this->config->item('MAIL_FROM'),
					'subject'=>'GRIEVANCE REDRESSAL PORTAL - New Complaint Register',
					'view'=>'complain_email'
					);

			$other_info_1=array('pattern'=>$pattern,'reply_status'=>'Open','msg_content'=>$grie_details,'name'=>$user_name,'user_email'=>$front_email,'user_mobile'=>$user_mobile,'user_id'=>$user_ID); 

			$emailsend=$this->email_sending->sendmail($info_arr_1,$other_info_1);
			$this->send_sms($user_mobile,$pattern);
			$this->send_sms_sro($sro_mobile,$pattern);

			// add logs
			$array_first = array_merge($insert_arr,$comp_assign);
			$json_encode_data = json_encode($array_first);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Callcenterpanel",
								'action_name' 	=> "Add Complaint",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> "Call center User",
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);	

			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			//end add logs

			if($emailsend)
			{
				$this->master_model->updateRecord('gri_complaint',array('comp_code'=>$pattern),array('c_id'=>$complaint_add));
				$this->session->set_flashdata('success_message','Your Complaint has been registered and your Grievance ID is <strong>'.$pattern.'</strong>');
				redirect(base_url().'admin/callcenterpanel/users');
			}
			

			//mail($to, $subjects, $message, $headers);
			$this->master_model->updateRecord('gri_complaint',array('comp_code'=>$pattern),array('c_id'=>$complaint_add));
			$this->session->set_flashdata('success_message','Your Complaint has been registered and your Grievance ID is <strong>'.$pattern.'</strong>');
			redirect(base_url().'admin/callcenterpanel/users');	
			
		}
			
		
		}
		
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		  //$this->session->unset_userdata("mycaptcha");
	    $_SESSION["complaintcaptcha"]=$cap['word'];
		
		$data['complaint_type_list']=$this->master_model->getRecords('gri_complaint_type','','',array('complaint_type_id'=>'DESC'));
		$user_info=$this->master_model->getRecords('userregistration',array('user_id'=>$cust_id ));
		$data['user_info']=$user_info;
		$data['middlecontent']='cc_panel/add_compaint';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	public function complaindetail($comp_id)
			{   //echo ">>>".$this->session->userdata('supadminid');
				$error = '';
				$complain_id=$comp_id;

				if(isset($_POST['reply']))
				{
					$this->form_validation->set_rules('reply_msg','Message','trim|required|xss_clean');
					if($this->form_validation->run())
					{
						 $reply_msg=$this->input->post('reply_msg');
						 
						 $insert_arr=array(
											'user_id'=>$this->session->userdata('supadminid'),
											'reply_msg'=>$reply_msg,
											'reply_date'=>date('Y-m-d'),
											'complain_id'=>$complain_id,
											'complaint_status'=>'1',
											'created_at'=>date('Y-m-d')
										   );
			         
		                $this->load->library('upload');
		                $config['upload_path'] 	 = 'images/reply_uploads';
		                $config['allowed_types'] = '*';
		                $config['max_size']	 = '500000000';
		                $config['overwrite']	 = TRUE;
		             
		                if($_FILES['reply_file']['name'])
		                {
			        		$file_dets1 = $_FILES["reply_file"]['name'];
			                $ext = pathinfo($file_dets1, PATHINFO_EXTENSION);
			                $file_dets1 ='file_'.round(microtime(true)).'.'.$ext;

		                    $config['file_name'] = $file_dets1;
		                    $file = $config['file_name'];
		                    $this->upload->initialize($config); // Important

		                    if(!$this->upload->do_upload('reply_file'))
		                    {
		                        $error.= $this->upload->display_errors();
		                        $this->session->set_flashdata('error_message',$this->upload->display_errors());
		                         redirect(base_url().'admin/callcenterpanel/complaindetail/'.$complain_id);
		                    }
		               	 	
		               	 	$insert_arr['reply_file'] = $file;

		                }

							if($insert_id=$this->master_model->insertRecord('complainreply',$insert_arr,true))
							{
								// add log
								$json_encode_data = json_encode($insert_arr);
								$log_user_id      = $this->session->userdata('supadminid');
								$ipAddr			  = $this->get_client_ip();
								$logDetails = array(
													'module_name' 	=> "Callcenterpanel",
													'action_name' 	=> "Add Reply",
													'json_response'	=> $json_encode_data,
													'login_id'		=> $log_user_id,
													'role_type'		=> "Call Center User",
													'createdAt'		=> date('Y-m-d H:i:s'),
													'ip_addr'		=> $ipAddr
													);
								$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								// end add log
								
								$complainid=array();

								$previous_reply=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id,'replyby !='=>null));		

								if(is_array($previous_reply) && count($previous_reply) > 0)
								{
									$complainid=explode(',',$previous_reply[0]['replyby']);

								}

								if(is_array($complainid) && count($complainid) >0)

								{

									if(!in_array($this->session->userdata('supadminid'),$complainid))

									{

										$complainid[]=$this->session->userdata('supadminid');

									}

								$strrepliedid=implode(',',$complainid);

								}

								else

								{

									$strrepliedid=$this->session->userdata('supadminid');

								}

								$update_arr=array('status'=>'1','replyby'=>$strrepliedid);
								$condition=array('c_id'=>"'".$complain_id."'");

								$this->master_model->updateRecord('gri_complaint',$update_arr,$condition);

								$admin_email=$this->master_model->getRecords('adminlogin',array('adminlogin.id'=>$this->session->userdata('adminid')));						

							    // get user information

								$this->db->join('userregistration','userregistration.user_id=gri_complaint.user_id');

								$user_info=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id));

								//email admin sending code 

								$info_arr=array(

								'to'=>$user_info[0]['user_email'],

								'from'=>$this->config->item('MAIL_FROM'),//$admin_email[0]['email']
								
								// 'cc'=>$admin_email[0]['emailcc'],
								'cc'=>'',


								'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$user_info[0]['comp_code'].'',

								'view'=>'complain_reply_email'

								);

								$other_info=array('name'=>$user_info[0]['user_name'],

															 'pattern'=>$user_info[0]['comp_code'],

															 'user_email'=>$user_info[0]['user_email'],

															 'user_mobile'=>$user_info[0]['user_mobile'],

															 'msg_content'=>$user_info[0]['grievence_details'],

															 'replyremark'=>$reply_msg,

															'reply_status'=>$user_info[0]['reply_status']);
								//sending email with info to user

								if($this->email_sending->sendmail($info_arr,$other_info))

								{  
									$this->session->set_flashdata('success_message','Reply has been post successfully');
									 redirect(base_url().'admin/callcenterpanel/complaindetail/'.$complain_id);
							    }

								}
							}

					}

				if(isset($_POST['btn_update']))

				{

					$this->form_validation->set_rules('update_reply_msg','Message','trim|required|xss_clean');

					if($this->form_validation->run())

					{

						 $reply_msg=$this->input->post('update_reply_msg');

						 $reply_id=$this->input->post('reply_id');

						 $update_arr=array('reply_msg'=>$reply_msg,

						 								'user_id'=>$this->session->userdata('adminid'),

														'reply_date'=>date('Y-m-d'),

														'complain_id'=>$complain_id,

														'complaint_status'=>'1'

													   );

							if($this->master_model->updateRecord('complainreply',$update_arr,array('reply_id'=>$reply_id)))

							{

								 $this->session->set_flashdata('success_reply','Replied message has been updated successfully');

								  redirect(base_url().'admin/callcenterpanel/complaindetail/'.$complain_id.'#reply_id');

							}

						}

					}


				if(isset($_POST['status_btn']))

				{
						$reply_status=$this->input->post('reply_type_status');

						if($reply_status==0)
						{
							 $closed_date=date('Y-m-d');
							 $update_arr=array(	'reply_status'=>$reply_status,
												'complaint_status'=>$reply_status,
				 								'closed_date'=>$closed_date,
				 								'compaint_closed_by'=>$this->session->userdata('supadminid')
													   );
							if($this->master_model->updateRecord('gri_complaint',$update_arr,array('c_id'=>$complain_id)))

							{
								// add log
								$json_encode_data = json_encode($update_arr);
								$log_user_id      = $this->session->userdata('supadminid');
								$ipAddr			  = $this->get_client_ip();
								$logDetails = array(
													'module_name' 	=> "Callcenterpanel",
													'action_name' 	=> "Close compaint",
													'json_response'	=> $json_encode_data,
													'login_id'		=> $log_user_id,
													'role_type'		=> "Call Center User",
													'createdAt'		=> date('Y-m-d H:i:s'),
													'ip_addr'		=> $ipAddr
													);
								$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								// end add log

								$admin_email=$this->master_model->getRecords('adminlogin',array('adminlogin.id'=>$this->session->userdata('adminid')));	

								// get user information

								$this->db->join('userregistration','userregistration.user_id=gri_complaint.user_id');

								$user_info=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id));

							    $complainreply=$this->master_model->getRecords('complainreply',array('complain_id'=>$complain_id));

								//email admin sending code 

								$info_arr=array(

								'to'=>$user_info[0]['user_email'],

								'from'=>$this->config->item('MAIL_FROM'),//$admin_email[0]['email']
								
								// 'cc'=>$admin_email[0]['emailcc'],

								'cc'=>'',


								'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$user_info[0]['comp_code'].'',

								'view'=>'complain_reply_email'

								);

								$other_info=array('name'=>$user_info[0]['user_name'],

															 'pattern'=>$user_info[0]['comp_code'],

															 'user_email'=>$user_info[0]['user_email'],

															 'user_mobile'=>$user_info[0]['user_mobile'],

															 'msg_content'=>$user_info[0]['grievence_details'],

															 'replyremark'=>$complainreply[0]['reply_msg'],

															'reply_status'=>$user_info[0]['reply_status']);
									$this->send_sms_close($user_info[0]['user_mobile'],$user_info[0]['comp_code']);
							

		                         if($this->email_sending->sendmail($info_arr,$other_info))

								 {

								 $this->session->set_flashdata('success_reply_status','Grievance has been closed');
								 redirect(base_url().'admin/callcenterpanel/complaindetail/'.$complain_id.'#reply_status_id');
								 }
							}

						} else if($reply_status==2){
							
							$closed_date=date('Y-m-d');
							 $update_arr=array('reply_status'=>$reply_status,
												'complaint_status'=>$reply_status,
				 								'closed_date'=>$closed_date,
				 								'compaint_closed_by'=>$this->session->userdata('supadminid')
												);
							$this->master_model->updateRecord('gri_complaint',$update_arr,array('c_id'=>$complain_id));	
							// add log
								$json_encode_data = json_encode($update_arr);
								$log_user_id      = $this->session->userdata('supadminid');
								$ipAddr			  = $this->get_client_ip();
								$logDetails = array(
													'module_name' 	=> "Callcenterpanel",
													'action_name' 	=> "Fake compaint",
													'json_response'	=> $json_encode_data,
													'login_id'		=> $log_user_id,
													'role_type'		=> "Call Center User",
													'createdAt'		=> date('Y-m-d H:i:s'),
													'ip_addr'		=> $ipAddr
													);
								$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								// end add log				
							$this->session->set_flashdata('success_reply_status','Grievance mark as a fake.');
							 redirect(base_url().'admin/callcenterpanel/complaindetail/'.$complain_id.'#reply_status_id');
								
						}
						else if($reply_status==3){
						$closed_date=date('Y-m-d');
						 $update_arr=array('reply_status'=>$reply_status,
											'complaint_status'=>$reply_status,
			 								'closed_date'=>$closed_date,
			 								'compaint_closed_by'=>$this->session->userdata('supadminid')
											);
						$this->master_model->updateRecord('gri_complaint',$update_arr,array('c_id'=>$complain_id));	
						// add log
						$json_encode_data = json_encode($update_arr);
						$log_user_id      = $this->session->userdata('supadminid');
						$ipAddr			  = $this->get_client_ip();
						$logDetails = array(
											'module_name' 	=> "Callcenterpanel",
											'action_name' 	=> "Query to compainant",
											'json_response'	=> $json_encode_data,
											'login_id'		=> $log_user_id,
											'role_type'		=> "Call Center User",
											'createdAt'		=> date('Y-m-d H:i:s'),
											'ip_addr'		=> $ipAddr
											);
						$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
						// end add log				
						$this->session->set_flashdata('success_reply_status','Grievance mark as a Query to Complaint.');
						 redirect(base_url().'admin/sro_panel/complaindetail/'.$complain_id.'#reply_status_id');
						
						}
						else
						{
							 $this->session->set_flashdata('success_reply_status','Grievance alredy closed');
							  redirect(base_url().'admin/callcenterpanel/complaindetail/'.$complain_id.'#reply_status_id');
										 
						}
				}

				

				$data['error_msg']='';

				$data['error_up']='';


				 // $user=$this->master_model->getRecords('userregistration',array('user_id'=>$details['user_id']));
				 //        $complaint_type=$this->master_model->getRecords('gri_complaint_type',array('complaint_type_id'=>$details['complaint_type_id']));
				 //        $office=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$details['complaint_sub_type_id']));
				 //        $subject=$this->master_model->getRecords('gri_subject',array('sub_id'=>$details['comp_subject']));

		        	$this->db->join('gri_complaint_type','gri_complaint_type.complaint_type_id=gri_complaint.complaint_type_id','left');	
		        	
		        	$this->db->join('gri_complaint_sub_type','gri_complaint_sub_type.complaint_sub_type_id=gri_complaint.complaint_sub_type_id','left');	
					
					$this->db->join('gri_subject','gri_subject.sub_id=gri_complaint.comp_subject','left');
				$data['complaint']=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complain_id));

				$data['error'] = $error;
				print_r($data['error']);

				$data['middlecontent']='sro_panel/details';
			    $this->load->view('admin/admin_combo',$data);
		}
	

	
	public function sha256($pass)
	{
		return hash('sha256',$pass);
	}

	public function user_details($value='')
	 {
		$this->db->join('userregistration','userregistration.user_id=gri_complaint.user_id');	
	 	$result=$this->master_model->getRecords('gri_complaint',array('gri_complaint.user_id'=>$value ));

	
		$data['result']=$result;

   //      $data['middlecontent']='user_details';

	 	// $this->load->view('admintemplate',$data);
	 	$data['middlecontent']='cc_panel/user_details';
	    $this->load->view('admin/admin_combo',$data);
	 
	 } 

	 public function usercomplaindetail($id)

	{
		$data['countuser']='';
	    $data['userinfo']=$this->master_model->getRecords('gri_complaint',array('c_id'=>$this->uri->segment(4)));
	    if(count($data['userinfo'])>0)

		{
			$data['countuser']='1';
		}
		// echo "<pre>";
		// print_r($data);
		// die;
		// $data['middlecontent']='call_cent_usercomdetails';
	 // 	$this->load->view('admintemplate',$data);
		$data['middlecontent']='cc_panel/comp_details';
	    $this->load->view('admin/admin_combo',$data);

	}

	

	public function index()
	{   
	 	$type='';
	    $data['error_msg']='';
		$data['error_up']='';
		if(isset($_POST['grievance_type']))
		{
			$type=htmlentities($this->input->post('grievance_type'));
		}
		if($type!='')
		{
			if($type==1)
			{

			}

			if($type==2)
			{

				$this->db->where('reply_status','0');
			}

			if($type==3)
			{
				$this->db->where('reply_status','1');
			}
		}

		if($this->session->userdata('type')=='7')
		{
			$data['complaint']=$this->master_model->getRecords('usercomplainbox','','',array('msgid'=>'DESC'));
		}
		else
		{
			$data['complaint']=$this->master_model->getRecords('usercomplainbox',array('category'=>$this->session->userdata('type')),'',array('msgid'=>'DESC'));
		}

		$data['type']=$type;	
		$data['middlecontent']='adminpanel_vw';
	 	$this->load->view('admintemplate',$data);
	}

	



	

	public function delete()

	{

		$complain_id=$this->uri->segment('3');

		$reply_id=$this->uri->segment('4');

		if($this->master_model->deleteRecord('complainreply','reply_id',$reply_id))

		{

			 $this->session->set_flashdata('success_reply','Replied message has been deleted successfully');

			 redirect(base_url().'adminpanel/complaindetail/'.$complain_id.'#reply_id');

		}

		else

		{

		  	$this->session->set_flashdata('success_reply','Error while deleting record');

			redirect(base_url().'adminpanel/complaindetail/'.$complain_id.'#reply_id');

		}
	

	}


public function getTypes(){
		$c_type_id 	= 	$this->input->post('complaint_types');
		$data['complaint_subtype_details']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => $c_type_id),'',array('complaint_type_id'=>'DESC'));
				
		if(count($data['complaint_subtype_details'])>0)	{
			foreach($data['complaint_subtype_details'] as $subtype){
				if($subtype['complaint_sub_type_id'] == $c_type_id){
					$chk = 'checked';
				}else{
					$chk = '';
					}
				$options .= '<option value="'.$subtype['complaint_sub_type_id'].'" '.$chk.'>'.$subtype['complaint_sub_type_name'].'</option>';
			}
		} else {
			$options .= '<option value="0">None</option>';
		}	
				
		
		echo $options;
	}
	public function getOther(){
		
		$c_office_type 		= 	$this->input->post('office_type');
		$c_complaint_type 	= 	$this->input->post('complaint_types');
		$options = "";		
		if($c_office_type == 3 && $c_complaint_type == 2){
		
			/*$data['get_regions']=$this->master_model->getRecords('gri_region_division','','',array('religion_id'=>'ASC'));
			
			$options .= '<div class="form-group" id="subject" >
        <div class="row">
          <div class="col-md-4">
            <label class="control-label">Subject<span style="color:red">*</span></label>
          </div>
          <div class="col-md-8">
            <select name="subject" class="form-control" id="subjectlist">
							<option value="0">--Select--</option>';
							if(count($data['get_regions'])>0){
								foreach($data['get_regions'] as $region){
									$options .= '<option value="'.$region['religion_id'].'">'.$region['region_division_name'].'</option>';
								}
							} else {
								$options .= '<option value="0">--Select--</option>';
							}
						$options .= '</select>	
          </div>
        </div>
					  </div>';*/
		
		} else if($c_office_type == 4 && $c_complaint_type == 2){
		
			$data['get_regions']=$this->master_model->getRecords('gri_region_division','','',array('religion_id'=>'ASC'));
			
			$options .= '<div class="form-group" id="regions" >
        <div class="row">
          <div class="col-md-4">
            <label class="control-label">Region<span style="color:red">*</span></label>
          </div>
          <div class="col-md-8">
            <select name="region" class="form-control" id="region">
							<option value="">--Select--</option>';
							if(count($data['get_regions'])>0){
								foreach($data['get_regions'] as $region){
									$options .= '<option value="'.$region['religion_id'].'">'.$region['region_division_name'].'</option>';
								}
							} else {
								$options .= '<option value="0">--Select--</option>';
							}
						$options .= '</select>
          </div>
        </div>
					  </div>';
		
		} else if($c_office_type == 6 && $c_complaint_type == 2){
			
			$data['get_regions']=$this->master_model->getRecords('gri_region_division','','',array('religion_id'=>'ASC'));
			
			$options .= '<div class="form-group" id="regions">
        <div class="row">
          <div class="col-md-4">
            <label class="control-label">Region<span style="color:red">*</span></label>
          </div>
          <div class="col-md-8">
            <select name="region" class="form-control listout" id="region">
							<option value="">--Select--</option>';
							if(count($data['get_regions'])>0){
								foreach($data['get_regions'] as $region){
									$options .= '<option value="'.$region['religion_id'].'">'.$region['region_division_name'].'</option>';
								}
							} else {
								$options .= '<option value="0">--Select--</option>';
							}
						$options .= '</select>
          </div>
        </div>
					  </div>';
					  
			$options .= '<div class="form-group" id="districtlists" >
							<div class="row">
							  <div class="col-md-4">
								<label class="control-label">District<span style="color:red">*</span></label>
							  </div>
							  <div class="col-md-8">
								<select name="district" class="form-control districtout" id="district">
												<option value="">--Select--</option>
											</select>	
							  </div>
							</div>
					  </div>';
				  
		
		} else if($c_office_type == 7 && $c_complaint_type == 2){
			
			$data['get_regions']=$this->master_model->getRecords('gri_region_division','','',array('religion_id'=>'ASC'));
			
			$options .= '<div class="form-group" id="regions">
							<div class="row">
							  <div class="col-md-4">
								<label class="control-label">Region<span style="color:red">*</span></label>
							  </div>
							  <div class="col-md-8">
								<select name="region" class="form-control listout" id="region">
												<option value="">--Select--</option>';
												if(count($data['get_regions'])>0){
													foreach($data['get_regions'] as $region){
														$options .= '<option value="'.$region['religion_id'].'">'.$region['region_division_name'].'</option>';
													}
												} else {
													$options .= '<option value="0">--Select--</option>';
												}
											$options .= '</select>						
							  </div>
							</div>
					  </div>';
					  
			$options .= '<div class="form-group" id="districtlists" >
					<div class="row">
					  <div class="col-md-4">
						<label class="control-label">District<span style="color:red">*</span></label>
					  </div>
					  <div class="col-md-8">
						<select name="district" class="form-control districtout" id="district">
										<option value="">--Select--</option>
									</select>						
					  </div>
					</div>
					</div>';
					  
			$options .= '<div class="form-group" id="srolists" >
							<div class="row">
							  <div class="col-md-4">
								<label class="control-label">SRO Offices<span style="color:red">*</span></label>
							  </div>
							  <div class="col-md-8">
								<select name="sro_office" class="form-control" id="sro_office">
												<option value="">--Select--</option>
											</select>						
							  </div>
							</div>
					  </div>';		  
		
		} else if($c_office_type == 8 && $c_complaint_type == 2){
			
			$data['get_regions']=$this->master_model->getRecords('gri_region_division','','',array('religion_id'=>'ASC'));
			
			$options .= '<div class="form-group" id="regions">
							<div class="row">
							  <div class="col-md-4">
								<label class="control-label">Region<span style="color:red">*</span></label>
							  </div>
							  <div class="col-md-8">
								<select name="region" class="form-control listout" id="region">
												<option value="">--Select--</option>';
												if(count($data['get_regions'])>0){
													foreach($data['get_regions'] as $region){
														$options .= '<option value="'.$region['religion_id'].'">'.$region['region_division_name'].'</option>';
													}
												} else {
													$options .= '<option value="0">--Select--</option>';
												}
											$options .= '</select>	
							  </div>
							</div>
					  </div>';
					  
			$options .= '<div class="form-group" id="districtlists" >
							<div class="row">
							  <div class="col-md-4">
								<label class="control-label">District<span style="color:red">*</span></label>
							  </div>
							  <div class="col-md-8">
								<select name="district" class="form-control districtout" id="district">
												<option value="">--Select--</option>
											</select>	
							  </div>
							</div>
					  </div>';
					  
			$options .= '<div class="form-group" id="srolists" >
							<div class="row">
							  <div class="col-md-4">
								<label class="control-label">SRO Offices<span style="color:red">*</span></label>
							  </div>
							  <div class="col-md-8">
								<select name="sro_office" class="form-control" id="sro_office">
												<option value="">--Select--</option>
											</select>						
							  </div>
							</div>
					  </div>';		  
		
		} 
		
		echo  $options;
	}
	
	public function getDistrict(){
		
		$c_office_type 		= 	$this->input->post('office_type');
		$c_complaint_type 	= 	$this->input->post('complaint_types');
		$c_regionid 		= 	$this->input->post('regionid');
		//$c_regionid 		= 	$this->input->post('region');
		$c_district_id		= 	$this->input->post('district_id');
		$csrf_token 		=   $this->security->get_csrf_hash();
		$data['distict_details'] = $this->master_model->getRecords('gri_district',array('religion_id' => $c_regionid),'',array('district_id'=>'ASC'));
		$options = '<option value="">-- Select --</option>';
		
		if($c_district_id!=""){
			if(count($data['distict_details'])>0)	{
				foreach($data['distict_details'] as $district){
					if($district['district_id'] == $c_district_id)
					{
						$sel = 'selected';
					} else {
						$sel = '';
					}
					$options .= '<option value="'.$district['district_id'].'" '.$sel.'>'.$district['district_name'].'</option>';
				}			
			} else {
				$options .= '<option value="">None</option>';
			}
		} else {
			
			if(count($data['distict_details'])>0)	{
				foreach($data['distict_details'] as $district){
					$options .= '<option value="'.$district['district_id'].'">'.$district['district_name'].'</option>';
				}			
			} else {
				$options .= '<option value="">None</option>';
			}
		}		
		
		//echo $options;
		echo json_encode(['options' => $options, 'csrf_token' => $csrf_token]);
	}
	
	
	public function getSroOffice(){
		$c_office_type 		= 	$this->input->post('office_type');
		$c_complaint_type 	= 	$this->input->post('complaint_types');
		$c_regionid 		= 	$this->input->post('region');
		$c_district_id 		= 	$this->input->post('district');
		$c_sro_id			= 	$this->input->post('sro_off');
		
		
		$data['sro_office'] =	$this->master_model->getRecords('gri_sro_offices',array('district_id' => $c_district_id, 'office_sub_type' => $c_office_type, 'status' => '1'),'',array('sro_office_id'=>'ASC'));
		
		$options = '<option value="">-- Select --</option>';
		if(count($data['sro_office'])>0)	{
			foreach($data['sro_office'] as $office){
				if($office['sro_office_id'] == $c_sro_id){
					$oType = 'selected';
				} else {
					$oType = '';
				}
				$options .= '<option value="'.$office['sro_office_id'].'" '.$oType.'>'.$office['office_name'].'</option>';
			}			
		} else {
			$options .= '<option value="">None</option>';
		}
		echo $options;	
	
	}

	public function getSubject(){
		$c_office_type 		= 	$this->input->post('office_type');
		$c_complaint_type 	= 	$this->input->post('complaint_types');
		$c_sub_id 			= 	$this->input->post('subject_id');
		
		$data['subject_details']=$this->master_model->getRecords('gri_subject',array('complaint_type' => $c_complaint_type, 'office_type' => $c_office_type),'',array('sub_id'=>'DESC'));
		$options = '<option value="">-- Select --</option>';
		
		if($c_sub_id == ""){
			
			if(count($data['subject_details'])>0)	{
				foreach($data['subject_details'] as $subject){
					if($subject['sub_id'] == $c_sub_id){
						$selid = 'selected';
					} else {
						$selid = '';
					}
					$options .= '<option value="'.$subject['sub_id'].'" >'.$subject['subject_name'].'</option>';
				}
				$options .= '<option value="-1">Others</option>';
			} else {
				$options .= '<option value="">None</option>';
			}
			
		} else {
			
			if(count($data['subject_details'])>0)	{
				foreach($data['subject_details'] as $subject){
					if($subject['sub_id'] == $c_sub_id){
						$selid = 'selected';
					} else {
						$selid = '';
					}
					$options .= '<option value="'.$subject['sub_id'].'" '.$selid.'>'.$subject['subject_name'].'</option>';
				}
				$options .= '<option value="-1">Others</option>';
			} else {
				$options .= '<option value="">None</option>';
			}
			
		}
		echo $options;	
	}
	
	public function getTextarea(){
		$c_subject_id	= 	$this->input->post('subjectid');
		$options = "";
		if($c_subject_id == '-1'){
			$options = '<div class="form-group" id="othr" >
							<div class="row">
							  <div class="col-md-4">
								  <label class="control-label">Other</label>
							  </div>
							  <div class="col-md-8">
								<textarea name="othertext" id="othertext" class="form-control"></textarea>						
							  </div>
							</div>
					  </div>';
		}
		echo $options;
	}
	
	//Random String
	public function random_strings($length_of_string) 
	{ 
	  
		// String of all alphanumeric character 
		$str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
	  
		// Shufle the $str_result and returns substring 
		// of specified length 
		return substr(str_shuffle($str_result),  
						   0, $length_of_string); 
	} 


	function send_sms($mobile=NULL,$pattern=NULL)
 	{

 		if($mobile!=NULL && $pattern!=NULL)
 		{
 			$text = "Your Grievance has been successfully submitted. Grievance Number: ".$pattern;
 			$msg = urlencode($text);
 			$url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH&dlt_entity_id=1001419850000010247";

 			$string = preg_replace('/\s+/', '', $url);
 			$x = curl_init($string);
 			curl_setopt($x, CURLOPT_HEADER, 0);	
 			curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
 			curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);	
 			curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);		
 			$reply = curl_exec($x);
 		// 	if (curl_errno($x)) {
			//     $error_msg = curl_error($x);
			// }
 		// 	print_r($error_msg);
 		// 	die;
 			// if($reply)
 			// {
 			// 	return true;
 			// 	print_r($reply);
 			// 	die;
 				
 			// }
 			// else
 			// {
 			// 	echo "failed";
 			// 	die;
 				
 			// }
 		
 		}
 	}

 	function send_sms_sro($mobile=NULL,$pattern=NULL)
 	{

 		if($mobile!=NULL && $pattern!=NULL)
 		{
 			$text = "New added on Grievance IGR Redressal System. Grievance Number: ".$pattern;
 			$msg = urlencode($text);
 			$url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH&dlt_entity_id=1001419850000010247";

 			$string = preg_replace('/\s+/', '', $url);
 			$x = curl_init($string);
 			curl_setopt($x, CURLOPT_HEADER, 0);	
 			curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
 			curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);	
 			curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);		
 			$reply = curl_exec($x);
 		// 	if (curl_errno($x)) {
			//     $error_msg = curl_error($x);
			// }
 		// 	print_r($error_msg);
 		// 	die;
 			// if($reply)
 			// {
 			// 	return true;
 				
 				
 			// }
 			// else
 			// {
 			// 	echo "failed";
 			// 	die;
 				
 			// }
 		
 		}
 	}

 	function send_sms_close($mobile=NULL,$pattern=NULL)
 	{
 		

 		if($mobile!=NULL && $pattern!=NULL)
 		{
 			$text = "Your Grievance Number ".$pattern." has been closed.";
 			$msg = urlencode($text);
 			$url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH&dlt_entity_id=1001419850000010247";

 			$string = preg_replace('/\s+/', '', $url);
 			$x = curl_init($string);
 			curl_setopt($x, CURLOPT_HEADER, 0);	
 			curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
 			curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);	
 			curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);		
 			$reply = curl_exec($x);		
 		
 		}
 	}

 	public function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}


	

	}

?>