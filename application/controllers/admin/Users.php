<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller 

{

   function __construct()

	{

		 parent::__construct();
		 $this->load->model('chk_session');
		 $this->load->model('master_model');
		 // $this->chk_session->chk_callcenter_session();

		
	}

	public function list()
	{   
		$data['users']=$this->master_model->getRecords('userregistration','','',array('created_at'=>'DESC'));
		$data['middlecontent']='users/userlist';
	    $this->load->view('admin/admin_combo',$data);
	}

	public function feedback()
	{   
		$data['data']=$this->master_model->getRecords('gri_feedback','','',array('created_at'=>'DESC'));
		$data['middlecontent']='users/feedback';
	    $this->load->view('admin/admin_combo',$data);
	}
	public function feedback_details($id)
	{   
		$data['data']=$this->master_model->getRecords('gri_feedback',array('id'=>$id));
		$data['middlecontent']='users/feedback_details';
	    $this->load->view('admin/admin_combo',$data);
	}


	
}