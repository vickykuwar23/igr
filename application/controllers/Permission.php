<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Permission extends CI_Controller 
{
   function __construct()
	{
		 parent::__construct();
		 $this->load->model('chk_session');
		 $this->chk_session->chk_superadmin_session();
	}
	
	public function add()
	{   
		//$data['add'] = 'Permission';
		//$this->master_model->hasPermission('');
		$data['module'] = $this->master_model->getRecords('gri_module',array('status'=>'1'));
		$data['roles'] = $this->master_model->getRecords('gri_roles',array('status'=>'1'));
	  	
		$id=$this->uri->segment('3');
		
			
	  if(isset($_POST['submit']))
		{		
			$this->form_validation->set_rules('role_id', 'Role Name', 'trim|required|xss_clean');
			
			if($this->form_validation->run())
			{
				$this->master_model->emptyTable('gri_permission');	
				
				$roleID 	 = $this->input->post('role_id');
				$permission 	 = $this->input->post('permission');				
				
				if(count($permission) > 0){
					
					foreach($permission as $key => $setperm){				
						$createdAt			=   date('Y-m-d H:i:s');
						$write  = in_array('add', $setperm) ? '1' : '0';
						$read   = in_array('read', $setperm) ? '1' : '0';
						$delete = in_array('delete', $setperm) ? '1' : '0';
						$export = in_array('export', $setperm) ? '1' : '0';
						$download = in_array('download', $setperm) ? '1' : '0';
						$edit   = in_array('edit', $setperm) ? '1' : '0';
						$import = in_array('import', $setperm) ? '1' : '0';
						$print  = in_array('print', $setperm) ? '1' : '0';
					
						$insert_arr	= array('read'=>$read,
											  'write'=>$write,
											  'edit'=>$edit,
											  'delete'=>$delete,
											  'export'=>$export,
											  'import'=>$import,
											  'print'=>$print,
											  'download'=>$download,
											  'module_id' => $key,
											  'role_id' => $roleID,
											  'status' => '1',
											  'createdat' => $createdAt
											  );
						$this->master_model->insertRecord('gri_permission',$insert_arr);
										  
						}	
					redirect(base_url().'permission/add/'.$id);				
				}				
				
			}
			
		}	
		
		$data['middlecontent']='permission/add';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
		
    }//F_login	
	
}//C_welcome