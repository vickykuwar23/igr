<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaintsubtype extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		$this->chk_session->chk_superadmin_session();
    }
	
	public function index()
	{
		$data['complaint_subtype_list']=$this->master_model->getRecords('gri_complaint_sub_type','','',array('complaint_sub_type_id'=>'DESC'));
		$data['middlecontent']='complaint_subtype/index';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}

	public function add()
	{	redirect(base_url().'complaintsubtype');
		$this->form_validation->set_rules('complaint_sub_type_name', 'Complaint Sub Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('complaint_type_id', 'Complaint Type', 'trim|required|numeric|xss_clean');
		//$this->form_validation->set_rules('resident_id', 'Resident ID', 'trim|required|numeric|xss_clean');	
		if($this->form_validation->run())
		{
			$c_sub_type_name 	= 	htmlentities($this->input->post('complaint_sub_type_name'));
			$c_type_id 			= 	$this->input->post('complaint_type_id');
			$c_region			= 	$this->input->post('region') ? $this->input->post('region') : '0';
			$c_district			= 	$this->input->post('district') ? $this->input->post('district') : '0';
			$c_sro_offices		= 	$this->input->post('sro_offices') ? $this->input->post('sro_offices') : '0';
			$c_none_record		= 	$this->input->post('none_record') ? $this->input->post('none_record') : '0';
			$createdAt			=   date('Y-m-d H:i:s');
			
			$insert_arr=array('complaint_sub_type_name'	=>	$c_sub_type_name,
							  'complaint_type_id'		=>	$c_type_id,
							  'region'					=>	$c_region,
							  'district'				=>	$c_district,
							  'sro_offices'				=>	$c_sro_offices,
							  'none_record'				=>	$c_none_record,
							  'status'					=>	1,
							  'createdat'				=>	$createdAt,
							  'updatedat'				=>	$createdAt
							  );
						  
			$complaintType_info = $this->master_model->insertRecord('gri_complaint_sub_type',$insert_arr);
			 //add logs
				$json_encode_data = json_encode($insert_arr);
				$log_user_id      = $this->session->userdata('supadminid');
				$ipAddr			  = $this->get_client_ip();
				$logDetails = array(
									'module_name' 	=> "Complaintsubtype",
									'action_name' 	=> "Add",
									'json_response'	=> $json_encode_data,
									'login_id'		=> $log_user_id,
									'role_type'		=> $this->session->userdata('role_name'),
									'createdAt'		=> date('Y-m-d H:i:s'),
									'ip_addr'		=> $ipAddr
									);
				$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
				//end add logs
			$this->session->set_flashdata('success_message','The office type has been created');
			redirect(base_url().'complaintsubtype');
		}
		$data['complaint_type_list']=$this->master_model->getRecords('gri_complaint_type','','',array('complaint_type_id'=>'DESC'));
		$data['middlecontent']='complaint_subtype/add';	
		$data['master_mod'] = 'Master';
	 	$this->load->view('admin/admin_combo',$data);
	}
	
/****** Edit Functionality ******/ 
	
	public function edit()
	{
		$data['error_msg']='';
		$updateid=$this->uri->segment('3');
		
			$this->form_validation->set_rules('complaint_sub_type_name', 'Complaint Sub Type', 'trim|required|xss_clean');
			$this->form_validation->set_rules('complaint_type_id', 'Complaint Type', 'trim|required|numeric|xss_clean');	
			
			if($this->form_validation->run())
			{
				$c_sub_type_name 	= 	htmlentities($this->input->post('complaint_sub_type_name'));
				$c_type_id 			= 	$this->input->post('complaint_type_id');
				$c_region			= 	$this->input->post('region') ? $this->input->post('region') : '0';
				$c_district			= 	$this->input->post('district') ? $this->input->post('district') : '0';
				$c_sro_offices		= 	$this->input->post('sro_offices') ? $this->input->post('sro_offices') : '0';
				$c_none_record		= 	$this->input->post('none_record') ? $this->input->post('none_record') : '0';
				$updatedAt			=   date('Y-m-d H:i:s');
				
				$update_arr=array('complaint_sub_type_name'	=>	$c_sub_type_name,
								  'complaint_type_id'		=>	$c_type_id,
								  'region'					=>	$c_region,
								  'district'				=>	$c_district,
								  'sro_offices'				=>	$c_sro_offices,
								  'none_record'				=>	$c_none_record,
								  'status'					=>	1,
								  'updatedat'				=>	$updatedAt
								  );
				
				if($this->master_model->updateRecord('gri_complaint_sub_type',$update_arr,array('complaint_sub_type_id'=>"'".$updateid."'")))
				{
					//add logs
					$for_id=array('record_id'=>$updateid);
					$array_first = array_merge($update_arr,$for_id);
					$json_encode_data = json_encode($array_first);
					$log_user_id      = $this->session->userdata('supadminid');
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Complaintsubtype",
										'action_name' 	=> "Edit",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> $this->session->userdata('role_name'),
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
					//end add logs
				
					$this->session->set_flashdata('success_message',' Office type has been updated successfully.');
					redirect(base_url().'complaintsubtype/edit/'.$updateid);
				}
				else
				{
					$data['error_msg']='Error while updating record.';
				}
			}
		

		$data['complaint_type_list']=$this->master_model->getRecords('gri_complaint_type','','',array('complaint_type_id'=>'DESC'));
		$data['complaint_subtype_details']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id' => $updateid),'',array('complaint_type_id'=>'DESC'));
		//print_r($data['complaint_type_list']);die();
		if(count($data['complaint_subtype_details']) <= 0)
		{
			redirect(base_url().'complaintsubtype/');
		}
			
		$data['middlecontent']='complaint_subtype/edit';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'complaintsubtype');
		}
		if($this->master_model->deleteRecord('gri_complaint_sub_type','complaint_sub_type_id',$id))
		{
			// logs
			$for_id=array('record_id'=>$id);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Complaintsubtype",
								'action_name' 	=> "Delete",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			// end logs
		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'complaintsubtype');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'complaintsubtype');
		}
	
	}
	
	public function changeStatus()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'complaintsubtype');
		}
		
		$data['complaint_subtype_details']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id' => $id),'',array('complaint_type_id'=>'DESC'));
		if($data['complaint_subtype_details'][0]['status'] == 1){
			$status = 1;
		} else {
			$status = 0;
		}
		echo ">>>".$status;die();
		$update_arr=array(
						  'status'					=>	$status,								  
						  'updatedAt'				=>	$updatedAt
						  );
				
				if($this->master_model->updateRecord('gri_complaint_sub_type',$update_arr,array('complaint_sub_type_id'=>"'".$id."'")))
				{
					$this->session->set_flashdata('success_message',' Complaint Subtype Status has been updated successfully.');
					redirect(base_url().'complaintsubtype');
				}
				else
				{
					$data['error_msg']='Error while updating record.';
				}
	
	}

	public function change_status()
	{
		 $id = $this->uri->segment(3);
		 $status = $this->uri->segment(4);
	
		if($status=='1'){$newstatus = '0'; }
		else if($status=='0'){$newstatus = '1';}
		if($this->master_model->updateRecord("gri_complaint_sub_type",array('status'=>$newstatus)
		,array('complaint_sub_type_id'=>$id)))
		{
			// logs
			$for_id=array('record_id'=>$id,'status'=>$status);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Complaintsubtype",
								'action_name' 	=> "change status",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			// logs end
			$this->session->set_flashdata('success_message','Status changed successfully.');						           
			redirect(base_url().'complaintsubtype');
		}
		else
		{
			$this->session->set_flashdata('error_message','Error while updating user status.');										            
			redirect(base_url().'complaintsubtype');
		}
	}

	function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }
	
} // Class End

?>