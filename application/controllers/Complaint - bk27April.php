<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		// $this->chk_session->chk_superadmin_session();
		$this->chk_session->chk_user_session();
		$this->load->helper('form');
		$this->load->helper('url');
		if($this->session->userdata('user_id')==""){
			redirect(base_url('userlogin'));
		}
    }
	
	// function to list complaints
	public function index()
	{
		$this->session->unset_userdata('p_comp_code');
		$userId = $this->session->userdata('user_id');		
		$data['complaint_listing']=$this->master_model->getRecords('gri_complaint',array('user_id' => $userId),'',array('complaint_date'=>'ASC'));
		$data['middlecontent']='complaint/index';
	    $this->load->view('template',$data);
	}
		
	public function getTypes(){
		$c_type_id 	= 	$this->input->post('complaint_types');
		$data['complaint_subtype_details']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => $c_type_id),'',array('complaint_type_id'=>'DESC'));
				
		if(count($data['complaint_subtype_details'])>0)	{
			foreach($data['complaint_subtype_details'] as $subtype){
				if($subtype['complaint_sub_type_id'] == $c_type_id){
					$chk = 'checked';
				}else{
					$chk = '';
					}
				$options .= '<option value="'.$subtype['complaint_sub_type_id'].'" '.$chk.'>'.$subtype['complaint_sub_type_name'].'</option>';
			}
		} else {
			$options .= '<option value="0">None</option>';
		}	
				
		
		echo $options;
	}
	
	
	public function getDistrict(){
		
		$c_office_type 		= 	$this->input->post('office_type');
		$c_complaint_type 	= 	$this->input->post('complaint_types');
		$c_regionid 		= 	$this->input->post('regionid');
		$c_district_id		= 	$this->input->post('district_id');
		
		$data['distict_details'] = $this->master_model->getRecords('gri_district',array('religion_id' => $c_regionid),'',array('district_id'=>'ASC'));
		$options = '<option value="">-- Select --</option>';
		
		if($c_district_id!=""){
			if(count($data['distict_details'])>0)	{
				foreach($data['distict_details'] as $district){
					if($district['district_id'] == $c_district_id)
					{
						$sel = 'selected';
					} else {
						$sel = '';
					}
					$options .= '<option value="'.$district['district_id'].'" '.$sel.'>'.$district['district_name'].'</option>';
				}			
			} else {
				$options .= '<option value="">None</option>';
			}
		} else {
			
			if(count($data['distict_details'])>0)	{
				foreach($data['distict_details'] as $district){
					$options .= '<option value="'.$district['district_id'].'">'.$district['district_name'].'</option>';
				}			
			} else {
				$options .= '<option value="">None</option>';
			}
		}		
		
		echo $options;
		
	}
	
	public function getSroOffice(){
		$c_office_type 		= 	$this->input->post('office_type');
		$c_complaint_type 	= 	$this->input->post('complaint_types');
		$c_regionid 		= 	$this->input->post('region');
		$c_district_id 		= 	$this->input->post('district');
		$c_sro_id			= 	$this->input->post('sro_off');
		
		
		$data['sro_office'] =	$this->master_model->getRecords('gri_sro_offices',array('district_id' => $c_district_id, 'office_sub_type' => $c_office_type, 'status' => '1'),'',array('sro_office_id'=>'ASC'));
		
		$options = '<option value="">-- Select --</option>';
		if(count($data['sro_office'])>0)	{
			foreach($data['sro_office'] as $office){
				if($office['sro_office_id'] == $c_sro_id){
					$oType = 'selected';
				} else {
					$oType = '';
				}
				$options .= '<option value="'.$office['sro_office_id'].'" '.$oType.'>'.$office['office_name'].'</option>';
			}			
		} else {
			$options .= '<option value="0">None</option>';
		}
		echo $options;	
	
	}
	
	public function getSubject(){
		$c_office_type 		= 	$this->input->post('office_type');
		$c_complaint_type 	= 	$this->input->post('complaint_types');
		$c_sub_id 			= 	$this->input->post('subject_id');
		
		$data['subject_details']=$this->master_model->getRecords('gri_subject',array('complaint_type' => $c_complaint_type, 'office_type' => $c_office_type),'',array('sub_id'=>'DESC'));
		$options = '<option value="">-- Select --</option>';
		
		if($c_sub_id == ""){
			
			if(count($data['subject_details'])>0)	{
				foreach($data['subject_details'] as $subject){
					if($subject['sub_id'] == $c_sub_id){
						$selid = 'selected';
					} else {
						$selid = '';
					}
					$options .= '<option value="'.$subject['sub_id'].'" >'.$subject['subject_name'].'</option>';
				}
				$options .= '<option value="-1">Others</option>';
			} else {
				$options .= '<option value="">None</option>';
			}
			
		} else {
			
			if(count($data['subject_details'])>0)	{
				foreach($data['subject_details'] as $subject){
					if($subject['sub_id'] == $c_sub_id){
						$selid = 'selected';
					} else {
						$selid = '';
					}
					$options .= '<option value="'.$subject['sub_id'].'" '.$selid.'>'.$subject['subject_name'].'</option>';
				}
				$options .= '<option value="-1">Others</option>';
			} else {
				$options .= '<option value="">None</option>';
			}
			
		}
		echo $options;	
	}
	
	public function getTextarea(){
		$c_subject_id	= 	$this->input->post('subjectid');
		$options = "";
		if($c_subject_id == '-1'){
			$options = '<div class="form-group" id="othr" >
							<div class="row">
							  <div class="col-md-4">
								  <label class="control-label">Other</label>
							  </div>
							  <div class="col-md-8">
								<textarea name="othertext" id="othertext" class="form-control"></textarea>						
							  </div>
							</div>
					  </div>';
		}
		echo $options;
	}
	
	//Random String
	public function random_strings($length_of_string) 
	{ 
	  
		// String of all alphanumeric character 
		$str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
	  
		// Shufle the $str_result and returns substring 
		// of specified length 
		return substr(str_shuffle($str_result),  
						   0, $length_of_string); 
	} 
	
	public function add()
	{	
		//$r_comp_code = $this->session->userdata('comp_code');
		$this->session->unset_userdata('p_comp_code');
		$r_comp_code = '';
		$is_level_1  = '1';
		$is_level_2  = '0';
		$is_level_3  = '0';
		$userId = $this->session->userdata('user_id');
		$data['get_regions']=$this->master_model->getRecords('gri_region_division',array('status' => '1'),'',array('religion_id'=>'ASC'));	
		$data['complaint_subtype_details']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => '2', 'status' => '1'),'',array('complaint_type_id'=>'DESC'));
		$data['complaint_subtype_service']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => '1', 'status' => '1'),'',array('complaint_type_id'=>'DESC'));
		$x=1;
		$this->load->library('upload');	
		$this->form_validation->set_rules('complaint_type', 'Complaint Type', 'required|xss_clean');
		$this->form_validation->set_rules('grievance_details', 'Grievance Details', 'trim|required|xss_clean');
		$this->form_validation->set_rules('acknowldgement', 'Acknowledgement', 'required|xss_clean');
		$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_complaint');
		$data['imageError'] = "";
		if($this->form_validation->run())
		{	
			
			
		if($_FILES['upload_file']['name']!=""){			
			
			$config['upload_path']      = './images/complaint_images';
			$config['allowed_types']    = '*';  
			$config['max_size']         = '5000';         
			$config['encrypt_name']     = TRUE;
						 
			$upload_files  = @$this->master_model->upload_file('upload_file', $_FILES, $config, FALSE);
		   $image = "";
			if(isset($upload_files[0]) && !empty($upload_files[0])){
				
				$image = $upload_files[0];
					
			} 			
			//print_r($upload_files);
			//$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png", "pdf" => "application/pdf", "doc" => "application/msword", 'docx'=>'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'txt'	=>	'text/plain', 'xlsx'	=>	array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip'), 'xls'	=>	array('application/excel', 'application/vnd.ms-excel', 'application/msexcel'), 'ppt'	=>	array('application/powerpoint', 'application/vnd.ms-powerpoint'), 'pptx'  =>  array('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/zip', 'application/msword','application/vnd.ms-powerpoint'), , 'mp3'	=>	'audio/mp3', 'wav' => 'audio/wav', '3gp' => 'audio/3gp');	
			$allowed = array("pdf" => "application/pdf", 'mp3'	=>	'audio/mp3', 'wav' => 'audio/wav', '3gp' => 'audio/3gp');					 
			$ext = pathinfo($image[0], PATHINFO_EXTENSION);
			//echo "===".$ext;die();				
			if(!array_key_exists($ext, $allowed)){
					
					$data['imageError'] = "Upload file in pdf format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url().'complaint/add');	
				}
						
				$filesize = $_FILES['upload_file']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
				   
				   if($filesize > $maxsize) 
				   {
					   $data['imageError'] = "File size is larger than the allowed limit";
						$this->session->set_flashdata('error',$data['imageError']);
						redirect(base_url().'complaint/add');					 
				   } 
				$createdAt		=   date('Y-m-d H:i:s');
				$complaint_type = $this->input->post('complaint_type');
				$office_type 	= $this->input->post('office_type');
				$subject 		= $this->input->post('subject');
				$region 		= $this->input->post('region');
				$district 		= $this->input->post('district');
				$sro_office 	= $this->input->post('sro_office');
				$othertext 		= $this->input->post('othertext');
				$grie_details 	= $this->input->post('grievance_details');
				$relief_ed 		= $this->input->post('relief_required');
				$residentid 	= $this->input->post('residentid');
				$acknowldgement = $this->input->post('acknowldgement');
				$online_service_id = $this->input->post('office_service');
				
				$code_generate  = $this->random_strings(4);				
				
				if($residentid!=""){$resident_id = $residentid;}else{$resident_id = "";}
				if($othertext!=""){$otxt = $othertext;}else{$otxt = "";}
				if($region!=""){$rtxt = $region;}else{$rtxt = "0";}
				if($district!=""){$dtxt = $district;}else{$dtxt = "0";}
				if($sro_office!=""){$stxt = $sro_office;}else{$stxt = "0";}
				if($subject!=""){$strSub = $subject;}else{$strSub = "0";}
				if($online_service_id!=""){$osid = $online_service_id;}else{$osid = "0";}
				if($office_type!=""){$offid = $office_type;}else{$offid = "0";}
				
				if($region == 4){
					//5 = cc, 3= IGRA, 4 = hd, 
					if($complaint_type != 1){ // Specific Office						
						$users_data = $this->master_model->getRecords('adminlogin', array('region_id'=>$region, 'district_id' => $district, 'sro_id' => $sro_office, 'office_id' =>$office_type),'','');
						
						if($complaint_type==2 && count($users_data) == 0){
							$this->session->set_flashdata('error','Officer not assign for this complaint.');
							redirect(base_url().'complaint/add');
						}
						
						$user_ID = $users_data[0]['id'];
						$sro_id2  = $users_data[0]['sro_id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					} else { // Online Office
						$users_data = $this->master_model->getRecords('adminlogin', array('role_id'=> '4'),'','');
						$sro_id2 = $users_data[0]['id'];
						$user_ID = $users_data[0]['id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					}
					
				} else {
					
					if($complaint_type != 1){ // Specific Office
						
						if($complaint_type==2 && count($users_data) == 0){
							$this->session->set_flashdata('error','Officer not assign for this complaint.');
							redirect(base_url().'complaint/add');
						}
						
						$users_data = $this->master_model->getRecords('adminlogin', array('region_id'=>$region, 'district_id' => $district, 'sro_id' => $sro_office, 'office_id' =>$office_type),'','');
						$user_ID = $users_data[0]['id'];
						$sro_id2  = $users_data[0]['sro_id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					} else { // Online Office
						$users_data = $this->master_model->getRecords('adminlogin', array('role_id'=> '4'),'','');
						$sro_id2 = $users_data[0]['id'];
						$user_ID = $users_data[0]['id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
					}
				}
				
				// For Mail And SMS sending
				$getUser 	 = $this->master_model->getRecords('userregistration',array('user_id'=>$this->session->userdata('user_id')));
				$front_email = $getUser[0]['user_email'];
				$user_name   = $getUser[0]['user_name'];
				$user_mobile = $getUser[0]['user_mobile'];
				
				
				if(!empty($sro_id2)){
					$sro_id = $sro_id2;
				} else {
					$sro_id = 0;
				}			
				
				
				$dateNow		= date('Ymd');
				$complaintDate  = date('Y-m-d');	
				$complaint_code = 'COM/'.$code_generate.'/'.$dateNow;
				$insert_arr=array('comp_code'				=>	'',
								  'user_id'					=>	$userId,
								  'complaint_type_id'		=>	$complaint_type,
								  'complaint_sub_type_id'	=>	$offid,
								  'comp_subject'			=>	$strSub,
								  'comp_other'				=>	$otxt,
								  'religion_id'				=>	$rtxt,
								  'district_id'				=>	$dtxt,
								  'sro_office_id'			=>	$stxt,
								  'grievence_details'		=>	$grie_details,
								  'relief_required'			=>	$relief_ed,
								  'residentid'				=>	$resident_id,
								  'acknowldgement'			=>	$acknowldgement,
								  'complaint_date'			=>	$complaintDate,
								  'complaint_status'		=>	'1',
								  'complaint_closed_date'	=>	null,
								  'complaint_assign_to'		=>	$sro_id,
								  'complaint_updated_by'	=>	'0',
								  'status'					=>	'1',
								  'createdat'				=>	$createdAt,
								  'updatedat'				=>	$createdAt,
								  'online_service_id'		=>  $osid,
								  'r_comp_code'				=>  $r_comp_code,
								  'is_level_1'				=>  $is_level_1,
								  'is_level_2'				=>  $is_level_2,
								  'is_level_3'				=>  $is_level_3
								  );			
				  
			$complaint_add = $this->master_model->insertRecord('gri_complaint',$insert_arr);

			$last_patternid=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complaint_add),'c_id');

					if(count($last_patternid) > 0)

					{

						$last_count = 100+$last_patternid[0]['c_id']; 

					}
				  
			$pattern='GR'.date('y').'0'.$x.'00'.$last_count;

		  $this->master_model->updateRecord('gri_complaint',array('comp_code'=>$pattern),array('c_id'=>$complaint_add));
			
			// If Complaint ID get
			if($complaint_add > 0){
				
				//Images Added 
				foreach($image as $img){
					
					$img_arr=array( 'c_id' 			=> $complaint_add,
									'comp_code' 	=> $pattern,
									'image_name' 	=> $img,
									'status' 		=> '1',
									'createdat' 	=> $createdAt);
					$images_add = $this->master_model->insertRecord('gri_complaint_images',$img_arr);
				}
				
				$date_com = date('Y-m-d');
				
				if($complaint_type == 1){
					
					$endDate =  date('Y-m-d', strtotime($date_com. ' + 14 days'));
					
				} else {
					
					$endDate =  date('Y-m-d', strtotime($date_com. ' + 7 days'));
					
				}
				
				
				
				//$sro_id
				$comp_assign =array( 
									'comp_code' 	=> $pattern,
									'assign_user_id' => $user_ID,
									'assign_date' 	=> date('Y-m-d'),
									'end_date'		=> $endDate,
									'status'		=> '1',
									'createdat' 	=> $createdAt);
				$compassing = $this->master_model->insertRecord('gri_complaint_assign',$comp_assign);
				
				
					
			}	
			
			//email admin sending code 
			$info_arr=array(
					'to'=>$front_email,					
					'cc'=>$sro_email,
					'from'=>$this->config->item('MAIL_FROM'),
					'subject'=>'GRIEVANCE REDRESSAL PORTAL REGISTRATION - Complaint Register',
					'view'=>'complain_email'
					);
			
			$other_info=array('pattern'=>$pattern,'reply_status'=>'Open','msg_content'=>$grie_details,'name'=>$user_name,'user_email'=>$front_email,'user_mobile'=>$user_mobile,'user_id'=>$user_ID); 
			$emailsend=$this->email_sending->sendmail($info_arr,$other_info);
			// SMS Funtions
			$this->send_sms($user_mobile,$pattern);
			//$this->send_sms_sro($sro_mobile,$pattern);
			if($emailsend)
			{				
				$this->session->set_flashdata('success_message','Your Complaint has been registered and your Grievance ID is <strong>'.$pattern.'</strong>');
				redirect(base_url().'complaint');
			}		
			
			
		} else { 
				//echo "<pre>";
				//print_r($this->input->post());
				$createdAt		=   date('Y-m-d H:i:s');
				$complaint_type = $this->input->post('complaint_type');
				$office_type 	= $this->input->post('office_type');
				$subject 		= $this->input->post('subject');
				$region 		= $this->input->post('region');
				$district 		= $this->input->post('district');
				$sro_office 	= $this->input->post('sro_office');
				$othertext 		= $this->input->post('othertext');
				$grie_details 	= $this->input->post('grievance_details');
				$relief_ed 		= $this->input->post('relief_required');
				$residentid 	= $this->input->post('residentid');
				$acknowldgement = $this->input->post('acknowldgement');
				$online_service_id = $this->input->post('office_service');
				$code_generate  = $this->random_strings(4);
				if($subject!=""){$strSub = $subject;}else{$strSub = "0";}
				if($residentid!=""){$resident_id = $residentid;}else{$resident_id = "";}
				if($othertext!=""){$otxt = $othertext;}else{$otxt = "";}
				if($region!=""){$rtxt = $region;}else{$rtxt = "0";}
				if($district!=""){$dtxt = $district;}else{$dtxt = "0";}
				if($sro_office!=""){$stxt = $sro_office;}else{$stxt = "0";}
				if($online_service_id!=""){$osid = $online_service_id;}else{$osid = "0";}
				if($office_type!=""){$offid = $office_type;}else{$offid = "0";}
				
				
				
				
				if($region == 4){
					//5 = cc, 3= IGRA, 4 = hd, 
					if($complaint_type != 1){ // Specific Office						
						$users_data = $this->master_model->getRecords('adminlogin', array('region_id'=>$region, 'district_id' => $district, 'sro_id' => $sro_office, 'office_id' =>$office_type),'','');
						
						if($complaint_type==2 && count($users_data) == 0){
							$this->session->set_flashdata('error','Officer not assign for this complaint.');
							redirect(base_url().'complaint/add');
						}
								
						$user_ID = $users_data[0]['id'];
						$sro_id2  = $users_data[0]['sro_id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
						$sro_id_off  = $users_data[0]['sro_id'];
						
					} else { // Online Office
						$users_data = $this->master_model->getRecords('adminlogin', array('role_id'=> '4'),'','');
						$sro_id2 = $users_data[0]['id'];
						$user_ID = $users_data[0]['id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
						$sro_id_off  = $users_data[0]['sro_id'];
					}
					
				} else {
					
					if($complaint_type != 1){ // Specific Office
						$users_data = $this->master_model->getRecords('adminlogin', array('region_id'=>$region, 'district_id' => $district, 'sro_id' => $sro_office, 'office_id' =>$office_type),'','');
						if($complaint_type==2 && count($users_data) == 0){
							$this->session->set_flashdata('error','Officer not assign for this complaint.');
							redirect(base_url().'complaint/add');
						}
						
						$user_ID = $users_data[0]['id'];
						$sro_id2  = $users_data[0]['sro_id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
						$sro_id_off  = $users_data[0]['sro_id'];
					} else { // Online Office
						$users_data = $this->master_model->getRecords('adminlogin', array('role_id'=> '4'),'','');
						$sro_id2 = $users_data[0]['id'];
						$user_ID = $users_data[0]['id'];
						$sro_email = $users_data[0]['email'];
						$sro_mobile = $users_data[0]['contact_no'];
						$sro_id_off  = $users_data[0]['sro_id'];
					}
				}				
				
				
				// For Mail And SMS sending
				$getUser 	= $this->master_model->getRecords('userregistration',array('user_id'=>$this->session->userdata('user_id')));
				$front_email = $getUser[0]['user_email'];
				$user_name = $getUser[0]['user_name'];
				$user_mobile = $getUser[0]['user_mobile'];
				if(!empty($sro_id2)){
					$sro_id = $sro_id2;
				} else {
					$sro_id = 0;
				}	
				
				$dateNow		= date('Ymd');
				$complaintDate  = date('Y-m-d');	
				$complaint_code = 'COM/'.$code_generate.'/'.$dateNow;
				$insert_arr=array('comp_code'				=>	'',
								  'user_id'					=>	$userId,
								  'complaint_type_id'		=>	$complaint_type,
								  'complaint_sub_type_id'	=>	$offid,
								  'comp_subject'			=>	$strSub,
								  'comp_other'				=>	$otxt,
								  'religion_id'				=>	$rtxt,
								  'district_id'				=>	$dtxt,
								  'sro_office_id'			=>	$stxt,
								  'grievence_details'		=>	$grie_details,
								  'relief_required'			=>	$relief_ed,
								  'residentid'				=>	$resident_id,
								  'acknowldgement'			=>	$acknowldgement,
								  'complaint_date'			=>	$complaintDate,
								  'complaint_status'		=>	'1',
								  'complaint_closed_date'	=>	null,
								  'complaint_assign_to'		=>	$sro_id,
								  'complaint_updated_by'	=>	'0',
								  'status'					=>	'1',
								  'createdat'				=>	$createdAt,
								  'updatedat'				=>	$createdAt,
								  'online_service_id'		=>  $osid,
								  'r_comp_code'				=>  $r_comp_code,
								  'is_level_1'				=>  $is_level_1,
								  'is_level_2'				=>  $is_level_2,
								  'is_level_3'				=>  $is_level_3
								  );
			 						
			
			$complaint_add = $this->master_model->insertRecord('gri_complaint',$insert_arr);
			//echo $this->db->last_query();die();	  
					$last_patternid=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complaint_add),'c_id');

					if(count($last_patternid) > 0)

					{

						$last_count = 100+$last_patternid[0]['c_id']; 

					}

				  
				  $pattern='GR'.date('y').'0'.$x.'00'.$last_count;
				  
				  if($complaint_add > 0){
					  
					//$date_com = date('Y-m-d');
					//$endDate =  date('Y-m-d', strtotime($date_com. ' + 7 days'));
					
					$date_com = date('Y-m-d');
				
					if($complaint_type == 1){
						
						$endDate =  date('Y-m-d', strtotime($date_com. ' + 14 days'));
						
					} else {
						
						$endDate =  date('Y-m-d', strtotime($date_com. ' + 7 days'));
						
					}					
					
					$comp_assign = array( 
											'comp_code' 	=> $pattern,
											'assign_user_id' => $user_ID,
											'assign_date' 	=> date('Y-m-d'),
											'end_date'		=> $endDate,
											'status'		=> '1',
											'createdat' 	=> $createdAt);
					//print_r($comp_assign);					
					$compassing = $this->master_model->insertRecord('gri_complaint_assign',$comp_assign);
					 	
				 }
				 
				 
			//email admin sending code 
			$info_arr=array(
					'to'=>$front_email,					
					'cc'=>$sro_email,
					'from'=>$this->config->item('MAIL_FROM'),
					'subject'=>'GRIEVANCE REDRESSAL PORTAL REGISTRATION - Complaint Register',
					'view'=>'complain_email'
					);

			$other_info=array('pattern'=>$pattern,'reply_status'=>'Open','msg_content'=>$grie_details,'name'=>$user_name,'user_email'=>$front_email,'user_mobile'=>$user_mobile,'user_id'=>$user_ID); 
			$emailsend=$this->email_sending->sendmail($info_arr,$other_info);
			
			// SMS Funtions
			$this->send_sms($user_mobile,$pattern);
			//$this->send_sms_sro($sro_mobile,$pattern);
			
			if($emailsend)
			{
				$this->master_model->updateRecord('gri_complaint',array('comp_code'=>$pattern),array('c_id'=>$complaint_add));
				$this->session->set_flashdata('success_message','Your Complaint has been registered and your Grievance ID is <strong>'.$pattern.'</strong>');
				redirect(base_url().'complaint');
			}
			
		}
			
		
	}
		
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		  //$this->session->unset_userdata("mycaptcha");
	    $_SESSION["complaintcaptcha"]=$cap['word'];
		
		$data['complaint_type_list']=$this->master_model->getRecords('gri_complaint_type','','',array('complaint_type_id'=>'DESC'));
		$data['middlecontent']='complaint/add';	
	 	$this->load->view('superadmintemplate',$data);
	}
	
	public function refComplaint(){
		//$this->session->unset_userdata('p_comp_code');
		$userId = $this->session->userdata('user_id');
		$ref_code = $this->session->userdata('p_comp_code');
		$x=1;		
		if($ref_code == ""){
			redirect(base_url().'home/notifyigra');
		}
		$data['imageError'] = "";	
		
		$this->load->library('upload');	
		
		$complaint_details 	= $this->master_model->getRecords('gri_complaint',array('comp_code'=>$ref_code));
		$c_user_id 			= $complaint_details[0]['user_id'];
		$c_type_id 			= $complaint_details[0]['complaint_type_id'];
		$c_sub_type_id 		= $complaint_details[0]['complaint_sub_type_id'];
		$c_subject 			= $complaint_details[0]['comp_subject'];
		$c_other 			= $complaint_details[0]['comp_other'];
		$c_religion_id		= $complaint_details[0]['religion_id'];
		$c_district_id		= $complaint_details[0]['district_id'];
		$c_sro_office_id	= $complaint_details[0]['sro_office_id'];
		$c_online_ser_id	= $complaint_details[0]['online_service_id'];
		$c_assign_to		= $complaint_details[0]['complaint_assign_to'];
		
				
		$c_query 	= "SELECT assign_user_id, end_date FROM gri_complaint_assign WHERE comp_code = '".$ref_code."' AND status = '1' order by id DESC LIMIT 1";
		$c_result 	= $this->db->query($c_query)->result();		
		$admin_pid	= $c_result[0]->assign_user_id;
		
		$c_admin 	= $this->master_model->getRecords('adminlogin',array('id'=>$admin_pid));
		$admin_role = $c_admin[0]['role_id'];
		$admin_region = $c_admin[0]['region_id'];
		$admin_district = $c_admin[0]['district_id'];
		
		$is_level_1 = '0';
		$is_level_2 = '0';
		$is_level_3 = '0';
		
		
		if($admin_role == 1 || $admin_role == 7){
			
			if($c_religion_id == 4 ){
				
				if($c_sub_type_id == 2){
					
					$c_jdr_admin 	= $this->master_model->getRecords('adminlogin',array('region_id'=>$admin_region, 'district_id' => $admin_district, 'role_id' => '9'));
				
				} else {
					
					$c_jdr_admin 	= $this->master_model->getRecords('adminlogin',array('region_id'=>$admin_region, 'district_id' => $admin_district, 'role_id' => '2'));
				}
				
				$is_level_1 	= '0';
				$is_level_2 	= '1';
				$is_level_3 	= '0';
				$c_admin_id  	= $c_jdr_admin[0]['id'];
				$c_admin_type  	= $c_jdr_admin[0]['role_id'];
				$c_admin_name  	= $c_jdr_admin[0]['namecc'];
				$c_admin_email  = $c_jdr_admin[0]['email'];
				$c_admin_phone  = $c_jdr_admin[0]['contact_no'];
				
				$r_role 	= "SELECT role_name FROM gri_roles WHERE role_id = '".$c_admin_type."'";
				$r_result 	= $this->db->query($r_role)->result();		
				$role_name	= $r_result[0]->role_name;
				$data['role_name']		= $role_name;
				
			} else {
				
				$is_level_1 	= '0';
				$is_level_2 	= '1';
				$is_level_3 	= '0';
				
				$c_jdr_admin 	= $this->master_model->getRecords('adminlogin',array('region_id'=>$admin_region, 'district_id' => $admin_district, 'role_id' => $admin_role));
			    $c_admin_id  	= $c_jdr_admin[0]['id'];
				$c_admin_name  	= $c_jdr_admin[0]['namecc'];
				$c_admin_type  	= $c_jdr_admin[0]['role_id'];
				$c_admin_email  = $c_jdr_admin[0]['email'];
				$c_admin_phone  = $c_jdr_admin[0]['contact_no'];
				
				$r_role 	= "SELECT role_name FROM gri_roles WHERE role_id = '".$c_admin_type."'";
				$r_result 	= $this->db->query($r_role)->result();		
				$role_name	= $r_result[0]->role_name;
				$data['role_name']		= $role_name;
				
			}
			
			//print_r($c_jdr_admin);exit;
			
			$date_com = date('Y-m-d');
			$endDate =  date('Y-m-d', strtotime($date_com. ' + 10 days'));
			
		} else if($admin_role == 2 || $admin_role == 9 || $admin_role == 4){
			
			$is_level_1 	= '0';
			$is_level_2 	= '0';
			$is_level_3 	= '1';
			$c_igra_admin 	= $this->master_model->getRecords('adminlogin',array('id' => 8));
			$c_admin_id   	= $c_igra_admin[0]['id'];
			$c_admin_name  	= $c_igra_admin[0]['namecc'];
			$c_admin_type  	= $c_igra_admin[0]['role_id'];
			$c_admin_email  = $c_igra_admin[0]['email'];
			$c_admin_phone  = $c_igra_admin[0]['contact_no'];
			$date_com = date('Y-m-d');
			$endDate =  date('Y-m-d', strtotime($date_com. ' + 15 days'));	
			
			$r_role 	= "SELECT role_name FROM gri_roles WHERE role_id = '".$c_admin_type."'";
			$r_result 	= $this->db->query($r_role)->result();		
			$role_name	= $r_result[0]->role_name;
			$data['role_name']		= $role_name;
			
		} 
		
		// For Mail And SMS sending
		$getUser 	= $this->master_model->getRecords('userregistration',array('user_id'=>$this->session->userdata('user_id')));
		$front_email = $getUser[0]['user_email'];
		$user_name = $getUser[0]['user_name'];
		$user_mobile = $getUser[0]['user_mobile'];
		
		$this->form_validation->set_rules('grievance_details', 'Grievance Details', 'trim|required|xss_clean');
		$this->form_validation->set_rules('acknowldgement', 'Acknowledgement', 'required|xss_clean');
		$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_complaint');
		
		
		////////////////////////////////////////////
		
		if($this->form_validation->run())
		{	
			
			
		if($_FILES['upload_file']['name']!=""){			
			
			$config['upload_path']      = './images/complaint_images';
			$config['allowed_types']    = '*';  
			$config['max_size']         = '5000';         
			$config['encrypt_name']     = TRUE;
						 
			$upload_files  = @$this->master_model->upload_file('upload_file', $_FILES, $config, FALSE);
		    $image = "";
			if(isset($upload_files[0]) && !empty($upload_files[0])){
				
				$image = $upload_files[0];
					
			} 			
			
			$allowed = array("pdf" => "application/pdf", 'mp3'	=>	'audio/mp3', 'wav' => 'audio/wav', '3gp' => 'audio/3gp');					 
			$ext = pathinfo($image[0], PATHINFO_EXTENSION);
			//echo "===".$ext;die();				
			if(!array_key_exists($ext, $allowed)){
					
					$data['imageError'] = "Upload file in pdf format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url().'complaint/add');	
				}
						
				$filesize = $_FILES['upload_file']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
				   
				   if($filesize > $maxsize) 
				   {
					   $data['imageError'] = "File size is larger than the allowed limit";
						$this->session->set_flashdata('error',$data['imageError']);
						redirect(base_url().'complaint/add');					 
				   } 
				$createdAt		=   date('Y-m-d H:i:s');				
				$grie_details 	= $this->input->post('grievance_details');
				$relief_ed 		= $this->input->post('relief_required');
				$acknowldgement = $this->input->post('acknowldgement');
				$residentid		= '';
				$code_generate  = $this->random_strings(4);					
				
				$dateNow		= date('Ymd');
				$complaintDate  = date('Y-m-d');	
				$complaint_code = 'COM/'.$code_generate.'/'.$dateNow;
				$insert_arr=array('comp_code'				=>	'',
								  'user_id'					=>	$userId,
								  'complaint_type_id'		=>	$c_type_id,
								  'complaint_sub_type_id'	=>	$c_sub_type_id,
								  'comp_subject'			=>	$c_subject,
								  'comp_other'				=>	$c_other,
								  'religion_id'				=>	$c_religion_id,
								  'district_id'				=>	$c_district_id,
								  'sro_office_id'			=>	$c_sro_office_id,
								  'grievence_details'		=>	$grie_details,
								  'relief_required'			=>	$relief_ed,
								  'residentid'				=>	$residentid,
								  'acknowldgement'			=>	$acknowldgement,
								  'complaint_date'			=>	$complaintDate,
								  'complaint_status'		=>	'1',
								  'complaint_closed_date'	=>	null,
								  'complaint_assign_to'		=>	$c_assign_to,
								  'complaint_updated_by'	=>	'0',
								  'status'					=>	'1',
								  'createdat'				=>	$createdAt,
								  'updatedat'				=>	$createdAt,
								  'online_service_id'		=>  $c_online_ser_id,
								  'r_comp_code'				=>  $ref_code,
								  'is_level_1'				=>  $is_level_1,
								  'is_level_2'				=>  $is_level_2,
								  'is_level_3'				=>  $is_level_3
								  );			
				  
			$complaint_add = $this->master_model->insertRecord('gri_complaint',$insert_arr);

			$last_patternid=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complaint_add),'c_id');

					if(count($last_patternid) > 0)

					{

						$last_count = 100+$last_patternid[0]['c_id']; 

					}
				  
			$pattern='GR'.date('y').'0'.$x.'00'.$last_count;

		  $this->master_model->updateRecord('gri_complaint',array('comp_code'=>$pattern),array('c_id'=>$complaint_add));
			
			// If Complaint ID get
			if($complaint_add > 0){
				
				//Images Added 
				foreach($image as $img){
					
					$img_arr=array( 'c_id' 			=> $complaint_add,
									'comp_code' 	=> $pattern,
									'image_name' 	=> $img,
									'status' 		=> '1',
									'createdat' 	=> $createdAt);
					$images_add = $this->master_model->insertRecord('gri_complaint_images',$img_arr);
				}
				
				
				
				
				
				//$sro_id
				$comp_assign =array( 
									'comp_code' 	 => $pattern,
									'assign_user_id' => $c_admin_id,
									'assign_date' 	 => date('Y-m-d'),
									'end_date'		 => $endDate,
									'status'		 => '1',
									'createdat' 	 => $createdAt);
				$compassing = $this->master_model->insertRecord('gri_complaint_assign',$comp_assign);
				
				
					
			}	
			
			//email admin sending code 
			$info_arr=array(
					'to'=>$front_email,					
					'cc'=>$c_admin_email,
					'from'=>$this->config->item('MAIL_FROM'),
					'subject'=>'GRIEVANCE REDRESSAL PORTAL REGISTRATION - Complaint Register',
					'view'=>'complain_email'
					);
			
			$other_info=array('pattern'=>$pattern,'reply_status'=>'Open','msg_content'=>$grie_details,'name'=>$user_name,'user_email'=>$front_email,'user_mobile'=>$user_mobile,'user_id'=>$user_ID); 
			$emailsend=$this->email_sending->sendmail($info_arr,$other_info);
			// SMS Funtions
			//$this->send_sms($user_mobile,$pattern);
			//$this->send_sms_sro($sro_mobile,$pattern);
			if($emailsend)
			{	
				$this->session->unset_userdata('p_comp_code');
				$this->session->set_flashdata('success_message','Your Complaint has been registered and your Grievance ID is <strong>'.$pattern.'</strong>');
				redirect(base_url().'complaint');
			}		
			
			
		} else { 
				
				$createdAt		=   date('Y-m-d H:i:s');
				
				$grie_details 	= $this->input->post('grievance_details');
				$relief_ed 		= $this->input->post('relief_required');
				$acknowldgement = $this->input->post('acknowldgement');
				$residentid		= '';
				$code_generate  = $this->random_strings(4);			
				
				
				$dateNow		= date('Ymd');
				$complaintDate  = date('Y-m-d');	
				$complaint_code = 'COM/'.$code_generate.'/'.$dateNow;
				$insert_arr=array('comp_code'				=>	'',
								  'user_id'					=>	$userId,
								  'complaint_type_id'		=>	$c_type_id,
								  'complaint_sub_type_id'	=>	$c_sub_type_id,
								  'comp_subject'			=>	$c_subject,
								  'comp_other'				=>	$c_other,
								  'religion_id'				=>	$c_religion_id,
								  'district_id'				=>	$c_district_id,
								  'sro_office_id'			=>	$c_sro_office_id,
								  'grievence_details'		=>	$grie_details,
								  'relief_required'			=>	$relief_ed,
								  'residentid'				=>	$residentid,
								  'acknowldgement'			=>	$acknowldgement,
								  'complaint_date'			=>	$complaintDate,
								  'complaint_status'		=>	'1',
								  'complaint_closed_date'	=>	null,
								  'complaint_assign_to'		=>	$c_assign_to,
								  'complaint_updated_by'	=>	'0',
								  'status'					=>	'1',
								  'createdat'				=>	$createdAt,
								  'updatedat'				=>	$createdAt,
								  'online_service_id'		=>  $c_online_ser_id,
								  'r_comp_code'				=>  $ref_code,
								  'is_level_1'				=>  $is_level_1,
								  'is_level_2'				=>  $is_level_2,
								  'is_level_3'				=>  $is_level_3
								  );			
			 						
			
			$complaint_add = $this->master_model->insertRecord('gri_complaint',$insert_arr);
			//echo $this->db->last_query();die();	  
					$last_patternid=$this->master_model->getRecords('gri_complaint',array('c_id'=>$complaint_add),'c_id');

					if(count($last_patternid) > 0)

					{

						$last_count = 100+$last_patternid[0]['c_id']; 

					}

				  
				  $pattern='GR'.date('y').'0'.$x.'00'.$last_count;
				  
				  if($complaint_add > 0){					
										
					
					$comp_assign = array( 
											'comp_code' 	=> $pattern,
											'assign_user_id' => $c_admin_id,
											'assign_date' 	=> date('Y-m-d'),
											'end_date'		=> $endDate,
											'status'		=> '1',
											'createdat' 	=> $createdAt);
									
					$compassing = $this->master_model->insertRecord('gri_complaint_assign',$comp_assign);
					 	
				 }
				 
				 
			//email admin sending code 
			$info_arr=array(
					'to'=>$front_email,					
					'cc'=>$c_admin_email,
					'from'=>$this->config->item('MAIL_FROM'),
					'subject'=>'GRIEVANCE REDRESSAL PORTAL REGISTRATION - Complaint Register',
					'view'=>'complain_email'
					);

			$other_info=array('pattern'=>$pattern,'reply_status'=>'Open','msg_content'=>$grie_details,'name'=>$user_name,'user_email'=>$front_email,'user_mobile'=>$user_mobile,'user_id'=>$user_ID); 
			$emailsend=$this->email_sending->sendmail($info_arr,$other_info);
			
			// SMS Funtions
			//$this->send_sms($user_mobile,$pattern);
			//$this->send_sms_sro($sro_mobile,$pattern);
			
			if($emailsend)
			{
				$this->session->unset_userdata('p_comp_code');
				$this->master_model->updateRecord('gri_complaint',array('comp_code'=>$pattern),array('c_id'=>$complaint_add));
				$this->session->set_flashdata('success_message','Your Complaint has been registered and your Grievance ID is <strong>'.$pattern.'</strong>');
				redirect(base_url().'complaint');
			}
			
		}
			
		
	}
		
	
		////////////////////////////////////////////

		
		//$this->session->unset_userdata('p_comp_code');		
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];		
	      $_SESSION["complaintcaptcha"]=$cap['word'];
		
		$data['complaint_type_list']=$this->master_model->getRecords('gri_complaint_type','','',array('complaint_type_id'=>'DESC'));
		$data['middlecontent']='complaint/refcomplaint';	
	 	$this->load->view('template',$data);
			
	}
	
	public function complaint_detail($id){
		
		if(isset($_POST['report_to_next']) && !empty($_POST['report_to_next'])) 
		{
			$comp_code = $this->input->post('c_code');
			$_SESSION['p_comp_code'] = $comp_code;
			$this->session->set_userdata($comp_code);
			redirect(base_url().'complaint/refComplaint');
		}
		$this->session->unset_userdata('p_comp_code');
		$data['complaint_details']=$this->master_model->getRecords('gri_complaint',array('c_id' => $id),'','');		
		$data['middlecontent']='complaint/details';	
	 	$this->load->view('template',$data);
	}

	public function refresh(){
        // Captcha configuration
        $this->load->helper('captcha');
        $config = array(
            'img_path' => './images/captcha/',
			'img_url' => base_url().'images/captcha/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('complaintcaptcha');
        $this->session->set_userdata('complaintcaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}	
	
	public function check_captcha_complaint($code) 
	{
	  // check if captcha is set -
		if($code == '' || $_SESSION["complaintcaptcha"] != $code )
		{ 
			$this->form_validation->set_message('check_captcha_complaint', 'Invalid %s.'); 
			$this->session->set_userdata("complaintcaptcha", rand(1,100000));
			
			return false;
		}
		if($_SESSION["complaintcaptcha"] == $code)
		{
			$this->session->unset_userdata("complaintcaptcha");
			return true;
		}
		
	}
	
	public function viewdetails(){
		$id = $this->input->post('id');
		$complaint_details = $this->master_model->getRecords('gri_complaint',array('c_id'=>$id),'',array('complaint_type_id'=>'DESC'));
		$str = '<div id="stripedTable" class="body">
					<p>'.$complaint_details[0]['grievence_details'].'</p>
				</div>';				
		echo $str;
	}
	
	
	
	public function details(){
		$this->session->unset_userdata('p_comp_code');
		error_reporting(0);
		$id = $this->input->post('id');
		$complaint_details = $this->master_model->getRecords('gri_complaint',array('c_id'=>$id),'',array('complaint_type_id'=>'DESC'));
		
		$userid  = $complaint_details[0]['user_id'];
		$comp_typeid = $complaint_details[0]['complaint_type_id'];
		$comp_code = $complaint_details[0]['comp_code'];
		$compsub_id = $complaint_details[0]['complaint_sub_type_id'];
		$sub_id = $complaint_details[0]['comp_subject'];
		$reg_id = $complaint_details[0]['religion_id'];
		$d_id = $complaint_details[0]['district_id'];
		$s_off_id = $complaint_details[0]['sro_office_id'];
		$gdetail = $complaint_details[0]['grievence_details'];
		$relief = $complaint_details[0]['relief_required'];
		$compDate = $complaint_details[0]['complaint_date'];
		
		$compStatus = $complaint_details[0]['reply_status'];
		if($compStatus == 0){$cStatus = "Closed";}else{$cStatus = "Open";}



		$compClosedate = $complaint_details[0]['complaint_closed_date'];
		$compAssign = $complaint_details[0]['complaint_assign_to'];
		$compCreateDate = $complaint_details[0]['createdat'];
		$complaint_type = $this->master_model->getRecords('gri_complaint_type',array('complaint_type_id'=>$comp_typeid),'','');
		$complaint_subtype = $this->master_model->getRecords('gri_complaint_sub_type',array('complaint_sub_type_id'=>$compsub_id),'','');
		$userDetails = $this->master_model->getRecords('userregistration',array('user_id'=>$userid),'','');

		
		if($sub_id == "-1"){
			$compSub = 'Other';
			$otherDe = $complaint_details[0]['comp_other'];
			$td = '<tr>
						<td><b>Other</b></td>
						<td>'.$otherDe.'</td>
					</tr>';
		} else {
			$comp_subject = $this->master_model->getRecords('gri_subject',array('sub_id'=>$sub_id),'','');
			$compSub = $comp_subject[0]['subject_name'];
			$td = '';
		}
		
		$reply_tr='';
		$replymsg=$this->master_model->getRecords('complainreply',array('complain_id'=>$id),'',array('reply_id'=>'DESC'));
		
         if(count($replymsg) > 0)
         {
        	foreach($replymsg as $reply)
          {
          	$repliedby=$this->master_model->getRecords('adminlogin',array('id'=>$reply['user_id']));

          	$reply_tr.= '<tr>
						<td><b>Replied Message</b></td>
						<td>'.$reply['reply_msg'].'</td>
						</tr>
						<tr>
						<td><b>Replied By</b></td>
						<td>'.ucfirst($repliedby[0]['adminuser']).'</td>
						</tr>';

         	 }
  		}

		$fileShow =$this->master_model->getRecords('gri_complaint_images',array('comp_code'=>$comp_code));
		$imgShow = "";
		if($fileShow[0]['image_name']!=""){
			$imgShow = '<tr>
						<td><b>View File</b></td>
						<td><a href="'.base_url('/images/complaint_images/'.$fileShow[0]['image_name']).'" target="_blank">Download</a></td>
						</tr>';
		} 
		$comp_region = $this->master_model->getRecords('gri_region_division',array('religion_id'=>$reg_id),'','');
		$comp_district = $this->master_model->getRecords('gri_district',array('district_id'=>$reg_id),'','');
		$comp_spo_office = $this->master_model->getRecords('gri_sro_offices',array('sro_office_id'=>$s_off_id),'','');
		//$comp_user = $this->master_model->getRecords('userregistration',array('user_id'=>$userid),'','');
		$str = '<div id="stripedTable" class="body">
					<table class="table table-striped responsive-table">							
						<tbody>								
							<tr>
								<td><b>Complaint Code</b></td>
								<td>'.$comp_code.'</td>
							</tr>
							<tr>
								<td><b>Name</b></td>
								<td>'.$userDetails[0]['user_name'].'</td>
							</tr>
							<tr>
								<td><b>Complaint Belongs To</b></td>
								<td>'.$complaint_type[0]['complaint_type_name'].'</td>
							</tr>
							<tr>
								<td><b>Office Type</b></td>
								<td>'.$complaint_subtype[0]['complaint_sub_type_name'].'</td>
							</tr>
							<tr>
								<td><b>Region</b></td>
								<td>'.$comp_region[0]['region_division_name'].'</td>
							</tr>
							<tr>
								<td><b>District</b></td>
								<td>'.$comp_district[0]['district_name'].'</td>
							</tr>
							<tr>
								<td><b>Office Name</b></td>
								<td>'.$comp_spo_office[0]['office_name'].'</td>
							</tr>
							<tr>
								<td><b>Subject</b></td>
								<td>'.$compSub.'</td>
							</tr>';
						$str .= $td;
						$str .= '<tr>
								<td><b>Grievance Details</b></td>
								<td>'.$gdetail.'</td>
							</tr>
							<tr>
								<td><b>Relief</b></td>
								<td>'.$relief.'</td>
							</tr>
							<tr>
								<td><b>Complaint Date</b></td>
								<td>'.$compDate.'</td>
							</tr>
							<tr>
								<td><b>Complaint Status</b></td>
								<td>'.$cStatus.'</td>
							</tr>';
						$str .= $imgShow;	
						$str .= $reply_tr;
						$str .='</tbody>                
					</table>
				</div>';
				
		echo $str;
		
	}
	
	public function officer_list(){
		echo $str = "No Officer Available";
	}
	
	public function delete()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'complaint');
		}
		if($this->master_model->deleteRecord('gri_complaint','c_id',$id))
		{
		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'complaint');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'complaint');
		}
	
	}
	
	public function assignOther(){
		$this->session->unset_userdata('p_comp_code');
		$comp_code = $this->input->post('id');
		
		$getDetails =$this->master_model->getRecords('gri_complaint_assign',array('comp_code'=>$comp_code, 'status' => 1, 'assign_user_id != ' => '8'));

		$getAssignuser  = $this->master_model->getRecords('adminlogin',array('role_id'=>'3', 'status'=>'1'));
		$igraID 		= $getAssignuser[0]['id'];
		$igraemail		= $getAssignuser[0]['email'];
		$igraname		= $getAssignuser[0]['namecc'];

		// For Mail And SMS sending
		$getUser 	 = $this->master_model->getRecords('userregistration',array('user_id'=>$this->session->userdata('user_id')));
		$front_email = $getUser[0]['user_email'];
		$user_name   = $getUser[0]['user_name'];
		$user_mobile = $getUser[0]['user_mobile'];

		
		if(count($getDetails) > 0){
			
			$update_arr = array('status' => 0);
			$this->master_model->updateRecord('gri_complaint_assign',$update_arr,array('comp_code'=>"'".$comp_code."'"));
 			
			// Assign Days
			$date_com 	= date('Y-m-d');
			$endDate 	= date('Y-m-d', strtotime($date_com. ' + 17 days'));
			$createdAt	= date('Y-m-d H:i:s');
			$current_date = date('Y-m-d 00:00:00');
			
			// Insert Array
			$insert_arr = array('comp_code' 		=> $comp_code,
								'assign_user_id' 	=> $igraID,
								'assign_date' 		=> $current_date,
								'end_date' 			=> $endDate,
								'status' 			=> '1',
								'createdat' 		=> $createdAt,
								'updatedat' 		=> $createdAt);	
								
			// Assign To IGRA
			$igrQuery  = $this->master_model->insertRecord('gri_complaint_assign',$insert_arr);	
			if ($igrQuery) {
				

				//send email and sms to user 
				$info_arr=array(
				'to'=>$front_email,	
				'cc'=>'',				
				'from'=>$this->config->item('MAIL_FROM'),
				'subject'=>'GRIEVANCE REDRESSAL PORTAL - Complaint Reported to IGRA',
				'view'=>'complain_report_igr'
				);

				$other_info=array('pattern'=>$comp_code,'reply_status'=>'Open','msg_content'=>$grie_details,'name'=>$user_name,'user_email'=>$front_email,'user_mobile'=>$user_mobile,'user_id'=>$user_ID); 
				$emailsend=$this->email_sending->sendmail($info_arr,$other_info);
				
				$this->send_sms_on_report_igr($user_mobile,$comp_code);

				//send email and sms to igra 

				$info_arr=array(
				'to'=>$igraemail,	
				'cc'=>'',				
				'from'=>$this->config->item('MAIL_FROM'),
				'subject'=>'GRIEVANCE REDRESSAL PORTAL - Complaint Reported to IGRA',
				'view'=>'complain_report_igr'
				);

				$other_info=array('pattern'=>$comp_code,'reply_status'=>'Open','msg_content'=>$grie_details,'name'=>$user_name,'user_email'=>$front_email,'user_mobile'=>$user_mobile,'user_id'=>$user_ID); 
				$emailsend=$this->email_sending->sendmail($info_arr,$other_info);
				
				$this->send_sms_on_report_igr($user_mobile,$comp_code);


			}
			return TRUE;
			
		} // Count End
		
	} // Function End
	
	
	function send_sms($mobile=NULL,$pattern=NULL)
 	{

 		if($mobile!=NULL && $pattern!=NULL)
 		{
 			$text = "Your Grievance has been successfully submitted. Grievance Number: ".$pattern;
 			$msg = urlencode($text);
 			$url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH";

 			$string = preg_replace('/\s+/', '', $url);
 			$x = curl_init($string);
 			curl_setopt($x, CURLOPT_HEADER, 0);	
 			curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
 			curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);	
 			curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);		
 			$reply = curl_exec($x);		
 		
 		}
 	}

 	function send_sms_on_report_igr($mobile=NULL,$pattern=NULL)
 	{

 		if($mobile!=NULL && $pattern!=NULL)
 		{
 			$text = "Grievance has been reportrd to IGRA. Grievance Number: ".$pattern;
 			$msg = urlencode($text);
 			$url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH";

 			$string = preg_replace('/\s+/', '', $url);
 			$x = curl_init($string);
 			curl_setopt($x, CURLOPT_HEADER, 0);	
 			curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
 			curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);	
 			curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);		
 			$reply = curl_exec($x);		
 		
 		}
 	}

 	function send_sms_sro($mobile=NULL,$pattern=NULL)
 	{

 		if($mobile!=NULL && $pattern!=NULL)
 		{
 			$text = "New Grievance added on IGR Redressal System. Grievance Number: ".$pattern;
 			$msg = urlencode($text);
 			$url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH";

 			$string = preg_replace('/\s+/', '', $url);
 			$x = curl_init($string);
 			curl_setopt($x, CURLOPT_HEADER, 0);	
 			curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
 			curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);	
 			curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);		
 			$reply = curl_exec($x);
 		
 		}
 	}
	
} // Class End

?>