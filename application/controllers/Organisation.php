<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Organisation extends CI_Controller {
	function __construct()
	{
		 parent::__construct();
		 

	}

		public function index()
		{
			
			 if (!empty($_SERVER['HTTP_CLIENT_IP']))   
			  {
				$ip_address = $_SERVER['HTTP_CLIENT_IP'];
			  }
			//whether ip is from proxy
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
			  {
				$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
			  }
			//whether ip is from remote address
			else
			  {
				$ip_address = $_SERVER['REMOTE_ADDR'];
			  }
			  
			$createdAt		=   date('Y-m-d'); 
				
			
			$getIPexist					= $this->master_model->getRecords('gri_visitors',array('ip_address'=>$ip_address));
			if(count($getIPexist) == 0):
				$insert_arr	=	array(	'ip_address'	=>	$ip_address,
										'created_at'	=>	$createdAt
										);					  
				$visitorCount = $this->master_model->insertRecord('gri_visitors',$insert_arr);
			endif;
			
			$data['middlecontent']='organisation/index';
			$data['title']='Organisation';	
			$data['subtitle']='Organisation';
			$this->load->view('template',$data);
			
			

		}	
		
		
		public function vision()
		{
			$data['subtitle']='Vision';	
			$data['middlecontent']='organisation/vision';			
			$this->load->view('template',$data);		

		}
		
		public function history()
		{
			$data['subtitle']='History';
			$data['middlecontent']='organisation/history';			
			$this->load->view('template',$data);		

		}
		
		public function structure()
		{
			$data['subtitle']='Structure';
			$data['middlecontent']='organisation/structure';			
			$this->load->view('template',$data);		

		}
		
		public function office()
		{
			$data['subtitle']='Office';
			$data['middlecontent']='organisation/offices';			
			$this->load->view('template',$data);		

		}
		
		public function functions()
		{
			$data['subtitle']='Functions';
			$data['middlecontent']='organisation/functions';			
			$this->load->view('template',$data);		

		}
		
		public function orders()
		{
			$data['subtitle']='Orders';
			$data['middlecontent']='organisation/orders';			
			$this->load->view('template',$data);		

		}
	 
}

	

/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
