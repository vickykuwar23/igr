<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Callcenterpanel extends CI_Controller 

{

   function __construct()

	{
		 parent::__construct();
		 $this->load->model('chk_session');
		 $this->load->model('master_model');
		 $this->chk_session->chk_callcenter_session();
	}

	
	public function users()

	{   
		$data['users']=$this->master_model->getRecords('userregistration','','',array('created_at'=>'DESC'));
		$data['middlecontent']='userlist';
	 	$this->load->view('admintemplate',$data);
	}

	function register_new_user()
	{
		$data['error_msg']=$data['file_error']='';
		$state=$city='';$emailsend='';$email='';
		
		if(isset($_POST['btn_reg']))
		{
	       $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|valid_email|is_unique[userregistration.user_email]');
			$this->form_validation->set_rules('mobile', 'Mobile No.', 'trim|required|xss_clean|min_length[10]|numeric|is_unique[userregistration.user_mobile]');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|is_unique[userregistration.uniqueusername]|alpha_numeric');
			$this->form_validation->set_rules('address1', 'Address', 'trim|required|xss_clean');
			$this->form_validation->set_rules('state', 'State', 'trim|required|xss_clean');
			$this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('pincode', 'Pincode', 'trim|required|xss_clean|numeric');	
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');	
	     	$this->form_validation->set_message('is_unique', 'The %s already exists');

			if($this->form_validation->run())

			{
				$name=htmlentities($this->input->post('name'));
				$uniqueusername=htmlentities($this->input->post('username'));
				$email=htmlentities($this->input->post('email'));
				$mobile=htmlentities($this->input->post('mobile'));
				$landline=htmlentities($this->input->post('landline'));
				$address1=htmlentities($this->input->post('address1'));
				$address2=htmlentities($this->input->post('address2'));
				$state=htmlentities($this->input->post('state'));
				$city=htmlentities($this->input->post('city'));
				$pincode=htmlentities($this->input->post('pincode'));
				$user_pass=htmlentities($this->input->post('password'));
				$insert_arr= array('user_name'=>$name,
								  'uniqueusername'=>$uniqueusername,
	                              'user_email'=>$email,
								  'user_mobile'=>$mobile,
								  'user_landline'=>$landline,
								  'user_address1'=>$address1,
								  'user_address2'=>$address2,
								  'user_state'=>$state,
								  'user_city'=>$city,
								  'user_pincode'=>$pincode,
								  'user_pass'=>md5($user_pass)
								);

			if($insert_id=$this->master_model->insertRecord('userregistration',$insert_arr,true))

			{
				if($email!='')
				{
					$admin_email=$this->master_model->getRecords('adminlogin');	
					
					$info_arr=array(
						'to'      =>$email,
						'cc'      =>'',
						'from'    =>$this->config->item('MAIL_FROM'),
						'subject' =>'GRIEVANCE REDRESSAL PORTAL REGISTRATION',
						'view'    =>'register-email'
					);

					$other_info=array('name'=>$name,
									  'user_id'=>$insert_id,
									  'password'=>$user_pass
									);
					
					$emailsend = $this->email_sending->sendmail($info_arr,$other_info);
					
					if($emailsend)
					{
						$verify = array('emailverify'=>$email,'login_time'=>time());
                        $this->session->set_userdata($verify);			
						$this->session->set_flashdata('success_message','User registration .');
				    	redirect(base_url().'callcenterpanel/add_grevience/'.$insert_id);
				    	exit;
					}
				}

		    }
		   }		
		}
	
		$data['state']=$this->master_model->getRecords('states',array('id'=>13),'',array('ch3'=>'ASC'));
		
		$data['middlecontent']='new_registration';
		$this->load->view('admintemplate',$data);
	}


function add_grevience(){
		$user_id=$this->uri->segment(3);
		$data['file_error']='';
		$custid=$user_id;

		if(isset($_POST['submit']))
		{
				$x='';
				$this->form_validation->set_rules('account','Category','trim|required|xss_clean');
				$this->form_validation->set_rules('message','Message','trim|required|xss_clean|max_length[500]');
				$this->form_validation->set_rules('empcode','Employee code','trim|xss_clean');
				$this->form_validation->set_rules('fdc_id','Folio No. / DP ID / Client ID','trim|xss_clean');
				$this->form_validation->set_rules('company_name','Company name','trim|xss_clean');
				if($this->form_validation->run())
				{
					$category = htmlentities($this->input->post('account'));
					$empcode = htmlentities($this->input->post('empcode'));
					$fdc = htmlentities($this->input->post('fdc_id'));
					$company_name=htmlentities($this->input->post('company_name'));
					$message=htmlentities($this->input->post('message'));
					//$uploadfile=$this->input->post('userfile');
					if($category=='1')
						{$x=1;}
					elseif($category=='2')
						{$x=2;}
					elseif($category=='3')
						{$x=3;}
					elseif($category=='4')
						{$x=4;}
					elseif($category=='5')
						{$x=5;}
					elseif($category=='6')
						{$x=6;}
					$filename='';
					//$this->load->library('upload'); 	
					$config=array('upload_path'=>'uploads/',
					'allowed_types'=>'doc|docx|pdf|txt|xls|PDF|DOC|DOCX|TXT|XLS',
					'file_name'=>rand(1,9999),'max_size' => 2048);
					/*	echo '
					<pre>
					';
					print_r($config);die;*/
					//$this->upload->initialize($config);
					$this->load->library('upload', $config);
					if(isset($_FILES['userfile']['name']) && ($_FILES['userfile']['name']!=''))
					{
						if($this->upload->do_upload('userfile'))
					{
						$dt = $this->upload->data();
						//  var_dump($dt); exit();
						$filename=$dt['file_name'];
					}
					else
					{
						$data['file_error']=$this->upload->display_errors();
						$this->session->set_flashdata('upload_err',$this->upload->display_errors());
						redirect(base_url().'userprofile/user/');
					//var_dump($data['file_error']);exit();
					}
					}
						$insert_arr=array('category'=>$category,
						'company'=>$company_name,
						'fdc_id'=>$fdc,
						'empcode'=>$empcode,
						'msg_content'=>$message,
						'user_id'=>$custid,
						'ip_address'=>$this->get_client_ip(),
						'registerdate'=>date('Y-m-d'),
						'uploadpath'=>$filename
						);
					if($insert_id=$this->master_model->insertRecord('usercomplainbox',$insert_arr,true))
					{
						$last_patternid=$this->master_model->getRecords('usercomplainbox',array('msgid'=>$insert_id),'msgid');
					if(count($last_patternid) > 0)
					{
						$last_count = 100+$last_patternid[0]['msgid']; 
					}
						$pattern='GR'.date('y').'0'.$x.'00'.$last_count;
					if($this->master_model->updateRecord('usercomplainbox',array('pattern'=>$pattern),array('msgid'=>$insert_id)))
					{
						$admin_email=$this->master_model->getRecords('adminlogin',array('type'=>$category));
					/*echo '
					<pre>
					';
					print_r($admin_email);die;*/
					$user_info=$this->master_model->getRecords('userregistration',array('user_id'=>$custid ));
					//Sending mail to user from superadmin for grievance has bees posted successfully 
						$info_arr=array(
						'to'=>$user_info[0]['user_email'],						
						'from'=>$this->config->item('MAIL_FROM'),//$admin_email[0]['email']
						'cc'=>$admin_email[0]['emailcc'],
						'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$pattern,
						'view'=>'complain_email'
					);
						$other_info=array('name'=>$user_info[0]['user_name'],
						'pattern'=>$pattern,
						'user_email'=>$user_info[0]['user_email'],
						'user_mobile'=>$user_info[0]['user_mobile'],
						'msg_content'=>$message,
						'reply_status'=>'1');
					if($this->email_sending->sendmail($info_arr,$other_info))
					{
					//Sending mail to admin user's from supeadmin for new  grievance has bees posted successfully 
						$admin_email_list=$this->master_model->getRecords('adminlogin',array('type'=>$category,'type !='=>'superadmin'));
						$super_admin_email=$this->master_model->getRecords('adminlogin',array('type'=>'superadmin'));
					if(count($admin_email_list) > 0)
					{
					foreach($admin_email_list as $res)
					{								
						$info_arr=array(
						'to'=>$res['email'],
						'cc'=>'',
						'from'=>$this->config->item('MAIL_FROM'),//$super_admin_email[0]['email']
						'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$pattern.'',
						'view'=>'complaint_email_to_admin'
					);
						$other_info=array('adminname'=>$res['adminuser'],
						'pattern'=>$pattern,
						'username'=>$user_info[0]['user_name'],
						'user_email'=>$user_info[0]['user_email'],
						'user_mobile'=>$user_info[0]['user_mobile'],
						'msg_content'=>$message,
						'reply_status'=>'1');
					$this->email_sending->sendmail($info_arr,$other_info);
					}
					}
						$this->session->set_flashdata('successmsgsend','Grievance '.$pattern.' has been registered for'.$user_info[0]['user_name']);
						redirect(base_url().'callcenterpanel/add_grevience');
					}
					}
					}
					}
				}
				$user_info=$this->master_model->getRecords('userregistration',array('user_id'=>$custid ));
				$this->db->like('type', 'Specific Office');
				$this->db->or_like('type', 'Online Office');
				$this->db->or_like('type', 'others');
				$data['category']=$this->master_model->getRecords('departments');
				$data['user_info']=$user_info;
				$data['middlecontent']='call_center_add_grev';
				$this->load->view('admintemplate',$data);

	}

	public function user_details($value='')
	 {
					$this->db->join('userregistration','userregistration.user_id=usercomplainbox.user_id');	
	 	$result=$this->master_model->getRecords('usercomplainbox',array('usercomplainbox.user_id'=>$value ));

		$data['result']=$result;

        $data['middlecontent']='user_details';

	 	$this->load->view('admintemplate',$data);
	 
	 } 

	 public function usercomplaindetail($id)

	{
		$data['countuser']='';
	    $data['userinfo']=$this->master_model->getRecords('usercomplainbox',array('msgid'=>$this->uri->segment(3)));
	    if(count($data['userinfo'])>0)

		{
			$data['countuser']='1';
		}
		$data['middlecontent']='call_cent_usercomdetails';
	 	$this->load->view('admintemplate',$data);

	}

	public function get_client_ip() {
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = '';
		return $ipaddress;
	}

	public function index()
	{   
	 	$type='';
	    $data['error_msg']='';
		$data['error_up']='';
		if(isset($_POST['grievance_type']))
		{
			$type=htmlentities($this->input->post('grievance_type'));
		}
		if($type!='')
		{
			if($type==1)
			{

			}

			if($type==2)
			{

				$this->db->where('reply_status','0');
			}

			if($type==3)
			{
				$this->db->where('reply_status','1');
			}
		}

		if($this->session->userdata('type')=='7')
		{
			$data['complaint']=$this->master_model->getRecords('usercomplainbox','','',array('msgid'=>'DESC'));
		}
		else
		{
			$data['complaint']=$this->master_model->getRecords('usercomplainbox',array('category'=>$this->session->userdata('type')),'',array('msgid'=>'DESC'));
		}

		$data['type']=$type;	
		$data['middlecontent']='adminpanel_vw';
	 	$this->load->view('admintemplate',$data);
	}

	

	public function complaindetail()

	{
		$complain_id=$this->uri->segment(3);
		if($complain_id=='')
		{
			redirect(base_url().'adminpanel');
		}

		if(isset($_POST['reply']))
		{
			$this->form_validation->set_rules('reply_msg','Message','trim|required|xss_clean');
			if($this->form_validation->run())
			{
				 $reply_msg=$this->input->post('reply_msg');
					
					$insert_arr        =array('reply_msg'=>$reply_msg,
												'user_id'          =>$this->session->userdata('adminid'),
												'reply_date'       =>date('Y-m-d'),
												'complain_id'      =>$complain_id,
												'complaint_status' =>'1'
												);

				if($insert_id=$this->master_model->insertRecord('complainreply',$insert_arr,true))
					{
						$complainid=array();
						$previous_reply=$this->master_model->getRecords('usercomplainbox',array('msgid'=>$complain_id,'replyby !='=>''),'replyby');		
						if(is_array($previous_reply) && count($previous_reply) > 0)
						{
							$complainid=explode(',',$previous_reply[0]['replyby']);
						}
						if(is_array($complainid) && count($complainid) >0)
						{
							if(!in_array($this->session->userdata('adminid'),$complainid))
							{
								$complainid[]=$this->session->userdata('adminid');
							}
						$strrepliedid=implode(',',$complainid);
						}
						else

						{
							$strrepliedid=$this->session->userdata('adminid');
						}

						$update_arr=array('status'=>'1','replyby'=>$strrepliedid);
						$condition=array('msgid'=>"'".$complain_id."'");
						$this->master_model->updateRecord('usercomplainbox',$update_arr,$condition);
					   //echo $this->db->last_query();exit;
						//get admin information	

						$this->db->join('departments','departments.id=adminlogin.type');
						$admin_email=$this->master_model->getRecords('adminlogin',array('adminlogin.id'=>$this->session->userdata('adminid')));						
					    // get user information

						$this->db->join('userregistration','userregistration.user_id=usercomplainbox.user_id');
						$user_info=$this->master_model->getRecords('usercomplainbox',array('msgid'=>$complain_id));
						//email admin sending code 

						$info_arr=array(
						'to'=>$user_info[0]['user_email'],
						'from'=>$this->config->item('MAIL_FROM'),//$admin_email[0]['email']
						'cc'=>$admin_email[0]['emailcc'],
						'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$user_info[0]['pattern'].'',
						'view'=>'complain_reply_email'

						);

						$other_info=array('name'=>$user_info[0]['user_name'],
													 'pattern'=>$user_info[0]['pattern'],
													 'user_email'=>$user_info[0]['user_email'],
													 'user_mobile'=>$user_info[0]['user_mobile'],
													 'msg_content'=>$user_info[0]['msg_content'],
													 'replyremark'=>$reply_msg,
													'reply_status'=>$user_info[0]['reply_status']);
						//sending email with info to user

						if($this->email_sending->sendmail($info_arr,$other_info))
						{  
							$this->session->set_flashdata('success_message','Reply has been post successfully');
							 redirect(base_url().'adminpanel/complaindetail/'.$complain_id);
					    }

					}

				}

			}

		if(isset($_POST['btn_update']))

		{

			$this->form_validation->set_rules('update_reply_msg','Message','trim|required|xss_clean');

			if($this->form_validation->run())

			{

				 $reply_msg=$this->input->post('update_reply_msg');

				 $reply_id=$this->input->post('reply_id');

				

				 $update_arr=array('reply_msg'=>$reply_msg,

				 								'user_id'=>$this->session->userdata('adminid'),

												'reply_date'=>date('Y-m-d'),

												'complain_id'=>$complain_id,

												'complaint_status'=>'1'

											   );

					if($this->master_model->updateRecord('complainreply',$update_arr,array('reply_id'=>$reply_id)))

					{

						 $this->session->set_flashdata('success_reply','Replied message has been updated successfully');

						  redirect(base_url().'adminpanel/complaindetail/'.$complain_id.'#reply_id');

					}

				}

			}


		if(isset($_POST['status_btn']))

		{

				$reply_status=$this->input->post('reply_type_status');
				if($reply_status==0)
				{
					 $closed_date=date('Y-m-d');
					 $update_arr=array('reply_status'=>$reply_status,

				 								'closed_date'=>$closed_date
											   );
					if($this->master_model->updateRecord('usercomplainbox',$update_arr,array('msgid'=>$complain_id)))

					{

						//get admin information	

						$this->db->join('departments','departments.id=adminlogin.type');

						$admin_email=$this->master_model->getRecords('adminlogin',array('adminlogin.id'=>$this->session->userdata('adminid')));	

						// get user information

						$this->db->join('userregistration','userregistration.user_id=usercomplainbox.user_id');

						$user_info=$this->master_model->getRecords('usercomplainbox',array('msgid'=>$complain_id));

					    $complainreply=$this->master_model->getRecords('complainreply',array('complain_id'=>$complain_id));

						//email admin sending code 

						$info_arr=array(

						'to'=>$user_info[0]['user_email'],

						'from'=>'esdstesting1@esds.co.in',//$admin_email[0]['email']
						
						'cc'=>$admin_email[0]['emailcc'],

						'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$user_info[0]['pattern'].'',

						'view'=>'complain_reply_email'

						);

						$other_info=array('name'=>$user_info[0]['user_name'],

													 'pattern'=>$user_info[0]['pattern'],

													 'user_email'=>$user_info[0]['user_email'],

													 'user_mobile'=>$user_info[0]['user_mobile'],

													 'msg_content'=>$user_info[0]['msg_content'],

													 'replyremark'=>$complainreply[0]['reply_msg'],

													'reply_status'=>$user_info[0]['reply_status']);

                         if($this->email_sending->sendmail($info_arr,$other_info))

						 {
							$superadmin_email=$this->master_model->getRecords('adminlogin',array('type'=>'superadmin'));	

							//email admin sending code 

							$admin_info_arr=array(

							'to'=>$admin_email[0]['email'],
							
							'cc'=>'',

							'from'=>'esdstesting1@esds.co.in',//$superadmin_email[0]['email']

							'subject'=>'GRIEVANCE REDRESSAL PORTAL, Ref No. - '.$user_info[0]['pattern'].'',

							'view'=>'complain_reply_email'

							);

							$admin_other_info=array('name'=>$user_info[0]['user_name'],

																'pattern'=>$user_info[0]['pattern'],

																'user_email'=>$user_info[0]['user_email'],

																'user_mobile'=>$user_info[0]['user_mobile'],

																'reply_status'=>$user_info[0]['reply_status']);

					
							//sending email with info to admin

							if($this->email_sending->sendmail($admin_info_arr,$admin_other_info))
							{
								 $this->session->set_flashdata('success_reply_status','Grievance has been closed');
								 redirect(base_url().'adminpanel/complaindetail/'.$complain_id.'#reply_status_id');
							}
						 }
					}

				}
				else
				{
					 $this->session->set_flashdata('success_reply_status','Grievance alredy closed');
					  redirect(base_url().'adminpanel/complaindetail/'.$complain_id.'#reply_status_id');
								 
				}
		}

		$data['error_msg']='';
		$data['error_up']='';

		if($this->session->userdata('type')=='7')
		{
			$data['complaint']=$this->master_model->getRecords('usercomplainbox',array('msgid'=>$complain_id));
		}
		else
		{
			$data['complaint']=$this->master_model->getRecords('usercomplainbox',array('msgid'=>$complain_id,'category'=>$this->session->userdata('type')));
		}

		if(is_array($data['complaint']) && count($data['complaint']) <=0)
		{
			redirect(base_url().'adminpanel');
		}

		$data['middlecontent']='complain_detail';
		$this->load->view('admintemplate',$data);

		}

	

	public function delete()

	{

		$complain_id=$this->uri->segment('3');

		$reply_id=$this->uri->segment('4');

		if($this->master_model->deleteRecord('complainreply','reply_id',$reply_id))

		{

			 $this->session->set_flashdata('success_reply','Replied message has been deleted successfully');

			 redirect(base_url().'adminpanel/complaindetail/'.$complain_id.'#reply_id');

		}

		else

		{

		  	$this->session->set_flashdata('success_reply','Error while deleting record');

			redirect(base_url().'adminpanel/complaindetail/'.$complain_id.'#reply_id');

		}
	

	}

	

	}

?>