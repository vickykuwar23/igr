<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Userlogin extends CI_Controller {
function __construct()
	{
		 parent::__construct();
		 $this->load->helper('security');
		 $this->load->model('chk_session');
		 $this->load->model('master_model');
		 $this->load->helper('captcha');
		 $this->load->helper('cookie');

	}

		public function index()
		{
			$ip = $this->input->ip_address();
			if($this->session->userdata('user_id')!='' && $this->session->userdata('user')!='')
			{
				redirect(base_url().'complaint');
			}
			
			if(!empty($_POST))
			{ 
				$this->form_validation->set_rules('username_mobile', 'Username or Mobile', 'trim|required|xss_clean');
				$this->form_validation->set_rules("password", 'Password', 'trim|required|xss_clean');
				$this->form_validation->set_rules('code','Captcha Code','trim|xss_clean|callback_check_captcha_userlogin|required');
				if($this->form_validation->run())
				{ 
					$_userName = 	$this->db->escape_str($this->input->post('username_mobile'));
					$_passWord = 	$this->input->post('password');
				

					$check_for_new="(uniqueusername='".$this->db->escape_str($_userName)."' OR user_mobile='".$this->db->escape_str($_userName)."')";
					
					$this->db->or_where($check_for_new);
					$is_new=$this->master_model->getRecords('userregistration');
					//echo $this->db->last_query();die();
						
					if(count($is_new)){
						
					if($is_new[0]['is_new_user'] == 1 ){
						
						$query="(uniqueusername='".$this->db->escape_str($_userName)."' OR user_mobile='".$this->db->escape_str($_userName)."')";
						//$query='(uniqueusername="'.$this->db->escape_str($_userName).'" OR user_mobile="'.$this->db->escape_str($_userName).'")';
						$this->db->or_where($query);
						$input_array=array('user_pass'=>$this->sha256($_passWord));
						$user_info=$this->master_model->getRecords('userregistration',$input_array);
						
							if( is_array($user_info) &&  !empty($user_info) && count($user_info))
							{ 
								$guid = md5(uniqid(rand(), true));
								$this->input->set_cookie('random_cookie', $guid, 36000);
								$user_data=array('user'=>$user_info[0]['user_email'],
												'user_id'=>$user_info[0]['user_id'],
												'uniqueusername'=>$user_info[0]['uniqueusername'],
												'user_mobile'=>$user_info[0]['user_mobile'],
												'login_time'=>date("Y-m-d H:i:s")

											);
								$this->session->sess_expiration = '1200';
								$this->session->set_userdata($user_data);
								//add logs
								$json_encode_data = json_encode(array('username'=>$_userName));
								$log_user_id      = $this->session->userdata('user_id');
								$ipAddr			  = $this->get_client_ip();
								$logDetails = array(
													'module_name' 	=> "Userlogin",
													'action_name' 	=> "Success",
													'json_response'	=> $json_encode_data,
													'login_id'		=> $log_user_id,
													'role_type'		=> "Citizen User",
													'createdAt'		=> date('Y-m-d H:i:s'),
													'ip_addr'		=> $ipAddr
													);
								$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								
								//print_r($_SESSION);
								//print_r(($logData)); exit;
								//end add logs
								redirect(base_url().'complaint');
							}
						   else
							{
								
								//add logs
								$json_encode_data = json_encode(array('username'=>$_userName));
								$log_user_id      = '';
								$ipAddr			  = $this->get_client_ip();
								$logDetails = array(
													'module_name' 	=> "Userlogin",
													'action_name' 	=> "Failed",
													'json_response'	=> $json_encode_data,
													'login_id'		=> $log_user_id,
													'role_type'		=> "Citizen User",
													'createdAt'		=> date('Y-m-d H:i:s'),
													'ip_addr'		=> $ipAddr
													);
								$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								//end add logs
								$this->session->set_flashdata('invalidcredential','Invalid credential');
								redirect(base_url().'userlogin');
							}
					}
						else{
								$this->session->set_flashdata('olduser','Your password expired.Please click on forgot password to set new password');
								redirect(base_url().'userlogin');
					}

			    }else{
			    				//add logs
								$json_encode_data = json_encode(array('username'=>$_userName));
								$log_user_id      = null;
								$ipAddr			  = $this->get_client_ip();
								$logDetails = array(
													'module_name' 	=> "Userlogin",
													'action_name' 	=> "Failed",
													'json_response'	=> $json_encode_data,
													'login_id'		=> $log_user_id,
													'role_type'		=> "Citizen User",
													'createdAt'		=> date('Y-m-d H:i:s'),
													'ip_addr'		=> $ipAddr
													);
								$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
								//end add logs

			    	$this->session->set_flashdata('invalidcredential','Invalid credential');
						redirect(base_url().'userlogin');
			    }



			    }

			}
			
			$this->load->helper('captcha');
			$vals = array(
							'img_path' => './images/captcha/',
							'img_url' => base_url().'images/captcha/',
							);
	          $cap = create_captcha($vals);
			  $data['image'] = $cap['image'];
		    $_SESSION["userlogincaptcha"]=$cap['word'];

			$data['error_msg']='';

			$data['middlecontent']='userlogin_vw';

			$this->load->view('template',$data);

		}
	
	public function refresh(){
        // Captcha configuration
        $config = array(
            'img_path' => './images/captcha/',
			'img_url' => base_url().'images/captcha/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('userlogincaptcha');
        $this->session->set_userdata('userlogincaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}	

	public function sha256($pass)
	{
		return hash('sha256',$pass);
	}

	public function is_new($username='')
	{
		$is_new="";
		// $username=$_POST['username'];
		if($username)
		{
			$input_array=array('uniqueusername'=>$username);
			$user_info=$this->master_model->getRecords('userregistration',$input_array);
			if($user_info[0]['is_new_user']==1){
				$is_new=1;die;
			}else{
				$is_new=0;die;
			}
		} else {
			echo $is_new;die;
		}
		
	}

	public function registration()
	{   
	   
		$data['error_msg']=$data['file_error']='';

		$state=$city='';$emailsend='';$email='';
		
		if(isset($_POST['btn_reg']))
		{
	       $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

			$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|valid_email|required|is_unique[userregistration.user_email]');

			$this->form_validation->set_rules('mobile', 'Mobile No.', 'trim|required|xss_clean|min_length[9]|max_length[10]|numeric|is_unique[userregistration.user_mobile]');

			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[50]|is_unique[userregistration.uniqueusername]|alpha_numeric');

			$this->form_validation->set_rules('address1', 'Address', 'trim|required|xss_clean');

			$this->form_validation->set_rules('aadhar_number', 'Aadhar card number', 'numeric|min_length[12]|max_length[16]|xss_clean');

			$this->form_validation->set_rules('state', 'State', 'trim|required|xss_clean');

			$this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');	

			$this->form_validation->set_rules('pincode', 'Pincode', 'trim|required|xss_clean|numeric');	
			
	     	$this->form_validation->set_message('is_unique', 'The %s already exists');
		
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha');
			
	  

			if($this->form_validation->run())
			{	
				$name=htmlentities($this->input->post('name'));

				$uniqueusername=htmlentities($this->input->post('username'));

				$email=htmlentities($this->input->post('email'));

				$mobile=htmlentities($this->input->post('mobile'));

				$landline=htmlentities($this->input->post('landline'));

				$aadhar_number=htmlentities($this->input->post('aadhar_number'));

				$aadhar_number=htmlentities($this->input->post('aadhar_number'));
				if ($aadhar_number) {
					$aadhar_no =$aadhar_number;
				}else{
					$aadhar_no = null;
				}

				$address1=htmlentities($this->input->post('address1'));

				$address2=htmlentities($this->input->post('address2'));

				$state=htmlentities($this->input->post('state'));

				$city=htmlentities($this->input->post('city'));

				$pincode=htmlentities($this->input->post('pincode'));

				$insert_arr= array('user_name'=>$name,

						  'uniqueusername'=>$uniqueusername,

						  'user_email'=>$email,

						  'user_mobile'=>$mobile,

						  'user_landline'=>$landline,

						  'user_address1'=>$address1,

						  'user_address2'=>$address2,

						  'user_state'=>$state,

						   'aadhar_number'=>$aadhar_no,
						  
						  'user_city'=>$city,

						  'user_pincode'=>$pincode);
						  
				if($insert_id=$this->master_model->insertRecord('userregistration',$insert_arr,true))
				{	
					//add logs
						$json_encode_data = json_encode($insert_arr);
						$log_user_id      = null;
						$ipAddr			  = $this->get_client_ip();
						$logDetails = array(
											'module_name' 	=> "Userlogin",
											'action_name' 	=> "Register",
											'json_response'	=> $json_encode_data,
											'login_id'		=> $log_user_id,
											'role_type'		=> "Citizen User",
											'createdAt'		=> date('Y-m-d H:i:s'),
											'ip_addr'		=> $ipAddr
											);
						$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
						//end add logs
	  
					$this->session->set_flashdata('success_message','You are successfully registered on '.$this->config->item('PROJECT_NAME').'s Grievance Redressal Portal. Please create your password.');
					redirect(base_url().'create/password/'.$insert_id);	  

				}

		   }		

		}
		// $this->session->set_userdata("mycaptcha", rand(1,100000));
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		  //$this->session->unset_userdata("mycaptcha");
		
	    $_SESSION["mycaptcha"]=$cap['word'];
        $this->session->set_userdata("used", 0);
		$data['state']=$this->master_model->getRecords('states','','',array('ch3'=>'ASC'));

		 $data['middlecontent']='registration';
		//$data['middlecontent']='reg_temp';

		$this->load->view('template',$data);

	}

	public function refresh_reg(){
        // Captcha configuration
        $config = array(
            'img_path' => './images/captcha/',
			'img_url' => base_url().'images/captcha/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}	

	
	public function check_captcha($code)
	{
		// echo "<pre>";
		// print_r($_SESSION["mycaptcha"]);die;
		if($code == '' || (isset($_SESSION["mycaptcha"]) && ($_SESSION["mycaptcha"] != $code))  ||  $this->session->userdata("used")==1  )
		{
			$this->form_validation->set_message('check_captcha', 'Invalid %s.');
			$this->session->set_userdata("mycaptcha", rand(1,100000));
			$this->session->set_userdata("used",0);
			return false;
		}
		if( (isset($_SESSION["mycaptcha"]) && ($_SESSION["mycaptcha"] == $code)) && $this->session->userdata("used")==0)
		{
			$this->session->unset_userdata("mycaptcha");
			//$this->session->set_userdata("mycaptcha", rand(1,100000));
			$this->session->set_userdata("used", 1);
			return true;
		}
		
	}
	public function check_captcha_userlogin($code) 

	{
		      // check if captcha is set -
				if($code == '')
				{
					$this->form_validation->set_message('check_captcha_userlogin', '%s is required.'); 
					$this->session->set_userdata("userlogincaptcha", rand(1,100000));
					return false;
				}
				if($code == '' || (isset($_SESSION["userlogincaptcha"])) && ($_SESSION["userlogincaptcha"] != $code))
				{
					$this->form_validation->set_message('check_captcha_userlogin', 'Invalid %s.'); 
					$this->session->set_userdata("userlogincaptcha", rand(1,100000));
					return false;
				}
				if((isset($_SESSION["userlogincaptcha"])) && ($_SESSION["userlogincaptcha"] == $code))
				{
					$this->session->unset_userdata("userlogincaptcha");
					$this->session->set_userdata("userlogincaptcha", rand(1,100000));
				
					return true;
				}
		
	}

   

	public function getcity()

	{
		$str='';

		$stateid=htmlentities($this->input->post('stateid'));

		if ($stateid=='') {

			redirect(base_url());
			die;
		}



		if($stateid)
		{
			$city=$this->master_model->getRecords('city',array('ch3'=>$stateid));



			foreach($city as $cities)

			{ 

							$str.= "<option value='".$cities['ch4']."'>". $cities['ch4'] . "</option>";

		    }

			echo $str;
		} else {
			echo "End";
			die;
		}

	}

	public function getcity_selected()

	{

		$str='';

		$stateid=htmlentities($this->input->post('stateid'));

		$city_id=htmlentities($this->input->post('city'));

		if($stateid)
		{
			$city=$this->master_model->getRecords('city',array('ch3'=>$stateid));

			

			foreach($city as $cities)

			{ 
				$selected = isset($cities['ch4'])&& $cities['ch4']==$city_id?"selected=selected":'';
				$str.= "<option value='".$cities['ch4']."' $selected > ". $cities['ch4'] . "</option>";

		    }

			echo $str;
		} else {
			echo "End";die;
		}

		

	}

	

	public function chkusername()

	{

		//$username='';

		$username=htmlentities($this->input->post('username'));

		$result=$this->master_model->getRecords('userregistration',array('uniqueusername'=>$username));

		if(count($result) > 0)

		{

			//email is already taken

			echo 0;

		}

		else 

		{

			//email is available

			echo 1;

		}

	}

	

	public function forgetpass()

	{

		if(isset($_POST['submitforgetpass']))		

			{
			
				$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha');
				if($this->form_validation->run()){
				$email=htmlentities($this->input->post('email'));

				$user_info=$this->master_model->getRecords('userregistration',array('user_email'=>$email));

				if(count($user_info)>0)	

				{
					$t=$user_info[0]['forget_email_time'];
					$t2=date("Y-m-d H:i:s") ;

					$hourdiff = round((strtotime($t2) - strtotime($t))/3600, 1);

				if($hourdiff>=1 || 1){

					$info_arr=array('to'=>$email,
					
					                           'cc'=>'',

											  'from'=>$this->config->item('MAIL_FROM'),

											  'subject'=>'Forget Password of '.$this->config->item('PROJECT_NAME').' Grievance Portal',

											  'view'=>'forgetpassmail'

											  );

					$other_info=array('name'=>$user_info[0]['user_name'],

			                                  	'user_id'=>$user_info[0]['user_id']);
											  
					if($this->email_sending->sendmail($info_arr,$other_info))

					{
						$time=date("Y-m-d H:i:s") ;
						$query = "UPDATE userregistration
						SET forget_email_time='$time'
						WHERE user_email='".$email."'";
						$q=$this->db->query($query);
						// $update_arr=array(
						// 	'is_new_user '=>1
						// );
						// echo $this->db->last_query();die;
						// echo $q; die;
						
						// $this->master_model->updateRecord('userregistration',
						// $update_arr,
						// array('user_email'=>$email));die;
						// echo $this->db->last_query();die;

						$verify=array(
							'forgetemailverify'=>$email,
							'forget_login_time'=>time()
						);
                        $this->session->set_userdata($verify);		
						
						//add logs
						$json_encode_data = json_encode(array('email'=>$email));
						$log_user_id      = null;
						$ipAddr			  = $this->get_client_ip();
						$logDetails = array(
											'module_name' 	=> "Userlogin",
											'action_name' 	=> "forgetpass",
											'json_response'	=> $json_encode_data,
											'login_id'		=> $log_user_id,
											'role_type'		=> "Citizen User",
											'createdAt'		=> date('Y-m-d H:i:s'),
											'ip_addr'		=> $ipAddr
											);
						$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
						//end add logs
						$this->session->set_flashdata('success_message','If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.');
						redirect(base_url().'userlogin/forgetpass/');

					}

					else

					{

						$this->session->set_flashdata('failsend','Something goes wrong please try again in some time. ');

						redirect(base_url().'userlogin/forgetpass/');

					}	
				}else{
					$this->session->set_flashdata('success_message','If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.');
						redirect(base_url().'userlogin/forgetpass/');
				}

			}

			else

			{

				$this->session->set_flashdata('success_message','If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.');

				redirect(base_url().'userlogin/forgetpass/');

			}



				}else{
						
						$this->session->set_flashdata('invalidcredential','Invalid credential');
						
				}

			}
			$this->load->helper('captcha');
			$vals = array(
				'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
			$cap = create_captcha($vals);
			$data['image'] = $cap['image'];
			//$this->session->unset_userdata("mycaptcha");
						  
			$_SESSION["mycaptcha"]=$cap['word'];		

		$data['middlecontent']='forgetpass';

		$this->load->view('template',$data);

	}



	public function refresh_forget(){
        // Captcha configuration
        $this->load->helper('captcha');
        $config = array(
            'img_path' => './images/captcha/',
			'img_url' => base_url().'images/captcha/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}


	public function logout()

	{
		$ip = $this->input->ip_address();

		$logs_data = array(
								'username' => $_SESSION['uniqueusername'],
								'ip_addr' => $ip,
								'login_date_time' => $_SESSION['login_time'],
								'login_status' =>'Success',
								'logout_time' => date("Y-m-d H:i:s"),
								'action_type' => 'Logout',
								'module_name'=>'logout',	
								'action_date' => date("Y-m-d H:i:s")
											);
							//$this->master_model->insertRecord("audit_tails",$logs_data);
        session_destroy();

	    $user_data=array('user'=>'','user_id'=>'','forgetemailverify'=>'');
		$this->session->unset_userdata($user_data);
         unset($_SESSION);
         delete_cookie("random_cookie");
		redirect(base_url().'userlogin');
	 }

	public function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}

	 
}

	

/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
