<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Adminlogin extends CI_Controller 
{
   function __construct()
	{
	  //  echo $_SERVER['REMOTE_HOST']; exit;
		 parent::__construct();
		 $this->load->model('chk_session');
		 $this->load->model('master_model');
		
	}
	public function index()
	{   
	   $ip = $this->get_client_ip();
	   
	   // if($ip!="115.124.115.71" && $ip!="59.145.23.38")
	   // {
	   //     	redirect(base_url());
	   // }
	 
	   if(isset($_POST['submit']))
		{
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_adminlogin');
			if($this->form_validation->run())
			{
				$_userName = 	$this->db->escape_str($this->input->post('username'));
				 $_passWord = 	$this->input->post('password'); 
				$input_array=array('adminuser'=>$_userName,'adminpass'=>sha1($_passWord),'type !='=>'superadmin');
				$user_info=$this->master_model->getRecords('adminlogin',$input_array);
			   // echo $this->db->last_query();exit;
			    
			    if(is_array($user_info))
			    {
    				if( $user_info)
    				{ 
    				 
    				  //session_destroy();
    				
    					$user_data=array('username'=>$user_info[0]['adminuser'],
    													'adminid'=>$user_info[0]['id'],
    													'type'=>$user_info[0]['type']);
    					$this->session->set_userdata($user_data);
    					
    					$department_type=$this->master_model->getRecords('departments',array('id'=>$user_info[0]['type']));
    					
    					if(ucfirst($department_type[0]['type'])=='Viewonly')
    					{
    						redirect(base_url().'index.php/viewonly/');
    					}
    					else
    					{
    						redirect(base_url().'index.php/adminpanel/');
    					}
    				}
    				  else
			         {
				    	$this->session->set_flashdata('invalidadminlogin','Invalid credential');
				    	redirect(base_url().'index.php/adminlogin');
			         }
			    }
			   else
			    {
					$this->session->set_flashdata('invalidadminlogin','Invalid credential');
					redirect(base_url().'index.php/adminlogin');
			    }
		    }
		}
		
		$this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
        $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		  //$this->session->unset_userdata("mycaptcha");
		  
	      $_SESSION["adminlogincaptcha"]=$cap['word'];
		$data['middlecontent']='adminlogin_vw';
	    $this->load->view('admintemplate',$data);
		
    }//F_login
		
  public function check_captcha_adminlogin($code) 

	{
	   // print_r($_SESSION);
		      // check if captcha is set -
				if($code == '' || $_SESSION["adminlogincaptcha"] != $code )
				{
					$this->form_validation->set_message('check_captcha_adminlogin', 'Invalid %s.'); 
					$this->session->set_userdata("adminlogincaptcha", rand(1,100000));
					
					return false;
				}
				if($_SESSION["adminlogincaptcha"] == $code)
				{
					$this->session->unset_userdata("adminlogincaptcha");
					//$this->session->set_userdata("mycaptcha", rand(1,100000));
					return true;
				}
		
	}
	
	public function change_password()
	{
	   //echo $this->session->userdata('username'); 
	   if(!$this->session->userdata('username') &&  empty($this->session->userdata('username')))
	   {
	   	 
			redirect(base_url().'index.php/adminlogin/');
	   }
	   if(isset($_POST['submit']))
		{ 
			$this->form_validation->set_rules('old_pw', 'Old Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_pw', 'New Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('c_pw', 'Confirm Password', 'trim|required|xss_clean|matches[new_pw]');			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_adminlogin');
 			if($this->form_validation->run())
			{ 
				$old_pw = $this->input->post('old_pw');  
				$new_pw = $this->input->post('new_pw');
				$c_pw = $this->input->post('c_pw');
				//print_r($this->session); exit;
				$input_array=array('adminpass'=>sha1($old_pw),'adminuser'=>$this->session->userdata('username'));
				$user_info=$this->master_model->getRecords('adminlogin',$input_array);
				 			
				  
				if(count($user_info) > 0)
				{
					if($new_pw == $old_pw)
					{
						$this->session->set_flashdata('invalidadminlogin','New Password is same as old password');
						redirect(base_url().'index.php/adminlogin/change_password');
					}
					else
					{ 
						$query = "UPDATE adminlogin
						SET adminpass='".sha1($new_pw)."'
						WHERE adminuser='".$this->session->userdata('username')."'"; 
						
						$q=$this->db->query($query);
 						$this->session->set_flashdata('success_msg','Password has been reset.');
						redirect(base_url().'index.php/adminlogin/change_password');
 					}
 				}
				else
				{ 
					$this->session->set_flashdata('invalidadminlogin','Invalid old password');
					redirect(base_url().'index.php/adminlogin/change_password');
				}
			}
			
		}
	    $this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		  //$this->session->unset_userdata("mycaptcha");
		  
	    $_SESSION["adminlogincaptcha"]=$cap['word'];
		$data['middlecontent']='change_pw';
	    $this->load->view('admintemplate',$data);
	}
	
	
	public function viewonly()
	{
	       
	   if(isset($_POST['submit']))
		{
			
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			if($this->form_validation->run())
			{
				$_userName = 	$this->db->escape_str($this->input->post('username'));
				$_passWord = 	$this->input->post('password');
				$input_array=array('adminuser'=>$_userName,'adminpass'=>sha1($_passWord),'type'=>'viewonly');
				$user_info=$this->master_model->getRecords('adminlogin',$input_array);
				if(count($user_info))
				{ 
					//$status=array('status'=>'1');
				    //$this->master_model->updateRecord('adminlogin',$status,array('id'=>$user_info[0]['id']));
					$user_data=array('viewusername'=>$user_info[0]['adminuser'],
												'viewadminid'=>$user_info[0]['id'],
												'viewtype'=>$user_info[0]['type']);
					$this->session->set_userdata($user_data);
					redirect(base_url().'index.php/viewonly');
			    }
			   else
			    {
					$this->session->set_flashdata('invalidadminlogin','Invalid credential');
					redirect(base_url().'index.php/adminlogin');
			    }
		    }
		}
		$data['middlecontent']='viewonly_vw';
	    $this->load->view('admintemplate',$data);
	 }
	
	
	public function adminlogout()
	{
	    $user_data=array('username'=>'',
						'adminid'=>'',
						'type'=>'');
		$this->session->unset_userdata($user_data);
		redirect(base_url().'index.php/adminlogin');
	 }
	 
	function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }
	
}//C_welcome