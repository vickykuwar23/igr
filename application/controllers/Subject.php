<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subject extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		$this->chk_session->chk_superadmin_session();
    }
	
	public function index()
	{
		$data['subject_list']=$this->master_model->getRecords('gri_subject','','',array('sub_id'=>'DESC'));
		$data['middlecontent']='subject/index';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}

	public function add()
	{	
		$this->form_validation->set_rules('subject_name', 'Subject Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('complaint_type', 'Complaint type', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('complaint_sub_type', 'Complaint sub type', 'trim|required|numeric|xss_clean');	
		
		if($this->form_validation->run())
		{
			$subject_name 		= 	htmlentities($this->input->post('subject_name'));
			$complaint_type 	= 	$this->input->post('complaint_type');
			$complaint_sub_type		= 	htmlentities($this->input->post('complaint_sub_type'));
			$createdAt			=   date('Y-m-d H:i:s');
			$insert_arr=array('subject_name'=>$subject_name,
							  'complaint_type'=>$complaint_type,
							  'office_type'=>$complaint_sub_type,
							  'status'=>'0',
							  'createdat'=>	$createdAt,
							  'updatedat'=>	$createdAt
							  );
			if($this->master_model->insertRecord('gri_subject',$insert_arr)){
				 //add logs
				$json_encode_data = json_encode($insert_arr);
				$log_user_id      = $this->session->userdata('supadminid');
				$ipAddr			  = $this->get_client_ip();
				$logDetails = array(
									'module_name' 	=> "Subject",
									'action_name' 	=> "Add",
									'json_response'	=> $json_encode_data,
									'login_id'		=> $log_user_id,
									'role_type'		=> $this->session->userdata('role_name'),
									'createdAt'		=> date('Y-m-d H:i:s'),
									'ip_addr'		=> $ipAddr
									);
				$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
				//end add logs
				$this->session->set_flashdata('success_message','The subject has been created');
			redirect(base_url().'subject');
		}else{
			$this->session->set_flashdata('error_message','Error while processing your request');
			redirect(base_url().'subject/add');
		}

			
		}
		$data['complaint_type']=$this->master_model->getRecords('gri_complaint_type','','',array('complaint_type_id'=>'DESC'));
		$data['complaint_sub_type']=$this->master_model->getRecords('gri_complaint_sub_type',array('status'=>'1'),'',array('complaint_sub_type_id'=>'DESC'));
		$data['middlecontent']='subject/add';	
		$data['master_mod'] = 'Master';
	 	$this->load->view('admin/admin_combo',$data);
	}
	
/****** Edit Functionality ******/ 
	
	public function edit()
	{
		$data['error_msg']='';
		$updateid=$this->uri->segment('3');
		
		if( $this->input->post('submit')  ) {
			$this->form_validation->set_rules('subject_name', 'Subject Type', 'trim|required|xss_clean');
			$this->form_validation->set_rules('complaint_type', 'Complaint type', 'trim|required|numeric|xss_clean');
			$this->form_validation->set_rules('complaint_sub_type', 'Complaint sub type', 'trim|required|numeric|xss_clean');	
			
			if($this->form_validation->run())
			{
				$subject_name 		= 	htmlentities($this->input->post('subject_name'));
				$complaint_type 	= 	$this->input->post('complaint_type');
				$complaint_sub_type		= 	htmlentities($this->input->post('complaint_sub_type'));
				
				$update_arr=array('subject_name'=>$subject_name,
								  'complaint_type'=>$complaint_type,
								  'office_type'=>$complaint_sub_type,
								  );

				if($this->master_model->updateRecord('gri_subject',$update_arr,array('sub_id'=>"'".$updateid."'"))){

					//add logs
					$for_id=array('record_id'=>$updateid);
					$array_first = array_merge($update_arr,$for_id);
					$json_encode_data = json_encode($array_first);
					$log_user_id      = $this->session->userdata('supadminid');
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Subject",
										'action_name' 	=> "Edit",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> $this->session->userdata('role_name'),
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
					//end add logs

					$this->session->set_flashdata('success_message','The subject has been updated');
				redirect(base_url().'subject');
			}else{
				$this->session->set_flashdata('error_message','Error while processing your request');
				redirect(base_url().'subject');
			}

				
			}
		}
		//$data['complaint_type']=$this->master_model->getRecords('gri_complaint_type','','',array('complaint_type_id'=>'DESC'));
		//$data['complaint_sub_type']=$this->master_model->getRecords('gri_complaint_sub_type',array('status' => '1'),'',array('complaint_sub_type_id'=>'DESC'));
		$data['subject']=$this->master_model->getRecords('gri_subject',array('sub_id'=>$updateid));
		//print_r($data['subject']);
		
		$data['complaint_type']=$this->master_model->getRecords('gri_complaint_type','','',array('complaint_type_id'=>'DESC'));
		$data['complaint_sub_type']=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id' => $data['subject'][0]['complaint_type'],'status'=>'1'),'',array('complaint_sub_type_id'=>'DESC'));
		
		$data['subject']=$this->master_model->getRecords('gri_subject',array('sub_id'=>$updateid));

		if(count($data['subject']) <= 0)
		{
			redirect(base_url().'subject');
		}
			
		$data['middlecontent']='subject/edit';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	public function delete()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'subject');
		}
		if($this->master_model->deleteRecord('gri_subject','sub_id',$id))
		{

		  // logs
			$for_id=array('record_id'=>$id);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Subject",
								'action_name' 	=> "Delete",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			// end logs		
		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'subject');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'subject');
		}
	
	}

	public function get_complaint_sub_type()
	{
		$csrf_token = $this->security->get_csrf_hash();
		$str='<option value="">Please select</option>';
		$complaint_id=htmlentities($this->input->post('complaint_id'));
		$sub_types=$this->master_model->getRecords('gri_complaint_sub_type',array('complaint_type_id'=>$complaint_id, 'status' => '1'));
		foreach($sub_types as $sub_type)
		{ 
			$str.= "<option value='".$sub_type['complaint_sub_type_id']."'>". $sub_type['complaint_sub_type_name'] . "</option>";
	    }
		// echo $str;
		echo json_encode(['str' => $str, 'csrf_token' => $csrf_token ]);

	}

	public function change_status()
	{
		 $id = $this->uri->segment(3);
		 $status = $this->uri->segment(4);
	
		if($status=='1'){$newstatus = '0'; }
		else if($status=='0'){$newstatus = '1';}
		if($this->master_model->updateRecord("gri_subject",array('status'=>$newstatus)
		,array('sub_id'=>$id)))
		{
			// logs
			$for_id=array('record_id'=>$id,'status'=>$status);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Subject",
								'action_name' 	=> "change status",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			// logs end
			$this->session->set_flashdata('success_message','Status changed successfully.');						           
			redirect(base_url().'subject');
		}
		else
		{
			$this->session->set_flashdata('error_message','Error while updating user status.');										            
			redirect(base_url().'subject');
		}
	}

	function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }
} // Class End

?>