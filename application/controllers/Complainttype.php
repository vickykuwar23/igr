<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complainttype extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		$this->chk_session->chk_superadmin_session();
    }
	
	public function index()
	{
		$data['complaint_type_list']=$this->master_model->getRecords('gri_complaint_type','','',array('complaint_type_id'=>'DESC'));
		$data['middlecontent']='complaint_type/index';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}

	public function add()
	{	
		redirect(base_url().'complainttype');
		$this->form_validation->set_rules('complaint_type_name', 'Complaint Type', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('response_time', 'Response Time', 'trim|required|numeric|xss_clean');
		//$this->form_validation->set_rules('resident_id', 'Resident ID', 'trim|required|numeric|xss_clean');	
		if($this->form_validation->run())
		{
			$c_type_name 		= 	htmlentities($this->input->post('complaint_type_name'));
			$c_response_time 	= 	$this->input->post('response_time');
			$c_resident_id		= 	htmlentities($this->input->post('resident_id'));
			$createdAt			=   date('Y-m-d H:i:s');
			$insert_arr=array('complaint_type_name'=>$c_type_name,
							  'response_time'=>'0',
							  'resident_id'=>'0',
							  'status'=>1,
							  'createdat'=>$createdAt,
							  'updatedat'=>$createdAt,
							  );
			$complaintType_info = $this->master_model->insertRecord('gri_complaint_type',$insert_arr);
			 //add logs
				$json_encode_data = json_encode($insert_arr);
				$log_user_id      = $this->session->userdata('supadminid');
				$ipAddr			  = $this->get_client_ip();
				$logDetails = array(
									'module_name' 	=> "Complainttype",
									'action_name' 	=> "Add",
									'json_response'	=> $json_encode_data,
									'login_id'		=> $log_user_id,
									'role_type'		=> $this->session->userdata('role_name'),
									'createdAt'		=> date('Y-m-d H:i:s'),
									'ip_addr'		=> $ipAddr
									);
				$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
				//end add logs
			$this->session->set_flashdata('success_message','The complaint type has been created');
			redirect(base_url().'complainttype');
		}
		
		$data['middlecontent']='complaint_type/add';
		$data['master_mod'] = 'Master';		
	 	$this->load->view('admin/admin_combo',$data);
	}
	
/****** Edit Functionality ******/ 
	
	public function edit()
	{
		$data['error_msg']='';
		$updateid=$this->uri->segment('3');
		
			$this->form_validation->set_rules('complaint_type_name', 'Complaint Type', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('response_time', 'Response Time', 'trim|required|numeric|xss_clean');
			//$this->form_validation->set_rules('resident_id', 'Resident ID', 'trim|required|numeric|xss_clean');	
			if($this->form_validation->run())
			{
				$c_type_name 		= 	htmlentities($this->input->post('complaint_type_name'));
				$c_response_time 	= 	$this->input->post('response_time');
				$c_resident_id		= 	htmlentities($this->input->post('resident_id'));
				$updatedAt			=   date('Y-m-d H:i:s');
				
				$update_arr=array('complaint_type_name'=>$c_type_name,
								  'updatedat'=>$updatedAt,
								  );
				
				if($this->master_model->updateRecord('gri_complaint_type',$update_arr,array('complaint_type_id'=>"'".$updateid."'")))
				{
					//add logs
					$for_id=array('record_id'=>$updateid);
					$array_first = array_merge($update_arr,$for_id);
					$json_encode_data = json_encode($array_first);
					$log_user_id      = $this->session->userdata('supadminid');
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "Complainttype",
										'action_name' 	=> "Edit",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> $this->session->userdata('role_name'),
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
					//end add logs
					$this->session->set_flashdata('success_message',' Complaint Type has been updated successfully.');
					redirect(base_url().'complainttype/edit/'.$updateid);
				}
				else
				{
					$data['error_msg']='Error while updating record.';
				}
			}
		

		$data['get_complaint_date']=$this->master_model->getRecords('gri_complaint_type',array('complaint_type_id'=>$updateid));
		if(count($data['get_complaint_date']) <= 0)
		{
			redirect(base_url().'complainttype/view/');
		}
			
		$data['middlecontent']='complaint_type/edit';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	public function delete()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'complainttype');
		}
		if($this->master_model->deleteRecord('gri_complaint_type','complaint_type_id',$id))
		{
			// logs
			$for_id=array('record_id'=>$id);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Complainttype",
								'action_name' 	=> "Delete",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'complainttype');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'complainttype');
		}
	
	}

	public function change_status()
	{
		 $id = $this->uri->segment(3);
		 $status = $this->uri->segment(4);
	
		if($status=='1'){$newstatus = '0'; }
		else if($status=='0'){$newstatus = '1';}
		if($this->master_model->updateRecord("gri_complaint_type",array('status'=>$newstatus)
		,array('complaint_type_id'=>$id)))
		{
			// logs
			$for_id=array('record_id'=>$id,'status'=>$status);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "Complainttype",
								'action_name' 	=> "change status",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			$this->session->set_flashdata('success_message','Status changed successfully.');						           
			redirect(base_url().'complainttype');
		}
		else
		{
			$this->session->set_flashdata('error_message','Error while updating user status.');										            
			redirect(base_url().'complainttype');
		}
	}

	function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }
	
} // Class End

?>