<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Imp_links extends CI_Controller {
	
		function __construct()
		{
			 parent::__construct();
			 

		}

		public function index()
		{
			 if (!empty($_SERVER['HTTP_CLIENT_IP']))   
			  {
				$ip_address = $_SERVER['HTTP_CLIENT_IP'];
			  }
			//whether ip is from proxy
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
			  {
				$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
			  }
			//whether ip is from remote address
			else
			  {
				$ip_address = $_SERVER['REMOTE_ADDR'];
			  }
			  
			$createdAt		=   date('Y-m-d'); 
			
			$getIPexist					= $this->master_model->getRecords('gri_visitors',array('ip_address'=>$ip_address));
			if(count($getIPexist) == 0):
				$insert_arr	=	array(	'ip_address'	=>	$ip_address,
										'created_at'	=>	$createdAt
										);					  
				$visitorCount = $this->master_model->insertRecord('gri_visitors',$insert_arr);
			endif;
			
			$data['middlecontent']='imp_links/index';			
			$this->load->view('template',$data);

		}
		

	 
}

	

/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
