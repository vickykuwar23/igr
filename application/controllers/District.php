<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class District extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('chk_session');
		$this->chk_session->chk_superadmin_session();
    }
	
	public function index()
	{
		$data['records']=$this->master_model->getRecords('gri_district','','',array('district_id'=>'DESC'));
		$data['middlecontent']='district/index';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}

	public function add()
	{	
		$this->form_validation->set_rules('district_name', 'District name', 'trim|required|max_length[200]|xss_clean');
		$this->form_validation->set_rules('religion_id', 'Region', 'trim|required|numeric|xss_clean');
		
		if($this->form_validation->run())
		{
			$district_name 		= 	htmlentities($this->input->post('district_name'));
			$religion_id 	= 	$this->input->post('religion_id');
			$createdAt			=   date('Y-m-d H:i:s');
			$insert_arr=array('district_name'=>$district_name,
							  'religion_id'=>$religion_id,
							  'status'=>'1',
							  'createdat'=>	$createdAt,
							  'updatedat'=>	$createdAt 
							  );

			if($this->master_model->insertRecord('gri_district',$insert_arr)){
				//add logs
				$json_encode_data = json_encode($insert_arr);
				$log_user_id      = $this->session->userdata('supadminid');
				$ipAddr			  = $this->get_client_ip();
				$logDetails = array(
									'module_name' 	=> "District",
									'action_name' 	=> "Add",
									'json_response'	=> $json_encode_data,
									'login_id'		=> $log_user_id,
									'role_type'		=> $this->session->userdata('role_name'),
									'createdAt'		=> date('Y-m-d H:i:s'),
									'ip_addr'		=> $ipAddr
									);
				$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
				//end add logs	
			$this->session->set_flashdata('success_message','District has been created.');
			redirect(base_url().'district');
		}else{
			$this->session->set_flashdata('error_message','Error while processing your request');
			redirect(base_url().'district/add');
		}
		}
		$data['regions']=$this->master_model->getRecords('gri_region_division','','',array('religion_id'=>'DESC'));
		$data['middlecontent']='district/add';	
		$data['master_mod'] = 'Master';
	 	$this->load->view('admin/admin_combo',$data);
	}
	
/****** Edit Functionality ******/ 
	
	public function edit()
	{
		$data['error_msg']='';
		$updateid=$this->uri->segment('3');
		
			$this->form_validation->set_rules('district_name', 'District name', 'trim|max_length[200]|required|xss_clean');
		$this->form_validation->set_rules('religion_id', 'Religion', 'trim|required|numeric|xss_clean');		
			if($this->form_validation->run())
			{
				$district_name 		= 	htmlentities($this->input->post('district_name'));
				$religion_id 	= 	$this->input->post('religion_id');
			$createdAt			=   date('Y-m-d H:i:s');
			$update_arr=array('district_name'=>$district_name,
							  'religion_id'=>$religion_id,
							  'updatedat'=>	$createdAt
							  );
				
				if($this->master_model->updateRecord('gri_district',$update_arr,array('district_id'=>"'".$updateid."'")))
				{
					//add logs
					$for_id=array('record_id'=>$updateid);
					$array_first = array_merge($update_arr,$for_id);
					$json_encode_data = json_encode($array_first);
					$log_user_id      = $this->session->userdata('supadminid');
					$ipAddr			  = $this->get_client_ip();
					$logDetails = array(
										'module_name' 	=> "District",
										'action_name' 	=> "Edit",
										'json_response'	=> $json_encode_data,
										'login_id'		=> $log_user_id,
										'role_type'		=> $this->session->userdata('role_name'),
										'createdAt'		=> date('Y-m-d H:i:s'),
										'ip_addr'		=> $ipAddr
										);
					$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
					//end add logs
					$this->session->set_flashdata('success_message',' District has been updated successfully.');
					redirect(base_url().'district/edit/'.$updateid);
				}
				else
				{
					$this->session->set_flashdata('error_message','Error while processing your request');
					redirect(base_url().'district/edit');
				}
			}
		

		// $data['info']=$this->master_model->getRecords('gri_region_division',array('religion_id'=>$updateid));
		// if(count($data['info']) <= 0)
		// {
		// 	redirect(base_url().'region');
		// }
		$data['regions']=$this->master_model->getRecords('gri_region_division','','',array('religion_id'=>'DESC'));
		
		$data['info']=$this->master_model->getRecords('gri_district','','',array('district_id'=>'DESC'));	
		$data['middlecontent']='district/edit';
		$data['master_mod'] = 'Master';
	    $this->load->view('admin/admin_combo',$data);
	}
	
	public function delete()
	{
		$id=$this->uri->segment('3');
		if(!is_int($id) && $id=='')
		{
			redirect(base_url().'district');
		}
		if($this->master_model->deleteRecord('gri_district','district_id',$id))
		{

			// logs
			$for_id=array('record_id'=>$id);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "District",
								'action_name' 	=> "Delete",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			// end logs	

		  $this->session->set_flashdata('success_message','Record deleted successfully.');
		 redirect(base_url().'district');
		}
		else
		{
		  $this->session->set_flashdata('error_msg','Error while deleting record.');
		  redirect(base_url().'district');
		}
	
	}

	public function change_status()
	{
		 $id = $this->uri->segment(3);
		 $status = $this->uri->segment(4);
	
		if($status=='1'){$newstatus = '0'; }
		else if($status=='0'){$newstatus = '1';}
		if($this->master_model->updateRecord("gri_district",array('status'=>$newstatus)
		,array('district_id'=>$id)))
		{	
			// logs
			$for_id=array('record_id'=>$id,'status'=>$status);
			$json_encode_data = json_encode($for_id);
			$log_user_id      = $this->session->userdata('supadminid');
			$ipAddr			  = $this->get_client_ip();
			$logDetails = array(
								'module_name' 	=> "District",
								'action_name' 	=> "change status",
								'json_response'	=> $json_encode_data,
								'login_id'		=> $log_user_id,
								'role_type'		=> $this->session->userdata('role_name'),
								'createdAt'		=> date('Y-m-d H:i:s'),
								'ip_addr'		=> $ipAddr
								);
			$logData = $this->master_model->insertRecord('gri_log_history',$logDetails);
			// logs end
			$this->session->set_flashdata('success_message','Status changed successfully.');						           
			redirect(base_url().'district');
		}
		else
		{
			$this->session->set_flashdata('error_message','Error while updating user status.');										            
			redirect(base_url().'district');
		}
	}

	function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }
	
} // Class End

?>