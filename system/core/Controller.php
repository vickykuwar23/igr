<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');

		 // 	 $ip_address = '';
		 //    if (getenv('HTTP_CLIENT_IP'))
		 //        $ip_address = getenv('HTTP_CLIENT_IP');
		 //    else if(getenv('HTTP_X_FORWARDED_FOR'))
		 //        $ip_address = getenv('HTTP_X_FORWARDED_FOR');
		 //    else if(getenv('HTTP_X_FORWARDED'))
		 //        $ip_address = getenv('HTTP_X_FORWARDED');
		 //    else if(getenv('HTTP_FORWARDED_FOR'))
		 //        $ip_address = getenv('HTTP_FORWARDED_FOR');
		 //    else if(getenv('HTTP_FORWARDED'))
		 //       $ip_address = getenv('HTTP_FORWARDED');
		 //    else if(getenv('REMOTE_ADDR'))
		 //        $ip_address = getenv('REMOTE_ADDR');
		 //    else
   //      	$ip_address = 'UNKNOWN';
			// $createdAt		=   date('Y-m-d');			 
			
			// $getIPexist					= $this->master_model->getRecords('gri_visitors',array('ip_address'=>$ip_address));
		
			// if(count($getIPexist) == 0):
			// 	$insert_arr	=	array(	'ip_address'	=>	$ip_address,
			// 							'created_at'	=>	$createdAt
			// 							);					  
			// 	$visitorCount = $this->master_model->insertRecord('gri_visitors',$insert_arr);
			// endif;
	}

	// --------------------------------------------------------------------

	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}

}
